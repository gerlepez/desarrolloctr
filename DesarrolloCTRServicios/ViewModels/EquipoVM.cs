﻿using System;

namespace DesarrolloCTRServicios.ViewModels
{
    public class EquipoVM
    {
        public int? idEquipo { get; set; }
        public string nombreEquipo { get; set; }

        public EstadoEquipoVM estadoEquipo { get; set; }

        public int? idUsarioAlta { get; set; }
        public DateTime? fechaUsarioAlta { get; set; }
        public int? idUsuarioModifica { get; set; }
        public DateTime? fechaUsuarioModifica { get; set; }
        public bool? activo { get; set; }
        public string nombresuario { get; set; }
        public string codigoEquipo { get; set; }
        public bool? TieneReclamo { get; set; }
    }
}
