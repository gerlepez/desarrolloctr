﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class UsuarioRolVM
    {
        public int? idUsuarioRol { get; set; }
        public int? idRol { get; set; }
        public int? idUsuario { get; set; }
        public bool? activo { get; set; }
        public string nombreUsuarioRol { get; set; }
        public string nombreRol { get; set; }
        public UsuarioVM usuario { get; set; }

    }
}
