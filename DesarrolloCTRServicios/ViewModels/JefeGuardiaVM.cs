﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class JefeGuardiaVM
    {
        public int? idJefe { get; set; }
        public int? idPersona { get; set; }
        public string nombreJefe { get; set; }

        public string telefono { get; set; }
    }
}
