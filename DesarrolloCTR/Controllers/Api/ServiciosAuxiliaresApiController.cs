﻿using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers.Api
{
    public class ServiciosAuxiliaresApiController : ApiController
    {
        [System.Web.Http.Route("Api/ServiciosAuxiliares/ObtenerServiciosAuxiliares")]
        [System.Web.Http.ActionName("ObtenerServiciosAuxiliares")]
        [System.Web.Http.HttpGet]
        public object ObtenerServiciosAuxiliares(bool activo)
        {
            ServicioServiciosAuxiliares servicio = new ServicioServiciosAuxiliares();

            object lista = servicio.ObtenerServiciosAuxiliares(activo);

            // ((IDisposable)servEstacion).Dispose();

            return lista;
        }

        [System.Web.Http.Route("Api/ServiciosAuxiliares/ObtenerServicioAuxiliarPorLugar")]
        [System.Web.Http.ActionName("ObtenerServicioAuxiliarPorLugar")]
        [System.Web.Http.HttpGet]
        public object ObtenerServicioAuxiliarPorLugar(int idLugar)
        {
            ServicioServiciosAuxiliares servEstacion = new ServicioServiciosAuxiliares();

            object lista = servEstacion.ObtenerServiciosAuxiliaresPorLugar(idLugar);

            return lista;
        }
    }
}