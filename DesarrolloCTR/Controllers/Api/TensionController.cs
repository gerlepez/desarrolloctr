﻿using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DesarrolloCTR.Controllers.Api
{
    public class TensionController : ApiController
    {
        [System.Web.Http.Route("Api/Tension/TraerListaTension")]
        [System.Web.Http.ActionName("TraerListaTension")]
        [System.Web.Http.HttpGet]
        public object TraerListaTension()
        {
            ServicioTension servTension = new ServicioTension();
            var tensionServicio = servTension.TraeListaTension();
            return tensionServicio;
        }

        [System.Web.Http.Route("Api/Tension/TraerUnaTension")]
        [System.Web.Http.ActionName("TraerUnaTension")]
        [System.Web.Http.HttpGet]
        public TensionVM TraerUnaTension(int idTension)
        {
            ServicioTension servTension = new ServicioTension();
            TensionVM unaTensionServicio = servTension.TraerUnaTension(idTension);
            return unaTensionServicio;
        }
    }
}
