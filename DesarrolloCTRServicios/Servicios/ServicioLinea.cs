﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioLinea
    {

        public object ObtenerListaLinea(bool? activo)
        {
            List<LineaVM> listaLinea;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                ServicioUsuario usuarioservice = new ServicioUsuario();
                listaLinea = (from c in db.LinLinea
                              join d in db.EstEstacion on c.idEstacionOrigen equals d.idEstacion
                              join e in db.EstEstacion on c.idEstacionDestino equals e.idEstacion
                              join f in db.GralTension on c.tension equals f.idTension
                              join t in db.LinTipoLinea on c.idTipoLinea equals t.idTipoLinea
                              join p in db.ProPropiedad on c.EsPropia equals p.idPropiedad
                              where c.activo == activo && c.fechaEdicion == null
                              select new LineaVM
                              {
                                  idLinea = c.idLinea,
                                  nombreLinea = d.nombreEstacion + " - " + e.nombreEstacion + " " + f.tension + " " + c.nroTerna,
                                  idEquipo = c.idTipoEquipo,
                                  idUsarioAlta = c.idUsuarioAlta,
                                  activo = c.activo,
                                  idPagotran = c.idPagotran,
                                  nroTerna = c.nroTerna,
                                  kilometraje = c.kilometraje,
                                  km = c.kilometraje == null ? " " : c.kilometraje + " km",
                                  idPropiedad = c.EsPropia,
                                  nombrePropiedad = p.nombrePropiedad,
                                  //PropiedadLinea = c.EsPropia == true ? "Distrocuyo" : "Terceros",
                                  remunerada = c.remunerado,
                                  esRemunerada = c.remunerado == true ? "Si" : "No",
                                  tipoLinea = new TipoLineaVM
                                  {
                                      idTipoLinea = t.idTipoLinea,
                                      nombreTipoLinea = t.nombreTipo,
                                      activo = t.activo,

                                  },
                                  estacionOrigen = new EstacionVM
                                  {
                                      idEstacion = d.idEstacion,
                                      nombreEstacion = d.nombreEstacion,
                                      activo = d.activo,

                                  },
                                  estacionDestino = new EstacionVM
                                  {
                                      idEstacion = e.idEstacion,
                                      nombreEstacion = e.nombreEstacion,
                                      activo = e.activo,

                                  },
                                  tension = new TensionVM
                                  {
                                      idTension = f.idTension,
                                      tension = f.tension,
                                      activo = f.activo,

                                  },

                              }).ToList();


                object json = new { data = listaLinea };
                return json;
            }
        }
        /*/
        public object TablaLinea(bool? activo, string draw, int recordsTotal, int skip, int pageSize)
        {
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    ServicioUsuario usuarioservice  = new ServicioUsuario();
                    IQueryable<LineaVM> query = (from c in db.LinLinea
                                                  join d in db.EstEstacion on c.idEstacionOrigen equals d.idEstacion
                                                  join e in db.EstEstacion on c.idEstacionDestino equals e.idEstacion
                                                  join f in db.GralTension on c.tension equals f.idTension
                                                  join t in db.LinTipoLinea on c.idTipoLinea equals t.idTipoLinea
                                                  where c.activo == activo
                                                  select new LineaVM
                                                  {
                                                      idLinea = c.idLinea,
                                                      nombreLinea = d.nombreEstacion + " - " + e.nombreEstacion + " " + f.tension + " " + c.nroTerna,
                                                      idEquipo = c.idTipoEquipo,
                                                      idUsarioAlta = c.idUsuarioAlta,
                                                      activo = c.activo,
                                                      idPagotran = c.idPagotran,
                                                      nroTerna = c.nroTerna,

                                                      esPropia = c.EsPropia,
                                                      PropiedadLinea = c.EsPropia == true ? "Distrocuyo" : "Terceros",

                                                      tipoLinea = new TipoLineaVM
                                                      {
                                                          idTipoLinea = t.idTipoLinea,
                                                          nombreTipoLinea = t.nombreTipo,
                                                          activo = t.activo,

                                                      },
                                                      estacionOrigen = new EstacionVM
                                                      {
                                                          idEstacion = d.idEstacion,
                                                          nombreEstacion = d.nombreEstacion,
                                                          activo = d.activo,

                                                      },
                                                      estacionDestino = new EstacionVM
                                                      {
                                                          idEstacion = e.idEstacion,
                                                          nombreEstacion = e.nombreEstacion,
                                                          activo = e.activo,

                                                      },
                                                      tension = new TensionVM
                                                      {
                                                          idTension = f.idTension,
                                                          tension = f.tension,
                                                          activo = f.activo,

                                                      },

                                                  }).OrderByDescending(x => x.idLinea).AsQueryable();

                    var queryFiltrada = query;
                    recordsTotal = queryFiltrada.Count();
                    var lst = queryFiltrada.Skip(skip).Take(pageSize).ToList();

                    object json = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = lst };
                    return json;
                }
            }
            catch (Exception e)
            {
                return 400;
            }
            
        }
        */
        public object ObtenerListaLineaDistroCuyo(bool activo)
        {
            List<LineaVM> listaLinea;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                listaLinea = (from c in db.LinLinea
                              join d in db.EstEstacion on c.idEstacionOrigen equals d.idEstacion
                              join e in db.EstEstacion on c.idEstacionDestino equals e.idEstacion
                              join f in db.GralTension on c.tension equals f.idTension
                              where c.activo == activo //&& c.EsPropia == true
                              select new LineaVM
                              {
                                  idLinea = c.idLinea,
                                  nombreLinea = c.nombreLinea,
                                  idEquipo = c.idLinea,
                                  nombreEquipo = c.nombreLinea,
                                  idUsarioAlta = c.idUsuarioAlta,
                                  //    fechaUsarioAlta = c.fechaUsuarioAlta,
                                  activo = c.activo,
                                  codigoLinea = c.codigoLinea,
                                  idPagotran = c.idPagotran,
                                  //    idBde = c.idBde,
                                  nroTerna = c.nroTerna,
                                  idPropiedad = c.EsPropia,
                                  kilometraje = c.kilometraje,
                                  estacionOrigen = new EstacionVM
                                  {
                                      idEstacion = d.idEstacion,
                                      nombreEstacion = d.nombreEstacion,
                                      activo = d.activo,

                                  },
                                  estacionDestino = new EstacionVM
                                  {
                                      idEstacion = e.idEstacion,
                                      nombreEstacion = e.nombreEstacion,
                                      activo = e.activo,

                                  },
                                  tension = new TensionVM
                                  {
                                      idTension = f.idTension,
                                      tension = f.tension,
                                      activo = f.activo,

                                  },

                              }).ToList();
            }
            object json = new { data = listaLinea };
            return json;
        }

        public object ObtenerLineasPorEstacion(int idEstacion, int idNovedad)
        {
            List<LineaVM> listaLinea;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                if (idNovedad == 0)
                {
                    listaLinea = (from c in db.LinLinea
                                  join d in db.EstEstacion on c.idEstacionOrigen equals d.idEstacion
                                  join e in db.EstEstacion on c.idEstacionDestino equals e.idEstacion
                                  join f in db.GralTension on c.tension equals f.idTension
                                  where c.activo == true && e.idEstacion == idEstacion && c.fechaEdicion == null
                                  select new LineaVM
                                  {
                                      idLinea = c.idLinea,
                                      idEquipo = c.idLinea,
                                      nombreLinea = c.nombreLinea,
                                      nombreEquipo = c.nombreLinea,
                                      idUsarioAlta = c.idUsuarioAlta,
                                      //    fechaUsarioAlta = c.fechaUsuarioAlta,
                                      activo = c.activo,
                                      codigoLinea = c.codigoLinea,
                                      idPagotran = c.idPagotran,
                                      //    idBde = c.idBde,
                                      nroTerna = c.nroTerna,
                                      kilometraje = c.kilometraje,
                                      estacionOrigen = new EstacionVM
                                      {
                                          idEstacion = d.idEstacion,
                                          nombreEstacion = d.nombreEstacion,
                                          activo = d.activo,

                                      },
                                      estacionDestino = new EstacionVM
                                      {
                                          idEstacion = e.idEstacion,
                                          nombreEstacion = e.nombreEstacion,
                                          activo = e.activo,

                                      },
                                      tension = new TensionVM
                                      {
                                          idTension = f.idTension,
                                          tension = f.tension,
                                          activo = f.activo,

                                      },

                                  }).ToList();
                }
                else
                {
                    listaLinea = (from c in db.LinLinea
                                  join d in db.EstEstacion on c.idEstacionOrigen equals d.idEstacion
                                  join e in db.EstEstacion on c.idEstacionDestino equals e.idEstacion
                                  join f in db.GralTension on c.tension equals f.idTension
                                  join n in db.NovNovedad on idNovedad equals n.idNovedad
                                  where e.idEstacion == idEstacion && c.fechaCreacion < n.horaNovedad && (c.fechaEdicion == null || c.fechaEdicion > n.horaNovedad)
                                  select new LineaVM
                                  {
                                      idLinea = c.idLinea,
                                      idEquipo = c.idLinea,
                                      nombreLinea = c.nombreLinea,
                                      nombreEquipo = c.nombreLinea,
                                      idUsarioAlta = c.idUsuarioAlta,
                                      //    fechaUsarioAlta = c.fechaUsuarioAlta,
                                      activo = c.activo,
                                      codigoLinea = c.codigoLinea,
                                      idPagotran = c.idPagotran,
                                      //    idBde = c.idBde,
                                      nroTerna = c.nroTerna,
                                      kilometraje = c.kilometraje,
                                      estacionOrigen = new EstacionVM
                                      {
                                          idEstacion = d.idEstacion,
                                          nombreEstacion = d.nombreEstacion,
                                          activo = d.activo,

                                      },
                                      estacionDestino = new EstacionVM
                                      {
                                          idEstacion = e.idEstacion,
                                          nombreEstacion = e.nombreEstacion,
                                          activo = e.activo,

                                      },
                                      tension = new TensionVM
                                      {
                                          idTension = f.idTension,
                                          tension = f.tension,
                                          activo = f.activo,

                                      },

                                  }).ToList();
                }

            }
            object json = new { data = listaLinea };
            return json;
        }

        public object ObtenerListaTipoLinea()
        {
            List<TipoLineaVM> tipoLinea;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                tipoLinea = (from c in db.LinTipoLinea
                             where c.activo == true
                             select new TipoLineaVM
                             {
                                 activo = c.activo,
                                 idTipoLinea = c.idTipoLinea,
                                 nombreTipoLinea = c.nombreTipo

                             }).ToList();
            }
            object json = new { data = tipoLinea };
            return json;
        }

        public object ObtenerLineaPorTipoPropiedad(int idTipoLinea, int tipoPropiedad, int idNovedad = 0)
        {
            List<LineaVM> linea;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                if (idNovedad == 0)
                {
                    linea = (from c in db.LinLinea
                             join p in db.ProPropiedad on c.EsPropia equals p.idPropiedad
                             where c.activo == true && c.idTipoLinea == idTipoLinea && c.EsPropia == tipoPropiedad
                             select new LineaVM
                             {
                                 activo = c.activo,
                                 idLinea = c.idLinea,
                                 nombreLinea = c.nombreLinea,
                                 codigoLinea = c.codigoLinea

                             }).ToList();
                }
                else
                {
                    linea = (from c in db.LinLinea
                             join n in db.NovNovedad on idNovedad equals n.idNovedad
                             where c.idTipoLinea == idTipoLinea && c.EsPropia == tipoPropiedad && c.fechaCreacion < n.horaNovedad && (c.fechaEdicion == null || c.fechaEdicion > n.horaNovedad)
                             select new LineaVM
                             {
                                 activo = c.activo,
                                 idLinea = c.idLinea,
                                 nombreLinea = c.nombreLinea,
                                 codigoLinea = c.codigoLinea

                             }).ToList();

                }
            }
            object json = new { data = linea };
            return json;
        }

        public int AltaLinea(LineaCrudVM linea, int idUsuario)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var nombreLineaRepetida = linea.nombreEstacionOrigen + " - " + linea.nombreEstacionDestino + " " + linea.tension + " " + linea.nroTerna;
                var lineaRepetida = db.LinLinea.Where(x => x.nombreLinea == nombreLineaRepetida).FirstOrDefault();

                var consultaunLinea = db.LinLinea.Where(x => x.idLinea == linea.idLinea).FirstOrDefault();
                if (consultaunLinea == null && lineaRepetida == null)
                {
                    try
                    {
                        LinLinea lin = new LinLinea
                        {
                            nombreLinea = linea.nombreEstacionOrigen + " - " + linea.nombreEstacionDestino + " " + linea.tension + " " + linea.nroTerna,
                            idTipoLinea = linea.idTipoLinea,
                            EsPropia = linea.idPropiedad,
                            idEstacionOrigen = linea.idEstacionOrigen,
                            idEstacionDestino = linea.idEstacionDestino,
                            idPagotran = linea.idPagotran,
                            tension = linea.idTension,
                            nroTerna = linea.nroTerna,
                            idUsuarioAlta = idUsuario,
                            activo = true,
                            idTipoEquipo = 4,
                            fechaCreacion = DateTime.Now,
                            kilometraje = linea.kilometraje,
                            remunerado = linea.remunerada,
                        };
                        db.LinLinea.Add(lin);
                        db.SaveChanges();

                        if (linea.remunerada == true)
                        {
                            IndisponibilidadRemuLineas LinRemu = new IndisponibilidadRemuLineas
                            {
                                idLinea = lin.idLinea,
                                activo = true,
                            };
                            db.IndisponibilidadRemuLineas.Add(LinRemu);
                            db.SaveChanges();
                        }

                        return 200;
                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                             ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        return 400;

                    }
                }
                else
                {
                    return 300; //ya existe ese dato
                }
            }

        }

        public int ModificarLinea(LineaCrudVM linea, int idUsuario)
        {
            //ver
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunLinea = db.LinLinea.Where(x => x.idLinea == linea.idLinea).FirstOrDefault();
                if (consultaunLinea != null)
                {
                    //consultaunLinea.nombreLinea = linea.nombreEstacionOrigen + " - " + linea.nombreEstacionDestino + " " + linea.tension + " " + linea.nroTerna;
                    //consultaunLinea.activo = true;
                    //consultaunLinea.idUsuarioModifica = idUsuario;
                    //consultaunLinea.fechaUsuarioModifica = DateTime.Now;
                    //consultaunLinea.idEstacionOrigen = linea.idEstacionOrigen;
                    //consultaunLinea.idEstacionDestino = linea.idEstacionDestino;
                    //consultaunLinea.tension = linea.idTension;
                    //consultaunLinea.codigoLinea = linea.codigoLinea;
                    //consultaunLinea.idPagotran = linea.idPagotran;
                    //consultaunLinea.nroTerna = linea.nroTerna;
                    //consultaunLinea.EsPropia = linea.idPropiedad;
                    //consultaunLinea.idTipoLinea = linea.idTipoLinea;

                    consultaunLinea.activo = false;
                    consultaunLinea.remunerado = false;
                    consultaunLinea.fechaEdicion = DateTime.Now;


                    LinLinea lin = new LinLinea
                    {
                        nombreLinea = linea.nombreEstacionOrigen + " - " + linea.nombreEstacionDestino + " " + linea.tension + " " + linea.nroTerna,
                        idTipoLinea = linea.idTipoLinea,
                        EsPropia = linea.idPropiedad,
                        idEstacionOrigen = linea.idEstacionOrigen,
                        idEstacionDestino = linea.idEstacionDestino,
                        idPagotran = linea.idPagotran,
                        tension = linea.idTension,
                        nroTerna = linea.nroTerna,
                        idUsuarioAlta = idUsuario,
                        activo = true,
                        idTipoEquipo = 4,
                        fechaCreacion = DateTime.Now,
                        kilometraje = linea.kilometraje,
                        remunerado = linea.remunerada,
                    };
                    db.LinLinea.Add(lin);
                    db.SaveChanges();

                    var lineaRemunerada = db.IndisponibilidadRemuLineas.Where(x => x.idLinea == linea.idLinea && x.activo == true).FirstOrDefault();
                    var kv = db.GralTension.Where(x => x.activo == true && x.idTension == lin.tension).FirstOrDefault();
                    decimal? cargoxHora = null;
                    decimal? Mesx = null;
                    if (lin.remunerado == true)
                    {
                        if (lineaRemunerada != null)
                        { //Si la linea ya existe en la tabla de IndisponibilidadesLinea entra y a la que estaba en esa tabla le pone como FALSE , y crea uno con el nuevo id de la linea

                            lineaRemunerada.activo = false;
                            if (kv.tension == "220" || kv.tension == "132")
                            {
                                var UkvInt = Convert.ToInt32(kv.tension);
                                var monto = db.IndisponibilidadMontoLineas.Where(x => x.activo == true && x.kv == UkvInt).FirstOrDefault();
                                if (lineaRemunerada.remunerado != null)
                                {
                                    cargoxHora = (monto.monto * lin.kilometraje * lineaRemunerada.remunerado / 100) / 100;

                                    var fechaHoy = new DateTime();
                                    var añoHoy = fechaHoy.Year;
                                    var mesHoy = fechaHoy.Month;

                                    int cantidadDias = DateTime.DaysInMonth(fechaHoy.Year, fechaHoy.Month);
                                    Mesx = cargoxHora * (cantidadDias * 24);

                                }

                            }


                            IndisponibilidadRemuLineas LinRemu = new IndisponibilidadRemuLineas
                            {
                                remunerado = lineaRemunerada.remunerado,
                                origenRegulatorio = lineaRemunerada.origenRegulatorio,
                                idLinea = lin.idLinea,
                                cargoRealxHoras = cargoxHora,
                                activo = true,
                                xMes = Mesx,
                            };
                            db.IndisponibilidadRemuLineas.Add(LinRemu);
                            db.SaveChanges();


                            var lineaIndisponible = db.IndisponibilidadLineas.Where(x => x.IdLineaIndisRemu == lineaRemunerada.idLineaIndisRemu && x.idLinea == consultaunLinea.idLinea).FirstOrDefault();
                            if (lineaIndisponible != null)
                            {
                                ServicioIndisponibilidadesRemu servIndis = new ServicioIndisponibilidadesRemu();
                                lineaIndisponible.IdLineaIndisRemu = LinRemu.idLineaIndisRemu;
                                lineaIndisponible.idLinea = lin.idLinea;
                                db.SaveChanges();
                                servIndis.ModificoLineaIndisponibles(lineaIndisponible, lineaRemunerada.idResolucion);
                            }


                        }
                        else //Si no existe la liena en indisponibilidades , la crea , PERO SIN LA REMUNERADO % 
                        {

                            IndisponibilidadRemuLineas LinRemu = new IndisponibilidadRemuLineas
                            {
                                idLinea = lin.idLinea,
                                activo = true,
                            };
                            db.IndisponibilidadRemuLineas.Add(LinRemu);
                            db.SaveChanges();
                        }
                    }

                }
            }


            return 200;
        }

        public int EliminarLinea(EliminarLineaVM linea)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnaLinea = db.LinLinea.Where(x => x.idLinea == linea.idLinea).FirstOrDefault();
                if (consultaUnaLinea != null)
                {
                    consultaUnaLinea.activo = false;
                }
                db.SaveChanges();
            }
            return 200;
        }

        public bool ValidarLinea(LineaVM linea)
        {

            if (linea.estacionOrigen.idEstacion > 0
                && linea.estacionDestino.idEstacion > 0
                 && linea.tension.idTension > 0
                 && linea.nroTerna < 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool ValidarAltaLinea(LineaCrudVM linea)
        {
            if (linea == null)
            {
                return false;
            }
            else
            {
                if (linea.idEstacionOrigen > 0
                && linea.idEstacionDestino > 0
                 && linea.idTension > 0
                 && linea.nroTerna > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public LineaVM ConsultarUnaLinea(int? idLinea)
        {
            LineaVM linea = new LineaVM();

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                linea = (from c in db.LinLinea
                         join d in db.EstEstacion on c.idEstacionOrigen equals d.idEstacion
                         join e in db.EstEstacion on c.idEstacionDestino equals e.idEstacion
                         join f in db.GralTension on c.tension equals f.idTension
                         join tl in db.LinTipoLinea on c.idTipoLinea equals tl.idTipoLinea
                         join p in db.ProPropiedad on c.EsPropia equals p.idPropiedad
                         where c.idLinea == idLinea
                         select new LineaVM
                         {
                             idLinea = c.idLinea,
                             nombreLinea = c.nombreLinea,
                             nombreEquipo = "Línea",
                             idUsarioAlta = c.idUsuarioAlta,
                             //    fechaUsarioAlta = c.fechaUsuarioAlta,
                             activo = c.activo,
                             codigoLinea = c.codigoLinea,
                             idPagotran = c.idPagotran,
                             //     idBde = c.idBde,
                             nroTerna = c.nroTerna,
                             idTipoLinea = tl.idTipoLinea,
                             TipoLinea = tl.nombreTipo,
                             idPropiedad = c.EsPropia,
                             nombrePropiedad = p.nombrePropiedad,
                             kilometraje = c.kilometraje,
                             remunerada = c.remunerado,
                             estacionOrigen = new EstacionVM
                             {
                                 idEstacion = d.idEstacion,
                                 nombreEstacion = d.nombreEstacion,
                                 activo = d.activo,

                             },
                             estacionDestino = new EstacionVM
                             {
                                 idEstacion = e.idEstacion,
                                 nombreEstacion = e.nombreEstacion,
                                 activo = e.activo,

                             },
                             tension = new TensionVM
                             {
                                 idTension = f.idTension,
                                 tension = f.tension,
                                 activo = f.activo,

                             },

                         }).FirstOrDefault();
            }
            return linea;
        }

        public int RecuperarLinea(EliminarLineaVM linea)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunLinea = db.LinLinea.Where(x => x.idLinea == linea.idLinea).FirstOrDefault();
                if (consultaunLinea != null)
                {
                    consultaunLinea.activo = true;
                    db.SaveChanges();
                }
            }
            return 200;
        }

        public List<IndisponibilidadVM> ConsultarIndisponibilidadLinea(int idLinea, int idNovedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                List<IndisponibilidadVM> listaDisponibilidad = new List<IndisponibilidadVM>();
                var consultaUnaLinea = db.LinLinea.Where(x => x.idLinea == idLinea).FirstOrDefault();

                return listaDisponibilidad;
            }
        }

        public PosesionLineaVM ConsultarPosesionLinea(int? posesionLinea)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var posesion = (from c in db.LinTipoLinea
                                where c.activo == true && c.idTipoLinea == posesionLinea
                                select new PosesionLineaVM
                                {
                                    idPosesionLinea = c.idTipoLinea,
                                    nombrePosesionLinea = c.nombreTipo,
                                }).FirstOrDefault();
                return posesion;
            }
        }

        public PropiedadLineaVM ConsultarPropiedadLinea(int? idLinea)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var propiedadLinea = (from c in db.LinLinea
                                      join p in db.ProPropiedad on c.EsPropia equals p.idPropiedad
                                      where c.idLinea == idLinea
                                      select new PropiedadLineaVM
                                      {
                                          idPropiedad = c.EsPropia,
                                          nombrePropiedad = p.nombrePropiedad,

                                      }).FirstOrDefault();
                return propiedadLinea;
            }
        }

        public object ObtenerListaPropiedades()
        {
            List<LineaVM> listaLinea;
            using (IntranetCTREntities bd = new IntranetCTREntities())
            {
                listaLinea = (from p in bd.ProPropiedad
                              where p.activa == true
                              select new LineaVM
                              {
                                  PropiedadLinea = p.nombrePropiedad,
                                  idPropiedad = p.idPropiedad,
                              }).ToList();
                object json = new { data = listaLinea };
                return json;
            }
        }


    }
}
