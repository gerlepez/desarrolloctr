﻿using System;

namespace DesarrolloCTRServicios.ViewModels
{
    public class AutorizacionTrabajoVM
    {
        public int idAutorizacionTrabajo { get; set; }
        public string descripcionAutorizacionTrabajo { get; set; }
        public int? idLicencia { get; set; }
        public int idNovedad { get; set; }
        public DateTime? fechaHoraInicioAutorizacionTrabajo { get; set; }
        public DateTime? fechaHoraFinAutorizacionTrabajo { get; set; }
        public int? idUsarioAlta { get; set; }
        public DateTime? fechaUsarioAlta { get; set; }
        public int? idUsuarioModifica { get; set; }
        public DateTime? fechaUsuarioModifica { get; set; }
        public bool? activo { get; set; }
        public string nombresuario { get; set; }
        public int? idUsuario { get; set; }
        public DateTime? fechaInicio { get; set; }
        public DateTime horaInicio { get; set; }
        public DateTime? fechaFin { get; set; }
        public DateTime horaFin { get; set; }
        //public DateTime? fecha {get;set;}
        public string fechaInicioParseada { get; set; }
        public string fechaFnParseada { get; set; }
        public string horaInicioParseada { get; set; }
        public string horaFnParseada { get; set; }
        //public DateTime hora { get; set; }
        public int? idEmpleado { get; set; }
        public int? codResultadoCierre { get; set; }
        public string resultCierre { get; set; }
        public int? codigoResultadoAlta { get; set; }
        public string codigoAutorizacion { get; set; }
        public EstadoAutorizacionVM estadoAutorizacion { get; set; }
        public int? numAutorizacion { get; set; }
        public string nombreAutorizado { get; set; }
    }
}
