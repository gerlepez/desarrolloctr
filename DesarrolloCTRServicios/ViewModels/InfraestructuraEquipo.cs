﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class InfraestructuraEquipo
    {
        public int idInfraEquipo { get; set; }
        public string nombreInfraEquipo { get; set; }
        public int idEquipo { get; set; }
        public string nombreEquipo { get; set; }
        public string codigoInfraEquipo { get; set; }
        public bool? activo { get; set; }
    }
}
