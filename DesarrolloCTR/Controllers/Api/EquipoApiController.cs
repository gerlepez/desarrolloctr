﻿using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DesarrolloCTR.Controllers.Api
{
    public class EquipoApiController : ApiController
    {
        [System.Web.Http.Route("Api/Equipo/ObtenerListaEquipo")]
        [System.Web.Http.ActionName("ObtenerListaEquipo")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaEquipo(bool activo)
        {
            ServicioEquipo servEquipo = new ServicioEquipo();

            object listaEquipo = servEquipo.ObtenerListaEquipo(activo);

            //((IDisposable)servEquipo).Dispose();

            return listaEquipo;
        }

        [System.Web.Http.Route("Api/Equipo/ObtenerEquipoPorTipo")]
        [System.Web.Http.ActionName("ObtenerEquipoPorTipo")]
        [System.Web.Http.HttpGet]
        public List<EquipoVM> ObtenerEquipoPorTipo(string codTipo)
        {
            ServicioEquipo servEquipo = new ServicioEquipo();

            List<EquipoVM> listaEquipo = servEquipo.ObtenerEquipoPorTipo(codTipo);

            //((IDisposable)servEquipo).Dispose();

            return listaEquipo.OrderBy(x => x.nombreEquipo).ToList();
        }

        [System.Web.Http.Route("Api/Equipo/ObtenerEquiposIndisponibles")]
        [System.Web.Http.ActionName("ObtenerEquiposIndisponibles")]
        [System.Web.Http.HttpGet]
        public List<EquipoVM> ObtenerEquiposIndisponibles(string codTipo)
        {
            ServicioEquipo servEquipo = new ServicioEquipo();

            List<EquipoVM> listaEquipo = servEquipo.ObtenerEquiposIndisponibles(codTipo);

            //((IDisposable)servEquipo).Dispose();

            return listaEquipo.OrderBy(x => x.nombreEquipo).ToList();
        }


        [System.Web.Http.Route("Api/Equipo/ObtenerEquipoDetallePorEstacion")]
        [System.Web.Http.ActionName("ObtenerEquipoDetallePorEstacion")]
        [System.Web.Http.HttpGet]
        public object ObtenerEquipoDetallePorEstacion(int idEstacion, string codigoEquipo , int idNovedad = 0)
        {
            ServicioEquipo servEquipo = new ServicioEquipo();

            object lista = servEquipo.ObtenerEquipoDetallePorEstacion(idEstacion, codigoEquipo , idNovedad);

            //((IDisposable)servEquipo).Dispose();

            return lista;
        }

        [System.Web.Http.Route("Api/Equipo/ObtenerEquiposPorInfraestructura")]
        [System.Web.Http.ActionName("ObtenerEquiposPorInfraestructura")]
        [System.Web.Http.HttpGet]
        public object ObtenerEquiposPorInfraestructura(string codigoInfra, bool activo)
        {
            ServicioEquipo servEquipo = new ServicioEquipo();

            object lista = servEquipo.ObtenerEquiposPorInfraestructura(codigoInfra, activo);

            

            return lista;
        }

        [System.Web.Http.Route("Api/Equipo/ObtenerEquiposPorIndisponibilidad")]
        [System.Web.Http.ActionName("ObtenerEquiposPorIndisponibilidad")]
        [System.Web.Http.HttpGet]
        public object ObtenerEquiposPorIndisponibilidad(bool activo)
        {
            ServicioEquipo servEquipo = new ServicioEquipo();

            object lista = servEquipo.ObtenerEquiposPorIndisponibilidad(activo);
                       
            return lista;
        }

        [System.Web.Http.Route("Api/Equipo/AltaEquipo")]
        [System.Web.Http.ActionName("AltaEquipo")]
        [System.Web.Http.HttpPost]
        public int AltaEquipo(EquipoVM equipo)
        {
            var idUsuarioLogueado = 0;
            if (equipo != null)
            {
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(equipo.nombresuario);
            }

            ServicioEquipo servEquipo = new ServicioEquipo();
            int codigo;
            if (servEquipo.ValidarEquipo(equipo))
            {
                codigo = servEquipo.AltaEquipo(equipo, idUsuarioLogueado);
            }
            else
            {
                codigo = 400;
            }

            //((IDisposable)servEquipo).Dispose();

            return codigo;
        }

        [System.Web.Http.Route("Api/Equipo/ModificarEquipo")]
        [System.Web.Http.ActionName("ModificarEquipo")]
        [System.Web.Http.HttpPost]
        public int ModificarEquipo(EquipoVM equipo)
        {
            var idUsuarioLogueado = 0;
            if (equipo != null)
            {
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(equipo.nombresuario);
            }
            ServicioEquipo servEquipo = new ServicioEquipo();
            int codigo;
            if (servEquipo.ValidarEquipo(equipo))
            {
                codigo = servEquipo.ModificarEquipo(equipo, idUsuarioLogueado);
            }
            else
            {
                codigo = 400;
            }

            //((IDisposable)servEquipo).Dispose();

            return codigo;
        }

        [System.Web.Http.Route("Api/Equipo/EliminarEquipo")]
        [System.Web.Http.ActionName("EliminarEquipo")]
        [System.Web.Http.HttpPost]
        public int EliminarEquipo(EquipoVM equipo)
        {
            ServicioEquipo servEquipo = new ServicioEquipo();
            int codigo = servEquipo.EliminarEquipo(equipo);

            //((IDisposable)servEquipo).Dispose();

            return codigo;
        }

        [System.Web.Http.Route("Api/Equipo/ConsultarUnEquipo")]
        [System.Web.Http.ActionName("ConsultarUnEquipo")]
        [System.Web.Http.HttpGet]
        public EquipoVM ConsultarUnEquipo(string codigoEquipo)
        {
            ServicioEquipo servEquipo = new ServicioEquipo();
            EquipoVM equipo = servEquipo.ConsultarUnEquipo(codigoEquipo);

            //((IDisposable)servEstacion).Dispose();

            return equipo;
        }

        [System.Web.Http.Route("Api/Equipo/ConsultarUnEquipoTipoEquipo")]
        [System.Web.Http.ActionName("ConsultarUnEquipoTipoEquipo")]
        [System.Web.Http.HttpGet]
        public object ConsultarUnEquipoTipoEquipo(int idEquipo, string codigoEquipo, int idCentral)
        {
            ServicioEquipo servEquipo = new ServicioEquipo();
            var equipo = servEquipo.ConsultarEquipoDetalle(idEquipo, codigoEquipo, idCentral);

            //((IDisposable)servEstacion).Dispose();

            return equipo;
        }

        [System.Web.Http.Route("Api/Equipo/RecuperarEquipo")]
        [System.Web.Http.ActionName("RecuperarEquipo")]
        [System.Web.Http.HttpPost]
        public int RecuperarEquipo(EquipoVM equipo)
        {
            ServicioEquipo servEquipo = new ServicioEquipo();
            int codigo = servEquipo.RecuperarEquipo(equipo);

            //((IDisposable)servEquipo).Dispose();

            return codigo;
        }
        [System.Web.Http.Route("Api/Equipo/ObtenerTipoIndisponibilidad")]
        [System.Web.Http.ActionName("ObtenerTipoIndisponibilidad")]
        [System.Web.Http.HttpGet]
        public int ObtenerTipoIndisponibilidad(int idEquipo)
        {
            ServicioEquipo serv = new ServicioEquipo();
            int response = serv.ObtenerTipoIndisponibilidad(idEquipo);
            return response;
        }

    }
}

