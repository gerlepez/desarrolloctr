﻿using System;

namespace DesarrolloCTRServicios.ViewModels
{
    public class TransformadorVM
    {
        public int idTransformador { get; set; }
        public int idEquipo { get; set; }
        public int? idUsarioAlta { get; set; }
        public int? idUsuarioModifica { get; set; }
        public int? idPagotran { get; set; }
        public int? idBde { get; set; }
        public int? potenciaAparente { get; set; }
        public int? idGeneral { get; set; }

        public decimal? tension1 { get; set; }
        public decimal? tension2 { get; set; }
        public decimal? tension3 { get; set; }
        public decimal? potencia1 { get; set; }
        public decimal? potencia2 { get; set; }
        public decimal? potencia3 { get; set; }


        public string potencias { get; set; }
        public string tensiones { get; set; }
        public string idCodigoSap { get; set; }
        public string nombreTransformador { get; set; }
        public string nombreEquipo { get; set; }
        public string nombresuario { get; set; }
        public string codigoTransformador { get; set; }
        public string codigoEquipo { get; set; }
        public string descripcion { get; set; }
        public string esRemunerado { get; set; }

        public DateTime? fechaUsarioAlta { get; set; }
        public DateTime? fechaUsuarioModifica { get; set; }

        public bool? activo { get; set; }
        public bool? esAutomatismo { get; set; }
        public bool? remunerado { get; set; }

        public TensionVM tensionEntrada { get; set; }
        public TensionVM tensionSalida1 { get; set; }
        public TensionVM tensionSalida2 { get; set; }
        public EstacionVM estacion { get; set; }
        public IndisponibilidadVM disponibilidad { get; set; }

    }
}
