﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioEstadoNovedad
    {
        public List<EstadoNovedadVM> ObtenerListaEstadoNovedad()
        {
            List<EstadoNovedadVM> listaEstadoNovedad;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                listaEstadoNovedad = (from c in db.NovEstadoNovedad
                                      where c.activo == true
                                      select new EstadoNovedadVM
                                      {
                                          idEstadoNovedad = c.idEstadoNovedad,
                                          nombreEstadoNovedad = c.estadoNovedad,
                                          activo = c.activo
                                      }).ToList();
            }

            return listaEstadoNovedad;
        }

    }
}
