﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioActuacion
    {
        private IntranetCTREntities db = new IntranetCTREntities();
        //Este es para la tabla de 1 Actuacion
        public object ObtenerActuacion(bool activo)
        {
            List<ActuacionVM> listaActuacion;
            listaActuacion = (from a in db.NovActuacionEstado
                              join ta in db.NovTipoActuacionEstado on a.idTipoActuacionEstado equals ta.idTipoActuacionEstado
                              where a.activo == activo
                              select new ActuacionVM
                              {
                                  activo = a.activo,
                                  idActuacion = a.idActuacionEstado,
                                  nombreActuacion = a.nombreActuacionEstado,
                                  nombreTipoActuacion = ta.nombreTipoActuacionEstado,
                              }).ToList();
            object json = new { data = listaActuacion };
            return json;
        }

        public object ObtenerActuacionIndisponibilidad(bool activo)
        {
            List<ActuacionVM> listaActuacion;
            listaActuacion = (from a in db.NovActuacionEstado
                              where a.activo == activo && a.itn != null
                              select new ActuacionVM
                              {
                                  activo = a.activo,
                                  idActuacion = a.idActuacionEstado,
                                  nombreActuacion = a.nombreActuacionEstado,
                                  itn = a.itn
                              }).ToList();
            object json = new { data = listaActuacion };
            return json;
        }
        /*
        public ActuacionVM ConsultarUnaActuacion(int? idActuacion)
        {
            var actuacion = (from a in db.NovActuacionEstado
                             where a.idActuacionEstado == idActuacion
                             select new ActuacionVM
                             {
                                 activo = a.activo,
                                 nombreActuacion = a.nombreActuacionEstado,
                                 idActuacion = a.idActuacionEstado,
                                 idTipoActuacionEstado = a.idTipoActuacionEstado

                             }).FirstOrDefault();
            return actuacion;
        }*/
        public List<ActuacionVM> ObtenerActuacionPorEquipo(string codigoEquipo, int? idEquipo, int idNovedad)
        {
            List<ActuacionVM> actuacion = new List<ActuacionVM>();

            var idTipoEquipo = db.EquiEquipo.Where(x => x.codigoEquipo == codigoEquipo).FirstOrDefault();

            if (idTipoEquipo != null && codigoEquipo != null && idEquipo != -1)
            {
                actuacion = (from a in db.NovActuacionEstado
                             join ae in db.NovActuacTipoEquipo on a.idActuacionEstado equals ae.idActuacion
                             where ae.idEquipo == idTipoEquipo.idEquipo && a.activo == true
                             select new ActuacionVM
                             {
                                 activo = a.activo,
                                 nombreActuacion = a.nombreActuacionEstado,
                                 idActuacion = a.idActuacionEstado,
                                 idTipoActuacion = ae.idTipoActuacion,
                                 idTipoActuacionEstado = a.idTipoActuacionEstado
                             }).Where((x) => x.idActuacion > 0).Distinct().ToList();
                actuacion = actuacion.GroupBy(p => p.idActuacion)
                            .Select(g => g.First())
                            .ToList();
            }
            else
            {
                actuacion = (from a in db.NovActuacionEstado
                             join ae in db.NovActuacTipoEquipo on a.idActuacionEstado equals ae.idActuacion
                             where a.activo == true
                             select new ActuacionVM
                             {
                                 activo = a.activo,
                                 nombreActuacion = a.nombreActuacionEstado,
                                 idActuacion = a.idActuacionEstado,
                                 idTipoActuacion = ae.idTipoActuacion,
                                 idTipoActuacionEstado = a.idTipoActuacionEstado
                             }).Where((x) => x.idActuacion > 0).Distinct().ToList();
                actuacion = actuacion.GroupBy(p => p.idActuacion)
                            .Select(g => g.First())
                            .ToList();
            }

            return actuacion;
        }



        public object ObtenerTipoActPorActEquipo(int idActuacion, string codigoEquipo)
        {
            List<ActuacionVM> actuacion = new List<ActuacionVM>();

            var idEquipo = (from e in db.EquiEquipo where e.codigoEquipo == codigoEquipo select e).FirstOrDefault();

            if (idEquipo != null)
            {
                actuacion = (from t in db.NovActuacTipoEquipo
                             join nt in db.NovTipoActuacion on t.idTipoActuacion equals nt.idTipoActuacion
                             where nt.activo == true && t.idActuacion == idActuacion && t.idEquipo == idEquipo.idEquipo
                             select new ActuacionVM
                             {
                                 activo = nt.activo,
                                 idTipoActuacion = nt.idTipoActuacion,
                                 nombreTipoActuacion = nt.nombreTipoActuacion
                             }).ToList();
            }
            else
            {
                actuacion = (from t in db.NovActuacTipoEquipo
                             join nt in db.NovTipoActuacion on t.idTipoActuacion equals nt.idTipoActuacion
                             where nt.activo == true && t.idActuacion == idActuacion
                             select new ActuacionVM
                             {
                                 activo = nt.activo,
                                 idTipoActuacion = nt.idTipoActuacion,
                                 nombreTipoActuacion = nt.nombreTipoActuacion
                             }).ToList();
            }

            return actuacion;
        }



        //TABLA DE LAS RELACIONES
        public object tablaActuacion(bool activo)
        {
            List<ActuacionVM> listaActuacion;
            listaActuacion = (from ate in db.NovActuacTipoEquipo
                              join a in db.NovActuacionEstado on ate.idActuacion equals a.idActuacionEstado
                              join tae in db.NovTipoActuacionEstado on a.idTipoActuacionEstado equals tae.idTipoActuacionEstado
                              join e in db.EquiEquipo on ate.idEquipo equals e.idEquipo
                              join ta in db.NovTipoActuacion on ate.idTipoActuacion equals ta.idTipoActuacion
                              where ate.activo == activo && a.activo == true
                              select new ActuacionVM
                              {
                                  idActuacTipoFallaEquipo = ate.idActuacTipoFallaEquipo,
                                  activo = ate.activo,
                                  idActuacion = a.idActuacionEstado,
                                  nombreActuacion = a.nombreActuacionEstado,

                                  tipoActuacionEstado = new TipoActuacionEstadoVM
                                  {
                                      idTipoActuacionEstado = tae.idTipoActuacionEstado,
                                      nombreTipoActuacionEstado = tae.nombreTipoActuacionEstado,
                                      activo = tae.activo
                                  },
                                  tipoActuacion = new TipoActuacionVM
                                  {
                                      idTipoActuacion = ta.idTipoActuacion,
                                      nombreTipoActuacion = ta.nombreTipoActuacion,
                                      activo = ta.activo
                                  },
                                  equipo = new EquipoVM
                                  {
                                      idEquipo = e.idEquipo,
                                      nombreEquipo = e.nombreEquipo,
                                      activo = e.activo,
                                  }

                              }).ToList();
            object json = new { data = listaActuacion };
            return json;
        }

        //Consulta las relaciones
        public ActuacionVM ConsultarUnaActuacion(int idActuacion)
        {
            var actuacion = (from ate in db.NovActuacTipoEquipo
                             join a in db.NovActuacionEstado on ate.idActuacion equals a.idActuacionEstado
                             join tae in db.NovTipoActuacionEstado on a.idTipoActuacionEstado equals tae.idTipoActuacionEstado
                             join e in db.EquiEquipo on ate.idEquipo equals e.idEquipo
                             join ta in db.NovTipoActuacion on ate.idTipoActuacion equals ta.idTipoActuacion
                             where a.idActuacionEstado == idActuacion
                             select new ActuacionVM
                             {
                                 activo = a.activo,
                                 idActuacion = a.idActuacionEstado,
                                 nombreActuacion = a.nombreActuacionEstado,

                                 tipoActuacionEstado = new TipoActuacionEstadoVM
                                 {
                                     idTipoActuacionEstado = tae.idTipoActuacionEstado,
                                     nombreTipoActuacionEstado = tae.nombreTipoActuacionEstado,
                                     activo = tae.activo
                                 },
                                 tipoActuacion = new TipoActuacionVM
                                 {
                                     idTipoActuacion = ta.idTipoActuacion,
                                     nombreTipoActuacion = ta.nombreTipoActuacion,
                                     activo = ta.activo
                                 },
                                 equipo = new EquipoVM
                                 {
                                     idEquipo = e.idEquipo,
                                     nombreEquipo = e.nombreEquipo,
                                     activo = e.activo,
                                 }
                             }).FirstOrDefault();
            return actuacion;
        }

        //Consulta solo 1 Actuacion sin relacionar
        public ActuacionVM ConsultarUnaActuaciones(int idActuacion)
        {
            var actuacion = (from a in db.NovActuacionEstado
                             join ta in db.NovTipoActuacionEstado on a.idTipoActuacionEstado equals ta.idTipoActuacionEstado
                             where a.idActuacionEstado == idActuacion && a.activo == true
                             select new ActuacionVM
                             {
                                 idActuacionEstado = a.idActuacionEstado,
                                 nombreActuacion = a.nombreActuacionEstado,
                                 idTipoActuacionEstado = a.idTipoActuacionEstado,
                                 nombreTipoActuacion = ta.nombreTipoActuacionEstado,
                             }).FirstOrDefault();
            return actuacion;
        }



        //Alta de 1 sola actuacion , NO ES LA RELACION 
        public int AltaActuacion(ActuacionVM actuacion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var ActuacionRepetido = db.NovActuacionEstado.Where(x => x.nombreActuacionEstado == actuacion.nombreTipoActuacion).FirstOrDefault();
                var consultaActuacion = db.NovActuacionEstado.Where(x => x.idActuacionEstado == actuacion.idActuacionEstado).FirstOrDefault();
                if (consultaActuacion == null && ActuacionRepetido == null)
                {
                    try
                    {
                        NovActuacionEstado a = new NovActuacionEstado
                        {
                            nombreActuacionEstado = actuacion.nombreTipoActuacion,
                            idTipoActuacionEstado = actuacion.idTipoActuacionEstado,
                            activo = true,
                        };
                        db.NovActuacionEstado.Add(a);
                        db.SaveChanges();
                        /*
                        NovActuacTipoEquipo te = new NovActuacTipoEquipo
                        {
                            idActuacion = a.idActuacionEstado,
                            idTipoActuacion = actuacion.tipoActuacion.idTipoActuacion,
                            idEquipo = actuacion.equipo.idEquipo,
                            activo = true,
                        };
                        db.NovActuacTipoEquipo.Add(te);
                        db.SaveChanges();*/
                        return 200;

                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }

            }

        }

        //Modifica la relacion
        public int ModificaraActuacion(ActuacionVM actuacion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaActuacion = db.NovActuacionEstado.Where(x => x.idActuacionEstado == actuacion.idTipoActuacion).FirstOrDefault();
                var consultaTipoActuacion = db.NovActuacTipoEquipo.Where(x => x.idActuacion == actuacion.idTipoActuacion).FirstOrDefault();
                if (consultaActuacion != null)
                {
                    consultaActuacion.nombreActuacionEstado = actuacion.nombreTipoActuacion;
                    consultaActuacion.idTipoActuacionEstado = actuacion.idTipoActuacionEstado;

                    //Tabla donde estan todos los datos
                    consultaTipoActuacion.idActuacion = actuacion.idTipoActuacion;
                    consultaTipoActuacion.idTipoActuacion = actuacion.tipoActuacion.idTipoActuacion;
                    consultaTipoActuacion.idEquipo = actuacion.equipo.idEquipo;
                    db.SaveChanges();
                }
            }
            return 200;
        }
        //Modifica solo 1 actuacion
        public int ModificaraActuaciones(ActuacionVM actuacion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaActuacion = db.NovActuacionEstado.Where(x => x.idActuacionEstado == actuacion.idTipoActuacion).FirstOrDefault();
                if (consultaActuacion != null)
                {
                    consultaActuacion.nombreActuacionEstado = actuacion.nombreTipoActuacion;
                    consultaActuacion.idTipoActuacionEstado = actuacion.idTipoActuacionEstado;

                    db.SaveChanges();
                }
            }
            return 200;
        }
        //Elimina solo 1 actuacion
        public int EliminarActuacion(ActuacionVM actuacion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaActuacion = db.NovActuacionEstado.Where(x => x.idActuacionEstado == actuacion.idActuacion).FirstOrDefault();
                if (consultaActuacion != null)
                {
                    consultaActuacion.activo = false;
                }
                db.SaveChanges();
            }
            return 200;
        }

        //Elimina la relacion
        public int EliminarActuacionRelacion(ActuacionVM actuacion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaActuacionRelacion = db.NovActuacTipoEquipo.Where(x => x.idActuacTipoFallaEquipo == actuacion.idActuacTipoFallaEquipo).FirstOrDefault();
                if (consultaActuacionRelacion != null)
                {
                    consultaActuacionRelacion.activo = false;
                }
                db.SaveChanges();
            }
            return 200;
        }


        //Recupera solo 1 actuacion
        public int RecuperarActuacion(ActuacionVM actuacion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())

            {
                var consultaActuacion = db.NovActuacionEstado.Where(x => x.idActuacionEstado == actuacion.idActuacion).FirstOrDefault();
                if (consultaActuacion != null)
                {
                    consultaActuacion.activo = true;
                    db.SaveChanges();
                    return 200;
                }
                else
                {
                    return 400;
                }

            }

        }
        //Recupera la relacion
        public int RecuperarActuacionRelacion(ActuacionVM actuacion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())

            {
                var consultaActuacionRelacionada = db.NovActuacTipoEquipo.Where(x => x.idActuacTipoFallaEquipo == actuacion.idActuacTipoFallaEquipo).FirstOrDefault();
                if (consultaActuacionRelacionada != null)
                {
                    consultaActuacionRelacionada.activo = true;
                    db.SaveChanges();
                    return 200;
                }
                else
                {
                    return 400;
                }

            }

        }




        public bool ValidarActuacion(ActuacionVM actuacion)
        {
            if (!string.IsNullOrWhiteSpace(actuacion.nombreTipoActuacion.ToString()))
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        public object ObtenerListaActuacionTipoEstado(bool activo)
        {
            List<TipoActuacionEstadoVM> listaTipoEstado;

            listaTipoEstado = (from e in db.NovTipoActuacionEstado
                               where e.activo == activo
                               select new TipoActuacionEstadoVM
                               {
                                   idTipoActuacionEstado = e.idTipoActuacionEstado,
                                   nombreTipoActuacionEstado = e.nombreTipoActuacionEstado,
                                   activo = e.activo,
                               }).ToList();
            listaTipoEstado = listaTipoEstado.OrderBy(x => x.idTipoActuacionEstado).ToList();
            object json = new { data = listaTipoEstado };
            return json;

        }


        public object ObtenerListaEquipo(bool activo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                List<EquipoVM> listaEquipo;

                listaEquipo = (from c in db.EquiEquipo
                               where c.activo == activo
                               select new EquipoVM
                               {
                                   idEquipo = c.idEquipo,
                                   nombreEquipo = c.nombreEquipo,
                                   idUsarioAlta = c.idUsuarioAlta,
                                   fechaUsarioAlta = c.fechaUsuarioAlta,
                                   codigoEquipo = c.codigoEquipo,
                                   activo = c.activo,
                               }).ToList();

                object json = new { data = listaEquipo };
                return json;
            }

        }

        public int AltaActuacionTipoYEquipo(ActuacionVM actuacion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaActuacion = db.NovActuacionEstado.Where(x => x.idActuacionEstado == actuacion.idActuacionEstado).FirstOrDefault();

                if (consultaActuacion != null)
                {
                    try
                    {
                        foreach (var tipo in actuacion.listaTipoActuacion)
                        {
                            var idTipo = Convert.ToInt32(tipo);

                            NovActuacTipoEquipo te = new NovActuacTipoEquipo
                            {
                                idActuacion = actuacion.idActuacionEstado,
                                idTipoActuacion = idTipo,
                                idEquipo = actuacion.equipo.idEquipo,
                                activo = true,
                            };
                            db.NovActuacTipoEquipo.Add(te);
                        }

                        db.SaveChanges();
                        return 200;

                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }

            }

        }


        ///  ↓↓↓↓↓↓  TIPO ACTUACION  ↓↓↓↓↓↓↓↓ ///

        public object ObtenerTipoActuacion(bool activo)
        {
            List<ActuacionVM> listaTipo;
            listaTipo = (from a in db.NovTipoActuacion
                         where a.activo == activo
                         select new ActuacionVM
                         {
                             activo = a.activo,
                             idTipoActuacion = a.idTipoActuacion,
                             nombreTipoActuacion = a.nombreTipoActuacion

                         }).ToList();
            object json = new { data = listaTipo };
            return json;
        }


        public int AltaActuacionTipo(TipoActuacionVM Tipoactuacion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var tipoRepetido = db.NovTipoActuacion.Where(x => x.nombreTipoActuacion == Tipoactuacion.nombreTipoActuacion).FirstOrDefault();
                var consultaTipoActuacion = db.NovTipoActuacion.Where(x => x.idTipoActuacion == Tipoactuacion.idTipoActuacion).FirstOrDefault();

                if (consultaTipoActuacion == null && tipoRepetido == null)
                {
                    try
                    {
                        NovTipoActuacion ta = new NovTipoActuacion
                        {
                            nombreTipoActuacion = Tipoactuacion.nombreTipoActuacion,
                            activo = true,
                        };
                        db.NovTipoActuacion.Add(ta);
                        db.SaveChanges();
                        return 200;

                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }

            }
        }

        public TipoActuacionVM ConsultarUnTipoActuacion(int? idTipoActuacion)
        {
            var Tipoactuacion = (from a in db.NovTipoActuacion
                                 where a.idTipoActuacion == idTipoActuacion
                                 select new TipoActuacionVM
                                 {
                                     activo = a.activo,
                                     idTipoActuacion = a.idTipoActuacion,
                                     nombreTipoActuacion = a.nombreTipoActuacion,

                                 }).FirstOrDefault();
            return Tipoactuacion;
        }

        public int ModificarTipoActuacion(TipoActuacionVM Tipoactuacion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaTipoActuacion = db.NovTipoActuacion.Where(x => x.idTipoActuacion == Tipoactuacion.idTipoActuacion).FirstOrDefault();
                if (consultaTipoActuacion != null)
                {
                    consultaTipoActuacion.nombreTipoActuacion = Tipoactuacion.nombreTipoActuacion;
                    consultaTipoActuacion.idTipoActuacion = Tipoactuacion.idTipoActuacion;

                    db.SaveChanges();
                }
            }
            return 200;
        }

        public int EliminarTipoActuacion(TipoActuacionVM Tipoactuacion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaTipoActuacion = db.NovTipoActuacion.Where(x => x.idTipoActuacion == Tipoactuacion.idTipoActuacion).FirstOrDefault();
                if (consultaTipoActuacion != null)
                {
                    consultaTipoActuacion.activo = false;
                }
                db.SaveChanges();
            }
            return 200;
        }

        public int RecuperarTipoActuacion(TipoActuacionVM Tipoactuacion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())

            {
                var consultaTipoActuacion = db.NovTipoActuacion.Where(x => x.idTipoActuacion == Tipoactuacion.idTipoActuacion).FirstOrDefault();
                if (consultaTipoActuacion != null)
                {
                    consultaTipoActuacion.activo = true;
                    db.SaveChanges();
                    return 200;
                }
                else
                {
                    return 400;
                }

            }

        }

        public bool ValidarTipoActuacion(TipoActuacionVM Tipoactuacion)
        {
            if (!string.IsNullOrWhiteSpace(Tipoactuacion.nombreTipoActuacion.ToString()))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
    }
}

