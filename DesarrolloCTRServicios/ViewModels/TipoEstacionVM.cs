﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class TipoEstacionVM
    {
        public int idTipoEstacion { get; set; }
        public string nombreTipoEstacion { get; set; }
        public string silgasTipo { get; set; }
        public bool? activo { get; set; }
    }
}
