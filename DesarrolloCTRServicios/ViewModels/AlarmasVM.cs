﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class AlarmasVM
    {
        public int? idAlarma { get; set; }
        public string nombreAlarma { get; set; }
        public bool? activo { get; set; }
    }
}
