﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System;
using System.IO;
using System.Net.Http;


namespace DesarrolloCTRUtilidades
{
    public class GenerarExcel
    {
        //INDISPONIBILIDADES UCIEL
        public HojaExcelVM GenerarExcelGeneral(HojaExcelVM datos)
        {
            // Creating an instance 
            // of ExcelPackage 
            // If you use EPPlus in a noncommercial context
            // according to the Polyform Noncommercial license:
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage excel = new ExcelPackage();

            // name of the sheet 
            var workSheet = excel.Workbook.Worksheets.Add("hoja1");

            // setting the properties 
            // of the work sheet  
            workSheet.TabColor = System.Drawing.Color.Black;
            workSheet.DefaultRowHeight = 12;

            // Setting the properties 
            // of the first row 
            workSheet.Row(1).Height = 20;
            workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Row(1).Style.Font.Bold = true;

            // Header of the Excel sheet 
            var contColumnas = 1;
            foreach (var item in datos.encabezado)
            {
                workSheet.Cells[1, contColumnas].Value = item.ToString();

                contColumnas++;
            }

            //workSheet.Cells[1, 1].Value = "S.No";
            //workSheet.Cells[1, 2].Value = "Id";
            //workSheet.Cells[1, 3].Value = "Name";

            // Inserting the article data into excel 
            // sheet by using the for each loop 
            // As we have values to the first row  
            // we will start with second row 
            int recordIndex = 2;
            var cantFilas = 1;
            foreach (var fila in datos.listaFilas)
            {
                var countFila = 1;
                foreach (var detFil in fila.datoPorFila)
                {
                    workSheet.Cells[recordIndex, countFila].Value = detFil;
                    workSheet.Column(countFila).AutoFit();
                    countFila++;
                }
                //workSheet.Cells[recordIndex, 1].Value = fila.datoPorFila;
                //workSheet.Cells[recordIndex, 2].Value = "hola";
                //workSheet.Cells[recordIndex, 3].Value = "soy yo";
                recordIndex++;
            }

            // By default, the column width is not  
            // set to auto fit for the content 
            // of the range, so we are using 
            // AutoFit() method here.  
            //workSheet.Column(1).AutoFit();
            //workSheet.Column(2).AutoFit();
            //workSheet.Column(3).AutoFit();

            // file name with .xlsx extension  
            //string p_strPath = "D:\\geeksforgeeks.xlsx";
            if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/TempFolder/")))
                Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/TempFolder/"));
            var pathInic = "~/TempFolder/";


            var nombreArchivo = "Indisponibilidades_" + Convert.ToDateTime(DateTime.Today).ToString("dd-MM-yyyy") + ".xlsx";
            var fechaParseada = Convert.ToDateTime(DateTime.Today).ToString("dd-MM-yyyy");

            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(pathInic), nombreArchivo);

            string p_strPath = "D:\\Indisponibilidades_" + fechaParseada + ".xlsx";
            //string p_strPath = pathInic + nombreArchivo;
            if (File.Exists(path))
                File.Delete(path);

            // Create excel file on physical disk  
            FileStream objFileStrm = File.Create(path);
            objFileStrm.Close();

            // Write content to excel file  
            File.WriteAllBytes(path, excel.GetAsByteArray());
            //Close Excel package 
            excel.Dispose();

            //Console.ReadKey();
            datos.fileName = nombreArchivo;
            datos.filePath = path;


            return datos;
        }

        //INDISPONIBILIDADES NICO EQUIPOS SEPARADOS
        public HojaExcelVM GenerarExcelGeneralRemunerado(HojaExcelVM datos, String equipo)
        {

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage excel = new ExcelPackage();

            var workSheet = excel.Workbook.Worksheets.Add(equipo);

            workSheet.TabColor = System.Drawing.Color.Black;
            workSheet.DefaultRowHeight = 12;

            workSheet.Row(1).Height = 20;
            workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Row(1).Style.Font.Bold = true;

            //var contColumnasT = 4;
            workSheet.Cells[1, 4].Value = "SISTEMA DE TRANSPORTE DE ENERGÍA ELECTRICA POR DISTRIBUCIÓN TRONCAL - DISTROCUYO S.A. - " + equipo;

            int recordIndex = 3; //Contador de Filas

            //Tarifas actuales
            workSheet.Row(2).Height = 20;
            workSheet.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Cells[2,1].Style.Border.BorderAround(ExcelBorderStyle.Thin,System.Drawing.Color.Black);
            workSheet.Row(2).Style.Font.Bold = true;
            workSheet.Cells[2, 1].Value = "Tarifas Actuales";

            //Filas y columnas de los montos
            foreach (var fila in datos.listaFilas)
            {
                var countColumn = 1;
                if (fila.datoPorFilaMonto != null)
                {
                    foreach (var detFil in fila.datoPorFilaMonto)
                    {
                        workSheet.Cells[recordIndex, countColumn].Value = detFil;
                        workSheet.Column(countColumn).AutoFit();
                        workSheet.Cells[recordIndex, countColumn].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                        countColumn++;
                    }
                    recordIndex++;

                }

            }
            recordIndex++;

            workSheet.Row(recordIndex).Style.Font.Bold = true;
            //Filas y Columnas de indisponibilidades ENCABEZADO DE LOS DATOS 
            var contColumnas = 1;//Encabezados de columnas
            workSheet.Row(recordIndex).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            var columnaTotalPenalizacion = 0; var columnaNoPercibido = 0; var columnaLucroCesante = 0;
            foreach (var item in datos.encabezado)
            {
                workSheet.Cells[recordIndex, contColumnas].Value = item.ToString();

                if (item.ToString() != "")
                {
                    workSheet.Cells[recordIndex, contColumnas].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);

                    if(item.ToString() == "Total Penalización")
                    {
                        columnaTotalPenalizacion = contColumnas;
                    }else if (item.ToString() == "No Percibido")
                    {
                        columnaNoPercibido = contColumnas;
                    }else if (item.ToString() == "Lucro Cesante")
                    {
                        columnaLucroCesante = contColumnas;
                    }

                }
                contColumnas++;
            }
            recordIndex++;
            //Datos de las indisponibildades
            foreach (var fila in datos.listaFilas)
            {
                var countFila = 1;
                if (fila.datoPorFila != null)
                {
                    foreach (var detFil in fila.datoPorFila)
                    {
                        workSheet.Cells[recordIndex, countFila].Value = detFil;
                        workSheet.Column(countFila).AutoFit();
                        if(detFil != " ")
                        {
                            workSheet.Cells[recordIndex, countFila].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                        }

                        countFila++;

                    }
                    recordIndex++;
                }
            }
                //Total Penalizacion
                workSheet.Cells[recordIndex, columnaTotalPenalizacion].Value = datos.totales.datoPorFilaTotales[0];
                workSheet.Cells[recordIndex, columnaTotalPenalizacion].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                workSheet.Cells[recordIndex, columnaTotalPenalizacion].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);
                //No Percibido
                workSheet.Cells[recordIndex, columnaNoPercibido].Value = datos.totales.datoPorFilaTotales[1];
                workSheet.Cells[recordIndex, columnaNoPercibido].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                workSheet.Cells[recordIndex, columnaNoPercibido].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);
            //Lucro Cesante
                workSheet.Cells[recordIndex, columnaLucroCesante].Value = datos.totales.datoPorFilaTotales[2];
                workSheet.Cells[recordIndex, columnaLucroCesante].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                workSheet.Cells[recordIndex, columnaLucroCesante].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);




            // file name with .xlsx extension  
            //string p_strPath = "D:\\geeksforgeeks.xlsx";
            if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/TempFolder/")))
                Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/TempFolder/"));
            var pathInic = "~/TempFolder/";


            var nombreArchivo = "IndisRemunerada_"+ equipo+ "_" + Convert.ToDateTime(DateTime.Today).ToString("dd-MM-yyyy") + ".xlsx";
            var fechaParseada = Convert.ToDateTime(DateTime.Today).ToString("dd-MM-yyyy");

            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(pathInic), nombreArchivo);

            string p_strPath = "D:\\Indisponibilidades_" + fechaParseada + ".xlsx";
            //string p_strPath = pathInic + nombreArchivo;
            if (File.Exists(path))
                File.Delete(path);

            // Create excel file on physical disk  
            FileStream objFileStrm = File.Create(path);
            objFileStrm.Close();

            // Write content to excel file  
            File.WriteAllBytes(path, excel.GetAsByteArray());
            //Close Excel package 
            excel.Dispose();

            //Console.ReadKey();
            datos.fileName = nombreArchivo;
            datos.filePath = path;


            return datos;
        }

        //MESES SOLAMENTE
        public HojaExcelVM GenerarExcelMesesRemunerados(HojaExcelVM datos, string mes , int ano)
        {

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage excel = new ExcelPackage();

            var workSheet = excel.Workbook.Worksheets.Add("Mes");

            workSheet.TabColor = System.Drawing.Color.Black;
            workSheet.DefaultRowHeight = 12;

            workSheet.Row(1).Height = 20;
            workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Row(1).Style.Font.Bold = true;

            workSheet.Cells[1, 1].Value = "CIERRE MENSUAL:  " + mes.ToUpper() + "  " + ano ; 


            //INGRESOS DISTROCUYO S.A.
            workSheet.Row(3).Height = 20;
            workSheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheet.Row(3).Style.Font.Bold = true;
            workSheet.Cells[3, 1].Value = "INGRESOS DISTROCUYO S.A.";


            int recordIndex = 5; //Contador de Filas
            workSheet.Row(recordIndex).Style.Font.Bold = true;
            //Filas y Columnas de Mes ENCABEZADO DE LOS DATOS 
            var contColumnas = 1;//Encabezados de columnas
            workSheet.Row(recordIndex).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            foreach (var item in datos.encabezado)
            {
                if (item.ToString() != "CALCULO DISTRO")
                {
                workSheet.Cells[recordIndex, contColumnas].Value = item.ToString();
                workSheet.Cells[recordIndex, contColumnas].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                workSheet.Column(recordIndex).AutoFit();
                contColumnas++;

                }
                else
                { //Esto es donde dice "Calculo Distro"
                    workSheet.Cells[recordIndex -1, contColumnas , recordIndex -1, contColumnas+1].Merge = true;
                    workSheet.Row(recordIndex -1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheet.Cells[recordIndex - 1, contColumnas].Value = item.ToString();
                    workSheet.Cells[recordIndex -1, contColumnas , recordIndex - 1, contColumnas + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                    workSheet.Row(recordIndex -1).Style.Font.Bold = true;

                    workSheet.Column(recordIndex).AutoFit();
                }
            }
            recordIndex++;
            //Datos
            foreach (var fila in datos.listaFilas)
            {
                var countFila = 1;
                if (fila.datoPorFila != null)
                {
                    foreach (var detFil in fila.datoPorFila)
                    {
                        workSheet.Cells[recordIndex, countFila].Value = detFil;
                        workSheet.Cells[recordIndex, countFila].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                        workSheet.Column(countFila).AutoFit();
                        countFila++;
                    }
                    recordIndex++;
                }
            }

            // file name with .xlsx extension  
            //string p_strPath = "D:\\geeksforgeeks.xlsx";
            if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/TempFolder/")))
                Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/TempFolder/"));
            var pathInic = "~/TempFolder/";


            var nombreArchivo = "IndisRemunerada_" + "Mes" + "_" + Convert.ToDateTime(DateTime.Today).ToString("dd-MM-yyyy") + ".xlsx";
            var fechaParseada = Convert.ToDateTime(DateTime.Today).ToString("dd-MM-yyyy");

            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(pathInic), nombreArchivo);

            string p_strPath = "D:\\Indisponibilidades_" + fechaParseada + ".xlsx";
            //string p_strPath = pathInic + nombreArchivo;
            if (File.Exists(path))
                File.Delete(path);

            // Create excel file on physical disk  
            FileStream objFileStrm = File.Create(path);
            objFileStrm.Close();

            // Write content to excel file  
            File.WriteAllBytes(path, excel.GetAsByteArray());
            //Close Excel package 
            excel.Dispose();

            //Console.ReadKey();
            datos.fileName = nombreArchivo;
            datos.filePath = path;


            return datos;
        }

        public HojaExcelVM GenerarExcelEquiposRemunerados(HojaExcelVM datos, int idEquipo)
        {

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage excel = new ExcelPackage();
            var contColumnas = 2;//Encabezados de columnas
            var workSheet = excel.Workbook.Worksheets.Add("Equipos");
            int recordIndex = 3; //Contador de Filas
            #region Linea
            if (idEquipo == 4)
            {
                

                workSheet.TabColor = System.Drawing.Color.Yellow;
                workSheet.DefaultRowHeight = 12;

                workSheet.Row(1).Height = 20;
                workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Row(1).Style.Font.Bold = true;

                //var contColumnasT = 4;
                workSheet.Cells[1, 1].Value = "1.-LÍNEA";

                

                //Tarifas actuales
                workSheet.Row(2).Height = 20;
                workSheet.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[2, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                workSheet.Row(2).Style.Font.Bold = true;
                workSheet.Cells[2, 2].Value = "Tarifas Actuales";

                //Filas y columnas de los montos
                foreach (var fila in datos.listaFilasLineaRemu)
                {
                    var countColumn = 2;
                    if (fila.datoPorFilaMontoLineaRemu != null)
                    {
                        foreach (var detFil in fila.datoPorFilaMontoLineaRemu)
                        {
                            workSheet.Cells[recordIndex, countColumn].Value = detFil;
                            workSheet.Column(countColumn).AutoFit();
                            workSheet.Cells[recordIndex, countColumn].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                            countColumn++;
                        }
                        recordIndex++;

                    }

                }
                recordIndex++;

                //Filas y Columnas de indisponibilidades ENCABEZADO DE LOS DATOS 

                var columnaTotalCargoLinea = 0;
                var columnaTotalKmLinea = 0;
                foreach (var item in datos.encabezadoLineaRemu)
                {
                    workSheet.Cells[recordIndex, contColumnas].Value = item.ToString();
                    workSheet.Cells[recordIndex, contColumnas].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);

                    if (item.ToString() == "CARGO REAL ($/hs)")
                    {
                        columnaTotalCargoLinea = contColumnas;
                    }
                    if (item.ToString() == "Long [km]")
                    {
                        columnaTotalKmLinea = contColumnas;
                    }

                    contColumnas++;
                }
                recordIndex++;
                //Datos body
                foreach (var fila in datos.listaFilasLineaRemu)
                {
                    var countFila = 2;
                    if (fila.datoPorFilaLineaRemu != null)
                    {
                        foreach (var detFil in fila.datoPorFilaLineaRemu)
                        {
                            workSheet.Cells[recordIndex, countFila].Value = detFil;
                            workSheet.Cells[recordIndex, countFila].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                            workSheet.Column(countFila).AutoFit();
                            countFila++;

                        }
                        recordIndex++;
                    }
                }
                //Total Cargo
                workSheet.Cells[recordIndex, columnaTotalCargoLinea].Value = datos.totalesLineaRemu.datoPorFilaTotalesLineaRemu[0];
                workSheet.Cells[recordIndex, columnaTotalCargoLinea].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                workSheet.Cells[recordIndex, columnaTotalCargoLinea].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);

                workSheet.Cells[recordIndex, columnaTotalKmLinea].Value = datos.totalesLineaRemu.datoPorFilaTotalesLineaRemu[1];
                workSheet.Cells[recordIndex, columnaTotalKmLinea].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                workSheet.Cells[recordIndex, columnaTotalKmLinea].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);

            }

            #endregion

            #region Transformador
            if(idEquipo == 1)
            {
                contColumnas += 2; //Aumento 2 columnas a la derecha 
                var inicioTrafo = contColumnas;

                workSheet.Cells[1, contColumnas - 1].Value = "1.-TRANSFORMADORES";

                recordIndex = 3; //Vuelvo a la fila 3

                //Tarifas actuales
                workSheet.Row(2).Height = 20;
                workSheet.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[2, contColumnas].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                workSheet.Row(2).Style.Font.Bold = true;
                workSheet.Cells[2, contColumnas].Value = "Tarifas Actuales";

                //Filas y columnas de los montos
                foreach (var fila in datos.listaFilasTrafosRemu)
                {
                    var countColumn = contColumnas;
                    if (fila.datoPorFilaMontoTrafoRemu != null)
                    {
                        foreach (var detFil in fila.datoPorFilaMontoTrafoRemu)
                        {
                            workSheet.Cells[recordIndex, countColumn].Value = detFil;
                            workSheet.Column(countColumn).AutoFit();
                            workSheet.Cells[recordIndex, countColumn].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                            countColumn++;
                        }
                        recordIndex++;

                    }

                }
                recordIndex++;

                //Filas y Columnas de indisponibilidades ENCABEZADO DE LOS DATOS 
                var columnaTotalCargoTrafo = 0;
                var columnaTotalPOT = 0;
                foreach (var item in datos.encabezadoTrafosRemu)
                {
                    workSheet.Cells[recordIndex, contColumnas].Value = item.ToString();
                    workSheet.Cells[recordIndex, contColumnas].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);

                    if (item.ToString() == "$/H")
                    {
                        columnaTotalCargoTrafo = contColumnas;
                    }
                    if (item.ToString() == "POT.[MVA]")
                    {
                        columnaTotalPOT = contColumnas;
                    }

                    contColumnas++;
                }
                recordIndex++;
                //Datos de las indisponibildades
                foreach (var fila in datos.listaFilasTrafosRemu)
                {
                    var countFila = inicioTrafo;
                    if (fila.datoPorFilaTrafoRemu != null)
                    {
                        foreach (var detFil in fila.datoPorFilaTrafoRemu)
                        {
                            workSheet.Cells[recordIndex, countFila].Value = detFil;
                            workSheet.Cells[recordIndex, countFila].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                            workSheet.Column(countFila).AutoFit();
                            countFila++;

                        }
                        recordIndex++;
                    }
                }
                //Total Cargo
                workSheet.Cells[recordIndex, columnaTotalCargoTrafo].Value = datos.totalesTrafosRemu.datoPorFilaTotalesTrafoRemu[0];
                workSheet.Cells[recordIndex, columnaTotalCargoTrafo].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                workSheet.Cells[recordIndex, columnaTotalCargoTrafo].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);

                workSheet.Cells[recordIndex, columnaTotalPOT].Value = datos.totalesTrafosRemu.datoPorFilaTotalesTrafoRemu[1];
                workSheet.Cells[recordIndex, columnaTotalPOT].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                workSheet.Cells[recordIndex, columnaTotalPOT].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);


            }

            #endregion

            #region PtosC
            if(idEquipo == 2)
            {
                contColumnas += 2; //Aumento 2 columnas a la derecha 
                var inicioPtosC = contColumnas;

                workSheet.Cells[1, contColumnas - 1].Value = "1.-CAMPOS";


                //Tarifas actuales
                workSheet.Row(2).Height = 20;
                workSheet.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[2, contColumnas].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                workSheet.Row(2).Style.Font.Bold = true;
                workSheet.Cells[2, contColumnas].Value = "Tarifas Actuales";

                recordIndex = 3; //Vuelvo a la fila 3
                                 //Filas y columnas de los montos
                foreach (var fila in datos.listaFilasPtosCRemu)
                {
                    var countColumn = contColumnas;
                    if (fila.datoPorFilaMontoPtoCRemu != null)
                    {
                        foreach (var detFil in fila.datoPorFilaMontoPtoCRemu)
                        {
                            workSheet.Cells[recordIndex, countColumn].Value = detFil;
                            workSheet.Column(countColumn).AutoFit();
                            workSheet.Cells[recordIndex, countColumn].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                            countColumn++;
                        }
                        recordIndex++;

                    }

                }
                recordIndex++;

                //Filas y Columnas de indisponibilidades ENCABEZADO DE LOS DATOS 
                var columnaTotalCargoPtosC = 0;
                var columnaTotalEquipos = 0;
                var columnaTotalMes = 0;
                foreach (var item in datos.encabezadoPtosCRemu)
                {
                    workSheet.Cells[recordIndex, contColumnas].Value = item.ToString();
                    workSheet.Cells[recordIndex, contColumnas].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);

                    if (item.ToString() == "CARGO REAL ($/hs)")
                    {
                        columnaTotalCargoPtosC = contColumnas;
                    }
                    if (item.ToString() == "")
                    {
                        columnaTotalEquipos = contColumnas;
                    }
                    if (item.ToString() == "$/mes")
                    {
                        columnaTotalMes = contColumnas;
                    }

                    contColumnas++;
                }
                recordIndex++;
                //Datos de las indisponibildades
                foreach (var fila in datos.listaFilasPtosCRemu)
                {
                    var countFila = inicioPtosC;
                    if (fila.datoPorFilaPtoCRemu != null)
                    {
                        foreach (var detFil in fila.datoPorFilaPtoCRemu)
                        {
                            workSheet.Cells[recordIndex, countFila].Value = detFil;
                            workSheet.Cells[recordIndex, countFila].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                            workSheet.Column(countFila).AutoFit();
                            countFila++;

                        }
                        recordIndex++;
                    }
                }
                //Total Cargo
                workSheet.Cells[recordIndex, columnaTotalCargoPtosC].Value = datos.totalesPtosCRemu.datoPorFilaTotalesPtoCRemu[0];
                workSheet.Cells[recordIndex, columnaTotalCargoPtosC].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                workSheet.Cells[recordIndex, columnaTotalCargoPtosC].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);

                workSheet.Cells[recordIndex, columnaTotalEquipos].Value = datos.totalesPtosCRemu.datoPorFilaTotalesPtoCRemu[1];
                workSheet.Cells[recordIndex, columnaTotalEquipos].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                workSheet.Cells[recordIndex, columnaTotalEquipos].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);

                workSheet.Cells[recordIndex, columnaTotalMes].Value = datos.totalesPtosCRemu.datoPorFilaTotalesPtoCRemu[2];
                workSheet.Cells[recordIndex, columnaTotalMes].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                workSheet.Cells[recordIndex, columnaTotalMes].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);


            }


            #endregion

            #region Capacitor
            if(idEquipo == 7)
            {
                contColumnas += 2; //Aumento 2 columnas a la derecha 
                var inicioCapacitor = contColumnas;

                workSheet.Cells[1, contColumnas - 1].Value = "1.-REACTIVO";

                //Tarifas actuales
                workSheet.Row(2).Height = 20;
                workSheet.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                workSheet.Cells[2, contColumnas].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                workSheet.Row(2).Style.Font.Bold = true;
                workSheet.Cells[2, contColumnas].Value = "Tarifas Actuales";

                recordIndex = 3; //Vuelvo a la fila 3
                                 //Filas y columnas de los montos
                foreach (var fila in datos.listaFilasCapCRemu)
                {
                    var countColumn = contColumnas;
                    if (fila.datoPorFilaMontoCapRemu != null)
                    {
                        foreach (var detFil in fila.datoPorFilaMontoCapRemu)
                        {
                            workSheet.Cells[recordIndex, countColumn].Value = detFil;
                            workSheet.Column(countColumn).AutoFit();
                            workSheet.Cells[recordIndex, countColumn].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                            countColumn++;
                        }
                        recordIndex++;

                    }

                }
                recordIndex++;

                //Filas y Columnas de indisponibilidades ENCABEZADO DE LOS DATOS 
                var columnaTotalCargoCap = 0;
                foreach (var item in datos.encabezadoCapRemu)
                {
                    workSheet.Cells[recordIndex, contColumnas].Value = item.ToString();
                    workSheet.Cells[recordIndex, contColumnas].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);

                    if (item.ToString() == "CARGO REAL ($/hs)")
                    {
                        columnaTotalCargoCap = contColumnas;
                    }

                    contColumnas++;
                }
                recordIndex++;
                //Datos de las indisponibildades
                foreach (var fila in datos.listaFilasCapCRemu)
                {
                    var countFila = inicioCapacitor;
                    if (fila.datoPorFilaCapRemu != null)
                    {
                        foreach (var detFil in fila.datoPorFilaCapRemu)
                        {
                            workSheet.Cells[recordIndex, countFila].Value = detFil;
                            workSheet.Cells[recordIndex, countFila].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                            workSheet.Column(countFila).AutoFit();
                            countFila++;

                        }
                        recordIndex++;
                    }
                }
                //Total Cargo
                workSheet.Cells[recordIndex, columnaTotalCargoCap].Value = datos.totalesCapRemu.datoPorFilaTotalesCapRemu[0];
                workSheet.Cells[recordIndex, columnaTotalCargoCap].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                workSheet.Cells[recordIndex, columnaTotalCargoCap].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);


            }


            #endregion

            // file name with .xlsx extension  
            //string p_strPath = "D:\\geeksforgeeks.xlsx";
            if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/TempFolder/")))
                Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/TempFolder/"));
            var pathInic = "~/TempFolder/";


            var nombreArchivo = "IndisRemunerada_" +"Equipos "+ Convert.ToDateTime(DateTime.Today).ToString("dd-MM-yyyy") + ".xlsx";
            var fechaParseada = Convert.ToDateTime(DateTime.Today).ToString("dd-MM-yyyy");

            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(pathInic), nombreArchivo);

            string p_strPath = "D:\\Indisponibilidades_" + fechaParseada + ".xlsx";
            //string p_strPath = pathInic + nombreArchivo;
            if (File.Exists(path))
                File.Delete(path);

            // Create excel file on physical disk  
            FileStream objFileStrm = File.Create(path);
            objFileStrm.Close();

            // Write content to excel file  
            File.WriteAllBytes(path, excel.GetAsByteArray());
            //Close Excel package 
            excel.Dispose();

            //Console.ReadKey();
            datos.fileName = nombreArchivo;
            datos.filePath = path;


            return datos;
        }



        //TODO INDISPONIBILIDADES NICO (EQUIPOS INDISPONIBLES Y MESES)
        public HojaExcelVM GenerarExcelCompleto(HojaExcelVM datos, DateTime fecha)
        {

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            ExcelPackage excel = new ExcelPackage();

            #region Linea

            var workSheetEquipos = excel.Workbook.Worksheets.Add("Equipos");

            workSheetEquipos.TabColor = System.Drawing.Color.Yellow;
            workSheetEquipos.DefaultRowHeight = 12;

            workSheetEquipos.Row(1).Height = 20;
            workSheetEquipos.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheetEquipos.Row(1).Style.Font.Bold = true;

            //var contColumnasT = 4;
            workSheetEquipos.Cells[1, 1].Value = "1.-LÍNEA";

            int recordIndexEquipo = 3; //Contador de Filas

            //Tarifas actuales
            workSheetEquipos.Row(2).Height = 20;
            workSheetEquipos.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheetEquipos.Cells[2, 2].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetEquipos.Row(2).Style.Font.Bold = true;
            workSheetEquipos.Cells[2, 2].Value = "Tarifas Actuales";

            //Filas y columnas de los montos
            foreach (var fila in datos.listaFilasLineaRemu)
            {
                var countColumnEquipo = 2;
                if (fila.datoPorFilaMontoLineaRemu != null)
                {
                    foreach (var detFil in fila.datoPorFilaMontoLineaRemu)
                    {
                        workSheetEquipos.Cells[recordIndexEquipo, countColumnEquipo].Value = detFil;
                        workSheetEquipos.Column(countColumnEquipo).AutoFit();
                        workSheetEquipos.Cells[recordIndexEquipo, countColumnEquipo].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                        countColumnEquipo++;
                    }
                    recordIndexEquipo++;

                }

            }
            recordIndexEquipo++;

            //Filas y Columnas de indisponibilidades ENCABEZADO DE LOS DATOS 
            var contColumnasEquipo = 2;//Encabezados de columnas
            var columnaTotalCargoLinea = 0;
            var columnaTotalKm = 0;
            foreach (var item in datos.encabezadoLineaRemu)
            {
                workSheetEquipos.Cells[recordIndexEquipo, contColumnasEquipo].Value = item.ToString();
                workSheetEquipos.Cells[recordIndexEquipo, contColumnasEquipo].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);

                if (item.ToString() == "$/mes")
                {
                    columnaTotalCargoLinea = contColumnasEquipo;
                }

                if (item.ToString() == "Long [km]")
                {
                    columnaTotalKm = contColumnasEquipo;
                }

                contColumnasEquipo++;
            }
            recordIndexEquipo++;
            //Datos body
            foreach (var fila in datos.listaFilasLineaRemu)
            {
                var countFila = 2;
                if (fila.datoPorFilaLineaRemu != null)
                {
                    foreach (var detFil in fila.datoPorFilaLineaRemu)
                    {
                        workSheetEquipos.Cells[recordIndexEquipo, countFila].Value = detFil;
                        workSheetEquipos.Cells[recordIndexEquipo, countFila].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                        workSheetEquipos.Column(countFila).AutoFit();
                        countFila++;

                    }
                    recordIndexEquipo++;
                }
            }
            //Total Cargo
            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalCargoLinea].Value = datos.totalesLineaRemu.datoPorFilaTotalesLineaRemu[0];
            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalCargoLinea].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalCargoLinea].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);

            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalKm].Value = datos.totalesLineaRemu.datoPorFilaTotalesLineaRemu[1];
            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalKm].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalKm].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);

            #endregion

            #region Transformador

            contColumnasEquipo += 2; //Aumento 2 columnas a la derecha 
            var inicioTrafo = contColumnasEquipo;

            workSheetEquipos.Cells[1, contColumnasEquipo - 1].Value = "1.-TRANSFORMADORES";

            recordIndexEquipo = 3; //Vuelvo a la fila 3

            //Tarifas actuales
            workSheetEquipos.Row(2).Height = 20;
            workSheetEquipos.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheetEquipos.Cells[2, contColumnasEquipo].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetEquipos.Row(2).Style.Font.Bold = true;
            workSheetEquipos.Cells[2, contColumnasEquipo].Value = "Tarifas Actuales";

            //Filas y columnas de los montos
            foreach (var fila in datos.listaFilasTrafosRemu)
            {
                var countColumnEquipo = contColumnasEquipo;
                if (fila.datoPorFilaMontoTrafoRemu != null)
                {
                    foreach (var detFil in fila.datoPorFilaMontoTrafoRemu)
                    {
                        workSheetEquipos.Cells[recordIndexEquipo, countColumnEquipo].Value = detFil;
                        workSheetEquipos.Column(countColumnEquipo).AutoFit();
                        workSheetEquipos.Cells[recordIndexEquipo, countColumnEquipo].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                        countColumnEquipo++;
                    }
                    recordIndexEquipo++;

                }

            }
            recordIndexEquipo++;

            //Filas y Columnas de indisponibilidades ENCABEZADO DE LOS DATOS 
            var columnaTotalCargoTrafo = 0;
            var columnaTotalPOT = 0;
            foreach (var item in datos.encabezadoTrafosRemu)
            {
                workSheetEquipos.Cells[recordIndexEquipo, contColumnasEquipo].Value = item.ToString();
                workSheetEquipos.Cells[recordIndexEquipo, contColumnasEquipo].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);

                if (item.ToString() == "X/MES")
                {
                    columnaTotalCargoTrafo = contColumnasEquipo;
                }

                if (item.ToString() == "POT.[MVA]")
                {
                    columnaTotalPOT = contColumnasEquipo;
                }

                contColumnasEquipo++;
            }
            recordIndexEquipo++;
            //Datos de las indisponibildades
            foreach (var fila in datos.listaFilasTrafosRemu)
            {
                var countFila = inicioTrafo;
                if (fila.datoPorFilaTrafoRemu != null)
                {
                    foreach (var detFil in fila.datoPorFilaTrafoRemu)
                    {
                        workSheetEquipos.Cells[recordIndexEquipo, countFila].Value = detFil;
                        workSheetEquipos.Cells[recordIndexEquipo, countFila].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                        workSheetEquipos.Column(countFila).AutoFit();
                        countFila++;

                    }
                    recordIndexEquipo++;
                }
            }
            //Total Cargo
            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalCargoTrafo].Value = datos.totalesTrafosRemu.datoPorFilaTotalesTrafoRemu[0];
            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalCargoTrafo].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalCargoTrafo].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);

            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalPOT].Value = datos.totalesTrafosRemu.datoPorFilaTotalesTrafoRemu[1];
            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalPOT].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalPOT].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);


            #endregion

            #region PtosC

            contColumnasEquipo += 2; //Aumento 2 columnas a la derecha 
            var inicioPtosC = contColumnasEquipo;

            workSheetEquipos.Cells[1, contColumnasEquipo - 1].Value = "1.-CAMPOS";


            //Tarifas actuales
            workSheetEquipos.Row(2).Height = 20;
            workSheetEquipos.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheetEquipos.Cells[2, contColumnasEquipo].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetEquipos.Row(2).Style.Font.Bold = true;
            workSheetEquipos.Cells[2, contColumnasEquipo].Value = "Tarifas Actuales";

            recordIndexEquipo = 3; //Vuelvo a la fila 3
            //Filas y columnas de los montos
            foreach (var fila in datos.listaFilasPtosCRemu)
            {
                var countColumnEquipo = contColumnasEquipo;
                if (fila.datoPorFilaMontoPtoCRemu != null)
                {
                    foreach (var detFil in fila.datoPorFilaMontoPtoCRemu)
                    {
                        workSheetEquipos.Cells[recordIndexEquipo, countColumnEquipo].Value = detFil;
                        workSheetEquipos.Column(countColumnEquipo).AutoFit();
                        workSheetEquipos.Cells[recordIndexEquipo, countColumnEquipo].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                        countColumnEquipo++;
                    }
                    recordIndexEquipo++;

                }

            }
            recordIndexEquipo++;

            //Filas y Columnas de indisponibilidades ENCABEZADO DE LOS DATOS 
            var columnaTotalCargoPtosC = 0;
            var columnaTotalEquipos = 0;
            var columnaTotalMes = 0;
            foreach (var item in datos.encabezadoPtosCRemu)
            {
                workSheetEquipos.Cells[recordIndexEquipo, contColumnasEquipo].Value = item.ToString();
                workSheetEquipos.Cells[recordIndexEquipo, contColumnasEquipo].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);

                if (item.ToString() == "$/mes")
                {
                    columnaTotalCargoPtosC = contColumnasEquipo;
                }

                if (item.ToString() == "")
                {
                    columnaTotalEquipos = contColumnasEquipo;
                }
                if (item.ToString() == "$/mes")
                {
                    columnaTotalMes = contColumnasEquipo;
                }

                contColumnasEquipo++;
            }
            recordIndexEquipo++;
            //Datos de las indisponibildades
            foreach (var fila in datos.listaFilasPtosCRemu)
            {
                var countFila = inicioPtosC;
                if (fila.datoPorFilaPtoCRemu != null)
                {
                    foreach (var detFil in fila.datoPorFilaPtoCRemu)
                    {
                        workSheetEquipos.Cells[recordIndexEquipo, countFila].Value = detFil;
                        workSheetEquipos.Cells[recordIndexEquipo, countFila].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                        workSheetEquipos.Column(countFila).AutoFit();
                        countFila++;

                    }
                    recordIndexEquipo++;
                }
            }
            //Total Cargo
            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalCargoPtosC].Value = datos.totalesPtosCRemu.datoPorFilaTotalesPtoCRemu[0];
            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalCargoPtosC].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalCargoPtosC].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);

            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalEquipos].Value = datos.totalesPtosCRemu.datoPorFilaTotalesPtoCRemu[1];
            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalEquipos].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalEquipos].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);

            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalMes].Value = datos.totalesPtosCRemu.datoPorFilaTotalesPtoCRemu[2];
            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalMes].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalMes].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);



            #endregion

            #region Capacitor

            contColumnasEquipo += 2; //Aumento 2 columnas a la derecha 
            var inicioCapacitor = contColumnasEquipo;

            workSheetEquipos.Cells[1, contColumnasEquipo - 1].Value = "1.-REACTIVO";

            //Tarifas actuales
            workSheetEquipos.Row(2).Height = 20;
            workSheetEquipos.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheetEquipos.Cells[2, contColumnasEquipo].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetEquipos.Row(2).Style.Font.Bold = true;
            workSheetEquipos.Cells[2, contColumnasEquipo].Value = "Tarifas Actuales";

            recordIndexEquipo = 3; //Vuelvo a la fila 3
            //Filas y columnas de los montos
            foreach (var fila in datos.listaFilasCapCRemu)
            {
                var countColumnEquipo = contColumnasEquipo;
                if (fila.datoPorFilaMontoCapRemu != null)
                {
                    foreach (var detFil in fila.datoPorFilaMontoCapRemu)
                    {
                        workSheetEquipos.Cells[recordIndexEquipo, countColumnEquipo].Value = detFil;
                        workSheetEquipos.Column(countColumnEquipo).AutoFit();
                        workSheetEquipos.Cells[recordIndexEquipo, countColumnEquipo].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                        countColumnEquipo++;
                    }
                    recordIndexEquipo++;

                }

            }
            recordIndexEquipo++;

            //Filas y Columnas de indisponibilidades ENCABEZADO DE LOS DATOS 
            var columnaTotalCargoCap = 0;
            foreach (var item in datos.encabezadoCapRemu)
            {
                workSheetEquipos.Cells[recordIndexEquipo, contColumnasEquipo].Value = item.ToString();
                workSheetEquipos.Cells[recordIndexEquipo, contColumnasEquipo].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);

                if (item.ToString() == "$/mes")
                {
                    columnaTotalCargoCap = contColumnasEquipo;
                }

                contColumnasEquipo++;
            }
            recordIndexEquipo++;
            //Datos de las indisponibildades
            foreach (var fila in datos.listaFilasCapCRemu)
            {
                var countFila = inicioCapacitor;
                if (fila.datoPorFilaCapRemu != null)
                {
                    foreach (var detFil in fila.datoPorFilaCapRemu)
                    {
                        workSheetEquipos.Cells[recordIndexEquipo, countFila].Value = detFil;
                        workSheetEquipos.Cells[recordIndexEquipo, countFila].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                        workSheetEquipos.Column(countFila).AutoFit();
                        countFila++;

                    }
                    recordIndexEquipo++;
                }
            }
            //Total Cargo
            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalCargoCap].Value = datos.totalesCapRemu.datoPorFilaTotalesCapRemu[0];
            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalCargoCap].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetEquipos.Cells[recordIndexEquipo, columnaTotalCargoCap].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);



            #endregion




            #region Linea

            var workSheetLinea = excel.Workbook.Worksheets.Add("Linea");

            workSheetLinea.TabColor = System.Drawing.Color.Yellow;
            workSheetLinea.DefaultRowHeight = 12;

            workSheetLinea.Row(1).Height = 20;
            workSheetLinea.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheetLinea.Row(1).Style.Font.Bold = true;

            //var contColumnasT = 4;
            workSheetLinea.Cells[1, 4].Value = "SISTEMA DE TRANSPORTE DE ENERGÍA ELECTRICA POR DISTRIBUCIÓN TRONCAL - DISTROCUYO S.A. - LINEA" ;

            int recordIndex = 3; //Contador de Filas

            //Tarifas actuales
            workSheetLinea.Row(2).Height = 20;
            workSheetLinea.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheetLinea.Cells[2, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetLinea.Row(2).Style.Font.Bold = true;
            workSheetLinea.Cells[2, 1].Value = "Tarifas Actuales";

            //Filas y columnas de los montos
            foreach (var fila in datos.listaFilasLinea)
            {
                var countColumn = 1;
                if (fila.datoPorFilaMontoLinea != null)
                {
                    foreach (var detFil in fila.datoPorFilaMontoLinea)
                    {
                        workSheetLinea.Cells[recordIndex, countColumn].Value = detFil;
                        workSheetLinea.Column(countColumn).AutoFit();
                        workSheetLinea.Cells[recordIndex, countColumn].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                        countColumn++;
                    }
                    recordIndex++;

                }

            }
            recordIndex++;

            workSheetLinea.Row(recordIndex).Style.Font.Bold = true;
            //Filas y Columnas de indisponibilidades ENCABEZADO DE LOS DATOS 
            var contColumnas = 1;//Encabezados de columnas
            workSheetLinea.Row(recordIndex).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            var columnaTotalPenalizacion = 0; var columnaNoPercibido = 0; var columnaLucroCesante = 0;
            foreach (var item in datos.encabezadoLinea)
            {
                workSheetLinea.Cells[recordIndex, contColumnas].Value = item.ToString();

                if (item.ToString() != "")
                {
                    workSheetLinea.Cells[recordIndex, contColumnas].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);

                    if (item.ToString() == "Total Penalización")
                    {
                        columnaTotalPenalizacion = contColumnas;
                    }
                    else if (item.ToString() == "No Percibido")
                    {
                        columnaNoPercibido = contColumnas;
                    }
                    else if (item.ToString() == "Lucro Cesante")
                    {
                        columnaLucroCesante = contColumnas;
                    }

                }
                contColumnas++;
            }
            recordIndex++;
            //Datos de las indisponibildades
            foreach (var fila in datos.listaFilasLinea)
            {
                var countFila = 1;
                if (fila.datoPorFilaLinea != null)
                {
                    foreach (var detFil in fila.datoPorFilaLinea)
                    {
                        workSheetLinea.Cells[recordIndex, countFila].Value = detFil;
                        workSheetLinea.Column(countFila).AutoFit();
                        if (detFil != " ")
                        {
                            workSheetLinea.Cells[recordIndex, countFila].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                        }

                        countFila++;

                    }
                    recordIndex++;
                }
            }
            //Total Penalizacion
            workSheetLinea.Cells[recordIndex, columnaTotalPenalizacion].Value = datos.totalesLineas.datoPorFilaTotalesLineas[0];
            workSheetLinea.Cells[recordIndex, columnaTotalPenalizacion].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetLinea.Cells[recordIndex, columnaTotalPenalizacion].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);
            //No Percibido
            workSheetLinea.Cells[recordIndex, columnaNoPercibido].Value = datos.totalesLineas.datoPorFilaTotalesLineas[1];
            workSheetLinea.Cells[recordIndex, columnaNoPercibido].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetLinea.Cells[recordIndex, columnaNoPercibido].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);
            //Lucro Cesante
            workSheetLinea.Cells[recordIndex, columnaLucroCesante].Value = datos.totalesLineas.datoPorFilaTotalesLineas[2];
            workSheetLinea.Cells[recordIndex, columnaLucroCesante].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetLinea.Cells[recordIndex, columnaLucroCesante].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);


            #endregion

            #region Transformador

            var workSheetTrafo = excel.Workbook.Worksheets.Add("Trafo");

            workSheetTrafo.TabColor = System.Drawing.Color.Yellow;
            workSheetTrafo.DefaultRowHeight = 12;

            workSheetTrafo.Row(1).Height = 20;
            workSheetTrafo.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheetTrafo.Row(1).Style.Font.Bold = true;

            //var contColumnasT = 4;
            workSheetTrafo.Cells[1, 4].Value = "SISTEMA DE TRANSPORTE DE ENERGÍA ELECTRICA POR DISTRIBUCIÓN TRONCAL - DISTROCUYO S.A. - TRANSFORMADORES";

            int recordIndexT = 3; //Contador de Filas

            //Tarifas actuales
            workSheetTrafo.Row(2).Height = 20;
            workSheetTrafo.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheetTrafo.Cells[2, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetTrafo.Row(2).Style.Font.Bold = true;
            workSheetTrafo.Cells[2, 1].Value = "Tarifas Actuales";

            //Filas y columnas de los montos
            foreach (var fila in datos.listaFilasTrafo)
            {
                var countColumnT = 1;
                if (fila.datoPorFilaMontoTrafo != null)
                {
                    foreach (var detFil in fila.datoPorFilaMontoTrafo)
                    {
                        workSheetTrafo.Cells[recordIndexT, countColumnT].Value = detFil;
                        workSheetTrafo.Column(countColumnT).AutoFit();
                        workSheetTrafo.Cells[recordIndexT, countColumnT].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                        countColumnT++;
                    }
                    recordIndexT++;

                }

            }
            recordIndexT++;

            workSheetTrafo.Row(recordIndexT).Style.Font.Bold = true;
            //Filas y Columnas de indisponibilidades ENCABEZADO DE LOS DATOS 
            var contColumnasT = 1;//Encabezados de columnas
            workSheetTrafo.Row(recordIndexT).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            var columnaTotalPenalizacionT = 0; var columnaNoPercibidoT = 0; var columnaLucroCesanteT = 0;
            foreach (var item in datos.encabezadoTrafo)
            {
                workSheetTrafo.Cells[recordIndexT, contColumnasT].Value = item.ToString();

                if (item.ToString() != "")
                {
                    workSheetTrafo.Cells[recordIndexT, contColumnasT].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);

                    if (item.ToString() == "Total Penalización")
                    {
                        columnaTotalPenalizacionT = contColumnasT;
                    }
                    else if (item.ToString() == "No Percibido")
                    {
                        columnaNoPercibidoT = contColumnasT;
                    }
                    else if (item.ToString() == "Lucro Cesante")
                    {
                        columnaLucroCesanteT = contColumnasT;
                    }

                }
                contColumnasT++;
            }
            recordIndexT++;
            //Datos de las indisponibildades
            foreach (var fila in datos.listaFilasTrafo)
            {
                var countFila = 1;
                if (fila.datoPorFilaTrafo != null)
                {
                    foreach (var detFil in fila.datoPorFilaTrafo)
                    {
                        workSheetTrafo.Cells[recordIndexT, countFila].Value = detFil;
                        workSheetTrafo.Column(countFila).AutoFit();
                        if (detFil != " ")
                        {
                            workSheetTrafo.Cells[recordIndexT, countFila].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                        }

                        countFila++;

                    }
                    recordIndexT++;
                }
            }
            //Total Penalizacion
            workSheetTrafo.Cells[recordIndexT, columnaTotalPenalizacionT].Value = datos.totalesTrafo.datoPorFilaTotalesTrafo[0];
            workSheetTrafo.Cells[recordIndexT, columnaTotalPenalizacionT].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetTrafo.Cells[recordIndexT, columnaTotalPenalizacionT].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);
            //No Percibido
            workSheetTrafo.Cells[recordIndexT, columnaNoPercibidoT].Value = datos.totalesTrafo.datoPorFilaTotalesTrafo[1];
            workSheetTrafo.Cells[recordIndexT, columnaNoPercibidoT].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetTrafo.Cells[recordIndexT, columnaNoPercibidoT].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);
            //Lucro Cesante
            workSheetTrafo.Cells[recordIndexT, columnaLucroCesanteT].Value = datos.totalesTrafo.datoPorFilaTotalesTrafo[2];
            workSheetTrafo.Cells[recordIndexT, columnaLucroCesanteT].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetTrafo.Cells[recordIndexT, columnaLucroCesanteT].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);

            #endregion

            #region PtosC

            var workSheetPtosC = excel.Workbook.Worksheets.Add("Campos");

            workSheetPtosC.TabColor = System.Drawing.Color.Yellow;
            workSheetPtosC.DefaultRowHeight = 12;

            workSheetPtosC.Row(1).Height = 20;
            workSheetPtosC.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheetPtosC.Row(1).Style.Font.Bold = true;

            //var contColumnasT = 4;
            workSheetPtosC.Cells[1, 4].Value = "SISTEMA DE TRANSPORTE DE ENERGÍA ELECTRICA POR DISTRIBUCIÓN TRONCAL - DISTROCUYO S.A. - CAMPOS";

            int recordIndexP = 3; //Contador de Filas

            //Tarifas actuales
            workSheetPtosC.Row(2).Height = 20;
            workSheetPtosC.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheetPtosC.Cells[2, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetPtosC.Row(2).Style.Font.Bold = true;
            workSheetPtosC.Cells[2, 1].Value = "Tarifas Actuales";

            //Filas y columnas de los montos
            foreach (var fila in datos.listaFilasPtoC)
            {
                var countColumnP = 1;
                if (fila.datoPorFilaMontoPtosC != null)
                {
                    foreach (var detFil in fila.datoPorFilaMontoPtosC)
                    {
                        workSheetPtosC.Cells[recordIndexP, countColumnP].Value = detFil;
                        workSheetPtosC.Column(countColumnP).AutoFit();
                        workSheetPtosC.Cells[recordIndexP, countColumnP].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                        countColumnP++;
                    }
                    recordIndexP++;

                }

            }
            recordIndexP++;

            workSheetPtosC.Row(recordIndexP).Style.Font.Bold = true;
            //Filas y Columnas de indisponibilidades ENCABEZADO DE LOS DATOS 
            var contColumnasP = 1;//Encabezados de columnas
            workSheetPtosC.Row(recordIndexP).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            var columnaTotalPenalizacionP = 0; var columnaNoPercibidoP = 0; var columnaLucroCesanteP = 0;
            foreach (var item in datos.encabezadoPtoC)
            {
                workSheetPtosC.Cells[recordIndexP, contColumnasP].Value = item.ToString();

                if (item.ToString() != "")
                {
                    workSheetPtosC.Cells[recordIndexP, contColumnasP].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);

                    if (item.ToString() == "Total Penalización")
                    {
                        columnaTotalPenalizacionP = contColumnasP;
                    }
                    else if (item.ToString() == "No Percibido")
                    {
                        columnaNoPercibidoP = contColumnasP;
                    }
                    else if (item.ToString() == "Lucro Cesante")
                    {
                        columnaLucroCesanteP = contColumnasP;
                    }

                }
                contColumnasP++;
            }
            recordIndexP++;
            //Datos de las indisponibildades
            foreach (var fila in datos.listaFilasPtoC)
            {
                var countFila = 1;
                if (fila.datoPorFilaPtosC!= null)
                {
                    foreach (var detFil in fila.datoPorFilaPtosC)
                    {
                        workSheetPtosC.Cells[recordIndexP, countFila].Value = detFil;
                        workSheetPtosC.Column(countFila).AutoFit();
                        if (detFil != " ")
                        {
                            workSheetPtosC.Cells[recordIndexP, countFila].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                        }

                        countFila++;

                    }
                    recordIndexP++;
                }
            }
            //Total Penalizacion
            workSheetPtosC.Cells[recordIndexP, columnaTotalPenalizacionP].Value = datos.totalesPtoC.datoPorFilaTotalesPtosC[0];
            workSheetPtosC.Cells[recordIndexP, columnaTotalPenalizacionP].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetPtosC.Cells[recordIndexP, columnaTotalPenalizacionP].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);
            //No Percibido
            workSheetPtosC.Cells[recordIndexP, columnaNoPercibidoP].Value = datos.totalesPtoC.datoPorFilaTotalesPtosC[1];
            workSheetPtosC.Cells[recordIndexP, columnaNoPercibidoP].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetPtosC.Cells[recordIndexP, columnaNoPercibidoP].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);
            //Lucro Cesante
            workSheetPtosC.Cells[recordIndexP, columnaLucroCesanteP].Value = datos.totalesPtoC.datoPorFilaTotalesPtosC[2];
            workSheetPtosC.Cells[recordIndexP, columnaLucroCesanteP].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetPtosC.Cells[recordIndexP, columnaLucroCesanteP].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);

            #endregion

            #region Capacitor

            var workSheetCapacitor = excel.Workbook.Worksheets.Add("Reactivos");

            workSheetCapacitor.TabColor = System.Drawing.Color.Yellow;
            workSheetCapacitor.DefaultRowHeight = 12;

            workSheetCapacitor.Row(1).Height = 20;
            workSheetCapacitor.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheetCapacitor.Row(1).Style.Font.Bold = true;

            //var contColumnasT = 4;
            workSheetCapacitor.Cells[1, 4].Value = "SISTEMA DE TRANSPORTE DE ENERGÍA ELECTRICA POR DISTRIBUCIÓN TRONCAL - DISTROCUYO S.A. - REACTIVOS";

            int recordIndexC = 3; //Contador de Filas

            //Tarifas actuales
            workSheetCapacitor.Row(2).Height = 20;
            workSheetCapacitor.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheetCapacitor.Cells[2, 1].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetCapacitor.Row(2).Style.Font.Bold = true;
            workSheetCapacitor.Cells[2, 1].Value = "Tarifas Actuales";

            //Filas y columnas de los montos
            foreach (var fila in datos.listaFilasCapacitor)
            {
                var countColumnC = 1;
                if (fila.datoPorFilaMontoCapacitor!= null)
                {
                    foreach (var detFil in fila.datoPorFilaMontoCapacitor)
                    {
                        workSheetCapacitor.Cells[recordIndexC, countColumnC].Value = detFil;
                        workSheetCapacitor.Column(countColumnC).AutoFit();
                        workSheetCapacitor.Cells[recordIndexC, countColumnC].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                        countColumnC++;
                    }
                    recordIndexC++;

                }

            }
            recordIndexC++;

            workSheetCapacitor.Row(recordIndexC).Style.Font.Bold = true;
            //Filas y Columnas de indisponibilidades ENCABEZADO DE LOS DATOS 
            var contColumnasC = 1;//Encabezados de columnas
            workSheetCapacitor.Row(recordIndexC).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            var columnaTotalPenalizacionC = 0; var columnaNoPercibidoC = 0; var columnaLucroCesanteC = 0;
            foreach (var item in datos.encabezadoCapacitor)
            {
                workSheetCapacitor.Cells[recordIndexC, contColumnasC].Value = item.ToString();

                if (item.ToString() != "")
                {
                    workSheetCapacitor.Cells[recordIndexC, contColumnasC].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);

                    if (item.ToString() == "Total Penalización")
                    {
                        columnaTotalPenalizacionC = contColumnasC;
                    }
                    else if (item.ToString() == "No Percibido")
                    {
                        columnaNoPercibidoC = contColumnasC;
                    }
                    else if (item.ToString() == "Lucro Cesante")
                    {
                        columnaLucroCesanteC = contColumnasC;
                    }

                }
                contColumnasC++;
            }
            recordIndexC++;
            //Datos de las indisponibildades
            foreach (var fila in datos.listaFilasCapacitor)
            {
                var countFila = 1;
                if (fila.datoPorFilaCapacitor!= null)
                {
                    foreach (var detFil in fila.datoPorFilaCapacitor)
                    {
                        workSheetCapacitor.Cells[recordIndexC, countFila].Value = detFil;
                        workSheetCapacitor.Column(countFila).AutoFit();
                        if (detFil != " ")
                        {
                            workSheetCapacitor.Cells[recordIndexC, countFila].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                        }

                        countFila++;

                    }
                    recordIndexC++;
                }
            }
            //Total Penalizacion
            workSheetCapacitor.Cells[recordIndexC, columnaTotalPenalizacionC].Value = datos.totalesCapacitores.datoPorFilaTotalesCapacitor[0];
            workSheetCapacitor.Cells[recordIndexC, columnaTotalPenalizacionC].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetCapacitor.Cells[recordIndexC, columnaTotalPenalizacionC].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);
            //No Percibido
            workSheetCapacitor.Cells[recordIndexC, columnaNoPercibidoC].Value = datos.totalesCapacitores.datoPorFilaTotalesCapacitor[1];
            workSheetCapacitor.Cells[recordIndexC, columnaNoPercibidoC].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetCapacitor.Cells[recordIndexC, columnaNoPercibidoC].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);
            //Lucro Cesante
            workSheetCapacitor.Cells[recordIndexC, columnaLucroCesanteC].Value = datos.totalesCapacitores.datoPorFilaTotalesCapacitor[2];
            workSheetCapacitor.Cells[recordIndexC, columnaLucroCesanteC].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
            workSheetCapacitor.Cells[recordIndexC, columnaLucroCesanteC].Style.Fill.SetBackground(System.Drawing.Color.LightGreen, ExcelFillStyle.Solid);

            #endregion

            #region Mes
            var fechaActual = fecha;//DateTime.Now;
            var ano = fechaActual.Year;
            string mes = fechaActual.ToString("MMMM");

            var workSheetMes = excel.Workbook.Worksheets.Add("Mes");

            workSheetMes.TabColor = System.Drawing.Color.Yellow;
            workSheetMes.DefaultRowHeight = 12;

            workSheetMes.Row(1).Height = 20;
            workSheetMes.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheetMes.Row(1).Style.Font.Bold = true;

            workSheetMes.Cells[1, 1].Value = "CIERRE MENSUAL:  " + mes.ToUpper() + "  " + ano;
            workSheetMes.Column(1).AutoFit();

            //INGRESOS DISTROCUYO S.A.
            workSheetMes.Row(3).Height = 20;
            workSheetMes.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            workSheetMes.Row(3).Style.Font.Bold = true;
            workSheetMes.Cells[3, 1].Value = "INGRESOS DISTROCUYO S.A.";


            recordIndex = 5; //Contador de Filas
            workSheetMes.Row(recordIndex).Style.Font.Bold = true;
            //Filas y Columnas de Mes ENCABEZADO DE LOS DATOS 
            contColumnas = 1;//Encabezados de columnas
            workSheetMes.Row(recordIndex).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            foreach (var item in datos.encabezadoMes)
            {
                if (item.ToString() != "CALCULO DISTRO")
                {
                    workSheetMes.Cells[recordIndex, contColumnas].Value = item.ToString();
                    workSheetMes.Cells[recordIndex, contColumnas].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                    workSheetMes.Column(recordIndex).AutoFit();
                    contColumnas++;

                }
                else
                { //Esto es donde dice "Calculo Distro"
                    workSheetMes.Cells[recordIndex - 1, contColumnas, recordIndex - 1, contColumnas + 1].Merge = true;
                    workSheetMes.Row(recordIndex - 1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    workSheetMes.Cells[recordIndex - 1, contColumnas].Value = item.ToString();
                    workSheetMes.Cells[recordIndex - 1, contColumnas, recordIndex - 1, contColumnas + 1].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                    workSheetMes.Row(recordIndex - 1).Style.Font.Bold = true;

                    workSheetMes.Column(recordIndex).AutoFit();
                }
            }
            recordIndex++;
            //Datos
            foreach (var fila in datos.listaFilasMes)
            {
                var countFila = 1;
                if (fila.datoPorFilaMes != null)
                {
                    foreach (var detFil in fila.datoPorFilaMes)
                    {
                        workSheetMes.Cells[recordIndex, countFila].Value = detFil;
                        workSheetMes.Cells[recordIndex, countFila].Style.Border.BorderAround(ExcelBorderStyle.Thin, System.Drawing.Color.Black);
                        workSheetMes.Column(countFila).AutoFit();
                        countFila++;
                    }
                    recordIndex++;
                }
            }
            #endregion

            // file name with .xlsx extension  
            //string p_strPath = "D:\\geeksforgeeks.xlsx";
            if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/TempFolder/")))
                Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/TempFolder/"));
            var pathInic = "~/TempFolder/";


            var nombreArchivo = "IndisRemunerada_" + Convert.ToDateTime(DateTime.Today).ToString("dd-MM-yyyy") + ".xlsx";
            var fechaParseada = Convert.ToDateTime(DateTime.Today).ToString("dd-MM-yyyy");

            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(pathInic), nombreArchivo);

            string p_strPath = "D:\\Indisponibilidades_" + fechaParseada + ".xlsx";
            //string p_strPath = pathInic + nombreArchivo;
            if (File.Exists(path))
                File.Delete(path);

            // Create excel file on physical disk  
            FileStream objFileStrm = File.Create(path);
            objFileStrm.Close();

            // Write content to excel file  
            File.WriteAllBytes(path, excel.GetAsByteArray());
            //Close Excel package 
            excel.Dispose();

            //Console.ReadKey();
            datos.fileName = nombreArchivo;
            datos.filePath = path;


            return datos;
        }

    }
}
