﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class ReclamoVM
    {
        public int idReclamo { get; set; }
        public int? idEquipo { get; set; }
        public string nombreReclamo { get; set; }
        public string nombreEquipo { get; set; }
        public bool? activo { get; set; }

    }
}
