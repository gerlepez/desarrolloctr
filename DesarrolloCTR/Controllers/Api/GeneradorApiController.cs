﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System.Net.Http.Formatting;

namespace DesarrolloCTR.Controllers.Api
{
    public class GeneradorApiController : ApiController
    {

        [System.Web.Http.Route("Api/Generador/ObtenerGeneradores")]
        [System.Web.Http.ActionName("ObtenerGeneradores")]
        [System.Web.Http.HttpGet]
        public object ObtenerGeneradores(FormDataCollection form, bool? activo)
        {

            ServicioGenerador servicioGenerador = new ServicioGenerador();
            object lista = servicioGenerador.ObtenerGeneradores(activo);
            return lista;
            
        }


        [System.Web.Http.Route("Api/Generador/TablaGeneradores")]
        [System.Web.Http.ActionName("TablaGeneradores")]
        [System.Web.Http.HttpPost]
        public object TablaGeneradores(FormDataCollection form , bool? activo)
        {
            var draw = form.Get("draw");
            var start = form.Get("start");
            var length = form.Get("length");
            var sortColumn = (form.Get("columns[" + form.Get("order[0][column]").FirstOrDefault() + "][data]").ToString()).ToString();
            var sortColumnDir = form.Get("order[0][dir]").ToString();
            var searchValue = form.Get("search[value]").ToString();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            int cantidadRegistrosPagina = Int32.Parse(length);

            ServicioGenerador servicioGenerador = new ServicioGenerador();

            object listagenerador = servicioGenerador.TablaGeneradores(activo ,draw, recordsTotal, skip, pageSize);
            return listagenerador;

            /*
            ServicioGenerador servicioGenerador = new ServicioGenerador();
            object lista = servicioGenerador.ObtenerGeneradores(activo);
            return lista;
            */
        }

        [System.Web.Http.Route("Api/Generador/AltaGenerador")]
        [System.Web.Http.ActionName("AltaGenerador")]
        [System.Web.Http.HttpPost]
        public int AltaGenerador(GeneradorVM generador)
        {

            var idUsuarioLogeado = 0;
            if(generador != null)
            {
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogeado = serUsuario.ObtenerUsuarioLogueado(generador.nombresuario);
            }
            ServicioGenerador servicioGenerador = new ServicioGenerador();
            int codigo;
            if (servicioGenerador.ValidarGenerador(generador))
            {
                codigo = servicioGenerador.AltaGenerador(generador , idUsuarioLogeado);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }

        [System.Web.Http.Route("Api/Generador/ModificarGenerador")]
        [System.Web.Http.ActionName("ModificarGenerador")]
        [System.Web.Http.HttpPost]
        public int ModificarGenerador(GeneradorVM generador)
        {
            var idUsuarioLogeado = 0;
            if (generador != null)
            {
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogeado = serUsuario.ObtenerUsuarioLogueado(generador.nombresuario);
            }

            ServicioGenerador servicioGenerador = new ServicioGenerador();
            int codigo;
            if (servicioGenerador.ValidarGenerador(generador))
            {
                codigo = servicioGenerador.ModificarGenerador(generador , idUsuarioLogeado);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }

        [System.Web.Http.Route("Api/Generador/EliminarGenerador")]
        [System.Web.Http.ActionName("EliminarGenerador")]
        [System.Web.Http.HttpPost]
        public int EliminarGenerador(GeneradorVM generador)
        {
            ServicioGenerador servicioGenerador = new ServicioGenerador();
            int codigo = servicioGenerador.EliminarGenerador(generador);
            return codigo;
        }

        [System.Web.Http.Route("Api/Generador/ConsultarUnGenerador")]
        [System.Web.Http.ActionName("ConsultarUnGenerador")]
        [System.Web.Http.HttpGet]
        public GeneradorVM ConsultarUnGenerador(int idGenerador)
        {
            ServicioGenerador servicioGenerador = new ServicioGenerador();
            GeneradorVM generador = servicioGenerador.ConsultarUnGenerador(idGenerador);
            return generador;
        }

        [System.Web.Http.Route("Api/Generador/ObtenerGeneradorPorCentral")]
        [System.Web.Http.ActionName("ObtenerGeneradorPorCentral")]
        [System.Web.Http.HttpGet]
        public object ObtenerGeneradorPorCentral(int idCentral , int idNovedad = 0)
        {
            ServicioGenerador servicioGenerador = new ServicioGenerador();
            object listGen = servicioGenerador.ObtenerGeneradoresPorCentral(idCentral , idNovedad);
            return listGen;
        }

       
        [System.Web.Http.Route("Api/Generador/ObtenerGeneradorPorUsuario")]
        [System.Web.Http.ActionName("ObtenerGeneradorPorUsuario")]
        [System.Web.Http.HttpGet]
        public object ObtenerGeneradorPorUsuario(int idUsuario)
        {
            ServicioGenerador servicioGenerador = new ServicioGenerador();
            object listGen = servicioGenerador.ObtenerGeneradoresPorUsuario(idUsuario);
            return listGen;
        }


        [System.Web.Http.Route("Api/Generador/RecuperarGenerador")]
        [System.Web.Http.ActionName("RecuperarGenerador")]
        [System.Web.Http.HttpPost]
        public int RecuperarGenerador(GeneradorVM generador)
        {
            ServicioGenerador servicioGenerador = new ServicioGenerador();
            int codigo = servicioGenerador.RecuperarGenerador(generador);
            return codigo;

        }

    }
}