﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class PosesionLineaVM
    {
        public int idPosesionLinea { get; set; }
        public string nombrePosesionLinea { get; set; }
        public bool activo { get; set; }
    }
}
