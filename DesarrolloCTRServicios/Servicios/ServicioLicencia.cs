﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioLicencia
    {
        public object ObtenerListaLicencia(bool activo)
        {
            List<LicenciaVM> listaLicencia;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                
                    ServicioUsuario servUsuario = new ServicioUsuario();

                    listaLicencia = (from c in db.LicLicencia
                                     join d in db.LicEstadoLicencia on c.idEstadoLicencia equals d.idEstadoLicencia
                                     where c.activo == activo
                                     select new LicenciaVM
                                     {
                                         idLicencia = c.idLicencia,
                                         codigoLicencia = c.codigoLicencia,
                                         descripcionLicencia = c.descripcionLicencia,
                                         activo = c.activo,
                                         idPersonal = c.idEmpleado,
                                         idPersonalAutoriza = c.idPersonalAutoriza,
                                         idPersonalCancela = c.idPersonalCancela,
                                         idPersonalFinaliza = c.idPersonalFinaliza,
                                         idResponsableConsigna = c.idResponsableConsigna,
                                         observacionesFinaliza = c.observacionesFinaliza,
                                         fechaHoraInicioLicencia = c.fechaHoraInicioLicencia,
                                         estadoLicencia = new EstadoLicenciaVM
                                         {
                                             idEstadoLicencia = d.idEstadoLicencia,
                                             nombreEstadoLicencia = d.nombreEstadoLicencia,
                                             activo = d.activo
                                         },

                                     }).ToList();
                    var listaOrdenada = listaLicencia.OrderBy(x => x.idLicencia).ToList();
                    object json = new { data = listaLicencia };
                    return json;

                
            }
        }

        public LicenciaVM ConsultarUnaLicencia(int? idLicencia)
        {
            LicenciaVM licencia = new LicenciaVM();

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                using (BuhoGestionEntities buho = new BuhoGestionEntities())
                {
                    // Consulta en el primer contexto
                    var licencias = (from l in db.LicLicencia
                                     join e in db.LicEstadoLicencia on l.idEstadoLicencia equals e.idEstadoLicencia
                                     join nov in db.NovNovedad on l.idLicencia equals nov.idLicencia
                                     where l.activo == true && nov.idLicencia != null && idLicencia == l.idLicencia
                                     select new
                                     {
                                         l.idLicencia,
                                         l.codigoLicencia,
                                         l.fechaHoraInicioLicencia,
                                         l.fechaHoraFinLicencia,
                                         l.descripcionLicencia,
                                         l.activo,
                                         l.idEmpleado,
                                         l.idResponsableConsigna,
                                         l.idPersonalAutoriza,
                                         l.observacionesFinaliza,
                                         l.idPersonalCancela,
                                         l.idPersonalFinaliza,
                                         l.idTipoLicencia,
                                         EstadoLicencia = new
                                         {
                                             e.idEstadoLicencia,
                                             e.nombreEstadoLicencia,
                                             e.activo
                                         },
                                         Novedad = new
                                         {
                                             nov.idNovedad,
                                             nov.descripcionNovedad
                                         }
                                     }).ToList();

                    // Consulta en el segundo contexto
                    var usuarios = (from u in buho.GralUsuario
                                    select new
                                    {
                                        u.idUsuario,
                                        u.Nombre
                                    }).ToList();

                    // Combinar resultados en memoria
                    var resultado = (from l in licencias
                                     join u in usuarios on l.idEmpleado equals u.idUsuario
                                     select new LicenciaVM
                                     {
                                         idLicencia = l.idLicencia,
                                         codigoLicencia = l.codigoLicencia,
                                         fechaHoraInicioLicencia = l.fechaHoraInicioLicencia,
                                         fechaHoraFinLicencia = l.fechaHoraFinLicencia,
                                         descripcionLicencia = l.descripcionLicencia,
                                         activo = l.activo,
                                         idPersonal = l.idEmpleado,
                                         nombreEntrega = u.Nombre,
                                         idResponsableConsigna = l.idResponsableConsigna,
                                         idPersonalAutoriza = l.idPersonalAutoriza,
                                         observacionesFinaliza = l.observacionesFinaliza,
                                         idPersonalCancela = l.idPersonalCancela,
                                         idPersonalFinaliza = l.idPersonalFinaliza,
                                         idTipoLicencia = l.idTipoLicencia,
                                         estadoLicencia = new EstadoLicenciaVM
                                         {
                                             idEstadoLicencia = l.EstadoLicencia.idEstadoLicencia,
                                             nombreEstadoLicencia = l.EstadoLicencia.nombreEstadoLicencia,
                                             activo = l.EstadoLicencia.activo
                                         },
                                         novedad = new NovedadVM
                                         {
                                             idNovedad = l.Novedad.idNovedad,
                                             descripcionNovedad = l.Novedad.descripcionNovedad
                                         }
                                     }).FirstOrDefault();

                    licencia = resultado;
                }
            }

            return licencia;

        }

        public LicenciaVM ConsultLicencia(int? idLicencia)
        {
            LicenciaVM licencia = new LicenciaVM();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                licencia = (from l in db.LicLicencia
                                //join l in db.LicLicencia on c.idNovedad equals l.idNovedad
                            join e in db.LicEstadoLicencia on l.idEstadoLicencia equals e.idEstadoLicencia
                            //join tl in db.LicTipoLicencia on l.idTipoLicencia equals tl.idTipoLicencia
                            where l.activo == true && idLicencia == l.idLicencia
                            select new { l, e }).AsEnumerable().Select(x => new LicenciaVM
                            {
                                idLicencia = x.l.idLicencia,
                                codigoLicencia = x.l.codigoLicencia,
                                fechaHoraInicioLicencia = x.l.fechaHoraInicioLicencia,
                                fechaHoraFinLicencia = x.l.fechaHoraFinLicencia,
                                fechaParseada = String.Format("{0:dd/MM/yyyy}", x.l.fechaHoraInicioLicencia),
                                //fechaLicenciaParseada = String.Format("{0:dd/MM/yyyy}", x.l.fechaHoraInicioLicencia),
                                horaLicenciaParseada = String.Format("{0:HH:mm}", x.l.fechaHoraInicioLicencia),
                                descripcionLicencia = x.l.descripcionLicencia,
                                activo = x.l.activo,
                                idPersonal = x.l.idEmpleado,
                                idResponsableConsigna = x.l.idResponsableConsigna,
                                idPersonalAutoriza = x.l.idPersonalAutoriza,
                                observacionesFinaliza = x.l.observacionesFinaliza,
                                idPersonalCancela = x.l.idPersonalCancela,
                                idPersonalFinaliza = x.l.idPersonalFinaliza,
                                idTipoLicencia = x.l.idTipoLicencia,
                                estadoLicencia = new EstadoLicenciaVM
                                {
                                    idEstadoLicencia = x.e.idEstadoLicencia,
                                    nombreEstadoLicencia = x.e.nombreEstadoLicencia,
                                    activo = x.e.activo,
                                },

                            }).FirstOrDefault();


                licencia.fechaLicenciaParseada = Convert.ToDateTime(licencia.fechaHoraInicioLicencia).ToString("yyyy-MM-dd");


                if (licencia != null)
                {
                    var listaAut = db.LicAutorizacionTrabajo.Where(x => x.idLicencia == idLicencia).ToList();
                    if (listaAut.Count > 0)
                    {
                        licencia.ultimoNumeroAutorizacion = listaAut.Max(x => x.numAutorizacion).Value;
                        List<AutorizacionTrabajoVM> listaAutorizacionesTrabajo = new List<AutorizacionTrabajoVM>();
                        foreach (var aut in listaAut)
                        {
                            if (aut.activo == true)
                            {
                                AutorizacionTrabajoVM autorizacion = new AutorizacionTrabajoVM
                                {
                                    idLicencia = idLicencia,
                                    idEmpleado = aut.idUsuario,
                                    idAutorizacionTrabajo = aut.idAutorizacionTrabajo,
                                    codigoAutorizacion = aut.codigoAutorizacion,
                                    descripcionAutorizacionTrabajo = aut.descripcion,
                                    estadoAutorizacion = new EstadoAutorizacionVM
                                    {
                                        idEstadoAutorizacion = aut.idEstadoActual,
                                        activo = aut.activo
                                    },
                                    fechaHoraInicioAutorizacionTrabajo = aut.fechaHoraInicio,
                                    fechaHoraFinAutorizacionTrabajo = aut.fechaHoraFin,
                                    numAutorizacion = aut.numAutorizacion,

                                };
                                using (BuhoGestionEntities buho = new BuhoGestionEntities())
                                {
                                    autorizacion.nombreAutorizado = buho.GralUsuario.Where(x => x.idUsuario == autorizacion.idEmpleado).FirstOrDefault().Nombre;
                                }
                                 listaAutorizacionesTrabajo.Add(autorizacion);
                            }

                        }
                        licencia.autorizacionTrabajo = listaAutorizacionesTrabajo;
                    }
                }
            }
            return licencia;
        }

        public NovedadVM ConsultaLicenciaParaAutTrabajo(int? idLicencia)
        {
            NovedadVM novedad = new NovedadVM();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                novedad = (from n in db.NovNovedad
                           join l in db.LicLicencia on n.idLicencia equals l.idLicencia
                           where n.activo == true && n.idLicencia == idLicencia
                           select new { n, l }).AsEnumerable().Select(x => new NovedadVM
                           {
                               idLicencia = x.l.idLicencia,
                               idNovedad = x.n.idNovedad,
                           }).FirstOrDefault();

            }
            return novedad;

        }

        public LicenciaVM GenerarLicencia(int idUsuario)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                DateTime dt = DateTime.Now;
                var currentYear = Int16.Parse(dt.ToString("yy"));
                var listaLicenciasAño = db.LicLicencia.Where(x => x.anioCodigo == currentYear).ToList();
                var ultimaLicenciaAño = listaLicenciasAño.OrderByDescending(x => x.numLicencia).FirstOrDefault();
                var codigo = "001";
                if (ultimaLicenciaAño != null)
                {
                    if (ultimaLicenciaAño.anioCodigo == currentYear)
                    {
                        codigo = (ultimaLicenciaAño.numLicencia + 1).ToString();
                        int count = codigo.Length;
                        switch (count)
                        {
                            case 1:
                                codigo = "00" + codigo;
                                break;
                            case 2:
                                codigo = "0" + codigo;
                                break;

                            default:
                                //Console.WriteLine("Default case");
                                break;
                        }
                    }

                }
                LicLicencia licencia = new LicLicencia
                {
                    codigoLicencia = currentYear.ToString() + "-" + codigo,
                    anioCodigo = currentYear,
                    numLicencia = Int16.Parse(codigo),
                    activo = true,
                    idEstadoLicencia = 1
                };



                db.LicLicencia.Add(licencia);
                db.SaveChanges();

                var estaLicencia = db.LicLicencia.OrderByDescending(x => x.idLicencia).FirstOrDefault();
                LicLicenciaUsuario licenciaUsuario = new LicLicenciaUsuario
                {
                    idLicLicencia = estaLicencia.idLicencia,
                    idUsuario = idUsuario,
                };

                //trackeo historial
                LicHistorialEstadoLic histLic = new LicHistorialEstadoLic
                {
                    idUsuario = idUsuario,
                    idLicencia = estaLicencia.idLicencia,
                    fechaHistEstLic = DateTime.Now,
                    idEstadoLic = 1
                };
                db.LicHistorialEstadoLic.Add(histLic);
                db.LicLicenciaUsuario.Add(licenciaUsuario);
                db.SaveChanges();
                return new LicenciaVM
                {
                    codigoLicencia = licencia.codigoLicencia,
                    idLicencia = estaLicencia.idLicencia
                };
            }
        }

        public List<LicenciaUsuarioVM> ConsultarRutesAbiertos(int idUsuario)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var rutes = db.LicLicenciaUsuario.ToList();
                List<LicenciaUsuarioVM> lista = new List<LicenciaUsuarioVM>();
                foreach (var rute in rutes)
                {
                    var lic = db.LicLicencia.Where(x => x.idLicencia == rute.idLicLicencia && x.activo == true).FirstOrDefault();
                    if (lic != null)
                    {
                        LicenciaUsuarioVM licencia = new LicenciaUsuarioVM
                        {
                            idLicLicencia = rute.idLicLicencia,
                            idUsuario = rute.idUsuario,
                            descripcion = rute.descripcion,
                            codigoLicencia = lic.codigoLicencia,
                            idLicLicenciaUsuario = rute.idLicLicenciaUsuario
                        };
                        lista.Add(licencia);
                    }
                }
                return lista.OrderByDescending(x => x.idLicLicencia).ToList();
            }
        }

        public object MostrarRutesAbiertos()
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var rutes = db.LicLicencia.Where(x => x.idEstadoLicencia == 1 && x.activo == true).ToList();
                //List<LicLicencia> lista = new List<LicLicencia>();
                List<LicenciaVM> lista = new List<LicenciaVM>();

                lista = (from l in db.LicLicencia
                         join n in db.NovNovedad on l.idLicencia equals n.idLicencia
                         where l.activo == true && l.idEstadoLicencia == 1 && n.activo == true
                         select new { n, l }).AsEnumerable().Select(x => new LicenciaVM
                         {
                             idLicencia = x.l.idLicencia,
                             codigoLicencia = x.l.codigoLicencia,
                             fechaHoraInicioLicencia = x.l.fechaHoraInicioLicencia,
                             descripcionLicencia = x.l.descripcionLicencia,
                             idNovedad = x.n.idNovedad,

                         }).ToList();

                //var listaOrdenada = lista.OrderByDescending(x => x.idLicencia).ToList();
                object json = new { data = lista.OrderByDescending(x => x.idLicencia) };
                return json;

                //if (rutes != null)
                //{          
                //foreach (var rute in rutes)
                //{
                //    var novedad = db.NovNovedad.Where(x => x.idLicencia == rute.idLicencia).FirstOrDefault();
                //    if (novedad != null)
                //    {

                //        lista.Add(rute);
                //    }
                //}
                //    lista = lista.OrderByDescending(x => x.idLicencia).ToList();
                //    object json = new { data = lista };
                //    return json;
                //}
                //else
                //{
                //    return null;
                //}

            }
        }




        public int GuardarDescripcion(string codigoLicencia, string descripcion = " ")
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var response = 0;

                var licencia = db.LicLicencia.Where(x => x.codigoLicencia == codigoLicencia).FirstOrDefault();
                var licenciaUsuario = db.LicLicenciaUsuario.Where(x => x.idLicLicencia == licencia.idLicencia).FirstOrDefault();
                licenciaUsuario.descripcion = descripcion;
                licencia.descripcionLicencia = descripcion;
                licencia.fechaHoraInicioLicencia = DateTime.Now;
                db.SaveChanges();
                return response;
            }
        }
        public LicenciaVM CompletarLicencia(LicenciaVM licencia, int idUsuario)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                DateTime fecha = (DateTime)licencia.fechaInicio;
                DateTime hora = (DateTime)licencia.horaInicio;
                DateTime date = fecha.AddHours(hora.Hour).AddMinutes(hora.Minute);
                DateTime dt = DateTime.Now;
                var currentYear = Int16.Parse(dt.ToString("yy"));
                var estaLicencia = db.LicLicencia.Where(x => x.idLicencia == licencia.idLicencia).FirstOrDefault();

                var idTipoLicencia = 0;
                var codigoTipoLicencia = "";
                if (licencia.tipoLicencia.idTipoLicencia != -1)
                {
                    idTipoLicencia = licencia.tipoLicencia.idTipoLicencia;
                    var tipoLicencia = db.LicTipoLicencia.Find(idTipoLicencia);
                    codigoTipoLicencia = "/" + tipoLicencia.codigoTipoLicencia;
                }

                estaLicencia.descripcionLicencia = licencia.descripcionLicencia;
                estaLicencia.activo = true;
                estaLicencia.idEstadoLicencia = 1;
                estaLicencia.idEmpleado = licencia.idPersonal;
                estaLicencia.codigoLicencia = estaLicencia.codigoLicencia.Split('/')[0] + codigoTipoLicencia;
                estaLicencia.idTipoLicencia = idTipoLicencia;
                estaLicencia.idPersonalAutoriza = licencia.idPersonalAutoriza;
                estaLicencia.idResponsableConsigna = licencia.idResponsableConsigna;
                estaLicencia.fechaHoraInicioLicencia = date;

                db.SaveChanges();
                return new LicenciaVM
                {
                    codigoLicencia = estaLicencia.codigoLicencia,
                    codigoEstadoAlta = 200
                };
            }
        }
        public LicenciaVM AltaLicencia(LicenciaVM licencia, int idUsuario)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                DateTime dt = DateTime.Now;

                var currentYear = Int16.Parse(dt.ToString("yy"));

                var idLicencia = licencia.idLicencia;
                var consultaunaLicencia = db.LicLicencia.Where(x => x.idLicencia == idLicencia).FirstOrDefault();
                var listaLicenciasAño = db.LicLicencia.Where(x => x.anioCodigo == currentYear).ToList();
                var ultimaLicencia = listaLicenciasAño.OrderByDescending(x => x.numLicencia).FirstOrDefault();
                var codigoTipoLicencia = "";
                var idTipoLicencia = 0;

                if (licencia.tipoLicencia.idTipoLicencia != -1)
                {
                    idTipoLicencia = licencia.tipoLicencia.idTipoLicencia;
                    var tipoLicencia = db.LicTipoLicencia.Find(idTipoLicencia);
                    codigoTipoLicencia = "/" + tipoLicencia.codigoTipoLicencia;
                }

                var codigo = "001";

                if (ultimaLicencia != null)
                {
                    if (ultimaLicencia.anioCodigo == currentYear)
                    {
                        codigo = (ultimaLicencia.numLicencia + 1).ToString();
                        int count = codigo.Length;
                        switch (count)
                        {
                            case 1:
                                codigo = "00" + codigo;
                                break;
                            case 2:
                                codigo = "0" + codigo;
                                break;

                            default:
                                //Console.WriteLine("Default case");
                                break;
                        }
                    }

                }

                if (consultaunaLicencia == null)
                {
                    // try
                    //  {

                    LicLicencia lic = new LicLicencia
                    {

                        descripcionLicencia = licencia.descripcionLicencia,
                        activo = true,
                        idEstadoLicencia = 1,
                        codigoLicencia = currentYear.ToString() + "-" + codigo + codigoTipoLicencia,
                        numLicencia = Int16.Parse(codigo),
                        anioCodigo = currentYear,
                        idEmpleado = licencia.idPersonal,
                        idTipoLicencia = idTipoLicencia,
                        idPersonalAutoriza = licencia.idPersonalAutoriza,
                        idResponsableConsigna = licencia.idResponsableConsigna

                    };

                    DateTime fecha = (DateTime)licencia.fechaInicio;
                    DateTime hora = (DateTime)licencia.horaInicio;
                    DateTime date = fecha.AddHours(hora.Hour).AddMinutes(hora.Minute);

                    lic.fechaHoraInicioLicencia = date;

                    db.LicLicencia.Add(lic);
                    db.SaveChanges();

                    var licenciaGuardada = db.LicLicencia.OrderByDescending(x => x.idLicencia).FirstOrDefault();

                    //trackeo historial
                    LicHistorialEstadoLic histLic = new LicHistorialEstadoLic
                    {
                        idUsuario = idUsuario,
                        idLicencia = licenciaGuardada.idLicencia,
                        fechaHistEstLic = DateTime.Now,
                        idEstadoLic = 1
                    };
                    db.LicHistorialEstadoLic.Add(histLic);
                    if (licencia.idNovedad != null)
                    {
                        var idNovedad = licencia.idNovedad;
                        var nov = db.NovNovedad.Find(idNovedad);
                        nov.idLicencia = licenciaGuardada.idLicencia;
                    }
                    db.SaveChanges();

                    return new LicenciaVM
                    {
                        codigoLicencia = lic.codigoLicencia,
                        codigoEstadoAlta = 200,
                        idLicencia = licenciaGuardada.idLicencia,

                    };


                    // }
                    /* catch (Exception e)
                     {
                         return new LicenciaVM
                         {
                             codigoLicencia = "",
                             codigoEstadoAlta = 400
                         }; ;
                     }*/

                }
                else
                {
                    consultaunaLicencia.descripcionLicencia = licencia.descripcionLicencia;
                    consultaunaLicencia.activo = true;
                    consultaunaLicencia.idEstadoLicencia = 1;
                    //consultaunaLicencia.codigoLicencia = currentYear.ToString() + "-" + codigo + codigoTipoLicencia;
                    //consultaunaLicencia.numLicencia = Int16.Parse(codigo);
                    consultaunaLicencia.anioCodigo = currentYear;
                    consultaunaLicencia.idEmpleado = licencia.idPersonal;
                    consultaunaLicencia.idTipoLicencia = idTipoLicencia;
                    consultaunaLicencia.idPersonalAutoriza = licencia.idPersonalAutoriza;
                    consultaunaLicencia.idResponsableConsigna = licencia.idResponsableConsigna;
                    DateTime fecha = (DateTime)licencia.fechaInicio;
                    DateTime hora = (DateTime)licencia.horaInicio;
                    DateTime date = fecha.AddHours(hora.Hour).AddMinutes(hora.Minute);
                    consultaunaLicencia.fechaHoraInicioLicencia = date;
                    db.SaveChanges();
                    return new LicenciaVM
                    {
                        codigoLicencia = consultaunaLicencia.codigoLicencia,
                        codigoEstadoAlta = 200,
                        idLicencia = consultaunaLicencia.idLicencia,
                    };
                }
            }
        }

        public LicenciaVM ModificarLicencia(LicenciaVM licencia, int idUsuario)
        {
            var licenciaVm = new LicenciaVM();

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var idLicencia = licencia.idLicencia;
                var consultaunaLicencia = db.LicLicencia.Find(idLicencia);
                if (consultaunaLicencia != null)
                {
                    // consultaunaLicencia.codigoLicencia = novedad.licencia.codigoLicencia;
                    consultaunaLicencia.descripcionLicencia = licencia.descripcionLicencia;
                    consultaunaLicencia.activo = true;
                    consultaunaLicencia.idEmpleado = licencia.idPersonal;
                    consultaunaLicencia.idPersonalAutoriza = licencia.idPersonalAutoriza;
                    consultaunaLicencia.idResponsableConsigna = licencia.idResponsableConsigna;
                    var idTipoLicencia = licencia.tipoLicencia.idTipoLicencia;
                    var codigoTipoLicString = "";
                    var codigo = consultaunaLicencia.codigoLicencia.Split('/');
                    var primCodigo = codigo[0].ToString();
                    if (idTipoLicencia == -1)
                    {
                        consultaunaLicencia.idTipoLicencia = 0;
                    }
                    else
                    {
                        consultaunaLicencia.idTipoLicencia = idTipoLicencia;

                        codigoTipoLicString = "/" + db.LicTipoLicencia.Find(idTipoLicencia).codigoTipoLicencia;
                    }

                    consultaunaLicencia.codigoLicencia = primCodigo.ToString() + codigoTipoLicString;


                    db.SaveChanges();
                    licenciaVm.codigoEstadoModif = 200;
                    licenciaVm.codigoLicencia = consultaunaLicencia.codigoLicencia;
                }
                else
                {
                    licenciaVm.codigoEstadoAlta = 300;
                    return licenciaVm;
                }
            }
            return licenciaVm;
        }

        public int EliminarLicencia(LicenciaVM licencia)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunaLicencia = db.LicLicencia.Where(x => x.idLicencia == licencia.idLicencia).FirstOrDefault();
                if (consultaunaLicencia != null)
                {
                    var consultaUnaNovedad = db.NovNovedad.Where(x => x.idLicencia == consultaunaLicencia.idLicencia).FirstOrDefault();
                    consultaunaLicencia.activo = false;
                    if (consultaUnaNovedad != null)//Si borro una Licencia y esta relacionada con una Novedad , borro la licencia de esa Novedad
                    {
                        consultaUnaNovedad.idLicencia = null;
                    }

                    //consultaunaLicencia.idEstadoLicencia = 4;
                    //trackeo historial 
                    //LicHistorialEstadoLic histLic = new LicHistorialEstadoLic
                    //{
                    //    idUsuario = idUsuario,
                    //    idLicencia = consultaunaLicencia.idLicencia,
                    //    fechaHistEstLic = DateTime.Now,
                    //    idEstadoLic = 4
                    //};
                    //db.LicHistorialEstadoLic.Add(histLic);
                    db.SaveChanges();
                }
                else
                {
                    return 300;
                }
            }
            return 200;
        }

        public int RecuperarLicencia(LicenciaVM licencia)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {

                var consultaunaLicencia = db.LicLicencia.Find(licencia.idLicencia);
                if (consultaunaLicencia != null)
                {
                    consultaunaLicencia.activo = true;

                    var ConsultoLicAbierta = db.LicLicenciaUsuario.Where(x => x.idLicLicencia == licencia.idLicencia).FirstOrDefault(); //Si la licencia esta para rute abiertos no la agrego
                    if (ConsultoLicAbierta == null)
                    {
                        LicLicenciaUsuario licUsuario = new LicLicenciaUsuario
                        {
                            idLicLicencia = licencia.idLicencia,
                            idUsuario = 0,
                            descripcion = licencia.descripcionLicencia

                        };
                        db.LicLicenciaUsuario.Add(licUsuario);
                    }
                    db.SaveChanges();

                }
                else
                {
                    return 300;
                }
            }
            return 200;
        }

        public bool ValidarLicencia(LicenciaVM licencia)
        {
            //habria q tener mas info para validar
            if (licencia.idPersonal != null || licencia.idPersonal != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<AutorizacionTrabajoVM> CerrarLicencia(LicenciaVM licencia)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var idLicencia = licencia.idLicencia;
                var consultaunaLicencia = db.LicLicencia.Find(idLicencia);
                List<AutorizacionTrabajoVM> autorizaciones = new List<AutorizacionTrabajoVM>();
                if (consultaunaLicencia != null)
                {
                    if (consultaunaLicencia.activo == true && consultaunaLicencia.idEstadoLicencia == 1)
                    {
                        var cuentaActivas = 0;
                        var listaAutor = db.LicAutorizacionTrabajo.Where(x =>
                            x.idLicencia == consultaunaLicencia.idLicencia && x.activo == true).ToList();
                        //aca setteo las autorizaciones abiertas o que tengan fecha posterior a la fecha de cierre de lali(cencia) ja

                        foreach (var item in listaAutor)
                        {
                            AutorizacionTrabajoVM aut = new AutorizacionTrabajoVM();
                            aut.idLicencia = item.idLicencia;
                            aut.activo = item.activo;
                            aut.descripcionAutorizacionTrabajo = item.descripcion;
                            aut.fechaHoraInicioAutorizacionTrabajo = item.fechaHoraInicio;
                            aut.fechaHoraFinAutorizacionTrabajo = item.fechaHoraFin;
                            aut.idAutorizacionTrabajo = item.idAutorizacionTrabajo;
                            aut.idUsuario = item.idUsuario;
                            var estadodb = db.LicEstadoAutorizacion.Find(item.idEstadoActual);
                            EstadoAutorizacionVM estado = new EstadoAutorizacionVM
                            {
                                idEstadoAutorizacion = estadodb.idEstadoAutorizacion,
                                nombreEstadoAutorizacion = estadodb.nombreEstadoAutorizacion
                            };
                            aut.estadoAutorizacion = estado;
                            aut.codResultadoCierre = 200;
                            aut.resultCierre = "Autorizacion ok";
                            if (item.idEstadoActual == 1)
                            {
                                aut.codResultadoCierre = 300;
                                aut.resultCierre = "Autorizacion abierta";
                                cuentaActivas += 1;

                            }
                            else if (item.idEstadoActual == 2)
                            {
                                DateTime fecha = (DateTime)licencia.fechaFin;
                                DateTime hora = (DateTime)licencia.horaFin;
                                DateTime date = fecha.AddHours(hora.Hour).AddMinutes(hora.Minute);
                                if (item.fechaHoraFin > date)
                                {
                                    aut.codResultadoCierre = 500;
                                    aut.resultCierre = "Autorizacion cerrada pero fecha excede el fin de la licencia";
                                    cuentaActivas += 1;
                                }
                            }
                            autorizaciones.Add(aut);

                        }
                        if (cuentaActivas == 0)
                        {
                            consultaunaLicencia.idEstadoLicencia = 2;
                            consultaunaLicencia.idPersonalFinaliza = licencia.idPersonalFinaliza;
                            consultaunaLicencia.idPersonalCancela = licencia.idPersonalCancela;
                            consultaunaLicencia.observacionesFinaliza = licencia.observacionesFinaliza;
                            DateTime fecha = (DateTime)licencia.fechaFin;
                            DateTime hora = (DateTime)licencia.horaFin;
                            DateTime date = fecha.AddHours(hora.Hour).AddMinutes(hora.Minute);

                            consultaunaLicencia.fechaHoraFinLicencia = date;
                            //trackeo historial 
                            LicHistorialEstadoLic histLic = new LicHistorialEstadoLic
                            {
                                idUsuario = licencia.idUsuarioModifica, //viene aca?
                                idLicencia = consultaunaLicencia.idLicencia,
                                fechaHistEstLic = DateTime.Now,
                                idEstadoLic = 2
                            };


                            db.LicHistorialEstadoLic.Add(histLic);
                            db.SaveChanges();

                            ServicioNovedad servicio = new ServicioNovedad();

                            //ver qué hacer con este estado
                            //var estadoCierreNovedad = servicio.CerrarNovedad(licencia.idNovedad);

                        }

                    }
                    else
                    {
                        AutorizacionTrabajoVM aut = new AutorizacionTrabajoVM();
                        aut.codResultadoCierre = 600;
                        aut.resultCierre = "Licencia cerrada o eliminada";
                        autorizaciones.Add(aut);
                    }

                }
                else
                {
                    AutorizacionTrabajoVM aut = new AutorizacionTrabajoVM();
                    aut.codResultadoCierre = 400;
                    aut.resultCierre = "Licencia inexistente";
                    autorizaciones.Add(aut);
                }
                return autorizaciones;
            }

        }

        public LicenciaVM ConsultarUltimoCodigoLicencia()
        {

            LicenciaVM licencia = new LicenciaVM();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var ultimaLicencia = db.LicLicencia.OrderByDescending(x => x.numLicencia).FirstOrDefault();
                DateTime dt = DateTime.Now;

                var currentYear = Int16.Parse(dt.ToString("yy"));
                var codigo = "001";

                if (ultimaLicencia != null)
                {
                    if (ultimaLicencia.anioCodigo == currentYear)
                    {
                        codigo = (ultimaLicencia.numLicencia + 1).ToString();
                        int count = codigo.Length;
                        switch (count)
                        {
                            case 1:
                                codigo = "00" + codigo;
                                break;
                            case 2:
                                codigo = "0" + codigo;
                                break;

                            default:
                                //Console.WriteLine("Default case");
                                break;
                        }
                    }

                }
                licencia.codigoLicencia = currentYear.ToString() + "-" + codigo;

            }
            return licencia;
        }

        public List<TipoLicenciaVM> ObtenerListaTipoLicencia()
        {

            using (IntranetCTREntities db = new IntranetCTREntities())
            {

                var tipoLicencia = (from t in db.LicTipoLicencia
                                    where t.activo == true
                                    select new TipoLicenciaVM
                                    {
                                        idTipoLicencia = t.idTipoLicencia,
                                        nombreTipoLicencia = t.nombreTipoLicencia

                                    }).ToList();
                return tipoLicencia;
            }

        }

        public TipoLicenciaVM ConsultarUnTipoLicencia(int? idTipoLicencia)
        {
            TipoLicenciaVM tipoLicencia = new TipoLicenciaVM();
            tipoLicencia.idTipoLicencia = -1;
            tipoLicencia.nombreTipoLicencia = "sin tipo licencia";
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                if (idTipoLicencia > -1)
                {
                    tipoLicencia = (from l in db.LicTipoLicencia
                                    where l.activo == true & l.idTipoLicencia == idTipoLicencia
                                    select new TipoLicenciaVM
                                    {
                                        idTipoLicencia = l.idTipoLicencia,
                                        nombreTipoLicencia = l.nombreTipoLicencia,

                                    }).FirstOrDefault();
                }
            }
            return tipoLicencia;
        }

        public int? ValidarEstadoLicencia(int idLicencia)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var licencia = db.LicLicencia.Find(idLicencia);
                if (licencia != null)
                {
                    return licencia.idEstadoLicencia;
                }
                else
                {
                    return 400;
                }
            }
        }
        public int VerificarCierreLicenciaNovedad(int? idNovedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunaNovedad = db.NovNovedad.Find(idNovedad);

                if (consultaunaNovedad != null)
                {
                    var licencia = db.LicLicencia.Find(consultaunaNovedad.idLicencia);
                    if (licencia != null)
                    {
                        if (licencia.idEstadoLicencia == 2)
                        {

                            return 200; // todo ok

                        }
                        else
                        {
                            return 300; // tiene licencia y no está cerrada
                        }
                    }

                    return 250; // todo ok pero no tenia licencia

                }
                else
                {
                    return 400;
                }
            }
        }
    }
}
