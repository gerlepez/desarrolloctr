﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System.Net.Http.Formatting;
using System.Web.Mvc;
using System.Web.Mvc.Routing;
namespace DesarrolloCTR.Controllers.Api
{
    public class GuardiasController : ApiController
    {
        [System.Web.Http.Route("Api/Guardias/ObtenerListaGuardia")]
        [System.Web.Http.ActionName("ObtenerListaGuardia")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaGuardia(int idMantenimiento)
        {
            ServicioGuardias servGuardia = new ServicioGuardias();

            object listaGuardia = servGuardia.ObtenerListaGuardia(idMantenimiento);
            return listaGuardia;
        }

        [System.Web.Http.Route("Api/Guardias/ObtenerGuardias")]
        [System.Web.Http.ActionName("ObtenerGuardias")]
        [System.Web.Http.HttpGet]
        public object ObtenerGuardias(string fechaDesde, string fechaHasta, string usuario, int idMantenimiento, int idGuardia)
        {
            ServicioGuardias servGuardia = new ServicioGuardias();

            ServicioUsuario serUsuario = new ServicioUsuario();
            int idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(usuario);
            
            object listaGuardia = servGuardia.ObtenerGuardias(fechaDesde, fechaHasta, idUsuarioLogueado, idMantenimiento , idGuardia);

            return listaGuardia;
        }


        [System.Web.Http.Route("Api/Guardia/VerificarUsuarioGuardia")]
        [System.Web.Http.ActionName("VerificarUsuarioGuardia")]
        [System.Web.Http.HttpGet]
        public UsuarioGuardiaVM VerificarUsuarioGuardia(string nombreUsuario)
        {
            UsuarioGuardiaVM guardia = new UsuarioGuardiaVM();
            if (nombreUsuario != null)
            {
                ServicioGuardias servGuardia = new ServicioGuardias();

                var idUsuarioLogueado = 0;
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(nombreUsuario);

                guardia = servGuardia.VerificarUsuarioGuardia(idUsuarioLogueado);
            }
            else
            {
                guardia.codigo = 400;
            }


            return guardia;
        }


        [System.Web.Http.Route("Api/Guardia/ObtenerListaMantenimiento")]
        [System.Web.Http.ActionName("ObtenerListaMantenimiento")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaMantenimiento(bool activo)
        {
            ServicioGuardias servGuardia = new ServicioGuardias();

            object listaMantenimiento = servGuardia.ObtenerListaMantenimiento(activo);

            return listaMantenimiento;
        }


        [System.Web.Http.Route("Api/Guardia/CRUD")]
        [System.Web.Http.ActionName("CRUD")]
        [System.Web.Http.HttpPost]
        public GuardiaVM CRUD(GuardiaVM guardia)
        {
            ServicioGuardias servGuardia = new ServicioGuardias();
            var idUsuarioLogueado = 0;
            if (guardia != null)
            {
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(guardia.usuario);
            }
            //aca realizar las validaciones correspondientes

            var guar = servGuardia.CRUD(guardia, idUsuarioLogueado);


            return guar;
        }


        [System.Web.Http.Route("Api/Guardia/ObtenerListaUsuariosGuardias")]
        [System.Web.Http.ActionName("ObtenerListaUsuarioCTR")]
        [System.Web.Http.HttpGet]
        public List<GuardiaVM> ObtenerListaUsuariosGuardias()
        {
            ServicioGuardias servGuardia = new ServicioGuardias();

            List<GuardiaVM> listaUsuario = servGuardia.ObtenerListaUsuariosGuardias();

            return listaUsuario;
        }


        // ↓↓↓ Personal de guardias ↓↓↓
        [System.Web.Http.Route("Api/Guardias/TablaPersonal")]
        [System.Web.Http.ActionName("TablaPersonal")]
        [System.Web.Http.HttpGet]
        public object TablaPersonal(FormDataCollection form, bool? activo)
        {
            ServicioGuardias servicioGuardias= new ServicioGuardias();
            object listaPersonal = servicioGuardias.TablaPersonal(activo);
           return listaPersonal;

        }
        /*
        [System.Web.Http.Route("Api/Guardias/AltaPersonal")]
        [System.Web.Http.ActionName("AltaPersonal")]
        [System.Web.Http.HttpPost]
        public int AltaPersona(GuardiaVM guardia)
        {
            var idUsuarioLogeado = 0;
            ServicioGuardias servicioGuardias = new ServicioGuardias();
            int codigo;
            if (servicioGuardias.ValidarPersona(guardia))
            {
                codigo = servicioGuardias.AltaPersonal(guardia);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }
        */

        [System.Web.Http.Route("Api/Guardias/ModificarPersonal")]
        [System.Web.Http.ActionName("ModificarPersonal")]
        [System.Web.Http.HttpPost]
        public int ModificarPersonal(GuardiaVM guardia)
        {

            ServicioGuardias servicioGuardias = new ServicioGuardias();
            int codigo;
            if (servicioGuardias.ValidarPersona(guardia))
            {
                codigo = servicioGuardias.ModificarPersonal(guardia);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }

        /*
        [System.Web.Http.Route("Api/Guardias/EliminarPersonal")]
        [System.Web.Http.ActionName("EliminarPersonal")]
        [System.Web.Http.HttpPost]
        public int EliminarPersonal(GuardiaVM guardia)
        {
            ServicioGuardias servicioGuardias = new ServicioGuardias();
            int codigo = servicioGuardias.EliminarPersonal(guardia);
            return codigo;
        }*/

        [System.Web.Http.Route("Api/Guardias/ConsultarUnPersonal")]
        [System.Web.Http.ActionName("ConsultarUnPersonal")]
        [System.Web.Http.HttpGet]
        public GuardiaVM ConsultarUnPersonal(int idPersonal)
        {
            ServicioGuardias servicioGuardias = new ServicioGuardias();
            GuardiaVM persona = servicioGuardias.ConsultarUnaPersona(idPersonal);
            return persona;
        }
        /*
        [System.Web.Http.Route("Api/Guardias/RecuperarPersonal")]
        [System.Web.Http.ActionName("RecuperarPersonal")]
        [System.Web.Http.HttpPost]
        public int RecuperarPersonal(GuardiaVM guardia)
        {
            ServicioGuardias servicioGuardias = new ServicioGuardias();
            int codigo = servicioGuardias.RecuperarPersonal(guardia);
            return codigo;

        }
        */
        [System.Web.Http.Route("Api/Guardias/ObtenerJefes")]
        [System.Web.Http.ActionName("ObtenerJefes")]
        [System.Web.Http.HttpGet]
        public object ObtenerJefes(bool activo)
        {
            ServicioGuardias servicioGuardias = new ServicioGuardias();
            object listGen = servicioGuardias.ObtenerJefes(activo);
            return listGen;
        }


    }

}

