﻿using System;

namespace DesarrolloCTRServicios.ViewModels
{
    public class LicenciaUsuarioVM
    {
        public int idLicLicenciaUsuario { get; set; }
        public int? idLicLicencia { get; set; }
        public int? idUsuario { get; set; }
        public string descripcion { get; set; }
        public string codigoLicencia { get; set; }
        public DateTime? fechaHoraInicioLicenciaU { get; set; }
        public string fecha { get; set; }
    }
}
