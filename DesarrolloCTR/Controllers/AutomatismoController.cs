﻿using DesarrolloCTR.Controllers.Extra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace DesarrolloCTR.Controllers
{
    [SessionUtility]
    [Permiso("Aut")]
    public class AutomatismoController : Controller
    {
        public ActionResult Automatismo()
        {
            return View();
        }
        public ActionResult AdministrarAutomatismo()
        {
            return View();
        }
        public ActionResult RelacionEquipoYEstacion()
        {
            return View();
        }
        public ActionResult AdministrarRelacion()
        {
            return View();
        }

    }
}
