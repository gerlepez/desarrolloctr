﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DesarrolloCTR.Controllers.Extra
{

    public class PermisoAttribute : ActionFilterAttribute
    {
        // GET: PermisoAttribute


        public string Code { get; set; }

        public PermisoAttribute(string code)
        {
            Code = code;
        }
        public List<GralMenu> accessList { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                base.OnActionExecuting(filterContext);

                var accessByCode = (from u in db.GralMenu
                                    where u.codigoMenu.Equals(Code) && u.activo == true
                                    select new MenuVM
                                    {
                                        idMenu = u.idMenu
                                    }).FirstOrDefault();


                var user = HttpContext.Current.Session["usuarioLogeado"];
                if (user != null && accessByCode != null)
                {
                    if (!ServicioUsuario.TienePermisoPorLogin(accessByCode.idMenu, user.ToString()))
                    {
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                        {
                            controller = "Login",
                            action = "ModuloDenegado"
                        }));
                    }
                    else
                    {
                        if (accessByCode.idMenu == 7002 && !ServicioUsuario.tienePermisoPorTurno(user.ToString()))
                        {
                            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                            {
                                controller = "Login",
                                action = "DenegacionPorTurno"
                            }));
                        }
                        else
                        {
                            var rol = ServicioUsuario.ObtenerTiposPermisos(accessByCode.idMenu, user.ToString());
                            HttpContext.Current.Session["tienePermisoEdicion"] = false;
                            HttpContext.Current.Session["tienePermisoEliminacion"] = false;
                            HttpContext.Current.Session["tienePermisoLectura"] = false;
                            if (rol != null)

                            {
                                ServicioTurno servTurno = new ServicioTurno();
                                var idUsuario = ServicioUsuario.Get(user.ToString());
                                if (servTurno.ObtenerTurnoEjecucion(idUsuario))
                                {
                                    HttpContext.Current.Session["tienePermisoEdicion"] = rol.tienePermisoEdicion;
                                    HttpContext.Current.Session["tienePermisoEliminacion"] = rol.tienePermisoEliminacion;
                                    HttpContext.Current.Session["tienePermisoLectura"] = rol.tienePermisoLectura;
                                    HttpContext.Current.Session["idRol"] = rol.idRol;
                                    HttpContext.Current.Session["turnoActivo"] = true;
                                }
                                else
                                {
                                    var TienePermisoLectura = db.GralAccesoRol.Where(x => x.idRol == rol.idRol && x.TienePermisoLectura == true && x.idMenu == accessByCode.idMenu).FirstOrDefault();
                                    var TienePermisoEdicion = db.GralAccesoRol.Where(x => x.idRol == rol.idRol && x.TienePermisoEdicion == true && x.idMenu == accessByCode.idMenu).FirstOrDefault();
                                    var TienePermisoEliminacion = db.GralAccesoRol.Where(x => x.idRol == rol.idRol && x.TienePermisoEliminacion == true && x.idMenu == accessByCode.idMenu).FirstOrDefault();

                                    if (TienePermisoLectura != null && TienePermisoLectura.TienePermisoLectura == true)
                                    {
                                        HttpContext.Current.Session["tienePermisoLectura"] = true;

                                    }
                                    else
                                    {
                                        HttpContext.Current.Session["tienePermisoLectura"] = false;

                                    }

                                    if (TienePermisoEdicion != null && TienePermisoEdicion.TienePermisoEdicion == true)
                                    {
                                        HttpContext.Current.Session["tienePermisoEdicion"] = true;
                                    }
                                    else
                                    {
                                        HttpContext.Current.Session["tienePermisoEdicion"] = false;
                                    }

                                    if (TienePermisoEliminacion != null && TienePermisoEliminacion.TienePermisoEliminacion == true)
                                    {
                                        HttpContext.Current.Session["tienePermisoEliminacion"] = false;
                                    }
                                    else
                                    {
                                        HttpContext.Current.Session["tienePermisoEliminacion"] = false;

                                    }

                                    HttpContext.Current.Session["turnoActivo"] = false;
                                    HttpContext.Current.Session["idRol"] = rol.idRol;


                                }

                            }

                        }

                    }
                }
                else if (accessByCode == null)
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                    {
                        controller = "Login",
                        action = "PermisoDenegado"
                    }));
                }

            }


        }

    }
}