﻿const boton = document.querySelector('#toggleButton');

const preferDarkScheme = window.matchMedia('(prefers-color-scheme: dark)');

const currentTheme = localStorage.getItem('theme');
console.log(currentTheme);

if (currentTheme === 'dark') {
    document.body.classList.toggle('dark-theme');
    boton.classList.toggle('active');
}
else if (currentTheme === 'light') {
    document.body.classList.toggle('light-theme');
}

boton.onclick = function () {
    boton.classList.toggle('active');
}

boton.addEventListener('click', () => {
    let colorTheme;
    //console.log('Un Clic');
    if (preferDarkScheme.matches) {
        console.log("tema negro en windows");
        document.body.classList.toggle('light-theme');
        colorTheme = document.body.classList.contains('light-theme') ? 'light' : 'dark';
    } else {
        console.log('tema blanco en windows');
        document.body.classList.toggle('dark-theme');
        colorTheme = document.body.classList.contains('dark-theme') ? 'dark' : 'light';

    }
    console.log(boton.compareDocumentPosition);

    localStorage.setItem('theme', colorTheme);

})