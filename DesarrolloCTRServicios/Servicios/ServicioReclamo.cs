﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioReclamo
    {
        IntranetCTREntities db = new IntranetCTREntities();
        public object ObtenerTipoReclamos(bool activo)
        {
            var listaReclamo = (from rec in db.RecTipoReclamo
                                where rec.activo == activo
                                select new ReclamoVM
                                {
                                    activo = rec.activo,
                                    idReclamo = rec.idTipoReclamo,
                                    nombreReclamo = rec.nombreTipoReclamo
                                }).ToList();
            object json = new { data = listaReclamo };
            return json;
        }

        public object TablaReclamo(bool? activo, string draw, int recordsTotal, int skip, int pageSize)
        {
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    ServicioUsuario usuarioservice = new ServicioUsuario();
                    IQueryable<ReclamoVM> query = (from r in db.RecTipoReclamo
                                                   where r.activo == activo
                                                   select new ReclamoVM
                                                   {
                                                       idReclamo = r.idTipoReclamo,
                                                       nombreReclamo = r.nombreTipoReclamo,
                                                       activo = r.activo,
                                                   }).OrderByDescending(x => x.idReclamo).AsQueryable();
                    var queryFiltrada = query;
                    recordsTotal = queryFiltrada.Count();
                    var lst = queryFiltrada.Skip(skip).Take(pageSize).ToList();

                    object json = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = lst };
                    return json;
                }
            }
            catch (Exception e)
            {
                return 400;
            }
        }

        public int AltaReclamo(ReclamoVM reclamo)
        {

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnReclamo = db.RecTipoReclamo.Where(x => x.idTipoReclamo == reclamo.idReclamo).FirstOrDefault();
                var ReclamoRepetido = db.RecTipoReclamo.Where(x => x.nombreTipoReclamo == reclamo.nombreReclamo).FirstOrDefault();
                if (consultaUnReclamo == null && ReclamoRepetido == null)
                {
                    try
                    {
                        RecTipoReclamo re = new RecTipoReclamo
                        {
                            nombreTipoReclamo = reclamo.nombreReclamo,
                            activo = true,
                        };
                        db.RecTipoReclamo.Add(re);
                        db.SaveChanges();
                        return 200;
                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }

            }
        }

        public int ModificarReclamo(ReclamoVM reclamo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarUnReclamo = db.RecTipoReclamo.Where(x => x.idTipoReclamo == reclamo.idReclamo).FirstOrDefault();
                if (consultarUnReclamo != null)
                {
                    consultarUnReclamo.nombreTipoReclamo = reclamo.nombreReclamo;
                    consultarUnReclamo.activo = true;
                    db.SaveChanges();
                }
            }
            return 200;

        }

        public ReclamoVM ConsultarUnReclamo(int? idReclamo)
        {
            var reclamo = (from rec in db.RecTipoReclamo
                           where rec.idTipoReclamo == idReclamo
                           select new ReclamoVM
                           {
                               activo = rec.activo,
                               nombreReclamo = rec.nombreTipoReclamo,
                               idReclamo = rec.idTipoReclamo,

                           }).FirstOrDefault();
            return reclamo;
        }

        public int EliminarReclamo(ReclamoVM reclamo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnReclamo = db.RecTipoReclamo.Where(x => x.idTipoReclamo == reclamo.idReclamo).FirstOrDefault();
                if (consultaUnReclamo != null)
                {
                    consultaUnReclamo.activo = false;
                }
                db.SaveChanges();
            }
            return 200;
        }

        public int RecuperarReclamo(ReclamoVM reclamo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnReclamo = db.RecTipoReclamo.Where(x => x.idTipoReclamo == reclamo.idReclamo).FirstOrDefault();

                if (consultaUnReclamo != null)
                {
                    consultaUnReclamo.activo = true;
                    db.SaveChanges();
                }
            }
            return 200;
        }

        public bool ValidarReclamo(ReclamoVM reclamo)
        {
            if (!string.IsNullOrWhiteSpace(reclamo.nombreReclamo.ToString()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }
}
