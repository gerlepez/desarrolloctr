﻿using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace DesarrolloCTR.Controllers.Api
{
    public class LineaApiController : ApiController
    {

        [System.Web.Http.Route("Api/Linea/ObtenerListaLinea")]
        [System.Web.Http.ActionName("ObtenerListaLinea")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaLinea(bool activo)
        {
            ServicioLinea servLinea = new ServicioLinea();
            object listaLinea = servLinea.ObtenerListaLinea(activo);
            return listaLinea;
        }

        /*
        [System.Web.Http.Route("Api/Linea/TablaLinea")]
        [System.Web.Http.ActionName("TablaLinea")]
        [System.Web.Http.HttpPost]
        public object TablaLinea(FormDataCollection form ,bool activo)
        {
            var draw = form.Get("draw");
            var start = form.Get("start");
            var length = form.Get("length");
            var sortColumn = (form.Get("columns[" + form.Get("order[0][column]").FirstOrDefault() + "][data]").ToString()).ToString();
            var sortColumnDir = form.Get("order[0][dir]").ToString();
            var searchValue = form.Get("search[value]").ToString();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            int cantidadRegistrosPagina = Int32.Parse(length);

            ServicioLinea servLinea = new ServicioLinea();

            object listaLinea = servLinea.TablaLinea(activo,draw, recordsTotal, skip, pageSize);
            return listaLinea;
        }
        */
        [System.Web.Http.Route("Api/Linea/ObtenerListaTipoLinea")]
        [System.Web.Http.ActionName("ObtenerListaTipoLinea")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaTipoLinea(bool activo)
        {
            ServicioLinea servLinea = new ServicioLinea();

            object listaLinea = servLinea.ObtenerListaTipoLinea();

            //((IDisposable)servLinea).Dispose();

            return listaLinea;
        }

        [System.Web.Http.Route("Api/Linea/ObtenerLineaPorTipoPropiedad")]
        [System.Web.Http.ActionName("ObtenerLineaPorTipoPropiedad")]
        [System.Web.Http.HttpGet]
        public object ObtenerLineaPorTipoPropiedad(int idTipoLinea, int tipoPropiedad, int idNovedad = 0)
        {
            ServicioLinea servLinea = new ServicioLinea();

            object listaLinea = servLinea.ObtenerLineaPorTipoPropiedad(idTipoLinea, tipoPropiedad, idNovedad);

            //((IDisposable)servLinea).Dispose();

            return listaLinea;
        }

        [System.Web.Http.Route("Api/Linea/AltaLinea")]
        [System.Web.Http.ActionName("AltaLinea")]
        [System.Web.Http.HttpPost]
        public int AltaLinea(LineaCrudVM linea)
        {
            var idUsuarioLogueado = 0;
            if (linea != null)
            {
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(linea.nombresuario);
            }
            ServicioLinea servLinea = new ServicioLinea();
            int codigo;
            if (servLinea.ValidarAltaLinea(linea))
            {

                codigo = servLinea.AltaLinea(linea, idUsuarioLogueado);
            }
            else
            {
                codigo = 400;
            }

            //((IDisposable)servLinea).Dispose();

            return codigo;
        }

        [System.Web.Http.Route("Api/Linea/ModificarLinea")]
        [System.Web.Http.ActionName("ModificarLinea")]
        [System.Web.Http.HttpPost]
        public int ModificarLinea(LineaCrudVM linea)
        {

            var idUsuarioLogueado = 0;
            if (linea != null)
            {
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(linea.nombresuario);
            }
            ServicioLinea servLinea = new ServicioLinea();
            int codigo;
            if (servLinea.ValidarAltaLinea(linea))
            {
                codigo = servLinea.ModificarLinea(linea, idUsuarioLogueado);
            }
            else
            {
                codigo = 400;
            }

            //((IDisposable)servLinea).Dispose();

            return codigo;
        }

        [System.Web.Http.Route("Api/Linea/EliminarLinea")]
        [System.Web.Http.ActionName("EliminarLinea")]
        [System.Web.Http.HttpPut]
        public int EliminarLinea(EliminarLineaVM objLinea)
        {
            ServicioLinea servLinea = new ServicioLinea();
            int codigo = servLinea.EliminarLinea(objLinea);
            return codigo;
        }

        [System.Web.Http.Route("Api/Linea/ConsultarUnaLinea")]
        [System.Web.Http.ActionName("ConsultarUnaLinea")]
        [System.Web.Http.HttpGet]
        public LineaVM ConsultarUnaLinea(int idLinea)
        {
            ServicioLinea servLinea = new ServicioLinea();
            LineaVM linea = servLinea.ConsultarUnaLinea(idLinea);

            //((IDisposable)servLinea).Dispose();

            return linea;
        }

        [System.Web.Http.Route("Api/Linea/RecuperarLinea")]
        [System.Web.Http.ActionName("RecuperarLinea")]
        [System.Web.Http.HttpPost]
        public int RecuperarLinea(EliminarLineaVM linea)
        {
            ServicioLinea servLinea = new ServicioLinea();
            int codigo = servLinea.RecuperarLinea(linea);

            //((IDisposable)servLinea).Dispose();

            return codigo;
        }

        [System.Web.Http.Route("Api/Linea/ConsultarIndisponibilidadLinea")]
        [System.Web.Http.ActionName("ConsultarIndisponibilidadLinea")]
        [System.Web.Http.HttpGet]
        public object ConsultarIndisponibilidadLinea(int idLinea, int idNovedad)
        {
            ServicioLinea servLinea = new ServicioLinea();
            var indisponibilidad = servLinea.ConsultarIndisponibilidadLinea(idLinea, idNovedad);

            //((IDisposable)servLinea).Dispose();

            return indisponibilidad;
        }


        [System.Web.Http.Route("Api/Linea/ConsultarPropiedadLinea")]
        [System.Web.Http.ActionName("ConsultarPropiedadLinea")]
        [System.Web.Http.HttpGet]
        public object ConsultarPropiedadLinea(int? idLinea)
        {
            ServicioLinea servicioLinea = new ServicioLinea();
            object lienea = servicioLinea.ConsultarPropiedadLinea(idLinea);
            return lienea;
        }


        [System.Web.Http.Route("Api/Linea/ObtenerListaPropiedades")]
        [System.Web.Http.ActionName("ObtenerListaPropiedades")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaPropiedades()
        {
            ServicioLinea servLinea = new ServicioLinea();
            object listaLinea = servLinea.ObtenerListaPropiedades();
            return listaLinea;
        }



    }
}
