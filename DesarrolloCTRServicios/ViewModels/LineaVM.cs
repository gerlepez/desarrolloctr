﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class LineaVM
    {
        public int? idLinea { get; set; }
        public int? idPagotran { get; set; }
        public int? idEquipo { get; set; }
        public int? idUsarioAlta { get; set; }
        public int? idTipoLinea { get; set; }
        public int? nroTerna { get; set; }
        public decimal? kilometraje { get; set; }
        public int? idPropiedad { get; set; }
        public bool? activo { get; set; }
        public bool? esPropia { get; set; }

        public bool? remunerada { get; set; }
        public string esRemunerada { get; set; }

        public string nombreEquipo { get; set; }
        public string nombreLinea { get; set; }
        public string nombresuario { get; set; }
        public string nombreTipoLinea { get; set; }
        public string codigoLinea { get; set; }
        public string TipoLinea { get; set; }
        public string PropiedadLinea { get; set; }
        public string nombrePropiedad { get; set; }
        public string km { get; set; }
        public EstacionVM estacionOrigen { get; set; }
        public EstacionVM estacionDestino { get; set; }
        public TipoLineaVM tipoLinea { get; set; }
        public TensionVM tension { get; set; }

    }

    public class EliminarLineaVM
    {
        public int? idLinea { get; set; }
    }

}
