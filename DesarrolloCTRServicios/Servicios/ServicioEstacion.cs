﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;


namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioEstacion : IDisposable
    {
        private IntranetCTREntities db = new IntranetCTREntities();
        private ServicioLinea _servLinea = new ServicioLinea();
        public object ObtenerListaEstacion(bool activo)
        {
            List<EstacionVM> listaEstacion;

            listaEstacion = (from c in db.EstEstacion
                             join d in db.EstEstadoEstacion on c.idEstadoActual equals d.idEstadoEstacion
                             join e in db.EstTipoEstacion on c.tipoEstacion equals e.idTipoEstacion
                             join p in db.ProPropiedad on c.propiedad equals p.idPropiedad
                             join a in db.EquiEquipoAdicionales on c.idEquipoAdicional equals a.idEquipoAdicional into ea
                             from a in ea.DefaultIfEmpty()
                             where c.activo == activo
                             select new EstacionVM
                             {
                                 idEstacion = c.idEstacion,
                                 nombreEstacion = e.siglaTipo + " " + c.nombreEstacion,
                                 idEquipo = c.idEstacion,
                                 nombreEquipo = c.nombreEstacion,
                                 fechaUsarioAlta = c.fechaUsuarioAlta,
                                 idUsarioAlta = c.idUsuarioAlta,
                                 codigoEstacion = c.codigoEstacion,
                                 activo = c.activo,
                                 nombreEquipoAdicional = a.equipoAdicional,
                                 estadoEstacion = new EstadoEstacionVM
                                 {
                                     idEstadoEstacion = d.idEstadoEstacion,
                                     nombreEstadoEstacion = d.nombreEstadoEstacion
                                 },
                                 propiedad = new PropiedadEstacionVM
                                 {
                                     idPropiedad = p.idPropiedad,
                                     nombrePropiedad = p.nombrePropiedad
                                 },
                                 tipoEstacion = new TipoEstacionVM
                                 {
                                     idTipoEstacion = e.idTipoEstacion,
                                     nombreTipoEstacion = e.nombreTipoEstacion
                                 }


                             }).ToList();

            //COMENTADO POR FRANCO
            //EstacionVM estacion = new EstacionVM {
            //    idEstacion = 1,
            //    nombreEstacion = "Seleccionar una opción"
            //};
            //listaEstacion.Add(estacion);
            listaEstacion = listaEstacion.OrderByDescending(x => x.idEstacion).ToList();
            object json = new { data = listaEstacion };
            return json;
        }
        public object ObtenerListaEstacionDistroCuyo(bool activo)
        {
            List<EstacionVM> listaEstacion;

            listaEstacion = (from c in db.EstEstacion

                             join d in db.EstEstadoEstacion on c.idEstadoActual equals d.idEstadoEstacion
                             join e in db.EstTipoEstacion on c.tipoEstacion equals e.idTipoEstacion
                             join p in db.ProPropiedad on c.propiedad equals p.idPropiedad
                             where c.activo == activo
                             select new EstacionVM
                             {
                                 idEstacion = c.idEstacion,
                                 nombreEstacion = e.siglaTipo + " " + c.nombreEstacion,
                                 idEquipo = c.idEstacion,
                                 nombreEquipo = c.nombreEstacion,
                                 fechaUsarioAlta = c.fechaUsuarioAlta,
                                 idUsarioAlta = c.idUsuarioAlta,
                                 codigoEstacion = c.codigoEstacion,
                                 activo = c.activo,

                                 estadoEstacion = new EstadoEstacionVM
                                 {
                                     idEstadoEstacion = d.idEstadoEstacion,
                                     nombreEstadoEstacion = d.nombreEstadoEstacion
                                 },
                                 propiedad = new PropiedadEstacionVM
                                 {
                                     idPropiedad = p.idPropiedad,
                                     nombrePropiedad = p.nombrePropiedad
                                 },
                                 tipoEstacion = new TipoEstacionVM
                                 {
                                     idTipoEstacion = e.idTipoEstacion,
                                     nombreTipoEstacion = e.nombreTipoEstacion
                                 }


                             }).ToList();

            listaEstacion = listaEstacion.OrderBy(x => x.idEstacion).ToList();
            object json = new { data = listaEstacion };
            return json;
        }

        public object ObtenerEstacionPorInfra(int idInfraestructura)
        {
            List<EstacionVM> listaEstacion;

            listaEstacion = (from c in db.EstEstacion
                             join d in db.EstEstadoEstacion on c.idEstadoActual equals d.idEstadoEstacion
                             join e in db.EstTipoEstacion on c.tipoEstacion equals e.idTipoEstacion
                             join p in db.ProPropiedad on c.propiedad equals p.idPropiedad
                             join ti in db.RecTipoInfraEstacion on c.idEstacion equals ti.idEstacion
                             where c.activo == true && idInfraestructura == ti.idTipoInfra
                             select new EstacionVM
                             {
                                 idEstacion = c.idEstacion,
                                 nombreEstacion = e.siglaTipo + " " + c.nombreEstacion,
                                 fechaUsarioAlta = c.fechaUsuarioAlta,
                                 idUsarioAlta = c.idUsuarioAlta,
                                 codigoEstacion = c.codigoEstacion,
                                 activo = c.activo,

                                 estadoEstacion = new EstadoEstacionVM
                                 {
                                     idEstadoEstacion = d.idEstadoEstacion,
                                     nombreEstadoEstacion = d.nombreEstadoEstacion
                                 },
                                 propiedad = new PropiedadEstacionVM
                                 {
                                     idPropiedad = p.idPropiedad,
                                     nombrePropiedad = p.nombrePropiedad
                                 },
                                 tipoEstacion = new TipoEstacionVM
                                 {
                                     idTipoEstacion = e.idTipoEstacion,
                                     nombreTipoEstacion = e.nombreTipoEstacion
                                 }


                             }).ToList();


            listaEstacion = listaEstacion.OrderBy(x => x.idEstacion).ToList();
            object json = new { data = listaEstacion };
            return json;
        }

        public object ObtenerEstacionPorAutomatismo(int idTipoAutomatismo)
        {
            List<EstacionVM> listaLugar = new List<EstacionVM>();
            //buscar los equipos para isla ypf
            if (idTipoAutomatismo == 1 || idTipoAutomatismo == 2)
            {
                listaLugar = (from c in db.CConCentral
                              where c.activo == true && c.idZonaAutomatismo == idTipoAutomatismo
                              select new EstacionVM
                              {
                                  idEstacion = c.idCentral,
                                  nombreEstacion = c.nombreCentral,
                                  fechaUsarioAlta = c.fechaUsuarioAlta,
                                  idUsarioAlta = c.idUsuarioAlta,
                                  //codigoEstacion = c.codigoEstacion,
                                  activo = c.activo
                              }).ToList();
            }
            else if (idTipoAutomatismo != 3)
            {
                listaLugar = (from c in db.EstEstacion
                              join d in db.EstEstadoEstacion on c.idEstadoActual equals d.idEstadoEstacion
                              join e in db.EstTipoEstacion on c.tipoEstacion equals e.idTipoEstacion
                              join p in db.ProPropiedad on c.propiedad equals p.idPropiedad
                              join ti in db.AutAutomatismoEstacion on c.idEstacion equals ti.idEstacion
                              where c.activo == true && ti.idAutomatismo == idTipoAutomatismo
                              select new EstacionVM
                              {
                                  idEstacion = c.idEstacion,
                                  nombreEstacion = e.siglaTipo + " " + c.nombreEstacion,
                                  fechaUsarioAlta = c.fechaUsuarioAlta,
                                  idUsarioAlta = c.idUsuarioAlta,
                                  codigoEstacion = c.codigoEstacion,
                                  activo = c.activo,

                                  estadoEstacion = new EstadoEstacionVM
                                  {
                                      idEstadoEstacion = d.idEstadoEstacion,
                                      nombreEstadoEstacion = d.nombreEstadoEstacion
                                  },
                                  propiedad = new PropiedadEstacionVM
                                  {
                                      idPropiedad = p.idPropiedad,
                                      nombrePropiedad = p.nombrePropiedad
                                  },
                                  tipoEstacion = new TipoEstacionVM
                                  {
                                      idTipoEstacion = e.idTipoEstacion,
                                      nombreTipoEstacion = e.nombreTipoEstacion
                                  }


                              }).ToList();

            }

            listaLugar = listaLugar.OrderBy(x => x.idEstacion).ToList();
            object json = new { data = listaLugar };
            return json;
        }
        public int AltaEstacion(EstacionVM estacion, int idUsuario)
        {
            var EstacionRepetida = db.EstEstacion.Where(x => x.nombreEstacion == estacion.nombreEstacion && x.tipoEstacion == estacion.tipoEstacion.idTipoEstacion && x.propiedad == estacion.propiedad.idPropiedad).FirstOrDefault();

            var estac = db.EstEstacion.Where(x => x.idEstacion == estacion.idEstacion).FirstOrDefault();
            if (estac == null && EstacionRepetida == null)
            {
                try
                {
                    EstEstacion est = new EstEstacion
                    {
                        nombreEstacion = estacion.nombreEstacion,
                        activo = true,
                        codigoEstacion = estacion.codigoEstacion,
                        idUsuarioAlta = idUsuario,
                        fechaCreacion = DateTime.Now,
                        idEstadoActual = 1,
                        propiedad = estacion.propiedad.idPropiedad,
                        tipoEstacion = estacion.tipoEstacion.idTipoEstacion,
                        idEquipoAdicional = estacion.idEquipoAdicional,
                    };
                    db.EstEstacion.Add(est);
                    db.SaveChanges();

                    EstHistorialEstacion historialEst = new EstHistorialEstacion
                    {
                        idEstacion = est.idEstacion,
                        activo = true,
                        fechaCambioEstado = est.fechaUsuarioAlta,
                        idEstadoEstacion = 1,
                        idUsuario = est.idUsuarioAlta,

                    };

                    db.EstHistorialEstacion.Add(historialEst);
                    db.SaveChanges();
                    return 200;

                }
                catch (Exception e)
                {
                    return 404;
                }
            }
            else
            {
                return 300;
            }

        }

        public int ModificarEstacion(EstacionVM estacion, int idUsuario)
        {

            var est = db.EstEstacion.Where(x => x.idEstacion == estacion.idEstacion).FirstOrDefault();
            if (est != null)
            {
                est.nombreEstacion = estacion.nombreEstacion;
                est.activo = true;
                est.codigoEstacion = estacion.codigoEstacion;
                est.idUsuarioModifica = idUsuario;
                est.fechaUsuarioModifica = DateTime.Now;
                est.tipoEstacion = estacion.tipoEstacion.idTipoEstacion;
                est.propiedad = estacion.propiedad.idPropiedad;
                est.fechaEdicion = DateTime.Now;
                est.idEquipoAdicional = estacion.idEquipoAdicional;
                db.SaveChanges();

                var lineasAModificar = (from linea in db.LinLinea
                                        join tipoLinea in db.LinTipoLinea on linea.idTipoLinea equals tipoLinea.idTipoLinea
                                        join estacionDestino in db.EstEstacion on linea.idEstacionDestino equals estacionDestino.idEstacion
                                        join estacionOrigen in db.EstEstacion on linea.idEstacionOrigen equals estacionOrigen.idEstacion
                                        join tension in db.GralTension on linea.tension equals tension.idTension
                                        where linea.idEstacionDestino == est.idEstacion && linea.activo == true || linea.idEstacionOrigen == est.idEstacion && linea.activo == true
                                        select new LineaCrudVM
                                        {
                                            idLinea = linea.idLinea,
                                            idTipoLinea = linea.idTipoLinea,
                                            idPagotran = linea.idPagotran,
                                            idTension = linea.tension,
                                            tension = (string)tension.tension,
                                            idEstacionOrigen = linea.idEstacionOrigen,
                                            idEstacionDestino = linea.idEstacionDestino,
                                            nroTerna = linea.nroTerna,
                                            kilometraje = linea.kilometraje,
                                            idPropiedad = linea.EsPropia,
                                            nombreEstacionDestino = (string)estacionDestino.nombreEstacion,
                                            nombreEstacionOrigen = (string)estacionOrigen.nombreEstacion,
                                            remunerada = linea.remunerado
                                        }).Distinct().ToList();
                if(lineasAModificar.Count >= 0 && lineasAModificar != null)
                {
                    foreach(var linea in lineasAModificar)
                    {
                        var lineasModificadas = _servLinea.ModificarLinea(linea, idUsuario);
                    }
                }
            }

            return 200;
        }

        public int EliminarEstacion(EstacionVM estacion)
        {

            var consultaCapacitor = db.CapCapacitor.Where(x => x.idEstacion == estacion.idEstacion).FirstOrDefault();
            var consultaLineaOrigen = db.LinLinea.Where(x => x.idEstacionOrigen == estacion.idEstacion).FirstOrDefault();
            var consultaLineaDestino = db.LinLinea.Where(x => x.idEstacionDestino == estacion.idEstacion).FirstOrDefault();
            var consultaTransformador = db.TranTransformador.Where(x => x.idEstacion == estacion.idEstacion).FirstOrDefault();
            var consultaNovedad = db.NovEstacion.Where(x => x.idEstacion == estacion.idEstacion).FirstOrDefault();
            var consultaPuntoConexion = db.PCPuntoConexion.Where(x => x.idEstacion == estacion.idEstacion).FirstOrDefault();

            if (consultaPuntoConexion == null && consultaCapacitor == null & consultaLineaOrigen == null & consultaLineaDestino == null & consultaTransformador == null & consultaNovedad == null)
            {
                var est = db.EstEstacion.Where(x => x.idEstacion == estacion.idEstacion).FirstOrDefault();
                if (est != null)
                {
                    est.activo = false;
                }
                db.SaveChanges();
                return 200;
            }
            else
            {
                return 300; //no se puede eliminar
            }

        }

        public bool ValidarEstacion(EstacionVM estacion)
        {
            if (estacion != null)
            {
                if (!string.IsNullOrWhiteSpace(estacion.nombreEstacion) &&
                estacion.nombreEstacion.Count() <= 100)

                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        public EstacionVM ConsultarUnEstacion(int? idEstacion)
        {
            EstacionVM estacion = new EstacionVM();
            estacion = (from c in db.EstEstacion
                        join d in db.EstEstadoEstacion on c.idEstadoActual equals d.idEstadoEstacion
                        join e in db.EstTipoEstacion on c.tipoEstacion equals e.idTipoEstacion
                        join p in db.ProPropiedad on c.propiedad equals p.idPropiedad
                        where c.activo == true && c.idEstacion == idEstacion
                        select new EstacionVM
                        {
                            idEstacion = c.idEstacion,
                            nombreEstacion = c.nombreEstacion,
                            idEquipo = c.idEstacion,
                            nombreEquipo = c.nombreEstacion,
                            fechaUsarioAlta = c.fechaUsuarioAlta,
                            idUsarioAlta = c.idUsuarioAlta,
                            codigoEstacion = c.codigoEstacion,
                            activo = c.activo,
                            idEquipoAdicional = c.idEquipoAdicional,
                            estadoEstacion = new EstadoEstacionVM
                            {
                                idEstadoEstacion = d.idEstadoEstacion,
                                nombreEstadoEstacion = d.nombreEstadoEstacion
                            },
                            propiedad = new PropiedadEstacionVM
                            {
                                idPropiedad = p.idPropiedad,
                                nombrePropiedad = p.nombrePropiedad
                            },
                            tipoEstacion = new TipoEstacionVM
                            {
                                idTipoEstacion = e.idTipoEstacion,
                                nombreTipoEstacion = e.nombreTipoEstacion
                            }
                        }).FirstOrDefault();

            return estacion;
        }

        public void Dispose()
        {
            //db2.Dispose();
        }

        public int RecuperarEstacion(EstacionVM estacion)
        {
            var est = db.EstEstacion.Where(x => x.idEstacion == estacion.idEstacion).FirstOrDefault();
            if (est != null)
            {
                est.activo = true;
            }
            db.SaveChanges();

            return 200;
        }

        public object ObtenerListaPropiedadEstacion()
        {
            List<PropiedadEstacionVM> propiedadEstacion;

            propiedadEstacion = (from c in db.EstPropiedadEstacion
                                 where c.activo == true
                                 select new PropiedadEstacionVM
                                 {
                                     idPropiedad = c.idPropiedad,
                                     nombrePropiedad = c.nombrePropiedad,
                                     activo = c.activo
                                 }).ToList();

            object json = new { data = propiedadEstacion };

            return json;

        }

        public object ObtenerListaTipoEstacion()
        {
            List<TipoEstacionVM> obtenerListatipoEstacion;

            obtenerListatipoEstacion = (from c in db.EstTipoEstacion
                                        where c.activo == true
                                        select new TipoEstacionVM
                                        {
                                            idTipoEstacion = c.idTipoEstacion,
                                            nombreTipoEstacion = c.nombreTipoEstacion,
                                            activo = c.activo,
                                        }).ToList();

            object json = new { data = obtenerListatipoEstacion };

            return json;
        }

    }
}



