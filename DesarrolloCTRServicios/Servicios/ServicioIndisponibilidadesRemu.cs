﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using DesarrolloCTRUtilidades;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioIndisponibilidadesRemu
    {
        public object ObtenerEquipos(bool? activo)
        {
            List<EquipoVM> listaEquipos;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                listaEquipos = (from e in db.EquiEquipo
                                where e.activo == true && e.idEquipo == 4 || e.idEquipo == 1 || e.idEquipo == 7 || e.idEquipo == 2
                                select new { e }).AsEnumerable().Select(x => new EquipoVM
                                {
                                    idEquipo = x.e.idEquipo,
                                    nombreEquipo = x.e.nombreEquipo,
                                    codigoEquipo = x.e.codigoEquipo,
                                }).ToList();

                //var automatismo = new EquipoVM();
                //automatismo.idEquipo = 10;
                //automatismo.nombreEquipo = "Mes";
                //automatismo.codigoEquipo = "mes";  //quitado para que no aparezca el mes en el select de los equipos

                //listaEquipos.Add(automatismo);
            }
            object json = new { data = listaEquipos };
            return json;
        }

        public IQueryable<IndisRemuEquipoVM> FiltrarQuery(IQueryable<IndisRemuEquipoVM> query, string fechaDesde, string fechaHasta)
        {
            if (fechaDesde != null && fechaHasta != null)
            {
                var fechaDateDesde = DateTime.Parse(fechaDesde);
                var fechaDateHasta = DateTime.Parse(fechaHasta);
                var lst = query.ToList();
                query = query.Where(x => x.fechaSalida <= fechaDateHasta && x.fechaSalida >= fechaDateDesde);
                lst = query.ToList();
            }
            return query;
        }


        #region Region Lineas

        public object obtenerLineas(int idResolucion, string fecha)
        {
            List<IndisRemuEquipoVM> listaLinea = new List<IndisRemuEquipoVM>();
            if (/*idResolucion > 0 &&*/ fecha != null)
            {
                DateTime fechaParse = DateTime.Parse(fecha);
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    if (idResolucion == 0)
                    {
                        listaLinea = (from l in db.LinLinea
                                      join g in db.GralTension on l.tension equals g.idTension
                                      join r in db.IndisponibilidadRemuLineas on l.idLinea equals r.idLinea into remu
                                      from r in remu.DefaultIfEmpty()
                                      where l.activo == true && l.remunerado == true && r.Anio == fechaParse.Year && r.Mes == fechaParse.Month
                                      select new IndisRemuEquipoVM
                                      {
                                          idlinea = l.idLinea,
                                          idLineaIndisRemu = r.idLineaIndisRemu,
                                          idPagotran = l.idPagotran,
                                          cammesa = l.nombreLinea,
                                          Ukv = g.tension,
                                          longKm = l.kilometraje,
                                          remunerado = r.remunerado,
                                          cargoRealxHora = r.cargoRealxHoras,
                                          muestroRealxHora = r.cargoRealxHoras != null ? "$ " + r.cargoRealxHoras : " ",
                                          muestroxMes = r.xMes != null ? "$ " + r.xMes : " ",
                                          xMes = r.xMes,
                                          origenRegulatorio = r.origenRegulatorio,
                                          activo = r.activo,
                                      }).ToList();
                    }
                    else
                    {
                        listaLinea = (from l in db.LinLinea
                                      join g in db.GralTension on l.tension equals g.idTension
                                      join r in db.IndisponibilidadRemuLineas on l.idLinea equals r.idLinea into remu
                                      from r in remu.DefaultIfEmpty()
                                      where l.activo == true && l.remunerado == true && r.idResolucion == idResolucion
                                      && r.Anio == fechaParse.Year && r.Mes == fechaParse.Month
                                      select new IndisRemuEquipoVM
                                      {
                                          idlinea = l.idLinea,
                                          idLineaIndisRemu = r.idLineaIndisRemu,
                                          idPagotran = l.idPagotran,
                                          cammesa = l.nombreLinea,
                                          Ukv = g.tension,
                                          longKm = l.kilometraje,
                                          remunerado = r.remunerado,
                                          cargoRealxHora = r.cargoRealxHoras,
                                          muestroRealxHora = r.cargoRealxHoras != null ? "$ " + r.cargoRealxHoras : " ",
                                          muestroxMes = r.xMes != null ? "$ " + r.xMes : " ",
                                          xMes = r.xMes,
                                          origenRegulatorio = r.origenRegulatorio,
                                          activo = r.activo,
                                      }).ToList();
                    }


                    foreach (var item in listaLinea)
                    {
                        var fechaHoraActual = DateTime.Now;
                        item.indisponible = true;
                        var consultaLineaIndisponible = db.IndisponibilidadLineas.Where(x => x.IdLineaIndisRemu == item.idLineaIndisRemu && x.activo == true && (x.fechaEntrada >= fechaHoraActual || x.fechaEntrada == null)).FirstOrDefault();
                        if (consultaLineaIndisponible != null)
                        {
                            item.indisponible = false;
                            db.SaveChanges();
                        }
                        db.SaveChanges();
                    }
                }
            }

            var listaOrdenada = listaLinea.OrderByDescending(x => x.idlinea);
            object json = new { data = listaOrdenada };
            return json;
        }

        public IndisRemuEquipoVM ConsultarLineaRemunerada(int? idLineaIndisRemu)
        {
            IndisRemuEquipoVM linea = new IndisRemuEquipoVM();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                linea = (from l in db.IndisponibilidadRemuLineas
                         where l.activo == true && l.idLineaIndisRemu == idLineaIndisRemu
                         select new IndisRemuEquipoVM
                         {
                             remunerado = l.remunerado,
                             origenRegulatorio = l.origenRegulatorio,
                         }).FirstOrDefault();
            }
            return linea;
        }

        public int ModificarLinea(IndisRemuEquipoVM linea)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunLinea = db.IndisponibilidadRemuLineas.Where(x => x.idLineaIndisRemu == linea.idLineaIndisRemu).FirstOrDefault();
                var lineaR = db.LinLinea.Where(x => x.idLinea == consultaunLinea.idLinea && x.activo == true).FirstOrDefault();
                var kv = db.GralTension.Where(x => x.activo == true && x.idTension == lineaR.tension).FirstOrDefault();
                var UkvInt = Convert.ToInt32(kv.tension);
                var monto = db.IndisponibilidadMontoLineas.Where(x => x.activo == true && x.kv == UkvInt).FirstOrDefault();

                if (monto != null)
                {
                    if (consultaunLinea != null)
                    {
                        if (kv.tension == "220" || kv.tension == "132")
                        {
                            consultaunLinea.cargoRealxHoras = (monto.monto * lineaR.kilometraje * linea.remunerado / 100) / 100;
                            var fechaHoy = new DateTime();
                            var añoHoy = fechaHoy.Year;
                            var mesHoy = fechaHoy.Month;

                            int cantidadDias = DateTime.DaysInMonth(fechaHoy.Year, fechaHoy.Month);
                            consultaunLinea.xMes = consultaunLinea.cargoRealxHoras * (cantidadDias * 24);
                            consultaunLinea.remunerado = linea.remunerado;
                            consultaunLinea.origenRegulatorio = linea.origenRegulatorio;
                            db.SaveChanges();

                        }

                    }

                }
                else
                {
                    return 300;
                }

            }
            return 200;
        }

        //Montos Lineas

        public Object ObtenerMontosLineas()
        {
            List<MontoIndisponibilidadesVM> montoL;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                montoL = (from m in db.IndisponibilidadMontoLineas
                          where m.activo == true
                          select new MontoIndisponibilidadesVM
                          {

                              idIndisponibilidadMontos = m.idIndisponibilidadMontos,
                              tipoEquipo = m.tipoEquipo,
                              nombreMonto = m.nombreMonto,
                              monto = m.monto,
                              mostrarMonto = "$ " + m.monto,
                              kv = m.kv,
                              activo = m.activo,
                              fecha = m.fechaMonto,
                              resolucion = m.resolucion,
                          }).ToList();
            }
            object json = new { data = montoL };
            return json;

        }
        public Object ObtenerMontosLineasTodos()
        {
            List<MontoIndisponibilidadesVM> montoL;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                montoL = (from m in db.IndisponibilidadMontoLineas
                          where m.activoHistorial == true
                          select new MontoIndisponibilidadesVM
                          {

                              idIndisponibilidadMontos = m.idIndisponibilidadMontos,
                              tipoEquipo = m.tipoEquipo,
                              nombreMonto = m.nombreMonto,
                              monto = m.monto,
                              mostrarMonto = "$ " + m.monto,
                              kv = m.kv,
                              activo = m.activo,
                              fecha = m.fechaMonto,
                              resolucion = m.resolucion,
                          }).ToList();
            }
            object json = new { data = montoL };
            return json;

        }

        public object ObtenerTensionLinea()
        {
            List<TensionVM> traerListaTension;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                traerListaTension = (from c in db.GralTension
                                     where c.activo == true && (c.idTension == 4 || c.idTension == 5)
                                     select new TensionVM
                                     {
                                         idTension = c.idTension,
                                         tension = c.tension,
                                         activo = c.activo
                                     }).ToList();
            }
            object json = new { data = traerListaTension };

            return json;
        }

        public int AltaMontoLinea(MontoIndisponibilidadesVM monto)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var kvString = db.GralTension.Where(x => x.idTension == monto.kv).FirstOrDefault();

                var kvInt = int.Parse(kvString.tension);
                //var montoRepetido = db.IndisponibilidadMontoLineas.Where(x => x.kv == kvInt && x.activo == true).FirstOrDefault();
                var idResolucion = 0;
                var consultaUnMonto = db.IndisponibilidadMontoLineas.Where(x => x.kv == kvInt && x.activo == true).FirstOrDefault();
                if (consultaUnMonto != null /*&& montoRepetido == null*/)
                {
                    consultaUnMonto.activo = false;
                }
                try
                {
                    IndisponibilidadMontoLineas mon = new IndisponibilidadMontoLineas
                    {
                        tipoEquipo = 4,
                        monto = monto.monto,
                        activo = true,
                        kv = kvInt,
                        fechaMonto = monto.fechaDesde,
                        resolucion = monto.resolucion,
                        activoHistorial = true
                    };
                    db.IndisponibilidadMontoLineas.Add(mon);



                    db.SaveChanges();
                    idResolucion = mon.idIndisponibilidadMontos;
                    var fechaHastaRes = monto.fechaHasta?.Date ?? DateTime.Today.Date;
                    while (monto.fechaDesde.Value.Date <= fechaHastaRes)
                    {
                        var lineas = db.LinLinea.Where(x => x.tension == monto.kv /*&& x.activo == true*/ && x.remunerado == true && x.fechaCreacion <= fechaHastaRes).ToList();
                        foreach (var item in lineas) //MODIFICO EL CARGO DE LAS LINEAS QUE TIENEN ASIGNADO ESE KV
                        {
                            var fechaHoy = monto.fechaDesde; /*var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;*/
                            var lineaRemunerada = db.IndisponibilidadRemuLineas.Where(x => x.idLinea == item.idLinea && x.activo == true && x.idResolucion == idResolucion && x.Mes == monto.fechaDesde.Value.Month && x.Anio == monto.fechaDesde.Value.Year).FirstOrDefault();

                            int cantidadDias = DateTime.DaysInMonth(fechaHoy.Value.Year, fechaHoy.Value.Month);

                            if (lineaRemunerada != null)
                            {
                                var nuevaLineaRemunerada = new IndisponibilidadRemuLineas
                                {
                                    activo = true,
                                    cargoRealxHoras = (monto.monto * item.kilometraje * lineaRemunerada.remunerado / 100) / 100,
                                    idLinea = item.idLinea,
                                    idResolucion = mon.idIndisponibilidadMontos,
                                    origenRegulatorio = lineaRemunerada.origenRegulatorio,
                                    remunerado = lineaRemunerada.remunerado,
                                    xMes = ((monto.monto * item.kilometraje * lineaRemunerada.remunerado / 100) / 100) * (cantidadDias * 24),
                                    Mes = fechaHoy.Value.Month,
                                    Anio = fechaHoy.Value.Year,
                                };
                                db.IndisponibilidadRemuLineas.Add(nuevaLineaRemunerada);
                                //lineaRemunerada.cargoRealxHoras = (monto.monto * item.kilometraje * lineaRemunerada.remunerado / 100) / 100;

                                //var fechaHoy = new DateTime(); var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;

                                //int cantidadDias = DateTime.DaysInMonth(fechaHoy.Year, fechaHoy.Month);
                                //lineaRemunerada.xMes = lineaRemunerada.cargoRealxHoras * (cantidadDias * 24);
                                db.SaveChanges();
                            }
                            else
                            {
                                var datosLinea = db.IndisponibilidadRemuLineas.Where(x => x.idLinea == item.idLinea).FirstOrDefault();
                                if (datosLinea != null)
                                {

                                    var cargoRealxHoras = (monto.monto * item.kilometraje * datosLinea.remunerado / 100) / 100;
                                    var xMes = cargoRealxHoras * (cantidadDias * 24);
                                    IndisponibilidadRemuLineas nuevaEquipos = new IndisponibilidadRemuLineas
                                    {
                                        activo = true,
                                        cargoRealxHoras = cargoRealxHoras,
                                        idResolucion = idResolucion,
                                        idLinea = item.idLinea,
                                        origenRegulatorio = datosLinea.origenRegulatorio,
                                        xMes = xMes,
                                        Mes = fechaHoy.Value.Month,
                                        Anio = fechaHoy.Value.Year,
                                        remunerado = datosLinea.remunerado
                                    };
                                    db.IndisponibilidadRemuLineas.Add(nuevaEquipos);
                                    db.SaveChanges();
                                }

                            }
                        }
                        var lineaIndisponible = db.IndisponibilidadLineas.Where(x => x.fechaEntrada.Value.Month == monto.fechaDesde.Value.Month && x.fechaEntrada.Value.Year == monto.fechaDesde.Value.Year /*&& x.idMonto == monto.idIndisponibilidadMontos*/).ToList();
                        foreach (var linea in lineaIndisponible)
                        {
                            ModificoLineaIndisponibles(linea, idResolucion);
                            //db.SaveChanges();
                        }
                        calculoMesesLinea(monto.fechaDesde);
                        monto.fechaDesde = monto.fechaDesde.Value.AddMonths(1);


                    }
                }


                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                         ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    return 400;

                }
                return 200;
            }
        }

        public int ModificarMontoLinea(MontoIndisponibilidadesVM monto)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var idResolucion = monto.idIndisponibilidadMontos;
                var consultarUnMonto = db.IndisponibilidadMontoLineas.Where(x => x.idIndisponibilidadMontos == monto.idIndisponibilidadMontos).FirstOrDefault();
                var kvConvertido = db.GralTension.Where(x => x.idTension == monto.kv).FirstOrDefault();
                var kvInt = int.Parse(kvConvertido.tension);
                var FechaActual = DateTime.Now;
                // var fechaDesde = consultarUnMonto.fechaMonto;
                if (consultarUnMonto != null)
                {
                    var montoHistorial = consultarUnMonto.monto;

                    //Cambio los datos del monto consultado y lo pongo en HistorialActivo 1 para mostrarlo en la parte de historiales

                    consultarUnMonto.monto = monto.monto;
                    consultarUnMonto.kv = kvInt;
                    consultarUnMonto.fechaMonto = monto.fechaDesde;
                    consultarUnMonto.resolucion = monto.resolucion;
                    //consultarUnMonto.activo = false;
                    consultarUnMonto.fechaHasta = monto.fechaHasta;
                    //consultarUnMonto.activoHistorial = true;
                    db.SaveChanges();

                    var fechaHastaRes = monto.fechaHasta == null ? DateTime.Today : monto.fechaHasta;


                    while (monto.fechaDesde <= fechaHastaRes)
                    {
                        //Console.WriteLine(fechaActual.ToString("MMMM", CultureInfo.InvariantCulture));
                        var lineas = db.LinLinea.Where(x => x.tension == monto.kv /*&& x.activo == true*/ && x.remunerado == true && x.fechaCreacion <= fechaHastaRes).ToList();
                        foreach (var item in lineas) //MODIFICO EL CARGO DE LAS LINEAS QUE TIENEN ASIGNADO ESE KV
                        {
                            var lineaRemunerada = db.IndisponibilidadRemuLineas.Where(x => x.idLinea == item.idLinea && x.activo == true && x.idResolucion == idResolucion && x.Mes == monto.fechaDesde.Value.Month && x.Anio == monto.fechaDesde.Value.Year).FirstOrDefault();

                            if (lineaRemunerada != null)
                            {
                                lineaRemunerada.cargoRealxHoras = (monto.monto * item.kilometraje * lineaRemunerada.remunerado / 100) / 100;

                                var fechaHoy = monto.fechaDesde; //var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;
                                int cantidadDias = DateTime.DaysInMonth(fechaHoy.Value.Year, fechaHoy.Value.Month);
                                lineaRemunerada.xMes = lineaRemunerada.cargoRealxHoras * (cantidadDias * 24);
                                db.SaveChanges();
                            }
                            else
                            {
                                var datosLinea = db.IndisponibilidadRemuLineas.Where(x => x.idLinea == item.idLinea).FirstOrDefault();
                                if (datosLinea != null)
                                {

                                    var cargoRealxHoras = (monto.monto * item.kilometraje * datosLinea.remunerado / 100) / 100;
                                    var fechaHoy = monto.fechaDesde; //var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;
                                    int cantidadDias = DateTime.DaysInMonth(fechaHoy.Value.Year, fechaHoy.Value.Month);
                                    var xMes = cargoRealxHoras * (cantidadDias * 24);
                                    IndisponibilidadRemuLineas nuevaEquipos = new IndisponibilidadRemuLineas
                                    {
                                        activo = true,
                                        cargoRealxHoras = cargoRealxHoras,
                                        idResolucion = idResolucion,
                                        idLinea = item.idLinea,
                                        origenRegulatorio = datosLinea.origenRegulatorio,
                                        xMes = xMes,
                                        Mes = fechaHoy.Value.Month,
                                        Anio = fechaHoy.Value.Year,
                                        remunerado = datosLinea.remunerado
                                    };
                                    db.IndisponibilidadRemuLineas.Add(nuevaEquipos);
                                    db.SaveChanges();
                                }

                            }

                        }



                        //IndisponibilidadMontoLineas NuevoMonto = new IndisponibilidadMontoLineas()
                        //{
                        //    tipoEquipo = 4,
                        //    monto = monto.monto,
                        //    kv = kvInt,
                        //    fechaMonto = monto.fechaDesde,
                        //    resolucion = monto.resolucion,
                        //    activo = true,
                        //};
                        //db.IndisponibilidadMontoLineas.Add(NuevoMonto);
                        //db.SaveChanges();

                        var lineaIndisponible = db.IndisponibilidadLineas.Where(x => x.fechaEntrada.Value.Month == monto.fechaDesde.Value.Month && x.fechaEntrada.Value.Year == monto.fechaDesde.Value.Year /*&& x.idMonto == monto.idIndisponibilidadMontos*/).ToList();
                        foreach (var linea in lineaIndisponible)
                        {
                            ModificoLineaIndisponibles(linea, idResolucion);
                            //db.SaveChanges();
                        }
                        //y esto?
                        calculoMesesLinea(monto.fechaDesde);
                        monto.fechaDesde = monto.fechaDesde.Value.AddMonths(1);

                    }


                }

                else
                {
                    AltaMontoLinea(monto);
                }
            }


            return 200;
        }

        public int EliminarMontoLinea(MontoIndisponibilidadesVM monto)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnMonto = db.IndisponibilidadMontoLineas.Where(x => x.idIndisponibilidadMontos == monto.idIndisponibilidadMontos && x.activo == true).FirstOrDefault();
                if (consultaUnMonto != null)
                {
                    consultaUnMonto.activo = false;
                    consultaUnMonto.fechaHasta = DateTime.Today;
                    consultaUnMonto.activoHistorial = true;

                }
                db.SaveChanges();
            }
            return 200;
        }

        public MontoIndisponibilidadesVM ConsultarMonto(int idlinea, DateTime fechaDesdeMonto, DateTime? fechaHastaMonto, int? idMonto)
        {
            MontoIndisponibilidadesVM montoEncontrado = new MontoIndisponibilidadesVM();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var lineaR = db.LinLinea.Where(x => x.idLinea == idlinea && x.activo == true).FirstOrDefault();
                var kvString = db.GralTension.Where(x => x.idTension == lineaR.tension).FirstOrDefault();
                var kvInt = int.Parse(kvString.tension);
                var consultaUnMonto = new IndisponibilidadMontoLineas();
                if (idMonto != 0)
                {
                    consultaUnMonto = db.IndisponibilidadMontoLineas.Where(x => x.idIndisponibilidadMontos == idMonto).FirstOrDefault();
                }
                else
                {
                    consultaUnMonto = db.IndisponibilidadMontoLineas.Where(x => x.kv == kvInt && ((x.fechaMonto <= fechaDesdeMonto && x.fechaHasta >= fechaHastaMonto && x.activoHistorial == true) || (x.fechaMonto <= fechaDesdeMonto && x.fechaHasta == null && x.activoHistorial == true))).FirstOrDefault();
                }

                if (consultaUnMonto != null)
                {
                    montoEncontrado = (from m in db.IndisponibilidadMontoLineas
                                       where m.idIndisponibilidadMontos == consultaUnMonto.idIndisponibilidadMontos
                                       select new MontoIndisponibilidadesVM
                                       {
                                           idIndisponibilidadMontos = m.idIndisponibilidadMontos,
                                           monto = m.monto,
                                           mostrarMonto = "$ " + m.monto,
                                           kv = m.kv,
                                           resolucion = "Resolucion: " + m.resolucion,
                                       }).FirstOrDefault();
                }

            }
            return montoEncontrado;
        }


        //Historial Lineas

        public object ObtenerHistorialLinea()
        {
            List<MontoIndisponibilidadesVM> historialLinea;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                historialLinea = (from h in db.IndisponibilidadMontoLineas
                                  where /*h.activo == false &&*/ h.activoHistorial == true
                                  select new MontoIndisponibilidadesVM
                                  {
                                      idIndisponibilidadHistorialMontLinea = h.idIndisponibilidadMontos,
                                      kv = h.kv,
                                      monto = h.monto,
                                      fechaDesde = h.fechaMonto,
                                      fechaHasta = h.fechaHasta,
                                      resolucion = h.resolucion,
                                      activo = h.activo
                                  }).ToList();
            }
            object json = new { data = historialLinea };
            return json;
        }

        public int EliminarHistorialLinea(MontoIndisponibilidadesVM historial)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarHistorialL = db.IndisponibilidadMontoLineas.Where(x => x.idIndisponibilidadMontos == historial.idIndisponibilidadHistorialMontLinea /*&& x.activoHistorial == true*/).FirstOrDefault();
                if (consultarHistorialL != null)
                {
                    consultarHistorialL.activoHistorial = false;
                    db.SaveChanges();
                }
                return 200;
            }
        }

        public int ModificarHistorialMontoLinea(MontoIndisponibilidadesVM monto)
        {
            //ver
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarUnMonto = db.IndisponibilidadMontoLineas.Where(x => x.idIndisponibilidadMontos == monto.idIndisponibilidadMontos && x.activoHistorial == true).FirstOrDefault();
                if (consultarUnMonto != null)
                {

                    consultarUnMonto.monto = monto.monto;
                    consultarUnMonto.resolucion = monto.resolucion;
                    if (monto.fechaDesdeF != "" && monto.fechaDesdeF != "Sin fecha")
                    {
                        DateTime fecha = DateTime.Parse(monto.fechaDesdeF);

                        consultarUnMonto.fechaMonto = fecha;
                    }
                    if (monto.fechaHastaF != "" && monto.fechaHastaF != "Sin fecha")
                    {
                        consultarUnMonto.fechaHasta = monto.fechaHasta;
                        if (consultarUnMonto.fechaHasta < DateTime.Today)
                        {
                            consultarUnMonto.activo = false;
                        }
                    }
                    db.SaveChanges();
                    ModificarMontoLinea(monto);
                    //while (monto.fechaDesde <= monto.fechaHasta)
                    //{
                    //    var consultaLineaIndisponible = db.IndisponibilidadLineas.Where(x => x.fechaSalida.Value.Month == monto.fechaDesde.Value.Month && x.fechaSalida.Value.Year == monto.fechaDesde.Value.Year).ToList();
                    //    foreach (var lineaIndisponible in consultaLineaIndisponible)
                    //    {
                    //        ModificoLineaIndisponibles(lineaIndisponible, monto.idIndisponibilidadMontos);
                    //    }
                    //    calculoMesesLinea(monto.fechaDesde);
                    //    monto.fechaDesde = monto.fechaDesde.Value.AddMonths(1);
                    //}


                }
            }
            return 200;
        }

        //Lineas Indisponibles

        public object obtenerLineasIndisponibles(DateTime fechaDesde, DateTime fechaHasta)
        {
            List<IndisRemuEquipoVM> listaLineaRemunerada;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                listaLineaRemunerada = (from l in db.LinLinea
                                        join g in db.GralTension on l.tension equals g.idTension
                                        join r in db.IndisponibilidadRemuLineas on l.idLinea equals r.idLinea into remu
                                        from r in remu.DefaultIfEmpty()
                                        join il in db.IndisponibilidadLineas on r.idLineaIndisRemu equals il.IdLineaIndisRemu into linR
                                        from il in linR.DefaultIfEmpty()
                                        where l.activo == true && l.remunerado == true && il.activo == true && r.activo == true && (il.fechaSalida >= fechaDesde && il.fechaEntrada <= fechaHasta)
                                        select new IndisRemuEquipoVM
                                        {
                                            idLineaRemunerada = il.IdLineaRemunerada,
                                            idlinea = l.idLinea,
                                            idLineaIndisRemu = r.idLineaIndisRemu,
                                            idPagotran = l.idPagotran,
                                            nombreLinea = l.nombreLinea,
                                            Ukv = g.tension,
                                            longKm = l.kilometraje,
                                            cargoRealxHora = il.montoXhora,
                                            muestroRealxHora = il.montoXhora != null ? "$ " + il.montoXhora : " ",
                                            fechaSalida = il.fechaSalida,
                                            fechaEntrada = il.fechaEntrada,
                                            hsIndisponible = il.hsIndisponible,
                                            minutosIndisponible = il.minutosIndisponible,
                                            tipoSalida = il.tipoSalida,
                                            hsForzada = il.hsForzada,
                                            hsProgramada = il.hsProgramada,
                                            activo = r.activo,
                                            penalizacionProgramada = il.penalizacionProgramada,
                                            MuestroinformoEnTermino = il.informoEnTermino == true ? "Si" : "No",
                                            totalPenalizacion = il.totalPenalizacion,
                                            PenalizacionForzada1 = il.PenalizacionForzada1,
                                            PenalizacionForzada2 = il.PenalizacionForzada2,
                                            PenalizacionForzada3 = il.PenalizacionForzada3,
                                            cargoReal = il.cargoReal,
                                            informoEnTermino = il.informoEnTermino,
                                            noPercibido = il.noPercibido,
                                            lucroCesante = il.lucroCesante,
                                            motivo = il.motivo,

                                            idMonto = il.idMonto,
                                        }).ToList();


            }
            listaLineaRemunerada = listaLineaRemunerada.OrderByDescending(x => x.idLineaRemunerada).ToList();
            object json = new { data = listaLineaRemunerada };
            return json;
        }
        //este es para el alta desde el otro servicio
        public int AltaLineaIndisponibles(IndisRemuEquipoVM linea, DateTime fechaDesdeMonto, DateTime? fechaHastaMonto)
        {

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnaLinea = db.IndisponibilidadLineas.Where(x => x.idLinea == linea.idlinea && x.fechaSalida == linea.fechaSalida && x.fechaEntrada == linea.fechaEntrada && x.activo == true).FirstOrDefault();
                if (consultaUnaLinea == null)
                {
                    try
                    {
                        var lineaRemunerada = db.IndisponibilidadRemuLineas.Where(x => x.idLinea == linea.idlinea).FirstOrDefault();
                        var lineaR = db.LinLinea.Where(x => x.idLinea == linea.idlinea && x.activo == true).FirstOrDefault();
                        var kv = db.GralTension.Where(x => x.activo == true && x.idTension == lineaR.tension).FirstOrDefault();
                        var UkvInt = Convert.ToInt32(kv.tension);
                        var monto = db.IndisponibilidadMontoLineas.Where(x => x.kv == UkvInt && ((x.fechaMonto <= fechaDesdeMonto && x.fechaHasta >= fechaHastaMonto && x.activoHistorial == true) || (x.fechaMonto <= fechaDesdeMonto && x.fechaHasta == null && x.activoHistorial == true))).FirstOrDefault();

                        if (monto == null)
                        {
                            return 303;
                        }

                        decimal? montoXhoraRemunerado = 0;
                        if (kv.tension == "220" || kv.tension == "132")
                        {
                            if (lineaR.kilometraje > 25)
                            {
                                montoXhoraRemunerado = Math.Round((decimal)(monto.monto * lineaR.kilometraje) / 100, 2);
                            }
                            else
                            {
                                montoXhoraRemunerado = Math.Round((decimal)(monto.monto * 25) / 100, 2);
                            }
                        }

                        DateTime fechaMal = Convert.ToDateTime("1/1/0001 00:00:00");
                        if (linea?.fechaEntrada != fechaMal)
                        {
                            var horasIndisponible = (linea.fechaEntrada - linea.fechaSalida)?.TotalHours;
                            var minDisponibles = Math.Round((decimal)(linea.fechaEntrada - linea.fechaSalida)?.TotalMinutes, 2);

                            decimal? hsProgramada = null;
                            decimal? hsForzada = null;
                            decimal? penalizacionPrograma = null;
                            decimal? totalPenalizacion = null;
                            int FactorPenalizacion = linea.factorPenalizacion;
                            //hsForzada = Convert.ToDecimal(horasIndisponible);
                            decimal? PenalFor1 = montoXhoraRemunerado * FactorPenalizacion;
                            decimal? PenalFor2 = 0;
                            decimal? PenalFor3 = 0;

                            if (linea.tipoSalida == "P")
                            {
                                hsProgramada = Convert.ToDecimal(horasIndisponible);
                                var factor1 = Math.Round(minDisponibles / 60, 2);
                                penalizacionPrograma = Math.Round((decimal)(factor1 * montoXhoraRemunerado * FactorPenalizacion * Convert.ToDecimal(0.01)), 2);
                                if (linea.informoEnTermino == true)
                                {
                                    totalPenalizacion = penalizacionPrograma * 1;
                                }
                                else
                                {
                                    totalPenalizacion = penalizacionPrograma * 2;
                                }
                            }
                            else if (linea.tipoSalida == "F" || linea.tipoSalida == "FA")
                            {
                                hsForzada = Convert.ToDecimal(horasIndisponible);
                                if (minDisponibles > 10)
                                {
                                    if (minDisponibles > 180)
                                    {
                                        PenalFor2 = FactorPenalizacion * montoXhoraRemunerado * 3;
                                    }
                                    else
                                    {
                                        PenalFor2 = FactorPenalizacion * montoXhoraRemunerado * Convert.ToDecimal(horasIndisponible);
                                    }
                                }

                                if (minDisponibles > 180)
                                {
                                    PenalFor3 = Math.Round((decimal)((Convert.ToDecimal(minDisponibles / 60) - 3) * montoXhoraRemunerado * FactorPenalizacion * Convert.ToDecimal(0.1)));
                                }

                                if (linea.informoEnTermino == true)
                                {
                                    totalPenalizacion = PenalFor1 + PenalFor2 + PenalFor3 * 1;
                                }
                                else
                                {
                                    totalPenalizacion = PenalFor1 + PenalFor2 + PenalFor3 * 2;
                                }
                            }
                            else if (linea.tipoSalida == "FM")
                            {
                                if (minDisponibles > 10)
                                {
                                    if (minDisponibles > 180)
                                    {
                                        PenalFor2 = FactorPenalizacion * montoXhoraRemunerado * 3;
                                    }
                                    else
                                    {
                                        PenalFor2 = FactorPenalizacion * montoXhoraRemunerado * Convert.ToDecimal(horasIndisponible);
                                    }
                                }

                                if (minDisponibles > 180)
                                {
                                    PenalFor3 = ((Convert.ToDecimal(minDisponibles) / 60) - 3) * montoXhoraRemunerado * FactorPenalizacion * Convert.ToDecimal(0.1);
                                }

                                if (linea.informoEnTermino == true)
                                {
                                    totalPenalizacion = PenalFor2 + PenalFor3 * 1;
                                }
                                else
                                {
                                    totalPenalizacion = PenalFor2 + PenalFor3 * 2;
                                }
                            }
                            else if (linea.tipoSalida == "ST")
                            {
                                totalPenalizacion = 0;

                            }

                            decimal? cargoReal = (monto.monto * lineaR.kilometraje) / 100;

                            decimal? noPercibido = cargoReal * Convert.ToDecimal(horasIndisponible);

                            decimal? lucroCesante = noPercibido + totalPenalizacion;

                            IndisponibilidadLineas lin = new IndisponibilidadLineas
                            {
                                IdLineaIndisRemu = lineaRemunerada.idLineaIndisRemu,
                                idLinea = linea.idlinea,
                                idMonto = monto.idIndisponibilidadMontos,
                                montoXhora = montoXhoraRemunerado,
                                fechaSalida = linea.fechaSalida,
                                fechaEntrada = linea.fechaEntrada,
                                hsIndisponible = Convert.ToDecimal(horasIndisponible),
                                minutosIndisponible = Convert.ToDecimal(minDisponibles),
                                tipoSalida = linea.tipoSalida,
                                activo = true,
                                hsProgramada = hsProgramada,
                                hsForzada = hsForzada,
                                penalizacionProgramada = penalizacionPrograma,
                                informoEnTermino = linea.informoEnTermino,
                                totalPenalizacion = totalPenalizacion,
                                PenalizacionForzada1 = PenalFor1,
                                PenalizacionForzada2 = PenalFor2,
                                PenalizacionForzada3 = PenalFor3,
                                cargoReal = cargoReal,
                                noPercibido = noPercibido,
                                lucroCesante = lucroCesante,
                                motivo = linea.motivo,
                                idIndisponibilidad = linea.idNovedadIndisponibilidad,
                            };
                            db.IndisponibilidadLineas.Add(lin);
                            db.SaveChanges();

                            //VIENE DE LO DE INDISPONIBILIDADES DEL UCIEL
                            if (linea.noCargar != true)//El no cargar es para que no cree otrs indisponibilidad por que viene de lo del Uciel
                            {
                                ServicioIndisponibilidad servicioIndisUciel = new ServicioIndisponibilidad();
                                NovNovedadIndisponibilidad indis = new NovNovedadIndisponibilidad { };
                                indis.activo = true;
                                indis.FechaEntrada = linea.fechaEntrada;
                                indis.FechaSalida = linea.fechaSalida;
                                indis.PorcentajeIndisponible = 100;
                                indis.idEquipo = linea.idlinea;
                                indis.idTipoEquipo = 4;
                                indis.idTipoIndisponibilidad = 1;
                                indis.descripcion = linea.motivo;
                                indis.fueraDeServicio = true;
                                indis.idIndisponibilidadRemu = lin.IdLineaRemunerada;
                                if (linea.tipoSalida == "F")
                                {
                                    indis.itn = 1;
                                }
                                else if (linea.tipoSalida == "FA")
                                {
                                    indis.itn = 2;
                                }
                                else if (linea.tipoSalida == "P")
                                {
                                    indis.itn = 3;
                                }
                                else if (linea.tipoSalida == "ST")
                                {
                                    indis.itn = 4;
                                }
                                db.NovNovedadIndisponibilidad.Add(indis);
                                db.SaveChanges();
                                var ultimaIndi = db.NovNovedadIndisponibilidad.Where(x => x.idIndisponibilidadRemu == lin.IdLineaRemunerada && x.activo == true && x.idTipoEquipo == 4).FirstOrDefault();
                                lin.idIndisponibilidad = ultimaIndi.idNovedadIndisponibilidad;
                            }

                            db.SaveChanges();
                            calculoMesesLinea(linea.fechaSalida);
                            return 200;
                        }
                        else
                        {

                            IndisponibilidadLineas lin = new IndisponibilidadLineas
                            {
                                IdLineaIndisRemu = lineaRemunerada.idLineaIndisRemu,
                                idLinea = linea.idlinea,
                                idMonto = monto.idIndisponibilidadMontos,
                                montoXhora = montoXhoraRemunerado,
                                fechaSalida = linea.fechaSalidaA,
                                //fechaEntrada = linea.fechaEntradaA,
                                //hsIndisponible = Convert.ToDecimal(horasIndisponible),
                                // minutosIndisponible = Convert.ToDecimal(minDisponibles),
                                tipoSalida = linea.tipoSalida,
                                activo = true,
                                // hsProgramada = hsProgramada,
                                // hsForzada = hsForzada,
                                //penalizacionProgramada = penalizacionPrograma,
                                informoEnTermino = linea.informoEnTermino,
                                //totalPenalizacion = totalPenalizacion,
                                //cargoReal = cargoReal,
                                //noPercibido = noPercibido,
                                //lucroCesante = lucroCesante,
                                motivo = linea.motivo,
                                idIndisponibilidad = linea.idNovedadIndisponibilidad,

                            };
                            db.IndisponibilidadLineas.Add(lin);
                            db.SaveChanges();
                            calculoMesesLinea(linea.fechaSalida);
                            return 200;
                        }

                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }

            }
        }

        public int ModificarLineaIndisponibles(IndisRemuEquipoVM linea, DateTime fechaDesdeMonto, DateTime? fechaHastaMonto)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunLineaRemu = db.IndisponibilidadLineas.Where(x => x.IdLineaRemunerada == linea.idLineaRemunerada).FirstOrDefault();
                if (consultaunLineaRemu != null)
                {
                    var lineaR = db.LinLinea.Where(x => x.idLinea == consultaunLineaRemu.idLinea && x.activo == true).FirstOrDefault();
                    var kv = db.GralTension.Where(x => x.activo == true && x.idTension == lineaR.tension).FirstOrDefault();
                    var UkvInt = Convert.ToInt32(kv.tension);
                    var monto = db.IndisponibilidadMontoLineas.Where(x => x.kv == UkvInt && ((x.fechaMonto <= fechaDesdeMonto && x.fechaHasta >= fechaHastaMonto && x.activoHistorial == true) || (x.fechaMonto <= fechaDesdeMonto && x.fechaHasta == null && x.activoHistorial == true))).FirstOrDefault();

                    if (monto == null)
                    {
                        return 303;
                    }
                    decimal? montoXhoraRemunerado = 0;
                    if (kv.tension == "220" || kv.tension == "132")
                    {
                        if (lineaR.kilometraje > 25)
                        {
                            montoXhoraRemunerado = Math.Round((decimal)(monto.monto * lineaR.kilometraje) / 100, 2);
                        }
                        else
                        {
                            montoXhoraRemunerado = Math.Round((decimal)(monto.monto * 25) / 100, 2);
                        }
                    }

                    consultaunLineaRemu.fechaSalida = linea.fechaSalida;
                    consultaunLineaRemu.fechaEntrada = linea.fechaEntrada;
                    consultaunLineaRemu.idLinea = linea.idlinea;
                    //consultaunLineaRemu.idMonto = monto.idIndisponibilidadMontos;
                    consultaunLineaRemu.montoXhora = montoXhoraRemunerado;


                    DateTime fechaMal = Convert.ToDateTime("1/1/0001 00:00:00");
                    if (linea.fechaEntrada != fechaMal)
                    {
                        var horasIndisponible = (linea.fechaEntrada - linea.fechaSalida)?.TotalHours;
                        var minDisponibles = Math.Round((decimal)(linea.fechaEntrada - linea.fechaSalida)?.TotalMinutes, 2);

                        consultaunLineaRemu.hsIndisponible = Convert.ToDecimal(horasIndisponible);
                        consultaunLineaRemu.minutosIndisponible = Convert.ToDecimal(minDisponibles);
                        consultaunLineaRemu.tipoSalida = linea.tipoSalida;
                        consultaunLineaRemu.informoEnTermino = linea.informoEnTermino;

                        int FactorPenalizacion = linea.factorPenalizacion;
                        decimal? PenalFor1 = 0;
                        decimal? PenalFor2 = 0;
                        decimal? PenalFor3 = 0;
                        if (consultaunLineaRemu.tipoSalida == "P") //Si es programada
                        {
                            consultaunLineaRemu.hsProgramada = Convert.ToDecimal(horasIndisponible);
                            consultaunLineaRemu.hsForzada = null;
                            var factor1 = Math.Round(minDisponibles / 60, 2);

                            consultaunLineaRemu.penalizacionProgramada = Math.Round((decimal)(factor1 * montoXhoraRemunerado * FactorPenalizacion * Convert.ToDecimal(0.01)), 2);
                            if (linea.informoEnTermino == true)
                            {
                                consultaunLineaRemu.totalPenalizacion = consultaunLineaRemu.penalizacionProgramada * 1;
                            }
                            else
                            {
                                consultaunLineaRemu.totalPenalizacion = consultaunLineaRemu.penalizacionProgramada * 2;
                            }

                        }
                        else if (consultaunLineaRemu.tipoSalida == "F" || consultaunLineaRemu.tipoSalida == "FA") // Si es forzada
                        {
                            consultaunLineaRemu.hsForzada = Convert.ToDecimal(horasIndisponible);
                            consultaunLineaRemu.hsProgramada = null;
                            consultaunLineaRemu.penalizacionProgramada = null;

                            PenalFor1 = montoXhoraRemunerado * FactorPenalizacion;

                            if (minDisponibles > 10)
                            {
                                if (minDisponibles > 180)
                                {
                                    PenalFor2 = FactorPenalizacion * montoXhoraRemunerado * 3;
                                }
                                else
                                {
                                    PenalFor2 = FactorPenalizacion * montoXhoraRemunerado * Convert.ToDecimal(horasIndisponible);
                                }
                            }

                            if (minDisponibles > 180)
                            {
                                PenalFor3 = Math.Round((decimal)((Convert.ToDecimal(minDisponibles / 60) - 3) * montoXhoraRemunerado * FactorPenalizacion * Convert.ToDecimal(0.1)));
                            }

                            if (linea.informoEnTermino == true)
                            {
                                consultaunLineaRemu.totalPenalizacion = PenalFor1 + PenalFor2 + PenalFor3 * 1;
                            }
                            else
                            {
                                consultaunLineaRemu.totalPenalizacion = PenalFor1 + PenalFor2 + PenalFor3 * 2;
                            }
                        }
                        else if (linea.tipoSalida == "FM")
                        {
                            if (minDisponibles > 10)
                            {
                                if (minDisponibles > 180)
                                {
                                    PenalFor2 = FactorPenalizacion * montoXhoraRemunerado * 3;
                                }
                                else
                                {
                                    PenalFor2 = FactorPenalizacion * montoXhoraRemunerado * Convert.ToDecimal(horasIndisponible);
                                }
                            }

                            if (minDisponibles > 180)
                            {
                                PenalFor3 = ((Convert.ToDecimal(minDisponibles) / 60) - 3) * montoXhoraRemunerado * FactorPenalizacion * Convert.ToDecimal(0.1);
                            }

                            if (linea.informoEnTermino == true)
                            {
                                consultaunLineaRemu.totalPenalizacion = PenalFor2 + PenalFor3 * 1;
                            }
                            else
                            {
                                consultaunLineaRemu.totalPenalizacion = PenalFor2 + PenalFor3 * 2;
                            }
                        }
                        else if (linea.tipoSalida == "ST")
                        {
                            consultaunLineaRemu.totalPenalizacion = 0;

                        }

                        decimal? cargoReal = (monto.monto * lineaR.kilometraje) / 100;

                        decimal? noPercibido = cargoReal * Convert.ToDecimal(horasIndisponible);

                        decimal? lucroCesante = noPercibido + consultaunLineaRemu.totalPenalizacion;

                        consultaunLineaRemu.cargoReal = cargoReal;
                        consultaunLineaRemu.noPercibido = noPercibido;
                        consultaunLineaRemu.lucroCesante = lucroCesante;
                        consultaunLineaRemu.motivo = linea.motivo;
                        consultaunLineaRemu.idMonto = monto.idIndisponibilidadMontos;
                        consultaunLineaRemu.PenalizacionForzada1 = PenalFor1;
                        consultaunLineaRemu.PenalizacionForzada2 = PenalFor2;
                        consultaunLineaRemu.PenalizacionForzada3 = PenalFor3;
                        db.SaveChanges();

                    }

                    //Esto es para indisponibilidades que hizo el uciel
                    ServicioIndisponibilidad serviIndispUciel = new ServicioIndisponibilidad();
                    var indisExistente = db.NovNovedadIndisponibilidad.Where(x => x.idIndisponibilidadRemu == consultaunLineaRemu.IdLineaRemunerada && x.activo == true && x.idTipoEquipo == 4).FirstOrDefault();

                    if (indisExistente != null) //SI ES UNA EDICION
                    {
                        indisExistente.FechaEntrada = linea.fechaEntrada;
                        indisExistente.FechaSalida = linea.fechaSalida;
                        indisExistente.PorcentajeIndisponible = 100;
                        indisExistente.idEquipo = linea.idlinea;
                        indisExistente.idTipoEquipo = 4;
                        indisExistente.descripcion = linea.motivo;
                        indisExistente.fueraDeServicio = linea.informoEnTermino;


                        if (linea.tipoSalida == "F")
                        {
                            indisExistente.itn = 1;
                        }
                        else if (linea.tipoSalida == "FM")
                        {
                            indisExistente.itn = 2;
                        }
                        else if (linea.tipoSalida == "P")
                        {
                            indisExistente.itn = 3;
                        }
                        db.SaveChanges();
                    }

                }

            }
            calculoMesesLinea(linea.fechaSalida);
            return 200;
        }

        public int EliminarLineaIndisponible(IndisRemuEquipoVM linea)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaLineaIndis = db.IndisponibilidadLineas.Where(x => x.IdLineaRemunerada == linea.idLineaRemunerada && x.activo == true).FirstOrDefault();
                if (consultaLineaIndis != null)
                {
                    consultaLineaIndis.activo = false;
                    db.SaveChanges();

                    var linNovIndis = db.NovNovedadIndisponibilidad.Where(x => x.idIndisponibilidadRemu == linea.idLineaRemunerada && x.activo == true && x.idTipoEquipo == 4).FirstOrDefault();
                    if (linNovIndis != null)
                    {
                        linNovIndis.activo = false;
                        db.SaveChanges();
                    }

                }
                calculoMesesLinea(consultaLineaIndis.fechaSalida);
                return 200;
            }
        }

        public int ModificoLineaIndisponibles(IndisponibilidadLineas linea, int? idResolucion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunLineaRemu = db.IndisponibilidadLineas.Where(x => x.IdLineaRemunerada == linea.IdLineaRemunerada).FirstOrDefault();
                var factorK = db.IndisponibilidadesUtilidades.Where(x => x.idTipoEquipo == 4 && x.activo == true).FirstOrDefault();
                var lineaRemunerada = db.IndisponibilidadRemuLineas.Where(x => x.idLinea == linea.idLinea && x.idResolucion == idResolucion
                                        && x.Mes == linea.fechaSalida.Value.Month && x.Anio == linea.fechaSalida.Value.Year).FirstOrDefault();
                if (consultaunLineaRemu != null /*&& lineaRemunerada != null*/)
                {
                    var lineaR = db.LinLinea.Where(x => x.idLinea == consultaunLineaRemu.idLinea /*&& x.activo == true*/).FirstOrDefault();
                    var kv = db.GralTension.Where(x => x.activo == true && x.idTension == lineaR.tension).FirstOrDefault();
                    var UkvInt = Convert.ToInt32(kv.tension);

                    var monto = new IndisponibilidadMontoLineas();
                    if (linea.fechaEntrada >= DateTime.Now)
                    {
                        //ver
                        monto = db.IndisponibilidadMontoLineas.Where(x => x.activo == true && x.kv == UkvInt).FirstOrDefault();
                        consultaunLineaRemu.idMonto = monto.idIndisponibilidadMontos;
                    }
                    else//esto es mas que nada para el historial
                    {
                        monto = db.IndisponibilidadMontoLineas.Where(x => x.idIndisponibilidadMontos == linea.idMonto).FirstOrDefault();
                    }

                    decimal? montoXhoraRemunerado = 0;
                    if (kv.tension == "220" || kv.tension == "132")
                    {
                        if (lineaR.kilometraje > 25)
                        {
                            montoXhoraRemunerado = (monto.monto * lineaR.kilometraje) / 100;
                        }
                        else
                        {
                            montoXhoraRemunerado = (monto.monto * 25) / 100;
                        }
                    }

                    consultaunLineaRemu.fechaSalida = linea.fechaSalida;
                    consultaunLineaRemu.fechaEntrada = linea.fechaEntrada;
                    consultaunLineaRemu.idLinea = linea.idLinea;
                    //consultaunLineaRemu.idMonto = monto.idIndisponibilidadMontos;
                    consultaunLineaRemu.montoXhora = montoXhoraRemunerado;
                    consultaunLineaRemu.IdLineaIndisRemu = lineaRemunerada != null ? lineaRemunerada.idLineaIndisRemu : consultaunLineaRemu.IdLineaIndisRemu;

                    DateTime fechaMal = Convert.ToDateTime("1/1/0001 00:00:00");
                    if (linea.fechaEntrada != fechaMal)
                    {

                        var horasIndisponible = (linea.fechaEntrada - linea.fechaSalida)?.TotalHours;
                        var minDisponibles = Math.Round((decimal)(linea.fechaEntrada - linea.fechaSalida)?.TotalMinutes);

                        consultaunLineaRemu.hsIndisponible = Convert.ToDecimal(horasIndisponible);
                        consultaunLineaRemu.minutosIndisponible = Convert.ToDecimal(minDisponibles);
                        consultaunLineaRemu.tipoSalida = linea.tipoSalida;
                        consultaunLineaRemu.informoEnTermino = linea.informoEnTermino;

                        int? FactorPenalizacion = factorK.valor;
                        decimal? PenalFor1 = 0;
                        decimal? PenalFor2 = 0;
                        decimal? PenalFor3 = 0;
                        if (consultaunLineaRemu.tipoSalida == "P") //Si es programada
                        {
                            consultaunLineaRemu.hsProgramada = Convert.ToDecimal(horasIndisponible);
                            consultaunLineaRemu.hsForzada = null;


                            consultaunLineaRemu.penalizacionProgramada = Math.Round((decimal)(Convert.ToDecimal(minDisponibles / 60) * montoXhoraRemunerado * FactorPenalizacion * Convert.ToDecimal(0.01)));
                            if (linea.informoEnTermino == true)
                            {
                                consultaunLineaRemu.totalPenalizacion = consultaunLineaRemu.penalizacionProgramada * 1;
                            }
                            else
                            {
                                consultaunLineaRemu.totalPenalizacion = consultaunLineaRemu.penalizacionProgramada * 2;
                            }

                        }
                        else if (consultaunLineaRemu.tipoSalida == "F" || consultaunLineaRemu.tipoSalida == "FA") // Si es forzada
                        {
                            consultaunLineaRemu.hsForzada = Convert.ToDecimal(horasIndisponible);
                            consultaunLineaRemu.hsProgramada = null;
                            consultaunLineaRemu.penalizacionProgramada = null;

                            PenalFor1 = montoXhoraRemunerado * FactorPenalizacion;
                            if (minDisponibles > 10)
                            {
                                if (minDisponibles > 180)
                                {
                                    PenalFor2 = FactorPenalizacion * montoXhoraRemunerado * 3;
                                }
                                else
                                {
                                    PenalFor2 = FactorPenalizacion * montoXhoraRemunerado * Convert.ToDecimal(horasIndisponible);
                                }
                            }

                            if (minDisponibles > 180)
                            {
                                PenalFor3 = Math.Round((decimal)((Convert.ToDecimal(minDisponibles / 60) - 3) * montoXhoraRemunerado * FactorPenalizacion * Convert.ToDecimal(0.1)));
                            }

                            if (linea.informoEnTermino == true)
                            {
                                consultaunLineaRemu.totalPenalizacion = PenalFor1 + PenalFor2 + PenalFor3 * 1;
                            }
                            else
                            {
                                consultaunLineaRemu.totalPenalizacion = PenalFor1 + PenalFor2 + PenalFor3 * 2;
                            }
                        }
                        else if (linea.tipoSalida == "FM")
                        {
                            if (minDisponibles > 10)
                            {
                                if (minDisponibles > 180)
                                {
                                    PenalFor2 = FactorPenalizacion * montoXhoraRemunerado * 3;
                                }
                                else
                                {
                                    PenalFor2 = FactorPenalizacion * montoXhoraRemunerado * Convert.ToDecimal(horasIndisponible);
                                }
                            }

                            if (minDisponibles > 180)
                            {
                                PenalFor3 = ((Convert.ToDecimal(minDisponibles) / 60) - 3) * montoXhoraRemunerado * FactorPenalizacion * Convert.ToDecimal(0.1);
                            }

                            if (linea.informoEnTermino == true)
                            {
                                consultaunLineaRemu.totalPenalizacion = PenalFor2 + PenalFor3 * 1;
                            }
                            else
                            {
                                consultaunLineaRemu.totalPenalizacion = PenalFor2 + PenalFor3 * 2;
                            }
                        }
                        else if (linea.tipoSalida == "ST")
                        {
                            consultaunLineaRemu.totalPenalizacion = 0;

                        }

                        decimal? cargoReal = (monto.monto * lineaR.kilometraje) / 100;
                        consultaunLineaRemu.cargoReal = cargoReal;


                        decimal? noPercibido = cargoReal * Convert.ToDecimal(horasIndisponible);

                        decimal? lucroCesante = noPercibido + consultaunLineaRemu.totalPenalizacion;

                        consultaunLineaRemu.noPercibido = noPercibido;
                        consultaunLineaRemu.lucroCesante = lucroCesante;
                        consultaunLineaRemu.motivo = linea.motivo;
                        consultaunLineaRemu.PenalizacionForzada1 = PenalFor1;
                        consultaunLineaRemu.PenalizacionForzada2 = PenalFor2;
                        consultaunLineaRemu.PenalizacionForzada3 = PenalFor3;
                    }
                    db.SaveChanges();


                    //Esto es para indisponibilidades que hizo el uciel
                    ServicioIndisponibilidad serviIndispUciel = new ServicioIndisponibilidad();
                    var indisExistente = db.NovNovedadIndisponibilidad.Where(x => x.idIndisponibilidadRemu == consultaunLineaRemu.idIndisponibilidad && x.activo == true && x.idTipoEquipo == 4).FirstOrDefault();

                    if (indisExistente != null) //SI ES UNA EDICION
                    {
                        indisExistente.activo = true;
                        indisExistente.FechaEntrada = linea.fechaEntrada;
                        indisExistente.FechaSalida = linea.fechaSalida;
                        indisExistente.PorcentajeIndisponible = 100;
                        indisExistente.idEquipo = linea.idLinea;
                        indisExistente.idTipoEquipo = 4;
                        indisExistente.descripcion = linea.motivo;
                        indisExistente.fueraDeServicio = linea.informoEnTermino;

                        if (linea.tipoSalida == "F")
                        {
                            indisExistente.itn = 1;
                        }
                        else if (linea.tipoSalida == "FM")
                        {
                            indisExistente.itn = 2;
                        }
                        else if (linea.tipoSalida == "P")
                        {
                            indisExistente.itn = 3;
                        }
                        else if (linea.tipoSalida == "ST")
                        {
                            indisExistente.itn = 4;
                        }

                        db.SaveChanges();
                    }


                }

            }
            //calculoMesesLinea(linea.fechaSalida);
            return 200;
        } //Esto es para usarlo en otros lados como funcion


        //FactorK
        public IndisRemuEquipoVM obtenerFactorPenalizacion()
        {
            IndisRemuEquipoVM factorPenalizacion = new IndisRemuEquipoVM();

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                factorPenalizacion = (from f in db.IndisponibilidadesUtilidades
                                      where f.activo == true && f.idTipoEquipo == 4
                                      select new IndisRemuEquipoVM
                                      {
                                          idUtilidad = f.idUtilidad,
                                          idTipoEquipo = f.idTipoEquipo,
                                          nombreUtilidad = f.nombreUtilidad,
                                          valorUtilidad = f.valor,
                                          activo = f.activo,
                                      }).FirstOrDefault();

            }
            return factorPenalizacion;
        }

        public int modificarFactorPenalizacion(int factorK)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUtilidad = db.IndisponibilidadesUtilidades.Where(x => x.idTipoEquipo == 4 && x.activo == true).FirstOrDefault();
                if (consultaUtilidad != null)
                {
                    consultaUtilidad.valor = factorK;
                    db.SaveChanges();
                }
                return 200;
            }
        }


        //Generar Reporte Excel 
        public HojaExcelVM GenerarReporteExcelLinea(string fechaDesde, string fechaHasta)
        {
            var fechaDesdeDate = Convert.ToDateTime(fechaDesde);
            var fechaHastaDate = Convert.ToDateTime(fechaHasta);
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    ServicioEquipo servEquipo = new ServicioEquipo();
                    List<IndisRemuEquipoVM> lineas = new List<IndisRemuEquipoVM>();
                    lineas = (from i in db.IndisponibilidadLineas
                              join r in db.IndisponibilidadRemuLineas on i.IdLineaIndisRemu equals r.idLineaIndisRemu
                              join e in db.EquiEquipo on i.idLinea equals e.idEquipo into ee
                              from e in ee.DefaultIfEmpty()
                              join l in db.LinLinea on i.idLinea equals l.idLinea
                              join m in db.IndisponibilidadMontoLineas on i.idMonto equals m.idIndisponibilidadMontos
                              join t in db.GralTension on l.tension equals t.idTension
                              where i.activo == true && fechaDesdeDate <= i.fechaSalida && fechaHastaDate >= i.fechaEntrada
                              select new { i, e, r, l, m, t }).AsEnumerable().Select(x => new IndisRemuEquipoVM
                              {
                                  idLineaRemunerada = x.i.IdLineaRemunerada,
                                  idLineaIndisRemu = x.i.IdLineaIndisRemu,
                                  idlinea = x.l.idLinea,
                                  idMonto = x.m.idIndisponibilidadMontos,
                                  montoXhora = x.i.montoXhora,
                                  fechaSalida = x.i.fechaSalida,
                                  fechaEntrada = x.i.fechaEntrada,
                                  hsIndisponible = x.i.hsIndisponible,
                                  minutosIndisponible = x.i.minutosIndisponible,
                                  tipoSalida = x.i.tipoSalida,
                                  activo = x.i.activo,
                                  hsProgramada = x.i.hsProgramada,
                                  hsForzada = x.i.hsProgramada,
                                  penalizacionProgramada = x.i.penalizacionProgramada,
                                  informoEnTermino = x.i.informoEnTermino,
                                  totalPenalizacion = x.i.totalPenalizacion,
                                  PenalizacionForzada1 = x.i.PenalizacionForzada1,
                                  PenalizacionForzada2 = x.i.PenalizacionForzada2,
                                  PenalizacionForzada3 = x.i.PenalizacionForzada3,
                                  cargoReal = x.i.cargoReal,
                                  noPercibido = x.i.noPercibido,
                                  lucroCesante = x.i.lucroCesante,
                                  motivo = x.i.motivo,
                                  Ukv = x.t.tension,
                                  longKm = x.l.kilometraje,
                                  nombreLinea = x.l.nombreLinea,
                                  idPagotran = x.l.idPagotran,

                                  montoIndis = new MontoIndisponibilidadesVM
                                  {
                                      monto = x.m.monto,
                                      nombreMonto = x.m.kv.ToString(),
                                      resolucion = x.m.resolucion,
                                  }
                              }).OrderByDescending(x => x.idLineaRemunerada).ToList();


                    GenerarExcel excel = new GenerarExcel();
                    HojaExcelVM hojaDetalle = new HojaExcelVM();
                    hojaDetalle.totales = new DetalleExcelVM();

                    decimal? totalPenalizacion = 0;
                    decimal? totalNoPercibo = 0;
                    decimal? totalLucroCesante = 0;

                    var col1 = "Mes";
                    var col2 = "Equipo";
                    var col3 = "ID";
                    var col4 = "Linea";
                    var col5 = "U[kV]";
                    var col6 = "Long.[Km]";
                    var col7 = "$/h";
                    var col8 = "Salida";
                    var col9 = "Entrada";
                    var col10 = "Hs.Indisp.";
                    var col11 = "Minutos Indis.";
                    var col12 = "Tipo Salida";
                    //var col13 = "Hs Prog";
                    //var col14 = "Hs Forz";
                    var col15 = "Penalizacion Programada";
                    var col16 = "Informo en termino";
                    var col17 = "Total Penalización";
                    //var col18 = "Cargo Real";
                    var col19 = "No Percibido";
                    var col20 = "Lucro Cesante";
                    var col21 = "Motivo";
                    var col22 = "";
                    var col23 = "Tarifa";
                    var col24 = "Resolucion";

                    hojaDetalle.encabezado = new List<string>();

                    hojaDetalle.encabezado.Add(col1);
                    hojaDetalle.encabezado.Add(col2);
                    hojaDetalle.encabezado.Add(col3);
                    hojaDetalle.encabezado.Add(col4);
                    hojaDetalle.encabezado.Add(col5);
                    hojaDetalle.encabezado.Add(col6);
                    hojaDetalle.encabezado.Add(col7);
                    hojaDetalle.encabezado.Add(col8);
                    hojaDetalle.encabezado.Add(col9);
                    hojaDetalle.encabezado.Add(col10);
                    hojaDetalle.encabezado.Add(col11);
                    hojaDetalle.encabezado.Add(col12);
                    //hojaDetalle.encabezado.Add(col13);
                    //hojaDetalle.encabezado.Add(col14);
                    hojaDetalle.encabezado.Add(col15);
                    hojaDetalle.encabezado.Add(col16);
                    hojaDetalle.encabezado.Add(col17);
                    //hojaDetalle.encabezado.Add(col18);
                    hojaDetalle.encabezado.Add(col19);
                    hojaDetalle.encabezado.Add(col20);
                    hojaDetalle.encabezado.Add(col21);
                    hojaDetalle.encabezado.Add(col22);
                    hojaDetalle.encabezado.Add(col23);
                    hojaDetalle.encabezado.Add(col24);
                    //if (lst.Count > 0)
                    //{
                    hojaDetalle.listaFilas = new List<DetalleExcelVM>();

                    foreach (var item in lineas)
                    {

                        DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                        Calendar cal = dfi.Calendar;
                        DateTime fechaActual = Convert.ToDateTime(item.fechaSalida);
                        string mesActual = fechaActual.ToString("MMMM");
                        hojaDetalle.unaFila = new DetalleExcelVM();
                        hojaDetalle.unaFila.datoPorFila = new List<string>();
                        hojaDetalle.unaFila.datoPorFila.Add(mesActual.ToUpperInvariant());
                        hojaDetalle.unaFila.datoPorFila.Add("Linea");
                        hojaDetalle.unaFila.datoPorFila.Add(item.idPagotran.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.nombreLinea);
                        hojaDetalle.unaFila.datoPorFila.Add(item.Ukv);
                        hojaDetalle.unaFila.datoPorFila.Add(item.longKm.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.montoXhora.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.fechaSalida.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.fechaEntrada.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.hsIndisponible.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.minutosIndisponible.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.tipoSalida);
                        //hojaDetalle.unaFila.datoPorFila.Add(item.hsProgramada != null ? item.hsProgramada.ToString() : "-");
                        //hojaDetalle.unaFila.datoPorFila.Add(item.hsForzada != null ? item.hsForzada.ToString() : "-");
                        hojaDetalle.unaFila.datoPorFila.Add(item.penalizacionProgramada.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.informoEnTermino == true ? "SI" : "NO");
                        hojaDetalle.unaFila.datoPorFila.Add(item.totalPenalizacion.ToString());
                        //hojaDetalle.unaFila.datoPorFila.Add(item.cargoReal.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.noPercibido.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.lucroCesante.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.motivo);
                        hojaDetalle.unaFila.datoPorFila.Add(" ");
                        hojaDetalle.unaFila.datoPorFila.Add(item.montoIndis.monto.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.montoIndis.resolucion);
                        hojaDetalle.listaFilas.Add(hojaDetalle.unaFila);

                        totalPenalizacion += item.totalPenalizacion;
                        totalNoPercibo += item.noPercibido;
                        totalLucroCesante += item.lucroCesante;
                    }

                    //De aca hago los totales 
                    hojaDetalle.totales.datoPorFilaTotales = new List<string>();
                    hojaDetalle.totales.datoPorFilaTotales.Add(totalPenalizacion.ToString());
                    hojaDetalle.totales.datoPorFilaTotales.Add(totalNoPercibo.ToString());
                    hojaDetalle.totales.datoPorFilaTotales.Add(totalLucroCesante.ToString());
                    hojaDetalle.listaFilas.Add(hojaDetalle.totales);



                    var montosLineas = db.IndisponibilidadMontoLineas.Where(x => x.activo == true).ToList();
                    foreach (var item in montosLineas)
                    {
                        hojaDetalle.unaFilaMonto = new DetalleExcelVM();
                        hojaDetalle.unaFilaMonto.datoPorFilaMonto = new List<string>();
                        hojaDetalle.unaFilaMonto.datoPorFilaMonto.Add("Kv: " + item.kv.ToString());
                        hojaDetalle.unaFilaMonto.datoPorFilaMonto.Add("$ " + item.monto.ToString());
                        hojaDetalle.unaFilaMonto.datoPorFilaMonto.Add(item.resolucion != "" ? item.resolucion : " - ");
                        hojaDetalle.listaFilas.Add(hojaDetalle.unaFilaMonto);

                    }


                    //}

                    var datoExcel = excel.GenerarExcelGeneralRemunerado(hojaDetalle, "Lineas");

                    return datoExcel;

                }
            }
            catch (Exception e)
            {
                var datoExcel = new HojaExcelVM();
                return datoExcel;
            }

        }




        #endregion

        #region Transformadores
        //Trafos Remunerados//
        public object obtenerTransformadores(int idResolucion, string fecha)
        {
            List<IndisRemuEquipoVM> listaTrafo = new List<IndisRemuEquipoVM>();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                if (fecha != null)
                {
                    DateTime fechaParse = DateTime.Parse(fecha);

                    if (idResolucion == 0)
                    {
                        listaTrafo = (from t in db.TranTransformador
                                      join e in db.EstEstacion on t.idEstacion equals e.idEstacion
                                      join r in db.IndisponibilidadRemuTrafo on t.idTransformador equals r.idTransformador
                                      into remu
                                      from r in remu.DefaultIfEmpty()  //esto puede llegar a traer trafos sin id que se hayan borrado de la bd de remuneraciones pero que sigan estando como remunerados en otra tabla
                                      where t.activo == true && t.remunerado == true && r.Anio == fechaParse.Year && r.Mes == fechaParse.Month
                                      select new IndisRemuEquipoVM
                                      {
                                          idTrafo = t.idTransformador,
                                          idIndisponibilidadesTrafo = r.idIndisponibilidadesTrafo,
                                          idPagotran = t.idPagotran,
                                          remunerado = r.remunerado,
                                          cargoRealxHora = r.cargoRealxHoras,
                                          xMes = r.xMes,
                                          origenRegulatorio = r.origenRegulatorio,
                                          activo = r.activo,
                                          muestroRealxHora = r.cargoRealxHoras != null ? "$ " + r.cargoRealxHoras : " ",
                                          muestroxMes = r.xMes != null ? "$ " + r.xMes : " ",


                                          nombreTransformador = t.nombreTransformador,
                                          potenciaAparente = t.potenciaAparente,
                                          potencias = t.Potencia1 + "/" + t.Potencia2 + "/" + t.Potencia3, //Pot [MVA] <- del excel
                                          tensiones = t.Tension1 + "/" + t.Tension2 + "/" + t.Tension3, // U[Kv] <- del excel
                                          estacion = new EstacionVM
                                          {
                                              idEstacion = e.idEstacion,
                                              nombreEstacion = e.nombreEstacion,
                                              codigoEstacion = e.codigoEstacion,
                                              activo = e.activo,
                                          }
                                      }).ToList();
                    }
                    else
                    {
                        listaTrafo = (from t in db.TranTransformador
                                      join e in db.EstEstacion on t.idEstacion equals e.idEstacion
                                      join r in db.IndisponibilidadRemuTrafo on t.idTransformador equals r.idTransformador
                                      into remu
                                      from r in remu.DefaultIfEmpty()  //esto puede llegar a traer trafos sin id que se hayan borrado de la bd de remuneraciones pero que sigan estando como remunerados en otra tabla
                                      where t.activo == true && t.remunerado == true && r.idResolucion == idResolucion
                                      && r.Anio == fechaParse.Year && r.Mes == fechaParse.Month
                                      select new IndisRemuEquipoVM
                                      {
                                          idTrafo = t.idTransformador,
                                          idIndisponibilidadesTrafo = r.idIndisponibilidadesTrafo,
                                          idPagotran = t.idPagotran,
                                          remunerado = r.remunerado,
                                          cargoRealxHora = r.cargoRealxHoras,
                                          xMes = r.xMes,
                                          origenRegulatorio = r.origenRegulatorio,
                                          activo = r.activo,
                                          muestroRealxHora = r.cargoRealxHoras != null ? "$ " + r.cargoRealxHoras : " ",
                                          muestroxMes = r.xMes != null ? "$ " + r.xMes : " ",


                                          nombreTransformador = t.nombreTransformador,
                                          potenciaAparente = t.potenciaAparente,
                                          potencias = t.Potencia1 + "/" + t.Potencia2 + "/" + t.Potencia3, //Pot [MVA] <- del excel
                                          tensiones = t.Tension1 + "/" + t.Tension2 + "/" + t.Tension3, // U[Kv] <- del excel
                                          estacion = new EstacionVM
                                          {
                                              idEstacion = e.idEstacion,
                                              nombreEstacion = e.nombreEstacion,
                                              codigoEstacion = e.codigoEstacion,
                                              activo = e.activo,
                                          }
                                      }).ToList();
                    }

                    foreach (var item in listaTrafo)
                    {
                        var fechaHoraActual = DateTime.Now;
                        item.indisponible = true;
                        var consultaTrafoIndisponible = db.IndisponibilidadTrafos.Where(x => x.idTrafoRemunerado == item.idIndisponibilidadesTrafo && x.activo == true && (x.fechaEntrada >= fechaHoraActual || x.fechaEntrada == null)).FirstOrDefault();
                        if (consultaTrafoIndisponible != null)
                        {

                            item.indisponible = false;
                            db.SaveChanges();

                        }
                        db.SaveChanges();
                    }
                }




            }
            var listaOrdenada = listaTrafo.OrderByDescending(x => x.idTrafo);
            object json = new { data = listaOrdenada };
            return json;
        }

        public IndisRemuEquipoVM ConsultarTrafoRemunerada(int? idIndisponibilidadesTrafo)
        {
            IndisRemuEquipoVM trafo = new IndisRemuEquipoVM();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                trafo = (from t in db.IndisponibilidadRemuTrafo
                         where t.activo == true && t.idIndisponibilidadesTrafo == idIndisponibilidadesTrafo
                         select new IndisRemuEquipoVM
                         {
                             remunerado = t.remunerado,
                             origenRegulatorio = t.origenRegulatorio,
                         }).FirstOrDefault();
            }
            return trafo;
        }

        public int ModificarTrafo(IndisRemuEquipoVM trafo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunTrafo = db.IndisponibilidadRemuTrafo.Where(x => x.idIndisponibilidadesTrafo == trafo.idIndisponibilidadesTrafo).FirstOrDefault();
                var trafoR = db.TranTransformador.Where(x => x.idTransformador == consultaunTrafo.idTransformador && x.activo == true).FirstOrDefault();

                var monto = db.IndisponibilidadMontoTrafo.Where(x => x.activo == true).FirstOrDefault();
                if (monto != null)
                {
                    consultaunTrafo.cargoRealxHoras = (trafoR.potenciaAparente * monto.monto * trafo.remunerado) / 100;

                    if (consultaunTrafo != null)
                    {
                        var fechaHoy = new DateTime(); var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;

                        int cantidadDias = DateTime.DaysInMonth(fechaHoy.Year, fechaHoy.Month);
                        consultaunTrafo.xMes = consultaunTrafo.cargoRealxHoras * (cantidadDias * 24);

                        consultaunTrafo.remunerado = trafo.remunerado;
                        consultaunTrafo.origenRegulatorio = trafo.origenRegulatorio;
                        db.SaveChanges();

                    }
                }
                else
                {
                    return 300;
                }


            }
            return 200;
        }

        // Montos Trafos //
        public Object ObtenerMontosTrafo()
        {
            List<MontoIndisponibilidadesVM> montoL;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                montoL = (from m in db.IndisponibilidadMontoTrafo
                          where m.activo == true
                          select new MontoIndisponibilidadesVM
                          {
                              idIndisponibilidadMontos = m.idIndisponibilidadMontosTrafo,
                              tipoEquipo = m.idTipoEquipo,
                              nombreMonto = m.nombreMonto,
                              monto = m.monto,
                              mostrarMonto = "$ " + m.monto,
                              activo = m.activo,
                              fecha = m.fechaMonto,
                              resolucion = m.resolucion,

                          }).ToList();
            }
            object json = new { data = montoL };
            return json;

        }
        public Object ObtenerMontosTrafoTodos()
        {
            List<MontoIndisponibilidadesVM> montoL;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                montoL = (from m in db.IndisponibilidadMontoTrafo
                          where m.activoHistorial == true
                          select new MontoIndisponibilidadesVM
                          {

                              idIndisponibilidadMontos = m.idIndisponibilidadMontosTrafo,
                              tipoEquipo = m.idTipoEquipo,
                              nombreMonto = m.nombreMonto,
                              monto = m.monto,
                              mostrarMonto = "$ " + m.monto,
                              activo = m.activo,
                              fecha = m.fechaMonto,
                              resolucion = m.resolucion,

                          }).ToList();
            }
            object json = new { data = montoL };
            return json;

        }

        public int AltaMontoTrafo(MontoIndisponibilidadesVM monto)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var montoExistente = db.IndisponibilidadMontoTrafo.Where(x => x.activo == true).FirstOrDefault();

                var consultaUnMonto = db.IndisponibilidadMontoTrafo.Where(x => x.idIndisponibilidadMontosTrafo == monto.idIndisponibilidadMontos).FirstOrDefault();
                if (consultaUnMonto == null && montoExistente == null)
                {
                    try
                    {
                        IndisponibilidadMontoTrafo mon = new IndisponibilidadMontoTrafo
                        {
                            idTipoEquipo = 1,
                            nombreMonto = monto.nombreMonto,
                            monto = monto.monto,
                            activo = true,
                            fechaMonto = monto.fechaDesde,
                            resolucion = monto.resolucion,
                        };
                        db.IndisponibilidadMontoTrafo.Add(mon);
                        db.SaveChanges();
                        var fechaHastaRes = monto.fechaHasta == null ? DateTime.Today : monto.fechaHasta;

                        while (monto.fechaDesde <= fechaHastaRes)
                        {
                            var trafosTodos = db.TranTransformador.Where(x => x.remunerado != null && x.remunerado == true).ToList();
                            if (trafosTodos.Any())
                            {
                                foreach (var item in trafosTodos)
                                {
                                    var trafoRemu = db.IndisponibilidadRemuTrafo.Where(x => x.remunerado != null && x.idTransformador == item.idTransformador).FirstOrDefault();
                                    var fechaHoy = monto.fechaDesde; /*var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;*/
                                    int cantidadDias = DateTime.DaysInMonth(fechaHoy.Value.Year, fechaHoy.Value.Month);
                                    var trafoR = db.TranTransformador.Where(x => x.idTransformador == item.idTransformador).FirstOrDefault();
                                    if (trafoRemu != null)
                                    {
                                        var nuevoTrafoRemu = new IndisponibilidadRemuTrafo
                                        {
                                            activo = true,
                                            cargoRealxHoras = (trafoR.potenciaAparente * monto.monto * trafoRemu.remunerado / 100) / 100,
                                            idTransformador = trafoR.idTransformador,
                                            idResolucion = mon.idIndisponibilidadMontosTrafo,
                                            origenRegulatorio = trafoRemu.origenRegulatorio,
                                            remunerado = trafoRemu.remunerado,
                                            Anio = fechaHoy.Value.Year,
                                            Mes = fechaHoy.Value.Month,
                                            xMes = (mon.monto * trafoR.potenciaAparente * trafoRemu.remunerado / 100) * cantidadDias * 24,

                                        };
                                        db.IndisponibilidadRemuTrafo.Add(nuevoTrafoRemu);
                                    }
                                    else
                                    {
                                        var remunerado = (bool)item.remunerado ? 100 : 0;
                                        var datosTrafos = db.IndisponibilidadRemuTrafo.Where(x => x.idTransformador == item.idTransformador).FirstOrDefault();
                                        var fechaHoy2 = monto.fechaDesde; //var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;
                                        var cargoRealxHoras = (item.potenciaAparente * monto.monto * remunerado) / 100;
                                        var xMes = cargoRealxHoras * (cantidadDias * 24);
                                        var nuevoTrafoRemu = new IndisponibilidadRemuTrafo
                                        {
                                            activo = true,
                                            cargoRealxHoras = (item.potenciaAparente * monto.monto * remunerado / 100) / 100,
                                            idTransformador = item.idTransformador,
                                            idResolucion = monto.idIndisponibilidadMontos,
                                            origenRegulatorio = datosTrafos != null ? datosTrafos.origenRegulatorio : DateTime.Today,
                                            remunerado = remunerado,
                                            Anio = fechaHoy.Value.Year,
                                            Mes = fechaHoy.Value.Month,
                                            xMes = xMes,

                                        };
                                        db.IndisponibilidadRemuTrafo.Add(nuevoTrafoRemu);
                                        db.SaveChanges();
                                    }
                                    //item.cargoRealxHoras = (trafoR.potenciaAparente * monto.monto * item.remunerado) / 100;

                                    //var fechaHoy = monto.fechaDesde; /*var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;*/

                                    //int cantidadDias = DateTime.DaysInMonth(fechaHoy.Value.Year, fechaHoy.Value.Month);
                                    //item.xMes = item.cargoRealxHoras * (cantidadDias * 24);

                                    db.SaveChanges();


                                }
                            }
                            var trafoIndisponibles = db.IndisponibilidadTrafos.Where(x => x.fechaEntrada.Value.Month == monto.fechaDesde.Value.Month && x.fechaEntrada.Value.Year == monto.fechaDesde.Value.Year).ToList();
                            foreach (var trafo in trafoIndisponibles)
                            {
                                ModificoTrafoIndisponibles(trafo, monto.idIndisponibilidadMontos);
                                //db.SaveChanges();
                            }
                            monto.fechaDesde = monto.fechaDesde.Value.AddMonths(1);
                            calculoMesesTrafo(monto.fechaDesde);
                        }



                        return 200;
                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                             ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        return 400;

                    }
                }
                else
                {
                    return 300; //ya existe ese dato
                }
            }
        }

        public int ModificarMontoTrafo(MontoIndisponibilidadesVM monto)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var idResolucion = monto.idIndisponibilidadMontos;

                var consultarUnMonto = db.IndisponibilidadMontoTrafo.Where(x => x.idIndisponibilidadMontosTrafo == monto.idIndisponibilidadMontos).FirstOrDefault();
                var FechaActual = DateTime.Today;
                var fechaDesde = consultarUnMonto.fechaMonto;
                if (consultarUnMonto != null)
                {
                    consultarUnMonto.monto = monto.monto;
                    consultarUnMonto.nombreMonto = monto.nombreMonto;
                    consultarUnMonto.fechaMonto = monto.fechaDesde;
                    consultarUnMonto.resolucion = monto.resolucion;
                    //consultarUnMonto.activo = false;
                    //consultarUnMonto.activoHistorial = true;
                    consultarUnMonto.fechaHasta = monto.fechaHasta;
                    db.SaveChanges();


                    //var trafoRemu = db.IndisponibilidadRemuTrafo.Where(x => x.remunerado != null).ToList(); //quitado condición que pide activo
                    var fechaHastaRes = monto.fechaHasta == null ? DateTime.Today : monto.fechaHasta;
                    while (monto.fechaDesde <= fechaHastaRes)
                    {
                        //var trafoR = db.TranTransformador.Where(x => x.remunerado == true && x.fechaCreacion <= monto.fechaDesde || x.fechaCreacion >= monto.fechaDesde).ToList(); //Eliminada condición que requeria transformador activo
                        var trafoR = db.TranTransformador
                                  .Where(x => x.remunerado == true
                                          //&& x.fechaCreacion >= monto.fechaDesde
                                          && x.fechaCreacion <= fechaHastaRes) // Asegurarse de que esté entre las fechas
                                  .GroupBy(x => x.idPagotran)
                                  .SelectMany(g => g) // Para traer todos los elementos de cada grupo
                                  .ToList();
                        //var s = trafoR.OrderBy(x => x.idTransformador);
                        //var t = trafoR.Where(x => x.idPagotran == 868).ToList();

                        foreach (var item in trafoR) //MODIFICO EL CARGO DE LAS Trafos QUE TIENEN ASIGNADO ESE KV
                        {
                           
                            var transformadorRemunerado = db.IndisponibilidadRemuTrafo.Where(x => x.idTransformador == item.idTransformador && x.idResolucion == idResolucion && x.Mes == monto.fechaDesde.Value.Month && x.Anio == monto.fechaDesde.Value.Year).FirstOrDefault();
                            var fechaHoy = monto.fechaDesde;
                           
                            if (transformadorRemunerado != null)
                            {
                                transformadorRemunerado.cargoRealxHoras = (item.potenciaAparente * monto.monto * transformadorRemunerado.remunerado) / 100;

                                transformadorRemunerado.activo = item.activo;
                                int cantidadDias = DateTime.DaysInMonth(fechaHoy.Value.Year, fechaHoy.Value.Month);
                                transformadorRemunerado.xMes = transformadorRemunerado.cargoRealxHoras * (cantidadDias * 24);
                                db.SaveChanges();

                            }
                            else
                            {
                                var remunerado = (bool)item.remunerado ? 100 : 0;
                                var datosTrafos = db.IndisponibilidadRemuTrafo.Where(x => x.idTransformador == item.idTransformador).FirstOrDefault();
                                var fechaHoy2 = monto.fechaDesde; //var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;
                                var cargoRealxHoras = (item.potenciaAparente * monto.monto * remunerado) / 100;
                                int cantidadDias = DateTime.DaysInMonth(fechaHoy2.Value.Year, fechaHoy2.Value.Month);
                                var xMes = cargoRealxHoras * (cantidadDias * 24);
                                var idTrafo = item.idTransformador;
                                var nuevoTrafoRemu = new IndisponibilidadRemuTrafo
                                {
                                    activo = item.activo,
                                    cargoRealxHoras = (item.potenciaAparente * monto.monto * remunerado / 100) / 100,
                                    idTransformador = idTrafo,
                                    idResolucion = monto.idIndisponibilidadMontos,
                                    origenRegulatorio = datosTrafos != null ? datosTrafos.origenRegulatorio : DateTime.Today,
                                    remunerado = remunerado,
                                    Anio = fechaHoy.Value.Year,
                                    Mes = fechaHoy.Value.Month,
                                    xMes = xMes,

                                };
                                db.IndisponibilidadRemuTrafo.Add(nuevoTrafoRemu);
                                db.SaveChanges();
                            }

                        }






                        //IndisponibilidadMontoTrafo NuevoMonto = new IndisponibilidadMontoTrafo()
                        //{
                        //    idTipoEquipo = 1,
                        //    nombreMonto = monto.nombreMonto,
                        //    monto = monto.monto,
                        //    fechaMonto = monto.fechaDesde,
                        //    resolucion = monto.resolucion,
                        //    activo = true,

                        //};
                        //db.IndisponibilidadMontoTrafo.Add(NuevoMonto);
                        //db.SaveChanges();

                        var trafoIndisponibles = db.IndisponibilidadTrafos.Where(x => x.fechaEntrada.Value.Month == monto.fechaDesde.Value.Month && x.fechaEntrada.Value.Year == monto.fechaDesde.Value.Year).ToList();
                        foreach (var trafo in trafoIndisponibles)
                        {
                            ModificoTrafoIndisponibles(trafo, monto.idIndisponibilidadMontos);
                            //db.SaveChanges();
                        }
                        monto.fechaDesde = monto.fechaDesde.Value.AddMonths(1);
                        calculoMesesTrafo(monto.fechaDesde);
                    }
                }
                else
                {
                    AltaMontoTrafo(monto);
                }
            }

            return 200;
        }

        public int EliminarMontoTrafo(MontoIndisponibilidadesVM monto)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnMonto = db.IndisponibilidadMontoTrafo.Where(x => x.idIndisponibilidadMontosTrafo == monto.idIndisponibilidadMontos).FirstOrDefault();
                if (consultaUnMonto != null)
                {
                    consultaUnMonto.activo = false;
                    consultaUnMonto.activoHistorial = true;
                    consultaUnMonto.fechaHasta = DateTime.Today;

                }
                db.SaveChanges();
            }
            return 200;
        }

        public MontoIndisponibilidadesVM ConsultarMontoTrafo(DateTime fechaDesdeMonto, DateTime? fechaHastaMonto, int? idMonto)
        {
            MontoIndisponibilidadesVM montoEncontrado = new MontoIndisponibilidadesVM();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnMonto = new IndisponibilidadMontoTrafo();
                if (idMonto != 0)
                {
                    consultaUnMonto = db.IndisponibilidadMontoTrafo.Where(x => x.idIndisponibilidadMontosTrafo == idMonto).FirstOrDefault();
                }
                else
                {
                    consultaUnMonto = db.IndisponibilidadMontoTrafo.Where(x => (x.fechaMonto <= fechaDesdeMonto && x.fechaHasta >= fechaHastaMonto) || (x.fechaMonto <= fechaDesdeMonto && x.activo == true)).FirstOrDefault();

                }

                if (consultaUnMonto != null)
                {
                    montoEncontrado = (from m in db.IndisponibilidadMontoTrafo
                                       where m.idIndisponibilidadMontosTrafo == consultaUnMonto.idIndisponibilidadMontosTrafo
                                       select new MontoIndisponibilidadesVM
                                       {
                                           idIndisponibilidadMontos = m.idIndisponibilidadMontosTrafo,
                                           monto = m.monto,
                                           mostrarMonto = "$ " + m.monto,
                                           nombreMonto = m.nombreMonto,
                                           resolucion = "Resolucion: " + m.resolucion,
                                       }).FirstOrDefault();
                }

            }
            return montoEncontrado;
        }

        // Historial //
        public object ObtenerHistorialTrafo()
        {
            List<MontoIndisponibilidadesVM> historialTrafo;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                historialTrafo = (from h in db.IndisponibilidadMontoTrafo
                                  where/* h.activo == false &&*/ h.activoHistorial == true
                                  select new MontoIndisponibilidadesVM
                                  {
                                      idIndisponibilidadHistorialMontTrafo = h.idIndisponibilidadMontosTrafo,
                                      nombreMonto = h.nombreMonto,
                                      monto = h.monto,
                                      fechaDesde = h.fechaMonto,
                                      fechaHasta = h.fechaHasta,
                                      resolucion = h.resolucion,
                                  }).ToList();
            }
            object json = new { data = historialTrafo };
            return json;
        }

        public int EliminarHistorialTrafo(MontoIndisponibilidadesVM historial)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarHistorialL = db.IndisponibilidadMontoTrafo.Where(x => x.idIndisponibilidadMontosTrafo == historial.idIndisponibilidadHistorialMontTrafo /*&& x.activoHistorial == true*/).FirstOrDefault();
                if (consultarHistorialL != null)
                {
                    consultarHistorialL.activoHistorial = false;
                    db.SaveChanges();
                }
                return 200;
            }
        }

        public int EliminarHistorialPto(MontoIndisponibilidadesVM historial)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarHistorialL = db.IndisponibilidadMontoPtosConexion.Where(x => x.idIndisponibilidadMontosPtosC == historial.idIndisponibilidadHistorialMontPtosC /*&& x.activoHistorial == true*/).FirstOrDefault();
                if (consultarHistorialL != null)
                {
                    consultarHistorialL.activoHistorial = false;
                    db.SaveChanges();
                }
                return 200;
            }
        }

        public int EliminarHistorialCapacitor(MontoIndisponibilidadesVM historial)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarHistorialL = db.IndisponibilidadMontoCapacitor.Where(x => x.idIndisponibilidadMontosCapacitor == historial.idIndisponibilidadHistorialMontTrafo /*&& x.activoHistorial == true*/).FirstOrDefault();
                if (consultarHistorialL != null)
                {
                    consultarHistorialL.activoHistorial = false;
                    db.SaveChanges();
                }
                return 200;
            }
        }

        public int ModificarHistorialMontoTrafo(MontoIndisponibilidadesVM monto)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarUnMonto = db.IndisponibilidadMontoTrafo.Where(x => x.idIndisponibilidadMontosTrafo == monto.idIndisponibilidadMontos && x.activoHistorial == true).FirstOrDefault();
                if (consultarUnMonto != null)
                {
                    consultarUnMonto.monto = monto.monto;
                    consultarUnMonto.resolucion = monto.resolucion;
                    if (monto.fechaDesdeF != "" && monto.fechaDesdeF != "Sin fecha")
                    {
                        DateTime fecha = DateTime.Parse(monto.fechaDesdeF);

                        consultarUnMonto.fechaMonto = fecha;
                    }
                    if (monto.fechaHastaF != "" && monto.fechaHastaF != "Sin fecha")
                    {
                        consultarUnMonto.fechaHasta = monto.fechaHasta;
                        if (consultarUnMonto.fechaHasta < DateTime.Today)
                        {
                            consultarUnMonto.activo = false;
                        }
                    }
                    db.SaveChanges();
                }

                ModificarMontoTrafo(monto);

            }

            return 200;
        }

        // Trafos Indisponibles //

        public object obtenerTrafosIndisponibles(DateTime fechaDesde, DateTime fechaHasta)
        {
            List<IndisRemuEquipoVM> listaTrafoIndisponibles;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                listaTrafoIndisponibles = (from t in db.TranTransformador
                                           join e in db.EstEstacion on t.idEstacion equals e.idEstacion
                                           join r in db.IndisponibilidadRemuTrafo on t.idTransformador equals r.idTransformador into remu
                                           from r in remu.DefaultIfEmpty()
                                           join it in db.IndisponibilidadTrafos on r.idIndisponibilidadesTrafo equals it.idTrafoRemunerado into traR
                                           from it in traR.DefaultIfEmpty()
                                           join m in db.IndisponibilidadMontoTrafo on it.idMonto equals m.idIndisponibilidadMontosTrafo
                                           where t.activo == true && t.remunerado == true && it.activo == true && r.activo == true && (it.fechaSalida >= fechaDesde && it.fechaEntrada <= fechaHasta)
                                           select new IndisRemuEquipoVM
                                           {
                                               idIndisponibilidadesTrafo = it.idIndisponibilidadTrafo,
                                               idTrafo = t.idTransformador,
                                               idIndisponibilidadTrafo = r.idIndisponibilidadesTrafo,
                                               idPagotran = t.idPagotran,
                                               cargoRealxHora = it.montoXHora,
                                               muestroRealxHora = it.montoXHora != null ? "$ " + it.montoXHora : " ",
                                               fechaSalida = it.fechaSalida,
                                               fechaEntrada = it.fechaEntrada,
                                               hsIndisponible = it.hsIndisponibles,
                                               minutosIndisponible = it.minutosIndisponibles,
                                               tipoSalida = it.tipoSalida,
                                               activo = r.activo,
                                               CR = it.CR,
                                               ENS = it.ENS,
                                               informoEnTermino = it.informoEnTermino,
                                               MuestroinformoEnTermino = it.informoEnTermino == true ? "Si" : "NO",
                                               motivo = it.motivo,
                                               totalPenalizacion = it.totalPenalizado,
                                               noPercibido = it.noPercibido,
                                               muestroENS = it.ENS == true ? "SI" : "NO",

                                               nombreTransformador = t.nombreTransformador,
                                               potenciaAparente = t.potenciaAparente,
                                               potencias = t.Potencia1 + "/" + t.Potencia2 + "/" + t.Potencia3, //Pot [MVA] <- del excel
                                               tensiones = t.Tension1 + "/" + t.Tension2 + "/" + t.Tension3, // U[Kv] <- del excel
                                               estacion = new EstacionVM
                                               {
                                                   idEstacion = e.idEstacion,
                                                   nombreEstacion = e.nombreEstacion,
                                                   codigoEstacion = e.codigoEstacion,
                                                   activo = e.activo,
                                               },

                                               idMonto = m.idIndisponibilidadMontosTrafo,

                                           }).ToList();

                foreach (var item in listaTrafoIndisponibles)
                {
                    var novIndisTrafo = db.NovNovedadIndisponibilidad.Where(x => x.idTipoEquipo == 1 && x.activo == true && x.idIndisponibilidadRemu == item.idIndisponibilidadesTrafo).FirstOrDefault();
                    if (novIndisTrafo != null)
                    {
                        item.idTipoIndisponibilidad = novIndisTrafo.idTipoIndisponibilidad;
                    }
                    else
                    {
                        item.idTipoIndisponibilidad = -1;
                    }
                }

            }
            listaTrafoIndisponibles = listaTrafoIndisponibles.OrderByDescending(x => x.idIndisponibilidadesTrafo).ToList();
            object json = new { data = listaTrafoIndisponibles };
            return json;
        }

        public int AltaTrafosIndisponibles(IndisRemuEquipoVM trafo, DateTime fechaDesdeMonto, DateTime? fechaHastaMonto)
        {

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnTrafo = db.IndisponibilidadTrafos.Where(x => x.idTrafo == trafo.idTrafo && x.fechaSalida == trafo.fechaSalida && x.fechaEntrada == trafo.fechaEntrada && x.activo == true).FirstOrDefault();
                if (consultaUnTrafo == null)
                {
                    try
                    {
                        var trafoRemunerado = db.IndisponibilidadRemuTrafo.Where(x => x.idTransformador == trafo.idTrafo && x.Mes == fechaHastaMonto.Value.Month && x.Anio == fechaHastaMonto.Value.Year).FirstOrDefault();
                        var transformador = db.TranTransformador.Where(x => x.idTransformador == trafo.idTrafo && x.activo == true).FirstOrDefault();
                        var monto = db.IndisponibilidadMontoTrafo.Where(x => (x.fechaMonto <= fechaDesdeMonto && x.fechaHasta >= fechaHastaMonto) || (x.fechaMonto <= fechaDesdeMonto && x.fechaHasta == null)).FirstOrDefault();
                        //var utilidad = db.IndisponibilidadesUtilidades.Where(x => x.idTipoEquipo == 1 && x.activo == true).FirstOrDefault();
                        var factorK = db.IndisponibilidadesUtilidades.Where(x => x.idTipoEquipo == 1 && x.activo == true).FirstOrDefault();

                        if (monto == null)
                        {
                            return 303;
                        }


                        DateTime fechaMal = Convert.ToDateTime("1/1/0001 00:00:00");
                        if (trafo?.fechaEntrada != fechaMal)
                        {
                            var cargoRealxHoras = trafoRemunerado != null ? trafoRemunerado.cargoRealxHoras : 0;

                            var horasIndisponible = (trafo.fechaEntrada - trafo.fechaSalida)?.TotalHours;
                            var minDisponibles = (trafo.fechaEntrada - trafo.fechaSalida)?.TotalMinutes;

                            decimal? totalPenalizacion = null;
                            int? Coeficiente = factorK.valor;
                            decimal? hsForzada = null;

                            //CR Y ENS NO TIENE NINGUN CALCULO

                            decimal? kPyENS = null;
                            if (trafo.tipoSalida == "P" || trafo.tipoSalida == "RP")
                            {
                                if (trafo.ENS == true)
                                {
                                    kPyENS = Convert.ToDecimal(Coeficiente * 0.1 * 1);
                                }
                                else
                                {
                                    kPyENS = Convert.ToDecimal(Coeficiente * 0.1 * 0.1);
                                }
                            }
                            else
                            {
                                if (trafo.ENS == true)
                                {
                                    kPyENS = Convert.ToDecimal(Coeficiente * 1 * 1);
                                }
                                else
                                {
                                    kPyENS = Convert.ToDecimal(Coeficiente * 1 * 0.1);
                                }
                            }

                            bool AUT;
                            decimal? penalizacionPrograma = null;
                            decimal? reduccionProgramada = null;
                            decimal? penalizacionForzada1 = 0;
                            decimal? penalizacionForzada2 = 0;
                            decimal? reduccionForzada1 = 0;
                            decimal? reduccionForzada2 = 0;
                            decimal? forzadoAutorizado = 0;
                            decimal? reduccionForzadoAutorizado = 0;

                            //decimal? tension1 = 0, tension2 = 0, tension3 = 0;
                            //if (transformador.Tension1 != null)
                            //{
                            //    tension1 = transformador.Tension1;
                            //}
                            //if (transformador.Tension2 != null)
                            //{
                            //    tension2 = transformador.Tension2;
                            //}
                            //if (transformador.Tension3 != null)
                            //{
                            //    tension3 = transformador.Tension3;
                            //}

                            //decimal? CRt = ((tension1 - tension2 - tension3) / tension1) * 100;

                            decimal? potencia1 = 0, potencia2 = 0, potencia3 = 0;
                            if (transformador.Potencia1 != null)
                            {
                                potencia1 = transformador.Potencia1;
                            }
                            if (transformador.Potencia2 != null)
                            {
                                potencia2 = transformador.Potencia2;
                            }
                            if (transformador.Potencia3 != null)
                            {
                                potencia3 = transformador.Potencia3;
                            }

                            decimal? CRt = 0;
                            var indisponibilidad = trafo.idTipoIndisponibilidad;
                            //var indisponibilidad = db.NovNovedadIndisponibilidad.Where(x => x.idNovedadIndisponibilidad == trafo.idIndisponibilidad && x.activo == true).FirstOrDefault()?.idTipoIndisponibilidad;

                            if (indisponibilidad == 0)
                            {
                                //indisponibilidad = db.NovNovedadIndisponibilidad.Where(x => x.idNovedadIndisponibilidad == trafo.idIndisponibilidad && x.activo == true).FirstOrDefault().idTipoIndisponibilidad;

                            }
                            if (indisponibilidad == 1)
                            {
                                CRt = 100;
                            }
                            else if (indisponibilidad == 2)
                            {
                                CRt = (potencia2 / (potencia2 + potencia3));
                            }
                            else if (indisponibilidad == 3)
                            {
                                CRt = (potencia3 / (potencia2 + potencia3));
                            }


                            if (CRt != 0 && CRt != null)
                            {
                                if (trafo.tipoSalida == "P")
                                {
                                    AUT = true;
                                    var redondeo = Math.Truncate((decimal)(minDisponibles / 60) * 100) / 100;
                                    penalizacionPrograma = Convert.ToDecimal(cargoRealxHoras * kPyENS * redondeo);

                                    totalPenalizacion = penalizacionPrograma;
                                }
                                else if (trafo.tipoSalida == "RP")
                                {
                                    AUT = true;
                                    var redondeo = Math.Truncate((decimal)(minDisponibles / 60) * 100) / 100;
                                    reduccionProgramada = Convert.ToDecimal(cargoRealxHoras * kPyENS * redondeo * CRt / 100);

                                    totalPenalizacion = reduccionProgramada;
                                }
                                else if (trafo.tipoSalida == "F")
                                {
                                    AUT = false;
                                    var redondeo = Math.Truncate((decimal)(minDisponibles / 60) * 100) / 100;
                                    penalizacionForzada1 = Convert.ToDecimal(cargoRealxHoras * kPyENS);
                                    penalizacionForzada2 = Convert.ToDecimal(cargoRealxHoras * kPyENS * redondeo);

                                    totalPenalizacion = penalizacionForzada1 + penalizacionForzada2;


                                }
                                else if (trafo.tipoSalida == "R")
                                {
                                    AUT = false;
                                    var redondeo = Math.Truncate((decimal)(minDisponibles / 60) * 100) / 100;
                                    reduccionForzada1 = Convert.ToDecimal(cargoRealxHoras * kPyENS * CRt / 100);
                                    reduccionForzada2 = Convert.ToDecimal(cargoRealxHoras * kPyENS * redondeo * CRt / 100);

                                    totalPenalizacion = reduccionForzada1 + reduccionForzada2;

                                }
                                else if (trafo.tipoSalida == "FA")
                                {
                                    var redondeo = Math.Truncate((decimal)(minDisponibles / 60) * 100) / 100;
                                    forzadoAutorizado = Convert.ToDecimal(cargoRealxHoras * kPyENS * redondeo);

                                    totalPenalizacion = forzadoAutorizado;

                                }
                                else if (trafo.tipoSalida == "RFA")
                                {
                                    var redondeo = Math.Truncate((decimal)(minDisponibles / 60) * 100) / 100;
                                    reduccionForzadoAutorizado = Convert.ToDecimal(cargoRealxHoras * kPyENS * CRt / 100);

                                    totalPenalizacion = reduccionForzadoAutorizado;
                                }

                                if (trafo.informoEnTermino == true)
                                {
                                    totalPenalizacion = totalPenalizacion * 1;
                                }
                                else
                                {
                                    totalPenalizacion = totalPenalizacion * 2;
                                }


                                decimal? noPercibido = cargoRealxHoras * Convert.ToDecimal(horasIndisponible);
                                decimal? lucroCesante = noPercibido + totalPenalizacion;

                                IndisponibilidadTrafos nuevaIndisp = new IndisponibilidadTrafos
                                {
                                    idTrafoRemunerado = trafoRemunerado.idIndisponibilidadesTrafo,
                                    idTrafo = trafo.idTrafo,
                                    idMonto = monto.idIndisponibilidadMontosTrafo,
                                    montoXHora = cargoRealxHoras,
                                    fechaSalida = trafo.fechaSalida,
                                    fechaEntrada = trafo.fechaEntrada,
                                    hsIndisponibles = Convert.ToDecimal(horasIndisponible),
                                    minutosIndisponibles = Convert.ToDecimal(minDisponibles),
                                    tipoSalida = trafo.tipoSalida,
                                    CR = CRt,
                                    ENS = trafo.ENS,
                                    informoEnTermino = trafo.informoEnTermino,
                                    totalPenalizado = totalPenalizacion,
                                    noPercibido = noPercibido,
                                    motivo = trafo.motivo,
                                    activo = true,
                                    idIndisponibilidad = trafo.idNovedadIndisponibilidad
                                };
                                db.IndisponibilidadTrafos.Add(nuevaIndisp);
                                db.SaveChanges();
                                var tra = db.IndisponibilidadTrafos.Where(x => x.idTrafo == trafo.idTrafo && x.idMonto == monto.idIndisponibilidadMontosTrafo).FirstOrDefault();

                                //VIENE DE LO DE INDISPONIBILIDADES DEL UCIEL
                                if (trafo.noCargar != true)//El no cargar es para que no cree otrs indisponibilidad por que viene de lo del Uciel
                                {
                                    ServicioIndisponibilidad servicioIndisUciel = new ServicioIndisponibilidad();
                                    NovNovedadIndisponibilidad indis = new NovNovedadIndisponibilidad { };
                                    indis.activo = true;
                                    indis.FechaEntrada = trafo.fechaEntrada;
                                    indis.FechaSalida = trafo.fechaSalida;
                                    indis.PorcentajeIndisponible = Convert.ToDouble(totalPenalizacion);
                                    indis.idEquipo = trafo.idTrafo;
                                    indis.idTipoEquipo = 1;
                                    indis.EnergiaNoSuministrada = trafo.ENS;
                                    indis.idTipoIndisponibilidad = trafo.idTipoIndisponibilidad; //Es solo para lo del uciel
                                    indis.descripcion = trafo.motivo;
                                    indis.fueraDeServicio = true;
                                    indis.idIndisponibilidadRemu = tra.idIndisponibilidadTrafo;
                                    if (trafo.tipoSalida == "F")
                                    {
                                        indis.itn = 1;
                                    }
                                    else if (trafo.tipoSalida == "FA")
                                    {
                                        indis.itn = 2;
                                    }
                                    else if (trafo.tipoSalida == "P")
                                    {
                                        indis.itn = 3;
                                    }
                                    else if (trafo.tipoSalida == "ST")
                                    {
                                        indis.itn = 4;
                                    }
                                    else
                                    {
                                        indis.itn = 99;
                                    }
                                    db.NovNovedadIndisponibilidad.Add(indis);
                                    db.SaveChanges();
                                    var ultimaIndi = db.NovNovedadIndisponibilidad.Where(x => x.idIndisponibilidadRemu == tra.idIndisponibilidadTrafo && x.activo == true && x.idTipoEquipo == 1).FirstOrDefault();
                                    tra.idIndisponibilidad = ultimaIndi.idNovedadIndisponibilidad;
                                }

                                db.SaveChanges();
                                calculoMesesTrafo(trafo.fechaSalida);
                                return 200;
                            }
                        }
                        else
                        {

                            ModificoTrafoIndisponibles(consultaUnTrafo, monto.idIndisponibilidadMontosTrafo);

                            //calculoMesesTrafo(trafo.fechaSalida);
                            return 200;
                        }

                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }
                return 200;
            }
        }

        public int ModificarTrafoIndisponibles(IndisRemuEquipoVM trafo, DateTime fechaDesdeMonto, DateTime? fechaHastaMonto)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var idResolucion = db.IndisponibilidadRemuTrafo.Where(x => x.idTransformador == trafo.idTrafo && x.Mes == fechaHastaMonto.Value.Month && x.Anio == fechaHastaMonto.Value.Year).FirstOrDefault();
                var trafoI = db.IndisponibilidadTrafos.Where(x => x.idIndisponibilidadTrafo == trafo.idIndisponibilidadTrafo).FirstOrDefault();
                if (idResolucion != null && trafoI != null)
                {
                    ModificoTrafoIndisponibles(trafoI, idResolucion.idResolucion);

                    calculoMesesTrafo(fechaHastaMonto);
                }
                else
                {
                    return 400;
                }


            }

            return 200;
        }

        public int EliminarTrafoIndisponible(IndisRemuEquipoVM trafo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaTrafoIndis = db.IndisponibilidadTrafos.Where(x => x.idIndisponibilidadTrafo == trafo.idIndisponibilidadTrafo && x.activo == true).FirstOrDefault();
                if (consultaTrafoIndis != null)
                {
                    consultaTrafoIndis.activo = false;
                    db.SaveChanges();

                    var trafoNovIndis = db.NovNovedadIndisponibilidad.Where(x => x.idIndisponibilidadRemu == consultaTrafoIndis.idIndisponibilidad && x.activo == true && x.idTipoEquipo == 1).FirstOrDefault();
                    if (trafoNovIndis != null)
                    {
                        trafoNovIndis.activo = false;
                        db.SaveChanges();
                    }

                }
                calculoMesesTrafo(consultaTrafoIndis.fechaSalida);
                return 200;
            }
        }

        public int ModificoTrafoIndisponibles(IndisponibilidadTrafos trafo, int? idResolucion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunTrafoRemu = db.IndisponibilidadTrafos.Where(x => x.idIndisponibilidadTrafo == trafo.idIndisponibilidadTrafo && x.activo == true).FirstOrDefault();
                var factorK = db.IndisponibilidadesUtilidades.Where(x => x.idTipoEquipo == 1 && x.activo == true).FirstOrDefault();
                var trafoRemunerado = db.IndisponibilidadRemuTrafo.Where(x => x.idTransformador == trafo.idTrafo && x.idResolucion == idResolucion
                                       && x.Mes == trafo.fechaSalida.Value.Month && x.Anio == trafo.fechaSalida.Value.Year).FirstOrDefault();

                if (consultaunTrafoRemu != null)
                {
                    var transformador = db.TranTransformador.Where(x => x.idTransformador == trafo.idTrafo && x.remunerado == true).FirstOrDefault();
                    //var monto = db.IndisponibilidadMontoTrafo.Where(x => (x.fechaMonto <= fechaDesdeMonto && x.fechaHasta >= fechaHastaMonto) || (x.fechaMonto <= fechaDesdeMonto && x.fechaHasta == null)).FirstOrDefault();
                    //var kv = db.GralTension.Where(x => x.activo == true && x.idTension == transformador.Tension1).FirstOrDefault();
                    //var UkvInt = Convert.ToInt32(kv.tension);

                    if (transformador != null)
                    {
                        var monto = new IndisponibilidadMontoTrafo();
                        if (trafo.fechaEntrada >= DateTime.Now)
                        {
                            monto = db.IndisponibilidadMontoTrafo.Where(x => x.activo == true).FirstOrDefault();
                            consultaunTrafoRemu.idMonto = monto.idIndisponibilidadMontosTrafo;
                        }
                        else//esto es mas que nada para el historial
                        {
                            monto = db.IndisponibilidadMontoTrafo.Where(x => x.idIndisponibilidadMontosTrafo == trafo.idMonto).FirstOrDefault();
                        }
                        decimal? montoXhoraRemunerado = 0;

                        DateTime fechaMal = Convert.ToDateTime("1/1/0001 00:00:00");
                        if (trafo?.fechaEntrada != fechaMal)
                        {
                            var cargoRealxHoras = trafoRemunerado != null ? trafoRemunerado.cargoRealxHoras : 0;

                            var horasIndisponible = (trafo.fechaEntrada - trafo.fechaSalida)?.TotalHours;
                            var minDisponibles = (trafo.fechaEntrada - trafo.fechaSalida)?.TotalMinutes;

                            decimal? totalPenalizacion = null;
                            int? Coeficiente = factorK.valor;
                            decimal? hsForzada = null;

                            //CR Y ENS NO TIENE NINGUN CALCULO

                            decimal? kPyENS = null;
                            if (trafo.tipoSalida == "P" || trafo.tipoSalida == "RP")
                            {
                                if (trafo.ENS == true)
                                {
                                    kPyENS = Convert.ToDecimal(Coeficiente * 0.1 * 1);
                                }
                                else
                                {
                                    kPyENS = Convert.ToDecimal(Coeficiente * 0.1 * 0.1);
                                }
                            }
                            else
                            {
                                if (trafo.ENS == true)
                                {
                                    kPyENS = Convert.ToDecimal(Coeficiente * 1 * 1);
                                }
                                else
                                {
                                    kPyENS = Convert.ToDecimal(Coeficiente * 1 * 0.1);
                                }
                            }

                            bool AUT;
                            decimal? penalizacionPrograma = null;
                            decimal? reduccionProgramada = null;
                            decimal? penalizacionForzada1 = 0;
                            decimal? penalizacionForzada2 = 0;
                            decimal? reduccionForzada1 = 0;
                            decimal? reduccionForzada2 = 0;
                            decimal? forzadoAutorizado = 0;
                            decimal? reduccionForzadoAutorizado = 0;

                            //decimal? tension1 = 0, tension2 = 0, tension3 = 0;
                            //if (transformador.Tension1 != null)
                            //{
                            //    tension1 = transformador.Tension1;
                            //}
                            //if (transformador.Tension2 != null)
                            //{
                            //    tension2 = transformador.Tension2;
                            //}
                            //if (transformador.Tension3 != null)
                            //{
                            //    tension3 = transformador.Tension3;
                            //}

                            //decimal? CRt = ((tension1 - tension2 - tension3) / tension1) * 100;

                            decimal? potencia1 = 0, potencia2 = 0, potencia3 = 0;
                            if (transformador.Potencia1 != null)
                            {
                                potencia1 = transformador.Potencia1;
                            }
                            if (transformador.Potencia2 != null)
                            {
                                potencia2 = transformador.Potencia2;
                            }
                            if (transformador.Potencia3 != null)
                            {
                                potencia3 = transformador.Potencia3;
                            }

                            decimal? CRt = 0;
                            var indisponibilidad = db.NovNovedadIndisponibilidad.Where(x => x.idNovedadIndisponibilidad == trafo.idIndisponibilidad && x.activo == true).FirstOrDefault()?.idTipoIndisponibilidad;

                            if (indisponibilidad == 0)
                            {
                                indisponibilidad = db.NovNovedadIndisponibilidad.Where(x => x.idNovedadIndisponibilidad == trafo.idIndisponibilidad && x.activo == true).FirstOrDefault().idTipoIndisponibilidad;

                            }
                            if (indisponibilidad == 1)
                            {
                                CRt = 100;
                            }
                            else if (indisponibilidad == 2)
                            {
                                CRt = (potencia2 / (potencia2 + potencia3));
                            }
                            else if (indisponibilidad == 3)
                            {
                                CRt = (potencia3 / (potencia2 + potencia3));
                            }


                            if (CRt != 0 && CRt != null)
                            {
                                if (trafo.tipoSalida == "P")
                                {
                                    AUT = true;
                                    var redondeo = Math.Truncate((decimal)(minDisponibles / 60) * 100) / 100;
                                    penalizacionPrograma = Convert.ToDecimal(cargoRealxHoras * kPyENS * redondeo);

                                    totalPenalizacion = penalizacionPrograma;
                                }
                                else if (trafo.tipoSalida == "RP")
                                {
                                    AUT = true;
                                    var redondeo = Math.Truncate((decimal)(minDisponibles / 60) * 100) / 100;
                                    reduccionProgramada = Convert.ToDecimal(cargoRealxHoras * kPyENS * redondeo * CRt / 100);

                                    totalPenalizacion = reduccionProgramada;
                                }
                                else if (trafo.tipoSalida == "F")
                                {
                                    AUT = false;
                                    var redondeo = Math.Truncate((decimal)(minDisponibles / 60) * 100) / 100;
                                    penalizacionForzada1 = Convert.ToDecimal(cargoRealxHoras * kPyENS);
                                    penalizacionForzada2 = Convert.ToDecimal(cargoRealxHoras * kPyENS * redondeo);

                                    totalPenalizacion = penalizacionForzada1 + penalizacionForzada2;


                                }
                                else if (trafo.tipoSalida == "R")
                                {
                                    AUT = false;
                                    var redondeo = Math.Truncate((decimal)(minDisponibles / 60) * 100) / 100;
                                    reduccionForzada1 = Convert.ToDecimal(cargoRealxHoras * kPyENS * CRt / 100);
                                    reduccionForzada2 = Convert.ToDecimal(cargoRealxHoras * kPyENS * redondeo * CRt / 100);

                                    totalPenalizacion = reduccionForzada1 + reduccionForzada2;

                                }
                                else if (trafo.tipoSalida == "FA")
                                {
                                    var redondeo = Math.Truncate((decimal)(minDisponibles / 60) * 100) / 100;
                                    forzadoAutorizado = Convert.ToDecimal(cargoRealxHoras * kPyENS * redondeo);

                                    totalPenalizacion = forzadoAutorizado;

                                }
                                else if (trafo.tipoSalida == "RFA")
                                {
                                    var redondeo = Math.Truncate((decimal)(minDisponibles / 60) * 100) / 100;
                                    reduccionForzadoAutorizado = Convert.ToDecimal(cargoRealxHoras * kPyENS * CRt / 100);

                                    totalPenalizacion = reduccionForzadoAutorizado;
                                }

                                if (trafo.informoEnTermino == true)
                                {
                                    totalPenalizacion = totalPenalizacion * 1;
                                }
                                else
                                {
                                    totalPenalizacion = totalPenalizacion * 2;
                                }


                                decimal? noPercibido = cargoRealxHoras * Convert.ToDecimal(horasIndisponible);
                                decimal? lucroCesante = noPercibido + totalPenalizacion;

                                consultaunTrafoRemu.idTrafoRemunerado = trafoRemunerado.idIndisponibilidadesTrafo;
                                consultaunTrafoRemu.idTrafo = trafo.idTrafo;
                                consultaunTrafoRemu.idMonto = monto.idIndisponibilidadMontosTrafo;
                                consultaunTrafoRemu.montoXHora = cargoRealxHoras;
                                consultaunTrafoRemu.fechaSalida = trafo.fechaSalida;
                                consultaunTrafoRemu.fechaEntrada = trafo.fechaEntrada;
                                consultaunTrafoRemu.hsIndisponibles = Convert.ToDecimal(horasIndisponible);
                                consultaunTrafoRemu.minutosIndisponibles = Convert.ToDecimal(minDisponibles);
                                consultaunTrafoRemu.tipoSalida = trafo.tipoSalida;
                                consultaunTrafoRemu.CR = CRt;
                                consultaunTrafoRemu.ENS = trafo.ENS;
                                consultaunTrafoRemu.informoEnTermino = trafo.informoEnTermino;
                                consultaunTrafoRemu.totalPenalizado = totalPenalizacion;
                                consultaunTrafoRemu.noPercibido = noPercibido;
                                consultaunTrafoRemu.motivo = trafo.motivo;
                                //trafoIndisponible.idIndisponibilidad = trafo.idNovedadIndisponibilidad;
                                db.SaveChanges();


                                //calculoMesesTrafo(trafo.fechaSalida);
                                return 200;
                            }
                            else { return 404; }
                        }
                    }
                }

            }
            calculoMesesTrafo(trafo.fechaSalida);
            return 200;
        }

        //FactorK
        public IndisRemuEquipoVM obtenerUtilidadTrafo()
        {
            IndisRemuEquipoVM factorPenalizacion = new IndisRemuEquipoVM();

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                factorPenalizacion = (from f in db.IndisponibilidadesUtilidades
                                      where f.activo == true && f.idTipoEquipo == 1
                                      select new IndisRemuEquipoVM
                                      {
                                          idUtilidad = f.idUtilidad,
                                          idTipoEquipo = f.idTipoEquipo,
                                          nombreUtilidad = f.nombreUtilidad,
                                          valorUtilidad = f.valor,
                                          activo = f.activo,
                                      }).FirstOrDefault();

            }
            return factorPenalizacion;
        }

        public int modificarUtilidadTrafo(int utilidad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUtilidad = db.IndisponibilidadesUtilidades.Where(x => x.idTipoEquipo == 1 && x.activo == true).FirstOrDefault();
                if (consultaUtilidad != null)
                {
                    consultaUtilidad.valor = utilidad;
                    db.SaveChanges();
                }
                return 200;
            }
        }

        //Reporte en Excel
        public HojaExcelVM GenerarReporteExcelTrafo(string fechaDesde, string fechaHasta)
        {
            var fechaDesdeDate = Convert.ToDateTime(fechaDesde);
            var fechaHastaDate = Convert.ToDateTime(fechaHasta);
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    ServicioEquipo servEquipo = new ServicioEquipo();
                    List<IndisRemuEquipoVM> trafos = new List<IndisRemuEquipoVM>();
                    trafos = (from i in db.IndisponibilidadTrafos
                              join r in db.IndisponibilidadRemuTrafo on i.idTrafoRemunerado equals r.idIndisponibilidadesTrafo
                              join t in db.TranTransformador on i.idTrafo equals t.idTransformador
                              join m in db.IndisponibilidadMontoTrafo on i.idMonto equals m.idIndisponibilidadMontosTrafo
                              where i.activo == true && fechaDesdeDate <= i.fechaSalida && fechaHastaDate >= i.fechaEntrada
                              select new { i, r, m, t }).AsEnumerable().Select(x => new IndisRemuEquipoVM
                              {
                                  idIndisponibilidadesTrafo = x.i.idIndisponibilidadTrafo,
                                  idTrafosRemu = x.i.idTrafoRemunerado,
                                  idTrafo = x.t.idTransformador,
                                  idMonto = x.m.idIndisponibilidadMontosTrafo,
                                  montoXhora = x.i.montoXHora,
                                  fechaSalida = x.i.fechaSalida,
                                  fechaEntrada = x.i.fechaEntrada,
                                  hsIndisponible = x.i.hsIndisponibles,
                                  minutosIndisponible = x.i.minutosIndisponibles,
                                  tipoSalida = x.i.tipoSalida,
                                  activo = x.i.activo,
                                  CR = x.i.CR,
                                  ENS = x.i.ENS,
                                  informoEnTermino = x.i.informoEnTermino,
                                  totalPenalizacion = x.i.totalPenalizado,
                                  noPercibido = x.i.noPercibido,
                                  motivo = x.i.motivo,
                                  //Trafo
                                  idPagotran = x.t.idPagotran,
                                  nombreTransformador = x.t.nombreTransformador,
                                  potenciaAparente = x.t.potenciaAparente,
                                  potencias = x.t.Potencia1 + "/" + x.t.Potencia2 + "/" + x.t.Potencia3, //Pot [MVA] <- del excel
                                  tensiones = x.t.Tension1 + "/" + x.t.Tension2 + "/" + x.t.Tension3, // U[Kv] <- del excel
                                                                                                      //Monto
                                  montoIndis = new MontoIndisponibilidadesVM
                                  {
                                      monto = x.m.monto,
                                      nombreMonto = x.m.nombreMonto,
                                      resolucion = x.m.resolucion,
                                  }
                              }).OrderByDescending(x => x.fechaSalida).ToList();


                    GenerarExcel excel = new GenerarExcel();
                    HojaExcelVM hojaDetalle = new HojaExcelVM();
                    hojaDetalle.totales = new DetalleExcelVM();

                    decimal? totalPenalizacion = 0;
                    decimal? totalNoPercibo = 0;
                    decimal? totalLucroCesante = 0;

                    var col1 = "Mes";
                    var col2 = "Equipo";
                    var col3 = "ID";
                    var col4 = "Transformador";
                    var col5 = "POT[MVA]";
                    var col6 = "U.[kV]";
                    var col7 = "Potencias";
                    var col8 = "$/h";
                    var col9 = "Salida";
                    var col10 = "Entrada";
                    var col11 = "Hs.Indisp.";
                    var col12 = "Minutos Indis.";
                    var col13 = "Tipo Salida";
                    //var col13 = "Hs Prog";
                    var col14 = "CR%";
                    var col15 = "E.N.S.";
                    var col16 = "Informo en termino";
                    var col17 = "Total Penalización";
                    //var col18 = "Cargo Real";
                    var col19 = "No Percibido";
                    var col20 = "Lucro Cesante";
                    var col21 = "Motivo";
                    var col22 = "";
                    var col23 = "Tarifa";
                    var col24 = "Resolucion";

                    hojaDetalle.encabezado = new List<string>();

                    hojaDetalle.encabezado.Add(col1);
                    hojaDetalle.encabezado.Add(col2);
                    hojaDetalle.encabezado.Add(col3);
                    hojaDetalle.encabezado.Add(col4);
                    hojaDetalle.encabezado.Add(col5);
                    hojaDetalle.encabezado.Add(col6);
                    hojaDetalle.encabezado.Add(col7);
                    hojaDetalle.encabezado.Add(col8);
                    hojaDetalle.encabezado.Add(col9);
                    hojaDetalle.encabezado.Add(col10);
                    hojaDetalle.encabezado.Add(col11);
                    hojaDetalle.encabezado.Add(col12);
                    hojaDetalle.encabezado.Add(col13);
                    hojaDetalle.encabezado.Add(col14);
                    hojaDetalle.encabezado.Add(col15);
                    hojaDetalle.encabezado.Add(col16);
                    hojaDetalle.encabezado.Add(col17);
                    //hojaDetalle.encabezado.Add(col18);
                    hojaDetalle.encabezado.Add(col19);
                    hojaDetalle.encabezado.Add(col20);
                    hojaDetalle.encabezado.Add(col21);
                    hojaDetalle.encabezado.Add(col22);
                    hojaDetalle.encabezado.Add(col23);
                    hojaDetalle.encabezado.Add(col24);

                    hojaDetalle.listaFilas = new List<DetalleExcelVM>();


                    foreach (var item in trafos)
                    {

                        DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                        Calendar cal = dfi.Calendar;
                        DateTime fechaActual = Convert.ToDateTime(item.fechaSalida);
                        string mesActual = fechaActual.ToString("MMMM");
                        hojaDetalle.unaFila = new DetalleExcelVM();
                        hojaDetalle.unaFila.datoPorFila = new List<string>();
                        hojaDetalle.unaFila.datoPorFila.Add(mesActual.ToUpperInvariant());
                        hojaDetalle.unaFila.datoPorFila.Add("Transformador");
                        hojaDetalle.unaFila.datoPorFila.Add(item.idPagotran.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.nombreTransformador);
                        hojaDetalle.unaFila.datoPorFila.Add(item.potenciaAparente.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.tensiones.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.potencias.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.montoXhora.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.fechaSalida.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.fechaEntrada.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.hsIndisponible.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.minutosIndisponible.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.tipoSalida);
                        //hojaDetalle.unaFila.datoPorFila.Add(item.hsProgramada != null ? item.hsProgramada.ToString() : "-");
                        //hojaDetalle.unaFila.datoPorFila.Add(item.hsForzada != null ? item.hsForzada.ToString() : "-");
                        hojaDetalle.unaFila.datoPorFila.Add(item.CR.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.ENS == true ? "SI" : "NO");
                        hojaDetalle.unaFila.datoPorFila.Add(item.informoEnTermino == true ? "SI" : "NO");
                        hojaDetalle.unaFila.datoPorFila.Add(item.totalPenalizacion.ToString());
                        //hojaDetalle.unaFila.datoPorFila.Add(item.cargoReal.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.noPercibido.ToString());
                        var lucroCesanteX = item.noPercibido + item.totalPenalizacion;
                        hojaDetalle.unaFila.datoPorFila.Add(lucroCesanteX.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.motivo);
                        hojaDetalle.unaFila.datoPorFila.Add(" ");
                        hojaDetalle.unaFila.datoPorFila.Add(item.montoIndis.monto.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.montoIndis.resolucion);
                        hojaDetalle.listaFilas.Add(hojaDetalle.unaFila);

                        totalPenalizacion += item.totalPenalizacion;
                        totalNoPercibo += item.noPercibido;
                        totalLucroCesante += lucroCesanteX;
                    }

                    //De aca hago los totales 
                    hojaDetalle.totales.datoPorFilaTotales = new List<string>();
                    hojaDetalle.totales.datoPorFilaTotales.Add(totalPenalizacion.ToString());
                    hojaDetalle.totales.datoPorFilaTotales.Add(totalNoPercibo.ToString());
                    hojaDetalle.totales.datoPorFilaTotales.Add(totalLucroCesante.ToString());
                    hojaDetalle.listaFilas.Add(hojaDetalle.totales);


                    var montosTrafos = db.IndisponibilidadMontoTrafo.Where(x => x.activo == true).ToList();
                    foreach (var item in montosTrafos)
                    {
                        hojaDetalle.unaFilaMonto = new DetalleExcelVM();
                        hojaDetalle.unaFilaMonto.datoPorFilaMonto = new List<string>();
                        hojaDetalle.unaFilaMonto.datoPorFilaMonto.Add(item.nombreMonto);
                        hojaDetalle.unaFilaMonto.datoPorFilaMonto.Add("$ " + item.monto.ToString());
                        hojaDetalle.unaFilaMonto.datoPorFilaMonto.Add(item.resolucion != "" ? item.resolucion : " - ");
                        hojaDetalle.listaFilas.Add(hojaDetalle.unaFilaMonto);
                    }

                    var datoExcel = excel.GenerarExcelGeneralRemunerado(hojaDetalle, "Transformador");

                    return datoExcel;

                }
            }
            catch (Exception e)
            {
                var datoExcel = new HojaExcelVM();
                return datoExcel;
            }

        }


        #endregion

        #region PTO Conexion
        // Ptos Remuenrados
        public object obtenerPTOConexion(int idResolucion, string fecha)
        {
            List<IndisRemuEquipoVM> listaPuntoConexion = new List<IndisRemuEquipoVM>();
            if (fecha != null)
            {
                DateTime fechaParse = DateTime.Parse(fecha);
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    if (idResolucion == 0)
                    {
                        listaPuntoConexion = (from c in db.PCPuntoConexion
                                              join d in db.EstEstacion on c.idEstacion equals d.idEstacion
                                              join e in db.GralTension on c.idTension equals e.idTension
                                              join r in db.IndisponibilidadRemuPtosConexion on c.idPuntoConexion equals r.idPuntoConexion into remu
                                              from r in remu.DefaultIfEmpty()
                                              where c.activo == true && c.remunerado == true && r.Anio == fechaParse.Year && r.Mes == fechaParse.Month
                                              select new IndisRemuEquipoVM
                                              {
                                                  idPtoRemunerado = r.idIndisponibilidadesPtosC,
                                                  idPuntoConexion = c.idPuntoConexion,
                                                  nombrePuntoConexion = c.nombrePuntoConexion,
                                                  activo = c.activo,
                                                  idPagotran = c.idPagotran,
                                                  cargoRealxHora = r.cargoRealxHora,
                                                  xMes = r.xMes,
                                                  idIndisponibilidadesPtosC = r.idIndisponibilidadesPtosC,
                                                  muestroRealxHora = "$ " + r.cargoRealxHora,
                                                  muestroxMes = "$ " + r.xMes,
                                                  tension = new TensionVM
                                                  {
                                                      idTension = e.idTension,
                                                      tension = e.tension,
                                                      activo = e.activo
                                                  },
                                                  estacion = new EstacionVM
                                                  {
                                                      idEstacion = d.idEstacion,
                                                      codigoEstacion = d.codigoEstacion,
                                                      nombreEstacion = d.nombreEstacion,
                                                      activo = d.activo,
                                                  },
                                              }).ToList();
                    }
                    else
                    {
                        listaPuntoConexion = (from c in db.PCPuntoConexion
                                              join d in db.EstEstacion on c.idEstacion equals d.idEstacion
                                              join e in db.GralTension on c.idTension equals e.idTension
                                              join r in db.IndisponibilidadRemuPtosConexion on c.idPuntoConexion equals r.idPuntoConexion into remu
                                              from r in remu.DefaultIfEmpty()
                                              where c.activo == true && c.remunerado == true && r.idResolucion == idResolucion
                                      && r.Anio == fechaParse.Year && r.Mes == fechaParse.Month
                                              select new IndisRemuEquipoVM
                                              {
                                                  idPtoRemunerado = r.idIndisponibilidadesPtosC,
                                                  idPuntoConexion = c.idPuntoConexion,
                                                  nombrePuntoConexion = c.nombrePuntoConexion,
                                                  activo = c.activo,
                                                  idPagotran = c.idPagotran,
                                                  cargoRealxHora = r.cargoRealxHora,
                                                  xMes = r.xMes,
                                                  idIndisponibilidadesPtosC = r.idIndisponibilidadesPtosC,
                                                  muestroRealxHora = "$ " + r.cargoRealxHora,
                                                  muestroxMes = "$ " + r.xMes,
                                                  tension = new TensionVM
                                                  {
                                                      idTension = e.idTension,
                                                      tension = e.tension,
                                                      activo = e.activo
                                                  },
                                                  estacion = new EstacionVM
                                                  {
                                                      idEstacion = d.idEstacion,
                                                      codigoEstacion = d.codigoEstacion,
                                                      nombreEstacion = d.nombreEstacion,
                                                      activo = d.activo,
                                                  },
                                              }).ToList();
                    }


                    foreach (var item in listaPuntoConexion)
                    {
                        var fechaHoraActual = DateTime.Now;
                        item.indisponible = true;
                        var consultaCapacitorIndisponible = db.IndisponibilidadPtosConexion.Where(x => x.idPtoIndisRemu == item.idIndisponibilidadesPtosC && x.activo == true && (x.fechaEntrada >= fechaHoraActual || x.fechaEntrada == null)).FirstOrDefault();
                        if (consultaCapacitorIndisponible != null)
                        {

                            item.indisponible = false;
                            db.SaveChanges();

                        }
                        db.SaveChanges();
                    }
                }
            }


            var listaOrdenada = listaPuntoConexion.OrderByDescending(x => x.idTrafo);
            object json = new { data = listaOrdenada };
            return json;
        }

        // Montos 
        public object obtenerMontosPtosConexion()
        {
            List<MontoIndisponibilidadesVM> listaMontoPtsC;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                listaMontoPtsC = (from m in db.IndisponibilidadMontoPtosConexion
                                  where m.activo == true
                                  select new MontoIndisponibilidadesVM
                                  {
                                      idIndisponibilidadMontosPtsC = m.idIndisponibilidadMontosPtosC,
                                      idIndisponibilidadMontos = m.idIndisponibilidadMontosPtosC,
                                      nombreMonto = "Salida en " + m.kv + " kV",
                                      kvPtosC = m.kv,
                                      monto = m.monto,
                                      mostrarMonto = "$ " + m.monto,
                                      fecha = m.fechaMonto,
                                      ku = m.valorAdicional,
                                      resolucion = m.resolucion,
                                  }).ToList();

                foreach (var item in listaMontoPtsC)
                {
                    if (item.nombreMonto != "Salida en 13.2 kV")
                    {
                        item.nombreMonto = item.nombreMonto.ToString().Split('.')[0];
                        item.nombreMonto += " kV";
                    }
                }
            }
            object json = new { data = listaMontoPtsC };
            return json;
        }
        public object obtenerMontosPtosConexionTodos()
        {
            List<MontoIndisponibilidadesVM> listaMontoPtsC;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                listaMontoPtsC = (from m in db.IndisponibilidadMontoPtosConexion
                                  where m.activoHistorial == true
                                  select new MontoIndisponibilidadesVM
                                  {
                                      idIndisponibilidadMontosPtsC = m.idIndisponibilidadMontosPtosC,
                                      idIndisponibilidadMontos = m.idIndisponibilidadMontosPtosC,
                                      nombreMonto = "Salida en " + m.kv + " kV",
                                      kvPtosC = m.kv,
                                      monto = m.monto,
                                      mostrarMonto = "$ " + m.monto,
                                      fecha = m.fechaMonto,
                                      ku = m.valorAdicional,
                                      resolucion = m.resolucion,
                                      kv = (int?)m.kv
                                  }).ToList();

                foreach (var item in listaMontoPtsC)
                {
                    if (item.nombreMonto != "Salida en 13.2 kV")
                    {
                        item.nombreMonto = item.nombreMonto.ToString().Split('.')[0];
                        item.nombreMonto += " kV";
                    }
                }
            }
            object json = new { data = listaMontoPtsC };
            return json;
        }


        public int AltaMontoPtosC(MontoIndisponibilidadesVM monto)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var kvString = db.GralTension.Where(x => x.idTension == monto.kv).FirstOrDefault();

                //var KvDecimal = Convert.ToDecimal(kvString.tension);
                var KvDecimal = Convert.ToDecimal(kvString.tension, CultureInfo.InvariantCulture);

                var consultaUnMonto = db.IndisponibilidadMontoPtosConexion.Where(x => x.kv == KvDecimal && x.activo == true).FirstOrDefault();
                if (consultaUnMonto == null /*&& montoRepetido == null*/)
                {
                    try
                    {
                        IndisponibilidadMontoPtosConexion mon = new IndisponibilidadMontoPtosConexion
                        {
                            idTipoEquipo = 2,
                            monto = monto.monto,
                            activo = true,
                            kv = KvDecimal,
                            fechaMonto = monto.fechaDesde,
                            resolucion = monto.resolucion,
                            valorAdicional = monto.ku,
                            activoHistorial = true
                        };
                        db.IndisponibilidadMontoPtosConexion.Add(mon);
                        db.SaveChanges();

                        var idResolucion = mon.idIndisponibilidadMontosPtosC;
                        //var fechaHoy = new DateTime(); var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;
                        //int cantidadDias = DateTime.DaysInMonth(fechaHoy.Year, fechaHoy.Month);
                        var fechaHastaRes = monto.fechaHasta == null ? DateTime.Today : monto.fechaHasta;
                        while (monto.fechaDesde <= fechaHastaRes)
                        {
                            var PtosC = db.PCPuntoConexion.Where(x => x.idTension == monto.kv && x.activo == true && x.remunerado == true).ToList();

                            foreach (var item in PtosC) //MODIFICO EL CARGO  QUE TIENEN ASIGNADO ESE KV
                            {
                                var fechaHoy = monto.fechaDesde;
                                var PtosCRemunerada = db.IndisponibilidadRemuPtosConexion.Where(x => x.idPuntoConexion == item.idPuntoConexion && x.activo == true && x.idResolucion == idResolucion && x.Mes == monto.fechaDesde.Value.Month && x.Anio == monto.fechaDesde.Value.Year).FirstOrDefault();
                                int cantidadDias = DateTime.DaysInMonth(fechaHoy.Value.Year, fechaHoy.Value.Month);
                                //var cargoHoraskv = 0;
                                if (PtosCRemunerada != null)
                                {
                                    if (monto.kv == 200)
                                    {
                                        PtosCRemunerada.cargoRealxHora = monto.monto * Convert.ToDecimal("1.25");

                                        PtosCRemunerada.xMes = PtosCRemunerada.cargoRealxHora * (cantidadDias * 24);
                                    }
                                    else
                                    {
                                        PtosCRemunerada.cargoRealxHora = monto.monto;
                                        PtosCRemunerada.xMes = PtosCRemunerada.cargoRealxHora * (cantidadDias * 24);
                                    }


                                }
                                else
                                {
                                    var cargoHorakv = monto.monto;
                                    if (monto.kv == 200)
                                    {
                                        cargoHorakv = monto.monto * Convert.ToDecimal("1.25");

                                    }

                                    var nuevaPtoRemunerada = new IndisponibilidadRemuPtosConexion
                                    {
                                        activo = true,
                                        Mes = fechaHoy.Value.Month,
                                        Anio = fechaHoy.Value.Year,
                                        idResolucion = mon.idIndisponibilidadMontosPtosC,
                                        idPuntoConexion = item.idPuntoConexion,
                                        cargoRealxHora = cargoHorakv,
                                        xMes = cargoHorakv * cantidadDias * 24
                                    };
                                    db.IndisponibilidadRemuPtosConexion.Add(nuevaPtoRemunerada);
                                }
                                db.SaveChanges();
                            }
                            var ptoIndisponible = db.IndisponibilidadPtosConexion.Where(x => x.fechaEntrada.Value.Month == monto.fechaDesde.Value.Month && x.fechaEntrada.Value.Year == monto.fechaDesde.Value.Year).ToList();
                            foreach (var ptoC in ptoIndisponible)
                            {
                                ModificoPtoCIndisponibles(ptoC);
                                // db.SaveChanges();
                            }
                            calculoMesesPtosC(monto.fechaDesde);
                            monto.fechaDesde = monto.fechaDesde.Value.AddMonths(1);
                        }
                        return 200;

                    }
                    catch (DbEntityValidationException e)
                    {
                        foreach (var eve in e.EntityValidationErrors)
                        {
                            Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                            foreach (var ve in eve.ValidationErrors)
                            {
                                Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                             ve.PropertyName, ve.ErrorMessage);
                            }
                        }
                        return 400;

                    }
                }
                else
                {
                    return 300; //ya existe ese dato
                }
            }
        }

        public int ModificarMontoPtosC(MontoIndisponibilidadesVM monto)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarUnMonto = db.IndisponibilidadMontoPtosConexion.Where(x => x.idIndisponibilidadMontosPtosC == monto.idIndisponibilidadMontos).FirstOrDefault();
                var kv = db.GralTension.Where(x => x.idTension == monto.kv).FirstOrDefault();
                var idResolucion = monto.idIndisponibilidadMontos;
                decimal kvDecimal = 0;
                if (kv.tension == "13.2")
                {
                    kvDecimal = Convert.ToDecimal(13.2);
                }
                else if (kv != null)
                {
                    kvDecimal = Convert.ToDecimal(kv.tension);
                }

                var FechaActual = DateTime.Today;
                var fechaDesde = consultarUnMonto.fechaMonto;
                var montoHistorial = consultarUnMonto.monto;

                if (consultarUnMonto != null)
                {
                    consultarUnMonto.monto = monto.monto;
                    consultarUnMonto.resolucion = monto.nombreMonto;
                    consultarUnMonto.fechaMonto = monto.fechaDesde;
                    consultarUnMonto.resolucion = monto.resolucion;
                    //consultarUnMonto.activo = false;
                    //consultarUnMonto.activoHistorial = true;
                    consultarUnMonto.fechaHasta = monto.fechaHasta;
                    db.SaveChanges();


                    var fechaHastaRes = monto.fechaHasta == null ? DateTime.Today : monto.fechaHasta;
                    while (monto.fechaDesde <= fechaHastaRes)
                    {

                        var PtosC = db.PCPuntoConexion.Where(x => x.idTension == monto.kv /*&& x.activo == true*/ && x.remunerado == true && x.fechaCreacion <= fechaHastaRes).ToList();
                        //var fechaHoy = new DateTime(); var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;
                        //int cantidadDias = DateTime.DaysInMonth(fechaHoy.Year, fechaHoy.Month);

                        foreach (var item in PtosC) //MODIFICO EL CARGO DE LAS  QUE TIENEN ASIGNADO ESE KV
                        {
                            var PtosCRemunerada = db.IndisponibilidadRemuPtosConexion.Where(x => x.idPuntoConexion == item.idPuntoConexion && x.activo == true && x.idResolucion == idResolucion && x.Mes == monto.fechaDesde.Value.Month && x.Anio == monto.fechaDesde.Value.Year).FirstOrDefault();
                            var fechaHoy = monto.fechaDesde; //var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;
                            int cantidadDias = DateTime.DaysInMonth(fechaHoy.Value.Year, fechaHoy.Value.Month);
                            if (PtosCRemunerada != null)
                            {

                                if (monto.kv == 200)
                                {
                                    PtosCRemunerada.cargoRealxHora = monto.monto * Convert.ToDecimal("1.25");
                                    PtosCRemunerada.activo = item.activo;
                                    PtosCRemunerada.xMes = PtosCRemunerada.cargoRealxHora * (cantidadDias * 24);
                                }
                                else
                                {
                                    PtosCRemunerada.cargoRealxHora = monto.monto;
                                    PtosCRemunerada.xMes = PtosCRemunerada.cargoRealxHora * (cantidadDias * 24);
                                    PtosCRemunerada.activo = item.activo;
                                }
                                db.SaveChanges();
                            }
                            else
                            {
                                var cargoHorakv = monto.monto;
                                if (monto.kv == 200)
                                {
                                    cargoHorakv = monto.monto * Convert.ToDecimal("1.25");

                                }

                                var nuevaPtoRemunerada = new IndisponibilidadRemuPtosConexion
                                {
                                    activo = item.activo,
                                    Mes = fechaHoy.Value.Month,
                                    Anio = fechaHoy.Value.Year,
                                    idResolucion = idResolucion,
                                    idPuntoConexion = item.idPuntoConexion,
                                    cargoRealxHora = cargoHorakv,
                                    xMes = cargoHorakv * cantidadDias * 24
                                };
                                db.IndisponibilidadRemuPtosConexion.Add(nuevaPtoRemunerada);
                                db.SaveChanges();
                            }
                            


                        }


                        //IndisponibilidadMontoPtosConexion nuevoMonto = new IndisponibilidadMontoPtosConexion()
                        //{
                        //    monto = monto.monto,
                        //    fechaMonto = monto.fechaDesde,
                        //    resolucion = monto.resolucion,
                        //    kv = kvDecimal,
                        //    idTipoEquipo = 2,
                        //    activo = true,
                        //    valorAdicional = monto.ku,
                        //};
                        //db.IndisponibilidadMontoPtosConexion.Add(nuevoMonto);
                        //db.SaveChanges();


                        var ptoIndisponible = db.IndisponibilidadPtosConexion.Where(x => x.fechaEntrada.Value.Month == monto.fechaDesde.Value.Month && x.fechaEntrada.Value.Year == monto.fechaDesde.Value.Year).ToList();
                        foreach (var ptoC in ptoIndisponible)
                        {
                            ModificoPtoCIndisponibles(ptoC);
                            // db.SaveChanges();
                        }
                        calculoMesesPtosC(monto.fechaDesde);
                        monto.fechaDesde = monto.fechaDesde.Value.AddMonths(1);

                    }
                }
                else
                {
                    AltaMontoPtosC(monto);
                }
            }
            return 200;
        }

        public int EliminarMontoPtosC(MontoIndisponibilidadesVM monto)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnMonto = db.IndisponibilidadMontoPtosConexion.Where(x => x.idIndisponibilidadMontosPtosC == monto.idIndisponibilidadMontos).FirstOrDefault();
                if (consultaUnMonto != null)
                {
                    consultaUnMonto.activo = false;
                    consultaUnMonto.activoHistorial = true;
                    consultaUnMonto.fechaHasta = DateTime.Today;
                    db.SaveChanges();
                }
            }
            return 200;
        }

        public MontoIndisponibilidadesVM ConsultarMontoPtosC(int idPtosC, DateTime fechaDesdeMonto, DateTime? fechaHastaMonto, int idMonto)
        {
            MontoIndisponibilidadesVM montoEncontrado = new MontoIndisponibilidadesVM();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var ptoCR = db.PCPuntoConexion.Where(x => x.idPuntoConexion == idPtosC && x.activo == true).FirstOrDefault();
                var kvString = db.GralTension.Where(x => x.idTension == ptoCR.idTension).FirstOrDefault();
                var KvDecimal = Convert.ToDecimal(kvString.tension);
                if (kvString.tension == "13.2")
                {
                    KvDecimal = 13.2M;
                }
                var consultaUnMonto = new IndisponibilidadMontoPtosConexion();
                if (idMonto != 0)
                {
                    consultaUnMonto = db.IndisponibilidadMontoPtosConexion.Where(x => x.idIndisponibilidadMontosPtosC == idMonto).FirstOrDefault();

                }
                else
                {
                    consultaUnMonto = db.IndisponibilidadMontoPtosConexion.Where(x => x.kv == KvDecimal && ((x.fechaMonto <= fechaDesdeMonto && x.fechaHasta >= fechaHastaMonto) || (x.fechaMonto <= fechaDesdeMonto && x.activo == true))).FirstOrDefault();

                }

                if (consultaUnMonto != null)
                {
                    montoEncontrado = (from m in db.IndisponibilidadMontoPtosConexion
                                       where m.idIndisponibilidadMontosPtosC == consultaUnMonto.idIndisponibilidadMontosPtosC
                                       select new MontoIndisponibilidadesVM
                                       {
                                           idIndisponibilidadMontos = m.idIndisponibilidadMontosPtosC,
                                           monto = m.monto,
                                           mostrarMonto = "$ " + m.monto,
                                           kvPtosC = m.kv,
                                           resolucion = "Resolucion: " + m.resolucion,
                                       }).FirstOrDefault();
                }

            }
            return montoEncontrado;
        }


        // Historial
        public object ObtenerTensionPtosC()
        {
            List<TensionVM> traerListaTension;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                traerListaTension = (from c in db.GralTension
                                     where c.activo == true && c.idTension != 6
                                     select new TensionVM
                                     {
                                         idTension = c.idTension,
                                         tension = c.tension,
                                         activo = c.activo
                                     }).ToList();
            }
            object json = new { data = traerListaTension };

            return json;
        }

        public object ObtenerHistorialPtosC()
        {
            List<MontoIndisponibilidadesVM> historialPtos;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                historialPtos = (from h in db.IndisponibilidadMontoPtosConexion
                                 where h.activoHistorial == true && h.fechaHasta != null
                                 select new MontoIndisponibilidadesVM
                                 {
                                     idIndisponibilidadHistorialMontPtosC = h.idIndisponibilidadMontosPtosC,
                                     idMontoPtosC = h.idIndisponibilidadMontosPtosC,
                                     fechaDesde = h.fechaMonto,
                                     fechaHasta = h.fechaHasta,
                                     monto = h.monto,
                                     mostrarMonto = "$ " + h.monto,
                                     kvPtosC = h.kv,
                                     resolucion = h.resolucion,
                                 }).ToList();
            }
            var json = new { data = historialPtos };
            return json;
        }

        public int ModificarHistorialMontoPtosC(MontoIndisponibilidadesVM monto)
        {
            //using (IntranetCTREntities db = new IntranetCTREntities())
            //{
            //    var consultarUnMonto = db.IndisponibilidadMontoPtosConexion.Where(x => x.idIndisponibilidadMontosPtosC == monto.idIndisponibilidadMontos).FirstOrDefault();
            //    if (consultarUnMonto != null)
            //    {
            //        consultarUnMonto.monto = monto.monto;
            //        consultarUnMonto.resolucion = monto.resolucion;
            //        db.SaveChanges();

            //        var consultaPtoCIndisponible = db.IndisponibilidadPtosConexion.Where(x => x.idMonto == monto.idIndisponibilidadMontos).ToList();
            //        foreach (var PtoC in consultaPtoCIndisponible)
            //        {
            //            ModificoPtoCIndisponibles(PtoC);
            //            db.SaveChanges();
            //        }

            //    }
            //}
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarUnMonto = db.IndisponibilidadMontoPtosConexion.Where(x => x.idIndisponibilidadMontosPtosC == monto.idIndisponibilidadMontos && x.activoHistorial == true).FirstOrDefault();
                if (consultarUnMonto != null)
                {
                    consultarUnMonto.monto = monto.monto;
                    consultarUnMonto.resolucion = monto.resolucion;
                    if (monto.fechaDesdeF != "" && monto.fechaDesdeF != "Sin fecha")
                    {
                        DateTime fecha = DateTime.Parse(monto.fechaDesdeF);

                        consultarUnMonto.fechaMonto = fecha;
                    }
                    if (monto.fechaHastaF != "" && monto.fechaHastaF != "Sin fecha")
                    {
                        consultarUnMonto.fechaHasta = monto.fechaHasta;
                        if (consultarUnMonto.fechaHasta < DateTime.Today)
                        {
                            consultarUnMonto.activo = false;
                        }
                    }
                    db.SaveChanges();
                }

                ModificarMontoPtosC(monto);

            }

            return 200;
        }
        public int ModificarHistorialMontoPtosC2(MontoIndisponibilidadesVM monto)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var idResolucion = monto.idIndisponibilidadMontos;
                var consultarUnMonto = db.IndisponibilidadMontoPtosConexion.Where(x => x.idIndisponibilidadMontosPtosC == monto.idIndisponibilidadMontos).FirstOrDefault();
                var kv = db.GralTension.Where(x => x.idTension == monto.kv).FirstOrDefault();
                decimal kvDecimal = 0;
                if (kv.tension == "13.2")
                {
                    kvDecimal = Convert.ToDecimal(13.2);
                }
                else if (kv != null)
                {
                    kvDecimal = Convert.ToDecimal(kv.tension);
                }

                var FechaActual = DateTime.Today;
                var fechaDesde = consultarUnMonto.fechaMonto;
                var montoHistorial = consultarUnMonto.monto;

                if (consultarUnMonto != null)
                {
                    consultarUnMonto.monto = monto.monto;
                    consultarUnMonto.kv = kvDecimal;
                    consultarUnMonto.fechaMonto = monto.fechaDesde;
                    consultarUnMonto.resolucion = monto.resolucion;
                    //consultarUnMonto.activo = false;
                    consultarUnMonto.fechaHasta = monto.fechaHasta;
                    //consultarUnMonto.activoHistorial = true;
                    db.SaveChanges();

                    var fechaHastaRes = monto.fechaHasta == null ? DateTime.Today : monto.fechaHasta;

                    while (monto.fechaDesde <= fechaHastaRes)
                    {
                        var PtosC = db.PCPuntoConexion.Where(x => x.idTension == monto.kv && x.activo == true && x.remunerado == true).ToList();
                        //var fechaHoy = new DateTime(); var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;
                        //int cantidadDias = DateTime.DaysInMonth(fechaHoy.Year, fechaHoy.Month);

                        foreach (var item in PtosC) //MODIFICO EL CARGO DE LAS  QUE TIENEN ASIGNADO ESE KV
                        {
                            var PtosCRemunerada = db.IndisponibilidadRemuPtosConexion.Where(x => x.idPuntoConexion == item.idPuntoConexion && x.activo == true && x.idResolucion == idResolucion && x.Mes == monto.fechaDesde.Value.Month && x.Anio == monto.fechaDesde.Value.Year).FirstOrDefault();

                            if (PtosCRemunerada != null)
                            {
                                var fechaHoy = monto.fechaDesde; //var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;
                                int cantidadDias = DateTime.DaysInMonth(fechaHoy.Value.Year, fechaHoy.Value.Month);

                                if (monto.kv == 200)
                                {
                                    PtosCRemunerada.cargoRealxHora = monto.monto * Convert.ToDecimal("1.25");

                                    PtosCRemunerada.xMes = PtosCRemunerada.cargoRealxHora * (cantidadDias * 24);
                                }
                                else
                                {
                                    PtosCRemunerada.cargoRealxHora = monto.monto;
                                    PtosCRemunerada.xMes = PtosCRemunerada.cargoRealxHora * (cantidadDias * 24);
                                }

                            }
                            else
                            {
                                var datosPto = db.IndisponibilidadMontoPtosConexion.Where(x => x.idIndisponibilidadMontosPtosC == item.idPuntoConexion).FirstOrDefault();
                                if (datosPto != null)
                                {
                                    //var cargoRealxHoras = (monto.monto * item.kilometraje * datosLinea.remunerado / 100) / 100;
                                    var fechaHoy = monto.fechaDesde; //var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;
                                    int cantidadDias = DateTime.DaysInMonth(fechaHoy.Value.Year, fechaHoy.Value.Month);
                                    //var xMes = cargoRealxHoras * (cantidadDias * 24);
                                    IndisponibilidadMontoPtosConexion nuevoMonto = new IndisponibilidadMontoPtosConexion()
                                    {
                                        monto = monto.monto,
                                        fechaMonto = monto.fechaDesde,
                                        resolucion = datosPto.resolucion,
                                        kv = datosPto.kv,
                                        idTipoEquipo = 2,
                                        activo = true,
                                        valorAdicional = monto.ku,
                                    };
                                    db.IndisponibilidadMontoPtosConexion.Add(nuevoMonto);
                                    db.SaveChanges();


                                }
                                else
                                {
                                    IndisponibilidadMontoPtosConexion nuevoMonto = new IndisponibilidadMontoPtosConexion()
                                    {
                                        monto = monto.monto,
                                        fechaMonto = monto.fechaDesde,
                                        resolucion = monto.resolucion,
                                        kv = kvDecimal,
                                        idTipoEquipo = 2,
                                        activo = true,
                                        valorAdicional = monto.ku,
                                    };
                                    db.IndisponibilidadMontoPtosConexion.Add(nuevoMonto);
                                    db.SaveChanges();
                                }
                            }

                        }


                        var ptoIndisponible = db.IndisponibilidadPtosConexion.Where(x => x.fechaEntrada >= DateTime.Now && x.idMonto == monto.idIndisponibilidadMontos).ToList();
                        foreach (var ptoC in ptoIndisponible)
                        {
                            ModificoPtoCIndisponibles(ptoC, idResolucion);
                        }
                        monto.fechaDesde = monto.fechaDesde.Value.AddMonths(1);
                    }
                }
            }
            return 200;
        }


        // Indisponibles

        public object obtenerPtosCIndisponibles(DateTime fechaDesde, DateTime fechaHasta)
        {
            List<IndisRemuEquipoVM> listraPtosCIndisponibles;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                listraPtosCIndisponibles = (from p in db.PCPuntoConexion
                                            join e in db.EstEstacion on p.idEstacion equals e.idEstacion
                                            join g in db.GralTension on p.idTension equals g.idTension
                                            join r in db.IndisponibilidadRemuPtosConexion on p.idPuntoConexion equals r.idPuntoConexion into remu
                                            from r in remu.DefaultIfEmpty()
                                            join ip in db.IndisponibilidadPtosConexion on r.idIndisponibilidadesPtosC equals ip.idPtoIndisRemu into ptoC
                                            from ip in ptoC.DefaultIfEmpty()
                                            join m in db.IndisponibilidadMontoPtosConexion on ip.idMonto equals m.idIndisponibilidadMontosPtosC
                                            where p.activo == true && p.remunerado == true && ip.activo == true && r.activo == true && (ip.fechaSalida >= fechaDesde && ip.fechaEntrada <= fechaHasta)
                                            select new IndisRemuEquipoVM
                                            {
                                                idIndisponibilidadesPtosC = ip.IdPtoRemunerado,
                                                idPuntoConexion = p.idPuntoConexion,
                                                idPtoIndisRemu = r.idIndisponibilidadesPtosC,
                                                idPagotran = p.idPagotran,
                                                nombrePuntoConexion = p.nombrePuntoConexion,
                                                Ukv = g.tension,
                                                cargoRealxHora = ip.montoXhora,
                                                muestroRealxHora = ip.montoXhora != null ? "$ " + ip.montoXhora : " ",
                                                fechaSalida = ip.fechaSalida,
                                                fechaEntrada = ip.fechaEntrada,
                                                hsIndisponible = ip.hsIndisponible,
                                                minutosIndisponible = ip.minutosIndisponible,
                                                tipoSalida = ip.tipoSalida,
                                                hsForzada = ip.hsForzada,
                                                hsProgramada = ip.hsProgramada,
                                                activo = r.activo,
                                                penalizacionProgramada = ip.penalizacionProgramada,
                                                MuestroinformoEnTermino = ip.informoEnTermino == true ? "Si" : "No",
                                                totalPenalizacion = ip.totalPenalizacion,
                                                cargoReal = ip.cargoReal,
                                                informoEnTermino = ip.informoEnTermino,
                                                noPercibido = ip.noPercibido,
                                                lucroCesante = ip.lucroCesante,
                                                motivo = ip.motivo,
                                                idMonto = m.idIndisponibilidadMontosPtosC,
                                                estacion = new EstacionVM
                                                {
                                                    idEstacion = e.idEstacion,
                                                    nombreEstacion = e.nombreEstacion,
                                                    codigoEstacion = e.codigoEstacion,
                                                    activo = e.activo,
                                                }

                                            }).ToList();

                foreach (var item in listraPtosCIndisponibles)
                {
                    var novIndisPtosC = db.NovNovedadIndisponibilidad.Where(x => x.idTipoEquipo == 2 && x.activo == true && x.idIndisponibilidadRemu == item.idIndisponibilidadesPtosC).FirstOrDefault();
                    if (novIndisPtosC != null)
                    {
                        item.idTipoIndisponibilidad = novIndisPtosC.idTipoIndisponibilidad;
                    }
                    else
                    {
                        item.idTipoIndisponibilidad = -1;
                    }
                }

            }
            listraPtosCIndisponibles = listraPtosCIndisponibles.OrderByDescending(x => x.idIndisponibilidadesPtosC).ToList();
            object json = new { data = listraPtosCIndisponibles };
            return json;
        }

        public int AltaPtoCIndisponibles(IndisRemuEquipoVM ptoC, DateTime fechaDesdeMonto, DateTime? fechaHastaMonto)
        {

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnPtoC = db.IndisponibilidadPtosConexion.Where(x => x.idPtoConexion == ptoC.idPuntoConexion && x.fechaSalida == ptoC.fechaSalida && x.fechaEntrada == ptoC.fechaEntrada && x.activo == true).FirstOrDefault();
                if (consultaUnPtoC == null)
                {
                    try
                    {
                        var puntoRemunerado = db.IndisponibilidadRemuPtosConexion.Where(x => x.idPuntoConexion == ptoC.idPuntoConexion).FirstOrDefault();
                        var PuntoConexion = db.PCPuntoConexion.Where(x => x.idPuntoConexion == ptoC.idPuntoConexion && x.activo == true).FirstOrDefault();
                        var kv = db.GralTension.Where(x => x.activo == true && x.idTension == PuntoConexion.idTension).FirstOrDefault();
                        var UkvInt = Convert.ToDecimal(kv.tension);
                        var monto = db.IndisponibilidadMontoPtosConexion.Where(x => x.kv == UkvInt && ((x.fechaMonto <= fechaDesdeMonto && x.fechaHasta >= fechaHastaMonto) || (x.fechaMonto <= fechaDesdeMonto && x.fechaHasta == null))).FirstOrDefault();

                        if (monto == null)
                        {
                            return 303;
                        }

                        decimal? montoXhoraRemunerado = 0;
                        if (monto != null)
                        {
                            montoXhoraRemunerado = monto.monto;
                        }

                        DateTime fechaMal = Convert.ToDateTime("1/1/0001 00:00:00");
                        if (ptoC?.fechaEntrada != fechaMal)
                        {
                            var horasIndisponible = (ptoC.fechaEntrada - ptoC.fechaSalida)?.TotalHours;
                            var minDisponibles = Math.Round((decimal)(ptoC.fechaEntrada - ptoC.fechaSalida)?.TotalMinutes);

                            decimal? hsProgramada = 0;
                            decimal? hsForzada = 0;
                            decimal? hsForzadaAut = 0;
                            decimal? hsSinTension = 0;
                            decimal? penalizacionPrograma = 0;
                            decimal? totalPenalizacion = 0;
                            decimal? penalizacionForzada = 0;
                            decimal? penalizacionForzada2 = 0;
                            decimal? forzadaAutorizada = 0;
                            var Ku = monto.valorAdicional;


                            if (ptoC.tipoSalida == "F")
                            {
                                hsForzada = Convert.ToDecimal(horasIndisponible) * 24;
                                penalizacionForzada = montoXhoraRemunerado * Ku;
                                penalizacionForzada2 = montoXhoraRemunerado * Ku * Decimal.Round(minDisponibles / 60, 2);
                            }
                            else if (ptoC.tipoSalida == "FA")
                            {
                                hsForzadaAut = Convert.ToDecimal(horasIndisponible) * 24;
                                forzadaAutorizada = montoXhoraRemunerado * Ku * Decimal.Round(minDisponibles / 60, 2);
                            }
                            else if (ptoC.tipoSalida == "P")
                            {
                                hsProgramada = Convert.ToDecimal(horasIndisponible) * 24;

                                penalizacionPrograma = montoXhoraRemunerado * Ku * Decimal.Round(minDisponibles / 60, 2) * (Convert.ToDecimal(0.1));
                            }
                            else if (ptoC.tipoSalida == "ST")
                            {
                                hsSinTension = Convert.ToDecimal(horasIndisponible) * 24;
                            }

                            totalPenalizacion = penalizacionPrograma + penalizacionForzada + penalizacionForzada2 + forzadaAutorizada;
                            if (ptoC.informoEnTermino == true)
                            {
                                totalPenalizacion *= 1;
                            }
                            else
                            {
                                totalPenalizacion *= 2;
                            }

                            if (UkvInt == 220)
                            {
                                totalPenalizacion *= 0;
                            }
                            else
                            {
                                totalPenalizacion *= 1;
                            }

                            decimal? cargoReal;
                            if (UkvInt == 220)
                            {
                                cargoReal = monto.monto * Convert.ToDecimal(1.25);
                            }
                            else
                            {
                                cargoReal = monto.monto;
                            }


                            decimal? noPercibido = cargoReal * Convert.ToDecimal(horasIndisponible);

                            decimal? lucroCesante = noPercibido + totalPenalizacion;

                            IndisponibilidadPtosConexion PuntoC = new IndisponibilidadPtosConexion
                            {
                                idPtoIndisRemu = puntoRemunerado.idIndisponibilidadesPtosC,
                                idPtoConexion = PuntoConexion.idPuntoConexion,
                                idMonto = monto.idIndisponibilidadMontosPtosC,
                                montoXhora = montoXhoraRemunerado,
                                fechaSalida = ptoC.fechaSalida,
                                fechaEntrada = ptoC.fechaEntrada,
                                hsIndisponible = Convert.ToDecimal(horasIndisponible),
                                minutosIndisponible = Convert.ToDecimal(minDisponibles),
                                tipoSalida = ptoC.tipoSalida,
                                activo = true,
                                hsProgramada = hsProgramada,
                                hsForzada = hsForzada,
                                penalizacionProgramada = penalizacionPrograma,
                                informoEnTermino = ptoC.informoEnTermino,
                                totalPenalizacion = totalPenalizacion,
                                cargoReal = cargoReal,
                                noPercibido = noPercibido,
                                lucroCesante = lucroCesante,
                                motivo = ptoC.motivo,
                                idIndisponibilidad = ptoC.idNovedadIndisponibilidad,
                            };
                            db.IndisponibilidadPtosConexion.Add(PuntoC);
                            db.SaveChanges();

                            //VIENE DE LO DE INDISPONIBILIDADES DEL UCIEL
                            if (ptoC.noCargar != true)//El no cargar es para que no cree otrs indisponibilidad por que viene de lo del Uciel
                            {
                                ServicioIndisponibilidad servicioIndisUciel = new ServicioIndisponibilidad();
                                NovNovedadIndisponibilidad indis = new NovNovedadIndisponibilidad { };
                                indis.activo = true;
                                indis.FechaEntrada = ptoC.fechaEntrada;
                                indis.FechaSalida = ptoC.fechaSalida;
                                indis.PorcentajeIndisponible = 100;
                                indis.idEquipo = ptoC.idPuntoConexion;
                                indis.idTipoEquipo = 2;
                                indis.idTipoIndisponibilidad = 1;
                                indis.descripcion = ptoC.motivo;
                                indis.fueraDeServicio = true;
                                indis.idIndisponibilidadRemu = PuntoC.IdPtoRemunerado;
                                if (ptoC.tipoSalida == "F")
                                {
                                    indis.itn = 1;
                                }
                                else if (ptoC.tipoSalida == "FA")
                                {
                                    indis.itn = 2;
                                }
                                else if (ptoC.tipoSalida == "P")
                                {
                                    indis.itn = 3;
                                }
                                else if (ptoC.tipoSalida == "ST")
                                {
                                    indis.itn = 4;
                                }
                                db.NovNovedadIndisponibilidad.Add(indis);
                                db.SaveChanges();
                                var ultimaIndi = db.NovNovedadIndisponibilidad.Where(x => x.idIndisponibilidadRemu == PuntoC.IdPtoRemunerado && x.activo == true && x.idTipoEquipo == 2).FirstOrDefault();
                                PuntoC.idIndisponibilidad = ultimaIndi.idNovedadIndisponibilidad;
                            }

                            db.SaveChanges();
                            calculoMesesPtosC(ptoC.fechaSalida);
                            return 200;
                        }
                        else
                        {
                            IndisponibilidadPtosConexion PuntoC = new IndisponibilidadPtosConexion
                            {
                                idPtoIndisRemu = ptoC.idPtoIndisRemu,
                                idPtoConexion = PuntoConexion.idPuntoConexion,
                                idMonto = monto.idIndisponibilidadMontosPtosC,
                                montoXhora = montoXhoraRemunerado,
                                fechaSalida = ptoC.fechaSalidaA,
                                tipoSalida = ptoC.tipoSalida,
                                activo = true,
                                informoEnTermino = ptoC.informoEnTermino,
                                motivo = ptoC.motivo,
                                idIndisponibilidad = ptoC.idNovedadIndisponibilidad,

                            };
                            db.IndisponibilidadPtosConexion.Add(PuntoC);
                            db.SaveChanges();
                            calculoMesesPtosC(ptoC.fechaSalida);
                            return 200;
                        }

                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }

            }
        }

        public int ModificarPtoCIndisponibles(IndisRemuEquipoVM ptoC, DateTime fechaDesdeMonto, DateTime? fechaHastaMonto)
        {

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnPtoC = db.IndisponibilidadPtosConexion.Where(x => x.IdPtoRemunerado == ptoC.idPtoRemunerado).FirstOrDefault();
                if (consultaUnPtoC != null)
                {
                    try
                    {
                        var puntoRemunerado = db.IndisponibilidadRemuPtosConexion.Where(x => x.idPuntoConexion == ptoC.idPuntoConexion).FirstOrDefault();
                        var PuntoConexion = db.PCPuntoConexion.Where(x => x.idPuntoConexion == ptoC.idPuntoConexion && x.activo == true).FirstOrDefault();
                        var kv = db.GralTension.Where(x => x.activo == true && x.idTension == PuntoConexion.idTension).FirstOrDefault();
                        var UkvInt = Convert.ToDecimal(kv.tension);
                        var monto = db.IndisponibilidadMontoPtosConexion.Where(x => x.kv == UkvInt && ((x.fechaMonto <= fechaDesdeMonto && x.fechaHasta >= fechaHastaMonto) || (x.fechaMonto <= fechaDesdeMonto && x.fechaHasta == null))).FirstOrDefault();

                        if (monto == null)
                        {
                            return 303;
                        }

                        decimal? montoXhoraRemunerado = 0;
                        if (monto != null)
                        {
                            montoXhoraRemunerado = monto.monto;
                        }

                        DateTime fechaMal = Convert.ToDateTime("1/1/0001 00:00:00");
                        if (ptoC?.fechaEntrada != fechaMal)
                        {
                            var horasIndisponible = (ptoC.fechaEntrada - ptoC.fechaSalida)?.TotalHours;
                            var minDisponibles = Math.Round((decimal)(ptoC.fechaEntrada - ptoC.fechaSalida)?.TotalMinutes);

                            decimal? hsProgramada = 0;
                            decimal? hsForzada = 0;
                            decimal? hsForzadaAut = 0;
                            decimal? hsSinTension = 0;
                            decimal? penalizacionPrograma = 0;
                            decimal? totalPenalizacion = 0;
                            decimal? penalizacionForzada = 0;
                            decimal? penalizacionForzada2 = 0;
                            decimal? forzadaAutorizada = 0;
                            var Ku = monto.valorAdicional;


                            if (ptoC.tipoSalida == "F")
                            {
                                hsForzada = Convert.ToDecimal(horasIndisponible) * 24;
                                penalizacionForzada = montoXhoraRemunerado * Ku;
                                penalizacionForzada2 = montoXhoraRemunerado * Ku * Decimal.Round(minDisponibles / 60, 2);
                            }
                            else if (ptoC.tipoSalida == "FA")
                            {
                                hsForzadaAut = Convert.ToDecimal(horasIndisponible) * 24;
                                forzadaAutorizada = montoXhoraRemunerado * Ku * Decimal.Round(minDisponibles / 60, 2);
                            }
                            else if (ptoC.tipoSalida == "P")
                            {
                                hsProgramada = Convert.ToDecimal(horasIndisponible) * 24;
                                penalizacionPrograma = montoXhoraRemunerado * Ku * Decimal.Round(minDisponibles / 60, 2) * Convert.ToDecimal(0.1);
                            }
                            else if (ptoC.tipoSalida == "ST")
                            {
                                hsSinTension = Convert.ToDecimal(horasIndisponible) * 24;
                            }

                            totalPenalizacion = penalizacionPrograma + penalizacionForzada + penalizacionForzada2 + forzadaAutorizada;
                            if (ptoC.informoEnTermino == true)
                            {
                                totalPenalizacion *= 1;
                            }
                            else
                            {
                                totalPenalizacion *= 2;
                            }

                            if (UkvInt == 220)
                            {
                                totalPenalizacion *= 0;
                            }
                            else
                            {
                                totalPenalizacion *= 1;
                            }

                            decimal? cargoReal;
                            if (UkvInt == 220)
                            {
                                cargoReal = monto.monto * Convert.ToDecimal(1.25);
                            }
                            else
                            {
                                cargoReal = monto.monto;
                            }


                            decimal? noPercibido = cargoReal * Convert.ToDecimal(horasIndisponible);

                            decimal? lucroCesante = noPercibido + totalPenalizacion;


                            consultaUnPtoC.idPtoIndisRemu = puntoRemunerado.idIndisponibilidadesPtosC;
                            consultaUnPtoC.idPtoConexion = PuntoConexion.idPuntoConexion;
                            consultaUnPtoC.idMonto = monto.idIndisponibilidadMontosPtosC;
                            consultaUnPtoC.montoXhora = montoXhoraRemunerado;
                            consultaUnPtoC.fechaSalida = ptoC.fechaSalida;
                            consultaUnPtoC.fechaEntrada = ptoC.fechaEntrada;
                            consultaUnPtoC.hsIndisponible = Convert.ToDecimal(horasIndisponible);
                            consultaUnPtoC.minutosIndisponible = Convert.ToDecimal(minDisponibles);
                            consultaUnPtoC.tipoSalida = ptoC.tipoSalida;
                            consultaUnPtoC.activo = true;
                            consultaUnPtoC.hsProgramada = hsProgramada;
                            consultaUnPtoC.hsForzada = hsForzada;
                            consultaUnPtoC.penalizacionProgramada = penalizacionPrograma;
                            consultaUnPtoC.informoEnTermino = ptoC.informoEnTermino;
                            consultaUnPtoC.totalPenalizacion = totalPenalizacion;
                            consultaUnPtoC.cargoReal = cargoReal;
                            consultaUnPtoC.noPercibido = noPercibido;
                            consultaUnPtoC.lucroCesante = lucroCesante;
                            consultaUnPtoC.motivo = ptoC.motivo;
                            consultaUnPtoC.idIndisponibilidad = ptoC.idNovedadIndisponibilidad;

                            db.SaveChanges();

                            //Esto es para indisponibilidades que hizo el uciel
                            ServicioIndisponibilidad serviIndispUciel = new ServicioIndisponibilidad();
                            var indisExistente = db.NovNovedadIndisponibilidad.Where(x => x.idIndisponibilidadRemu == consultaUnPtoC.IdPtoRemunerado && x.activo == true && x.idTipoEquipo == 2).FirstOrDefault();

                            if (indisExistente != null) //SI ES UNA EDICION
                            {
                                indisExistente.FechaEntrada = ptoC.fechaEntrada;
                                indisExistente.FechaSalida = ptoC.fechaSalida;
                                indisExistente.PorcentajeIndisponible = 100;
                                indisExistente.idEquipo = ptoC.idPuntoConexion;
                                indisExistente.idTipoEquipo = 2;
                                indisExistente.descripcion = ptoC.motivo;
                                indisExistente.fueraDeServicio = ptoC.informoEnTermino;


                                if (ptoC.tipoSalida == "F")
                                {
                                    indisExistente.itn = 1;
                                }
                                else if (ptoC.tipoSalida == "FM")
                                {
                                    indisExistente.itn = 2;
                                }
                                else if (ptoC.tipoSalida == "P")
                                {
                                    indisExistente.itn = 3;
                                }
                                else if (ptoC.tipoSalida == "ST")
                                {
                                    indisExistente.itn = 4;
                                }
                                db.SaveChanges();
                            }
                            calculoMesesPtosC(ptoC.fechaSalida);
                            return 200;
                        }
                        else
                        {
                            return 404;
                        }

                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }

            }
        }

        public int ModificoPtoCIndisponibles(IndisponibilidadPtosConexion ptoC) //Para usarlo con otras funciones
        {

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnPtoC = db.IndisponibilidadPtosConexion.Where(x => x.IdPtoRemunerado == ptoC.IdPtoRemunerado).FirstOrDefault();
                if (consultaUnPtoC != null)
                {
                    try
                    {
                        var puntoRemunerado = db.IndisponibilidadRemuPtosConexion.Where(x => x.idPuntoConexion == ptoC.idPtoConexion).FirstOrDefault();
                        var PuntoConexion = db.PCPuntoConexion.Where(x => x.idPuntoConexion == ptoC.idPtoConexion && x.activo == true).FirstOrDefault();
                        var kv = db.GralTension.Where(x => x.activo == true && x.idTension == PuntoConexion.idTension).FirstOrDefault();
                        var UkvInt = Convert.ToDecimal(kv.tension);
                        //  var monto = db.IndisponibilidadMontoPtosConexion.Where(x => x.kv == UkvInt && ((x.fechaMonto <= fechaDesdeMonto && x.fechaHasta >= fechaHastaMonto) || (x.fechaMonto <= fechaDesdeMonto && x.fechaHasta == null))).FirstOrDefault();

                        var monto = new IndisponibilidadMontoPtosConexion();
                        if (ptoC.fechaEntrada >= DateTime.Now)
                        {
                            monto = db.IndisponibilidadMontoPtosConexion.Where(x => x.activo == true && x.kv == UkvInt).FirstOrDefault();
                            consultaUnPtoC.idMonto = monto.idIndisponibilidadMontosPtosC;
                        }
                        else//esto es mas que nada para el historial
                        {
                            monto = db.IndisponibilidadMontoPtosConexion.Where(x => x.idIndisponibilidadMontosPtosC == ptoC.idMonto).FirstOrDefault();
                        }

                        decimal? montoXhoraRemunerado = 0;
                        if (monto != null)
                        {
                            montoXhoraRemunerado = monto.monto;
                        }

                        DateTime fechaMal = Convert.ToDateTime("1/1/0001 00:00:00");
                        if (ptoC?.fechaEntrada != fechaMal)
                        {
                            var horasIndisponible = (ptoC.fechaEntrada - ptoC.fechaSalida)?.TotalHours;
                            var minDisponibles = Math.Round((decimal)(ptoC.fechaEntrada - ptoC.fechaSalida)?.TotalMinutes);

                            decimal? hsProgramada = 0;
                            decimal? hsForzada = 0;
                            decimal? hsForzadaAut = 0;
                            decimal? hsSinTension = 0;
                            decimal? penalizacionPrograma = 0;
                            decimal? totalPenalizacion = 0;
                            decimal? penalizacionForzada = 0;
                            decimal? penalizacionForzada2 = 0;
                            decimal? forzadaAutorizada = 0;
                            var Ku = monto.valorAdicional;


                            if (ptoC.tipoSalida == "F")
                            {
                                hsForzada = Convert.ToDecimal(horasIndisponible) * 24;
                                penalizacionForzada = montoXhoraRemunerado * Ku;
                                penalizacionForzada2 = montoXhoraRemunerado * Ku * Decimal.Round(minDisponibles / 60, 2);
                            }
                            else if (ptoC.tipoSalida == "FA")
                            {
                                hsForzadaAut = Convert.ToDecimal(horasIndisponible) * 24;
                                forzadaAutorizada = montoXhoraRemunerado * Ku * Decimal.Round(minDisponibles / 60, 2);
                            }
                            else if (ptoC.tipoSalida == "P")
                            {
                                hsProgramada = Convert.ToDecimal(horasIndisponible) * 24;
                                penalizacionPrograma = montoXhoraRemunerado * Ku * Decimal.Round(minDisponibles / 60, 2) * Convert.ToDecimal(0.1);
                            }
                            else if (ptoC.tipoSalida == "ST")
                            {
                                hsSinTension = Convert.ToDecimal(horasIndisponible) * 24;
                            }

                            totalPenalizacion = penalizacionPrograma + penalizacionForzada + penalizacionForzada2 + forzadaAutorizada;
                            if (ptoC.informoEnTermino == true)
                            {
                                totalPenalizacion *= 1;
                            }
                            else
                            {
                                totalPenalizacion *= 2;
                            }

                            if (UkvInt == 220)
                            {
                                totalPenalizacion *= 0;
                            }
                            else
                            {
                                totalPenalizacion *= 1;
                            }

                            decimal? cargoReal;
                            if (UkvInt == 220)
                            {
                                cargoReal = monto.monto * Convert.ToDecimal(1.25);
                            }
                            else
                            {
                                cargoReal = monto.monto;
                            }


                            decimal? noPercibido = cargoReal * Convert.ToDecimal(horasIndisponible);

                            decimal? lucroCesante = noPercibido + totalPenalizacion;


                            consultaUnPtoC.idPtoIndisRemu = puntoRemunerado.idIndisponibilidadesPtosC;
                            consultaUnPtoC.idPtoConexion = PuntoConexion.idPuntoConexion;
                            consultaUnPtoC.idMonto = monto.idIndisponibilidadMontosPtosC;
                            consultaUnPtoC.montoXhora = montoXhoraRemunerado;
                            consultaUnPtoC.fechaSalida = ptoC.fechaSalida;
                            consultaUnPtoC.fechaEntrada = ptoC.fechaEntrada;
                            consultaUnPtoC.hsIndisponible = Convert.ToDecimal(horasIndisponible);
                            consultaUnPtoC.minutosIndisponible = Convert.ToDecimal(minDisponibles);
                            consultaUnPtoC.tipoSalida = ptoC.tipoSalida;
                            consultaUnPtoC.hsProgramada = hsProgramada;
                            consultaUnPtoC.hsForzada = hsForzada;
                            consultaUnPtoC.penalizacionProgramada = penalizacionPrograma;
                            consultaUnPtoC.informoEnTermino = ptoC.informoEnTermino;
                            consultaUnPtoC.totalPenalizacion = totalPenalizacion;
                            consultaUnPtoC.cargoReal = cargoReal;
                            consultaUnPtoC.noPercibido = noPercibido;
                            consultaUnPtoC.lucroCesante = lucroCesante;
                            consultaUnPtoC.motivo = ptoC.motivo;

                            db.SaveChanges();

                            //Esto es para indisponibilidades que hizo el uciel
                            ServicioIndisponibilidad serviIndispUciel = new ServicioIndisponibilidad();
                            var indisExistente = db.NovNovedadIndisponibilidad.Where(x => x.idIndisponibilidadRemu == consultaUnPtoC.IdPtoRemunerado && x.activo == true && x.idTipoEquipo == 2).FirstOrDefault();

                            if (indisExistente != null) //SI ES UNA EDICION
                            {
                                indisExistente.FechaEntrada = ptoC.fechaEntrada;
                                indisExistente.FechaSalida = ptoC.fechaSalida;
                                indisExistente.PorcentajeIndisponible = 100;
                                indisExistente.idEquipo = ptoC.idPtoConexion;
                                indisExistente.idTipoEquipo = 2;
                                indisExistente.descripcion = ptoC.motivo;
                                indisExistente.fueraDeServicio = ptoC.informoEnTermino;


                                if (ptoC.tipoSalida == "F")
                                {
                                    indisExistente.itn = 1;
                                }
                                else if (ptoC.tipoSalida == "FM")
                                {
                                    indisExistente.itn = 2;
                                }
                                else if (ptoC.tipoSalida == "P")
                                {
                                    indisExistente.itn = 3;
                                }
                                else if (ptoC.tipoSalida == "ST")
                                {
                                    indisExistente.itn = 4;
                                }
                                db.SaveChanges();
                            }
                            //calculoMesesPtosC(ptoC.fechaSalida);
                            return 200;
                        }
                        else
                        {
                            return 404;
                        }

                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }

            }
        }
        public int ModificoPtoCIndisponibles(IndisponibilidadPtosConexion ptoC, int? idResolucion) //Para usarlo con otras funciones
        {

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnPtoC = db.IndisponibilidadPtosConexion.Where(x => x.IdPtoRemunerado == ptoC.IdPtoRemunerado).FirstOrDefault();
                if (consultaUnPtoC != null)
                {
                    try
                    {
                        var puntoRemunerado = db.IndisponibilidadRemuPtosConexion.Where(x => x.idPuntoConexion == ptoC.idPtoConexion && x.idResolucion == idResolucion
                                       && x.Mes == ptoC.fechaSalida.Value.Month && x.Anio == ptoC.fechaSalida.Value.Year).FirstOrDefault();
                        var PuntoConexion = db.PCPuntoConexion.Where(x => x.idPuntoConexion == ptoC.idPtoConexion && x.activo == true).FirstOrDefault();
                        var kv = db.GralTension.Where(x => x.activo == true && x.idTension == PuntoConexion.idTension).FirstOrDefault();
                        var UkvInt = Convert.ToDecimal(kv.tension);
                        //  var monto = db.IndisponibilidadMontoPtosConexion.Where(x => x.kv == UkvInt && ((x.fechaMonto <= fechaDesdeMonto && x.fechaHasta >= fechaHastaMonto) || (x.fechaMonto <= fechaDesdeMonto && x.fechaHasta == null))).FirstOrDefault();

                        var monto = new IndisponibilidadMontoPtosConexion();
                        if (ptoC.fechaEntrada >= DateTime.Now)
                        {
                            monto = db.IndisponibilidadMontoPtosConexion.Where(x => x.activo == true && x.kv == UkvInt).FirstOrDefault();
                            consultaUnPtoC.idMonto = monto.idIndisponibilidadMontosPtosC;
                        }
                        else//esto es mas que nada para el historial
                        {
                            monto = db.IndisponibilidadMontoPtosConexion.Where(x => x.idIndisponibilidadMontosPtosC == ptoC.idMonto).FirstOrDefault();
                        }

                        decimal? montoXhoraRemunerado = 0;
                        if (monto != null)
                        {
                            montoXhoraRemunerado = monto.monto;
                        }

                        DateTime fechaMal = Convert.ToDateTime("1/1/0001 00:00:00");
                        if (ptoC?.fechaEntrada != fechaMal)
                        {
                            var horasIndisponible = (ptoC.fechaEntrada - ptoC.fechaSalida)?.TotalHours;
                            var minDisponibles = Math.Round((decimal)(ptoC.fechaEntrada - ptoC.fechaSalida)?.TotalMinutes);

                            decimal? hsProgramada = 0;
                            decimal? hsForzada = 0;
                            decimal? hsForzadaAut = 0;
                            decimal? hsSinTension = 0;
                            decimal? penalizacionPrograma = 0;
                            decimal? totalPenalizacion = 0;
                            decimal? penalizacionForzada = 0;
                            decimal? penalizacionForzada2 = 0;
                            decimal? forzadaAutorizada = 0;
                            var Ku = monto.valorAdicional;


                            if (ptoC.tipoSalida == "F")
                            {
                                hsForzada = Convert.ToDecimal(horasIndisponible) * 24;
                                penalizacionForzada = montoXhoraRemunerado * Ku;
                                penalizacionForzada2 = montoXhoraRemunerado * Ku * Decimal.Round(minDisponibles / 60, 2);
                            }
                            else if (ptoC.tipoSalida == "FA")
                            {
                                hsForzadaAut = Convert.ToDecimal(horasIndisponible) * 24;
                                forzadaAutorizada = montoXhoraRemunerado * Ku * Decimal.Round(minDisponibles / 60, 2);
                            }
                            else if (ptoC.tipoSalida == "P")
                            {
                                hsProgramada = Convert.ToDecimal(horasIndisponible) * 24;
                                penalizacionPrograma = montoXhoraRemunerado * Ku * Decimal.Round(minDisponibles / 60, 2) * Convert.ToDecimal(0.1);
                            }
                            else if (ptoC.tipoSalida == "ST")
                            {
                                hsSinTension = Convert.ToDecimal(horasIndisponible) * 24;
                            }

                            totalPenalizacion = penalizacionPrograma + penalizacionForzada + penalizacionForzada2 + forzadaAutorizada;
                            if (ptoC.informoEnTermino == true)
                            {
                                totalPenalizacion *= 1;
                            }
                            else
                            {
                                totalPenalizacion *= 2;
                            }

                            if (UkvInt == 220)
                            {
                                totalPenalizacion *= 0;
                            }
                            else
                            {
                                totalPenalizacion *= 1;
                            }

                            decimal? cargoReal;
                            if (UkvInt == 220)
                            {
                                cargoReal = monto.monto * Convert.ToDecimal(1.25);
                            }
                            else
                            {
                                cargoReal = monto.monto;
                            }


                            decimal? noPercibido = cargoReal * Convert.ToDecimal(horasIndisponible);

                            decimal? lucroCesante = noPercibido + totalPenalizacion;


                            consultaUnPtoC.idPtoIndisRemu = puntoRemunerado.idIndisponibilidadesPtosC;
                            consultaUnPtoC.idPtoConexion = PuntoConexion.idPuntoConexion;
                            consultaUnPtoC.idMonto = monto.idIndisponibilidadMontosPtosC;
                            consultaUnPtoC.montoXhora = montoXhoraRemunerado;
                            consultaUnPtoC.fechaSalida = ptoC.fechaSalida;
                            consultaUnPtoC.fechaEntrada = ptoC.fechaEntrada;
                            consultaUnPtoC.hsIndisponible = Convert.ToDecimal(horasIndisponible);
                            consultaUnPtoC.minutosIndisponible = Convert.ToDecimal(minDisponibles);
                            consultaUnPtoC.tipoSalida = ptoC.tipoSalida;
                            consultaUnPtoC.hsProgramada = hsProgramada;
                            consultaUnPtoC.hsForzada = hsForzada;
                            consultaUnPtoC.penalizacionProgramada = penalizacionPrograma;
                            consultaUnPtoC.informoEnTermino = ptoC.informoEnTermino;
                            consultaUnPtoC.totalPenalizacion = totalPenalizacion;
                            consultaUnPtoC.cargoReal = cargoReal;
                            consultaUnPtoC.noPercibido = noPercibido;
                            consultaUnPtoC.lucroCesante = lucroCesante;
                            consultaUnPtoC.motivo = ptoC.motivo;

                            db.SaveChanges();

                            //Esto es para indisponibilidades que hizo el uciel
                            ServicioIndisponibilidad serviIndispUciel = new ServicioIndisponibilidad();
                            var indisExistente = db.NovNovedadIndisponibilidad.Where(x => x.idIndisponibilidadRemu == consultaUnPtoC.IdPtoRemunerado && x.activo == true && x.idTipoEquipo == 2).FirstOrDefault();

                            if (indisExistente != null) //SI ES UNA EDICION
                            {
                                indisExistente.FechaEntrada = ptoC.fechaEntrada;
                                indisExistente.FechaSalida = ptoC.fechaSalida;
                                indisExistente.PorcentajeIndisponible = 100;
                                indisExistente.idEquipo = ptoC.idPtoConexion;
                                indisExistente.idTipoEquipo = 2;
                                indisExistente.descripcion = ptoC.motivo;
                                indisExistente.fueraDeServicio = ptoC.informoEnTermino;


                                if (ptoC.tipoSalida == "F")
                                {
                                    indisExistente.itn = 1;
                                }
                                else if (ptoC.tipoSalida == "FM")
                                {
                                    indisExistente.itn = 2;
                                }
                                else if (ptoC.tipoSalida == "P")
                                {
                                    indisExistente.itn = 3;
                                }
                                else if (ptoC.tipoSalida == "ST")
                                {
                                    indisExistente.itn = 4;
                                }
                                db.SaveChanges();
                            }
                            calculoMesesPtosC(ptoC.fechaSalida);
                            return 200;
                        }
                        else
                        {
                            return 404;
                        }

                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }

            }
        }

        public int EliminarPtoCIndisponible(IndisRemuEquipoVM ptoC)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaPtoCIndis = db.IndisponibilidadPtosConexion.Where(x => x.IdPtoRemunerado == ptoC.idIndisponibilidadesPtosC && x.activo == true).FirstOrDefault();
                if (consultaPtoCIndis != null)
                {
                    consultaPtoCIndis.activo = false;
                    db.SaveChanges();

                    var linNovIndis = db.NovNovedadIndisponibilidad.Where(x => x.idIndisponibilidadRemu == ptoC.idIndisponibilidadesPtosC && x.activo == true && x.idTipoEquipo == 2).FirstOrDefault();
                    if (linNovIndis != null)
                    {
                        linNovIndis.activo = false;
                        db.SaveChanges();
                    }

                }
                calculoMesesPtosC(consultaPtoCIndis.fechaSalida);
                return 200;
            }
        }

        //Reporte en Excel
        public HojaExcelVM GenerarReporteExcelPtosC(string fechaDesde, string fechaHasta)
        {
            var fechaDesdeDate = Convert.ToDateTime(fechaDesde);
            var fechaHastaDate = Convert.ToDateTime(fechaHasta);
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    ServicioEquipo servEquipo = new ServicioEquipo();
                    List<IndisRemuEquipoVM> ptosC = new List<IndisRemuEquipoVM>();
                    ptosC = (from i in db.IndisponibilidadPtosConexion
                             join r in db.IndisponibilidadRemuPtosConexion on i.idPtoIndisRemu equals r.idIndisponibilidadesPtosC
                             join p in db.PCPuntoConexion on i.idPtoConexion equals p.idPuntoConexion
                             join m in db.IndisponibilidadMontoPtosConexion on i.idMonto equals m.idIndisponibilidadMontosPtosC
                             join t in db.GralTension on p.idTension equals t.idTension
                             where i.activo == true && fechaDesdeDate <= i.fechaSalida && fechaHastaDate >= i.fechaEntrada
                             select new { i, r, m, p, t }).AsEnumerable().Select(x => new IndisRemuEquipoVM
                             {
                                 idIndisponibilidadesPtosC = x.i.IdPtoRemunerado,
                                 idPtoIndisRemu = x.i.idPtoIndisRemu,
                                 idPuntoConexion = x.p.idPuntoConexion,
                                 idMonto = x.m.idIndisponibilidadMontosPtosC,
                                 montoXhora = x.i.montoXhora,
                                 fechaSalida = x.i.fechaSalida,
                                 fechaEntrada = x.i.fechaEntrada,
                                 hsIndisponible = x.i.hsIndisponible,
                                 minutosIndisponible = x.i.minutosIndisponible,
                                 tipoSalida = x.i.tipoSalida,
                                 activo = x.i.activo,
                                 informoEnTermino = x.i.informoEnTermino,
                                 totalPenalizacion = x.i.totalPenalizacion,
                                 noPercibido = x.i.noPercibido,
                                 motivo = x.i.motivo,
                                 penalizacionProgramada = x.i.penalizacionProgramada,
                                 cargoReal = x.i.cargoReal,
                                 lucroCesante = x.i.lucroCesante,

                                 //PtoC
                                 idPagotran = x.p.idPagotran,
                                 nombrePuntoConexion = x.p.nombrePuntoConexion,
                                 Ukv = x.t.tension,
                                 //Monto
                                 montoIndis = new MontoIndisponibilidadesVM
                                 {
                                     monto = x.m.monto,
                                     kvPtosC = x.m.kv,
                                     resolucion = x.m.resolucion,
                                 }
                             }).OrderByDescending(x => x.fechaSalida).ToList();


                    GenerarExcel excel = new GenerarExcel();
                    HojaExcelVM hojaDetalle = new HojaExcelVM();
                    hojaDetalle.totales = new DetalleExcelVM();

                    decimal? totalPenalizacion = 0;
                    decimal? totalNoPercibo = 0;
                    decimal? totalLucroCesante = 0;

                    var col1 = "Mes";
                    var col2 = "Equipo";
                    var col3 = "ID";
                    var col4 = "Punto de Conexion";
                    var col5 = "U.[kV]";
                    var col6 = "$/h";
                    var col7 = "Salida";
                    var col8 = "Entrada";
                    //var col9 = "Tipo Salida";
                    // var col10 = "Entrada";
                    var col11 = "Hs.Indisp.";
                    var col12 = "Minutos Indis.";
                    var col13 = "Tipo Salida";
                    //var col13 = "Hs Prog";
                    // var col14 = "CR%";
                    //var col15 = "E.N.S.";
                    var col16 = "Informo en termino";
                    var col17 = "Total Penalización";
                    var col18 = "Cargo Real";
                    var col19 = "No Percibido";
                    var col20 = "Lucro Cesante";
                    var col21 = "Motivo";
                    var col22 = "";
                    var col23 = "Tarifa";
                    var col24 = "Resolucion";

                    hojaDetalle.encabezado = new List<string>();

                    hojaDetalle.encabezado.Add(col1);
                    hojaDetalle.encabezado.Add(col2);
                    hojaDetalle.encabezado.Add(col3);
                    hojaDetalle.encabezado.Add(col4);
                    hojaDetalle.encabezado.Add(col5);
                    hojaDetalle.encabezado.Add(col6);
                    hojaDetalle.encabezado.Add(col7);
                    hojaDetalle.encabezado.Add(col8);
                    // hojaDetalle.encabezado.Add(col9);
                    // hojaDetalle.encabezado.Add(col10);
                    hojaDetalle.encabezado.Add(col11);
                    hojaDetalle.encabezado.Add(col12);
                    hojaDetalle.encabezado.Add(col13);
                    // hojaDetalle.encabezado.Add(col14);
                    //hojaDetalle.encabezado.Add(col15);
                    hojaDetalle.encabezado.Add(col16);
                    hojaDetalle.encabezado.Add(col17);
                    hojaDetalle.encabezado.Add(col18);
                    hojaDetalle.encabezado.Add(col19);
                    hojaDetalle.encabezado.Add(col20);
                    hojaDetalle.encabezado.Add(col21);
                    hojaDetalle.encabezado.Add(col22);
                    hojaDetalle.encabezado.Add(col23);
                    hojaDetalle.encabezado.Add(col24);

                    hojaDetalle.listaFilas = new List<DetalleExcelVM>();


                    foreach (var item in ptosC)
                    {

                        DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                        Calendar cal = dfi.Calendar;
                        DateTime fechaActual = Convert.ToDateTime(item.fechaSalida);
                        string mesActual = fechaActual.ToString("MMMM");
                        hojaDetalle.unaFila = new DetalleExcelVM();
                        hojaDetalle.unaFila.datoPorFila = new List<string>();
                        hojaDetalle.unaFila.datoPorFila.Add(mesActual.ToUpperInvariant());
                        hojaDetalle.unaFila.datoPorFila.Add("Punto de Conexion");
                        hojaDetalle.unaFila.datoPorFila.Add(item.idPagotran.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.nombrePuntoConexion);
                        hojaDetalle.unaFila.datoPorFila.Add(item.Ukv.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.montoXhora.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.fechaSalida.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.fechaEntrada.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.hsIndisponible.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.minutosIndisponible.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.tipoSalida);
                        hojaDetalle.unaFila.datoPorFila.Add(item.informoEnTermino == true ? "SI" : "NO");
                        hojaDetalle.unaFila.datoPorFila.Add(item.totalPenalizacion.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.cargoReal.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.noPercibido.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.lucroCesante.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.motivo);
                        hojaDetalle.unaFila.datoPorFila.Add(" ");
                        hojaDetalle.unaFila.datoPorFila.Add(item.montoIndis.monto.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.montoIndis.resolucion);
                        hojaDetalle.listaFilas.Add(hojaDetalle.unaFila);

                        totalPenalizacion += item.totalPenalizacion;
                        totalNoPercibo += item.noPercibido;
                        totalLucroCesante += item.lucroCesante;
                    }

                    //De aca hago los totales 
                    hojaDetalle.totales.datoPorFilaTotales = new List<string>();
                    hojaDetalle.totales.datoPorFilaTotales.Add(totalPenalizacion.ToString());
                    hojaDetalle.totales.datoPorFilaTotales.Add(totalNoPercibo.ToString());
                    hojaDetalle.totales.datoPorFilaTotales.Add(totalLucroCesante.ToString());
                    hojaDetalle.listaFilas.Add(hojaDetalle.totales);

                    var montosPtosC = db.IndisponibilidadMontoPtosConexion.Where(x => x.activo == true).ToList();
                    foreach (var item in montosPtosC)
                    {
                        hojaDetalle.unaFilaMonto = new DetalleExcelVM();
                        hojaDetalle.unaFilaMonto.datoPorFilaMonto = new List<string>();
                        hojaDetalle.unaFilaMonto.datoPorFilaMonto.Add(item.kv.ToString());
                        hojaDetalle.unaFilaMonto.datoPorFilaMonto.Add("$ " + item.monto.ToString());
                        hojaDetalle.unaFilaMonto.datoPorFilaMonto.Add(item.resolucion != "" ? item.resolucion : " - ");
                        hojaDetalle.listaFilas.Add(hojaDetalle.unaFilaMonto);
                    }

                    var datoExcel = excel.GenerarExcelGeneralRemunerado(hojaDetalle, "Campos");

                    return datoExcel;

                }
            }
            catch (Exception e)
            {
                var datoExcel = new HojaExcelVM();
                return datoExcel;
            }

        }

        #endregion

        #region Capacitores
        public object obtenerCapacitores(int idResolucion, string fecha)
        {
            List<IndisRemuEquipoVM> listaCap = new List<IndisRemuEquipoVM>();
            if (fecha != null)
            {
                DateTime fechaParse = DateTime.Parse(fecha);
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    if (idResolucion == 0)
                    {
                        listaCap = (from c in db.CapCapacitor
                                    join r in db.IndisponibilidadRemuCapacitor on c.idCapacitor equals r.idCapacitor
                                    where c.activo == true && c.remunerado == true && r.Anio == fechaParse.Year && r.Mes == fechaParse.Month
                                    select new IndisRemuEquipoVM
                                    {
                                        idIndisponibilidadCap = r.idIndisponibilidadesCapacitor,
                                        idCapacitor = c.idCapacitor,
                                        idPagotran = c.idPagotran,
                                        nombreCapacitor = c.nombreCapacitor,
                                        TensionKv = c.TensionKV,
                                        potenciaMvra = c.potenciaMVAR,
                                        cargoRealxHora = r.cargoRealxHoras,
                                        xMes = r.xMes,
                                        muestroRealxHora = "$ " + r.cargoRealxHoras,
                                        muestroxMes = "$ " + r.xMes,
                                    }).ToList();
                    }
                    else
                    {
                        listaCap = (from c in db.CapCapacitor
                                    join r in db.IndisponibilidadRemuCapacitor on c.idCapacitor equals r.idCapacitor
                                    // && r.Anio == fechaParse.Year && r.Mes == fechaParse.Month && r.idResolucion == idResolucion
                                    where c.activo == true && c.remunerado == true && r.Anio == fechaParse.Year && r.Mes == fechaParse.Month && r.idResolucion == idResolucion
                                    select new IndisRemuEquipoVM
                                    {
                                        idIndisponibilidadCap = r.idIndisponibilidadesCapacitor,
                                        idCapacitor = c.idCapacitor,
                                        idPagotran = c.idPagotran,
                                        nombreCapacitor = c.nombreCapacitor,
                                        TensionKv = c.TensionKV,
                                        potenciaMvra = c.potenciaMVAR,
                                        cargoRealxHora = r.cargoRealxHoras,
                                        xMes = r.xMes,
                                        muestroRealxHora = "$ " + r.cargoRealxHoras,
                                        muestroxMes = "$ " + r.xMes,
                                    }).ToList();
                    }


                    foreach (var item in listaCap)
                    {
                        var fechaHoraActual = DateTime.Now;
                        item.indisponible = true;
                        var consultaCapacitorIndisponible = db.IndisponibilidadCapacitor.Where(x => x.idCapacitorRemunerado == item.idIndisponibilidadCap && x.activo == true && (x.fechaEntrada >= fechaHoraActual || x.fechaEntrada == null)).FirstOrDefault();
                        if (consultaCapacitorIndisponible != null)
                        {
                            item.indisponible = false;
                            db.SaveChanges();
                        }
                        db.SaveChanges();
                    }
                }
            }

            listaCap.OrderByDescending(x => x.idIndisponibilidadCap).ToList();
            var json = new { data = listaCap };
            return json;
        }

        // Montos //
        public object obtenerMontosCapacitores()
        {
            List<MontoIndisponibilidadesVM> montoC;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                montoC = (from m in db.IndisponibilidadMontoCapacitor
                          where m.activo == true
                          select new MontoIndisponibilidadesVM
                          {
                              idIndisponibilidadMontos = m.idIndisponibilidadMontosCapacitor,
                              tipoEquipo = m.idTipoEquipo,
                              nombreMonto = m.nombreMonto,
                              monto = m.monto,
                              mostrarMonto = "$ " + m.monto,
                              activo = m.activo,
                              fecha = m.fechaMonto,
                              resolucion = m.resolucion,
                          }).ToList();
            }
            object json = new { data = montoC };
            return json;
        }
        public object obtenerMontosCapacitoresTodos()
        {
            List<MontoIndisponibilidadesVM> montoC;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                montoC = (from m in db.IndisponibilidadMontoCapacitor
                          where m.activoHistorial == true
                          select new MontoIndisponibilidadesVM
                          {
                              idIndisponibilidadMontos = m.idIndisponibilidadMontosCapacitor,
                              tipoEquipo = m.idTipoEquipo,
                              nombreMonto = m.nombreMonto,
                              monto = m.monto,
                              mostrarMonto = "$ " + m.monto,
                              activo = m.activo,
                              fecha = m.fechaMonto,
                              resolucion = m.resolucion,
                          }).ToList();
            }
            object json = new { data = montoC };
            return json;
        }

        public int AltaMontoCapacitor(MontoIndisponibilidadesVM monto)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var montoExistente = db.IndisponibilidadMontoCapacitor.Where(x => x.activo == true).FirstOrDefault();
                var idResolucion = 0;
                var consultaUnMonto = db.IndisponibilidadMontoTrafo.Where(x => x.idIndisponibilidadMontosTrafo == monto.idIndisponibilidadMontos).FirstOrDefault();
                if (consultaUnMonto != null /*&& montoRepetido == null*/)
                {
                    consultaUnMonto.activo = false;
                }

                try
                {
                    IndisponibilidadMontoCapacitor mon = new IndisponibilidadMontoCapacitor
                    {
                        idTipoEquipo = 7,
                        nombreMonto = monto.nombreMonto,
                        monto = monto.monto,
                        fechaMonto = monto.fechaDesde,
                        resolucion = monto.resolucion,
                        activoHistorial = true,
                        activo = true
                    };
                    db.IndisponibilidadMontoCapacitor.Add(mon);
                    db.SaveChanges();
                    idResolucion = mon.idIndisponibilidadMontosCapacitor;

                    var fechaHastaRes = monto.fechaHasta?.Date ?? DateTime.Today.Date;
                    while (monto.fechaDesde.Value.Date <= fechaHastaRes)
                    {
                        var CapacitorR = db.CapCapacitor.Where(x => x.remunerado == true && x.activo == true).ToList();
                        foreach (var item in CapacitorR)
                        {
                            var fechaHoy = monto.fechaDesde;
                            var capacitorRemunerado = db.IndisponibilidadRemuCapacitor.Where(x => x.idCapacitor == item.idCapacitor && x.Mes == monto.fechaDesde.Value.Month && x.Anio == monto.fechaDesde.Value.Year).FirstOrDefault();
                            int cantidadDias = DateTime.DaysInMonth(fechaHoy.Value.Year, fechaHoy.Value.Month);
                            if (capacitorRemunerado != null)
                            {
                                var nuevoCapacitorRemunerado = new IndisponibilidadRemuCapacitor
                                {
                                    activo = true,
                                    cargoRealxHoras = monto.monto,
                                    idCapacitor = item.idCapacitor,
                                    idResolucion = mon.idIndisponibilidadMontosCapacitor,
                                    OrigenRegulatorio = capacitorRemunerado.OrigenRegulatorio,
                                    xMes = monto.monto * cantidadDias * 24,
                                    Mes = fechaHoy.Value.Month,
                                    Anio = fechaHoy.Value.Year,
                                };
                                db.IndisponibilidadRemuCapacitor.Add(nuevoCapacitorRemunerado);
                                db.SaveChanges();
                            }
                            else
                            {
                                var datosCapacitor = db.IndisponibilidadRemuCapacitor.Where(x => x.idCapacitor == item.idCapacitor).FirstOrDefault();

                                if (datosCapacitor != null)
                                {
                                    var nuevoCapacitorRemunerado = new IndisponibilidadRemuCapacitor
                                    {
                                        activo = true,
                                        cargoRealxHoras = monto.monto,
                                        idCapacitor = item.idCapacitor,
                                        idResolucion = mon.idIndisponibilidadMontosCapacitor,
                                        OrigenRegulatorio = datosCapacitor.OrigenRegulatorio,
                                        xMes = monto.monto * cantidadDias * 24,
                                        Mes = fechaHoy.Value.Month,
                                        Anio = fechaHoy.Value.Year

                                    };
                                    db.IndisponibilidadRemuCapacitor.Add(nuevoCapacitorRemunerado);
                                    db.SaveChanges();
                                }
                            }
                        }


                        var capacitorIndisponible = db.IndisponibilidadCapacitor.Where(x => x.fechaEntrada.Value.Month == monto.fechaDesde.Value.Month && x.fechaEntrada.Value.Year == monto.fechaDesde.Value.Year /*&& x.idMonto == monto.idIndisponibilidadMontos*/).ToList();

                        foreach (var item in capacitorIndisponible) //MODIFICO EL CARGO DE LAS Capacitor QUE TIENEN ASIGNADO ESE KV
                        {
                            ModificoCapacitorIndisponibles(item, idResolucion);

                        }

                        monto.fechaDesde = monto.fechaDesde.Value.AddMonths(1);

                    }

                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                         ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    return 400;

                }
                return 200;

            }
        }

        public int ModificarMontoCapacitor(MontoIndisponibilidadesVM monto)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarUnMonto = db.IndisponibilidadMontoCapacitor.Where(x => x.idIndisponibilidadMontosCapacitor == monto.idIndisponibilidadMontos).FirstOrDefault();
                //var FechaActual = DateTime.Today;
                var fechaDesde = consultarUnMonto.fechaMonto;
                var idResolucion = monto.idIndisponibilidadMontos;
                if (consultarUnMonto != null)
                {
                    var montoHistorial = consultarUnMonto.monto;

                    //Cambio los datos del monto consultado y lo pongo en HistorialActivo 1 para mostrarlo en la parte de historiales

                    consultarUnMonto.monto = monto.monto;

                    consultarUnMonto.fechaMonto = monto.fechaDesde;
                    consultarUnMonto.resolucion = monto.resolucion;
                    //consultarUnMonto.activo = false;
                    consultarUnMonto.fechaHasta = monto.fechaHasta;
                    //consultarUnMonto.activoHistorial = true;
                    db.SaveChanges();

                    var fechaHastaRes = monto.fechaHasta == null ? DateTime.Today : monto.fechaHasta;

                    while (monto.fechaDesde <= fechaHastaRes)
                    {
                        var capacitores = db.CapCapacitor.Where(x => x.remunerado == true && x.fechaCreacion <= fechaHastaRes).ToList();
                        foreach (var item in capacitores)
                        {
                            var capacitorRemunerado = db.IndisponibilidadRemuCapacitor.Where(x => x.idCapacitor == item.idCapacitor && x.activo == true && x.idResolucion == idResolucion && x.Mes == monto.fechaDesde.Value.Month && x.Anio == monto.fechaDesde.Value.Year).FirstOrDefault();
                            if (capacitorRemunerado != null)
                            {
                                capacitorRemunerado.cargoRealxHoras = monto.monto * item.potenciaMVAR;
                                var fechaHoy = monto.fechaDesde; //var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;
                                int cantidadDias = DateTime.DaysInMonth(fechaHoy.Value.Year, fechaHoy.Value.Month);
                                capacitorRemunerado.xMes = monto.monto * item.potenciaMVAR * (cantidadDias * 24);
                                db.SaveChanges();
                            }
                            else
                            {
                                var datosCapacitor = db.IndisponibilidadRemuCapacitor.Where(x => x.idCapacitor == item.idCapacitor).FirstOrDefault();
                                if (datosCapacitor != null)
                                {
                                    var cargoRealxHoras = monto.monto * item.potenciaMVAR;
                                    var fechaHoy = monto.fechaDesde; //var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;
                                    int cantidadDias = DateTime.DaysInMonth(fechaHoy.Value.Year, fechaHoy.Value.Month);
                                    var xMes = cargoRealxHoras * (cantidadDias * 24);

                                    IndisponibilidadRemuCapacitor nuevoRemu = new IndisponibilidadRemuCapacitor
                                    {
                                        activo = true,
                                        cargoRealxHoras = cargoRealxHoras,
                                        idResolucion = idResolucion,
                                        idCapacitor = item.idCapacitor,
                                        OrigenRegulatorio = datosCapacitor.OrigenRegulatorio,
                                        xMes = xMes,
                                        Mes = fechaHoy.Value.Month,
                                        Anio = fechaHoy.Value.Year

                                    };
                                    db.IndisponibilidadRemuCapacitor.Add(nuevoRemu);
                                    db.SaveChanges();

                                }
                            }
                        }
                        var capacitorIndisponible = db.IndisponibilidadCapacitor.Where(x => x.fechaEntrada.Value.Month == monto.fechaDesde.Value.Month && x.fechaEntrada.Value.Year == monto.fechaDesde.Value.Year).ToList();
                        foreach (var capacitor in capacitorIndisponible)
                        {
                            ModificoCapacitorIndisponibles(capacitor, idResolucion);

                        }
                        calculoMesesCapacitores(monto.fechaDesde);
                        monto.fechaDesde = monto.fechaDesde.Value.AddMonths(1);
                    }

                }
                else
                {
                    AltaMontoCapacitor(monto);
                }
            }

            return 200;
        }

        public int EliminarMontoCapacitor(MontoIndisponibilidadesVM monto)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnMonto = db.IndisponibilidadMontoCapacitor.Where(x => x.idIndisponibilidadMontosCapacitor == monto.idIndisponibilidadMontos).FirstOrDefault();
                if (consultaUnMonto != null)
                {
                    consultaUnMonto.activo = false;
                    consultaUnMonto.fechaHasta = DateTime.Today;
                    consultaUnMonto.activoHistorial = true;
                }
                db.SaveChanges();
            }
            return 200;
        }

        public object ObtenerHistorialMontosCapacitores()
        {
            List<MontoIndisponibilidadesVM> historialCapacitor;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                historialCapacitor = (from h in db.IndisponibilidadMontoCapacitor
                                      where h.activo == false && h.activoHistorial == true && h.fechaHasta != null
                                      select new MontoIndisponibilidadesVM
                                      {
                                          idIndisponibilidadHistorialMontCap = h.idIndisponibilidadMontosCapacitor,
                                          nombreMonto = h.nombreMonto,
                                          mostrarMonto = "$ " + h.monto,
                                          monto = h.monto,
                                          fechaDesde = h.fechaMonto,
                                          fechaHasta = h.fechaHasta,
                                          activo = h.activo,
                                          resolucion = h.resolucion,
                                      }).ToList();
            }
            object json = new { data = historialCapacitor };
            return json;
        }

        public int ModificarHistorialMontoCapacitor(MontoIndisponibilidadesVM monto)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarUnMonto = db.IndisponibilidadMontoCapacitor.Where(x => x.idIndisponibilidadMontosCapacitor == monto.idIndisponibilidadMontos).FirstOrDefault();
                if (consultarUnMonto != null)
                {
                    consultarUnMonto.monto = monto.monto;
                    consultarUnMonto.resolucion = monto.resolucion;

                    if (monto.fechaDesdeF != "" && monto.fechaDesdeF != "Sin fecha")
                    {
                        DateTime fecha = DateTime.Parse(monto.fechaDesdeF);

                        consultarUnMonto.fechaMonto = fecha;
                    }
                    if (monto.fechaHastaF != "" && monto.fechaHastaF != "Sin fecha")
                    {
                        consultarUnMonto.fechaHasta = monto.fechaHasta;
                        if (consultarUnMonto.fechaHasta < DateTime.Today)
                        {
                            consultarUnMonto.activo = false;
                        }
                    }
                    db.SaveChanges();
                    ModificarMontoCapacitor(monto);
                }

            }
            return 200;
        }

        public MontoIndisponibilidadesVM ConsultarMontoCapacitor(DateTime fechaDesdeMonto, DateTime? fechaHastaMonto)
        {
            MontoIndisponibilidadesVM montoEncontrado = new MontoIndisponibilidadesVM();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnMonto = db.IndisponibilidadMontoCapacitor.Where(x => (x.fechaMonto <= fechaDesdeMonto && x.fechaHasta >= fechaHastaMonto) || (x.fechaMonto <= fechaDesdeMonto && x.activo == true)).FirstOrDefault();

                if (consultaUnMonto != null)
                {
                    montoEncontrado = (from m in db.IndisponibilidadMontoCapacitor
                                       where m.idIndisponibilidadMontosCapacitor == consultaUnMonto.idIndisponibilidadMontosCapacitor
                                       select new MontoIndisponibilidadesVM
                                       {
                                           idIndisponibilidadMontos = m.idIndisponibilidadMontosCapacitor,
                                           monto = m.monto,
                                           mostrarMonto = "$ " + m.monto,
                                           nombreMonto = m.nombreMonto,
                                           resolucion = "Resolucion: " + m.resolucion,
                                       }).FirstOrDefault();
                }

            }
            return montoEncontrado;
        }


        // Indisponibilidades

        public object obtenerCapacitoresIndisponibles(DateTime fechaDesde, DateTime fechaHasta)
        {
            List<IndisRemuEquipoVM> listaCapacitorRemunerada;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                listaCapacitorRemunerada = (from c in db.CapCapacitor
                                            join r in db.IndisponibilidadRemuCapacitor on c.idCapacitor equals r.idCapacitor into remu
                                            from r in remu.DefaultIfEmpty()
                                            join ic in db.IndisponibilidadCapacitor on r.idIndisponibilidadesCapacitor equals ic.idCapacitorRemunerado into capR
                                            from ic in capR.DefaultIfEmpty()
                                            where c.activo == true && c.remunerado == true && ic.activo == true && r.activo == true && (ic.fechaSalida >= fechaDesde && ic.fechaEntrada <= fechaHasta)
                                            select new IndisRemuEquipoVM
                                            {
                                                idIndisponibilidadCapacitor = ic.idIndisponibilidadCapacitor,
                                                idCapacitor = c.idCapacitor,
                                                idCapacitorRemunerado = r.idIndisponibilidadesCapacitor,
                                                idPagotran = c.idPagotran,
                                                nombreCapacitor = c.nombreCapacitor,
                                                TensionKv = c.TensionKV,
                                                potenciaMvra = c.potenciaMVAR,
                                                cargoRealxHora = ic.montoXHora,
                                                muestroRealxHora = ic.montoXHora != null ? "$ " + ic.montoXHora : " ",
                                                fechaSalida = ic.fechaSalida,
                                                fechaEntrada = ic.fechaEntrada,
                                                hsIndisponible = ic.hsIndisponible,
                                                minutosIndisponible = ic.minutosIndisponibles,
                                                tipoSalida = ic.tipoSalida,
                                                hsForzada = ic.hsForzada,
                                                hsProgramada = ic.hsProgramada,
                                                activo = r.activo,
                                                MuestroinformoEnTermino = ic.informoEnTermino == true ? "Si" : "No",
                                                totalPenalizacion = ic.totalPenalizado,
                                                informoEnTermino = ic.informoEnTermino,
                                                noPercibido = ic.noPercibido,
                                                lucroCesante = ic.lucroCesante,
                                                motivo = ic.motivo,
                                            }).ToList();


            }
            listaCapacitorRemunerada = listaCapacitorRemunerada.OrderByDescending(x => x.idIndisponibilidadCapacitor).ToList();
            object json = new { data = listaCapacitorRemunerada };
            return json;
        }

        public int AltaCapacitoresIndisponibles(IndisRemuEquipoVM capacitor, DateTime fechaDesdeMonto, DateTime? fechaHastaMonto)
        {

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnCapacitor = db.IndisponibilidadCapacitor.Where(x => x.idCapacitor == capacitor.idCapacitor && x.fechaSalida == capacitor.fechaSalida && x.fechaEntrada == capacitor.fechaEntrada && x.activo == true).FirstOrDefault();
                if (consultaUnCapacitor == null)
                {
                    try
                    {
                        var capacitorRemunerado = db.IndisponibilidadRemuCapacitor.Where(x => x.idCapacitor == capacitor.idCapacitor).FirstOrDefault();
                        var capacit = db.CapCapacitor.Where(x => x.idCapacitor == capacitor.idCapacitor && x.activo == true).FirstOrDefault();
                        var monto = db.IndisponibilidadMontoCapacitor.Where(x => (x.fechaMonto <= fechaDesdeMonto && x.fechaHasta >= fechaHastaMonto) || (x.fechaMonto <= fechaDesdeMonto && x.fechaHasta == null)).FirstOrDefault();
                        var utilidad = db.IndisponibilidadesUtilidades.Where(x => x.idTipoEquipo == 7 && x.activo == true).FirstOrDefault();
                        if (monto == null)
                        {
                            return 303;
                        }


                        DateTime fechaMal = Convert.ToDateTime("1/1/0001 00:00:00");
                        if (capacitor?.fechaEntrada != fechaMal)
                        {
                            var cargoRealxHoras = capacit.potenciaMVAR * monto.monto;

                            var horasIndisponible = (capacitor.fechaEntrada - capacitor.fechaSalida)?.TotalHours;
                            var minDisponibles = Math.Round((decimal)(capacitor.fechaEntrada - capacitor.fechaSalida)?.TotalMinutes);

                            decimal? totalPenalizacion = null;
                            int? Coeficiente = utilidad.valor;

                            decimal? hsForzada = 0;
                            decimal? hsProgramada = 0;

                            if (capacitor.tipoSalida == "F" || capacitor.tipoSalida == "R")
                            {
                                hsForzada = Convert.ToDecimal(horasIndisponible);
                            }
                            else if (capacitor.tipoSalida == "P" || capacitor.tipoSalida == "RP")
                            {
                                hsProgramada = Convert.ToDecimal(horasIndisponible);

                            }

                            //CR Y ENS NO TIENE NINGUN CALCULO

                            decimal? kPyENS = null;
                            if (capacitor.tipoSalida == "P" || capacitor.tipoSalida == "RP")
                            {

                                kPyENS = Convert.ToDecimal(Coeficiente * 0.1);
                            }
                            else
                            {
                                kPyENS = Convert.ToDecimal(Coeficiente * 1);
                            }
                            if (capacitor.ENS == true)
                            {
                                kPyENS *= 1;
                            }
                            else
                            {
                                kPyENS = (kPyENS * Convert.ToDecimal(0.1));
                            }

                            bool AUT;
                            decimal? penalizacionPrograma = null;
                            decimal? penalizacionForzada1 = 0;
                            decimal? penalizacionForzada2 = 0;

                            if (capacitor.tipoSalida == "P")
                            {
                                AUT = true;
                                var redondeo = Math.Round((decimal)(minDisponibles / 60), 2);
                                penalizacionPrograma = Convert.ToDecimal(cargoRealxHoras * kPyENS * redondeo);

                                totalPenalizacion = penalizacionPrograma;
                            }
                            else if (capacitor.tipoSalida == "F")
                            {
                                AUT = false;
                                var redondeo = Math.Round((decimal)(minDisponibles / 60), 2);
                                penalizacionForzada1 = Convert.ToDecimal(cargoRealxHoras * kPyENS * redondeo);
                                penalizacionForzada2 = Convert.ToDecimal(cargoRealxHoras * kPyENS);

                                totalPenalizacion = penalizacionForzada1 + penalizacionForzada2;


                            }


                            if (capacitor.informoEnTermino == true)
                            {
                                totalPenalizacion = totalPenalizacion * 1;
                            }
                            else
                            {
                                totalPenalizacion = totalPenalizacion * 2;
                            }


                            decimal? noPercibido = cargoRealxHoras * Convert.ToDecimal(horasIndisponible);
                            decimal? lucroCesante = noPercibido + totalPenalizacion;

                            IndisponibilidadCapacitor cap = new IndisponibilidadCapacitor
                            {
                                idCapacitorRemunerado = capacitorRemunerado.idIndisponibilidadesCapacitor,
                                idCapacitor = capacit.idCapacitor,
                                idMonto = monto.idIndisponibilidadMontosCapacitor,
                                montoXHora = cargoRealxHoras,
                                fechaSalida = capacitor.fechaSalida,
                                fechaEntrada = capacitor.fechaEntrada,
                                hsIndisponible = Convert.ToDecimal(horasIndisponible),
                                minutosIndisponibles = Convert.ToDecimal(minDisponibles),
                                tipoSalida = capacitor.tipoSalida,
                                activo = true,
                                ENS = capacitor.ENS,
                                informoEnTermino = capacitor.informoEnTermino,
                                totalPenalizado = totalPenalizacion,
                                noPercibido = noPercibido,
                                motivo = capacitor.motivo,
                                lucroCesante = lucroCesante,
                                idIndisponibilidad = capacitor.idNovedadIndisponibilidad,
                            };
                            db.IndisponibilidadCapacitor.Add(cap);
                            db.SaveChanges();

                            //VIENE DE LO DE INDISPONIBILIDADES DEL UCIEL
                            if (capacitor.noCargar != true)//El no cargar es para que no cree otrs indisponibilidad por que viene de lo del Uciel
                            {
                                ServicioIndisponibilidad servicioIndisUciel = new ServicioIndisponibilidad();
                                NovNovedadIndisponibilidad indis = new NovNovedadIndisponibilidad { };
                                indis.activo = true;
                                indis.FechaEntrada = capacitor.fechaEntrada;
                                indis.FechaSalida = capacitor.fechaSalida;
                                indis.PorcentajeIndisponible = 100;
                                indis.idEquipo = capacitor.idCapacitor;
                                indis.idTipoEquipo = 7;
                                indis.EnergiaNoSuministrada = capacitor.ENS;
                                indis.idTipoIndisponibilidad = capacitor.idTipoIndisponibilidad; //Es solo para lo del uciel
                                indis.descripcion = capacitor.motivo;
                                indis.fueraDeServicio = true;
                                indis.idIndisponibilidadRemu = cap.idIndisponibilidadCapacitor;
                                if (capacitor.tipoSalida == "F")
                                {
                                    indis.itn = 1;
                                }
                                else if (capacitor.tipoSalida == "P")
                                {
                                    indis.itn = 3;
                                }

                                db.NovNovedadIndisponibilidad.Add(indis);
                                db.SaveChanges();
                                var ultimaIndi = db.NovNovedadIndisponibilidad.Where(x => x.idIndisponibilidadRemu == cap.idIndisponibilidadCapacitor && x.activo == true && x.idTipoEquipo == 7).FirstOrDefault();
                                cap.idIndisponibilidad = ultimaIndi.idNovedadIndisponibilidad;
                            }

                            db.SaveChanges();
                            calculoMesesCapacitores(capacitor.fechaSalida);
                            return 200;
                        }
                        else
                        {

                            IndisponibilidadCapacitor cap = new IndisponibilidadCapacitor
                            {
                                idCapacitorRemunerado = capacitorRemunerado.idIndisponibilidadesCapacitor,
                                idCapacitor = capacit.idCapacitor,
                                idMonto = monto.idIndisponibilidadMontosCapacitor,
                                montoXHora = capacitorRemunerado.cargoRealxHoras,
                                fechaSalida = capacitor.fechaSalida,
                                tipoSalida = capacitor.tipoSalida,
                                activo = true,
                                ENS = capacitor.ENS,
                                informoEnTermino = capacitor.informoEnTermino,
                                motivo = capacitor.motivo,
                                //idIndisponibilidad = trafo.idNovedadIndisponibilidad,
                            };
                            db.IndisponibilidadCapacitor.Add(cap);
                            db.SaveChanges();
                            calculoMesesCapacitores(capacitor.fechaSalida);
                            return 200;
                        }

                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }

            }
        }

        public int ModificarCapacitoresIndisponibles(IndisRemuEquipoVM capacitor, DateTime fechaDesdeMonto, DateTime? fechaHastaMonto)
        {

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnCapacitor = db.IndisponibilidadCapacitor.Where(x => x.idIndisponibilidadCapacitor == capacitor.idIndisponibilidadCapacitor && x.activo == true).FirstOrDefault();
                if (consultaUnCapacitor != null)
                {
                    try
                    {
                        var capacitorRemunerado = db.IndisponibilidadRemuCapacitor.Where(x => x.idCapacitor == capacitor.idCapacitor).FirstOrDefault();
                        var capacit = db.CapCapacitor.Where(x => x.idCapacitor == capacitor.idCapacitor && x.activo == true).FirstOrDefault();
                        var monto = db.IndisponibilidadMontoCapacitor.Where(x => (x.fechaMonto <= fechaDesdeMonto && x.fechaHasta >= fechaHastaMonto) || (x.fechaMonto <= fechaDesdeMonto && x.fechaHasta == null)).FirstOrDefault();
                        var utilidad = db.IndisponibilidadesUtilidades.Where(x => x.idTipoEquipo == 7 && x.activo == true).FirstOrDefault();
                        if (monto == null)
                        {
                            return 303;
                        }


                        DateTime fechaMal = Convert.ToDateTime("1/1/0001 00:00:00");
                        if (capacitor?.fechaEntrada != fechaMal)
                        {
                            var cargoRealxHoras = capacit.potenciaMVAR * monto.monto;

                            var horasIndisponible = (capacitor.fechaEntrada - capacitor.fechaSalida)?.TotalHours;
                            var minDisponibles = Math.Round((decimal)(capacitor.fechaEntrada - capacitor.fechaSalida)?.TotalMinutes);

                            decimal? totalPenalizacion = null;
                            int? Coeficiente = utilidad.valor;

                            decimal? hsForzada = 0;
                            decimal? hsProgramada = 0;

                            if (capacitor.tipoSalida == "F" || capacitor.tipoSalida == "R")
                            {
                                hsForzada = Convert.ToDecimal(horasIndisponible);
                            }
                            else if (capacitor.tipoSalida == "P" || capacitor.tipoSalida == "RP")
                            {
                                hsProgramada = Convert.ToDecimal(horasIndisponible);

                            }

                            //CR Y ENS NO TIENE NINGUN CALCULO

                            decimal? kPyENS = null;
                            if (capacitor.tipoSalida == "P" || capacitor.tipoSalida == "RP")
                            {

                                kPyENS = Convert.ToDecimal(Coeficiente * 0.1);
                            }
                            else
                            {
                                kPyENS = Convert.ToDecimal(Coeficiente * 1);
                            }
                            if (capacitor.ENS == true)
                            {
                                kPyENS *= 1;
                            }
                            else
                            {
                                kPyENS = (kPyENS * Convert.ToDecimal(0.1));
                            }

                            bool AUT;
                            decimal? penalizacionPrograma = null;
                            decimal? penalizacionForzada1 = 0;
                            decimal? penalizacionForzada2 = 0;

                            if (capacitor.tipoSalida == "P")
                            {
                                AUT = true;
                                var redondeo = Math.Round((decimal)(minDisponibles / 60), 2);
                                penalizacionPrograma = Convert.ToDecimal(cargoRealxHoras * kPyENS * redondeo);

                                totalPenalizacion = penalizacionPrograma;
                            }
                            else if (capacitor.tipoSalida == "F")
                            {
                                AUT = false;
                                var redondeo = Math.Round((decimal)(minDisponibles / 60), 2);
                                penalizacionForzada1 = Convert.ToDecimal(cargoRealxHoras * kPyENS * redondeo);
                                penalizacionForzada2 = Convert.ToDecimal(cargoRealxHoras * kPyENS);

                                totalPenalizacion = penalizacionForzada1 + penalizacionForzada2;


                            }


                            if (capacitor.informoEnTermino == true)
                            {
                                totalPenalizacion = totalPenalizacion * 1;
                            }
                            else
                            {
                                totalPenalizacion = totalPenalizacion * 2;
                            }


                            decimal? noPercibido = cargoRealxHoras * Convert.ToDecimal(horasIndisponible);
                            decimal? lucroCesante = noPercibido + totalPenalizacion;


                            consultaUnCapacitor.idCapacitorRemunerado = capacitorRemunerado.idIndisponibilidadesCapacitor;
                            consultaUnCapacitor.idCapacitor = capacit.idCapacitor;
                            consultaUnCapacitor.idMonto = monto.idIndisponibilidadMontosCapacitor;
                            consultaUnCapacitor.montoXHora = cargoRealxHoras;
                            consultaUnCapacitor.fechaSalida = capacitor.fechaSalida;
                            consultaUnCapacitor.fechaEntrada = capacitor.fechaEntrada;
                            consultaUnCapacitor.hsIndisponible = Convert.ToDecimal(horasIndisponible);
                            consultaUnCapacitor.minutosIndisponibles = Convert.ToDecimal(minDisponibles);
                            consultaUnCapacitor.tipoSalida = capacitor.tipoSalida;
                            consultaUnCapacitor.activo = true;
                            consultaUnCapacitor.ENS = capacitor.ENS;
                            consultaUnCapacitor.informoEnTermino = capacitor.informoEnTermino;
                            consultaUnCapacitor.totalPenalizado = totalPenalizacion;
                            consultaUnCapacitor.noPercibido = noPercibido;
                            consultaUnCapacitor.motivo = capacitor.motivo;
                            consultaUnCapacitor.lucroCesante = lucroCesante;
                            //idIndisponibilidad = trafo.idNovedadIndisponibilidad,
                            db.SaveChanges();

                            //VIENE DE LO DE INDISPONIBILIDADES DEL UCIEL
                            ServicioIndisponibilidad serviIndispUciel = new ServicioIndisponibilidad();
                            var indisExistente = db.NovNovedadIndisponibilidad.Where(x => x.idNovedadIndisponibilidad == consultaUnCapacitor.idIndisponibilidad && x.activo == true && x.idTipoEquipo == 7).FirstOrDefault();

                            if (indisExistente != null) //SI ES UNA EDICION
                            {
                                indisExistente.FechaEntrada = capacitor.fechaEntrada;
                                indisExistente.FechaSalida = capacitor.fechaSalida;
                                indisExistente.PorcentajeIndisponible = Convert.ToDouble(totalPenalizacion);
                                indisExistente.idEquipo = capacitor.idCapacitor;
                                indisExistente.idTipoEquipo = 7;
                                indisExistente.descripcion = capacitor.motivo;
                                indisExistente.EnergiaNoSuministrada = capacitor.ENS;
                                indisExistente.idTipoIndisponibilidad = capacitor.idTipoIndisponibilidad;

                                if (capacitor.tipoSalida == "F")
                                {
                                    indisExistente.itn = 1;
                                }
                                else if (capacitor.tipoSalida == "P")
                                {
                                    indisExistente.itn = 3;
                                }
                                consultaUnCapacitor.idIndisponibilidad = indisExistente.idNovedadIndisponibilidad;

                                db.SaveChanges();
                            }
                            db.SaveChanges();
                            calculoMesesCapacitores(capacitor.fechaSalida);
                            return 200;
                        }
                        else
                        {

                            consultaUnCapacitor.idCapacitorRemunerado = capacitorRemunerado.idIndisponibilidadesCapacitor;
                            consultaUnCapacitor.idCapacitor = capacit.idCapacitor;
                            consultaUnCapacitor.idMonto = monto.idIndisponibilidadMontosCapacitor;
                            consultaUnCapacitor.montoXHora = capacitorRemunerado.cargoRealxHoras;
                            consultaUnCapacitor.fechaSalida = capacitor.fechaSalida;
                            consultaUnCapacitor.fechaEntrada = capacitor.fechaEntrada;
                            consultaUnCapacitor.informoEnTermino = capacitor.informoEnTermino;
                            consultaUnCapacitor.motivo = capacitor.motivo;
                            //idIndisponibilidad = trafo.idNovedadIndisponibilidad,
                            db.SaveChanges();
                            calculoMesesCapacitores(capacitor.fechaSalida);
                            return 200;
                        }

                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }

            }
        }

        public int EliminarCapacitorIndisponible(IndisRemuEquipoVM capacitor)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaCapacitorIndis = db.IndisponibilidadCapacitor.Where(x => x.idIndisponibilidadCapacitor == capacitor.idIndisponibilidadCapacitor && x.activo == true).FirstOrDefault();
                if (consultaCapacitorIndis != null)
                {
                    consultaCapacitorIndis.activo = false;
                    db.SaveChanges();

                    var CapNovIndis = db.NovNovedadIndisponibilidad.Where(x => x.idNovedadIndisponibilidad == consultaCapacitorIndis.idIndisponibilidad && x.activo == true && x.idTipoEquipo == 7).FirstOrDefault();
                    if (CapNovIndis != null)
                    {
                        CapNovIndis.activo = false;
                        db.SaveChanges();
                    }

                }
                calculoMesesCapacitores(consultaCapacitorIndis.fechaSalida);
                return 200;
            }
        }

        public int ModificoCapacitorIndisponibles(IndisponibilidadCapacitor capacitor, int? idResolucion) //Para usarlo con otras funciones
        {

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnCapacitor = db.IndisponibilidadCapacitor.Where(x => x.idCapacitor == capacitor.idCapacitor).FirstOrDefault();
                var capacitorRemunerado = db.IndisponibilidadRemuCapacitor.Where(x => x.idCapacitor == capacitor.idCapacitor && x.idResolucion == idResolucion
                                        && x.Mes == capacitor.fechaSalida.Value.Month && x.Anio == capacitor.fechaSalida.Value.Year).FirstOrDefault();

                if (consultaUnCapacitor != null)
                {
                    try
                    {
                        var capacit = db.CapCapacitor.Where(x => x.idCapacitor == capacitor.idCapacitor && x.activo == true).FirstOrDefault();
                        // var monto = db.IndisponibilidadMontoCapacitor.Where(x => (x.fechaMonto <= fechaDesdeMonto && x.fechaHasta >= fechaHastaMonto) || (x.fechaMonto <= fechaDesdeMonto && x.fechaHasta == null)).FirstOrDefault();
                        var utilidad = db.IndisponibilidadesUtilidades.Where(x => x.idTipoEquipo == 7 && x.activo == true).FirstOrDefault();

                        var monto = new IndisponibilidadMontoCapacitor();
                        if (capacitor.fechaEntrada >= DateTime.Now)
                        {
                            monto = db.IndisponibilidadMontoCapacitor.Where(x => x.activo == true).FirstOrDefault();
                            consultaUnCapacitor.idMonto = monto.idIndisponibilidadMontosCapacitor;
                        }
                        else//esto es mas que nada para el historial
                        {
                            monto = db.IndisponibilidadMontoCapacitor.Where(x => x.idIndisponibilidadMontosCapacitor == capacitor.idMonto).FirstOrDefault();
                        }

                        decimal? montoXhoraRemunerado = 0;
                        if (monto != null)
                        {
                            montoXhoraRemunerado = monto.monto;
                        }

                        DateTime fechaMal = Convert.ToDateTime("1/1/0001 00:00:00");
                        if (capacitor?.fechaEntrada != fechaMal)
                        {
                            var cargoRealxHoras = capacit.potenciaMVAR * monto.monto;

                            var horasIndisponible = (capacitor.fechaEntrada - capacitor.fechaSalida)?.TotalHours;
                            var minDisponibles = Math.Round((decimal)(capacitor.fechaEntrada - capacitor.fechaSalida)?.TotalMinutes);

                            decimal? totalPenalizacion = null;
                            int? Coeficiente = utilidad.valor;

                            decimal? hsForzada = 0;
                            decimal? hsProgramada = 0;

                            if (capacitor.tipoSalida == "F" || capacitor.tipoSalida == "R")
                            {
                                hsForzada = Convert.ToDecimal(horasIndisponible);
                            }
                            else if (capacitor.tipoSalida == "P" || capacitor.tipoSalida == "RP")
                            {
                                hsProgramada = Convert.ToDecimal(horasIndisponible);

                            }

                            //CR Y ENS NO TIENE NINGUN CALCULO

                            decimal? kPyENS = null;
                            if (capacitor.tipoSalida == "P" || capacitor.tipoSalida == "RP")
                            {

                                kPyENS = Convert.ToDecimal(Coeficiente * 0.1);
                            }
                            else
                            {
                                kPyENS = Convert.ToDecimal(Coeficiente * 1);
                            }
                            if (capacitor.ENS == true)
                            {
                                kPyENS *= 1;
                            }
                            else
                            {
                                kPyENS = (kPyENS * Convert.ToDecimal(0.1));
                            }

                            bool AUT;
                            decimal? penalizacionPrograma = null;
                            decimal? penalizacionForzada1 = 0;
                            decimal? penalizacionForzada2 = 0;

                            if (capacitor.tipoSalida == "P")
                            {
                                AUT = true;
                                var redondeo = Math.Round((decimal)(minDisponibles / 60), 2);
                                penalizacionPrograma = Convert.ToDecimal(cargoRealxHoras * kPyENS * redondeo);

                                totalPenalizacion = penalizacionPrograma;
                            }
                            else if (capacitor.tipoSalida == "F")
                            {
                                AUT = false;
                                var redondeo = Math.Round((decimal)(minDisponibles / 60), 2);
                                penalizacionForzada1 = Convert.ToDecimal(cargoRealxHoras * kPyENS * redondeo);
                                penalizacionForzada2 = Convert.ToDecimal(cargoRealxHoras * kPyENS);

                                totalPenalizacion = penalizacionForzada1 + penalizacionForzada2;


                            }


                            if (capacitor.informoEnTermino == true)
                            {
                                totalPenalizacion = totalPenalizacion * 1;
                            }
                            else
                            {
                                totalPenalizacion = totalPenalizacion * 2;
                            }


                            decimal? noPercibido = cargoRealxHoras * Convert.ToDecimal(horasIndisponible);
                            decimal? lucroCesante = noPercibido + totalPenalizacion;


                            consultaUnCapacitor.idCapacitorRemunerado = capacitorRemunerado.idIndisponibilidadesCapacitor;
                            consultaUnCapacitor.idCapacitor = capacit.idCapacitor;
                            consultaUnCapacitor.idMonto = monto.idIndisponibilidadMontosCapacitor;
                            consultaUnCapacitor.montoXHora = cargoRealxHoras;
                            consultaUnCapacitor.fechaSalida = capacitor.fechaSalida;
                            consultaUnCapacitor.fechaEntrada = capacitor.fechaEntrada;
                            consultaUnCapacitor.hsIndisponible = Convert.ToDecimal(horasIndisponible);
                            consultaUnCapacitor.minutosIndisponibles = Convert.ToDecimal(minDisponibles);
                            consultaUnCapacitor.tipoSalida = capacitor.tipoSalida;
                            consultaUnCapacitor.activo = true;
                            consultaUnCapacitor.ENS = capacitor.ENS;
                            consultaUnCapacitor.informoEnTermino = capacitor.informoEnTermino;
                            consultaUnCapacitor.totalPenalizado = totalPenalizacion;
                            consultaUnCapacitor.noPercibido = noPercibido;
                            consultaUnCapacitor.motivo = capacitor.motivo;
                            consultaUnCapacitor.lucroCesante = lucroCesante;
                            db.SaveChanges();


                            //Esto es para indisponibilidades que hizo el uciel
                            ServicioIndisponibilidad serviIndispUciel = new ServicioIndisponibilidad();
                            var indisExistente = db.NovNovedadIndisponibilidad.Where(x => x.idIndisponibilidadRemu == consultaUnCapacitor.idIndisponibilidadCapacitor && x.activo == true && x.idTipoEquipo == 7).FirstOrDefault();

                            if (indisExistente != null) //SI ES UNA EDICION
                            {
                                indisExistente.FechaEntrada = capacitor.fechaEntrada;
                                indisExistente.FechaSalida = capacitor.fechaSalida;
                                indisExistente.PorcentajeIndisponible = 100;
                                indisExistente.idEquipo = capacit.idCapacitor;
                                indisExistente.idTipoEquipo = 7;
                                indisExistente.descripcion = capacitor.motivo;
                                indisExistente.fueraDeServicio = capacitor.informoEnTermino;


                                if (consultaUnCapacitor.tipoSalida == "F")
                                {
                                    indisExistente.itn = 1;
                                }
                                else if (consultaUnCapacitor.tipoSalida == "P")
                                {
                                    indisExistente.itn = 3;
                                }
                                db.SaveChanges();
                            }
                            calculoMesesCapacitores(capacitor.fechaSalida);
                            return 200;
                        }
                        else
                        {
                            return 404;
                        }

                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }

            }
        }


        //FactorK
        public IndisRemuEquipoVM obtenerCoeficienteDePenalizacion()
        {
            IndisRemuEquipoVM factorPenalizacion = new IndisRemuEquipoVM();

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                factorPenalizacion = (from f in db.IndisponibilidadesUtilidades
                                      where f.activo == true && f.idTipoEquipo == 7
                                      select new IndisRemuEquipoVM
                                      {
                                          idUtilidad = f.idUtilidad,
                                          idTipoEquipo = f.idTipoEquipo,
                                          nombreUtilidad = f.nombreUtilidad,
                                          valorUtilidad = f.valor,
                                          activo = f.activo,
                                      }).FirstOrDefault();

            }
            return factorPenalizacion;
        }

        public int CambiarCoeficienteDePenalizacion(int factorK)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUtilidad = db.IndisponibilidadesUtilidades.Where(x => x.idTipoEquipo == 7 && x.activo == true).FirstOrDefault();
                if (consultaUtilidad != null)
                {
                    consultaUtilidad.valor = factorK;
                    db.SaveChanges();
                }
                return 200;
            }
        }

        //Generar Excel
        public HojaExcelVM GenerarReporteExcelCapacitores(string fechaDesde, string fechaHasta)
        {
            var fechaDesdeDate = Convert.ToDateTime(fechaDesde);
            var fechaHastaDate = Convert.ToDateTime(fechaHasta);
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    ServicioEquipo servEquipo = new ServicioEquipo();
                    List<IndisRemuEquipoVM> capacitores = new List<IndisRemuEquipoVM>();
                    capacitores = (from i in db.IndisponibilidadCapacitor
                                   join r in db.IndisponibilidadRemuCapacitor on i.idCapacitorRemunerado equals r.idIndisponibilidadesCapacitor
                                   join c in db.CapCapacitor on i.idCapacitor equals c.idCapacitor
                                   join m in db.IndisponibilidadMontoCapacitor on i.idMonto equals m.idIndisponibilidadMontosCapacitor
                                   where i.activo == true && fechaDesdeDate <= i.fechaSalida && fechaHastaDate >= i.fechaEntrada
                                   select new { i, r, m, c }).AsEnumerable().Select(x => new IndisRemuEquipoVM
                                   {
                                       idIndisponibilidadCapacitor = x.i.idIndisponibilidadCapacitor,
                                       idCapacitorRemunerado = x.i.idCapacitorRemunerado,
                                       idCapacitor = x.c.idCapacitor,
                                       idMonto = x.m.idIndisponibilidadMontosCapacitor,
                                       montoXhora = x.i.montoXHora,
                                       fechaSalida = x.i.fechaSalida,
                                       fechaEntrada = x.i.fechaEntrada,
                                       hsIndisponible = x.i.hsIndisponible,
                                       minutosIndisponible = x.i.minutosIndisponibles,
                                       tipoSalida = x.i.tipoSalida,
                                       activo = x.i.activo,
                                       CR = x.i.CR,
                                       ENS = x.i.ENS,
                                       informoEnTermino = x.i.informoEnTermino,
                                       totalPenalizacion = x.i.totalPenalizado,
                                       noPercibido = x.i.noPercibido,
                                       motivo = x.i.motivo,
                                       lucroCesante = x.i.lucroCesante,
                                       //Capacitor
                                       idPagotran = x.c.idPagotran,
                                       nombreCapacitor = x.c.nombreCapacitor,
                                       TensionKv = x.c.TensionKV,
                                       potenciaMvra = x.c.potenciaMVAR == 0 ? 0 : x.c.potenciaMVAR,
                                       //Monto
                                       montoIndis = new MontoIndisponibilidadesVM
                                       {
                                           monto = x.m.monto,
                                           nombreMonto = x.m.nombreMonto,
                                           resolucion = x.m.resolucion,
                                       }
                                   }).OrderByDescending(x => x.fechaSalida).ToList();


                    GenerarExcel excel = new GenerarExcel();
                    HojaExcelVM hojaDetalle = new HojaExcelVM();
                    hojaDetalle.totales = new DetalleExcelVM();

                    decimal? totalPenalizacion = 0;
                    decimal? totalNoPercibo = 0;
                    decimal? totalLucroCesante = 0;

                    var col1 = "Mes";
                    var col2 = "Equipo";
                    var col3 = "ID";
                    var col4 = "Capacitor";
                    var col5 = "Tension";
                    var col6 = "POT.[MVA]";
                    //var col7 = "Potencias";
                    var col8 = "$/h";
                    var col9 = "Salida";
                    var col10 = "Entrada";
                    var col11 = "Hs.Indisp.";
                    var col12 = "Minutos Indis.";
                    var col13 = "Tipo Salida";
                    //var col13 = "Hs Prog";
                    // var col14 = "CR%";
                    // var col15 = "E.N.S.";
                    var col16 = "Informo en termino";
                    var col17 = "Total Penalización";
                    //var col18 = "Cargo Real";
                    var col19 = "No Percibido";
                    var col20 = "Lucro Cesante";
                    var col21 = "Motivo";
                    var col22 = "";
                    var col23 = "Tarifa";
                    var col24 = "Resolucion";

                    hojaDetalle.encabezado = new List<string>();

                    hojaDetalle.encabezado.Add(col1);
                    hojaDetalle.encabezado.Add(col2);
                    hojaDetalle.encabezado.Add(col3);
                    hojaDetalle.encabezado.Add(col4);
                    hojaDetalle.encabezado.Add(col5);
                    hojaDetalle.encabezado.Add(col6);
                    //hojaDetalle.encabezado.Add(col7);
                    hojaDetalle.encabezado.Add(col8);
                    hojaDetalle.encabezado.Add(col9);
                    hojaDetalle.encabezado.Add(col10);
                    hojaDetalle.encabezado.Add(col11);
                    hojaDetalle.encabezado.Add(col12);
                    hojaDetalle.encabezado.Add(col13);
                    //hojaDetalle.encabezado.Add(col14);
                    //hojaDetalle.encabezado.Add(col15);
                    hojaDetalle.encabezado.Add(col16);
                    hojaDetalle.encabezado.Add(col17);
                    //hojaDetalle.encabezado.Add(col18);
                    hojaDetalle.encabezado.Add(col19);
                    hojaDetalle.encabezado.Add(col20);
                    hojaDetalle.encabezado.Add(col21);
                    hojaDetalle.encabezado.Add(col22);
                    hojaDetalle.encabezado.Add(col23);
                    hojaDetalle.encabezado.Add(col24);

                    hojaDetalle.listaFilas = new List<DetalleExcelVM>();


                    foreach (var item in capacitores)
                    {

                        DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                        Calendar cal = dfi.Calendar;
                        DateTime fechaActual = Convert.ToDateTime(item.fechaSalida);
                        string mesActual = fechaActual.ToString("MMMM");
                        hojaDetalle.unaFila = new DetalleExcelVM();
                        hojaDetalle.unaFila.datoPorFila = new List<string>();
                        hojaDetalle.unaFila.datoPorFila.Add(mesActual.ToUpperInvariant());
                        hojaDetalle.unaFila.datoPorFila.Add("Capacitores");
                        hojaDetalle.unaFila.datoPorFila.Add(item.idPagotran.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.nombreCapacitor);
                        hojaDetalle.unaFila.datoPorFila.Add(item.TensionKv.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.potenciaMvra.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.montoXhora.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.fechaSalida.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.fechaEntrada.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.hsIndisponible.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.minutosIndisponible.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.tipoSalida);
                        //hojaDetalle.unaFila.datoPorFila.Add(item.hsProgramada != null ? item.hsProgramada.ToString() : "-");
                        //hojaDetalle.unaFila.datoPorFila.Add(item.hsForzada != null ? item.hsForzada.ToString() : "-");
                        // hojaDetalle.unaFila.datoPorFila.Add(item.CR.ToString());
                        //hojaDetalle.unaFila.datoPorFila.Add(item.ENS == true ? "SI" : "NO");
                        hojaDetalle.unaFila.datoPorFila.Add(item.informoEnTermino == true ? "SI" : "NO");
                        hojaDetalle.unaFila.datoPorFila.Add(item.totalPenalizacion.ToString());
                        //hojaDetalle.unaFila.datoPorFila.Add(item.cargoReal.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.noPercibido.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.lucroCesante.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.motivo);
                        hojaDetalle.unaFila.datoPorFila.Add(" ");
                        hojaDetalle.unaFila.datoPorFila.Add(item.montoIndis.monto.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.montoIndis.resolucion);
                        hojaDetalle.listaFilas.Add(hojaDetalle.unaFila);

                        totalPenalizacion += item.totalPenalizacion;
                        totalNoPercibo += item.noPercibido;
                        totalLucroCesante += item.lucroCesante;
                    }

                    //De aca hago los totales 
                    hojaDetalle.totales.datoPorFilaTotales = new List<string>();
                    hojaDetalle.totales.datoPorFilaTotales.Add(totalPenalizacion.ToString());
                    hojaDetalle.totales.datoPorFilaTotales.Add(totalNoPercibo.ToString());
                    hojaDetalle.totales.datoPorFilaTotales.Add(totalLucroCesante.ToString());
                    hojaDetalle.listaFilas.Add(hojaDetalle.totales);

                    var montosCapacitor = db.IndisponibilidadMontoCapacitor.Where(x => x.activo == true).ToList();
                    foreach (var item in montosCapacitor)
                    {
                        hojaDetalle.unaFilaMonto = new DetalleExcelVM();
                        hojaDetalle.unaFilaMonto.datoPorFilaMonto = new List<string>();
                        hojaDetalle.unaFilaMonto.datoPorFilaMonto.Add(item.nombreMonto);
                        hojaDetalle.unaFilaMonto.datoPorFilaMonto.Add("$ " + item.monto.ToString());
                        hojaDetalle.unaFilaMonto.datoPorFilaMonto.Add(item.resolucion != "" ? item.resolucion : " - ");
                        hojaDetalle.listaFilas.Add(hojaDetalle.unaFilaMonto);
                    }

                    var datoExcel = excel.GenerarExcelGeneralRemunerado(hojaDetalle, "Reactivos");

                    return datoExcel;

                }
            }
            catch (Exception e)
            {
                var datoExcel = new HojaExcelVM();
                return datoExcel;
            }

        }

        #endregion

        #region Meses
        public Object ObtenerMontosMes(DateTime fecha)
        {
            List<MontoIndisponibilidadesVM> montos;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                montos = (from m in db.IndisponibilidadRemuneracionesMeses
                          where m.activo == true && m.fecha.Value.Month == fecha.Month && m.fecha.Value.Year == fecha.Year
                          select new MontoIndisponibilidadesVM
                          {
                              idRemuneracionMes = m.idRemuneracionMes,
                              idEquipoMes = m.idEquipo,
                              nombreRemuneracion = m.nombreRemuneracion,
                              calculoParcial = m.calculoParcial,
                              calculoTotal = m.caculoTotal,
                              calculoCammesa = m.calculoCammesa == null ? 0 : m.calculoCammesa,
                              diferencia = m.diferencia == null ? 0 : m.diferencia,
                              fecha = m.fecha,

                              mostrarcalculoParcial = "$ " + m.calculoParcial,
                              mostrarcalculoTotal = "$ " + m.caculoTotal,
                              mostrarcalculoCammesa = m.calculoCammesa == null ? "-" : "$ " + m.calculoCammesa,
                              mostrardiferencia = m.diferencia == null ? "-" : "$ " + m.diferencia,

                          }).ToList();
            }
            object json = new { data = montos };
            return json;

        }

        //Calculo para los meses Lineas
        public void calculoMesesLinea(DateTime? mesACambiar)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var listasLineasRemuneradas = db.IndisponibilidadRemuLineas.Where(x => x.activo == true && x.Mes == mesACambiar.Value.Month && x.Anio == mesACambiar.Value.Year).ToList();
                decimal totalCargoRealxHora = 0;
                var parcial = 0;
                //foreach (var item in listasLineasRemuneradas)
                //{
                //    totalCargoRealxHora += Convert.ToDecimal(item.cargoRealxHoras);
                //}
                totalCargoRealxHora = Convert.ToDecimal(listasLineasRemuneradas.Where(x => x.activo == true).Sum(x => x.cargoRealxHoras));
                var mes = Convert.ToDateTime(mesACambiar).Month;
                var anio = Convert.ToDateTime(mesACambiar).Year;

                DateTime firstDayOfMonth = new DateTime(anio, mes, 1);

                // Obtener el primer día del siguiente mes para calcular el último día del mes actual
                DateTime firstDayOfNextMonth = firstDayOfMonth.AddMonths(1);

                // Calcular el número de días en el mes actual
                int daysInMonth = (firstDayOfNextMonth - firstDayOfMonth).Days;

                // Calcular la cantidad de horas en el mes
                int hoursInMonth = daysInMonth * 24;

                totalCargoRealxHora = totalCargoRealxHora * hoursInMonth;

                decimal? cargoTotalNoPercibido = 0;
                var listasLineasIndisponibles = db.IndisponibilidadLineas.Where(x => x.activo == true && ((x.fechaSalida.Value.Month == mes && x.fechaSalida.Value.Year == anio) || (x.fechaEntrada.Value.Month == mes && x.fechaEntrada.Value.Year == anio))).ToList();
                foreach (var item in listasLineasIndisponibles)
                {
                    cargoTotalNoPercibido += item.cargoReal * item.hsIndisponible;
                }

                var calculoParcial = totalCargoRealxHora - cargoTotalNoPercibido;

                var mesCalculado = db.IndisponibilidadRemuneracionesMeses.Where(x => x.fecha.Value.Month == mes && x.fecha.Value.Year == anio && x.idEquipo == 4).FirstOrDefault();
                if (mesCalculado != null)
                {
                    mesCalculado.calculoParcial = calculoParcial;
                    mesCalculado.caculoTotal = calculoParcial;
                    mesCalculado.diferencia = mesCalculado.calculoCammesa - mesCalculado.caculoTotal;
                    db.SaveChanges();
                }
                else
                {
                    IndisponibilidadRemuneracionesMeses nuevo = new IndisponibilidadRemuneracionesMeses()
                    {
                        idEquipo = 4,
                        nombreRemuneracion = "CAPAC.  DE TRANSPORTE",
                        calculoParcial = calculoParcial,
                        caculoTotal = calculoParcial,
                        calculoCammesa = 0,
                        diferencia = 0,
                        fecha = mesACambiar,
                        activo = true,
                    };
                    db.IndisponibilidadRemuneracionesMeses.Add(nuevo);
                    db.SaveChanges();
                }
            }
        }
        //Calculo para los meses Trafos
        public void calculoMesesTrafo(DateTime? mesCambio)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var listasTrafosRemunerados = db.IndisponibilidadRemuTrafo.Where(x => x.activo == true && x.Mes == mesCambio.Value.Month && x.Anio == mesCambio.Value.Year).ToList();
                decimal? totalCargoRealxHora = 0;
                var parcial = 0;
                //foreach (var item in listasTrafosRemunerados)
                //{
                //    totalCargoRealxHora += item.cargoRealxHoras;
                //}
                totalCargoRealxHora = Convert.ToDecimal(listasTrafosRemunerados.Where(x => x.activo == true).Sum(x => x.cargoRealxHoras));
                var mes = Convert.ToDateTime(mesCambio).Month;
                var anio = Convert.ToDateTime(mesCambio).Year;

                DateTime firstDayOfMonth = new DateTime(anio, mes, 1);

                // Obtener el primer día del siguiente mes para calcular el último día del mes actual
                DateTime firstDayOfNextMonth = firstDayOfMonth.AddMonths(1);

                // Calcular el número de días en el mes actual
                int daysInMonth = (firstDayOfNextMonth - firstDayOfMonth).Days;

                // Calcular la cantidad de horas en el mes
                int hoursInMonth = daysInMonth * 24;

                totalCargoRealxHora = totalCargoRealxHora * hoursInMonth;

                decimal? cargoTotalNoPercibido = 0;
                var listasTrafosIndisponibles = db.IndisponibilidadTrafos.Where(x => x.activo == true && ((x.fechaSalida.Value.Month == mes && x.fechaSalida.Value.Year == anio) || (x.fechaEntrada.Value.Month == mes && x.fechaEntrada.Value.Year == anio))).ToList();
                foreach (var item in listasTrafosIndisponibles)
                {
                    cargoTotalNoPercibido += item.montoXHora * item.hsIndisponibles;
                }

                var calculoParcial = totalCargoRealxHora - cargoTotalNoPercibido;
                var mesPtosC = db.IndisponibilidadRemuneracionesMeses.Where(x => x.fecha.Value.Month == mes && x.fecha.Value.Year == anio && x.idEquipo == 2).FirstOrDefault();
                decimal? parcialPtosC = 0;
                if (mesPtosC != null)
                {
                    parcialPtosC = mesPtosC.calculoParcial;

                }

                var mesCalculado = db.IndisponibilidadRemuneracionesMeses.Where(x => x.fecha.Value.Month == mes && x.fecha.Value.Year == anio && x.idEquipo == 1).FirstOrDefault();
                if (mesCalculado != null)
                {
                    mesCalculado.calculoParcial = calculoParcial;
                    mesCalculado.caculoTotal = calculoParcial + parcialPtosC;
                    mesCalculado.diferencia = mesCalculado.calculoCammesa - mesCalculado.caculoTotal;
                    db.SaveChanges();
                }
                else
                {

                    IndisponibilidadRemuneracionesMeses nuevo = new IndisponibilidadRemuneracionesMeses()
                    {
                        idEquipo = 1,
                        nombreRemuneracion = "POR TRANSFORMACION",
                        calculoParcial = calculoParcial,
                        caculoTotal = calculoParcial + parcialPtosC,
                        calculoCammesa = 0,
                        diferencia = 0,
                        fecha = mesCambio,
                        activo = true,
                    };
                    db.IndisponibilidadRemuneracionesMeses.Add(nuevo);
                    db.SaveChanges();
                }

                if (mesPtosC != null)
                {
                    mesPtosC.caculoTotal = calculoParcial + parcialPtosC;
                    db.SaveChanges();

                }
                else
                {
                    calculoMesesCapacitores(mesCambio);
                }

            }
        }
        //Calculo para los meses PtosC
        public void calculoMesesPtosC(DateTime? mesCambio)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var listaPtosCRemuneradas = db.IndisponibilidadRemuPtosConexion.Where(x => x.activo == true && x.Mes == mesCambio.Value.Month && x.Anio == mesCambio.Value.Year).ToList();
                decimal? totalCargoRealxHora = 0;
                var parcial = 0;
                //foreach (var item in listaPtosCRemuneradas)
                //{
                //    totalCargoRealxHora += item.cargoRealxHora;
                //}
                totalCargoRealxHora = Convert.ToDecimal(listaPtosCRemuneradas.Where(x => x.activo == true).Sum(x => x.cargoRealxHora));

                var mes = Convert.ToDateTime(mesCambio).Month;
                var anio = Convert.ToDateTime(mesCambio).Year;

                DateTime firstDayOfMonth = new DateTime(anio, mes, 1);

                // Obtener el primer día del siguiente mes para calcular el último día del mes actual
                DateTime firstDayOfNextMonth = firstDayOfMonth.AddMonths(1);

                // Calcular el número de días en el mes actual
                int daysInMonth = (firstDayOfNextMonth - firstDayOfMonth).Days;

                // Calcular la cantidad de horas en el mes
                int hoursInMonth = daysInMonth * 24;

                totalCargoRealxHora = totalCargoRealxHora * hoursInMonth;

                decimal? cargoTotalNoPercibido = 0;
                var listasPtosCIndisponibles = db.IndisponibilidadPtosConexion.Where(x => x.activo == true && ((x.fechaSalida.Value.Month == mes && x.fechaSalida.Value.Year == anio) || (x.fechaEntrada.Value.Month == mes && x.fechaEntrada.Value.Year == anio))).ToList();
                foreach (var item in listasPtosCIndisponibles)
                {
                    cargoTotalNoPercibido += item.montoXhora * item.hsIndisponible;
                }

                var calculoParcial = totalCargoRealxHora - cargoTotalNoPercibido;

                var mesTrafo = db.IndisponibilidadRemuneracionesMeses.Where(x => x.fecha.Value.Month == mes && x.fecha.Value.Year == anio && x.idEquipo == 1).FirstOrDefault();
                decimal? parcialTrafos = 0;
                if (mesTrafo != null)
                {
                    parcialTrafos = mesTrafo.calculoParcial;
                }

                var mesCalculado = db.IndisponibilidadRemuneracionesMeses.Where(x => x.fecha.Value.Month == mes && x.fecha.Value.Year == anio && x.idEquipo == 2).FirstOrDefault();
                if (mesCalculado != null)
                {
                    mesCalculado.calculoParcial = calculoParcial;
                    mesCalculado.caculoTotal = calculoParcial + parcialTrafos;
                    mesCalculado.diferencia = mesCalculado.calculoCammesa - mesCalculado.caculoTotal;
                    db.SaveChanges();
                }
                else
                {
                    IndisponibilidadRemuneracionesMeses nuevo = new IndisponibilidadRemuneracionesMeses()
                    {
                        idEquipo = 2,
                        nombreRemuneracion = "POR CONEXION",
                        calculoParcial = calculoParcial,
                        caculoTotal = calculoParcial + parcialTrafos,
                        calculoCammesa = 0,
                        diferencia = 0,
                        fecha = mesCambio,
                        activo = true,
                    };
                    db.IndisponibilidadRemuneracionesMeses.Add(nuevo);
                    db.SaveChanges();
                }
                if (mesTrafo != null)
                {
                    mesTrafo.caculoTotal = calculoParcial + parcialTrafos;
                    db.SaveChanges();

                }
                else
                {
                    calculoMesesTrafo(mesCambio);
                }
            }
        }
        //Calculo para los meses Capacitores
        public void calculoMesesCapacitores(DateTime? mesCambio)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var listasCapacitorRemuneradas = db.IndisponibilidadRemuCapacitor.Where(x => x.activo == true && x.Mes == mesCambio.Value.Month && x.Anio == mesCambio.Value.Year).ToList();
                decimal? totalCargoRealxHora = 0;
                var parcial = 0;
                //foreach (var item in listasCapacitorRemuneradas)
                //{
                //    totalCargoRealxHora += item.cargoRealxHoras;
                //}
                totalCargoRealxHora = Convert.ToDecimal(listasCapacitorRemuneradas.Where(x => x.activo == true).Sum(x => x.cargoRealxHoras));

                var mes = Convert.ToDateTime(mesCambio).Month;
                var anio = Convert.ToDateTime(mesCambio).Year;


                DateTime firstDayOfMonth = new DateTime(anio, mes, 1);

                // Obtener el primer día del siguiente mes para calcular el último día del mes actual
                DateTime firstDayOfNextMonth = firstDayOfMonth.AddMonths(1);

                // Calcular el número de días en el mes actual
                int daysInMonth = (firstDayOfNextMonth - firstDayOfMonth).Days;

                // Calcular la cantidad de horas en el mes
                int hoursInMonth = daysInMonth * 24;

                totalCargoRealxHora = totalCargoRealxHora * hoursInMonth;

                decimal? cargoTotalNoPercibido = 0;
                var listasCapacitorIndisponibles = db.IndisponibilidadCapacitor.Where(x => x.activo == true && ((x.fechaSalida.Value.Month == mes && x.fechaSalida.Value.Year == anio) || (x.fechaEntrada.Value.Month == mes && x.fechaEntrada.Value.Year == anio))).ToList();
                foreach (var item in listasCapacitorIndisponibles)
                {
                    cargoTotalNoPercibido += item.montoXHora * item.hsIndisponible;
                }

                var calculoParcial = totalCargoRealxHora - cargoTotalNoPercibido;

                var mesCalculado = db.IndisponibilidadRemuneracionesMeses.Where(x => x.fecha.Value.Month == mes && x.fecha.Value.Year == anio && x.idEquipo == 7).FirstOrDefault();
                if (mesCalculado != null)
                {
                    mesCalculado.calculoParcial = calculoParcial;
                    mesCalculado.caculoTotal = calculoParcial;
                    mesCalculado.diferencia = mesCalculado.calculoCammesa - mesCalculado.caculoTotal;
                    db.SaveChanges();
                }
                else
                {
                    IndisponibilidadRemuneracionesMeses nuevo = new IndisponibilidadRemuneracionesMeses()
                    {
                        idEquipo = 7,
                        nombreRemuneracion = "POR REACTIVO",
                        calculoParcial = calculoParcial,
                        caculoTotal = calculoParcial,
                        calculoCammesa = 0,
                        diferencia = 0,
                        fecha = mesCambio,
                        activo = true,
                    };
                    db.IndisponibilidadRemuneracionesMeses.Add(nuevo);
                    db.SaveChanges();
                }
            }
        }

        public int ModificarMontoCammesa(MontoIndisponibilidadesVM MesCambio)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var mesEncontrado = db.IndisponibilidadRemuneracionesMeses.Where(x => x.idRemuneracionMes == MesCambio.idRemuneracionMes && x.idEquipo == MesCambio.idEquipoMes).FirstOrDefault();
                mesEncontrado.calculoCammesa = MesCambio.monto;
                db.SaveChanges();
                if (mesEncontrado != null)
                {


                    switch (MesCambio.idEquipoMes)
                    {
                        case 4:
                            calculoMesesLinea(mesEncontrado.fecha);
                            break;
                        case 1:
                            calculoMesesTrafo(mesEncontrado.fecha);
                            break;
                        case 2:
                            calculoMesesPtosC(mesEncontrado.fecha);
                            break;
                        case 7:
                            calculoMesesCapacitores(mesEncontrado.fecha);
                            break;
                    }
                    //var mesEncontradoActualizado = db.IndisponibilidadRemuneracionesMeses.Where(x => x.idRemuneracionMes == MesCambio.idRemuneracionMes && x.idEquipo == MesCambio.idEquipoMes).FirstOrDefault();

                    //mesEncontradoActualizado.calculoCammesa = MesCambio.monto;
                    //mesEncontradoActualizado.diferencia = MesCambio.monto - mesEncontrado.caculoTotal;


                    //db.SaveChanges();
                    return 200;
                }
                else
                {
                    return 400;
                }

            }
        }

        public HojaExcelVM GenerarReporteExcelMeses(string fecha)
        {

            var fechaF = Convert.ToDateTime(fecha);
            var mes = fechaF.Month;
            var ano = fechaF.Year;
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    ServicioEquipo servEquipo = new ServicioEquipo();
                    List<MontoIndisponibilidadesVM> meses = new List<MontoIndisponibilidadesVM>();
                    meses = (from i in db.IndisponibilidadRemuneracionesMeses
                             where i.activo == true && i.fecha.Value.Month == mes && i.fecha.Value.Year == ano
                             select new { i }).AsEnumerable().Select(x => new MontoIndisponibilidadesVM
                             {
                                 idRemuneracionMes = x.i.idRemuneracionMes,
                                 idEquipoMes = x.i.idEquipo,
                                 calculoParcial = x.i.calculoParcial,
                                 calculoTotal = x.i.caculoTotal,
                                 calculoCammesa = x.i.calculoCammesa,
                                 diferencia = x.i.diferencia,
                                 fecha = x.i.fecha,
                                 activo = x.i.activo,
                                 nombreRemuneracion = x.i.nombreRemuneracion,
                             }).OrderByDescending(x => x.fecha).ToList();


                    GenerarExcel excel = new GenerarExcel();
                    HojaExcelVM hojaDetalle = new HojaExcelVM();

                    var col1 = "CARGO";
                    var col2 = "CALCULO DISTRO";
                    var col3 = "PARCIAL";
                    var col4 = "TOTAL";
                    var col5 = "CAMMESA";
                    var col6 = "DIFERENCIA";

                    hojaDetalle.encabezado = new List<string>();
                    hojaDetalle.encabezado.Add(col1);
                    hojaDetalle.encabezado.Add(col2);
                    hojaDetalle.encabezado.Add(col3);
                    hojaDetalle.encabezado.Add(col4);
                    hojaDetalle.encabezado.Add(col5);
                    hojaDetalle.encabezado.Add(col6);

                    hojaDetalle.listaFilas = new List<DetalleExcelVM>();

                    foreach (var item in meses)
                    {

                        DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                        Calendar cal = dfi.Calendar;
                        hojaDetalle.unaFila = new DetalleExcelVM();
                        hojaDetalle.unaFila.datoPorFila = new List<string>();
                        hojaDetalle.unaFila.datoPorFila.Add(item.nombreRemuneracion);
                        hojaDetalle.unaFila.datoPorFila.Add(item.calculoParcial.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.calculoTotal.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.calculoCammesa.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.diferencia.ToString());
                        hojaDetalle.listaFilas.Add(hojaDetalle.unaFila);
                    }

                    string mesStrin = fechaF.ToString("MMMM");

                    var datoExcel = excel.GenerarExcelMesesRemunerados(hojaDetalle, mesStrin, ano);

                    return datoExcel;

                }
            }
            catch (Exception e)
            {
                var datoExcel = new HojaExcelVM();
                return datoExcel;
            }

        }


        #endregion


        // General excel Gnerico
        public HojaExcelVM GenerarExcelGenerico(string fechaDesde, string fechaHasta, string equipo)
        {
            var datoExcel = new HojaExcelVM();
            switch (equipo)
            {
                case "Linea":
                    datoExcel = GenerarReporteExcelLinea(fechaDesde, fechaHasta);
                    break;

                case "Transformador":
                    datoExcel = GenerarReporteExcelTrafo(fechaDesde, fechaHasta);
                    break;
                case "PtoConexion":
                    datoExcel = GenerarReporteExcelPtosC(fechaDesde, fechaHasta);
                    break;
                case "Capacitor":
                    datoExcel = GenerarReporteExcelCapacitores(fechaDesde, fechaHasta);
                    break;
            }
            return datoExcel;
        }


        public HojaExcelVM GenerarExcelCompleto(string fechaDesde, string fechaHasta)
        {
            var datoExcel = new HojaExcelVM();
            var fechaDesdeDate = Convert.ToDateTime(fechaDesde);
            var fechaHastaDate = Convert.ToDateTime(fechaHasta);
            fechaHastaDate = fechaHastaDate.AddDays(1);
            GenerarExcel excel = new GenerarExcel();
            HojaExcelVM hojaDetalle = new HojaExcelVM();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                ServicioEquipo servEquipo = new ServicioEquipo();

                //Equipos Remunrados
                #region Lineas
                List<IndisRemuEquipoVM> listaLinea = new List<IndisRemuEquipoVM>();
                listaLinea = (from r in db.IndisponibilidadRemuLineas                               
                              join l in db.LinLinea on r.idLinea equals l.idLinea 
                              into remu
                              from l in remu.DefaultIfEmpty()
                              join g in db.GralTension on l.tension equals g.idTension
                              where l.activo == true
                                                   && l.remunerado == true
                                                   && ( r.Anio == fechaDesdeDate.Year && r.Mes == fechaDesdeDate.Month) 
                                                   
                              select new IndisRemuEquipoVM
                              {
                                  idlinea = l.idLinea,
                                  idLineaIndisRemu = r.idLineaIndisRemu,
                                  idPagotran = l.idPagotran,
                                  cammesa = l.nombreLinea,
                                  Ukv = g.tension,
                                  longKm = l.kilometraje,
                                  remunerado = r.remunerado,
                                  cargoRealxHora = r.cargoRealxHoras,
                                  muestroRealxHora = r.cargoRealxHoras != null ? "$ " + r.cargoRealxHoras : " ",
                                  muestroxMes = r.xMes != null ? "$ " + r.xMes : " ",
                                  xMes = r.xMes,
                                  origenRegulatorio = r.origenRegulatorio,
                                  activo = r.activo,
                                  idTipoEquipo = 4,
                              }).ToList();

                hojaDetalle.totalesLineaRemu = new DetalleExcelVM();
                decimal? totalCargoRealLinea = 0;
                decimal? totalLineaKm = 0;

                var col1LR = "ID PAGOTRAN";
                var col2LR = "CAMMESA";
                var col3LR = "U[KV]";
                var col4LR = "Long [km]";
                var col5LR = "REMUNERADO";
                var col6LR = "CARGO REAL ($/hs)";
                var col7LR = "$/mes";
                var col8LR = "Origen Regulatorio";

                hojaDetalle.encabezadoLineaRemu = new List<string>();

                hojaDetalle.encabezadoLineaRemu.Add(col1LR);
                hojaDetalle.encabezadoLineaRemu.Add(col2LR);
                hojaDetalle.encabezadoLineaRemu.Add(col3LR);
                hojaDetalle.encabezadoLineaRemu.Add(col4LR);
                hojaDetalle.encabezadoLineaRemu.Add(col5LR);
                hojaDetalle.encabezadoLineaRemu.Add(col6LR);
                hojaDetalle.encabezadoLineaRemu.Add(col7LR);
                hojaDetalle.encabezadoLineaRemu.Add(col8LR);

                hojaDetalle.listaFilasLineaRemu = new List<DetalleExcelVM>();

                foreach (var item in listaLinea)
                {
                    hojaDetalle.unaFilaLineaRemu = new DetalleExcelVM();
                    hojaDetalle.unaFilaLineaRemu.datoPorFilaLineaRemu = new List<string>();
                    hojaDetalle.unaFilaLineaRemu.datoPorFilaLineaRemu.Add(item.idPagotran.ToString());
                    hojaDetalle.unaFilaLineaRemu.datoPorFilaLineaRemu.Add(item.cammesa);
                    hojaDetalle.unaFilaLineaRemu.datoPorFilaLineaRemu.Add(item.Ukv.ToString());
                    hojaDetalle.unaFilaLineaRemu.datoPorFilaLineaRemu.Add(item.longKm.ToString());
                    hojaDetalle.unaFilaLineaRemu.datoPorFilaLineaRemu.Add(item.remunerado.ToString());
                    hojaDetalle.unaFilaLineaRemu.datoPorFilaLineaRemu.Add(item.cargoRealxHora.ToString());
                    hojaDetalle.unaFilaLineaRemu.datoPorFilaLineaRemu.Add(item.xMes.ToString());
                    hojaDetalle.unaFilaLineaRemu.datoPorFilaLineaRemu.Add(item.origenRegulatorio.ToString());
                    hojaDetalle.listaFilasLineaRemu.Add(hojaDetalle.unaFilaLineaRemu);

                    totalCargoRealLinea += item.xMes;
                    totalLineaKm += item.longKm;
                }

                //De aca hago los totales 
                hojaDetalle.totalesLineaRemu.datoPorFilaTotalesLineaRemu = new List<string>();
                hojaDetalle.totalesLineaRemu.datoPorFilaTotalesLineaRemu.Add(totalCargoRealLinea.ToString());
                hojaDetalle.totalesLineaRemu.datoPorFilaTotalesLineaRemu.Add(totalLineaKm.ToString());
                hojaDetalle.listaFilasLineaRemu.Add(hojaDetalle.totalesLineaRemu);



                var montosLineasR = db.IndisponibilidadMontoLineas.Where(x => x.activoHistorial == true &&
                                                                        ((fechaDesdeDate >= x.fechaMonto && fechaDesdeDate <= x.fechaHasta) ||
                                                                         (fechaHastaDate >= x.fechaMonto && fechaHastaDate <= x.fechaHasta) ||
                                                                         (x.fechaMonto >= fechaDesdeDate && x.fechaMonto <= fechaHastaDate) ||
                                                                         (x.fechaHasta >= fechaDesdeDate && x.fechaHasta <= fechaHastaDate)))
                                                                         .GroupBy(x => x.idIndisponibilidadMontos)  // Agrupar por idIndisponibilidadMontos
                                                                         .Select(g => g.FirstOrDefault())  // Seleccionar el primer elemento de cada grupo
                                                                          .ToList();
                foreach (var item in montosLineasR)
                {
                    hojaDetalle.unaFilaMontoLineaRemu = new DetalleExcelVM();
                    hojaDetalle.unaFilaMontoLineaRemu.datoPorFilaMontoLineaRemu = new List<string>();
                    hojaDetalle.unaFilaMontoLineaRemu.datoPorFilaMontoLineaRemu.Add(" $/100km-h: Líneas " + item.kv.ToString() + " kv");
                    hojaDetalle.unaFilaMontoLineaRemu.datoPorFilaMontoLineaRemu.Add("$ " + item.monto.ToString());
                    hojaDetalle.unaFilaMontoLineaRemu.datoPorFilaMontoLineaRemu.Add(item.resolucion != "" ? item.resolucion : " - ");
                    hojaDetalle.listaFilasLineaRemu.Add(hojaDetalle.unaFilaMontoLineaRemu);
                }

                #endregion

                #region Trafos
                List<IndisRemuEquipoVM> listaTrafo;
                listaTrafo = (from  r in db.IndisponibilidadRemuTrafo
                              join t in db.TranTransformador on r.idTransformador equals t.idTransformador
                              into remu
                              from t in remu.DefaultIfEmpty()
                              join e in db.EstEstacion on t.idEstacion equals e.idEstacion
                              //esto puede llegar a traer trafos sin id que se hayan borrado de la bd de remuneraciones pero que sigan estando como remunerados en otra tabla
                              where t.activo == true && t.remunerado == true
                                                   && (r.Anio == fechaDesdeDate.Year && r.Mes == fechaDesdeDate.Month)
                              select new IndisRemuEquipoVM
                              {
                                  idTrafo = t.idTransformador,
                                  idIndisponibilidadesTrafo = r.idIndisponibilidadesTrafo,
                                  idPagotran = t.idPagotran,
                                  remunerado = r.remunerado,
                                  cargoRealxHora = r.cargoRealxHoras,
                                  xMes = r.xMes,
                                  origenRegulatorio = r.origenRegulatorio,
                                  activo = r.activo,
                                  muestroRealxHora = r.cargoRealxHoras != null ? "$ " + r.cargoRealxHoras : " ",
                                  muestroxMes = r.xMes != null ? "$ " + r.xMes : " ",


                                  nombreTransformador = t.nombreTransformador,
                                  potenciaAparente = t.potenciaAparente,
                                  potencias = t.Potencia1 + "/" + t.Potencia2 + "/" + t.Potencia3, //Pot [MVA] <- del excel
                                  tensiones = t.Tension1 + "/" + t.Tension2 + "/" + t.Tension3, // U[Kv] <- del excel
                                  estacion = new EstacionVM
                                  {
                                      idEstacion = e.idEstacion,
                                      nombreEstacion = e.nombreEstacion,
                                      codigoEstacion = e.codigoEstacion,
                                      activo = e.activo,
                                  }
                              }).ToList();

                hojaDetalle.totalesTrafosRemu = new DetalleExcelVM();
                decimal? totalCargoRealTrafo = 0;
                decimal? totalPot = 0;

                var col1TR = "ID PAGOTRAN";
                var col2TR = "ESTACIÓN TRANSFORMADORA";
                var col3TR = "EQUIPO";
                var col4TR = "POT.[MVA]";
                var col5TR = "U[kV]";
                var col6TR = "POTENCIAS";
                var col7TR = "REMUNERADO";
                var col8TR = "$/H";
                var col9TR = "X/MES";

                hojaDetalle.encabezadoTrafosRemu = new List<string>();

                hojaDetalle.encabezadoTrafosRemu.Add(col1TR);
                hojaDetalle.encabezadoTrafosRemu.Add(col2TR);
                hojaDetalle.encabezadoTrafosRemu.Add(col3TR);
                hojaDetalle.encabezadoTrafosRemu.Add(col4TR);
                hojaDetalle.encabezadoTrafosRemu.Add(col5TR);
                hojaDetalle.encabezadoTrafosRemu.Add(col6TR);
                hojaDetalle.encabezadoTrafosRemu.Add(col7TR);
                hojaDetalle.encabezadoTrafosRemu.Add(col8TR);
                hojaDetalle.encabezadoTrafosRemu.Add(col9TR);

                hojaDetalle.listaFilasTrafosRemu = new List<DetalleExcelVM>();

                foreach (var item in listaTrafo)
                {
                    hojaDetalle.unaFilaTrafosRemu = new DetalleExcelVM();
                    hojaDetalle.unaFilaTrafosRemu.datoPorFilaTrafoRemu = new List<string>();
                    hojaDetalle.unaFilaTrafosRemu.datoPorFilaTrafoRemu.Add(item.idPagotran.ToString());
                    hojaDetalle.unaFilaTrafosRemu.datoPorFilaTrafoRemu.Add(item.estacion.nombreEstacion);
                    hojaDetalle.unaFilaTrafosRemu.datoPorFilaTrafoRemu.Add(item.nombreTransformador);
                    hojaDetalle.unaFilaTrafosRemu.datoPorFilaTrafoRemu.Add(item.potenciaAparente.ToString());
                    hojaDetalle.unaFilaTrafosRemu.datoPorFilaTrafoRemu.Add(item.tensiones.ToString());
                    hojaDetalle.unaFilaTrafosRemu.datoPorFilaTrafoRemu.Add(item.potencias.ToString());
                    hojaDetalle.unaFilaTrafosRemu.datoPorFilaTrafoRemu.Add(item.remunerado.ToString());
                    hojaDetalle.unaFilaTrafosRemu.datoPorFilaTrafoRemu.Add(item.cargoRealxHora.ToString());
                    hojaDetalle.unaFilaTrafosRemu.datoPorFilaTrafoRemu.Add(item.xMes.ToString());
                    hojaDetalle.listaFilasTrafosRemu.Add(hojaDetalle.unaFilaTrafosRemu);

                    totalCargoRealTrafo += item.xMes;
                    totalPot += item.potenciaAparente;
                }

                //De aca hago los totales 
                hojaDetalle.totalesTrafosRemu.datoPorFilaTotalesTrafoRemu = new List<string>();
                hojaDetalle.totalesTrafosRemu.datoPorFilaTotalesTrafoRemu.Add(totalCargoRealTrafo.ToString());
                hojaDetalle.totalesTrafosRemu.datoPorFilaTotalesTrafoRemu.Add(totalPot.ToString());
                hojaDetalle.listaFilasTrafosRemu.Add(hojaDetalle.totalesTrafosRemu);



                var montosTrafosR = db.IndisponibilidadMontoTrafo.Where(x => x.activoHistorial == true &&
                                                                        ((fechaDesdeDate >= x.fechaMonto && fechaDesdeDate <= x.fechaHasta) ||
                                                                         (fechaHastaDate >= x.fechaMonto && fechaHastaDate <= x.fechaHasta) ||
                                                                         (x.fechaMonto >= fechaDesdeDate && x.fechaMonto <= fechaHastaDate) ||
                                                                         (x.fechaHasta >= fechaDesdeDate && x.fechaHasta <= fechaHastaDate)))
                                                                         .GroupBy(x => x.idIndisponibilidadMontosTrafo)  // Agrupar por idIndisponibilidadMontos
                                                                         .Select(g => g.FirstOrDefault())  // Seleccionar el primer elemento de cada grupo
                                                                          .ToList();
                foreach (var item in montosTrafosR)
                {
                    hojaDetalle.unaFilaMontoTrafosRemu = new DetalleExcelVM();
                    hojaDetalle.unaFilaMontoTrafosRemu.datoPorFilaMontoTrafoRemu = new List<string>();
                    hojaDetalle.unaFilaMontoTrafosRemu.datoPorFilaMontoTrafoRemu.Add(item.nombreMonto);
                    hojaDetalle.unaFilaMontoTrafosRemu.datoPorFilaMontoTrafoRemu.Add("$ " + item.monto.ToString());
                    hojaDetalle.unaFilaMontoTrafosRemu.datoPorFilaMontoTrafoRemu.Add(item.resolucion != "" ? item.resolucion : " - ");
                    hojaDetalle.listaFilasTrafosRemu.Add(hojaDetalle.unaFilaMontoTrafosRemu);
                }

                #endregion

                #region PtosC
                List<IndisRemuEquipoVM> listaPuntoConexion;
                listaPuntoConexion = (from r in db.IndisponibilidadRemuPtosConexion
                                      
                                      join c in db.PCPuntoConexion on r.idPuntoConexion equals c.idPuntoConexion into remu
                                      from c in remu.DefaultIfEmpty()
                                      join d in db.EstEstacion on c.idEstacion equals d.idEstacion
                                      join e in db.GralTension on c.idTension equals e.idTension
                                      where c.activo == true && c.remunerado == true
                                                   && (r.Anio == fechaDesdeDate.Year && r.Mes == fechaDesdeDate.Month)
                                      select new IndisRemuEquipoVM
                                      {
                                          idPtoRemunerado = r.idIndisponibilidadesPtosC,
                                          idPuntoConexion = c.idPuntoConexion,
                                          nombrePuntoConexion = c.nombrePuntoConexion,
                                          activo = c.activo,
                                          idPagotran = c.idPagotran,
                                          cargoRealxHora = r.cargoRealxHora,
                                          xMes = r.xMes,
                                          idIndisponibilidadesPtosC = r.idIndisponibilidadesPtosC,
                                          muestroRealxHora = "$ " + r.cargoRealxHora,
                                          muestroxMes = "$ " + r.xMes,
                                          tension = new TensionVM
                                          {
                                              idTension = e.idTension,
                                              tension = e.tension,
                                              activo = e.activo
                                          },
                                          estacion = new EstacionVM
                                          {
                                              idEstacion = d.idEstacion,
                                              codigoEstacion = d.codigoEstacion,
                                              nombreEstacion = d.nombreEstacion,
                                              activo = d.activo,
                                          },
                                      }).ToList();

                hojaDetalle.totalesPtosCRemu = new DetalleExcelVM();
                decimal? totalCargoRealPtosC = 0;
                decimal? totalEquipos = 0;
                decimal? totalMes = 0;

                var col0PR = "";
                var col1PR = "ID PAGOTRAN";
                var col2PR = "ESTACION";
                var col3PR = "EQUIPO";
                var col4PR = "U[KV]";
                var col5PR = "CARGO REAL ($/hs)";
                var col6PR = "$/mes";

                hojaDetalle.encabezadoPtosCRemu = new List<string>();

                hojaDetalle.encabezadoPtosCRemu.Add(col0PR);
                hojaDetalle.encabezadoPtosCRemu.Add(col1PR);
                hojaDetalle.encabezadoPtosCRemu.Add(col2PR);
                hojaDetalle.encabezadoPtosCRemu.Add(col3PR);
                hojaDetalle.encabezadoPtosCRemu.Add(col4PR);
                hojaDetalle.encabezadoPtosCRemu.Add(col5PR);
                hojaDetalle.encabezadoPtosCRemu.Add(col6PR);

                hojaDetalle.listaFilasPtosCRemu = new List<DetalleExcelVM>();
                var countPto = 1;
                foreach (var item in listaPuntoConexion)
                {
                    hojaDetalle.unaFilaMontoPtosCRemu = new DetalleExcelVM();
                    hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaPtoCRemu = new List<string>();
                    hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaPtoCRemu.Add(countPto.ToString());
                    hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaPtoCRemu.Add(item.idPagotran.ToString());
                    hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaPtoCRemu.Add(item.estacion.nombreEstacion);
                    hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaPtoCRemu.Add(item.nombrePuntoConexion);
                    hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaPtoCRemu.Add(item.tension.tension);
                    hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaPtoCRemu.Add(item.cargoRealxHora.ToString());
                    hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaPtoCRemu.Add(item.xMes.ToString());
                    hojaDetalle.listaFilasPtosCRemu.Add(hojaDetalle.unaFilaMontoPtosCRemu);

                    totalCargoRealPtosC += item.cargoRealxHora;
                    totalEquipos = countPto;
                    totalMes += item.xMes;
                    countPto++;
                }

                //De aca hago los totales 
                hojaDetalle.totalesPtosCRemu.datoPorFilaTotalesPtoCRemu = new List<string>();
                hojaDetalle.totalesPtosCRemu.datoPorFilaTotalesPtoCRemu.Add(totalCargoRealPtosC.ToString());
                hojaDetalle.totalesPtosCRemu.datoPorFilaTotalesPtoCRemu.Add("Total: " + totalEquipos.ToString());
                hojaDetalle.totalesPtosCRemu.datoPorFilaTotalesPtoCRemu.Add(totalMes.ToString());
                hojaDetalle.listaFilasPtosCRemu.Add(hojaDetalle.totalesPtosCRemu);

                var montosPtosCR = db.IndisponibilidadMontoPtosConexion.Where(x => x.activoHistorial == true &&
                                                                        ((fechaDesdeDate >= x.fechaMonto && fechaDesdeDate <= x.fechaHasta) ||
                                                                         (fechaHastaDate >= x.fechaMonto && fechaHastaDate <= x.fechaHasta) ||
                                                                         (x.fechaMonto >= fechaDesdeDate && x.fechaMonto <= fechaHastaDate) ||
                                                                         (x.fechaHasta >= fechaDesdeDate && x.fechaHasta <= fechaHastaDate)))
                                                                         .GroupBy(x => x.idIndisponibilidadMontosPtosC)  // Agrupar por idIndisponibilidadMontos
                                                                         .Select(g => g.FirstOrDefault())  // Seleccionar el primer elemento de cada grupo
                                                                          .ToList();
                foreach (var item in montosPtosCR)
                {
                    hojaDetalle.unaFilaMontoPtosCRemu = new DetalleExcelVM();
                    hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaMontoPtoCRemu = new List<string>();
                    hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaMontoPtoCRemu.Add("Salida en: " + item.kv.ToString());
                    hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaMontoPtoCRemu.Add("$ " + item.monto.ToString());
                    hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaMontoPtoCRemu.Add(item.resolucion != "" ? item.resolucion : " - ");
                    hojaDetalle.listaFilasPtosCRemu.Add(hojaDetalle.unaFilaMontoPtosCRemu);
                }


                #endregion

                #region Capacitor
                List<IndisRemuEquipoVM> listaCap;
                listaCap = (from r in db.IndisponibilidadRemuCapacitor
                            join c in db.CapCapacitor on r.idCapacitor equals c.idCapacitor into remu
                            from c in remu.DefaultIfEmpty()
                            where c.activo == true && c.remunerado == true
                                                   && (r.Anio == fechaDesdeDate.Year && r.Mes == fechaDesdeDate.Month)
                            select new IndisRemuEquipoVM
                            {
                                idIndisponibilidadCap = r.idIndisponibilidadesCapacitor,
                                idCapacitor = c.idCapacitor,
                                idPagotran = c.idPagotran,
                                nombreCapacitor = c.nombreCapacitor,
                                TensionKv = c.TensionKV,
                                potenciaMvra = c.potenciaMVAR,
                                cargoRealxHora = r.cargoRealxHoras,
                                xMes = r.xMes,
                                muestroRealxHora = "$ " + r.cargoRealxHoras,
                                muestroxMes = "$ " + r.xMes,
                            }).ToList();

                decimal? totalCargoRealCap = 0;

                var col1CR = "ID PAGOTRAN";
                var col2CR = "ESTACION";
                var col3CR = "EQUIPO";
                var col4CR = "U[KV]";
                var col5CR = "CARGO REAL ($/hs)";
                var col6CR = "$/mes";

                hojaDetalle.encabezadoCapRemu = new List<string>();

                hojaDetalle.encabezadoCapRemu.Add(col1CR);
                hojaDetalle.encabezadoCapRemu.Add(col2CR);
                hojaDetalle.encabezadoCapRemu.Add(col3CR);
                hojaDetalle.encabezadoCapRemu.Add(col4CR);
                hojaDetalle.encabezadoCapRemu.Add(col5CR);
                hojaDetalle.encabezadoCapRemu.Add(col6CR);

                hojaDetalle.listaFilasCapCRemu = new List<DetalleExcelVM>();

                foreach (var item in listaCap)
                {
                    hojaDetalle.unaFilaMontoCapRemu = new DetalleExcelVM();
                    hojaDetalle.unaFilaMontoCapRemu.datoPorFilaCapRemu = new List<string>();
                    hojaDetalle.unaFilaMontoCapRemu.datoPorFilaCapRemu.Add(item.idPagotran.ToString());
                    hojaDetalle.unaFilaMontoCapRemu.datoPorFilaCapRemu.Add(item.nombreCapacitor);
                    hojaDetalle.unaFilaMontoCapRemu.datoPorFilaCapRemu.Add(item.TensionKv.ToString());
                    hojaDetalle.unaFilaMontoCapRemu.datoPorFilaCapRemu.Add(item.potenciaMvra.ToString());
                    hojaDetalle.unaFilaMontoCapRemu.datoPorFilaCapRemu.Add(item.cargoRealxHora.ToString());
                    hojaDetalle.unaFilaMontoCapRemu.datoPorFilaCapRemu.Add(item.xMes.ToString());
                    hojaDetalle.listaFilasCapCRemu.Add(hojaDetalle.unaFilaMontoCapRemu);

                    totalCargoRealCap += item.xMes;
                }

                //De aca hago los totales 
                hojaDetalle.totalesCapRemu = new DetalleExcelVM();
                hojaDetalle.totalesCapRemu.datoPorFilaTotalesCapRemu = new List<string>();
                hojaDetalle.totalesCapRemu.datoPorFilaTotalesCapRemu.Add(totalCargoRealCap.ToString());
                hojaDetalle.listaFilasCapCRemu.Add(hojaDetalle.totalesCapRemu);

                var montosCapacitorR = db.IndisponibilidadMontoCapacitor.Where(x => x.activoHistorial == true &&
                                                                        ((fechaDesdeDate >= x.fechaMonto && fechaDesdeDate <= x.fechaHasta) ||
                                                                         (fechaHastaDate >= x.fechaMonto && fechaHastaDate <= x.fechaHasta) ||
                                                                         (x.fechaMonto >= fechaDesdeDate && x.fechaMonto <= fechaHastaDate) ||
                                                                         (x.fechaHasta >= fechaDesdeDate && x.fechaHasta <= fechaHastaDate)))
                                                                         .GroupBy(x => x.idIndisponibilidadMontosCapacitor)  // Agrupar por idIndisponibilidadMontos
                                                                         .Select(g => g.FirstOrDefault())  // Seleccionar el primer elemento de cada grupo
                                                                          .ToList();
                foreach (var item in montosCapacitorR)
                {
                    hojaDetalle.unaFilaMontoCapRemu = new DetalleExcelVM();
                    hojaDetalle.unaFilaMontoCapRemu.datoPorFilaMontoCapRemu = new List<string>();
                    hojaDetalle.unaFilaMontoCapRemu.datoPorFilaMontoCapRemu.Add(item.nombreMonto);
                    hojaDetalle.unaFilaMontoCapRemu.datoPorFilaMontoCapRemu.Add("$ " + item.monto.ToString());
                    hojaDetalle.unaFilaMontoCapRemu.datoPorFilaMontoCapRemu.Add(item.resolucion != "" ? item.resolucion : " - ");
                    hojaDetalle.listaFilasCapCRemu.Add(hojaDetalle.unaFilaMontoCapRemu);
                }

                #endregion

                //Equipos Indisponibles
                #region Lineas
                List<IndisRemuEquipoVM> lineas = new List<IndisRemuEquipoVM>();
                lineas = (from i in db.IndisponibilidadLineas
                          join r in db.IndisponibilidadRemuLineas on i.IdLineaIndisRemu equals r.idLineaIndisRemu
                          join e in db.EquiEquipo on i.idLinea equals e.idEquipo into ee
                          from e in ee.DefaultIfEmpty()
                          join l in db.LinLinea on i.idLinea equals l.idLinea
                          join m in db.IndisponibilidadMontoLineas on i.idMonto equals m.idIndisponibilidadMontos
                          join t in db.GralTension on l.tension equals t.idTension
                          where i.activo == true && fechaDesdeDate <= i.fechaSalida && fechaHastaDate >= i.fechaEntrada
                          select new { i, e, r, l, m, t }).AsEnumerable().Select(x => new IndisRemuEquipoVM
                          {
                              idLineaRemunerada = x.i.IdLineaRemunerada,
                              idLineaIndisRemu = x.i.IdLineaIndisRemu,
                              idlinea = x.l.idLinea,
                              idMonto = x.m.idIndisponibilidadMontos,
                              montoXhora = x.i.montoXhora,
                              fechaSalida = x.i.fechaSalida,
                              fechaEntrada = x.i.fechaEntrada,
                              hsIndisponible = x.i.hsIndisponible,
                              minutosIndisponible = x.i.minutosIndisponible,
                              tipoSalida = x.i.tipoSalida,
                              activo = x.i.activo,
                              hsProgramada = x.i.hsProgramada,
                              hsForzada = x.i.hsProgramada,
                              penalizacionProgramada = x.i.penalizacionProgramada,
                              informoEnTermino = x.i.informoEnTermino,
                              totalPenalizacion = x.i.totalPenalizacion,
                              cargoReal = x.i.cargoReal,
                              noPercibido = x.i.noPercibido,
                              lucroCesante = x.i.lucroCesante,
                              motivo = x.i.motivo,
                              Ukv = x.t.tension,
                              longKm = x.l.kilometraje,
                              nombreLinea = x.l.nombreLinea,
                              idPagotran = x.l.idPagotran,
                              PenalizacionForzada1 = x.i.PenalizacionForzada1,
                              PenalizacionForzada2 = x.i.PenalizacionForzada2,
                              PenalizacionForzada3 = x.i.PenalizacionForzada3,
                              montoIndis = new MontoIndisponibilidadesVM
                              {
                                  monto = x.m.monto,
                                  nombreMonto = x.m.kv.ToString(),
                                  resolucion = x.m.resolucion,
                              }
                          }).OrderByDescending(x => x.idLineaRemunerada).ToList();


                hojaDetalle.totalesLineas = new DetalleExcelVM();

                decimal? totalPenalizacion = 0;
                decimal? totalNoPercibo = 0;
                decimal? totalLucroCesante = 0;

                var col1 = "Mes";
                var col2 = "Equipo";
                var col3 = "ID";
                var col4 = "Linea";
                var col5 = "U[kV]";
                var col6 = "Long.[Km]";
                var col7 = "$/h";
                var col8 = "Salida";
                var col9 = "Entrada";
                var col10 = "Hs.Indisp.";
                var col11 = "Minutos Indis.";
                var col12 = "Tipo Salida";
                //var col13 = "Hs Prog";
                //var col14 = "Hs Forz";
                var col15 = "Penalizacion Programada";
                var col16 = "Penalizacion Forzada1";
                var col17 = "Penalizacion Forzada2";
                var col18 = "Penalizacion Forzada3";
                var col19 = "Informo en termino";
                var col20 = "Total Penalización";
                //var col18 = "Cargo Real";
                var col21 = "No Percibido";
                var col22 = "Lucro Cesante";
                var col23 = "Motivo";
                var col24 = "";
                var col25 = "Tarifa";
                var col26 = "Resolucion";

                hojaDetalle.encabezadoLinea = new List<string>();

                hojaDetalle.encabezadoLinea.Add(col1);
                hojaDetalle.encabezadoLinea.Add(col2);
                hojaDetalle.encabezadoLinea.Add(col3);
                hojaDetalle.encabezadoLinea.Add(col4);
                hojaDetalle.encabezadoLinea.Add(col5);
                hojaDetalle.encabezadoLinea.Add(col6);
                hojaDetalle.encabezadoLinea.Add(col7);
                hojaDetalle.encabezadoLinea.Add(col8);
                hojaDetalle.encabezadoLinea.Add(col9);
                hojaDetalle.encabezadoLinea.Add(col10);
                hojaDetalle.encabezadoLinea.Add(col11);
                hojaDetalle.encabezadoLinea.Add(col12);
                hojaDetalle.encabezadoLinea.Add(col15);
                hojaDetalle.encabezadoLinea.Add(col16);
                hojaDetalle.encabezadoLinea.Add(col17);
                hojaDetalle.encabezadoLinea.Add(col18);
                hojaDetalle.encabezadoLinea.Add(col19);
                hojaDetalle.encabezadoLinea.Add(col20);
                hojaDetalle.encabezadoLinea.Add(col21);
                hojaDetalle.encabezadoLinea.Add(col22);
                hojaDetalle.encabezadoLinea.Add(col23);
                hojaDetalle.encabezadoLinea.Add(col24);
                hojaDetalle.encabezadoLinea.Add(col25);
                hojaDetalle.encabezadoLinea.Add(col26);
                //if (lst.Count > 0)
                //{
                hojaDetalle.listaFilasLinea = new List<DetalleExcelVM>();

                foreach (var item in lineas)
                {

                    DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                    Calendar cal = dfi.Calendar;
                    DateTime fechaActual = Convert.ToDateTime(item.fechaSalida);
                    string mesActual = fechaActual.ToString("MMMM");
                    hojaDetalle.unaFilaLinea = new DetalleExcelVM();
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea = new List<string>();
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(mesActual.ToUpperInvariant());
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add("Linea");
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.idPagotran.ToString());
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.nombreLinea);
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.Ukv);
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.longKm.ToString());
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.montoXhora.ToString());
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.fechaSalida.ToString());
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.fechaEntrada.ToString());
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.hsIndisponible.ToString());
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.minutosIndisponible.ToString());
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.tipoSalida);
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.penalizacionProgramada.ToString());

                    //
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.PenalizacionForzada1.ToString());
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.PenalizacionForzada2.ToString());
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.PenalizacionForzada3.ToString());
                    //

                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.informoEnTermino == true ? "SI" : "NO");
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.totalPenalizacion.ToString());
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.noPercibido.ToString());
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.lucroCesante.ToString());
                    //18
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.motivo);
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(" ");
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.montoIndis.monto.ToString());
                    hojaDetalle.unaFilaLinea.datoPorFilaLinea.Add(item.montoIndis.resolucion);
                    hojaDetalle.listaFilasLinea.Add(hojaDetalle.unaFilaLinea);

                    totalPenalizacion += item.totalPenalizacion;
                    totalNoPercibo += item.noPercibido;
                    totalLucroCesante += item.lucroCesante;
                }

                //De aca hago los totales 
                hojaDetalle.totalesLineas.datoPorFilaTotalesLineas = new List<string>();
                hojaDetalle.totalesLineas.datoPorFilaTotalesLineas.Add(totalPenalizacion.ToString());
                hojaDetalle.totalesLineas.datoPorFilaTotalesLineas.Add(totalNoPercibo.ToString());
                hojaDetalle.totalesLineas.datoPorFilaTotalesLineas.Add(totalLucroCesante.ToString());
                hojaDetalle.listaFilasLinea.Add(hojaDetalle.totalesLineas);



                //var montosLineas = db.IndisponibilidadMontoLineas.Where(x => x.activoHistorial == true && x.fechaMonto >= fechaDesdeDate && x.fechaMonto <= fechaHastaDate).ToList();
                foreach (var item in montosLineasR)
                {
                    hojaDetalle.unaFilaMontoLinea = new DetalleExcelVM();
                    hojaDetalle.unaFilaMontoLinea.datoPorFilaMontoLinea = new List<string>();
                    hojaDetalle.unaFilaMontoLinea.datoPorFilaMontoLinea.Add("Kv: " + item.kv.ToString());
                    hojaDetalle.unaFilaMontoLinea.datoPorFilaMontoLinea.Add("$ " + item.monto.ToString());
                    hojaDetalle.unaFilaMontoLinea.datoPorFilaMontoLinea.Add(item.resolucion != "" ? item.resolucion : " - ");
                    hojaDetalle.listaFilasLinea.Add(hojaDetalle.unaFilaMontoLinea);

                }
                //}

                #endregion

                #region Transformador
                List<IndisRemuEquipoVM> trafos = new List<IndisRemuEquipoVM>();
                trafos = (from i in db.IndisponibilidadTrafos
                          join r in db.IndisponibilidadRemuTrafo on i.idTrafoRemunerado equals r.idIndisponibilidadesTrafo
                          join t in db.TranTransformador on i.idTrafo equals t.idTransformador
                          join m in db.IndisponibilidadMontoTrafo on i.idMonto equals m.idIndisponibilidadMontosTrafo
                          where i.activo == true && fechaDesdeDate <= i.fechaSalida && fechaHastaDate >= i.fechaEntrada
                          select new { i, r, m, t }).AsEnumerable().Select(x => new IndisRemuEquipoVM
                          {
                              idIndisponibilidadesTrafo = x.i.idIndisponibilidadTrafo,
                              idTrafosRemu = x.i.idTrafoRemunerado,
                              idTrafo = x.t.idTransformador,
                              idMonto = x.m.idIndisponibilidadMontosTrafo,
                              montoXhora = x.i.montoXHora,
                              fechaSalida = x.i.fechaSalida,
                              fechaEntrada = x.i.fechaEntrada,
                              hsIndisponible = x.i.hsIndisponibles,
                              minutosIndisponible = x.i.minutosIndisponibles,
                              tipoSalida = x.i.tipoSalida,
                              activo = x.i.activo,
                              CR = x.i.CR,
                              ENS = x.i.ENS,
                              informoEnTermino = x.i.informoEnTermino,
                              totalPenalizacion = x.i.totalPenalizado,
                              noPercibido = x.i.noPercibido,
                              motivo = x.i.motivo,
                              //Trafo
                              idPagotran = x.t.idPagotran,
                              nombreTransformador = x.t.nombreTransformador,
                              potenciaAparente = x.t.potenciaAparente,
                              potencias = x.t.Potencia1 + "/" + x.t.Potencia2 + "/" + x.t.Potencia3, //Pot [MVA] <- del excel
                              tensiones = x.t.Tension1 + "/" + x.t.Tension2 + "/" + x.t.Tension3, // U[Kv] <- del excel
                                                                                                  //Monto
                              montoIndis = new MontoIndisponibilidadesVM
                              {
                                  monto = x.m.monto,
                                  nombreMonto = x.m.nombreMonto,
                                  resolucion = x.m.resolucion,
                              }
                          }).OrderByDescending(x => x.fechaSalida).ToList();


                hojaDetalle.totalesTrafo = new DetalleExcelVM();

                decimal? totalPenalizacionTrafo = 0;
                decimal? totalNoPerciboTrafo = 0;
                decimal? totalLucroCesanteTrafo = 0;

                var col1T = "Mes";
                var col2T = "Equipo";
                var col3T = "ID";
                var col4T = "Transformador";
                var col5T = "POT[MVA]";
                var col6T = "U.[kV]";
                var col7T = "Potencias";
                var col8T = "$/h";
                var col9T = "Salida";
                var col10T = "Entrada";
                var col11T = "Hs.Indisp.";
                var col12T = "Minutos Indis.";
                var col13T = "Tipo Salida";
                //var col13 = "Hs Prog";
                var col14T = "CR%";
                var col15T = "E.N.S.";
                var col16T = "Informo en termino";
                var col17T = "Total Penalización";
                //var col18 = "Cargo Real";
                var col19T = "No Percibido";
                var col20T = "Lucro Cesante";
                var col21T = "Motivo";
                var col22T = "";
                var col23T = "Tarifa";
                var col24T = "Resolucion";

                hojaDetalle.encabezadoTrafo = new List<string>();

                hojaDetalle.encabezadoTrafo.Add(col1T);
                hojaDetalle.encabezadoTrafo.Add(col2T);
                hojaDetalle.encabezadoTrafo.Add(col3T);
                hojaDetalle.encabezadoTrafo.Add(col4T);
                hojaDetalle.encabezadoTrafo.Add(col5T);
                hojaDetalle.encabezadoTrafo.Add(col6T);
                hojaDetalle.encabezadoTrafo.Add(col7T);
                hojaDetalle.encabezadoTrafo.Add(col8T);
                hojaDetalle.encabezadoTrafo.Add(col9T);
                hojaDetalle.encabezadoTrafo.Add(col10T);
                hojaDetalle.encabezadoTrafo.Add(col11T);
                hojaDetalle.encabezadoTrafo.Add(col12T);
                hojaDetalle.encabezadoTrafo.Add(col13T);
                hojaDetalle.encabezadoTrafo.Add(col14T);
                hojaDetalle.encabezadoTrafo.Add(col15T);
                hojaDetalle.encabezadoTrafo.Add(col16T);
                hojaDetalle.encabezadoTrafo.Add(col17T);
                //hojaDetalle.encabezado.Add(col18)T;
                hojaDetalle.encabezadoTrafo.Add(col19T);
                hojaDetalle.encabezadoTrafo.Add(col20T);
                hojaDetalle.encabezadoTrafo.Add(col21T);
                hojaDetalle.encabezadoTrafo.Add(col22T);
                hojaDetalle.encabezadoTrafo.Add(col23T);
                hojaDetalle.encabezadoTrafo.Add(col24T);

                hojaDetalle.listaFilasTrafo = new List<DetalleExcelVM>();


                foreach (var item in trafos)
                {

                    DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                    Calendar cal = dfi.Calendar;
                    DateTime fechaActual = Convert.ToDateTime(item.fechaSalida);
                    string mesActual = fechaActual.ToString("MMMM");
                    hojaDetalle.unaFilaTrafo = new DetalleExcelVM();
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo = new List<string>();
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(mesActual.ToUpperInvariant());
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add("Transformador");
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(item.idPagotran.ToString());
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(item.nombreTransformador);
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(item.potenciaAparente.ToString());
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(item.tensiones.ToString());
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(item.potencias.ToString());
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(item.montoXhora.ToString());
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(item.fechaSalida.ToString());
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(item.fechaEntrada.ToString());
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(item.hsIndisponible.ToString());
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(item.minutosIndisponible.ToString());
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(item.tipoSalida);
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(item.CR.ToString());
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(item.ENS == true ? "SI" : "NO");
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(item.informoEnTermino == true ? "SI" : "NO");
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(item.totalPenalizacion.ToString());
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(item.noPercibido.ToString());
                    var lucroCesanteX = item.noPercibido + item.totalPenalizacion;
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(lucroCesanteX.ToString());
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(item.motivo);
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(" ");
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(item.montoIndis.monto.ToString());
                    hojaDetalle.unaFilaTrafo.datoPorFilaTrafo.Add(item.montoIndis.resolucion);
                    hojaDetalle.listaFilasTrafo.Add(hojaDetalle.unaFilaTrafo);

                    totalPenalizacionTrafo += item.totalPenalizacion;
                    totalNoPerciboTrafo += item.noPercibido;
                    totalLucroCesanteTrafo += lucroCesanteX;
                }

                //De aca hago los totales 
                hojaDetalle.totalesTrafo.datoPorFilaTotalesTrafo = new List<string>();
                hojaDetalle.totalesTrafo.datoPorFilaTotalesTrafo.Add(totalPenalizacionTrafo.ToString());
                hojaDetalle.totalesTrafo.datoPorFilaTotalesTrafo.Add(totalPenalizacionTrafo.ToString());
                hojaDetalle.totalesTrafo.datoPorFilaTotalesTrafo.Add(totalPenalizacionTrafo.ToString());
                hojaDetalle.listaFilasTrafo.Add(hojaDetalle.totalesTrafo);


                //var montosTrafos = db.IndisponibilidadMontoTrafo.Where(x => x.activoHistorial == true && x.fechaMonto >= fechaDesdeDate && x.fechaMonto <= fechaHastaDate).ToList();
                foreach (var item in montosTrafosR)
                {
                    hojaDetalle.unaFilaMontoTrafo = new DetalleExcelVM();
                    hojaDetalle.unaFilaMontoTrafo.datoPorFilaMontoTrafo = new List<string>();
                    hojaDetalle.unaFilaMontoTrafo.datoPorFilaMontoTrafo.Add(item.nombreMonto);
                    hojaDetalle.unaFilaMontoTrafo.datoPorFilaMontoTrafo.Add("$ " + item.monto.ToString());
                    hojaDetalle.unaFilaMontoTrafo.datoPorFilaMontoTrafo.Add(item.resolucion != "" ? item.resolucion : " - ");
                    hojaDetalle.listaFilasTrafo.Add(hojaDetalle.unaFilaMontoTrafo);
                }

                #endregion

                #region PtosC
                List<IndisRemuEquipoVM> ptosC = new List<IndisRemuEquipoVM>();
                ptosC = (from i in db.IndisponibilidadPtosConexion
                         join r in db.IndisponibilidadRemuPtosConexion on i.idPtoIndisRemu equals r.idIndisponibilidadesPtosC
                         join p in db.PCPuntoConexion on i.idPtoConexion equals p.idPuntoConexion
                         join m in db.IndisponibilidadMontoPtosConexion on i.idMonto equals m.idIndisponibilidadMontosPtosC
                         join t in db.GralTension on p.idTension equals t.idTension
                         where i.activo == true && fechaDesdeDate <= i.fechaSalida && fechaHastaDate >= i.fechaEntrada
                         select new { i, r, m, p, t }).AsEnumerable().Select(x => new IndisRemuEquipoVM
                         {
                             idIndisponibilidadesPtosC = x.i.IdPtoRemunerado,
                             idPtoIndisRemu = x.i.idPtoIndisRemu,
                             idPuntoConexion = x.p.idPuntoConexion,
                             idMonto = x.m.idIndisponibilidadMontosPtosC,
                             montoXhora = x.i.montoXhora,
                             fechaSalida = x.i.fechaSalida,
                             fechaEntrada = x.i.fechaEntrada,
                             hsIndisponible = x.i.hsIndisponible,
                             minutosIndisponible = x.i.minutosIndisponible,
                             tipoSalida = x.i.tipoSalida,
                             activo = x.i.activo,
                             informoEnTermino = x.i.informoEnTermino,
                             totalPenalizacion = x.i.totalPenalizacion,
                             noPercibido = x.i.noPercibido,
                             motivo = x.i.motivo,
                             penalizacionProgramada = x.i.penalizacionProgramada,
                             cargoReal = x.i.cargoReal,
                             lucroCesante = x.i.lucroCesante,

                             //PtoC
                             idPagotran = x.p.idPagotran,
                             nombrePuntoConexion = x.p.nombrePuntoConexion,
                             Ukv = x.t.tension,
                             //Monto
                             montoIndis = new MontoIndisponibilidadesVM
                             {
                                 monto = x.m.monto,
                                 kvPtosC = x.m.kv,
                                 resolucion = x.m.resolucion,
                             }
                         }).OrderByDescending(x => x.fechaSalida).ToList();


                hojaDetalle.totalesPtoC = new DetalleExcelVM();

                decimal? totalPenalizacionP = 0;
                decimal? totalNoPerciboP = 0;
                decimal? totalLucroCesanteP = 0;

                var col1P = "Mes";
                var col2P = "Equipo";
                var col3P = "ID";
                var col4P = "Punto de Conexion";
                var col5P = "U.[kV]";
                var col6P = "$/h";
                var col7P = "Salida";
                var col8P = "Entrada";
                //var col9 = "Tipo Salida";
                // var col10 = "Entrada";
                var col11P = "Hs.Indisp.";
                var col12P = "Minutos Indis.";
                var col13P = "Tipo Salida";
                //var col13 = "Hs Prog";
                // var col14 = "CR%";
                //var col15 = "E.N.S.";
                var col16P = "Informo en termino";
                var col17P = "Total Penalización";
                var col18P = "Cargo Real";
                var col19P = "No Percibido";
                var col20P = "Lucro Cesante";
                var col21P = "Motivo";
                var col22P = "";
                var col23P = "Tarifa";
                var col24P = "Resolucion";

                hojaDetalle.encabezadoPtoC = new List<string>();

                hojaDetalle.encabezadoPtoC.Add(col1P);
                hojaDetalle.encabezadoPtoC.Add(col2P);
                hojaDetalle.encabezadoPtoC.Add(col3P);
                hojaDetalle.encabezadoPtoC.Add(col4P);
                hojaDetalle.encabezadoPtoC.Add(col5P);
                hojaDetalle.encabezadoPtoC.Add(col6P);
                hojaDetalle.encabezadoPtoC.Add(col7P);
                hojaDetalle.encabezadoPtoC.Add(col8P);
                // hojaDetalle.encabezado.Add(col9);
                // hojaDetalle.encabezado.Add(col10);
                hojaDetalle.encabezadoPtoC.Add(col11P);
                hojaDetalle.encabezadoPtoC.Add(col12P);
                hojaDetalle.encabezadoPtoC.Add(col13P);
                // hojaDetalle.encabezado.Add(col14);
                //hojaDetalle.encabezado.Add(col15);
                hojaDetalle.encabezadoPtoC.Add(col16P);
                hojaDetalle.encabezadoPtoC.Add(col17P);
                hojaDetalle.encabezadoPtoC.Add(col18P);
                hojaDetalle.encabezadoPtoC.Add(col19P);
                hojaDetalle.encabezadoPtoC.Add(col20P);
                hojaDetalle.encabezadoPtoC.Add(col21P);
                hojaDetalle.encabezadoPtoC.Add(col22P);
                hojaDetalle.encabezadoPtoC.Add(col23P);
                hojaDetalle.encabezadoPtoC.Add(col24P);

                hojaDetalle.listaFilasPtoC = new List<DetalleExcelVM>();


                foreach (var item in ptosC)
                {

                    DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                    Calendar cal = dfi.Calendar;
                    DateTime fechaActual = Convert.ToDateTime(item.fechaSalida);
                    string mesActual = fechaActual.ToString("MMMM");
                    hojaDetalle.unaFilaPtoC = new DetalleExcelVM();
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC = new List<string>();
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC.Add(mesActual.ToUpperInvariant());
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC.Add("Punto de Conexion");
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC.Add(item.idPagotran.ToString());
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC.Add(item.nombrePuntoConexion);
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC.Add(item.Ukv.ToString());
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC.Add(item.montoXhora.ToString());
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC.Add(item.fechaSalida.ToString());
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC.Add(item.fechaEntrada.ToString());
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC.Add(item.hsIndisponible.ToString());
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC.Add(item.minutosIndisponible.ToString());
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC.Add(item.tipoSalida);
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC.Add(item.informoEnTermino == true ? "SI" : "NO");
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC.Add(item.totalPenalizacion.ToString());
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC.Add(item.cargoReal.ToString());
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC.Add(item.noPercibido.ToString());
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC.Add(item.lucroCesante.ToString());
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC.Add(item.motivo);
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC.Add(" ");
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC.Add(item.montoIndis.monto.ToString());
                    hojaDetalle.unaFilaPtoC.datoPorFilaPtosC.Add(item.montoIndis.resolucion);
                    hojaDetalle.listaFilasPtoC.Add(hojaDetalle.unaFilaPtoC);

                    totalPenalizacionP += item.totalPenalizacion;
                    totalNoPerciboP += item.noPercibido;
                    totalLucroCesanteP += item.lucroCesante;
                }

                //De aca hago los totales 
                hojaDetalle.totalesPtoC.datoPorFilaTotalesPtosC = new List<string>();
                hojaDetalle.totalesPtoC.datoPorFilaTotalesPtosC.Add(totalPenalizacionP.ToString());
                hojaDetalle.totalesPtoC.datoPorFilaTotalesPtosC.Add(totalNoPerciboP.ToString());
                hojaDetalle.totalesPtoC.datoPorFilaTotalesPtosC.Add(totalLucroCesanteP.ToString());
                hojaDetalle.listaFilasPtoC.Add(hojaDetalle.totalesPtoC);

                //var montosPtosC = db.IndisponibilidadMontoPtosConexion.Where(x => x.activoHistorial == true && x.fechaMonto >= fechaDesdeDate && x.fechaMonto <= fechaHastaDate).ToList();
                foreach (var item in montosPtosCR)
                {
                    hojaDetalle.unaFilaMontoPtoC = new DetalleExcelVM();
                    hojaDetalle.unaFilaMontoPtoC.datoPorFilaMontoPtosC = new List<string>();
                    hojaDetalle.unaFilaMontoPtoC.datoPorFilaMontoPtosC.Add(item.kv.ToString());
                    hojaDetalle.unaFilaMontoPtoC.datoPorFilaMontoPtosC.Add("$ " + item.monto.ToString());
                    hojaDetalle.unaFilaMontoPtoC.datoPorFilaMontoPtosC.Add(item.resolucion != "" ? item.resolucion : " - ");
                    hojaDetalle.listaFilasPtoC.Add(hojaDetalle.unaFilaMontoPtoC);
                }


                #endregion

                #region Capacitores
                List<IndisRemuEquipoVM> capacitores = new List<IndisRemuEquipoVM>();
                capacitores = (from i in db.IndisponibilidadCapacitor
                               join r in db.IndisponibilidadRemuCapacitor on i.idCapacitorRemunerado equals r.idIndisponibilidadesCapacitor
                               join c in db.CapCapacitor on i.idCapacitor equals c.idCapacitor
                               join m in db.IndisponibilidadMontoCapacitor on i.idMonto equals m.idIndisponibilidadMontosCapacitor
                               where i.activo == true && fechaDesdeDate <= i.fechaSalida && fechaHastaDate >= i.fechaEntrada
                               select new { i, r, m, c }).AsEnumerable().Select(x => new IndisRemuEquipoVM
                               {
                                   idIndisponibilidadCapacitor = x.i.idIndisponibilidadCapacitor,
                                   idCapacitorRemunerado = x.i.idCapacitorRemunerado,
                                   idCapacitor = x.c.idCapacitor,
                                   idMonto = x.m.idIndisponibilidadMontosCapacitor,
                                   montoXhora = x.i.montoXHora,
                                   fechaSalida = x.i.fechaSalida,
                                   fechaEntrada = x.i.fechaEntrada,
                                   hsIndisponible = x.i.hsIndisponible,
                                   minutosIndisponible = x.i.minutosIndisponibles,
                                   tipoSalida = x.i.tipoSalida,
                                   activo = x.i.activo,
                                   CR = x.i.CR,
                                   ENS = x.i.ENS,
                                   informoEnTermino = x.i.informoEnTermino,
                                   totalPenalizacion = x.i.totalPenalizado,
                                   noPercibido = x.i.noPercibido,
                                   motivo = x.i.motivo,
                                   lucroCesante = x.i.lucroCesante,
                                   //Capacitor
                                   idPagotran = x.c.idPagotran,
                                   nombreCapacitor = x.c.nombreCapacitor,
                                   TensionKv = x.c.TensionKV,
                                   potenciaMvra = x.c.potenciaMVAR == 0 ? 0 : x.c.potenciaMVAR,
                                   //Monto
                                   montoIndis = new MontoIndisponibilidadesVM
                                   {
                                       monto = x.m.monto,
                                       nombreMonto = x.m.nombreMonto,
                                       resolucion = x.m.resolucion,
                                   }
                               }).OrderByDescending(x => x.fechaSalida).ToList();

                hojaDetalle.totalesCapacitores = new DetalleExcelVM();

                decimal? totalPenalizacionC = 0;
                decimal? totalNoPerciboC = 0;
                decimal? totalLucroCesanteC = 0;

                var col1C = "Mes";
                var col2C = "Equipo";
                var col3C = "ID";
                var col4C = "Capacitor";
                var col5C = "Tension";
                var col6C = "POT.[MVA]";
                //var col7 = "Potencias";
                var col8C = "$/h";
                var col9C = "Salida";
                var col10C = "Entrada";
                var col11C = "Hs.Indisp.";
                var col12C = "Minutos Indis.";
                var col13C = "Tipo Salida";
                //var col13 = "Hs Prog";
                // var col14 = "CR%";
                // var col15 = "E.N.S.";
                var col16C = "Informo en termino";
                var col17C = "Total Penalización";
                //var col18 = "Cargo Real";
                var col19C = "No Percibido";
                var col20C = "Lucro Cesante";
                var col21C = "Motivo";
                var col22C = "";
                var col23C = "Tarifa";
                var col24C = "Resolucion";

                hojaDetalle.encabezadoCapacitor = new List<string>();

                hojaDetalle.encabezadoCapacitor.Add(col1C);
                hojaDetalle.encabezadoCapacitor.Add(col2C);
                hojaDetalle.encabezadoCapacitor.Add(col3C);
                hojaDetalle.encabezadoCapacitor.Add(col4C);
                hojaDetalle.encabezadoCapacitor.Add(col5C);
                hojaDetalle.encabezadoCapacitor.Add(col6C);
                //hojaDetalle.encabezadoCapacitor.Add(col7);
                hojaDetalle.encabezadoCapacitor.Add(col8C);
                hojaDetalle.encabezadoCapacitor.Add(col9C);
                hojaDetalle.encabezadoCapacitor.Add(col10C);
                hojaDetalle.encabezadoCapacitor.Add(col11C);
                hojaDetalle.encabezadoCapacitor.Add(col12C);
                hojaDetalle.encabezadoCapacitor.Add(col13C);
                //hojaDetalle.encabezadoCapacitor.Add(col14);
                //hojaDetalle.encabezadoCapacitor.Add(col15);
                hojaDetalle.encabezadoCapacitor.Add(col16C);
                hojaDetalle.encabezadoCapacitor.Add(col17C);
                //hojaDetalle.encabezadoCapacitor.Add(col18);
                hojaDetalle.encabezadoCapacitor.Add(col19C);
                hojaDetalle.encabezadoCapacitor.Add(col20C);
                hojaDetalle.encabezadoCapacitor.Add(col21C);
                hojaDetalle.encabezadoCapacitor.Add(col22C);
                hojaDetalle.encabezadoCapacitor.Add(col23C);
                hojaDetalle.encabezadoCapacitor.Add(col24C);

                hojaDetalle.listaFilasCapacitor = new List<DetalleExcelVM>();


                foreach (var item in capacitores)
                {

                    DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                    Calendar cal = dfi.Calendar;
                    DateTime fechaActual = Convert.ToDateTime(item.fechaSalida);
                    string mesActual = fechaActual.ToString("MMMM");
                    hojaDetalle.unaFilaCapacitor = new DetalleExcelVM();
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor = new List<string>();
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor.Add(mesActual.ToUpperInvariant());
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor.Add("Capacitores");
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor.Add(item.idPagotran.ToString());
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor.Add(item.nombreCapacitor);
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor.Add(item.TensionKv.ToString());
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor.Add(item.potenciaMvra.ToString());
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor.Add(item.montoXhora.ToString());
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor.Add(item.fechaSalida.ToString());
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor.Add(item.fechaEntrada.ToString());
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor.Add(item.hsIndisponible.ToString());
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor.Add(item.minutosIndisponible.ToString());
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor.Add(item.tipoSalida);
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor.Add(item.informoEnTermino == true ? "SI" : "NO");
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor.Add(item.totalPenalizacion.ToString());
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor.Add(item.noPercibido.ToString());
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor.Add(item.lucroCesante.ToString());
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor.Add(item.motivo);
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor.Add(" ");
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor.Add(item.montoIndis.monto.ToString());
                    hojaDetalle.unaFilaCapacitor.datoPorFilaCapacitor.Add(item.montoIndis.resolucion);
                    hojaDetalle.listaFilasCapacitor.Add(hojaDetalle.unaFilaCapacitor);

                    totalPenalizacionC += item.totalPenalizacion;
                    totalNoPerciboC += item.noPercibido;
                    totalLucroCesanteC += item.lucroCesante;
                }

                //De aca hago los totales 
                hojaDetalle.totalesCapacitores.datoPorFilaTotalesCapacitor = new List<string>();//totales ACA REVISAR
                hojaDetalle.totalesCapacitores.datoPorFilaTotalesCapacitor.Add(totalPenalizacionC.ToString());
                hojaDetalle.totalesCapacitores.datoPorFilaTotalesCapacitor.Add(totalNoPerciboC.ToString());
                hojaDetalle.totalesCapacitores.datoPorFilaTotalesCapacitor.Add(totalLucroCesanteC.ToString());
                hojaDetalle.listaFilasCapacitor.Add(hojaDetalle.totalesCapacitores);

                //var montosCapacitor = db.IndisponibilidadMontoCapacitor.Where(x => x.activoHistorial == true && x.fechaMonto >= fechaDesdeDate && x.fechaMonto <= fechaHastaDate).ToList();
                foreach (var item in montosCapacitorR)
                {
                    hojaDetalle.unaFilaMontoCapacitor = new DetalleExcelVM();
                    hojaDetalle.unaFilaMontoCapacitor.datoPorFilaMontoCapacitor = new List<string>();
                    hojaDetalle.unaFilaMontoCapacitor.datoPorFilaMontoCapacitor.Add(item.nombreMonto);
                    hojaDetalle.unaFilaMontoCapacitor.datoPorFilaMontoCapacitor.Add("$ " + item.monto.ToString());
                    hojaDetalle.unaFilaMontoCapacitor.datoPorFilaMontoCapacitor.Add(item.resolucion != "" ? item.resolucion : " - ");
                    hojaDetalle.listaFilasCapacitor.Add(hojaDetalle.unaFilaMontoCapacitor);
                }
                #endregion

                #region Mes

                var fechaF = fechaDesdeDate; //DateTime.Now; ;
                var mes = fechaF.Month;
                var ano = fechaF.Year;

                List<MontoIndisponibilidadesVM> meses = new List<MontoIndisponibilidadesVM>();
                meses = (from i in db.IndisponibilidadRemuneracionesMeses
                         where i.activo == true && i.fecha.Value.Month == mes && i.fecha.Value.Year == ano
                         select new { i }).AsEnumerable().Select(x => new MontoIndisponibilidadesVM
                         {
                             idRemuneracionMes = x.i.idRemuneracionMes,
                             idEquipoMes = x.i.idEquipo,
                             calculoParcial = x.i.calculoParcial,
                             calculoTotal = x.i.caculoTotal,
                             calculoCammesa = x.i.calculoCammesa,
                             diferencia = x.i.diferencia,
                             fecha = x.i.fecha,
                             activo = x.i.activo,
                             nombreRemuneracion = x.i.nombreRemuneracion,
                         }).OrderByDescending(x => x.fecha).ToList();


                var col1M = "CARGO";
                var col2M = "CALCULO DISTRO";
                var col3M = "PARCIAL";
                var col4M = "TOTAL";
                var col5M = "CAMMESA";
                var col6M = "DIFERENCIA";

                hojaDetalle.encabezadoMes = new List<string>();
                hojaDetalle.encabezadoMes.Add(col1M);
                hojaDetalle.encabezadoMes.Add(col2M);
                hojaDetalle.encabezadoMes.Add(col3M);
                hojaDetalle.encabezadoMes.Add(col4M);
                hojaDetalle.encabezadoMes.Add(col5M);
                hojaDetalle.encabezadoMes.Add(col6M);

                hojaDetalle.listaFilasMes = new List<DetalleExcelVM>();

                foreach (var item in meses)
                {

                    DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                    Calendar cal = dfi.Calendar;
                    hojaDetalle.unaFilaMes = new DetalleExcelVM();
                    hojaDetalle.unaFilaMes.datoPorFilaMes = new List<string>();
                    hojaDetalle.unaFilaMes.datoPorFilaMes.Add(item.nombreRemuneracion);
                    hojaDetalle.unaFilaMes.datoPorFilaMes.Add(item.calculoParcial.ToString());
                    hojaDetalle.unaFilaMes.datoPorFilaMes.Add(item.calculoTotal.ToString());
                    hojaDetalle.unaFilaMes.datoPorFilaMes.Add(item.calculoCammesa.ToString());
                    hojaDetalle.unaFilaMes.datoPorFilaMes.Add(item.diferencia.ToString());
                    hojaDetalle.listaFilasMes.Add(hojaDetalle.unaFilaMes);
                }
                #endregion
            }

            datoExcel = excel.GenerarExcelCompleto(hojaDetalle, fechaDesdeDate);

            return datoExcel;
        }


        public HojaExcelVM GenerarExcelEquiposRemu(int idResolucion, int idEquipo)
        {
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    GenerarExcel excel = new GenerarExcel();
                    HojaExcelVM hojaDetalle = new HojaExcelVM();
                    ServicioEquipo servEquipo = new ServicioEquipo();
                    if (idEquipo == 4)
                    {
                        #region Lineas
                        List<IndisRemuEquipoVM> listaLinea = new List<IndisRemuEquipoVM>();
                        listaLinea = (from l in db.LinLinea
                                      join g in db.GralTension on l.tension equals g.idTension
                                      join r in db.IndisponibilidadRemuLineas on l.idLinea equals r.idLinea into remu
                                      from r in remu.DefaultIfEmpty()
                                      where l.activo == true && l.remunerado == true && r.idResolucion == idResolucion
                                      select new IndisRemuEquipoVM
                                      {
                                          idlinea = l.idLinea,
                                          idLineaIndisRemu = r.idLineaIndisRemu,
                                          idPagotran = l.idPagotran,
                                          cammesa = l.nombreLinea,
                                          Ukv = g.tension,
                                          longKm = l.kilometraje,
                                          remunerado = r.remunerado,
                                          cargoRealxHora = r.cargoRealxHoras,
                                          muestroRealxHora = r.cargoRealxHoras != null ? "$ " + r.cargoRealxHoras : " ",
                                          muestroxMes = r.xMes != null ? "$ " + r.xMes : " ",
                                          xMes = r.xMes,
                                          origenRegulatorio = r.origenRegulatorio,
                                          activo = r.activo,
                                          idTipoEquipo = 4,
                                      }).ToList();

                        hojaDetalle.totalesLineaRemu = new DetalleExcelVM();
                        decimal? totalCargoRealLinea = 0;
                        decimal? totalLineaKm = 0;

                        var col1LR = "ID PAGOTRAN";
                        var col2LR = "CAMMESA";
                        var col3LR = "U[KV]";
                        var col4LR = "Long [km]";
                        var col5LR = "REMUNERADO";
                        var col6LR = "CARGO REAL ($/hs)";
                        var col7LR = "$/mes";
                        var col8LR = "Origen Regulatorio";

                        hojaDetalle.encabezadoLineaRemu = new List<string>();

                        hojaDetalle.encabezadoLineaRemu.Add(col1LR);
                        hojaDetalle.encabezadoLineaRemu.Add(col2LR);
                        hojaDetalle.encabezadoLineaRemu.Add(col3LR);
                        hojaDetalle.encabezadoLineaRemu.Add(col4LR);
                        hojaDetalle.encabezadoLineaRemu.Add(col5LR);
                        hojaDetalle.encabezadoLineaRemu.Add(col6LR);
                        hojaDetalle.encabezadoLineaRemu.Add(col7LR);
                        hojaDetalle.encabezadoLineaRemu.Add(col8LR);

                        hojaDetalle.listaFilasLineaRemu = new List<DetalleExcelVM>();

                        foreach (var item in listaLinea)
                        {
                            hojaDetalle.unaFilaLineaRemu = new DetalleExcelVM();
                            hojaDetalle.unaFilaLineaRemu.datoPorFilaLineaRemu = new List<string>();
                            hojaDetalle.unaFilaLineaRemu.datoPorFilaLineaRemu.Add(item.idPagotran.ToString());
                            hojaDetalle.unaFilaLineaRemu.datoPorFilaLineaRemu.Add(item.cammesa);
                            hojaDetalle.unaFilaLineaRemu.datoPorFilaLineaRemu.Add(item.Ukv.ToString());
                            hojaDetalle.unaFilaLineaRemu.datoPorFilaLineaRemu.Add(item.longKm.ToString());
                            hojaDetalle.unaFilaLineaRemu.datoPorFilaLineaRemu.Add(item.remunerado.ToString());
                            hojaDetalle.unaFilaLineaRemu.datoPorFilaLineaRemu.Add(item.cargoRealxHora.ToString());
                            hojaDetalle.unaFilaLineaRemu.datoPorFilaLineaRemu.Add(item.xMes.ToString());
                            hojaDetalle.unaFilaLineaRemu.datoPorFilaLineaRemu.Add(item.origenRegulatorio.ToString());
                            hojaDetalle.listaFilasLineaRemu.Add(hojaDetalle.unaFilaLineaRemu);

                            totalCargoRealLinea += item.cargoRealxHora;
                            totalLineaKm += item.longKm;
                        }

                        //De aca hago los totales 
                        hojaDetalle.totalesLineaRemu.datoPorFilaTotalesLineaRemu = new List<string>();
                        hojaDetalle.totalesLineaRemu.datoPorFilaTotalesLineaRemu.Add(totalCargoRealLinea.ToString());
                        hojaDetalle.totalesLineaRemu.datoPorFilaTotalesLineaRemu.Add(totalLineaKm.ToString());
                        hojaDetalle.listaFilasLineaRemu.Add(hojaDetalle.totalesLineaRemu);



                        var montosLineasR = db.IndisponibilidadMontoLineas.Where(x => x.activo == true).ToList();
                        foreach (var item in montosLineasR)
                        {
                            hojaDetalle.unaFilaMontoLineaRemu = new DetalleExcelVM();
                            hojaDetalle.unaFilaMontoLineaRemu.datoPorFilaMontoLineaRemu = new List<string>();
                            hojaDetalle.unaFilaMontoLineaRemu.datoPorFilaMontoLineaRemu.Add(" $/100km-h: Líneas " + item.kv.ToString() + " kv");
                            hojaDetalle.unaFilaMontoLineaRemu.datoPorFilaMontoLineaRemu.Add("$ " + item.monto.ToString());
                            hojaDetalle.unaFilaMontoLineaRemu.datoPorFilaMontoLineaRemu.Add(item.resolucion != "" ? item.resolucion : " - ");
                            hojaDetalle.listaFilasLineaRemu.Add(hojaDetalle.unaFilaMontoLineaRemu);
                        }

                        #endregion
                    }
                    if (idEquipo == 1)
                    {
                        #region Trafos
                        List<IndisRemuEquipoVM> listaTrafo;
                        listaTrafo = (from t in db.TranTransformador
                                      join e in db.EstEstacion on t.idEstacion equals e.idEstacion
                                      join r in db.IndisponibilidadRemuTrafo on t.idTransformador equals r.idTransformador into remu
                                      from r in remu.DefaultIfEmpty()
                                      where t.activo == true && t.remunerado == true && r.idResolucion == idResolucion
                                      select new IndisRemuEquipoVM
                                      {
                                          idTrafo = t.idTransformador,
                                          idIndisponibilidadesTrafo = r.idIndisponibilidadesTrafo,
                                          idPagotran = t.idPagotran,
                                          remunerado = r.remunerado,
                                          cargoRealxHora = r.cargoRealxHoras,
                                          xMes = r.xMes,
                                          origenRegulatorio = r.origenRegulatorio,
                                          activo = r.activo,
                                          muestroRealxHora = r.cargoRealxHoras != null ? "$ " + r.cargoRealxHoras : " ",
                                          muestroxMes = r.xMes != null ? "$ " + r.xMes : " ",


                                          nombreTransformador = t.nombreTransformador,
                                          potenciaAparente = t.potenciaAparente,
                                          potencias = t.Potencia1 + "/" + t.Potencia2 + "/" + t.Potencia3, //Pot [MVA] <- del excel
                                          tensiones = t.Tension1 + "/" + t.Tension2 + "/" + t.Tension3, // U[Kv] <- del excel
                                          estacion = new EstacionVM
                                          {
                                              idEstacion = e.idEstacion,
                                              nombreEstacion = e.nombreEstacion,
                                              codigoEstacion = e.codigoEstacion,
                                              activo = e.activo,
                                          }
                                      }).ToList();

                        hojaDetalle.totalesTrafosRemu = new DetalleExcelVM();
                        decimal? totalCargoRealTrafo = 0;
                        decimal? totalPot = 0;

                        var col1TR = "ID PAGOTRAN";
                        var col2TR = "ESTACIÓN TRANSFORMADORA";
                        var col3TR = "EQUIPO";
                        var col4TR = "POT.[MVA]";
                        var col5TR = "U[kV]";
                        var col6TR = "POTENCIAS";
                        var col7TR = "REMUNERADO";
                        var col8TR = "$/H";
                        var col9TR = "X/MES";

                        hojaDetalle.encabezadoTrafosRemu = new List<string>();

                        hojaDetalle.encabezadoTrafosRemu.Add(col1TR);
                        hojaDetalle.encabezadoTrafosRemu.Add(col2TR);
                        hojaDetalle.encabezadoTrafosRemu.Add(col3TR);
                        hojaDetalle.encabezadoTrafosRemu.Add(col4TR);
                        hojaDetalle.encabezadoTrafosRemu.Add(col5TR);
                        hojaDetalle.encabezadoTrafosRemu.Add(col6TR);
                        hojaDetalle.encabezadoTrafosRemu.Add(col7TR);
                        hojaDetalle.encabezadoTrafosRemu.Add(col8TR);
                        hojaDetalle.encabezadoTrafosRemu.Add(col9TR);

                        hojaDetalle.listaFilasTrafosRemu = new List<DetalleExcelVM>();

                        foreach (var item in listaTrafo)
                        {
                            hojaDetalle.unaFilaTrafosRemu = new DetalleExcelVM();
                            hojaDetalle.unaFilaTrafosRemu.datoPorFilaTrafoRemu = new List<string>();
                            hojaDetalle.unaFilaTrafosRemu.datoPorFilaTrafoRemu.Add(item.idPagotran.ToString());
                            hojaDetalle.unaFilaTrafosRemu.datoPorFilaTrafoRemu.Add(item.estacion.nombreEstacion);
                            hojaDetalle.unaFilaTrafosRemu.datoPorFilaTrafoRemu.Add(item.nombreTransformador);
                            hojaDetalle.unaFilaTrafosRemu.datoPorFilaTrafoRemu.Add(item.potenciaAparente.ToString());
                            hojaDetalle.unaFilaTrafosRemu.datoPorFilaTrafoRemu.Add(item.tensiones.ToString());
                            hojaDetalle.unaFilaTrafosRemu.datoPorFilaTrafoRemu.Add(item.potencias.ToString());
                            hojaDetalle.unaFilaTrafosRemu.datoPorFilaTrafoRemu.Add(item.remunerado.ToString());
                            hojaDetalle.unaFilaTrafosRemu.datoPorFilaTrafoRemu.Add(item.cargoRealxHora.ToString());
                            hojaDetalle.unaFilaTrafosRemu.datoPorFilaTrafoRemu.Add(item.xMes.ToString());
                            hojaDetalle.listaFilasTrafosRemu.Add(hojaDetalle.unaFilaTrafosRemu);

                            totalCargoRealTrafo += item.cargoRealxHora;
                            totalPot += item.potenciaAparente;
                        }

                        //De aca hago los totales 
                        hojaDetalle.totalesTrafosRemu.datoPorFilaTotalesTrafoRemu = new List<string>();
                        hojaDetalle.totalesTrafosRemu.datoPorFilaTotalesTrafoRemu.Add(totalCargoRealTrafo.ToString());
                        hojaDetalle.totalesTrafosRemu.datoPorFilaTotalesTrafoRemu.Add(totalPot.ToString());
                        hojaDetalle.listaFilasTrafosRemu.Add(hojaDetalle.totalesTrafosRemu);



                        var montosTrafosR = db.IndisponibilidadMontoTrafo.Where(x => x.activo == true).ToList();
                        foreach (var item in montosTrafosR)
                        {
                            hojaDetalle.unaFilaMontoTrafosRemu = new DetalleExcelVM();
                            hojaDetalle.unaFilaMontoTrafosRemu.datoPorFilaMontoTrafoRemu = new List<string>();
                            hojaDetalle.unaFilaMontoTrafosRemu.datoPorFilaMontoTrafoRemu.Add(item.nombreMonto);
                            hojaDetalle.unaFilaMontoTrafosRemu.datoPorFilaMontoTrafoRemu.Add("$ " + item.monto.ToString());
                            hojaDetalle.unaFilaMontoTrafosRemu.datoPorFilaMontoTrafoRemu.Add(item.resolucion != "" ? item.resolucion : " - ");
                            hojaDetalle.listaFilasTrafosRemu.Add(hojaDetalle.unaFilaMontoTrafosRemu);
                        }

                        #endregion
                    }

                    if (idEquipo == 2)
                    {
                        #region PtosC
                        List<IndisRemuEquipoVM> listaPuntoConexion;
                        listaPuntoConexion = (from c in db.PCPuntoConexion
                                              join d in db.EstEstacion on c.idEstacion equals d.idEstacion
                                              join e in db.GralTension on c.idTension equals e.idTension
                                              join r in db.IndisponibilidadRemuPtosConexion on c.idPuntoConexion equals r.idPuntoConexion into remu
                                              from r in remu.DefaultIfEmpty()
                                              where c.activo == true && c.remunerado == true && r.idResolucion == idResolucion
                                              select new IndisRemuEquipoVM
                                              {
                                                  idPtoRemunerado = r.idIndisponibilidadesPtosC,
                                                  idPuntoConexion = c.idPuntoConexion,
                                                  nombrePuntoConexion = c.nombrePuntoConexion,
                                                  activo = c.activo,
                                                  idPagotran = c.idPagotran,
                                                  cargoRealxHora = r.cargoRealxHora,
                                                  xMes = r.xMes,
                                                  idIndisponibilidadesPtosC = r.idIndisponibilidadesPtosC,
                                                  muestroRealxHora = "$ " + r.cargoRealxHora,
                                                  muestroxMes = "$ " + r.xMes,
                                                  tension = new TensionVM
                                                  {
                                                      idTension = e.idTension,
                                                      tension = e.tension,
                                                      activo = e.activo
                                                  },
                                                  estacion = new EstacionVM
                                                  {
                                                      idEstacion = d.idEstacion,
                                                      codigoEstacion = d.codigoEstacion,
                                                      nombreEstacion = d.nombreEstacion,
                                                      activo = d.activo,
                                                  },
                                              }).ToList();

                        hojaDetalle.totalesPtosCRemu = new DetalleExcelVM();
                        decimal? totalCargoRealPtosC = 0;
                        decimal? totalEquipos = 0;
                        decimal? totalMes = 0;
                        var col0PR = ""; //prueba conteo
                        var col1PR = "ID PAGOTRAN";
                        var col2PR = "ESTACION TRANSFORMADORA";
                        var col3PR = "EQUIPO";
                        var col4PR = "U[KV]";
                        var col5PR = "CARGO REAL ($/hs)";
                        var col6PR = "$/mes";

                        hojaDetalle.encabezadoPtosCRemu = new List<string>();

                        hojaDetalle.encabezadoPtosCRemu.Add(col0PR); //prueba conteo
                        hojaDetalle.encabezadoPtosCRemu.Add(col1PR);
                        hojaDetalle.encabezadoPtosCRemu.Add(col2PR);
                        hojaDetalle.encabezadoPtosCRemu.Add(col3PR);
                        hojaDetalle.encabezadoPtosCRemu.Add(col4PR);
                        hojaDetalle.encabezadoPtosCRemu.Add(col5PR);
                        hojaDetalle.encabezadoPtosCRemu.Add(col6PR);

                        hojaDetalle.listaFilasPtosCRemu = new List<DetalleExcelVM>();
                        var countPto = 1; //prueba conteo

                        foreach (var item in listaPuntoConexion)
                        {
                            hojaDetalle.unaFilaMontoPtosCRemu = new DetalleExcelVM();
                            hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaPtoCRemu = new List<string>();
                            hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaPtoCRemu.Add(countPto.ToString()); //agregado para probar conteo
                            hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaPtoCRemu.Add(item.idPagotran.ToString());
                            hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaPtoCRemu.Add(item.estacion.nombreEstacion);
                            hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaPtoCRemu.Add(item.nombrePuntoConexion);
                            hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaPtoCRemu.Add(item.tension.tension);
                            hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaPtoCRemu.Add(item.cargoRealxHora.ToString());
                            hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaPtoCRemu.Add(item.xMes.ToString());
                            hojaDetalle.listaFilasPtosCRemu.Add(hojaDetalle.unaFilaMontoPtosCRemu);

                            totalCargoRealPtosC += item.cargoRealxHora;
                            totalEquipos = countPto;
                            totalMes += item.xMes;
                            countPto++;
                        }

                        //De aca hago los totales 
                        hojaDetalle.totalesPtosCRemu.datoPorFilaTotalesPtoCRemu = new List<string>();
                        hojaDetalle.totalesPtosCRemu.datoPorFilaTotalesPtoCRemu.Add(totalCargoRealPtosC.ToString());
                        hojaDetalle.totalesPtosCRemu.datoPorFilaTotalesPtoCRemu.Add("Total: " + totalEquipos.ToString());
                        hojaDetalle.totalesPtosCRemu.datoPorFilaTotalesPtoCRemu.Add(totalMes.ToString());
                        hojaDetalle.listaFilasPtosCRemu.Add(hojaDetalle.totalesPtosCRemu);

                        var montosPtosCR = db.IndisponibilidadMontoPtosConexion.Where(x => x.activo == true).ToList();
                        foreach (var item in montosPtosCR)
                        {
                            hojaDetalle.unaFilaMontoPtosCRemu = new DetalleExcelVM();
                            hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaMontoPtoCRemu = new List<string>();
                            hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaMontoPtoCRemu.Add("Salida en: " + item.kv.ToString());
                            hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaMontoPtoCRemu.Add("$ " + item.monto.ToString());
                            hojaDetalle.unaFilaMontoPtosCRemu.datoPorFilaMontoPtoCRemu.Add(item.resolucion != "" ? item.resolucion : " - ");
                            hojaDetalle.listaFilasPtosCRemu.Add(hojaDetalle.unaFilaMontoPtosCRemu);
                        }


                        #endregion
                    }
                    if (idEquipo == 7)
                    {
                        #region Capacitor
                        List<IndisRemuEquipoVM> listaCap;
                        listaCap = (from c in db.CapCapacitor
                                    join r in db.IndisponibilidadRemuCapacitor on c.idCapacitor equals r.idCapacitor
                                    where c.activo == true && c.remunerado == true && r.idResolucion == idResolucion
                                    select new IndisRemuEquipoVM
                                    {
                                        idIndisponibilidadCap = r.idIndisponibilidadesCapacitor,
                                        idCapacitor = c.idCapacitor,
                                        idPagotran = c.idPagotran,
                                        nombreCapacitor = c.nombreCapacitor,
                                        TensionKv = c.TensionKV,
                                        potenciaMvra = c.potenciaMVAR,
                                        cargoRealxHora = r.cargoRealxHoras,
                                        xMes = r.xMes,
                                        muestroRealxHora = "$ " + r.cargoRealxHoras,
                                        muestroxMes = "$ " + r.xMes,
                                    }).ToList();

                        decimal? totalCargoRealCap = 0;

                        var col1CR = "ID PAGOTRAN";
                        var col2CR = "ESTACION TRANSFORMADORA";
                        var col3CR = "EQUIPO";
                        var col4CR = "U[KV]";
                        var col5CR = "CARGO REAL ($/hs)";
                        var col6CR = "$/mes";

                        hojaDetalle.encabezadoCapRemu = new List<string>();

                        hojaDetalle.encabezadoCapRemu.Add(col1CR);
                        hojaDetalle.encabezadoCapRemu.Add(col2CR);
                        hojaDetalle.encabezadoCapRemu.Add(col3CR);
                        hojaDetalle.encabezadoCapRemu.Add(col4CR);
                        hojaDetalle.encabezadoCapRemu.Add(col5CR);
                        hojaDetalle.encabezadoCapRemu.Add(col6CR);

                        hojaDetalle.listaFilasCapCRemu = new List<DetalleExcelVM>();

                        foreach (var item in listaCap)
                        {
                            hojaDetalle.unaFilaMontoCapRemu = new DetalleExcelVM();
                            hojaDetalle.unaFilaMontoCapRemu.datoPorFilaCapRemu = new List<string>();
                            hojaDetalle.unaFilaMontoCapRemu.datoPorFilaCapRemu.Add(item.idPagotran.ToString());
                            hojaDetalle.unaFilaMontoCapRemu.datoPorFilaCapRemu.Add(item.nombreCapacitor);
                            hojaDetalle.unaFilaMontoCapRemu.datoPorFilaCapRemu.Add(item.TensionKv.ToString());
                            hojaDetalle.unaFilaMontoCapRemu.datoPorFilaCapRemu.Add(item.potenciaMvra.ToString());
                            hojaDetalle.unaFilaMontoCapRemu.datoPorFilaCapRemu.Add(item.cargoRealxHora.ToString());
                            hojaDetalle.unaFilaMontoCapRemu.datoPorFilaCapRemu.Add(item.xMes.ToString());
                            hojaDetalle.listaFilasCapCRemu.Add(hojaDetalle.unaFilaMontoCapRemu);

                            totalCargoRealCap += item.cargoRealxHora;
                        }

                        //De aca hago los totales 
                        hojaDetalle.totalesCapRemu = new DetalleExcelVM();
                        hojaDetalle.totalesCapRemu.datoPorFilaTotalesCapRemu = new List<string>();
                        hojaDetalle.totalesCapRemu.datoPorFilaTotalesCapRemu.Add(totalCargoRealCap.ToString());
                        hojaDetalle.listaFilasCapCRemu.Add(hojaDetalle.totalesCapRemu);

                        var montosCapacitorR = db.IndisponibilidadMontoCapacitor.Where(x => x.activo == true).ToList();
                        foreach (var item in montosCapacitorR)
                        {
                            hojaDetalle.unaFilaMontoCapRemu = new DetalleExcelVM();
                            hojaDetalle.unaFilaMontoCapRemu.datoPorFilaMontoCapRemu = new List<string>();
                            hojaDetalle.unaFilaMontoCapRemu.datoPorFilaMontoCapRemu.Add(item.nombreMonto);
                            hojaDetalle.unaFilaMontoCapRemu.datoPorFilaMontoCapRemu.Add("$ " + item.monto.ToString());
                            hojaDetalle.unaFilaMontoCapRemu.datoPorFilaMontoCapRemu.Add(item.resolucion != "" ? item.resolucion : " - ");
                            hojaDetalle.listaFilasCapCRemu.Add(hojaDetalle.unaFilaMontoCapRemu);
                        }

                        #endregion
                    }


                    var datoExcel = excel.GenerarExcelEquiposRemunerados(hojaDetalle, idEquipo);

                    return datoExcel;

                }
            }
            catch (Exception e)
            {
                var datoExcel = new HojaExcelVM();
                return datoExcel;
            }

        }
        static int CalcularMeses(DateTime fechaInicio, DateTime fechaFin)
        {
            return ((fechaFin.Year - fechaInicio.Year) * 12) + fechaFin.Month - fechaInicio.Month;
        }

    }
}


