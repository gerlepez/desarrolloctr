﻿using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers.Api
{
    public class TurnoApiController : ApiController
    {

        [System.Web.Http.Route("Api/Turno/ObtenerTurnos")]
        [System.Web.Http.ActionName("ObtenerTurnos")]
        [System.Web.Http.HttpGet]
        public object ObtenerTurnos(string fechaDesde, string fechaHasta, string usuario)
        {

            ServicioTurno servTurno = new ServicioTurno();

            ServicioUsuario serUsuario = new ServicioUsuario();
            int idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(usuario);


            object listaTurnos = servTurno.ObtenerTurnos(fechaDesde, fechaHasta, idUsuarioLogueado);
            return listaTurnos;
        }

        [System.Web.Http.Route("Api/Turno/ObtenerListaTipoTurno")]
        [System.Web.Http.ActionName("ObtenerListaTipoTurno")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaTipoTurno(bool activo)
        {
            ServicioTurno servTurno = new ServicioTurno();

            object listaTipoTurno = servTurno.ObtenerListaTipoTurno(activo);

            //((IDisposable)servLinea).Dispose();

            return listaTipoTurno;
        }

        [System.Web.Http.Route("Api/Turno/ObtenerListaMantenimiento")]
        [System.Web.Http.ActionName("ObtenerListaMantenimiento")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaMantenimiento(bool activo)
        {
            ServicioTurno servTurno = new ServicioTurno();

            object listaMantenimiento = servTurno.ObtenerListaMantenimiento(activo);

            //((IDisposable)servLinea).Dispose();

            return listaMantenimiento;
        }

        [System.Web.Http.Route("Api/Turno/ObtenerListaGuardia")]
        [System.Web.Http.ActionName("ObtenerListaGuardia")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaGuardia(int idMantenimiento)
        {
            ServicioTurno servTurno = new ServicioTurno();

            object listaGuardia = servTurno.ObtenerListaGuardia(idMantenimiento);

            //((IDisposable)servLinea).Dispose();

            return listaGuardia;
        }
        [System.Web.Http.Route("Api/Turno/CRUD")]
        [System.Web.Http.ActionName("CRUD")]
        [System.Web.Http.HttpPost]
        public TurnoVM CRUD(TurnoVM turno)
        {
            ServicioTurno servTurno = new ServicioTurno();
            var idUsuarioLogueado = 0;
            if (turno != null)
            {
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(turno.usuario);
            }
            //aca realizar las validaciones correspondientes

            var tur = servTurno.CRUD(turno, idUsuarioLogueado);


            return tur;
        }



        [System.Web.Http.Route("Api/Turno/ActualizarTurnos")]
        [System.Web.Http.ActionName("ActualizarTurnos")]
        [System.Web.Http.HttpGet]
        public int ActualizarTurnos()
        {
            //esta api debe actualizar los vencimientos de los turnos de acuerdo a la fecha actual
            ServicioTurno servTurno = new ServicioTurno();

            object listaTurnos = null;

            //((IDisposable)servLinea).Dispose();

            return 200;
        }

        [System.Web.Http.Route("Api/Turno/VerificarUsuarioGuardia")]
        [System.Web.Http.ActionName("VerificarUsuarioGuardia")]
        [System.Web.Http.HttpGet]
        public UsuarioGuardiaVM VerificarUsuarioGuardia(string nombreUsuario)
        {
            UsuarioGuardiaVM guardia = new UsuarioGuardiaVM();
            if (nombreUsuario != null)
            {
                ServicioTurno servTurno = new ServicioTurno();

                var idUsuarioLogueado = 0;
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(nombreUsuario);

                guardia = servTurno.VerificarUsuarioGuardia(idUsuarioLogueado);
            }
            else
            {
                guardia.codigo = 400;
            }


            return guardia;
        }


        [System.Web.Http.Route("Api/Turno/VerificarTurno")]
        [System.Web.Http.ActionName("VerificarTurno")]
        [System.Web.Http.HttpGet]
        public TurnoVM VerificarTurno(string nombreUsuario)
        {
            TurnoVM turno = new TurnoVM();

            ServicioTurno servicioTurno = new ServicioTurno();
            ServicioUsuario serUsuario = new ServicioUsuario();
            var idUsuarioLogueado = 0;
            idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(nombreUsuario);
            turno = servicioTurno.VerificarTurno(idUsuarioLogueado);
            return turno;
        }
        [System.Web.Http.Route("Api/Turno/ObtenerTurnoEjecucion")]
        [System.Web.Http.ActionName("ObtenerTurnoEjecucion")]
        [System.Web.Http.HttpGet]
        public bool ObtenerTurno(string nombreUsuario)
        {
            var turno = false;

            ServicioTurno servicioTurno = new ServicioTurno();
            ServicioUsuario serUsuario = new ServicioUsuario();
            var idUsuarioLogueado = 0;
            idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(nombreUsuario);
            turno = servicioTurno.ObtenerTurnoEjecucion(idUsuarioLogueado);
            return turno;
        }
        [System.Web.Http.Route("Api/Turno/CerrarTurno")]
        [System.Web.Http.ActionName("CerrarTurno")]
        [System.Web.Http.HttpGet]
        public int CerrarTurno(string nombreUsuario, int idTurnoEjecucion)
        {
            var response = 0;

            ServicioTurno servicioTurno = new ServicioTurno();
            ServicioUsuario serUsuario = new ServicioUsuario();
            var idUsuarioLogueado = 0;
            idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(nombreUsuario);
            response = servicioTurno.CerrarTurno(idUsuarioLogueado, idTurnoEjecucion);
            return response;
        }
        [System.Web.Http.Route("Api/Turno/AbrirTurno")]
        [System.Web.Http.ActionName("AbrirTurno")]
        [System.Web.Http.HttpGet]
        public object AbrirTurno(string nombreJefe, int idTipoTurno, string idUsuario, string estadoTiempo)
        {
            ServicioTurno servicioTurno = new ServicioTurno();
            ServicioUsuario serUsuario = new ServicioUsuario();
            var idUsuarioLogueado = 0;
            idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(nombreJefe);
            object response = servicioTurno.AbrirTurno(idUsuarioLogueado, idTipoTurno, idUsuario, estadoTiempo);
            return response;
        }

        [System.Web.Http.Route("Api/Turno/ObtenerTurnosAbiertos")]
        [System.Web.Http.ActionName("ObtenerTurnosAbiertos")]
        [System.Web.Http.HttpGet]

        public object ObtenerTurnosAbiertos(string nombreUsuario)
        {

            ServicioUsuario servicioUsuario = new ServicioUsuario();
            var idUsuarioLogueado = servicioUsuario.ObtenerUsuarioLogueado(nombreUsuario);

            ServicioTurno servicioTurno = new ServicioTurno();
            object response = servicioTurno.ObtenerTurnosAbiertos(idUsuarioLogueado);

            return response;
        }


        [System.Web.Http.Route("Api/Turno/ObtenerListaTurno")]
        [System.Web.Http.ActionName("ObtenerListaTurno")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaTurno(bool activo)
        {
            ServicioTurno servTurno = new ServicioTurno();

            object listaTurno = servTurno.ObtenerListaTurno(activo);

            //((IDisposable)servLinea).Dispose();

            return listaTurno;
        }

        [System.Web.Http.Route("Api/Turno/ObtenerTipoTurnoCTR")]
        [System.Web.Http.ActionName("ObtenerTipoTurnoCTR")]
        [System.Web.Http.HttpGet]
        public object ObtenerTipoTurnoCTR(bool activo)
        {
            ServicioTurno servTurno = new ServicioTurno();

            object listaTurnoTipoCtr = servTurno.ObtenerTipoTurnoCTR(activo);

            //((IDisposable)servLinea).Dispose();

            return listaTurnoTipoCtr;
        }

        [System.Web.Http.Route("Api/Turno/ObtenerTurnoAbiertoParaGrilla")]
        [System.Web.Http.ActionName("ObtenerTurnoAbiertoParaGrilla")]
        [System.Web.Http.HttpGet]
        public TurnoVM ObtenerTurnoAbiertoParaGrilla()
        {
            ServicioTurno servTurno = new ServicioTurno();
            ServicioUsuario serUsuario = new ServicioUsuario();
            TurnoVM turno = servTurno.ObtenerTurnoAbiertoParaGrilla();
            return turno;
        }

        [System.Web.Http.Route("Api/Turno/ProcesarExcel")]
        [System.Web.Http.ActionName("ProcesarExcel")]
        [System.Web.Http.HttpPost]
        public void ProcesarExcel()
        {
            if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
            {
                ServicioTurno servicio = new ServicioTurno();
                var fechaDesde = System.Web.HttpContext.Current.Request["fechaDesde"];
                var fechaHasta = System.Web.HttpContext.Current.Request["fechaHasta"];
                HttpResponseMessage result = null;
                var httpRequest = System.Web.HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {
                    var docfiles = new List<string>();
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/TempFolder/")))
                            Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/TempFolder/"));
                        if (file != null && postedFile.ContentLength > 0)
                        {
                            var fileName = Path.GetFileName(postedFile.FileName);
                            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/TempFolder/"), fileName);
                            postedFile.SaveAs(path);
                        }
                        string ruta = System.Web.HttpContext.Current.Server.MapPath("~/TempFolder/");
                       
                        servicio.procesarTurnos(ruta, postedFile.FileName, fechaDesde, fechaHasta);
                    }
                }
            }
        }

    }
}
