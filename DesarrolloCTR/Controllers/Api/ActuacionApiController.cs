﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;

namespace DesarrolloCTR.Controllers.Api
{
    public class ActuacionApiController : ApiController
    {
        [System.Web.Http.Route("Api/Actuacion/ObtenerActuacion")]
        [System.Web.Http.ActionName("ObtenerActuacion")]
        [System.Web.Http.HttpGet]
        public object ObtenerActuacion(bool activo)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            object lista = servicioActuacion.ObtenerActuacion(activo);
            return lista;
        }

        [System.Web.Http.Route("Api/Actuacion/ObtenerActuacionIndisponibilidad")]
        [System.Web.Http.ActionName("ObtenerActuacionIndisponibilidad")]
        [System.Web.Http.HttpGet]
        public object ObtenerActuacionIndisponibilidad(bool activo)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            object lista = servicioActuacion.ObtenerActuacionIndisponibilidad(activo);
            return lista;
        }

        [System.Web.Http.Route("Api/Actuacion/ConsultarUnaActuacion")]
        [System.Web.Http.ActionName("ConsultarUnaActuacion")]
        [System.Web.Http.HttpGet]
        public ActuacionVM ConsultarUnaActuacion(int idActuacion)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            ActuacionVM actuacion = servicioActuacion.ConsultarUnaActuacion(idActuacion);
            return actuacion;
        }

        [System.Web.Http.Route("Api/Actuacion/ObtenerActuacionPorEquipo")]
        [System.Web.Http.ActionName("ObtenerActuacionPorEquipo")]
        [System.Web.Http.HttpGet]
        public object ObtenerActuacionPorEquipo(string codigoEquipo, int idEquipo, int idNovedad)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            object lista = servicioActuacion.ObtenerActuacionPorEquipo(codigoEquipo, idEquipo, idNovedad);
            return lista;
        }

        [System.Web.Http.Route("Api/Actuacion/ObtenerTipoActuacion")]
        [System.Web.Http.ActionName("ObtenerTipoActuacion")]
        [System.Web.Http.HttpGet]
        public object ObtenerTipoActuacion(bool activo)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            object lista = servicioActuacion.ObtenerTipoActuacion(activo);
            return lista;
        }

        [System.Web.Http.Route("Api/Actuacion/ObtenerTipoActPorActEquipo")]
        [System.Web.Http.ActionName("ObtenerTipoActPorActEquipo")]
        [System.Web.Http.HttpGet]
        public object ObtenerTipoActPorActEquipo(int idActuacion, string codigoEquipo)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            object lista = servicioActuacion.ObtenerTipoActPorActEquipo(idActuacion,codigoEquipo);



            return lista;
        }


       
        /// ////////////////////////////////////////////////////////////////////////////

        [System.Web.Http.Route("Api/Actuacion/TablaActuacion")]
        [System.Web.Http.ActionName("TablaActuacion")]
        [System.Web.Http.HttpGet]
        public object TablaActuacion(bool activo)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            object lista = servicioActuacion.tablaActuacion(activo);
            return lista;
        }
        //Este es para las relaciones
        [System.Web.Http.Route("Api/Actuacion/ConsultarUnActuacion")]
        [System.Web.Http.ActionName("ConsultarUnActuacion")]
        [System.Web.Http.HttpGet]
        public ActuacionVM ConsultarUnActuacion(int idActuacion)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            ActuacionVM actuacion = servicioActuacion.ConsultarUnaActuacion(idActuacion);
            return actuacion;
        }

        //Este es solo para 1 actuacion
        [System.Web.Http.Route("Api/Actuacion/ConsultarUnaActuaciones")]
        [System.Web.Http.ActionName("ConsultarUnaActuaciones")]
        [System.Web.Http.HttpGet]
        public ActuacionVM ConsultarUnaActuaciones(int idActuacion)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            ActuacionVM actuacion = servicioActuacion.ConsultarUnaActuaciones(idActuacion);
            return actuacion;
        }

        [System.Web.Http.Route("Api/Actuacion/AltaActuacion")]
        [System.Web.Http.ActionName("AltaActuacion")]
        [System.Web.Http.HttpPost]
        public int AltaActuacion(ActuacionVM actuacion)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            int codigo;
            if (servicioActuacion.ValidarActuacion(actuacion))
            {
                codigo = servicioActuacion.AltaActuacion(actuacion);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }

        //Relaciones
        [System.Web.Http.Route("Api/Actuacion/ModificarActuacion")]
        [System.Web.Http.ActionName("ModificarActuacion")]
        [System.Web.Http.HttpPost]
        public int ModificarActuacion(ActuacionVM actuacion)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            int codigo;
            if (servicioActuacion.ValidarActuacion(actuacion))
            {
                codigo = servicioActuacion.ModificaraActuacion(actuacion);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }
        //Solo 1 actuacion
        [System.Web.Http.Route("Api/Actuacion/ModificarActuaciones")]
        [System.Web.Http.ActionName("ModificarActuaciones")]
        [System.Web.Http.HttpPost]
        public int ModificarActuaciones(ActuacionVM actuacion)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            int codigo;
            if (servicioActuacion.ValidarActuacion(actuacion))
            {
                codigo = servicioActuacion.ModificaraActuaciones(actuacion);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }
        //Elimina 1 actuacion
        [System.Web.Http.Route("Api/Actuacion/EliminarActuacion")]
        [System.Web.Http.ActionName("EliminarActuacion")]
        [System.Web.Http.HttpPost]
        public int EliminarActuacion(ActuacionVM actuacion)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            int codigo = servicioActuacion.EliminarActuacion(actuacion);
            return codigo;
        }
        //Elimina la relacion
        [System.Web.Http.Route("Api/Actuacion/EliminarActuacionRelacion")]
        [System.Web.Http.ActionName("EliminarActuacionRelacion")]
        [System.Web.Http.HttpPost]
        public int EliminarActuacionRelacion(ActuacionVM actuacion)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            int codigo = servicioActuacion.EliminarActuacionRelacion(actuacion);
            return codigo;
        }

        [System.Web.Http.Route("Api/Actuacion/RecuperarActuacion")]
        [System.Web.Http.ActionName("RecuperarActuacion")]
        [System.Web.Http.HttpPost]
        public int RecuperarActuacion(ActuacionVM actuacion)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            int codigo = servicioActuacion.RecuperarActuacion(actuacion);
            return codigo;
        }
        
        [System.Web.Http.Route("Api/Actuacion/RecuperarActuacionRelacion")]
        [System.Web.Http.ActionName("RecuperarActuacionRelacion")]
        [System.Web.Http.HttpPost]
        public int RecuperarActuacionRelacion(ActuacionVM actuacion)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            int codigo = servicioActuacion.RecuperarActuacionRelacion(actuacion);
            return codigo;
        }
        [System.Web.Http.Route("Api/Actuacion/ObtenerListaActuacionTipoEstado")]
        [System.Web.Http.ActionName("ObtenerListaActuacionTipoEstado")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaActuacionTipoEstado(bool activo)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            object lista = servicioActuacion.ObtenerListaActuacionTipoEstado(activo);
            return lista;
        }

        [System.Web.Http.Route("Api/Actuacion/ObtenerListaEquipo")]
        [System.Web.Http.ActionName("ObtenerListaEquipo")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaEquipo(bool activo)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            object listaEquipo = servicioActuacion.ObtenerListaEquipo(activo);

            return listaEquipo;
        }
        // ESTE ES PARA EL ALTA DE UNA RELACION PARA SUMAR EL TIPO Y EL QUIPO A UNA ACTUACION
        [System.Web.Http.Route("Api/Actuacion/AltaActuacionTipoYEquipo")]
        [System.Web.Http.ActionName("AltaActuacionTipoYEquipo")]
        [System.Web.Http.HttpPost]
        public int AltaActuacionTipoYEquipo(ActuacionVM actuacion)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            int codigo;
            codigo = servicioActuacion.AltaActuacionTipoYEquipo(actuacion);

            return codigo;
        }


        ///  ↓↓↓↓↓↓  TIPO ACTUACION  ↓↓↓↓↓↓↓↓ ///

        [System.Web.Http.Route("Api/Actuacion/AltaActuacionTipo")]
        [System.Web.Http.ActionName("AltaActuacionTipo")]
        [System.Web.Http.HttpPost]
        public int AltaActuacionTipo(TipoActuacionVM Tipoactuacion)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            int codigo;
            codigo = servicioActuacion.AltaActuacionTipo(Tipoactuacion);

            return codigo;
        }

        [System.Web.Http.Route("Api/Actuacion/ConsultarUnTipoActuacion")]
        [System.Web.Http.ActionName("ConsultarUnTipoActuacion")]
        [System.Web.Http.HttpGet]
        public TipoActuacionVM ConsultarUnTipoActuacion(int idTipoActuacion)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            TipoActuacionVM actuacion = servicioActuacion.ConsultarUnTipoActuacion(idTipoActuacion);
            return actuacion;
        }

        [System.Web.Http.Route("Api/Actuacion/ModificarTipoActuacion")]
        [System.Web.Http.ActionName("ModificarTipoActuacion")]
        [System.Web.Http.HttpPost]
        public int ModificarTipoActuacion(TipoActuacionVM Tipoactuacion)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            int codigo;
            if (servicioActuacion.ValidarTipoActuacion(Tipoactuacion))
            {
                codigo = servicioActuacion.ModificarTipoActuacion(Tipoactuacion);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }

        [System.Web.Http.Route("Api/Actuacion/EliminarTipoActuacion")]
        [System.Web.Http.ActionName("EliminarTipoActuacion")]
        [System.Web.Http.HttpPost]
        public int EliminarTipoActuacion(TipoActuacionVM Tipoactuacion)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            int codigo = servicioActuacion.EliminarTipoActuacion(Tipoactuacion);
            return codigo;
        }

        [System.Web.Http.Route("Api/Actuacion/RecuperarTipoActuacion")]
        [System.Web.Http.ActionName("RecuperarTipoActuacion")]
        [System.Web.Http.HttpPost]
        public int RecuperarTipoActuacion(TipoActuacionVM Tipoactuacion)
        {
            ServicioActuacion servicioActuacion = new ServicioActuacion();
            int codigo = servicioActuacion.RecuperarTipoActuacion(Tipoactuacion);
            return codigo;
        }
    }
}
