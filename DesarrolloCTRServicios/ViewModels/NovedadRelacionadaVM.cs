﻿using System;
using System.Collections.Generic;

namespace DesarrolloCTRServicios.ViewModels
{
    public class NovedadRelacionadaVM
    {
        public int idNovedad { get; set; }
        public int idNovedadOrigen { get; set; }
        public int idNovedadRelacionada { get; set; }
        public string nombreNovedad { get; set; }
        public string descripcionNovedad { get; set; }
        public string fechaHoraNovedad { get; set; }
        public int? idUsarioAlta { get; set; }
        public DateTime? fechaUsarioAlta { get; set; }
        public int idUsuarioAlta { get; set; }
        public int? idUsuarioModifica { get; set; }
        public int? idTipoNovedad { get; set; }
        public int? idEquipo { get; set; }
        public int? idTipoFalla { get; set; }
        public int? idTransformador { get; set; }
        public int? idLinea { get; set; }
        public int? idLicencia { get; set; }
        public int? idPuntoConexion { get; set; }
        public int? idCapacitores { get; set; }
        public int? idCentral { get; set; }
        public int? idGenerador { get; set; }
        public int? idActuacion { get; set; }
        public int? idTipoActuacion { get; set; }
        public int? idTipoLinea { get; set; }
        public int? posesionLinea { get; set; }
        public int? idTipoReclamo { get; set; }
        public int? idInfraestructura { get; set; }
        public int? idEquipoReclamo { get; set; }
        public int? idTipoEquipo { get; set; }
        public int? idEstacion { get; set; }
        public int? idCentroControl { get; set; }
        public string descripcionNota { get; set; }
        public DateTime? fechaUsuarioModifica { get; set; }
        public bool? activo { get; set; }
        public string nombresuario { get; set; }
        public int? codigo { get; set; }
        public int? idEmpleado { get; set; }
        public string fechaParseada { get; set; } // no se si usarla
        public string horaParseada { get; set; }
        public string CodigoNovedad { get; set; }
        public DateTime? fechaNovedad { get; set; }
        public DateTime? horaNovedad { get; set; }
        public int? idServiciosAuxiliares { get; set; }
        public AutomatismoVM Automatismo { get; set; }
        public EstadoNovedadVM estadoNovedad { get; set; }
        public LicenciaVM licencia { get; set; }
        public TipoNovedadVM tipoNovedad { get; set; }
        public EquipoVM equipo { get; set; }
        public TipoActuacionVM tipo { get; set; }
        public TransformadorVM transformador { get; set; }
        public EstacionVM estacion { get; set; }
        public LineaVM linea { get; set; }
        public CapacitorVM capacitor { get; set; }
        public PuntoConexionVM puntoConexion { get; set; }
        public CentroControlVM centroControl { get; set; }
        public AlarmasVM alarma { get; set; }
        public List<NovedadEquipoVM> novedadEquipo { get; set; }

    }
}
