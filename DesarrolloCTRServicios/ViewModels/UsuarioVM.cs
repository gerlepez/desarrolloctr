﻿using System;

namespace DesarrolloCTRServicios.ViewModels
{
    public class UsuarioVM
    {
        public int idLogin { get; set; }
        public int idUsuario { get; set; }
        public string nombreUsuario { get; set; }
        public string contraseniaUsuario { get; set; }
        public bool activo { get; set; }
        public DateTime? fechaCambioContrasenia { get; set; }
        public string foto { get; set; }
        public string filePath { get; set; }
        public bool? esGuardia { get; set; }
        public bool? esJefeGuardia { get; set; }
    }
}

