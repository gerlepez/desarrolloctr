﻿using System;

namespace DesarrolloCTRServicios.ViewModels
{
    public class HistorialEquipoVM
    {
        public int idHistorialEquipo { get; set; }
        public int idEquipo { get; set; }
        public int idEstadoEquipo { get; set; }
        public int idUsuario { get; set; }
        public DateTime fechaCambioEstado { get; set; }
        public bool activo { get; set; }
    }
}
