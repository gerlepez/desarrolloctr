﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;

namespace DesarrolloCTR.Controllers.Api
{
    public class EstacionApiController : ApiController
    {
        [System.Web.Http.Route("Api/Estacion/ObtenerListaEstacion")]
        [System.Web.Http.ActionName("ObtenerListaEstacion")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaEstacion(bool activo)
        {
            ServicioEstacion servEstacion = new ServicioEstacion();

            object listaEstacion = servEstacion.ObtenerListaEstacion(activo);

            ((IDisposable)servEstacion).Dispose();

            return listaEstacion;
        }

        [System.Web.Http.Route("Api/Estacion/ObtenerEstacionPorInfra")]
        [System.Web.Http.ActionName("ObtenerEstacionPorInfra")]
        [System.Web.Http.HttpGet]
        public object ObtenerEstacionPorInfra(int idInfraestructura)
        {
            ServicioEstacion servEstacion = new ServicioEstacion();

            object listaEstacion = servEstacion.ObtenerEstacionPorInfra(idInfraestructura);

            ((IDisposable)servEstacion).Dispose();

            return listaEstacion;
        }

        [System.Web.Http.Route("Api/Estacion/ObtenerEstacionPorAutomatismo")]
        [System.Web.Http.ActionName("ObtenerEstacionPorAutomatismo")]
        [System.Web.Http.HttpGet]
        public object ObtenerEstacionPorAutomatismo(int idTipoAutomatismo)
        {
            ServicioEstacion servEstacion = new ServicioEstacion();

            object listaEstacion = servEstacion.ObtenerEstacionPorAutomatismo(idTipoAutomatismo);

            return listaEstacion;
        }

        [System.Web.Http.Route("Api/Estacion/AltaEstacion")]
        [System.Web.Http.ActionName("AltaEstacion")]
        [System.Web.Http.HttpPost]
        public int AltaEstacion(EstacionVM estacion)
        {
            int codigo = 0;
            var idUsuarioLogueado = 0; 
            if (estacion != null)
            {
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogueado  = serUsuario.ObtenerUsuarioLogueado(estacion.nombresuario);
                ServicioEstacion servEstacion = new ServicioEstacion();

                if (servEstacion.ValidarEstacion(estacion))
                {
                    codigo = servEstacion.AltaEstacion(estacion, idUsuarioLogueado);
                }
                else
                {
                    codigo = 400;
                }
            }
            //((IDisposable)servEstacion).Dispose();

            return codigo;
        }

        [System.Web.Http.Route("Api/Estacion/ModificarEstacion")]
        [System.Web.Http.ActionName("ModificarEstacion")]
        [System.Web.Http.HttpPost]
        public int ModificarEstacion(EstacionVM estacion)
        {
            var idUsuarioLogueado = 0;
            if (estacion != null)
            {
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(estacion.nombresuario);
            }
            ServicioEstacion servEstacion = new ServicioEstacion();
            int codigo;
            if (servEstacion.ValidarEstacion(estacion))
            {
                codigo = servEstacion.ModificarEstacion(estacion, idUsuarioLogueado);
            }
            else
            {
                codigo = 400;
            }

            //((IDisposable)servEstacion).Dispose();

            return codigo;
        }

        [System.Web.Http.Route("Api/Estacion/EliminarEstacion")]
        [System.Web.Http.ActionName("EliminarEstacion")]
        [System.Web.Http.HttpPost]
        public int EliminarEstacion(EstacionVM estacion)
        {
            ServicioEstacion servEstacion = new ServicioEstacion();
            int codigo = servEstacion.EliminarEstacion(estacion);

            //((IDisposable)servEstacion).Dispose();

            return codigo;
        }

        [System.Web.Http.Route("Api/Estacion/ConsultarUnEstacion")]
        [System.Web.Http.ActionName("ConsultarUnEstacion")]
        [System.Web.Http.HttpGet]
        public EstacionVM ConsultarUnEstacion(int idEstacion)
        {
            ServicioEstacion servEstacion = new ServicioEstacion();
            EstacionVM estacion = servEstacion.ConsultarUnEstacion(idEstacion);

            //((IDisposable)servEstacion).Dispose();

            return estacion;
        }
        [System.Web.Http.Route("Api/Estacion/RecuperarEstacion")]
        [System.Web.Http.ActionName("RecuperarEstacion")]
        [System.Web.Http.HttpPost]
        public int RecuperarEstacion(EstacionVM estacion)
        {
            ServicioEstacion servEstacion = new ServicioEstacion();
            int codigo = servEstacion.RecuperarEstacion(estacion);

            //((IDisposable)servEstacion).Dispose();

            return codigo;
        }

        [System.Web.Http.Route("Api/Estacion/ObtenerListaPropiedadEstacion")]
        [System.Web.Http.ActionName("ObtenerListaPropiedadEstacion")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaPropiedadEstacion()
        {
            ServicioEstacion servEstacion = new ServicioEstacion();

            object listaPropiedadEstacion = servEstacion.ObtenerListaPropiedadEstacion();


            return listaPropiedadEstacion;
        }


        [System.Web.Http.Route("Api/Estacion/ObtenerListaTipoEstacion")]
        [System.Web.Http.ActionName("ObtenerListaTipoEstacion")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaTipoEstacion()
        {
            ServicioEstacion servEstacion = new ServicioEstacion();

            object listaTipoEstacion = servEstacion.ObtenerListaTipoEstacion();


            return listaTipoEstacion;
        }




    }
}
