﻿using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DesarrolloCTR.Controllers.Api
{
    public class TipoActuacionApiController : ApiController
    {
        [System.Web.Http.Route("Api/TipoActuacion/ObtenerListaTipoActuacion")]
        [System.Web.Http.ActionName("ObtenerListaTipoActuacion")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaTipoActuacion(bool activo)
        {
            ServicioTipoActuacion servActuacion = new ServicioTipoActuacion();

            object listaActuacion = servActuacion.ObtenerListaTipoActuacion(activo);

            //((IDisposable)servActuacion).Dispose();

            return listaActuacion;

        }

        [System.Web.Http.Route("Api/TipoActuacion/ObtenerTipoActuacionPorActEqui")]
        [System.Web.Http.ActionName("ObtenerTipoActuacionPorActEqui")]
        [System.Web.Http.HttpGet]
        public object ObtenerTipoActuacionPorActEqui(int idActuacion, string codigoEquipo)
        {
            ServicioTipoActuacion servActuacion = new ServicioTipoActuacion();

            object listaActuacion = servActuacion.ObtenerTipoActuacionPorActEqui(idActuacion, codigoEquipo);

            //((IDisposable)servActuacion).Dispose();

            return listaActuacion;

        }

        [System.Web.Http.Route("Api/TipoActuacion/AltaTipoActuacion")]
        [System.Web.Http.ActionName("AltaTipoActuacion")]
        [System.Web.Http.HttpPost]
        public int AltaTipoActuacion(TipoActuacionVM TipoActuacion)
        {
            ServicioTipoActuacion servActuacion = new ServicioTipoActuacion();

            int codigo;
            if (servActuacion.ValidarActuacion(TipoActuacion))
            {
                codigo = servActuacion.AltaTipoActuacion(TipoActuacion);
            }
            else
            {
                codigo = 400;
            }

            //((IDisposable)servActuacion).Dispose();

            return codigo;
        }

        [System.Web.Http.Route("Api/TipoActuacion/ModificarTipoActuacion")]
        [System.Web.Http.ActionName("ModificarTipoActuacion")]
        [System.Web.Http.HttpPost]
        public int ModificarTipoActuacion(TipoActuacionVM TipoActuacion)
        {
            ServicioTipoActuacion servActuacion = new ServicioTipoActuacion();

            int codigo;
            if (servActuacion.ValidarActuacion(TipoActuacion))
            {
                codigo = servActuacion.ModificarTipoActuacion(TipoActuacion);
            }
            else
            {
                codigo = 400;
            }

            //((IDisposable)servActuacion).Dispose();

            return codigo;
        }

        [System.Web.Http.Route("Api/TipoActuacion/EliminarTipoActuacion")]
        [System.Web.Http.ActionName("EliminarTipoActuacion")]
        [System.Web.Http.HttpPost]
        public int EliminarTipoActuacion(TipoActuacionVM TipoActuacion)
        {
            ServicioTipoActuacion servActuacion = new ServicioTipoActuacion();
            int codigo = servActuacion.EliminarTipoActuacion(TipoActuacion);

            //((IDisposable)servActuacion).Dispose();

            return codigo;
        }


        [System.Web.Http.Route("Api/TipoActuacion/ConsultarUnTipoActuacion")]
        [System.Web.Http.ActionName("ConsultarUnTipoActuacion")]
        [System.Web.Http.HttpGet]
        public TipoActuacionVM ConsultarUnTipoActuacion(int idTipoActuacion)
        {
            ServicioTipoActuacion servActuacion = new ServicioTipoActuacion();
            TipoActuacionVM Actuacion = servActuacion.ConsultarUnTipoActuacion(idTipoActuacion);

            //((IDisposable)servActuacion).Dispose();

            return Actuacion;
        }


        [System.Web.Http.Route("Api/TipoActuacion/TipoRecuperarActuacion")]
        [System.Web.Http.ActionName("RecuperarTipoActuacion")]
        [System.Web.Http.HttpPost]
        public int RecuperarTipoActuacion(TipoActuacionVM TipoActuacion)
        {
            ServicioTipoActuacion servActuacion = new ServicioTipoActuacion();
            int codigo = servActuacion.RecuperarTipoActuacion(TipoActuacion);

            //((IDisposable)servActuacion).Dispose();

            return codigo;
        }
    }
}
