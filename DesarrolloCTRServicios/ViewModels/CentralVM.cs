﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class CentralVM
    {
        public int? idCentral { get; set; }
        public int? idCentroControl { get; set; }
        public int? idTipoCentral { get; set; }
        public int idUsuarioAlta { get; set; }

        public string nombreTipoCentral { get; set; }
        public string nombreCentral { get; set; }
        public string nombreEquipo { get; set; }
        public string nombreCentroContorl { get; set; }
        public string propiedadGenerador { get; set; }
        public string nombresuario { get; set; }
        public string telefono { get; set; }

        public bool? activo { get; set; }
        public bool? TieneGenerador { get; set; }

        public CentroControlVM centroControl { get; set; }
        public UsuarioVM usuario { get; set; }

    }
}
