﻿using System.Collections.Generic;

namespace DesarrolloCTRServicios.ViewModels
{
    public class ActuacionVM
    {
        public int? idActuacion { get; set; }
        public int? idActuacionEstado { get; set; }
        public string nombreActuacion { get; set; }
        public bool? activo { get; set; }
        public int? idTipoActuacion { get; set; }
        public string nombreTipoActuacion { get; set; }
        public int? idTipoActuacionEstado { get; set; }
        public int? itn { get; set; }
        public int? idActuacTipoFallaEquipo { get; set; }

        public EquipoVM equipo { get; set; }
        public TipoActuacionEstadoVM tipoActuacionEstado { get; set; }
        public TipoActuacionVM tipoActuacion { get; set; }
        public ActuacionTipoEquipo actuacionTipoEquipo { get; set; }

        public List<string> listaTipoActuacion { get; set; }

    }

}
