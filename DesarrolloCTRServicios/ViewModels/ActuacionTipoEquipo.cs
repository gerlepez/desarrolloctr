﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesarrolloCTRServicios.ViewModels
{
    public class ActuacionTipoEquipo
    {
       public int? idActuacTipoFallaEquipo { get; set; }
        public int? idActuacion { get; set; }
        public int? idTipoActuacion { get; set; }
        public int? idEquipo { get; set; }
        public bool? activo { get; set; }
    }
}
