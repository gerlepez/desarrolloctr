﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class EquipoDetalleEstacionVM
    {
        public int? idEquipoDetalleEstacion { get; set; }
        public string nombreEquipoDetalleEstacion { get; set; }
        public int? idEquipo { get; set; }
        public string nombreEquipo { get; set; }
        public string nombreEstacion { get; set; }
        public bool activo { get; set; }
        public int? idEstacion { get; set; }
        public int? idCentral { get; set; }
        public int? idCentroControl { get; set; }
        public string CodigoEquipo { get; set; }
        public int? idTipoLinea { get; set; }
        public bool? propiedadLinea { get; set; }
        public string nombreTipoEquipo { get; set; }
        public int? idPagoTran { get; set; }
        public int? idPropiedad { get; set; }
    }
}
