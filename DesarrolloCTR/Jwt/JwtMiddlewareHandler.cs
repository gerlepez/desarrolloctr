﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Web.Http.Controllers;
using Microsoft.IdentityModel.Tokens;
using System.Web.Http.Filters;
using DesarrolloCTRServicios.Servicios;

namespace BuhoGestion.App_Start
{
    public class JwtMiddlewareHandler : DelegatingHandler
    {
        private static readonly string SecretKey = ConfigurationManager.AppSettings["JwtSecretKey"];
        private static readonly string[] ExcludedRoutes = { "Api/Login" };
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            //Lo siguiente se agrego para incluir una excepcion por si el ambiente esta en testing
            var isTesting = ConfigurationManager.AppSettings["isTesting"];
            if (!string.IsNullOrEmpty(isTesting) && isTesting.Equals("Si", StringComparison.OrdinalIgnoreCase))
            {
                return await base.SendAsync(request, cancellationToken);
            }
            var requestPath = request.RequestUri.AbsolutePath.Trim('/');
            if (ExcludedRoutes.Any(route => requestPath.StartsWith(route, StringComparison.OrdinalIgnoreCase)))
            {
                return await base.SendAsync(request, cancellationToken);
            }

            HttpCookie cookie = HttpContext.Current.Request.Cookies["jwtToken"];
            if (cookie == null || string.IsNullOrEmpty(cookie?.Value))
                return new HttpResponseMessage(System.Net.HttpStatusCode.Redirect)
                {
                    Content = new StringContent("Token no proporcionado"),
                    Headers =
                    {
                        Location = new Uri(request.RequestUri,"/Login/Index")
                    }
                };

            try
            {
                var token = cookie.Value;
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.UTF8.GetBytes(SecretKey);
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero,
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;

                var nameIdClaim = jwtToken.Claims.FirstOrDefault(x => x.Type == "nameid");
                if (nameIdClaim == null || string.IsNullOrEmpty(nameIdClaim.Value))
                    throw new SecurityTokenValidationException("El claim 'nameid' no está presente o es inválido.");

                var validFrom = jwtToken.ValidFrom;
                if (DateTime.UtcNow < validFrom)
                    throw new SecurityTokenValidationException("El token fue emitido en una fecha futura.");

                var exp = jwtToken.ValidTo;
                if (DateTime.UtcNow > exp)
                    throw new SecurityTokenValidationException("El token ha expirado.");

                var issuedAt = jwtToken.IssuedAt;
                if (issuedAt > DateTime.UtcNow)
                    throw new SecurityTokenValidationException("El token fue emitido en una fecha futura.");

                var userId = int.Parse(nameIdClaim.Value);
                ServicioUsuario _servicioUsuario = new ServicioUsuario();
                var user = _servicioUsuario.ObtenerDatosUsuarioPorId(userId);
                if (user == null || user.activo != true)
                    throw new SecurityTokenValidationException("El token no contiene un id usuario válido.");

                request.Properties["UserId"] = userId;
            }
            catch
            {

                return new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized)
                {
                    Content = new StringContent("Token invalido")
                };
            }
            return await base.SendAsync(request, cancellationToken);
        }
        public static DateTime ConvertFromUnixTimestamp(long timestamp)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return epoch.AddSeconds(timestamp);
        }

    }
}