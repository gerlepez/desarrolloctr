//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DesarrolloCTRModelos
{
    using System;
    using System.Collections.Generic;
    
    public partial class GralHistorialEstado
    {
        public int idHistorialEstado { get; set; }
        public Nullable<int> idEstado { get; set; }
        public Nullable<int> fechaEstado { get; set; }
        public Nullable<int> idUsuarioAlta { get; set; }
        public Nullable<int> idUsuarioModif { get; set; }
        public Nullable<System.DateTime> fechaAlta { get; set; }
        public Nullable<System.DateTime> fechaModif { get; set; }
    }
}
