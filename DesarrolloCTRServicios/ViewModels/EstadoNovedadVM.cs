﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class EstadoNovedadVM
    {
        public int idEstadoNovedad { get; set; }
        public string nombreEstadoNovedad { get; set; }
        public bool? activo { get; set; }
    }
}
