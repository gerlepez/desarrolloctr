﻿using DesarrolloCTR.Controllers.Extra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers
{
    [SessionUtilityAttribute]
    [Permiso("INDI")]
    public class IndisponibilidadController : Controller
    {
        // GET: Indisponibilidad
        public ActionResult PlanillaIndisponibilidad()
        {
            return View();
        }
        public ActionResult AdministrarIndisponibilidad()
        {
            return View();
        }
    }
}