//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DesarrolloCTRModelos
{
    using System;
    using System.Collections.Generic;
    
    public partial class LicLicencia
    {
        public int idLicencia { get; set; }
        public Nullable<System.DateTime> fechaHoraInicioLicencia { get; set; }
        public Nullable<System.DateTime> fechaHoraFinLicencia { get; set; }
        public string codigoLicencia { get; set; }
        public string descripcionLicencia { get; set; }
        public Nullable<bool> activo { get; set; }
        public Nullable<int> idEstadoLicencia { get; set; }
        public Nullable<int> idEmpleado { get; set; }
        public Nullable<int> anioCodigo { get; set; }
        public Nullable<int> numLicencia { get; set; }
        public Nullable<int> idTipoLicencia { get; set; }
        public Nullable<int> idPersonalAutoriza { get; set; }
        public Nullable<int> idResponsableConsigna { get; set; }
        public Nullable<int> idPersonalFinaliza { get; set; }
        public Nullable<int> idPersonalCancela { get; set; }
        public string observacionesFinaliza { get; set; }
    }
}
