﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using DesarrolloCTRUtilidades;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioNovedad
    {

        public IQueryable<GrillaNovedadVM> ObtenerGrillaNovedad(DateTime fechaDesde, DateTime fechaHasta)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {

                IQueryable<GrillaNovedadVM> iquery = from novedad in db.NovNovedad
                                                     join tipoNovedad in db.NovTipoNovedad on novedad.idTipoNovedad equals tipoNovedad.idTipoNovedad
                                                     join estadoNovedad in db.NovEstadoNovedad on novedad.idEstadoNovedad equals estadoNovedad.idEstadoNovedad
                                                     join licencia in db.LicLicencia on novedad.idLicencia equals licencia.idLicencia into lic
                                                     from licen in lic.DefaultIfEmpty()
                                                     where novedad.fechaNovedad <= fechaHasta && novedad.fechaNovedad >= fechaDesde
                                                     select new GrillaNovedadVM
                                                     {
                                                         codigoTipoNovedad = tipoNovedad.codigoTipoNovedad,
                                                         activo = novedad.activo,
                                                         idNovedad = novedad.idNovedad,
                                                         codigoNovedad = novedad.codNovedad,
                                                         descripcion = novedad.descripcionNovedad + "&" + novedad.nota,
                                                         idEstado = estadoNovedad.idEstadoNovedad,
                                                         idLicencia = licen.idLicencia,
                                                         codigoLicencia = licen.codigoLicencia,
                                                         idTipoNovedad = novedad.idTipoNovedad,
                                                         fecha = novedad.fechaNovedad,
                                                         hora = novedad.horaNovedad,
                                                         nombreNovedad = tipoNovedad.nombreTipoNovedad,
                                                         nombreEstado = estadoNovedad.estadoNovedad,
                                                     };

                return iquery;
            }

        }

        public List<GrillaNovedadVM> ObtenerGrillaNovedadFiltrada(IQueryable<GrillaNovedadVM> iquery2, bool activo, DateTime fechaDesde, DateTime fechaHasta, ref int cantidadDeRegistros, int idTipoNovedad = 0,
            int idActuacion = 0, int skip = 0, int tamano = 0, string descripcion = " ", string codigoLicencia = " ", int estadoNoveda = 0, string novRelacionadas = " ", string sortColumn = "", string sortColumnDir = "", string searchValue = "")
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                IQueryable<GrillaNovedadVM> iquery = (from novedad in db.NovNovedad
                                                      join tipoNovedad in db.NovTipoNovedad on novedad.idTipoNovedad equals tipoNovedad.idTipoNovedad
                                                      join estadoNovedad in db.NovEstadoNovedad on novedad.idEstadoNovedad equals estadoNovedad.idEstadoNovedad
                                                      join licencia in db.LicLicencia on novedad.idLicencia equals licencia.idLicencia into lic
                                                      from licen in lic.DefaultIfEmpty()
                                                      join rec in db.NovReclamo on novedad.idNovedad equals rec.idNovedad into r
                                                      from reclamo in r.DefaultIfEmpty()
                                                      join ac in db.NovActuacion on novedad.idNovedad equals ac.idNovedad into actu
                                                      from actuacion in actu.DefaultIfEmpty()
                                                      join nac in db.NovActuacionEstado on actuacion.idActuacionEstado equals nac.idActuacionEstado into nact
                                                      from nombreActuacion in nact.DefaultIfEmpty()
                                                      join turnE in db.TurnTurnoEjecucion on novedad.idTurnoEjecucion equals turnE.idTurnoEjecucion into turnEj
                                                      from turnoEjecucion in turnEj.DefaultIfEmpty()
                                                      where novedad.fechaNovedad <= fechaHasta && novedad.fechaNovedad >= fechaDesde
                                                      select new { novedad, tipoNovedad, estadoNovedad, reclamo, licen, actuacion, nombreActuacion, turnoEjecucion} ).AsEnumerable().Select(x=> new GrillaNovedadVM
                                                      {
                                                          codigoTipoNovedad = x.tipoNovedad != null ? x.tipoNovedad.codigoTipoNovedad: null,
                                                          activo = x.novedad.activo,
                                                          idNovedad = x.novedad.idNovedad,
                                                          codigoNovedad = x.novedad.codNovedad,
                                                          descripcion = x.novedad.descripcionNovedad + "&" + x.novedad.nota,
                                                          idEstado = x.estadoNovedad!=null? x.estadoNovedad.idEstadoNovedad: 0,
                                                          numeroReclamo =x.reclamo != null? x.reclamo.numReclamo : null,
                                                          idLicencia = x.licen != null ? x.licen.idLicencia:0,
                                                          idActuacion = x.actuacion != null? x.actuacion.idActuacionEstado:0,
                                                          nombreActuacion = x.nombreActuacion!= null? x.nombreActuacion.nombreActuacionEstado: null,
                                                          codigoLicencia = x.licen != null ? x.licen.codigoLicencia:null,
                                                          idTipoNovedad = x.novedad.idTipoNovedad,
                                                          fecha = x.novedad.fechaNovedad.Value.AddHours(x.novedad.horaNovedad.Value.Hour).AddMinutes(x.novedad.horaNovedad.Value.Minute),
                                                          hora = x.novedad.horaNovedad,
                                                          nombreNovedad = x.tipoNovedad != null? x.tipoNovedad.nombreTipoNovedad: null,
                                                          nombreEstado = x.estadoNovedad != null ? x.estadoNovedad.estadoNovedad:null,
                                                          tieneTurnoEjecucion = x.turnoEjecucion!=null?( x.turnoEjecucion.idTurnoEjecucion == 0 ? false : true): false,
                                                          detalleNovedad = new NovedadVM
                                                          {

                                                          }
                                                      }).AsQueryable();
               

                //var contador = 0;  //HICE ESTO PARA COMPARAR SI ME TRAE BIEN LAS NOVEDADES EN EL BOTON DE LIBRO DE NOVEDADES 
                //foreach (var item in list)
                //{
                //    if(item.tieneTurnoEjecucion == true && item.activo == true)
                //    {
                //        contador++;
                //    }

                //}
                //var algo = contador;
                iquery = iquery.OrderByDescending(x => x.fecha);
                //Filtros              
                if (novRelacionadas != null)
                {
                    var list = iquery.ToList();
                    List<GrillaNovedadVM> final = new List<GrillaNovedadVM>();
                    var ramaBase = list.Where(x => x.codigoNovedad.ToUpper() == "NOV-" + novRelacionadas.ToUpper());
                    foreach (var novR in ramaBase)
                    {
                        var NovRama = ObtenerRamaCompletaDeNovedad(novR.idNovedad);
                        foreach (var idRama in NovRama)
                        {
                            final.Add(list.Where(x => x.idNovedad == idRama.idNovedad).FirstOrDefault());
                        }
                    }
                    iquery = final.AsQueryable<GrillaNovedadVM>();
                }

                if (activo == false)
                {
                    iquery = iquery.Where(x => x.activo == false);
                }
                else
                {
                    iquery = iquery.Where(x => x.activo == true);
                }

                if (idTipoNovedad > 0)
                {
                    iquery = iquery.Where(x => x.idTipoNovedad == idTipoNovedad);
                }
                if (idActuacion > 0)
                {
                    iquery = iquery.Where(x => x.idActuacion == idActuacion);
                }
                if (descripcion != null)
                {
                    iquery = iquery.Where(x => x.descripcion.ToUpper().Contains(descripcion.ToUpper()));
                }
                if (codigoLicencia != null)
                {
                    iquery = iquery.Where(x => x.codigoLicencia.ToUpper().Contains(codigoLicencia.ToUpper()));
                }
                if (estadoNoveda != 0)
                {
                    iquery = iquery.Where(x => x.idEstado == estadoNoveda);
                }
                //Ordeno
                
                cantidadDeRegistros = iquery.Count();
                var lst = iquery.Skip(skip).Take(tamano).ToList();

                if (sortColumn != "" && sortColumnDir != "")
                {
                    if (sortColumnDir == "asc")
                    {
                        switch (sortColumn)
                        {
                            case "codigoNovedad":
                                lst = lst.OrderBy(x => x.codigoNovedad).ToList();
                                break;
                            case "fecha":
                                lst = lst.OrderBy(x => x.fecha).ThenBy(x => x.hora).ToList();
                                break;
                            case "hora":
                                lst = lst.OrderBy(x => x.hora).ThenBy(x => x.fecha).ToList();
                                break;
                            case "codigoLicencia":
                                lst = lst.OrderBy(x => x.codigoLicencia).ToList();
                                break;
                            case "idActuacion":
                                lst = lst.OrderBy(x => x.idActuacion).ToList();
                                break;
                            case "nombreNovedad":
                                lst = lst.OrderBy(x => x.nombreNovedad).ToList();
                                break;
                            case "descripcion":
                                lst = lst.OrderBy(x => x.descripcion).ToList();
                                break;
                            case "nombreEstado":
                                lst = lst.OrderBy(x => x.nombreEstado).ToList();
                                break;
                            case "detalleNovedad.fechaHoraNovedad":
                                lst = lst.OrderBy(x => x.fecha).ThenBy(x => x.hora).ToList();
                                break;
                        }
                    }
                    if (sortColumnDir == "desc")
                    {
                        switch (sortColumn)
                        {
                            case "codigoNovedad":
                                lst = lst.OrderByDescending(x => x.codigoNovedad).ToList();
                                break;
                            case "fecha":
                                lst = lst.OrderByDescending(x => x.fecha).ThenByDescending(x => x.hora).ToList();
                                break;
                            case "hora":
                                lst = lst.OrderByDescending(x => x.hora).ThenBy(x => x.fecha).ToList();
                                break;
                            case "codigoLicencia":
                                lst = lst.OrderByDescending(x => x.codigoLicencia).ToList();
                                break;
                            case "idActuacion":
                                lst = lst.OrderByDescending(x => x.idActuacion).ToList();
                                break;
                            case "nombreNovedad":
                                lst = lst.OrderByDescending(x => x.nombreNovedad).ToList();
                                break;
                            case "descripcion":
                                lst = lst.OrderByDescending(x => x.descripcion).ToList();
                                break;
                            case "nombreEstado":
                                lst = lst.OrderByDescending(x => x.nombreEstado).ToList();
                                break;

                            case "detalleNovedad.fechaHoraNovedad":
                                lst = lst.OrderByDescending(x => x.fecha).ThenByDescending(x => x.hora).ToList();
                                break;
                        }
                    }
                    foreach (var item in lst)
                    {
                        item.detalleNovedad = ConsultarUnaNovedad(item.idNovedad, activo);
                    }
                }
                return lst;
            }
        }

        public IndicadoresNovedadVM ObtenerIndicadoresGrillaNovedad(IQueryable<GrillaNovedadVM> iquery2, bool activo, DateTime fechaDesde, DateTime fechaHasta, int idTipoNovedad = 0,
            int idActuacion = 0, string descripcion = " ")
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                IQueryable<GrillaNovedadVM> iquery = from novedad in db.NovNovedad
                                                     join tipoNovedad in db.NovTipoNovedad on novedad.idTipoNovedad equals tipoNovedad.idTipoNovedad
                                                     join estadoNovedad in db.NovEstadoNovedad on novedad.idEstadoNovedad equals estadoNovedad.idEstadoNovedad
                                                     join licencia in db.LicLicencia on novedad.idLicencia equals licencia.idLicencia into lic
                                                     // from act in db.NovActuacionEstado
                                                     from licen in lic.DefaultIfEmpty()
                                                     where novedad.fechaNovedad <= fechaHasta && novedad.fechaNovedad >= fechaDesde
                                                     select new GrillaNovedadVM
                                                     {
                                                         codigoTipoNovedad = tipoNovedad.codigoTipoNovedad,
                                                         activo = novedad.activo,
                                                         idNovedad = novedad.idNovedad,
                                                         codigoNovedad = novedad.codNovedad,
                                                         descripcion = novedad.descripcionNovedad,
                                                         idEstado = estadoNovedad.idEstadoNovedad,
                                                         idLicencia = licen.idLicencia,
                                                         codigoLicencia = licen.codigoLicencia,
                                                         idTipoNovedad = novedad.idTipoNovedad,
                                                         fecha = novedad.fechaNovedad,
                                                         hora = novedad.horaNovedad,
                                                         nombreNovedad = tipoNovedad.nombreTipoNovedad,
                                                         nombreEstado = estadoNovedad.estadoNovedad,

                                                     };


                //Filtros
                if (activo == false)
                {
                    iquery = iquery.Where(x => x.activo == false);
                }
                else
                {
                    iquery = iquery.Where(x => x.activo == true);
                }
                if (idTipoNovedad > 0)
                {
                    iquery = iquery.Where(x => x.idTipoNovedad == idTipoNovedad);
                }

                if (descripcion != null)
                {
                    iquery = iquery.Where(x => x.descripcion.ToUpper().Contains(descripcion.ToUpper()));
                }


                //foreach (var lic in rutes)
                //{
                //    var nov = db.NovNovedad.Where(x => x.idLicencia == lic.idLicencia && x.activo == true).FirstOrDefault();
                //    if (nov != null)
                //    {
                //        licenciasAbiertas++;
                //    }
                //}
                var licenciasAbiertas = 0;
                var rutes = db.LicLicencia.Where(x => x.activo == true && x.idEstadoLicencia == 1).ToList();
                if (rutes != null)
                {
                    foreach (var item in rutes)
                    {
                        var licNov = db.NovNovedad.Where(x => x.idLicencia == item.idLicencia && x.activo == true && x.idEstadoNovedad == 1).FirstOrDefault();
                        if (licNov != null)
                        {
                            licenciasAbiertas++;
                        }
                    }

                    //List<LicenciaVM> lista = new List<LicenciaVM>();
                    //lista = (from l in db.LicLicencia
                    //         join n in db.NovNovedad on l.idLicencia equals n.idLicencia
                    //         where n.activo == true && l.activo == true
                    //         select new { n, l }).AsEnumerable().Select(x => new LicenciaVM
                    //         {
                    //             idLicencia = x.l.idLicencia,
                    //             codigoLicencia = x.l.codigoLicencia,
                    //             fechaHoraInicioLicencia = x.l.fechaHoraInicioLicencia,
                    //             descripcionLicencia = x.l.descripcionLicencia,
                    //             idNovedad = x.n.idNovedad,

                    //         }).ToList();

                    // licenciasAbiertas = lista.Count();                   
                }


                IndicadoresNovedadVM indicadores = new IndicadoresNovedadVM();

                indicadores.CantidadNovedad = iquery.Count();
                indicadores.CantidadNovedadesAbiertas = iquery.Where(x => x.idEstado == 1).Count();
                indicadores.CantidadNovedadesCerradas = iquery.Where(x => x.idEstado == 2).Count();
                indicadores.CantidadRUTE = licenciasAbiertas;

                return indicadores;
            }
        }

        public IndicadoresNovedadVM ObtenerIndicadoresNovedad(bool activo, string fechaDesde, string fechaHasta, string descripcion, int idEquipo, string nombreEquipo, int idTipoEquipo, int tipoNovedad, int actuacion, int propiedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())

            {
                IndicadoresNovedadVM indicadoresNovedadVM = new IndicadoresNovedadVM();

                indicadoresNovedadVM.CantidadNovedad = 0;
                indicadoresNovedadVM.CantidadNovedadesAbiertas = 0;
                indicadoresNovedadVM.CantidadNovedadesCerradas = 0;
                indicadoresNovedadVM.CantidadRUTE = 0;


                IQueryable<NovedadVM> query = (from c in db.NovNovedad
                                               join d in db.NovEstadoNovedad on c.idEstadoNovedad equals d.idEstadoNovedad
                                               join e in db.NovTipoNovedad on c.idTipoNovedad equals e.idTipoNovedad
                                               join l in db.LicLicencia on c.idLicencia equals l.idLicencia into ps
                                               from l in ps.DefaultIfEmpty()

                                               where c.activo == activo
                                               select new { c, d, e, l }).AsEnumerable().Select(x => new NovedadVM
                                               {
                                                   idNovedad = x.c.idNovedad,
                                                   fechaNovedad = x.c.fechaNovedad,
                                                   activo = x.c.activo,
                                                   idTipoNovedad = x.c.idTipoNovedad,
                                                   idLicencia = x.c.idLicencia,

                                                   tipoNovedad = new TipoNovedadVM
                                                   {
                                                       idTipoNovedad = x.c.idTipoNovedad,
                                                       nombreTipoNovedad = x.e.nombreTipoNovedad
                                                   },
                                                   estadoNovedad = new EstadoNovedadVM
                                                   {
                                                       idEstadoNovedad = x.d.idEstadoNovedad,
                                                       nombreEstadoNovedad = x.d.estadoNovedad,
                                                       activo = x.d.activo
                                                   },
                                                   licencia = new LicenciaVM
                                                   {
                                                       idLicencia = x.l == null ? 0 : x.l.idLicencia,
                                                       codigoLicencia = x.l == null ? "" : x.l.codigoLicencia
                                                   },



                                               }).AsQueryable();
                try
                {
                    var queryFiltrada = FiltrarQuery(query, fechaDesde, fechaHasta, idEquipo, idTipoEquipo, tipoNovedad, actuacion, propiedad);

                    if (descripcion != null)
                    {
                        queryFiltrada = queryFiltrada.Where(x => x.descripcionNovedad.ToUpper().Contains(descripcion.ToUpper()));
                    }
                    if (nombreEquipo != null)
                    {
                        queryFiltrada = queryFiltrada.Where(x => x.equipo.nombreEquipo.ToUpper().Contains(nombreEquipo.ToUpper()));
                    }

                    queryFiltrada = queryFiltrada.OrderByDescending(x => x.idNovedad);
                    var queryNovedadesAbiertas = queryFiltrada.Where(x => x.estadoNovedad.idEstadoNovedad == 1);
                    var queryNovedadesCerradas = queryFiltrada.Where(x => x.estadoNovedad.idEstadoNovedad == 2);

                    var queryCantidadRUTE = queryFiltrada.Where(x => x.licencia.estadoLicencia.idEstadoLicencia == 1);



                    indicadoresNovedadVM.CantidadNovedad = queryFiltrada.Count();

                    indicadoresNovedadVM.CantidadNovedadesAbiertas = queryNovedadesAbiertas.Count();

                    indicadoresNovedadVM.CantidadNovedadesCerradas = queryNovedadesCerradas.Count();

                    indicadoresNovedadVM.CantidadRUTE = queryCantidadRUTE.Count();

                    return indicadoresNovedadVM;

                }
                catch (Exception)
                {

                    return indicadoresNovedadVM;
                }

            }

        }

        public object ObtenerLibroNovedad(bool activo, string fechaDesde, string fechaHasta, string descripcion, bool descCompleta, int idEquipo, string nombreEquipo, int idTipoEquipo, int tipoNovedad, int actuacion, int propiedad, string draw, int recordsTotal, int skip, int pageSize)
        {
            ServicioEquipo servEquipo = new ServicioEquipo();

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                IQueryable<NovedadVM> query = (from c in db.NovNovedad
                                               join d in db.NovEstadoNovedad on c.idEstadoNovedad equals d.idEstadoNovedad
                                               join e in db.NovTipoNovedad on c.idTipoNovedad equals e.idTipoNovedad
                                               join l in db.LicLicencia on c.idLicencia equals l.idLicencia into ps
                                               from l in ps.DefaultIfEmpty()

                                               where c.activo == activo
                                               select new { c, d, e, l }).AsEnumerable().Select(x => new NovedadVM
                                               {
                                                   idNovedad = x.c.idNovedad,
                                                   descripcionNovedad = !descCompleta ? (x.c.descripcionNovedad.Length <= 50 ? x.c.descripcionNovedad : x.c.descripcionNovedad.Substring(0, 50)) : x.c.descripcionNovedad,
                                                   fechaNovedad = x.c.fechaNovedad.Value.AddHours(x.c.horaNovedad.Value.Hour).AddMinutes(x.c.horaNovedad.Value.Minute),
                                                   horaNovedad = x.c.horaNovedad,
                                                   fechaHoraNovedad = String.Format("{0:dd/MM/yyyy}", x.c.fechaNovedad) + " " + String.Format("{0:HH:mm}", x.c.horaNovedad),
                                                   fechaParseada = String.Format("{0:dd/MM/yyyy}", x.c.fechaNovedad),
                                                   horaParseada = String.Format("{0:HH:mm}", x.c.horaNovedad),
                                                   fechaVisible = String.Format("{0:dd/MM/yyyy}", x.c.fechaNovedad),
                                                   idUsuarioAlta = x.c.idUsuarioAlta,
                                                   activo = x.c.activo,
                                                   idTipoNovedad = x.c.idTipoNovedad,
                                                   idLicencia = x.c.idLicencia,
                                                   idNovedadRelacionada = x.c.idNovRelacionada,
                                                   CodigoNovedad = x.c.codNovedad,
                                                   codigoTipoNovedad = x.e.codigoTipoNovedad,
                                                   tipoNovedad = new TipoNovedadVM
                                                   {
                                                       idTipoNovedad = x.c.idTipoNovedad,
                                                       nombreTipoNovedad = x.e.nombreTipoNovedad
                                                   },
                                                   estadoNovedad = new EstadoNovedadVM
                                                   {
                                                       idEstadoNovedad = x.d.idEstadoNovedad,
                                                       nombreEstadoNovedad = x.d.estadoNovedad
                                                   },
                                                   licencia = new LicenciaVM
                                                   {
                                                       idLicencia = x.l == null ? 0 : x.l.idLicencia,
                                                       codigoLicencia = x.l == null ? "" : x.l.codigoLicencia
                                                   },


                                               }).AsQueryable();

                query = query.OrderByDescending(x => x.fechaNovedad);
                var queryFiltrada = FiltrarQuery(query, fechaDesde, fechaHasta, idEquipo, idTipoEquipo, tipoNovedad, actuacion, propiedad);
                //queryFiltrada = queryFiltrada.OrderByDescending(x => x.fechaHoraNovedad);

                if (descripcion != null)
                {
                    queryFiltrada = queryFiltrada.Where(x => x.descripcionNovedad.ToUpper().Contains(descripcion.ToUpper()));
                }
                if (nombreEquipo != null)
                {
                    queryFiltrada = queryFiltrada.Where(x => x.equipo.nombreEquipo.ToUpper().Contains(nombreEquipo.ToUpper()));
                }
                recordsTotal = queryFiltrada.Count();
                var toTake = pageSize;
                if (recordsTotal < pageSize)
                {
                    toTake = recordsTotal;
                }
                var lst = queryFiltrada.Skip(skip).Take(toTake).ToList();
                List<NovedadVM> listaDetallada = new List<NovedadVM>();
                foreach (var item in lst)
                {
                    listaDetallada.Add(ConsultarUnaNovedad(item.idNovedad, activo));
                }

                object json = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = listaDetallada };
                return json;
            }
        }


        public NovedadVM ObtenerTipoNovedad(int idNovedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                NovedadVM novedad = new NovedadVM();

                var estaNovedad = db.NovNovedad.Where(x => x.idNovedad == idNovedad).FirstOrDefault();
                if (estaNovedad != null)
                {
                    switch (estaNovedad.idTipoNovedad)
                    {
                        case 1004:
                            var novTipoCentro = db.NovCentroControl.FirstOrDefault(x => x.idNovedad == idNovedad);
                            novedad.idTipoEquipo = 5;
                            novedad.idEquipo = novTipoCentro.idEquipo;
                            break;
                        case 1005:
                            var novEstacion = db.NovEstacion.FirstOrDefault(x => x.idNovedad == idNovedad);
                            novedad.idTipoEquipo = novEstacion.idTipoEquipo;
                            novedad.idEquipo = novEstacion.idEquipo;
                            break;
                        case 1006:
                            var novLinea = db.NovLinea.FirstOrDefault(x => x.idNovedad == idNovedad);
                            novedad.idTipoEquipo = 4;
                            novedad.idEquipo = novLinea.idLinea;
                            break;
                        case 1007:
                            var novAutomatismo = db.NovAutomatismo.FirstOrDefault(x => x.idNovedad == idNovedad);
                            break;
                        case 1008:
                            var novReclamo = db.NovReclamo.FirstOrDefault(x => x.idNovedad == idNovedad);
                            break;
                    }
                }
                return novedad;
            }
        }

        public int buscarTurnoEjecucion(int idUsuarioLogueado)
        {
            using (BuhoGestionEntities buhoGestion = new BuhoGestionEntities())
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    var turnoEjecucion = db.TurnTurnoEjecucion.Where(x => x.fechaHoraApertura != null && x.fechaHoraCierre == null).ToList();
                    if (turnoEjecucion.Count > 0)
                    {
                        foreach (var turno in turnoEjecucion)
                        {
                            if (turno.idJefeTurno == idUsuarioLogueado)
                            {
                                return turno.idTurnoEjecucion;
                            }
                            else
                            {
                                var acompañantes = db.TurnTurnoAcompañante.Where(x => x.idTurnoEjecucion == turno.idTurnoEjecucion).ToList();
                                if (acompañantes.Count > 0)
                                {
                                    foreach (var ac in acompañantes)
                                    {
                                        if (ac.idAcompañante == idUsuarioLogueado)
                                        {
                                            return turno.idTurnoEjecucion;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        return 0;
                    }
                    return 0;
                }
            }
        }

        public NovedadVM CRUD(NovedadVM novedad, int? idUsuario, string tipoCRUD)
        {
            NovedadVM unaNovedad = new NovedadVM();
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {

                    #region Region Delete
                    if (novedad.tipoCRUD == "D")
                    {
                        var novedadEliminar = db.NovNovedad.Find(novedad.idNovedad);
                        unaNovedad.codigo = EliminarNovedad(novedad);
                        if (unaNovedad.codigo == 200)
                        {
                            unaNovedad.codigo = GenerarHistorialNovedad(novedadEliminar, "Eliminación", idUsuario);
                        }

                        unaNovedad.idNovedad = novedad.idNovedad;
                        unaNovedad.idTipoNovedad = novedad.idTipoNovedad;
                        return unaNovedad;
                    }
                    #endregion


                    var TipoNovedadEquipo = new TipoNovedadVM();
                    if (novedad.codigoTipoNovedad != null)
                    {
                        ServicioTipoNovedad servTipNov = new ServicioTipoNovedad();
                        TipoNovedadEquipo = servTipNov.ConsultarTipoNovedadCodigo(novedad.codigoTipoNovedad);
                    }

                    // generación de códigos
                    DateTime dt = DateTime.Now;

                    var currentYear = Int16.Parse(dt.ToString("yy"));

                    var codNov = GenerarCodigoNovedad();

                    NovNovedad novedadVM = new NovNovedad();
                    novedadVM.idTipoNovedad = novedad.idTipoNovedad;
                    novedadVM.descripcionNovedad = novedad.descripcionNovedad;
                    novedadVM.fechaNovedad = novedad.fechaNovedad;
                    novedadVM.horaNovedad = novedad.horaNovedad;
                    novedadVM.idEstadoNovedad = 1;
                    novedadVM.codNovedad = codNov.CodigoNovedad;
                    novedadVM.NumNovedad = codNov.NumNovedad;
                    novedadVM.anioNovedad = codNov.anioNovedad;
                    novedadVM.activo = true;
                    novedadVM.nota = novedad.nota;
                    novedadVM.idUsuarioAlta = idUsuario;
                    //COMENTO
                    novedadVM.fechaHoraNovedad = DateTime.Now;
                    //novedadVM.idIndisponibilidad = novedad.idIndisponibilidad; //agregado
                    if (novedad.idAutomatismo != null && novedad.idAutomatismo != -1)
                    {
                        novedadVM.idAutomatismo = novedad.idAutomatismo;

                    }




                    //fin generación de códigos
                    if (tipoCRUD == "C")
                    {
                        var idTurnoEjecucion = buscarTurnoEjecucion(Convert.ToInt32(idUsuario));
                        novedadVM.idTurnoEjecucion = idTurnoEjecucion;

                        db.NovNovedad.Add(novedadVM);
                        db.SaveChanges();

                        switch (TipoNovedadEquipo.codigoTipoNovedad)
                        {
                            case "CCON":
                                NovCentroControl novCentroControl = new NovCentroControl
                                {
                                    idCentral = novedad.idCentral,
                                    idNovedad = novedadVM.idNovedad,
                                    idEquipo = novedad.idEquipo,
                                    idCentroControl = novedad.centroControl.idCentroControl,
                                };
                                NovActuacion novActuacion = new NovActuacion
                                {
                                    idNovedad = novedadVM.idNovedad,
                                    idActuacionEstado = novedad.idActuacion,
                                    idTipoActuacion = novedad.idTipoActuacion
                                };
                                if (novedadVM.idAutomatismo != null)
                                {
                                    if (novedad.automatismos != null)
                                    {
                                        if (novedad.automatismos.Count > 0)
                                        {
                                            foreach (var aut in novedad.automatismos)
                                            {
                                                NovAutomatismo novAutomatismo = new NovAutomatismo
                                                {
                                                    idAutomatismo = aut,
                                                    idNovedad = novedadVM.idNovedad
                                                };
                                                db.NovAutomatismo.Add(novAutomatismo);
                                            }
                                        }
                                    }
                                }
                                db.NovActuacion.Add(novActuacion);
                                db.NovCentroControl.Add(novCentroControl);
                                db.SaveChanges();
                                break;

                            case "EST":

                                NovEstacion novEstacion = new NovEstacion
                                {
                                    idEquipo = novedad.idEquipo,
                                    idTipoEquipo = novedad.idTipoEquipo,
                                    idEstacion = novedad.idEstacion,
                                    idNovedad = novedadVM.idNovedad,
                                    alarma = novedad.alarma,
                                    idEquipoAdicional = novedad.idEquipoAdicional,
                                };
                                NovActuacion novActuacion2 = new NovActuacion
                                {
                                    idNovedad = novedadVM.idNovedad,
                                    idActuacionEstado = novedad.idActuacion,
                                    idTipoActuacion = novedad.idTipoActuacion
                                };
                                if (novedadVM.idAutomatismo != null)
                                {
                                    if (novedad.automatismos != null)
                                    {
                                        if (novedad.automatismos.Count > 0)
                                        {
                                            foreach (var aut in novedad.automatismos)
                                            {
                                                NovAutomatismo novAutomatismo = new NovAutomatismo
                                                {
                                                    idAutomatismo = aut,
                                                    idNovedad = novedadVM.idNovedad
                                                };
                                                db.NovAutomatismo.Add(novAutomatismo);
                                            }
                                        }
                                    }
                                }
                                db.NovActuacion.Add(novActuacion2);
                                db.NovEstacion.Add(novEstacion);
                                db.SaveChanges();

                                break;

                            case "LIN":

                                NovLinea novLinea = new NovLinea
                                {
                                    idNovedad = novedadVM.idNovedad,
                                    idLinea = novedad.idEquipo,
                                    idPropiedad = novedad.posesionLinea,
                                    idTipoLinea = novedad.idTipoLinea,
                                    alarma = novedad.alarma,
                                };
                                NovActuacion novActuacion3 = new NovActuacion
                                {
                                    idNovedad = novedadVM.idNovedad,
                                    idActuacionEstado = novedad.idActuacion,
                                    idTipoActuacion = novedad.idTipoActuacion
                                };
                                db.NovActuacion.Add(novActuacion3);
                                db.NovLinea.Add(novLinea);
                                db.SaveChanges();

                                break;

                            case "REC":
                                string numReclamo = ObtenerCodigoReclamo();
                                NovReclamo novReclamo = new NovReclamo
                                {
                                    numReclamo = numReclamo,
                                    idInfraestructura = novedad.idInfraestructura,
                                    idTipoReclamo = novedad.idTipoReclamo,
                                    idNovedad = novedadVM.idNovedad,
                                    idInfraParticular = novedad.idEquipoReclamo,

                                };
                                unaNovedad.numeroReclamo = novReclamo.numReclamo;
                                db.NovReclamo.Add(novReclamo);
                                db.SaveChanges();

                                break;

                        }
                        var TipoEquipo = new EquipoVM();
                        if (novedad.CodigoEquipo != null)
                        {
                            ServicioEquipo servEq = new ServicioEquipo();
                            TipoEquipo = servEq.ConsultarUnEquipo(novedad.CodigoEquipo);

                        }

                        //var tipoNovedad=ObtenerTipoNovedad(novedadVM.idNovedad);

                        //if(tipoNovedad.idTipoEquipo!=null && tipoNovedad.idTipoEquipo!=-1 && tipoNovedad.idEquipo != null && tipoNovedad.idEquipo != -1)
                        //{

                        //    var novedadActuacion = db.NovActuacion.Where(x => x.idNovedad == novedadVM.idNovedad).FirstOrDefault();
                        //    if (novedadActuacion != null)
                        //    {
                        //        if (novedadActuacion.idActuacionEstado != null && novedadActuacion.idActuacionEstado != -1)
                        //        {
                        //            if (TipoEquipo.codigoEquipo == "TRA" || TipoEquipo.codigoEquipo == "PCON" || TipoEquipo.codigoEquipo == "LIN")
                        //            {
                        //                ServicioActuacion servAct = new ServicioActuacion();
                        //                ServicioIndisponibilidad ind = new ServicioIndisponibilidad();
                        //                if (!novedad.entradaIndisponibilidad)
                        //                {
                        //                    var idTipoActEstado = servAct.ConsultarUnaActuacion(novedadActuacion.idActuacionEstado).idTipoActuacionEstado;

                        //                    if (idTipoActEstado == 2) //ver la posibilidad de manejarlo por codigo
                        //                    {
                        //                        novedadVM.idIndisponibilidad = ind.GenerarIndisponibilidad(TipoEquipo.codigoEquipo, novedadVM.idNovedad, novedad.idIndisponibilidad, tipoNovedad.idEquipo, tipoNovedad.idTipoEquipo, !novedad.energiaNoSuministrada);
                        //                        db.SaveChanges();
                        //                    }
                        //                    else if (idTipoActEstado == 1)
                        //                    {
                        //                        unaNovedad.codigo = ind.LiberarIndisponibilidad(novedadVM.idNovedad, true);
                        //                    }
                        //                }                                        
                        //            }
                        //        }
                        //    }
                        //}



                        //retorno estado de alta (junto con historial)
                        unaNovedad.idNovedad = novedadVM.idNovedad;
                        unaNovedad.codigo = GenerarHistorialNovedad(novedadVM, "Alta", idUsuario);
                        unaNovedad.idTipoNovedad = novedadVM.idTipoNovedad;


                    }
                    else if (tipoCRUD == "U")
                    {
                        var consultaunaNovedad = db.NovNovedad.Where(x => x.idNovedad == novedad.idNovedad).FirstOrDefault();
                        if (consultaunaNovedad != null)
                        {
                            consultaunaNovedad.activo = true;
                            consultaunaNovedad.descripcionNovedad = novedadVM.descripcionNovedad;
                            consultaunaNovedad.idEstadoNovedad = novedadVM.idEstadoNovedad;
                            consultaunaNovedad.nota = novedadVM.nota;
                            consultaunaNovedad.fechaNovedad = novedad.fechaNovedad;
                            consultaunaNovedad.horaNovedad = novedad.horaNovedad;


                            if (novedad.idAutomatismo != null && novedad.idAutomatismo != -1)
                            {
                                consultaunaNovedad.idAutomatismo = novedadVM.idAutomatismo;
                            }
                            var tipoUnaNovedad = buscarTipoNovedad(novedad.idNovedad);

                            if (tipoUnaNovedad != "")
                            {
                                switch (tipoUnaNovedad)
                                {
                                    case "CCON":
                                        var ccon = db.NovCentroControl.Where(x => x.idNovedad == novedad.idNovedad).FirstOrDefault();
                                        ccon.idCentral = novedad.idCentral;
                                        ccon.idEquipo = novedad.idEquipo;
                                        ccon.idCentroControl = novedad.centroControl.idCentroControl;

                                        var actuccon = db.NovActuacion.Where(x => x.idNovedad == novedad.idNovedad).FirstOrDefault();
                                        actuccon.idActuacionEstado = novedad.idActuacion;
                                        actuccon.idTipoActuacion = novedad.idTipoActuacion;
                                        if (novedadVM.idAutomatismo != null)
                                        {
                                            if (novedad.automatismos != null)
                                            {
                                                var autViejos = db.NovAutomatismo.Where(x => x.idNovedad == novedad.idNovedad).ToList();
                                                if (autViejos != null)
                                                {
                                                    foreach (var autViejo in autViejos)
                                                    {
                                                        db.NovAutomatismo.Remove(autViejo);
                                                    }
                                                }
                                                if (novedad.automatismos.Count > 0)
                                                {
                                                    foreach (var auto in novedad.automatismos)
                                                    {
                                                        NovAutomatismo novAutomatismo = new NovAutomatismo
                                                        {
                                                            idAutomatismo = auto,
                                                            idNovedad = novedad.idNovedad
                                                        };
                                                        db.NovAutomatismo.Add(novAutomatismo);
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    case "LIN":

                                        var linea = db.NovLinea.Where(x => x.idNovedad == novedad.idNovedad).FirstOrDefault();
                                        linea.idLinea = novedad.idEquipo;
                                        linea.idTipoLinea = novedad.idTipoLinea;
                                        linea.idPropiedad = novedad.posesionLinea;
                                        linea.alarma = novedad.alarma;

                                        var actulinea = db.NovActuacion.Where(x => x.idNovedad == novedad.idNovedad).FirstOrDefault();
                                        actulinea.idActuacionEstado = novedad.idActuacion;
                                        actulinea.idTipoActuacion = novedad.idTipoActuacion;

                                        break;
                                    case "AUT":
                                        var aut = db.NovAutomatismo.Where(x => x.idNovedad == novedad.idNovedad).FirstOrDefault();
                                        aut.idAutomatismo = novedad.idAutomatismo;
                                        aut.idDetalleAutomatismo = novedad.idDetalleAutomatismo;

                                        break;
                                    case "REC":
                                        var rec = db.NovReclamo.Where(x => x.idNovedad == novedad.idNovedad).FirstOrDefault();
                                        rec.idInfraestructura = novedad.idInfraestructura;
                                        rec.idTipoReclamo = novedad.idTipoReclamo;
                                        rec.idInfraParticular = novedad.idEquipoReclamo;

                                        break;
                                    case "EST":
                                        var est = db.NovEstacion.Where(x => x.idNovedad == novedad.idNovedad).FirstOrDefault();
                                        est.idEquipo = novedad.idEquipo;
                                        est.idTipoEquipo = novedad.idTipoEquipo;
                                        est.idEstacion = novedad.idEstacion;
                                        est.alarma = novedad.alarma;
                                        est.idEquipoAdicional = novedad.idEquipoAdicional;

                                        var actuest = db.NovActuacion.Where(x => x.idNovedad == novedad.idNovedad).FirstOrDefault();
                                        actuest.idActuacionEstado = novedad.idActuacion;
                                        actuest.idTipoActuacion = novedad.idTipoActuacion;
                                        if (novedadVM.idAutomatismo != null)
                                        {
                                            if (novedad.automatismos != null)
                                            {
                                                var autViejos = db.NovAutomatismo.Where(x => x.idNovedad == novedad.idNovedad).ToList();
                                                if (autViejos != null)
                                                {
                                                    foreach (var autViejo in autViejos)
                                                    {
                                                        db.NovAutomatismo.Remove(autViejo);
                                                    }
                                                }
                                                if (novedad.automatismos.Count > 0)
                                                {
                                                    foreach (var auto in novedad.automatismos)
                                                    {
                                                        NovAutomatismo novAutomatismo = new NovAutomatismo
                                                        {
                                                            idAutomatismo = auto,
                                                            idNovedad = novedad.idNovedad
                                                        };
                                                        db.NovAutomatismo.Add(novAutomatismo);
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                }
                                db.SaveChanges();
                            }


                            //#region Indisponibilidad
                            //var TipoEquipo = new EquipoVM();
                            //if (novedad.CodigoEquipo != null)
                            //{
                            //    ServicioEquipo servEq = new ServicioEquipo();
                            //    TipoEquipo = servEq.ConsultarUnEquipo(novedad.CodigoEquipo);

                            //}

                            //var tipoNovedad = ObtenerTipoNovedad(novedad.idNovedad);
                            ////verifico si tiene indisponibilidad
                            //if (consultaunaNovedad.idIndisponibilidad != null)
                            //{
                            //    if (consultaunaNovedad.idIndisponibilidad != novedad.idIndisponibilidad || tipoNovedad.idEquipo != novedad.idEquipo)
                            //    {
                            //        ServicioIndisponibilidad ind = new ServicioIndisponibilidad();
                            //        var tipoIndisp = ind.ConsultarIndPorNovedad(consultaunaNovedad.idNovedad).idTipoIndisponibilidad;
                            //        if (tipoIndisp != novedad.idIndisponibilidad)
                            //        {
                            //            var idIndispGenerada = ind.GenerarIndisponibilidad(TipoEquipo.codigoEquipo, consultaunaNovedad.idNovedad, novedad.idIndisponibilidad, novedad.idEquipo, novedad.idTipoEquipo, !novedad.energiaNoSuministrada);
                            //            unaNovedad.codigo = idIndispGenerada;
                            //            //si se generó correctamente //todo ver cambiar el código
                            //            if (unaNovedad.codigo != -1)
                            //            {
                            //                //doy de baja logica la indisponibilidad actual
                            //                unaNovedad.codigo = ind.LiberarIndisponibilidad(consultaunaNovedad.idNovedad, false);
                            //                consultaunaNovedad.idIndisponibilidad = idIndispGenerada;
                            //            }
                            //        }
                            //    }
                            //}
                            //#endregion
                            consultaunaNovedad.idTipoNovedad = novedadVM.idTipoNovedad;

                            db.SaveChanges();

                            unaNovedad.idNovedad = consultaunaNovedad.idNovedad;
                            unaNovedad.codigo = GenerarHistorialNovedad(consultaunaNovedad, "Modificación", idUsuario);
                            unaNovedad.idTipoNovedad = novedadVM.idTipoNovedad;
                            if (consultaunaNovedad.idNovRelacionada != null)
                            {
                                //verifico si tiene novedades relacionadas
                                var RamaNovedad = ObtenerRamaCompletaDeNovedad(consultaunaNovedad.idNovedad).OrderBy(x => x.idNovedad).ToList();
                                //   var NovBaseAct = db.NovActuacion.Where(x => x.idNovedad == consultaunaNovedad.idNovedad).FirstOrDefault(); //Actuaciones de la Base

                                if (RamaNovedad != null)
                                {
                                    //si tiene novedades relacionadas, modifico sus encabezados
                                    foreach (var item in RamaNovedad)
                                    {
                                        //verifico que no sea la novedad actual porque ya se modificó
                                        if (item.idNovedad != consultaunaNovedad.idNovedad)
                                        {
                                            //   var NovActHija = db.NovActuacion.Where(x => x.idNovedad == item.idNovedad).FirstOrDefault(); //Busco las novedades hijas y le asigno lo de Actuaciones
                                            var idNovedadRama = item.idNovedad;
                                            var novedadRelacionada = db.NovNovedad.Where(x => x.idNovedad == idNovedadRama).FirstOrDefault();
                                            // novedadRelacionada.activo = true;
                                            //novedadRelacionada.descripcionNovedad = novedadVM.descripcionNovedad;
                                            //   novedadRelacionada.idEstadoNovedad = novedadVM.idEstadoNovedad;
                                            //novedadRelacionada.idIndisponibilidad = nov.idIndisponibilidad;
                                            novedadRelacionada.idTipoNovedad = novedadVM.idTipoNovedad;
                                            //   novedadRelacionada.idAutomatismo = consultaunaNovedad.idAutomatismo;
                                            //   NovActHija.idActuacionEstado = NovBaseAct.idActuacionEstado;
                                            //  NovActHija.idTipoActuacion = NovBaseAct.idTipoActuacion;

                                            db.SaveChanges();
                                            unaNovedad.codigo = GenerarHistorialNovedad(novedadRelacionada, "Modificación", idUsuario);

                                        }

                                    }

                                }
                            }

                        }
                    }

                    return unaNovedad;

                }
            }
            catch (Exception e)
            {
                var a = e.GetBaseException();
                unaNovedad.idNovedad = 0;
                unaNovedad.codigo = 400;

                return unaNovedad;
            }

        }


        public string buscarTipoNovedad(int idNovedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {

                var ccon = db.NovCentroControl.Where(x => x.idNovedad == idNovedad).FirstOrDefault();
                var estacion = db.NovEstacion.Where(x => x.idNovedad == idNovedad).FirstOrDefault();
                var rec = db.NovReclamo.Where(x => x.idNovedad == idNovedad).FirstOrDefault();
                var linea = db.NovLinea.Where(x => x.idNovedad == idNovedad).FirstOrDefault();
                var aut = db.NovAutomatismo.Where(x => x.idNovedad == idNovedad).FirstOrDefault();

                if (ccon != null)
                {
                    return "CCON";
                }
                if (estacion != null)
                {
                    return "EST";
                }
                if (linea != null)
                {
                    return "LIN";
                }
                if (rec != null)
                {
                    return "REC";
                }
                if (aut != null)
                {
                    return "AUT";
                }
                return "";
            }
        }


        public int RelacionarRuteNovedad(int idLicLicenciaUsuario, int idNovedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                try
                {
                    var licenciaUsuario = db.LicLicenciaUsuario.Where(x => x.idLicLicenciaUsuario == idLicLicenciaUsuario).FirstOrDefault();
                    var novedad = db.NovNovedad.Where(x => x.idNovedad == idNovedad).FirstOrDefault();
                    novedad.idLicencia = licenciaUsuario.idLicLicencia;
                    db.LicLicenciaUsuario.Remove(licenciaUsuario);
                    db.SaveChanges();
                    return 1;
                }
                catch
                {
                    return 0;
                }
            }
        }

        public int EliminarNovedad(NovedadVM novedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunaNovedad = db.NovNovedad.Where(x => x.idNovedad == novedad.idNovedad).FirstOrDefault();
                if (consultaunaNovedad.idNovRelacionada != null)
                {
                    var ramaCompleta = ObtenerRamaCompletaDeNovedad(novedad.idNovedad);
                    var hijo = ramaCompleta.Where(x => x.idNovedad == consultaunaNovedad.idNovRelacionada).FirstOrDefault();
                    var padre = ramaCompleta.Where(x => x.idNovedadRelacionada == consultaunaNovedad.idNovedad).FirstOrDefault();

                    consultaunaNovedad.activo = false;

                    if (padre != null)
                    {
                        var setPadre = db.NovNovedad.Where(x => x.idNovedad == padre.idNovedad).FirstOrDefault();
                        if (hijo != null)
                        {
                            setPadre.idNovRelacionada = hijo.idNovedad;

                        }
                    }
                    db.SaveChanges();

                }
                else
                {
                    consultaunaNovedad.activo = false;
                    db.SaveChanges();
                }
            }
            return 200;
        }

        public int CerrarNovedad(int idNovedad)

        {

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunaNovedad = db.NovNovedad.Find(idNovedad);

                if (consultaunaNovedad != null)
                {
                    var novedadACopiar = ConsultarUnaNovedad(idNovedad, true);
                    novedadACopiar.idNovedadOrigen = idNovedad;
                    //novedadACopiar.entradaIndisponibilidad = true;

                    novedadACopiar.horaNovedad = DateTime.Now;
                    novedadACopiar.fechaNovedad = DateTime.Today;
                    novedadACopiar.fechaHoraNovedad = DateTime.Now.ToString();

                    var licencia = db.LicLicencia.Find(consultaunaNovedad.idLicencia);
                    var tipoNovedad = ObtenerTipoNovedad(idNovedad);
                    if (licencia != null)
                    {
                        if (licencia.idEstadoLicencia == 2)
                        {

                            consultaunaNovedad.idEstadoNovedad = 2;


                            //Aca hay que corregir

                            //con esta porcion de codigo estamos liberando la indisponibilidad




                            //if (tipoNovedad.idEquipo != null)
                            //{
                            //    ServicioIndisponibilidad servInd = new ServicioIndisponibilidad();
                            //    servInd.LiberarIndisponibilidad(idNovedad, true);
                            //}

                            //aca deberiamos guardar en el historial qué sucedió

                            var codCierreNovedad = GenerarHistorialNovedad(consultaunaNovedad, "Cierre", 0); //pongo cero hasta que encuentre el usuario actual sin sesion

                            if (codCierreNovedad == 200)
                            {
                                db.SaveChanges();
                                return 200; // todo ok
                            }

                            return 400; // todo ok
                        }
                        else
                        {
                            var listaAutor = db.LicAutorizacionTrabajo.Where(x =>
                           x.idLicencia == licencia.idLicencia && x.activo == true).ToList();
                            if (listaAutor != null)
                            {
                                foreach (var item in listaAutor)
                                {
                                    if (item.idEstadoActual == 1)
                                    {
                                        return 350; //tiene al menos una autorizacion abierta
                                    }
                                }
                            }
                            return 300; // tiene licencia y no está cerrada
                        }
                    }

                    consultaunaNovedad.idEstadoNovedad = 2;

                    //Aca hay que corregir
                    //aca manejo la indisponibilidad 
                    //if (tipoNovedad.idEquipo != null)
                    //{
                    //    ServicioIndisponibilidad servInd = new ServicioIndisponibilidad();
                    //    servInd.LiberarIndisponibilidad(idNovedad, true);
                    //}

                    //aca deberiamos guardar en el historial qué sucedió
                    var codigoCierreNovedad = GenerarHistorialNovedad(consultaunaNovedad, "Cierre", 0); //pongo cero hasta que encuentre el usuario actual sin sesion
                    if (codigoCierreNovedad == 200)
                    {
                        db.SaveChanges();
                        return 250; // todo ok pero no tenia licencia
                    }
                    return 400;

                }
                else
                {
                    //error en el cierre
                    return 400;
                }

            }


        }

        public bool ValidarNovedadRelacionada(int idNovedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunaNovedad = db.NovNovedad.Find(idNovedad);
                if (consultaunaNovedad != null)
                {
                    if (consultaunaNovedad.idEstadoNovedad == 1)
                    {
                        NovedadVM novedadActualHijos = new NovedadVM();
                        NovedadVM novedadActualPadres = new NovedadVM();

                        var hijos = ObtenerListaHijosNovedad(idNovedad, novedadActualHijos).NovedadesNodos;
                        if (hijos != null)
                        {
                            foreach (var item in hijos)
                            {
                                var estadoNovedad = db.NovNovedad.Find(item.idNovedad).idEstadoNovedad;
                                if (estadoNovedad == 1)
                                {
                                    return false; // existe al menos una de las novedades hija abiertas
                                }
                            }
                        }

                        var padres = ObtenerListaPadresNovedad(idNovedad, novedadActualPadres).NovedadesNodos;
                        if (padres != null)
                        {
                            foreach (var item in padres)
                            {
                                var estadoNovedad = db.NovNovedad.Find(item.idNovedad).idEstadoNovedad;
                                if (estadoNovedad == 1)
                                {
                                    return false; // existe al menos una de las novedades padre abiertas
                                }
                            }
                        }

                        return true; //no tiene novedades relacionadas o las que tiene las tiene cerradas a todas

                    }
                }
                return false; //no encontró novedad
            }
        }

        public List<NovedadVM> ObtenerRamaCompletaDeNovedad(int? idNovedad)
        {
            List<NovedadVM> ramaEntera = new List<NovedadVM>();
            ServicioUsuario servUsuario = new ServicioUsuario();
            var novedadActual = ConsultarUnaNovedad(idNovedad, true);
            if (novedadActual != null)
            {
                ramaEntera.Add(novedadActual);
            }
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunaNovedad = db.NovNovedad.Find(idNovedad);
                if (consultaunaNovedad != null)
                {
                    NovedadVM novedadActualHijos = new NovedadVM();
                    NovedadVM novedadActualPadres = new NovedadVM();
                    var hijos = ObtenerListaHijosNovedad(idNovedad, novedadActualHijos).NovedadesNodos;
                    if (hijos != null)
                    {
                        foreach (var item in hijos)
                        {
                            ramaEntera.Add(item);
                        }
                    }
                    var padres = ObtenerListaPadresNovedad(idNovedad, novedadActualPadres).NovedadesNodos;
                    if (padres != null)
                    {
                        foreach (var item in padres)
                        {
                            ramaEntera.Add(item);
                        }
                    }

                    //foreach (var nov in ramaEntera)
                    //{
                    //    var nombreUsuarioAlta = servUsuario.ObtenerDatosUsuarioPorId(nov.idUsuarioAlta);

                    //    nov.nombreUsuarioAlta = nombreUsuarioAlta.nombreUsuario;
                    //}

                }
            }
            var ramaOrdenada = ramaEntera.OrderBy(x => x.idNovedad).ToList();
            return ramaOrdenada;
        }

        public NovedadVM ObtenerLaUltimaNovedadRelacionada(int idNovedad)
        {
            try
            {
                var novedadVacia = new NovedadVM();
                var ObtenerRamaPadre = ObtenerListaPadresNovedad(idNovedad, novedadVacia);
                if (ObtenerRamaPadre.NovedadesNodos != null)
                {
                    var ObtenerPrimeraNovedad = ObtenerRamaPadre.NovedadesNodos.LastOrDefault();
                    var RamaCompleta = ObtenerRamaCompletaDeNovedad(ObtenerPrimeraNovedad.idNovedad);
                    var UltimaRama = RamaCompleta.LastOrDefault();
                    return UltimaRama;
                }
                else
                {
                    var ObtenerRamaHija = ObtenerListaHijosNovedad(idNovedad, novedadVacia);
                    if (ObtenerRamaHija.NovedadesNodos == null)
                    {
                        return null;
                    }
                    else
                    {
                        var ultimoHijo = ObtenerRamaHija.NovedadesNodos.LastOrDefault();
                        return ultimoHijo;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
                return null;
            }
        }

        private NovedadVM ObtenerListaPadresNovedad(int? idNovedad, NovedadVM novedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var padre = (from n in db.NovNovedad
                             where n.activo == true && idNovedad == n.idNovRelacionada
                             select n).FirstOrDefault();
                if (padre != null)
                {
                    var novedadPadreCompleta = ConsultarUnaNovedad(padre.idNovedad, true);
                    if (novedad.NovedadesNodos == null)
                    {
                        novedad.NovedadesNodos = new List<NovedadVM>();
                    }
                    novedad.NovedadesNodos.Add(novedadPadreCompleta);
                    if (novedadPadreCompleta.idNovedadRelacionada != null)
                    {
                        ObtenerListaPadresNovedad(novedadPadreCompleta.idNovedad, novedad);
                    }
                }

                return novedad;
            }
        }

        private NovedadVM ObtenerListaHijosNovedad(int? idNovedad, NovedadVM novedad)
        {

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarNovedad = db.NovNovedad.Find(idNovedad);
                var hijo = (from n in db.NovNovedad
                            where n.activo == true && n.idNovedad == consultarNovedad.idNovRelacionada
                            select n).FirstOrDefault();

                if (hijo != null)
                {
                    var novedadHijaCompleta = ConsultarUnaNovedad(hijo.idNovedad, true);
                    if (novedad.NovedadesNodos == null)
                    {
                        novedad.NovedadesNodos = new List<NovedadVM>();
                    }
                    novedad.NovedadesNodos.Add(novedadHijaCompleta);
                    if (novedadHijaCompleta.idNovedadRelacionada != null)
                    {
                        ObtenerListaHijosNovedad(novedadHijaCompleta.idNovedad, novedad);
                    }
                }

                return novedad;

            }

        }

        public List<NovedadVM> ObtenerNovedadPorFecha(string fecha)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var fechaIndis = Convert.ToDateTime(fecha);
                List<NovedadVM> lst = (from n in db.NovNovedad
                                       where n.activo == true
   && n.fechaNovedad == fechaIndis
                                       select new { n }
                           ).Select(x => new NovedadVM
                           {
                               CodigoNovedad = x.n.codNovedad,
                               descripcionNovedad = x.n.descripcionNovedad,
                               idNovedad = x.n.idNovedad
                           }).ToList();
                return lst;
            }
        }

        public List<TurnoPdfVM> filtrarPdf(List<TurnoPdfVM> turnos, string texto)
        {
            var contadorNovedades = 0;
            var text = "";
            if (texto != null)
            {
                text = texto.ToUpper();
            }
            foreach (var turno in turnos)
            {
                List<NovedadPdfVM> listaFiltrada = new List<NovedadPdfVM>();

                foreach (var novedad in turno.novedades)
                {
                    if (novedad.automatismos != null && novedad.automatismos != "" && novedad.nota != null && novedad.nota != "")
                    {
                        if (novedad.descripcionNovedad.ToUpper().Contains(text) || novedad.nombreActuacion.ToUpper().Contains(text) || novedad.nombreEquipo.ToUpper().Contains(text) || novedad.nota.ToUpper().Contains(text) || novedad.automatismos.ToUpper().Contains(text))
                        {
                            listaFiltrada.Add(novedad);
                        }
                    }
                    else if (novedad.nota != null && novedad.nota != "")
                    {
                        if (novedad.nombreActuacion.ToUpper().Contains(text) || novedad.nombreEquipo.ToUpper().Contains(text) || novedad.nota.ToUpper().Contains(text))
                        {
                            listaFiltrada.Add(novedad);
                        }
                    }
                    else if (novedad.automatismos != null && novedad.automatismos != "")
                    {

                        if (novedad.nombreActuacion.ToUpper().Contains(text) || novedad.nombreEquipo.ToUpper().Contains(text) || novedad.automatismos.ToUpper().Contains(text))
                        {
                            listaFiltrada.Add(novedad);
                        }
                    }
                    else if (novedad.descripcionNovedad != null && novedad.descripcionNovedad != "")
                    {

                        if (novedad.descripcionNovedad.ToUpper().Contains(text) || novedad.nombreActuacion.ToUpper().Contains(text) || novedad.nombreEquipo.ToUpper().Contains(text))
                        {
                            listaFiltrada.Add(novedad);
                        }
                    }
                    else
                    {
                        if (novedad != null)
                        {
                            listaFiltrada.Add(novedad);
                        }
                    }
                    // contadorNovedades++;
                }


                listaFiltrada = listaFiltrada.OrderBy(x => x.horaParseada).ToList();

                turno.novedades = listaFiltrada;
            }
            //var novedadesC = contadorNovedades;  //Hice esto para comparar las novedades con la grilla de novedades para ver si trae bien el calculo

            return turnos;
        }
        public List<TurnoPdfVM> generarPdf(string fechaDesde, string fechaHasta)
        {
            using (BuhoGestionEntities db2 = new BuhoGestionEntities())
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    var fechaHastaString = fechaHasta + " " + "23:59:59";
                    DateTime fechaDesdePdf = Convert.ToDateTime(fechaDesde);
                    DateTime fechaHastaPdf = Convert.ToDateTime(fechaHastaString);
                    List<TurnoPdfVM> lista = (from t in db.TurnTurnoEjecucion
                                              where t.fechaHoraApertura >= fechaDesdePdf && t.fechaHoraApertura <= fechaHastaPdf || t.fechaHoraCierre >= fechaDesdePdf && t.fechaHoraCierre <= fechaHastaPdf
                                              select new { t }).Select(x => new TurnoPdfVM
                                              {
                                                  idTurno = x.t.idTurnoEjecucion,
                                                  idJefeTurno = x.t.idJefeTurno,
                                                  fechaTurno = x.t.fechaHoraApertura,
                                                  estadoTiempo = x.t.estadoTiempo,
                                                  idTurnoEjecucion = x.t.idTurnoEjecucion,
                                                  idTipoTurno = x.t.idTipoTurno,
                                                  fechaHoraCierre = x.t.fechaHoraCierre
                                              }).ToList();

                    foreach (var turno in lista)
                    {
                        var turnoCtr = db2.RecTurnoCtr.Where(x => x.idTurnoCtr == turno.idTipoTurno).FirstOrDefault();
                        if (turnoCtr != null)
                        {
                            turno.horaAperturaStr = turnoCtr.horaEntrada.ToString();
                            turno.horaCierreStr = turnoCtr.horaSalida.ToString();
                            turno.nombreTipoTurno = turnoCtr.nombreTurno;
                        }

                        //Estado del tiempo
                        #region Clima
                        var estTiempo = Convert.ToInt32(turno.estadoTiempo);
                        var tiempo = db.EstadoTiempo.Where(x => x.idEstadoTiempo == estTiempo).FirstOrDefault().nombreEstadoTiempo;
                        turno.estadoTiempo = tiempo;
                        #endregion

                        //Jefe
                        #region Jefe
                        if(turno.idJefeTurno > 0)
                        {
                            var jefe = db2.GralUsuario.Where(x => x.idUsuario == turno.idJefeTurno).FirstOrDefault().Nombre;
                            if (jefe != null)
                            {
                                turno.nombreJefeTurno = jefe;
                            }
                            else
                            {
                                turno.nombreJefeTurno = "";
                            }
                        }
                        
                        #endregion

                        //Acompañantes
                        #region Acompañantes
                        var idAcompañantes = db.TurnTurnoAcompañante.Where(x => x.idTurnoEjecucion == turno.idTurnoEjecucion).ToList();
                        List<string> nombresAc = new List<string>();
                        foreach (var idAc in idAcompañantes)
                        {
                            var nombreac = db2.GralUsuario.Where(x => x.idUsuario == idAc.idAcompañante).FirstOrDefault().Nombre;
                            if (nombreac != null)
                            {
                                nombresAc.Add(nombreac);
                            }
                            else
                            {
                                nombresAc.Add("");
                            }
                        }
                        turno.nombreAcompañantes = nombresAc;
                        #endregion

                        //Novedades
                        var dateTimeSimple = DateTime.Now;
                        dateTimeSimple = (DateTime)turno.fechaTurno;
                        dateTimeSimple.ToShortDateString();
                        //var listaNov = db.NovNovedad.Where(x => x.idTurnoEjecucion == turno.idTurnoEjecucion && x.activo == true && x.fechaNovedad == dateTimeSimple.Date).ToList();
                        var listaNov = db.NovNovedad.Where(x => x.idTurnoEjecucion == turno.idTurnoEjecucion && x.activo == true && x.fechaNovedad >= fechaDesdePdf && x.fechaNovedad <= fechaHastaPdf).ToList();
                        listaNov.OrderByDescending(x => x.horaNovedad);
                        List<NovedadPdfVM> novedades = new List<NovedadPdfVM>();
                        foreach (var nov in listaNov)
                        {

                            switch (nov.idTipoNovedad)
                            {
                                case 1004: //CentroControl
                                    var novccon = db.NovCentroControl.Where(x => x.idNovedad == nov.idNovedad).FirstOrDefault();
                                    if (novccon != null)
                                    {
                                        var nombre = "";
                                        var autString = "";
                                        if (novccon.idCentroControl != null)
                                        {
                                            var centro = db.CConCentroControl.Where(x => x.activo == true && x.idCentroControl == novccon.idCentroControl).FirstOrDefault();
                                            if (centro != null)
                                            {
                                                nombre = centro.nombreCentroControl;
                                            }
                                        }
                                        if (novccon.idCentral != null)
                                        {
                                            var central = db.CConCentral.Where(x => x.activo == true && x.idCentral == novccon.idCentral).FirstOrDefault();
                                            if (central != null)
                                            {
                                                nombre = nombre + " " + central.nombreCentral;
                                            }
                                        }
                                        if (novccon.idEquipo != null)
                                        {
                                            var equipo = db.TranTransformador.Where(x => x.activo == true && x.idTransformador == novccon.idEquipo).FirstOrDefault();
                                            if (equipo != null)
                                            {
                                                nombre = nombre + " " + equipo.nombreTransformador;
                                            }
                                        }
                                        if (nov.idAutomatismo != null && nov.idAutomatismo != -1)
                                        {
                                            var listaAutomatismos = db.NovAutomatismo.Where(x => x.idNovedad == nov.idNovedad).ToList();
                                            if (listaAutomatismos != null)
                                            {
                                                foreach (var automatismo in listaAutomatismos)
                                                {
                                                    var relacion = db.AutRelacionAutomatismo.Where(x => x.idAutoAutomRelacion == automatismo.idAutomatismo).FirstOrDefault();
                                                    if (relacion != null)
                                                    {
                                                        switch (relacion.idTipoEquipo)
                                                        {
                                                            case 1: //Transformador
                                                                var Transformador = db.TranTransformador.Where(x => x.idTransformador == relacion.idEquipo).FirstOrDefault();
                                                                var Estacion = db.EstEstacion.Where(x => x.idEstacion == relacion.idLugar).FirstOrDefault();
                                                                if (Transformador != null)
                                                                {
                                                                    autString += " - " + Transformador.nombreTransformador;
                                                                }

                                                                break;
                                                            case 2: //Pto Conexion
                                                                var PtoConexion = db.PCPuntoConexion.Where(x => x.idPuntoConexion == relacion.idEquipo).FirstOrDefault();
                                                                var EstacionPto = db.EstEstacion.Where(x => x.idEstacion == relacion.idLugar).FirstOrDefault();
                                                                if (PtoConexion != null)
                                                                {
                                                                    autString += " - " + PtoConexion.nombrePuntoConexion;
                                                                }

                                                                break;
                                                            case 5: //Generador
                                                                var Generador = db.GenGenerador.Where(x => x.idGenerador == relacion.idEquipo).FirstOrDefault();
                                                                var Central = db.CConCentral.Where(x => x.idCentral == relacion.idEquipo).FirstOrDefault();
                                                                if (Generador != null)
                                                                {
                                                                    autString += " - " + Generador.nombreGenerador;
                                                                }

                                                                break;

                                                            case 7: //Capacitor
                                                                var Capacitor = db.CapCapacitor.Where(x => x.idCapacitor == relacion.idEquipo).FirstOrDefault();
                                                                var EstacionCapacitor = db.EstEstacion.Where(x => x.idEstacion == relacion.idLugar).FirstOrDefault();
                                                                if (Capacitor != null)
                                                                {
                                                                    autString += " - " + Capacitor.nombreCapacitor;
                                                                }
                                                                break;
                                                        }
                                                    }
                                                }
                                            }

                                        }

                                        NovedadPdfVM novedad = new NovedadPdfVM
                                        {
                                            descripcionNovedad = nov.descripcionNovedad,
                                            horaParseada = nov.horaNovedad.ToString().Split(':')[0] + ':' + nov.horaNovedad.ToString().Split(':')[1],
                                            nota = nov.nota,
                                            nombreEquipo = nombre,
                                            codNovedad = nov.codNovedad,
                                            automatismos = autString
                                        };
                                        var actu = db.NovActuacion.Where(x => x.idNovedad == nov.idNovedad).FirstOrDefault();
                                        if (actu != null)
                                        {
                                            if (actu.idTipoActuacion != null && actu.idTipoActuacion != -1 && actu.idActuacionEstado != null && actu.idActuacionEstado != -1)
                                            {
                                                var nombreActu = db.NovTipoActuacion.Where(x => x.activo == true && x.idTipoActuacion == actu.idTipoActuacion).FirstOrDefault().nombreTipoActuacion + "]#[" + db.NovActuacionEstado.Where(x => x.activo == true && x.idActuacionEstado == actu.idActuacionEstado).FirstOrDefault().nombreActuacionEstado + " / ";
                                                novedad.nombreActuacion = "" + nombreActu;
                                            }
                                            else
                                            {
                                                novedad.nombreActuacion = "";
                                            }
                                        }
                                        else
                                        {
                                            novedad.nombreActuacion = "";
                                        }
                                        novedades.Add(novedad);
                                    }

                                    break;
                                case 1005: //Estaciones
                                    var novest = db.NovEstacion.Where(x => x.idNovedad == nov.idNovedad).FirstOrDefault();
                                    if (novest != null)
                                    {

                                        var autString = "";
                                        var nombre = "";
                                        if (novest.idEstacion != null)
                                        {
                                            var estacion = db.EstEstacion.Where(x => x.activo == true && x.idEstacion == novest.idEstacion).FirstOrDefault();
                                            if (estacion != null)
                                            {
                                                nombre = estacion.nombreEstacion;
                                            }
                                        }
                                        if (novest.idTipoEquipo != null && novest.idEquipo != null)
                                        {
                                            switch (novest.idTipoEquipo)
                                            {
                                                case 1:

                                                    var tran = db.TranTransformador.Where(x => x.activo == true && x.idTransformador == novest.idEquipo).FirstOrDefault();
                                                    if (tran != null)
                                                    {
                                                        nombre = nombre + " " + tran.nombreTransformador;
                                                    }

                                                    break;
                                                case 2:
                                                    var pcon = db.PCPuntoConexion.Where(x => x.activo == true && x.idPuntoConexion == novest.idEquipo).FirstOrDefault();
                                                    if (pcon != null)
                                                    {
                                                        nombre = nombre + " " + pcon.nombrePuntoConexion;
                                                    }
                                                    break;
                                                case 4:
                                                    var linea = db.LinLinea.Where(x => x.activo == true && x.idLinea == novest.idEquipo).FirstOrDefault();
                                                    if (linea != null)
                                                    {
                                                        nombre = nombre + " " + linea.nombreLinea;
                                                    }
                                                    break;
                                                case 5:
                                                    var generador = db.GenGenerador.Where(x => x.activo == true && x.idGenerador == novest.idEquipo).FirstOrDefault();
                                                    if (generador != null)
                                                    {
                                                        nombre = nombre + " " + generador.nombreGenerador;
                                                    }
                                                    break;
                                                case 7:
                                                    var capacitor = db.CapCapacitor.Where(x => x.activo == true && x.idCapacitor == novest.idEquipo).FirstOrDefault();
                                                    if (capacitor != null)
                                                    {
                                                        nombre = nombre + " " + capacitor.nombreCapacitor;
                                                    }
                                                    break;
                                                case 1002:
                                                    var servicio = db.ServAuxServicioAuxiliar.Where(x => x.activo == true && x.idServAuxiliar == novest.idEquipo).FirstOrDefault();
                                                    if (servicio != null)
                                                    {
                                                        nombre = nombre + " " + servicio.nombreServicioAuxiliar;
                                                    }
                                                    break;
                                                case 3002:
                                                    var parque = db.CConCentral.Where(x => x.activo == true && x.idTipoCentral == 3 && x.idCentral == novest.idEquipo).FirstOrDefault();
                                                    if (parque != null)
                                                    {
                                                        nombre = nombre + " " + parque.nombreCentral;
                                                    }
                                                    break;
                                            }
                                        }
                                        if (nov.idAutomatismo != null && nov.idAutomatismo != -1)
                                        {
                                            var listaAutomatismos = db.NovAutomatismo.Where(x => x.idNovedad == nov.idNovedad).ToList();
                                            if (listaAutomatismos != null)
                                            {
                                                foreach (var automatismo in listaAutomatismos)
                                                {
                                                    var relacion = db.AutRelacionAutomatismo.Where(x => x.idAutoAutomRelacion == automatismo.idAutomatismo).FirstOrDefault();
                                                    if (relacion != null)
                                                    {
                                                        switch (relacion.idTipoEquipo)
                                                        {
                                                            case 1: //Transformador
                                                                var Transformador = db.TranTransformador.Where(x => x.idTransformador == relacion.idEquipo).FirstOrDefault();
                                                                var Estacion = db.EstEstacion.Where(x => x.idEstacion == relacion.idLugar).FirstOrDefault();
                                                                if (Transformador != null)
                                                                {
                                                                    autString += " - " + Transformador.nombreTransformador;
                                                                }

                                                                break;
                                                            case 2: //Pto Conexion
                                                                var PtoConexion = db.PCPuntoConexion.Where(x => x.idPuntoConexion == relacion.idEquipo).FirstOrDefault();
                                                                var EstacionPto = db.EstEstacion.Where(x => x.idEstacion == relacion.idLugar).FirstOrDefault();
                                                                if (PtoConexion != null)
                                                                {
                                                                    autString += " - " + PtoConexion.nombrePuntoConexion;
                                                                }

                                                                break;
                                                            case 5: //Generador
                                                                var Generador = db.GenGenerador.Where(x => x.idGenerador == relacion.idEquipo).FirstOrDefault();
                                                                var Central = db.CConCentral.Where(x => x.idCentral == relacion.idEquipo).FirstOrDefault();
                                                                if (Generador != null)
                                                                {
                                                                    autString += " - " + Generador.nombreGenerador;
                                                                }

                                                                break;

                                                            case 7: //Capacitor
                                                                var Capacitor = db.CapCapacitor.Where(x => x.idCapacitor == relacion.idEquipo).FirstOrDefault();
                                                                var EstacionCapacitor = db.EstEstacion.Where(x => x.idEstacion == relacion.idLugar).FirstOrDefault();
                                                                if (Capacitor != null)
                                                                {
                                                                    autString += " - " + Capacitor.nombreCapacitor;
                                                                }
                                                                break;
                                                        }
                                                    }
                                                }
                                            }

                                        }



                                        NovedadPdfVM novedad = new NovedadPdfVM
                                        {
                                            descripcionNovedad = nov.descripcionNovedad,
                                            horaParseada = nov.horaNovedad.ToString().Split(':')[0] + ':' + nov.horaNovedad.ToString().Split(':')[1],
                                            nota = nov.nota,
                                            nombreEquipo = nombre,
                                            codNovedad = nov.codNovedad,
                                            automatismos = autString

                                        };
                                        var actu = db.NovActuacion.Where(x => x.idNovedad == nov.idNovedad).FirstOrDefault();
                                        if (actu != null)
                                        {
                                            if (actu.idTipoActuacion != null && actu.idTipoActuacion != -1 && actu.idActuacionEstado != null && actu.idActuacionEstado != -1)
                                            {
                                                var nombreActu = db.NovTipoActuacion.Where(x => x.activo == true && x.idTipoActuacion == actu.idTipoActuacion).FirstOrDefault().nombreTipoActuacion + "]#[" + db.NovActuacionEstado.Where(x => x.activo == true && x.idActuacionEstado == actu.idActuacionEstado).FirstOrDefault().nombreActuacionEstado + " / ";
                                                novedad.nombreActuacion = nombreActu;
                                            }
                                            else
                                            {
                                                novedad.nombreActuacion = "";
                                            }
                                        }
                                        else
                                        {
                                            novedad.nombreActuacion = "";
                                        }
                                        novedades.Add(novedad);
                                    }
                                    break;




                                case 1006: //Lineas
                                    var novlin = db.NovLinea.Where(x => x.idNovedad == nov.idNovedad).FirstOrDefault();
                                    if (novlin != null)
                                    {

                                        var nombre = "";
                                        if (novlin.idTipoLinea != null)
                                        {
                                            var tipolinea = db.LinTipoLinea.Where(x => x.activo == true && x.idTipoLinea == novlin.idTipoLinea).FirstOrDefault();
                                            if (tipolinea != null)
                                            {
                                                nombre = tipolinea.nombreTipo;
                                            }
                                        }
                                        if (novlin.idLinea != null)
                                        {
                                            var linea = db.LinLinea.Where(x => x.activo == true && x.idLinea == novlin.idLinea).FirstOrDefault();
                                            if (linea != null)
                                            {
                                                nombre = nombre + " " + linea.nombreLinea;
                                            }
                                        }

                                        NovedadPdfVM novedad = new NovedadPdfVM
                                        {
                                            descripcionNovedad = nov.descripcionNovedad,
                                            horaParseada = nov.horaNovedad.ToString().Split(':')[0] + ':' + nov.horaNovedad.ToString().Split(':')[1],
                                            nota = nov.nota,
                                            nombreEquipo = nombre,
                                            codNovedad = nov.codNovedad
                                        };
                                        var actu = db.NovActuacion.Where(x => x.idNovedad == nov.idNovedad).FirstOrDefault();
                                        if (actu != null)
                                        {
                                            if (actu.idTipoActuacion != null && actu.idTipoActuacion != -1)
                                            {
                                                var nombreActu = db.NovTipoActuacion.Where(x => x.activo == true && x.idTipoActuacion == actu.idTipoActuacion).FirstOrDefault().nombreTipoActuacion + "]#[" + db.NovActuacionEstado.Where(x => x.activo == true && x.idActuacionEstado == actu.idActuacionEstado).FirstOrDefault().nombreActuacionEstado + " / ";
                                                novedad.nombreActuacion = nombreActu;
                                            }
                                            else
                                            {
                                                novedad.nombreActuacion = "";
                                            }
                                        }
                                        else
                                        {
                                            novedad.nombreActuacion = "";
                                        }
                                        novedades.Add(novedad);
                                    }
                                    break;
                                case 1008: //Reclamos
                                    var novrec = db.NovReclamo.Where(x => x.idNovedad == nov.idNovedad).FirstOrDefault();
                                    if (novrec != null)
                                    {
                                        var idTipoInfra = 0;
                                        var nombre = "";
                                        if (novrec.idInfraestructura != null)
                                        {
                                            var infra = db.RecInfraestructura.Where(x => x.activo == true && x.idInfraestructura == novrec.idInfraestructura).FirstOrDefault();
                                            if (infra != null)
                                            {
                                                nombre = nombre + " " + infra.nombreInfraestructura;
                                                idTipoInfra = Convert.ToInt32(infra.idTipoInfra);
                                            }
                                        }
                                        if (novrec.idInfraParticular != null)
                                        {
                                            switch (idTipoInfra)
                                            {
                                                case 1:
                                                    var part = db.EstEstacion.Where(x => x.idEstacion == novrec.idInfraParticular).FirstOrDefault();
                                                    if (part != null)
                                                    {
                                                        nombre = nombre + " " + part.nombreEstacion;
                                                    }
                                                    break;
                                                case 2:
                                                    var lin = db.LinLinea.Where(x => x.idLinea == novrec.idInfraParticular).FirstOrDefault();
                                                    if (lin != null)
                                                    {
                                                        nombre = nombre + " " + lin.nombreLinea;
                                                    }
                                                    break;
                                                case 3:
                                                    var edi = db.RecReclamoTipoInfra.Where(x => x.idTipoInfra == novrec.idInfraParticular).FirstOrDefault();
                                                    if (edi != null)
                                                    {
                                                        nombre = nombre + " " + edi.nombreInfra;
                                                    }
                                                    break;
                                            }

                                        }
                                        if (novrec.idTipoReclamo != null)
                                        {
                                            var tiporec = db.RecTipoReclamo.Where(x => x.activo == true && x.idTipoReclamo == novrec.idTipoReclamo).FirstOrDefault();
                                            if (tiporec != null)
                                            {
                                                nombre = nombre + " - Reclamo: " + tiporec.nombreTipoReclamo;
                                            }
                                        }





                                        NovedadPdfVM novedad = new NovedadPdfVM
                                        {
                                            descripcionNovedad = nov.descripcionNovedad,
                                            horaParseada = nov.horaNovedad.ToString().Split(':')[0] + ':' + nov.horaNovedad.ToString().Split(':')[1],
                                            nota = nov.nota,
                                            nombreEquipo = nombre,
                                            codNovedad = nov.codNovedad
                                        };
                                        var actu = db.NovActuacion.Where(x => x.idNovedad == nov.idNovedad).FirstOrDefault();
                                        if (actu != null)
                                        {
                                            if (actu.idTipoActuacion != null && actu.idTipoActuacion != -1)
                                            {
                                                var nombreActu = db.NovTipoActuacion.Where(x => x.activo == true && x.idTipoActuacion == actu.idTipoActuacion).FirstOrDefault().nombreTipoActuacion + "  " + db.NovActuacionEstado.Where(x => x.activo == true && x.idActuacionEstado == actu.idActuacionEstado).FirstOrDefault().nombreActuacionEstado;
                                                novedad.nombreActuacion = nombreActu;
                                            }
                                            else
                                            {
                                                novedad.nombreActuacion = "";
                                            }
                                        }
                                        else
                                        {
                                            novedad.nombreActuacion = "";
                                        }

                                        novedades.Add(novedad);
                                    }
                                    break;

                            }
                            novedades.OrderByDescending(x => x.horaParseada);

                        }
                        turno.novedades = novedades;
                    }

                    return lista;
                }
            }

        }

        public bool ValidarCierreNovedad(int? idNovedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {

                var consultaunaNovedad = ConsultarUnaNovedad(idNovedad, true);

                if (consultaunaNovedad != null)
                {
                    if (consultaunaNovedad.estadoNovedad.idEstadoNovedad == 1)
                    {
                        var respuesta = true;

                        return respuesta;
                    }

                }
                return false; // ya esta cerrada o no la encuentra
            }
        }


        public NovedadVM ConsultarUnaNovedad(int? idNovedad, bool activo)
        {

            NovedadVM novedad = new NovedadVM();
            LicenciaVM licencia = new LicenciaVM();
            List<AutorizacionTrabajoVM> autorizacionTrabajo = new List<AutorizacionTrabajoVM>();
            //manejar si no existe la novedad
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                novedad = (from c in db.NovNovedad
                           join d in db.NovEstadoNovedad on c.idEstadoNovedad equals d.idEstadoNovedad
                           join l in db.LicLicencia on c.idLicencia equals l.idLicencia into lic
                           from l in lic.DefaultIfEmpty()
                           where c.activo == activo && c.idNovedad == idNovedad
                           select new { c, d, l }).AsEnumerable().Select(x => new NovedadVM
                           {
                               idNovedad = x.c.idNovedad,
                               descripcionNovedad = x.c.descripcionNovedad,
                               fechaNovedad = x.c.fechaNovedad,
                               horaNovedad = x.c.horaNovedad,
                               fechaHoraNovedad = String.Format("{0:dd/MM/yyyy}", x.c.fechaNovedad) + " " + String.Format("{0:HH:mm}", x.c.horaNovedad),
                               fechaParseada = String.Format("{0:dd/MM/yyyy}", x.c.fechaNovedad),
                               horaParseada = String.Format("{0:HH:mm}", x.c.horaNovedad),
                               fechaVisible = String.Format("{0:dd/MM/yyyy}", x.c.fechaNovedad),
                               idUsuarioAlta = x.c.idUsuarioAlta,
                               activo = x.c.activo,
                               nota = x.c.nota,
                               idTipoNovedad = x.c.idTipoNovedad,
                               idLicencia = x.c.idLicencia,
                               idNovedadRelacionada = x.c.idNovRelacionada,
                               CodigoNovedad = x.c.codNovedad,
                               estadoNovedad = new EstadoNovedadVM
                               {
                                   idEstadoNovedad = x.d.idEstadoNovedad,
                                   nombreEstadoNovedad = x.d.estadoNovedad,
                                   activo = x.d.activo
                               },
                               licencia = new LicenciaVM
                               {
                                   idLicencia = x.l == null ? 0 : x.l.idLicencia,
                                   codigoLicencia = x.l == null ? "" : x.l.codigoLicencia,
                                   descripcionLicencia = x.l == null ? "" : x.l.descripcionLicencia,
                               },
                               idAutomatismo = x.c.idAutomatismo

                           }).FirstOrDefault();

                if (novedad != null)
                {
                    var fechaParseada = Convert.ToDateTime(novedad.fechaNovedad).ToString("yyyy-MM-dd");
                    novedad.fechaParseada = fechaParseada;
                    novedad.fechaVisible = Convert.ToDateTime(novedad.fechaNovedad).ToString("dd/MM/yyyy");
                    var horaParseada = Convert.ToDateTime(novedad.horaNovedad).ToString("HH:mm");
                    novedad.horaParseada = horaParseada;

                    var NovedadRelacionada = db.NovNovedad.Find(novedad.idNovedadRelacionada);

                    NovedadVM novRelacion = new NovedadVM();
                    if (NovedadRelacionada != null)
                    {
                        novRelacion.idNovedad = NovedadRelacionada.idNovedad;
                        novRelacion.descripcionNovedad = NovedadRelacionada.descripcionNovedad != null && NovedadRelacionada.descripcionNovedad.Length >= 50 ? NovedadRelacionada.descripcionNovedad.Substring(0, 50) : NovedadRelacionada.descripcionNovedad;
                        novRelacion.idUsuarioAlta = NovedadRelacionada.idUsuarioAlta;
                        novRelacion.activo = NovedadRelacionada.activo;
                        novRelacion.idLicencia = NovedadRelacionada.idLicencia;
                        novRelacion.fechaNovedad = NovedadRelacionada.fechaNovedad;
                        novRelacion.horaNovedad = NovedadRelacionada.horaNovedad;
                        novRelacion.fechaHoraNovedad = String.Format("{0:dd/MM/yyyy}", NovedadRelacionada.fechaNovedad) + " " + String.Format("{0:HH:mm}", NovedadRelacionada.horaNovedad);
                        novRelacion.fechaParseada = String.Format("{0:dd/MM/yyyy}", NovedadRelacionada.fechaNovedad);
                        novRelacion.horaParseada = String.Format("{0:HH:mm}", NovedadRelacionada.horaNovedad);
                        novRelacion.idAutomatismo = NovedadRelacionada.idAutomatismo;
                        novedad.novedadRelacionada = novRelacion;
                        //novedad.CodigoNovedad = NovedadRelacionada.codNovedad;
                    }

                    ServicioUsuario servUsuario = new ServicioUsuario();
                    if (novedad.idUsuarioAlta != null)
                    {
                        var usuarioNovedad = servUsuario.ObtenerDatosUsuarioPorId(novedad.idUsuarioAlta);
                        if (usuarioNovedad != null)
                        {
                            novedad.nombresuario = usuarioNovedad.nombreUsuario;
                        }
                    }

                    if (novedad.idTipoNovedad != null)
                    {
                        ServicioTipoNovedad servTipoNov = new ServicioTipoNovedad();
                        var tipoNovedad = servTipoNov.ObtenerUnTipoNovedad(novedad.idTipoNovedad);

                        novedad.tipoNovedad = tipoNovedad;
                        novedad.codigoTipoNovedad = tipoNovedad.codigoTipoNovedad;
                    }
                    var stringTipoNovedad = buscarTipoNovedad(Convert.ToInt32(idNovedad));

                    if (stringTipoNovedad != "")
                    {
                        ServicioEquipo servEquipo = new ServicioEquipo();

                        if (stringTipoNovedad == "CCON")
                        {
                            var novCentroControl = db.NovCentroControl.Where(x => x.idNovedad == idNovedad).FirstOrDefault();
                            novedad.CodigoEquipo = "GEN";
                            novedad.idEquipo = novCentroControl.idEquipo;
                            novedad.idCentral = novCentroControl.idCentral;
                            novedad.idCentroControl = novCentroControl.idCentroControl;
                            novedad.idTipoEquipo = 5;
                            var equipoDetalle = servEquipo.ConsultarEquipoDetalle(Convert.ToInt16(novedad.idEquipo), novedad.CodigoEquipo, Convert.ToInt16(novedad.idCentral));
                            novedad.equipoDetalleEstacion = equipoDetalle;


                        }
                        if (stringTipoNovedad == "EST")
                        {

                            var novEstacion = db.NovEstacion.Where(x => x.idNovedad == idNovedad).FirstOrDefault();
                            novedad.idEstacion = novEstacion.idEstacion;
                            novedad.idTipoEquipo = novEstacion.idTipoEquipo;
                            novedad.alarma = novEstacion.alarma;
                            novedad.idEquipoAdicional = novEstacion.idEquipoAdicional;
                            var Equipo = (from ce in db.EquiEquipo where ce.idEquipo == novedad.idTipoEquipo select ce).FirstOrDefault();
                            if (Equipo != null)
                            {
                                var codigoEquipo = Equipo.codigoEquipo;
                                novedad.CodigoEquipo = codigoEquipo;
                                novedad.idEquipo = novEstacion.idEquipo;
                                if (novedad.idEquipo != null && novedad.idEquipo != -1)
                                {
                                    var equipoDetalle = servEquipo.ConsultarEquipoDetalle(Convert.ToInt16(novedad.idEquipo), novedad.CodigoEquipo, Convert.ToInt16(novedad.idCentral));
                                    novedad.equipoDetalleEstacion = equipoDetalle;
                                }

                            }
                            var equipoAdicional = db.EquiEquipoAdicionales.Where(x => x.idEquipoAdicional == novEstacion.idEquipoAdicional && x.activo == true).FirstOrDefault();
                            if (equipoAdicional != null)
                            {
                                novedad.nombreEquipoAdicional = equipoAdicional.equipoAdicional;
                            }
                        }
                        if (stringTipoNovedad == "AUT")
                        {
                            var novAutomatismo = db.NovAutomatismo.Where(x => x.idNovedad == idNovedad).FirstOrDefault();
                            novedad.idAutomatismo = novAutomatismo.idAutomatismo;
                            novedad.idDetalleAutomatismo = novAutomatismo.idDetalleAutomatismo;
                        }
                        if (stringTipoNovedad == "LIN")
                        {
                            novedad.CodigoEquipo = "LIN";
                            var novLinea = db.NovLinea.Where(x => x.idNovedad == idNovedad).FirstOrDefault();
                            novedad.idTipoLinea = novLinea.idTipoLinea;
                            novedad.idEquipo = novLinea.idLinea;
                            novedad.alarma = novLinea.alarma;
                            novedad.idTipoEquipo = 4;
                            novedad.posesionLinea = novLinea.idPropiedad;
                            var equipoDetalle = servEquipo.ConsultarEquipoDetalle(Convert.ToInt16(novedad.idEquipo), novedad.CodigoEquipo, Convert.ToInt16(novedad.idCentral));
                            novedad.equipoDetalleEstacion = equipoDetalle;
                        }
                        if (stringTipoNovedad == "REC")
                        {

                            var novReclamo = db.NovReclamo.Where(x => x.idNovedad == idNovedad).FirstOrDefault();
                            novedad.idTipoReclamo = novReclamo.idTipoReclamo;
                            novedad.numeroReclamo = novReclamo.numReclamo;
                            if (novedad.idInfraestructura != -1)
                            {
                                novedad.idInfraestructura = novReclamo.idInfraestructura;
                                novedad.idEquipo = novReclamo.idInfraParticular;
                            }

                        }


                    }

                    if (novedad.equipoDetalleEstacion != null)
                    {
                        IndisponibilidadVM Indisponibilidad = new IndisponibilidadVM
                        {
                            idIndisponibilidad = 0,
                            nombreIndisponibilidad = "Indisponible al 100%"
                        };
                        ServicioIndisponibilidad servInd = new ServicioIndisponibilidad();

                        if (novedad.equipoDetalleEstacion.CodigoEquipo == "GEN" || novedad.equipoDetalleEstacion.CodigoEquipo == "PSFV")
                        {
                            GeneradorVM generador = new GeneradorVM();
                            if (novedad.equipoDetalleEstacion.CodigoEquipo == "GEN")
                            {
                                novedad.idGenerador = novedad.equipoDetalleEstacion.idEquipo;
                                generador.idGenerador = novedad.equipoDetalleEstacion.idEquipo;
                            }


                            generador.idCentral = novedad.idCentral;
                            generador.idCentroControl = novedad.idCentroControl;
                            generador.idGeneral = novedad.equipoDetalleEstacion.idEquipo;
                            generador.nombreEquipo = novedad.equipoDetalleEstacion.nombreEquipo;
                            generador.nombreGenerador = novedad.equipoDetalleEstacion.nombreEquipo;
                            generador.nombreDetalleCentral = novedad.equipoDetalleEstacion.nombreEquipoDetalleEstacion;
                            novedad.generador = generador;


                        }
                        else
                        if (novedad.equipoDetalleEstacion.CodigoEquipo == "LIN")
                        {
                            LineaVM linea = new LineaVM();
                            //ServicioIndisponibilidad servInd = new ServicioIndisponibilidad();
                            //var indisp = servInd.ConsultarIndPorNovedad(novedad.idNovedad);

                            novedad.idLinea = novedad.equipoDetalleEstacion.idEquipo;
                            novedad.idEstacion = novedad.equipoDetalleEstacion.idEstacion;
                            novedad.idTipoLinea = novedad.equipoDetalleEstacion.idTipoLinea;
                            novedad.posesionLinea = novedad.equipoDetalleEstacion.idTipoLinea;
                            //novedad.energiaNoSuministrada = !indisp.energiaNoSuministrada;

                            linea.idEquipo = novedad.equipoDetalleEstacion.idEquipo;
                            linea.idLinea = novedad.equipoDetalleEstacion.idEquipo;
                            linea.idTipoLinea = novedad.equipoDetalleEstacion.idTipoLinea;
                            linea.nombreEquipo = novedad.equipoDetalleEstacion.nombreEquipo;
                            linea.nombreLinea = novedad.equipoDetalleEstacion.nombreEquipo;

                            ServicioLinea servLinea = new ServicioLinea();

                            TipoLineaVM tipoLinea = new TipoLineaVM();
                            tipoLinea.idTipoLinea = novedad.equipoDetalleEstacion.idTipoLinea;
                            novedad.tipoLinea = tipoLinea;

                            novedad.lineaPosesion = servLinea.ConsultarPosesionLinea(novedad.posesionLinea);

                            novedad.propiedadLinea = servLinea.ConsultarPropiedadLinea(novedad.idLinea);

                        }
                        else if (novedad.equipoDetalleEstacion.CodigoEquipo == "TRA" || novedad.equipoDetalleEstacion.CodigoEquipo == "PCON" || novedad.equipoDetalleEstacion.CodigoEquipo == "CAP" || novedad.equipoDetalleEstacion.CodigoEquipo == "SAUX")
                        {
                            EquipoVM equipo = new EquipoVM();
                            if (novedad.equipoDetalleEstacion.CodigoEquipo == "TRA")
                            {
                                novedad.idTransformador = novedad.equipoDetalleEstacion.idEquipo;

                                equipo.idEquipo = novedad.idTransformador;
                                equipo.nombreEquipo = novedad.equipoDetalleEstacion.nombreEquipo;
                                equipo.codigoEquipo = novedad.equipoDetalleEstacion.CodigoEquipo;

                                novedad.equipo = equipo;
                                novedad.idEstacion = novedad.equipoDetalleEstacion.idEquipoDetalleEstacion;
                            }
                            else if (novedad.equipoDetalleEstacion.CodigoEquipo == "PCON")
                            {
                                novedad.idPuntoConexion = novedad.equipoDetalleEstacion.idEquipo;

                                equipo.idEquipo = novedad.idPuntoConexion;
                                equipo.nombreEquipo = novedad.equipoDetalleEstacion.nombreEquipo;
                                equipo.codigoEquipo = novedad.equipoDetalleEstacion.CodigoEquipo;
                                novedad.idEstacion = novedad.equipoDetalleEstacion.idEquipoDetalleEstacion;

                                novedad.equipo = equipo;

                            }
                            else if (novedad.equipoDetalleEstacion.CodigoEquipo == "CAP")
                            {
                                novedad.idCapacitores = novedad.equipoDetalleEstacion.idEquipo;

                                equipo.idEquipo = novedad.idCapacitores;
                                equipo.nombreEquipo = novedad.equipoDetalleEstacion.nombreEquipo;
                                equipo.codigoEquipo = novedad.equipoDetalleEstacion.CodigoEquipo;
                                novedad.idEstacion = novedad.equipoDetalleEstacion.idEquipoDetalleEstacion;

                                novedad.equipo = equipo;
                            }
                            else if (novedad.equipoDetalleEstacion.CodigoEquipo == "SAUX")
                            {
                                novedad.idServiciosAuxiliares = novedad.equipoDetalleEstacion.idEquipo;
                                equipo.idEquipo = novedad.idServiciosAuxiliares;
                                equipo.nombreEquipo = novedad.equipoDetalleEstacion.nombreEquipo;
                                equipo.codigoEquipo = novedad.equipoDetalleEstacion.CodigoEquipo;
                                novedad.idEstacion = novedad.equipoDetalleEstacion.idEquipoDetalleEstacion;

                                novedad.equipo = equipo;
                            }
                            novedad.idTipoEquipo = novedad.idTipoEquipo;
                            novedad.idEquipo = novedad.equipoDetalleEstacion.idEquipo;
                        }
                    }




                    if (novedad.idEstacion != null)
                    {
                        ServicioEstacion est = new ServicioEstacion();
                        novedad.estacion = est.ConsultarUnEstacion(novedad.idEstacion);
                    }

                    if (novedad.idCentral != null)
                    {
                        ServicioCentrales central = new ServicioCentrales();
                        novedad.central = central.ConsultarUnaCentral(Convert.ToInt16(novedad.idCentral));
                    }
                    if (novedad.idCentroControl != null)
                    {
                        ServicioCentroControl centro = new ServicioCentroControl();
                        novedad.centroControl = centro.ConsultarUnCentroControl(Convert.ToInt16(novedad.idCentroControl));
                    }


                    if (novedad.idTransformador != null)
                    {
                        ServicioTransformador transf = new ServicioTransformador();
                        novedad.transformador = transf.ConsultarUnTransformador(novedad.idTransformador);
                    }
                    if (novedad.idCapacitores != null)
                    {
                        ServicioCapacitores cap = new ServicioCapacitores();
                        novedad.capacitor = cap.ConsultarUnCapacitor(novedad.idCapacitores);

                    }

                    if (novedad.idPuntoConexion != null)
                    {
                        ServicioPuntoConexion punto = new ServicioPuntoConexion();
                        novedad.puntoConexion = punto.ConsultarUnPuntoDeConexion(novedad.idPuntoConexion);
                    }


                    if (novedad.idInfraestructura != null)
                    {
                        ServicioInfraestructura reclamo = new ServicioInfraestructura();
                        novedad.infraestructura = reclamo.ConsultarUnaInfraestructura(novedad.idInfraestructura);
                    }




                    if (novedad.idTipoReclamo != null)
                    {
                        ServicioReclamo reclamo = new ServicioReclamo();
                        novedad.reclamo = reclamo.ConsultarUnReclamo(novedad.idTipoReclamo);

                        ServicioEquipo servEquipo = new ServicioEquipo();
                        if (novedad.infraestructura != null)
                        {
                            if (novedad.idEquipo != -1)
                            {
                                var detalleEquipo = servEquipo.ObtenerUnEquipoPorInfra(novedad.infraestructura.codigoInfraestructura, novedad.idEquipo, true);

                                novedad.reclamo.idEquipo = detalleEquipo.idEquipoReclamo;
                                novedad.reclamo.nombreEquipo = detalleEquipo.nombreEquipoReclamo;

                                novedad.equipoReclamo = detalleEquipo;

                                novedad.idEquipoReclamo = detalleEquipo.idEquipoReclamo;
                            }

                        }

                    }


                    //Revisar aca Uciel      

                    if (novedad.idAutomatismo != null)
                        if (novedad.idAutomatismo != null)
                        {
                            List<int> list = new List<int>();
                            var listAut = db.NovAutomatismo.Where(x => x.idNovedad == novedad.idNovedad).ToList();
                            foreach (var item in listAut)
                            {
                                list.Add(Convert.ToInt32(item.idAutomatismo));
                            }
                            novedad.automatismos = list;
                        }

                    var novActuacion = db.NovActuacion.Where(x => x.idNovedad == idNovedad).FirstOrDefault();
                    if (novActuacion != null)
                    {
                        novedad.idActuacion = novActuacion.idActuacionEstado;
                        novedad.idTipoActuacion = novActuacion.idTipoActuacion;
                    }

                    if (novedad.idActuacion != null)
                    {
                        ServicioActuacion actuacion = new ServicioActuacion();
                        novedad.actuacion = actuacion.ConsultarUnaActuacion(Convert.ToInt16(novedad.idActuacion));


                    }
                    if (novedad.idTipoActuacion != null)
                    {
                        ServicioTipoActuacion tipoActuacion = new ServicioTipoActuacion();
                        novedad.tipo = tipoActuacion.ConsultarUnTipoActuacion(Convert.ToInt16(novedad.idTipoActuacion));
                        //QUE ONDA CON ESTO DE ACA ABAJO?? NUNCA CREA INSTANCIA DE ACTUACION EN NOVEDAD
                        //novedad.actuacion.idTipoActuacion = novedad.tipo.idTipoActuacion;
                    }

                    ServicioLicencia servLic = new ServicioLicencia();
                    // consulta licencias y autorizaciones si tiene
                    licencia = servLic.ConsultarUnaLicencia(novedad.idLicencia);

                    TipoLicenciaVM tipoLicencia = new TipoLicenciaVM();
                    LicenciaVM lic = new LicenciaVM
                    {
                        codigoLicencia = "",
                        tipoLicencia = tipoLicencia
                    };
                    novedad.licencia = lic;

                    if (licencia != null)
                    {

                        if (licencia.idTipoLicencia != 0)
                        {
                            var tipoLic = servLic.ConsultarUnTipoLicencia(licencia.idTipoLicencia);
                            tipoLicencia.idTipoLicencia = tipoLic.idTipoLicencia;
                            tipoLicencia.nombreTipoLicencia = tipoLic.nombreTipoLicencia;


                        }
                        licencia.tipoLicencia = tipoLicencia;

                        licencia.fechaLicenciaParseada = Convert.ToDateTime(licencia.fechaHoraInicioLicencia).ToString("yyyy-MM-dd");
                        licencia.horaLicenciaParseada = Convert.ToDateTime(licencia.fechaHoraInicioLicencia).ToString("HH:mm");
                        if (licencia.fechaHoraFinLicencia != null)
                        {
                            licencia.fechaFinLicenciaParseada = Convert.ToDateTime(licencia.fechaHoraFinLicencia).ToString("yyyy-MM-dd");
                            licencia.horaFinLicenciaParseada = Convert.ToDateTime(licencia.fechaHoraInicioLicencia).ToString("HH:mm");
                        }
                        var usuarioAltaLicencia = servUsuario.ObtenerDatosUsuarioPorId(licencia.idUsarioAlta);

                        if (usuarioAltaLicencia != null)
                        {
                            licencia.nombresuario = usuarioAltaLicencia.nombreUsuario;
                        }
                        var idLicencia = licencia.idLicencia;
                        ServicioAutorizacionTrabajo servAutTrab = new ServicioAutorizacionTrabajo();
                        autorizacionTrabajo = servAutTrab.ObtenerListaAutorizacionTrabajo(idLicencia);

                        foreach (var item2 in autorizacionTrabajo)
                        {
                            item2.fechaInicioParseada = Convert.ToDateTime(item2.fechaHoraInicioAutorizacionTrabajo).ToString("yyyy-MM-dd");
                            item2.horaInicioParseada = Convert.ToDateTime(item2.fechaHoraInicioAutorizacionTrabajo).ToString("HH:mm");
                            if (item2.fechaHoraFinAutorizacionTrabajo != null)
                            {
                                item2.fechaFnParseada = Convert.ToDateTime(item2.fechaHoraFinAutorizacionTrabajo).ToString("yyyy-MM-dd");
                                item2.horaFnParseada = Convert.ToDateTime(item2.fechaHoraFinAutorizacionTrabajo).ToString("HH:mm");
                            }

                        }
                        licencia.autorizacionTrabajo = autorizacionTrabajo;
                        novedad.licencia = licencia;
                    }

                    // consulta archivos

                    novedad.listArchivos = ConsultarArchivos(novedad.idNovedad);
                }
            }
            return novedad;

        }

        public int RecuperarNovedad(NovedadVM novedad)
        {
            //manejar el error en la recuperacion
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunaNovedad = db.NovNovedad.Where(x => x.idNovedad == novedad.idNovedad).FirstOrDefault();

                if (consultaunaNovedad.idNovRelacionada != null || consultaunaNovedad.idNovRelacionada != 0)
                {
                    var ramaCompleta = ObtenerRamaCompletaDeNovedad(consultaunaNovedad.idNovRelacionada);
                    var hijo = ramaCompleta.Where(x => x.idNovedad == consultaunaNovedad.idNovRelacionada).FirstOrDefault();
                    if (hijo != null)
                    {
                        var padre = ramaCompleta.Where(x => x.idNovedadRelacionada == hijo.idNovedad).FirstOrDefault();
                        if (padre != null)
                        {
                            var setPadre = db.NovNovedad.Where(x => x.idNovedad == padre.idNovedad).FirstOrDefault();
                            setPadre.idNovRelacionada = consultaunaNovedad.idNovedad;

                        }
                        db.SaveChanges();
                    }

                    //var padre = ramaCompleta.Where(x => x.idNovedadRelacionada == padre.idNovedad).FirstOrDefault();

                }



                if (consultaunaNovedad != null)
                {
                    consultaunaNovedad.activo = true;
                    db.SaveChanges();
                    //manejar el historial 
                }
            }
            return 200;
        }

        public bool ValidarNovedad(NovedadVM novedad)
        {
            //meterle más lógica en el validar
            if (novedad != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public NovedadVM GenerarNovedadRelacionada(NovedadVM novedad, int? idUsuarioAlta, bool cierre)
        {
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    ServicioActuacion servAct = new ServicioActuacion();

                    novedad.idActuacion = novedad.idActuacion;
                    novedad.idTipoActuacion = novedad.idTipoActuacion;
                    novedad.descripcionNovedad = novedad.descripcionNovedad;

                    //ind.LiberarIndisponibilidad(nov.idNovedad, true);


                    var retornoAltaNovedad = CRUD(novedad, idUsuarioAlta, "C");
                    var buscaUltimaRama = ObtenerLaUltimaNovedadRelacionada(novedad.idNovedadOrigen);
                    var buscarNovXActuacion = db.NovActuacion.Where(x => x.idNovedad == novedad.idNovedadOrigen).FirstOrDefault();
                    if (buscaUltimaRama != null)
                    {
                        var Origen = db.NovNovedad.Find(buscaUltimaRama.idNovedad);
                        var novedadActual = db.NovNovedad.Find(retornoAltaNovedad.idNovedad);
                        novedadActual.idLicencia = Origen.idLicencia;
                        Origen.idNovRelacionada = retornoAltaNovedad.idNovedad;

                    }
                    else
                    {
                        var Origen = db.NovNovedad.Find(novedad.idNovedadOrigen);
                        var novedadActual = db.NovNovedad.Find(retornoAltaNovedad.idNovedad);
                        novedadActual.idLicencia = Origen.idLicencia;
                        Origen.idNovRelacionada = retornoAltaNovedad.idNovedad;


                    }

                    db.SaveChanges();

                    retornoAltaNovedad.codigo = 200;

                    return retornoAltaNovedad;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }

        }

        public ArchivoVM GetArchivo(int idArchivo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var archivo = db.NovNovedadArchivo.Find(idArchivo);
                ArchivoVM arcVm = new ArchivoVM
                {
                    idArchivo = archivo.idNovNovedadArchivo,
                    activo = archivo.activo,
                    idEntidad = archivo.idNovedad,
                    nombreArchivo = archivo.nombreArchivo,
                    urlArchivo = archivo.url,
                    nombreRandom = archivo.nombreArchivoGuardado
                };
                return arcVm;
            }
        }

        public int GuardarArchivoNovedad(List<ArchivoVM> listaArchivos)
        {
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    foreach (var item in listaArchivos)
                    {
                        NovNovedadArchivo novedadArchivo = new NovNovedadArchivo()
                        {
                            activo = true,
                            fechaHoraCarga = DateTime.Now,
                            idNovedad = item.idEntidad,
                            nombreArchivo = item.nombreArchivo,
                            nombreArchivoGuardado = item.nombreRandom,
                            url = item.urlArchivo
                        };
                        db.NovNovedadArchivo.Add(novedadArchivo);
                    }

                    db.SaveChanges();
                }
                return 200;
            }
            catch (Exception e)
            {
                return 400;
            }
        }

        public int EliminarArchivoNovedad(string listArchivo)
        {
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    if (listArchivo != null)
                    {
                        var splitIds = listArchivo.Split(',');
                        foreach (var item in splitIds)
                        {
                            var idArchivo = Int32.Parse(item);
                            var archivo = db.NovNovedadArchivo.Find(idArchivo);
                            archivo.activo = false;
                            db.SaveChanges();
                        }
                    }

                    return 200;
                }
            }
            catch (Exception e)
            {
                return 400;
            }

        }

        public int GenerarHistorialNovedad(NovNovedad novedad, string tipoHistorial, int? idUsuario)
        {
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    var tipoHist = (from th in db.NovTipoHistoriaNovedad where th.estadoNovedad == tipoHistorial select th).FirstOrDefault();
                    var ultHist = db.NovHistorialNovedad.Where(x => x.idNovedad == novedad.idNovedad && x.EsUltimo == true).FirstOrDefault();
                    //var ultHistNovedad = (from uh in db.NovHistorialNovedad where uh.idNovedad == novedad.idNovedad && uh.EsUltimo == true select uh).FirstOrDefault();
                    //COMENTO
                    NovHistorialNovedad historial = new NovHistorialNovedad
                    {
                        idNovedad = novedad.idNovedad,
                        fechaHistorialNovedad = DateTime.Today,
                        idEstadoNovedad = 1,
                        anioNovedad = novedad.anioNovedad,
                        codNovedad = novedad.codNovedad,
                        descripcionNovedad = novedad.descripcionNovedad,
                        EsUltimo = true,
                        horaHistorialNovedad = DateTime.Now,
                        idAlarma = null,
                        idIndisponibilidad = novedad.idIndisponibilidad,
                        idLicencia = novedad.idLicencia,
                        idNovRelacionada = novedad.idNovRelacionada,
                        idTipoHistoria = tipoHist.idTipoHistoriaNovedad,
                        idTipoNovedad = novedad.idTipoNovedad,
                        idUsuarioAltaModif = idUsuario,
                        NumNovedad = novedad.NumNovedad,
                        fechaHoraHistorial = novedad.fechaHoraNovedad
                       
                    };

                    db.NovHistorialNovedad.Add(historial);
                    db.SaveChanges();
                    if (ultHist != null)
                    {
                        ultHist.EsUltimo = false;
                    }

                    db.SaveChanges();
                    return 200;
                }
            }
            catch (Exception e)
            {
                return 400;
            }
        }

        public List<ArchivoVM> ConsultarArchivos(int idNovedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var archivos = (from d in db.NovNovedadArchivo
                                where d.activo == true && d.idNovedad == idNovedad
                                select new ArchivoVM
                                {
                                    idArchivo = d.idNovNovedadArchivo,
                                    idEntidad = d.idNovedad,
                                    nombreArchivo = d.nombreArchivo,
                                    urlArchivo = d.url
                                }).ToList();


                return archivos;
            }


        }
        public NovedadVM GenerarCodigoNovedad()
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                NovedadVM novCodigo = new NovedadVM();
                DateTime dt = DateTime.Now;

                var currentYear = Int16.Parse(dt.ToString("yy"));
                var codigo = "0001";

                var ultimaNovedad = db.NovNovedad.OrderByDescending(x => x.idNovedad).FirstOrDefault();

                if (ultimaNovedad != null)
                {
                    if (ultimaNovedad.anioNovedad == currentYear)
                    {
                        codigo = (ultimaNovedad.NumNovedad + 1).ToString();
                        int count = codigo.Length;
                        switch (count)
                        {
                            case 1:
                                codigo = "000" + codigo;
                                break;
                            case 2:
                                codigo = "00" + codigo;
                                break;
                            case 3:
                                codigo = "0" + codigo;
                                break;

                            default:
                                //Console.WriteLine("Default case");
                                break;
                        }
                    }
                }
                novCodigo.CodigoNovedad = "NOV-" + currentYear.ToString() + "-" + codigo;
                novCodigo.NumNovedad = Int16.Parse(codigo);
                novCodigo.anioNovedad = currentYear;
                return novCodigo;
            }
        }
        public string ObtenerCodigoReclamo()
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                DateTime dt = DateTime.Now;

                var currentYear = Int16.Parse(dt.ToString("yy"));
                int cantidadDigitos = 4;

                //ver la posibilidad de manejarlo por codigo y no por id 
                var ultimoReclamo = db.NovReclamo.OrderByDescending(x => x.idNovedad).FirstOrDefault();
                var ultimoNumeroReclamo = 0;
                if (ultimoReclamo != null)
                {
                    var anioUltimoReclamo = Int32.Parse(ultimoReclamo.numReclamo.Split('-')[1]);
                    if (anioUltimoReclamo == currentYear)
                    {
                        ultimoNumeroReclamo = Int32.Parse(ultimoReclamo.numReclamo.Split('-')[2]);
                    }
                }

                var numeroReclamo = (ultimoNumeroReclamo + 1).ToString();

                while (numeroReclamo.Length < cantidadDigitos)
                {
                    numeroReclamo = "0" + numeroReclamo;
                }
                var reclamoString = "REC-" + currentYear.ToString() + "-" + numeroReclamo;
                return reclamoString;
            }
        }


        public IQueryable<NovedadVM> FiltrarQuery(IQueryable<NovedadVM> query, string fechaDesde, string fechaHasta, int idEquipo, int idTipoEquipo, int tipoNovedad, int actuacion, int propiedad)
        {
            if (fechaDesde != null && fechaHasta != null)
            {
                var fechaDateDesde = DateTime.Parse(fechaDesde);
                var fechaDateHasta = DateTime.Parse(fechaHasta);

                query = query.Where(x => x.fechaNovedad <= fechaDateHasta);
                query = query.Where(x => x.fechaNovedad >= fechaDateDesde);
            }
            if (tipoNovedad != 0)
            {
                query = query.Where(x => x.tipoNovedad.idTipoNovedad == tipoNovedad);
            }
            if (idEquipo != 0)
            {
                query = query.Where(x => x.idEquipo == idEquipo);
            }

            if (idTipoEquipo != 0)
            {
                query = query.Where(x => x.idTipoEquipo == idTipoEquipo);
            }

            if (propiedad != -1)
            {
                query = query.Where(x => x.posesionLinea == propiedad);
            }

            if (actuacion != 0)
            {
                query = query.Where(x => x.idActuacion == actuacion);
            }

            return query;

        }

        public object ObtenerTipoEstadoNovedad(bool Activo)
        {
            List<EstadoNovedadVM> estadoNovedad = new List<EstadoNovedadVM>();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                estadoNovedad = (from e in db.NovEstadoNovedad
                                 where e.activo == Activo
                                 select new EstadoNovedadVM
                                 {
                                     idEstadoNovedad = e.idEstadoNovedad,
                                     nombreEstadoNovedad = e.estadoNovedad,
                                     activo = e.activo,
                                 }).ToList();
            }
            object json = new { data = estadoNovedad };
            return json;
        }

        public int CerrarNoveadesConLicencia(int? idNovedad)
        {
            var novedades = ObtenerRamaCompletaDeNovedad(idNovedad);

            if (novedades != null)
            {
                foreach (var novedad in novedades)
                {
                    CerrarNovedad(novedad.idNovedad);
                }
                return 200;
            }
            else
            {
                return 400;
            }

        }

        public int consultaTurnoEjecucion(int? idNovedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultoNovedad = db.NovNovedad.Where(x => x.idNovedad == idNovedad && x.activo == true).FirstOrDefault();
                var consutlTurnoEjecucion = db.TurnTurnoEjecucion.Where(x => x.activo == true && x.idTurnoEjecucion == consultoNovedad.idTurnoEjecucion).FirstOrDefault();

                if (consutlTurnoEjecucion != null)
                {
                    return 200;
                }
                else
                {
                    return 400;
                }
            }
        }

        public bool consultarRamaNovedades(int idNovedad)
        {
            var novedades = ObtenerRamaCompletaDeNovedad(idNovedad);

            if (novedades.Count > 1)
            {
                return true;
            }
            else if (novedades.Count == 1)
            {
                CerrarNovedad(idNovedad);
                return false;
            }
            else
            {
                return false;
            }
        }

    }
}
