﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class ArchivoVM
    {
        public int idArchivo { get; set; }
        public string nombreArchivo { get; set; }
        public string nombreRandom { get; set; }
        public string urlArchivo { get; set; }
        public int? idEntidad { get; set; }
        public bool? activo { get; set; }
        public int error { get; set; }
    }
}
