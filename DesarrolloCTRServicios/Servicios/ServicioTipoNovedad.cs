﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioTipoNovedad
    {
        //lista de tipo de novedad que recibe un activo y devuelve un obj
        public object ObtenerListaTipoNovedad(bool activo)
        {
            List<TipoNovedadVM> listaTipoNovedad;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                listaTipoNovedad = (from c in db.NovTipoNovedad
                                    where c.activo == activo
                                    select new TipoNovedadVM
                                    {
                                        idTipoNovedad = c.idTipoNovedad,
                                        nombreTipoNovedad = c.nombreTipoNovedad,
                                        activo = c.activo,
                                        codigoTipoNovedad = c.codigoTipoNovedad
                                    }).ToList();
            }

            object json = new { data = listaTipoNovedad };

            return json;
        }
        //lista de tipo nvedad que no recibe nada y devuleve una lista
        public List<TipoNovedadVM> ObtenerListaTipoNovedad()
        {
            List<TipoNovedadVM> listaTipoNovedad;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                listaTipoNovedad = (from c in db.NovTipoNovedad
                                    where c.activo == true
                                    select new TipoNovedadVM
                                    {
                                        idTipoNovedad = c.idTipoNovedad,
                                        nombreTipoNovedad = c.nombreTipoNovedad,
                                        tipofalla = c.tipoFalla,
                                        personal = c.personal,
                                        transformador = c.transformador,
                                        equipo = c.equipo,
                                        estacion = c.estacion,
                                        linea = c.linea,
                                        activo = c.activo,
                                        automatismo = c.Automatismos,
                                        reclamo = c.Reclamos,
                                        codigoTipoNovedad = c.codigoTipoNovedad
                                    }).ToList();
            }

            return listaTipoNovedad;
        }
        public TipoNovedadVM ObtenerUnTipoNovedad(int? idTipoNovedad)
        {
            TipoNovedadVM untipoNovedad = new TipoNovedadVM();
            TipoNovedadVM tipoNovedad = new TipoNovedadVM();

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                tipoNovedad = (from c in db.NovTipoNovedad
                               where c.activo == true && c.idTipoNovedad == idTipoNovedad
                               select new TipoNovedadVM
                               {
                                   idTipoNovedad = c.idTipoNovedad,
                                   nombreTipoNovedad = c.nombreTipoNovedad,
                                   tipofalla = c.tipoFalla,
                                   personal = c.personal,
                                   equipo = c.equipo,
                                   transformador = c.transformador,
                                   estacion = c.estacion,
                                   linea = c.linea,
                                   activo = c.activo,
                                   capacitor = c.capacitor,
                                   puntoConexion = c.puntoConexion,
                                   centroControl = c.centroControl,
                                   automatismo = c.Automatismos,
                                   reclamo = c.Reclamos,
                                   codigoTipoNovedad = c.codigoTipoNovedad
                               }).FirstOrDefault();
            }
            if (tipoNovedad != null)
            {
                untipoNovedad.idTipoNovedad = tipoNovedad.idTipoNovedad;
                untipoNovedad.nombreTipoNovedad = tipoNovedad.nombreTipoNovedad;
                untipoNovedad.activo = tipoNovedad.activo;
                untipoNovedad.codigoTipoNovedad = tipoNovedad.codigoTipoNovedad;
                if (tipoNovedad.estacion == true)
                {
                    untipoNovedad.estacion = tipoNovedad.estacion;
                }
                if (tipoNovedad.personal == true)
                {
                    untipoNovedad.personal = tipoNovedad.personal;
                }
                if (tipoNovedad.equipo == true)
                {
                    untipoNovedad.equipo = tipoNovedad.equipo;
                }
                if (tipoNovedad.transformador == true)
                {
                    untipoNovedad.transformador = tipoNovedad.transformador;
                }
                if (tipoNovedad.tipofalla == true)
                {
                    untipoNovedad.tipofalla = tipoNovedad.tipofalla;
                }
                if (tipoNovedad.linea == true)
                {
                    untipoNovedad.linea = tipoNovedad.linea;
                }
                if (tipoNovedad.capacitor == true)
                {
                    untipoNovedad.capacitor = tipoNovedad.capacitor;
                }
                if (tipoNovedad.puntoConexion == true)
                {
                    untipoNovedad.puntoConexion = tipoNovedad.puntoConexion;
                }
                if (tipoNovedad.centroControl == true)
                {
                    untipoNovedad.centroControl = tipoNovedad.centroControl;
                }
                if (tipoNovedad.automatismo == true)
                {
                    untipoNovedad.automatismo = tipoNovedad.automatismo;
                }
                if (tipoNovedad.reclamo == true)
                {
                    untipoNovedad.reclamo = tipoNovedad.reclamo;
                }
            }
            else
            {
                untipoNovedad.idTipoNovedad = -2;
            }



            return untipoNovedad;
        }

        public int AltaTipoNovedad(TipoNovedadVM tipoNovedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunTipoNovedad = db.NovTipoNovedad.Where(x => x.idTipoNovedad == tipoNovedad.idTipoNovedad).FirstOrDefault();
                if (consultaunTipoNovedad == null)
                {
                    try
                    {
                        NovTipoNovedad tipoN = new NovTipoNovedad
                        {
                            nombreTipoNovedad = tipoNovedad.nombreTipoNovedad,
                            tipoFalla = tipoNovedad.tipofalla,
                            personal = tipoNovedad.personal,
                            transformador = tipoNovedad.transformador,
                            equipo = tipoNovedad.equipo,
                            estacion = tipoNovedad.estacion,
                            linea = tipoNovedad.linea,
                            capacitor = tipoNovedad.capacitor,
                            puntoConexion = tipoNovedad.puntoConexion,
                            activo = true,
                            centroControl = tipoNovedad.centroControl,
                            Reclamos = tipoNovedad.reclamo,
                            Automatismos = tipoNovedad.automatismo
                        };
                        db.NovTipoNovedad.Add(tipoN);
                        db.SaveChanges();
                        return 200;
                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //ya existe ese dato
                }
            }

        }

        public TipoNovedadVM ConsultarUnTipoNovedad(int idtipoNovedad)
        {
            TipoNovedadVM unTiponovedad = new TipoNovedadVM();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                unTiponovedad = (from c in db.NovTipoNovedad
                                 where c.activo == true && c.idTipoNovedad == idtipoNovedad
                                 select new TipoNovedadVM
                                 {
                                     idTipoNovedad = c.idTipoNovedad,
                                     nombreTipoNovedad = c.nombreTipoNovedad,
                                     activo = c.activo,
                                     tipofalla = c.tipoFalla,
                                     personal = c.personal,
                                     transformador = c.transformador,
                                     equipo = c.equipo,
                                     estacion = c.estacion,
                                     linea = c.linea,
                                     capacitor = c.capacitor,
                                     puntoConexion = c.puntoConexion,
                                     centroControl = c.centroControl,
                                     automatismo = c.Automatismos,
                                     reclamo = c.Reclamos
                                 }).FirstOrDefault();
            }
            return unTiponovedad;
        }

        public int ModificarTipoNovedad(TipoNovedadVM tipoNovedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunTipoNovedad = db.NovTipoNovedad.Where(x => x.idTipoNovedad == tipoNovedad.idTipoNovedad).FirstOrDefault();
                if (consultaunTipoNovedad != null)
                {
                    consultaunTipoNovedad.nombreTipoNovedad = tipoNovedad.nombreTipoNovedad;
                    consultaunTipoNovedad.equipo = tipoNovedad.equipo;
                    consultaunTipoNovedad.estacion = tipoNovedad.estacion;
                    consultaunTipoNovedad.personal = tipoNovedad.personal;
                    consultaunTipoNovedad.transformador = tipoNovedad.transformador;
                    consultaunTipoNovedad.tipoFalla = tipoNovedad.tipofalla;
                    consultaunTipoNovedad.linea = tipoNovedad.linea;
                    consultaunTipoNovedad.capacitor = tipoNovedad.capacitor;
                    consultaunTipoNovedad.puntoConexion = tipoNovedad.puntoConexion;
                    consultaunTipoNovedad.centroControl = tipoNovedad.centroControl;
                    consultaunTipoNovedad.Automatismos = tipoNovedad.automatismo;
                    consultaunTipoNovedad.Reclamos = tipoNovedad.reclamo;
                    db.SaveChanges();
                }
            }
            return 200;
        }

        public int RecuperarTipoNovedad(TipoNovedadVM tipoNovedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunTipoNovedad = db.NovTipoNovedad.Where(x => x.idTipoNovedad == tipoNovedad.idTipoNovedad).FirstOrDefault();
                if (consultaunTipoNovedad != null)
                {
                    consultaunTipoNovedad.activo = true;
                    db.SaveChanges();
                }
            }
            return 200;
        }

        public int EliminarTipoNovedad(TipoNovedadVM tipoNovedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunTipoNovedad = db.NovTipoNovedad.Where(x => x.idTipoNovedad == tipoNovedad.idTipoNovedad).FirstOrDefault();
                if (consultaunTipoNovedad != null)
                {
                    consultaunTipoNovedad.activo = false;
                    db.SaveChanges();
                }
            }
            return 200;
        }

        public bool ValidarTipoNovedad(TipoNovedadVM tipoNovedad)
        {
            if (!string.IsNullOrWhiteSpace(tipoNovedad.nombreTipoNovedad) &&
                tipoNovedad.nombreTipoNovedad.Count() <= 30)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public TipoNovedadVM ConsultarTipoNovedadCodigo(string codigoTipo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var tipoNovedad = (from c in db.NovTipoNovedad
                                   where c.activo == true && c.codigoTipoNovedad == codigoTipo
                                   select new TipoNovedadVM
                                   {
                                       idTipoNovedad = c.idTipoNovedad,
                                       activo = c.activo,
                                       automatismo = c.Automatismos,
                                       capacitor = c.capacitor,
                                       centroControl = c.centroControl,
                                       codigoTipoNovedad = c.codigoTipoNovedad,
                                       equipo = c.equipo,
                                       estacion = c.estacion,
                                       linea = c.linea,
                                       nombreTipoNovedad = c.nombreTipoNovedad,
                                       puntoConexion = c.puntoConexion,
                                       reclamo = c.Reclamos,
                                       tipofalla = c.tipoFalla,
                                       transformador = c.transformador
                                   }).FirstOrDefault();
                return tipoNovedad;
            }
        }

        public object CargarTipoEquipoXTipoNovedad(int idEquipo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                if (idEquipo == 1004)
                {
                    var centroContoles = db.CConCentroControl.Where(x => x.activo == true).ToList();
                    object json = new { data = centroContoles };
                    return json;
                }
                else if (idEquipo == 1005)
                {
                    var estaciones = db.EstEstacion.Where(x => x.activo == true).ToList();
                    object json = new { data = estaciones };
                    return json;
                }
                else if (idEquipo == 1006)
                {
                    var lineas = db.LinLinea.Where(x => x.activo == true).ToList();
                    object json = new { data = lineas };
                    return json;
                }
                else if (idEquipo == 1008)
                {
                    var reclamos = db.RecReclamoTipoInfra.Where(x => x.activo == true).ToList();
                    object json = new { data = reclamos };
                    return json;
                }
                else
                {
                    return null;
                }
            }

        }


    }
}
