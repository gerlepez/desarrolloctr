﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioTransformador
    {
        public object ObtenerListaTransformador(bool activo)
        {
            List<TransformadorVM> listaTransformador;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                listaTransformador = (from c in db.TranTransformador
                                      join x in db.EstEstacion on c.idEstacion equals x.idEstacion
                                      //join d in db.GralTension on c.tensionEntrada equals d.idTension
                                      //join e in db.GralTension on c.tensionSalida1 equals e.idTension
                                      where c.activo == activo
                                      select new TransformadorVM
                                      {
                                          idTransformador = c.idTransformador,
                                          nombreTransformador = c.nombreTransformador,
                                          idEquipo = c.idTransformador,
                                          nombreEquipo = c.nombreTransformador,
                                          idUsarioAlta = c.idUsuarioAlta,
                                          fechaUsarioAlta = c.fechaUsuarioAlta,
                                          activo = c.activo,
                                          codigoTransformador = c.codigoTransformador,
                                          idPagotran = c.idPagotran,
                                          idBde = c.idBde,
                                          potenciaAparente = c.potenciaAparente,
                                          idCodigoSap = c.codigoSap,
                                          remunerado = c.remunerado,
                                          esRemunerado = c.remunerado == true ? "Si" : "No",

                                          potencias = c.Potencia1 + "/" + c.Potencia2 + "/" + c.Potencia3,
                                          tensiones = c.Tension1 + "/" + c.Tension2 + "/" + c.Tension3,
                                          /*
                                          tensionEntrada = new TensionVM
                                          {
                                              idTension = d.idTension,
                                              tension = d.tension,
                                              activo = d.activo
                                          },*/
                                          /*
                                          tensionSalida1 = new TensionVM
                                          {
                                              idTension = e.idTension,
                                              tension = e.tension,
                                              activo = e.activo
                                          },*/
                                          estacion = new EstacionVM
                                          {
                                              idEstacion = x.idEstacion,
                                              nombreEstacion = x.nombreEstacion,
                                              codigoEstacion = x.codigoEstacion,
                                              activo = x.activo,
                                          }


                                      }).ToList();

                foreach (var item in listaTransformador)
                {
                    var tension2 = (from c in db.GralTension
                                        //join e in db.TranTransformador on c.idTension equals e.tensionSalida2
                                    where c.activo == activo
                                    select new TensionVM
                                    {
                                        idTension = c.idTension,
                                        tension = c.tension,
                                        activo = c.activo
                                    }).FirstOrDefault();
                    if (tension2 != null)
                    {
                        /*
                        item.tensionSalida2 = new TensionVM();
                        item.tensionSalida2 = tension2;*/
                    }
                }


            }
            object json = new { data = listaTransformador };
            return json;
        }

        public object ObtenerTransformadoresPorEstacion(int idEstacion, int idNovedad = 0)
        {
            List<TransformadorVM> listaTransformador;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                if (idNovedad == 0)
                {
                    listaTransformador = (from c in db.TranTransformador
                                          join x in db.EstEstacion on c.idEstacion equals x.idEstacion
                                          // join d in db.GralTension on c.tensionEntrada equals d.idTension
                                          //join e in db.GralTension on c.tensionSalida1 equals e.idTension
                                          where c.activo == true && x.idEstacion == idEstacion
                                          select new TransformadorVM
                                          {
                                              idTransformador = c.idTransformador,
                                              idEquipo = c.idTransformador,
                                              nombreTransformador = c.nombreTransformador,
                                              nombreEquipo = c.nombreTransformador,
                                              idUsarioAlta = c.idUsuarioAlta,
                                              fechaUsarioAlta = c.fechaUsuarioAlta,
                                              activo = c.activo,
                                              codigoTransformador = c.codigoTransformador,
                                              idPagotran = c.idPagotran,
                                              idBde = c.idBde,
                                              potenciaAparente = c.potenciaAparente,
                                              idGeneral = c.idTransformador,
                                              codigoEquipo = "TRA",
                                              /*  tensionEntrada = new TensionVM
                                                {
                                                    idTension = d.idTension,
                                                    tension = d.tension,
                                                    activo = d.activo
                                                },
                                                /*
                                                tensionSalida1 = new TensionVM
                                                {
                                                    idTension = e.idTension,
                                                    tension = e.tension,
                                                    activo = e.activo
                                                },*/
                                              estacion = new EstacionVM
                                              {
                                                  idEstacion = x.idEstacion,
                                                  nombreEstacion = x.nombreEstacion,
                                                  codigoEstacion = x.codigoEstacion,
                                                  activo = x.activo,
                                              }


                                          }).ToList();

                    foreach (var item in listaTransformador)
                    {
                        var tension2 = (from c in db.GralTension
                                            //join e in db.TranTransformador on c.idTension equals e.tensionSalida2
                                        where c.activo == true
                                        select new TensionVM
                                        {
                                            idTension = c.idTension,
                                            tension = c.tension,
                                            activo = c.activo
                                        }).FirstOrDefault();
                        if (tension2 != null)
                        {/*
                        item.tensionSalida2 = new TensionVM();
                        item.tensionSalida2 = tension2;*/
                        }
                    }
                }
                else
                {
                    listaTransformador = (from c in db.TranTransformador
                                          join x in db.EstEstacion on c.idEstacion equals x.idEstacion
                                          join n in db.NovNovedad on idNovedad equals n.idNovedad
                                          // join d in db.GralTension on c.tensionEntrada equals d.idTension
                                          //join e in db.GralTension on c.tensionSalida1 equals e.idTension
                                          where x.idEstacion == idEstacion && c.fechaCreacion < n.horaNovedad && (c.fechaEdicion == null || c.fechaEdicion > n.horaNovedad)
                                          select new TransformadorVM
                                          {
                                              idTransformador = c.idTransformador,
                                              idEquipo = c.idTransformador,
                                              nombreTransformador = c.nombreTransformador,
                                              nombreEquipo = c.nombreTransformador,
                                              idUsarioAlta = c.idUsuarioAlta,
                                              fechaUsarioAlta = c.fechaUsuarioAlta,
                                              activo = c.activo,
                                              codigoTransformador = c.codigoTransformador,
                                              idPagotran = c.idPagotran,
                                              idBde = c.idBde,
                                              potenciaAparente = c.potenciaAparente,
                                              idGeneral = c.idTransformador,
                                              codigoEquipo = "TRA",
                                              /*  tensionEntrada = new TensionVM
                                                {
                                                    idTension = d.idTension,
                                                    tension = d.tension,
                                                    activo = d.activo
                                                },
                                                /*
                                                tensionSalida1 = new TensionVM
                                                {
                                                    idTension = e.idTension,
                                                    tension = e.tension,
                                                    activo = e.activo
                                                },*/
                                              estacion = new EstacionVM
                                              {
                                                  idEstacion = x.idEstacion,
                                                  nombreEstacion = x.nombreEstacion,
                                                  codigoEstacion = x.codigoEstacion,
                                                  activo = x.activo,
                                              }


                                          }).ToList();

                    foreach (var item in listaTransformador)
                    {
                        var tension2 = (from c in db.GralTension
                                            //join e in db.TranTransformador on c.idTension equals e.tensionSalida2
                                        where c.activo == true
                                        select new TensionVM
                                        {
                                            idTension = c.idTension,
                                            tension = c.tension,
                                            activo = c.activo
                                        }).FirstOrDefault();
                        if (tension2 != null)
                        {/*
                        item.tensionSalida2 = new TensionVM();
                        item.tensionSalida2 = tension2;*/
                        }
                    }
                }



            }
            object json = new { data = listaTransformador };
            return json;
        }
        public object ObtenerObjetoTransformadorPorEstacion(int idEstacion)
        {
            List<TransformadorVM> listaTransformador;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                listaTransformador = (from c in db.TranTransformador
                                      join x in db.EstEstacion on c.idEstacion equals x.idEstacion
                                      join d in db.GralTension on c.tensionEntrada equals d.idTension
                                      //join e in db.GralTension on c.tensionSalida1 equals e.idTension
                                      where c.activo == true && x.idEstacion == idEstacion
                                      select new TransformadorVM
                                      {
                                          idTransformador = c.idTransformador,
                                          nombreTransformador = c.nombreTransformador,
                                          idUsarioAlta = c.idUsuarioAlta,
                                          fechaUsarioAlta = c.fechaUsuarioAlta,
                                          activo = c.activo,
                                          codigoTransformador = c.codigoTransformador,
                                          idPagotran = c.idPagotran,
                                          idBde = c.idBde,
                                          potenciaAparente = c.potenciaAparente,
                                          idGeneral = c.idTransformador,
                                          descripcion = c.nombreTransformador,
                                          esAutomatismo = c.tieneAutomatismo == null ? false : c.tieneAutomatismo,
                                          tensionEntrada = new TensionVM
                                          {
                                              idTension = d.idTension,
                                              tension = d.tension,
                                              activo = d.activo
                                          },
                                          estacion = new EstacionVM
                                          {
                                              idEstacion = x.idEstacion,
                                              nombreEstacion = x.nombreEstacion,
                                              codigoEstacion = x.codigoEstacion,
                                              activo = x.activo,
                                          }


                                      }).ToList();

                foreach (var item in listaTransformador)
                {
                    var tension2 = (from c in db.GralTension
                                        // join e in db.TranTransformador on c.idTension equals e.tensionSalida2
                                    where c.activo == true
                                    select new TensionVM
                                    {
                                        idTension = c.idTension,
                                        tension = c.tension,
                                        activo = c.activo
                                    }).FirstOrDefault();
                    if (tension2 != null)
                    {/*
                        item.tensionSalida2 = new TensionVM();
                        item.tensionSalida2 = tension2;*/
                    }
                }


            }
            object json = new { data = listaTransformador };
            return json;
        }
        public int AltaTransformador(TransformadorVM transformador, int idUsuario)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var nombreTrafo = transformador.estacion.nombreEstacion + " " + transformador.tension1 + "/" + transformador.tension2 + "/" + transformador.tension3 + " " + transformador.potenciaAparente + transformador.codigoTransformador;
                var trafoRepetido = db.TranTransformador.Where(x => x.nombreTransformador == nombreTrafo).FirstOrDefault();

                var consultaunTransformador = db.TranTransformador.Where(x => x.idTransformador == transformador.idTransformador).FirstOrDefault();
                if (consultaunTransformador == null && trafoRepetido == null)
                {
                    try
                    {
                        TranTransformador trans = new TranTransformador
                        {
                            activo = true,
                            idUsuarioAlta = idUsuario,
                            fechaUsuarioAlta = DateTime.Now,
                            //tensionEntrada = transformador.tension1,
                            // tensionSalida1 = transformador.tension2,
                            //tensionSalida2 = transformador.tension3,
                            idEstacion = transformador.estacion.idEstacion,
                            codigoTransformador = transformador.codigoTransformador,
                            idPagotran = transformador.idPagotran,
                            idBde = transformador.idBde,
                            potenciaAparente = transformador.potenciaAparente,
                            idTipoEquipo = 1,
                            Tension1 = transformador.tension1,
                            Tension2 = transformador.tension2,
                            Tension3 = transformador.tension3,
                            Potencia1 = transformador.potencia1,
                            Potencia2 = transformador.potencia2,
                            Potencia3 = transformador.potencia3,
                            fechaCreacion = DateTime.Now,
                            nombreTransformador = transformador.estacion.nombreEstacion + " " + transformador.tension1 + "/" + transformador.tension2 + "/" + transformador.tension3 + " " + transformador.potenciaAparente + transformador.codigoTransformador,
                            remunerado = transformador.remunerado,
                        };
                        /*
                        if (transformador.tensionSalida2.idTension == -2)
                        {
                            trans.nombreTransformador = transformador.estacion.nombreEstacion + " " + transformador.tensionEntrada.tension + "/" + transformador.tensionSalida1.tension + " " + transformador.potenciaAparente + " " + transformador.codigoTransformador;
                            trans.tensionSalida2 = null;
                        }
                        else
                        {
                            trans.nombreTransformador = transformador.estacion.nombreEstacion + " " + transformador.tensionEntrada.tension + "/" + transformador.tensionSalida1.tension + "/" + transformador.tensionSalida2.tension + " " + transformador.potenciaAparente + " " + transformador.codigoTransformador;
                            trans.tensionSalida2 = transformador.tensionSalida2.idTension;

                        }
                        */
                        db.TranTransformador.Add(trans);
                        db.SaveChanges();


                        if (transformador.remunerado == true)
                        {
                            IndisponibilidadRemuTrafo TrafoRemu = new IndisponibilidadRemuTrafo
                            {
                                idTransformador = trans.idTransformador,
                                activo = true,
                            };
                            db.IndisponibilidadRemuTrafo.Add(TrafoRemu);
                            db.SaveChanges();
                        }
                        return 200;
                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //ya existe ese dato
                }
            }
        }

        public int ModificarTransformador(TransformadorVM transformador, int idUsuario)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunTransformador = db.TranTransformador.Where(x => x.idTransformador == transformador.idTransformador).FirstOrDefault();

                if (consultaunTransformador != null)
                {
                    //consultaunTransformador.activo = true;
                    //consultaunTransformador.idUsuarioModifica = idUsuario;
                    //consultaunTransformador.fechaUsuarioModifica = DateTime.Now;
                    ////consultaunTransformador.tensionEntrada = transformador.tensionEntrada.idTension;
                    ////consultaunTransformador.tensionSalida1 = transformador.tensionSalida1.idTension;
                    //consultaunTransformador.idEstacion = transformador.estacion.idEstacion;
                    //consultaunTransformador.codigoTransformador = transformador.codigoTransformador;
                    //consultaunTransformador.idPagotran = transformador.idPagotran;
                    //consultaunTransformador.idBde = transformador.idBde;
                    //consultaunTransformador.potenciaAparente = transformador.potenciaAparente;

                    //consultaunTransformador.Tension1 = transformador.tension1;
                    //consultaunTransformador.Tension2 = transformador.tension2;
                    //consultaunTransformador.Tension3 = transformador.tension3;

                    //consultaunTransformador.Potencia1 = transformador.potencia1;
                    //consultaunTransformador.Potencia2 = transformador.potencia2;
                    //consultaunTransformador.Potencia3 = transformador.potencia3;

                    //consultaunTransformador.nombreTransformador = transformador.estacion.nombreEstacion + " " + transformador.tension1 + "/" + transformador.tension2 + "/" + transformador.tension3 + " " + transformador.potenciaAparente + transformador.codigoTransformador;

                    /*
                    if (transformador.tensionSalida2.idTension == -2)
                    {
                        consultaunTransformador.nombreTransformador = transformador.estacion.nombreEstacion + " " + transformador.tensionEntrada.tension + "/" + transformador.tensionSalida1.tension + " " + transformador.potenciaAparente + " " + transformador.codigoTransformador;
                        consultaunTransformador.tensionSalida2 = null;
                    }
                    else
                    {
                        consultaunTransformador.nombreTransformador = transformador.estacion.nombreEstacion + " " + transformador.tensionEntrada.tension + "/" + transformador.tensionSalida1.tension + "/" + transformador.tensionSalida2.tension + " " + transformador.potenciaAparente + " " + transformador.codigoTransformador;
                        consultaunTransformador.tensionSalida2 = transformador.tensionSalida2.idTension;

                    }*/

                    consultaunTransformador.activo = false;
                    consultaunTransformador.fechaEdicion = DateTime.Now;
                    consultaunTransformador.idUsuarioModifica = idUsuario;

                    if (transformador.remunerado == true)
                    {
                        consultaunTransformador.remunerado = false;
                    }

                    TranTransformador trans = new TranTransformador
                    {
                        activo = true,
                        idUsuarioAlta = idUsuario,
                        fechaUsuarioAlta = DateTime.Now,
                        // tensionEntrada = transformador.tensionEntrada.idTension,
                        //   tensionSalida1 = transformador.tensionSalida1.idTension,
                        idEstacion = transformador.estacion.idEstacion,
                        codigoTransformador = transformador.codigoTransformador,
                        idPagotran = transformador.idPagotran,
                        idBde = transformador.idBde,
                        potenciaAparente = transformador.potenciaAparente,
                        idTipoEquipo = 1,
                        Tension1 = transformador.tension1,
                        Tension2 = transformador.tension2,
                        Tension3 = transformador.tension3,
                        Potencia1 = transformador.potencia1,
                        Potencia2 = transformador.potencia2,
                        Potencia3 = transformador.potencia3,
                        fechaCreacion = DateTime.Now,
                        nombreTransformador = transformador.estacion.nombreEstacion + " " + transformador.tension1 + "/" + transformador.tension2 + "/" + transformador.tension3 + " " + transformador.potenciaAparente + transformador.codigoTransformador,
                        remunerado = transformador.remunerado,
                    };
                    db.TranTransformador.Add(trans);
                    db.SaveChanges();

                    var ultimoTransf = db.TranTransformador.OrderByDescending(x => x.idTransformador).FirstOrDefault();

                    var AutoRelacion = db.AutRelacionAutomatismo.Where(x => x.idEquipo == consultaunTransformador.idTransformador && x.idTipoEquipo == 1 && x.idLugar == consultaunTransformador.idEstacion).ToList();
                    foreach (var TransRelac in AutoRelacion)
                    {
                        if (TransRelac.activo != false)
                        {
                            AutRelacionAutomatismo a = new AutRelacionAutomatismo
                            {
                                idAutomatismo = TransRelac.idAutomatismo,
                                idTipoEquipo = TransRelac.idTipoEquipo,
                                idLugar = TransRelac.idLugar,
                                idEquipo = ultimoTransf.idTransformador,
                                activo = true,
                            };
                            db.AutRelacionAutomatismo.Add(a);
                            TransRelac.activo = false;
                            db.SaveChanges();

                        }
                    }


                    var trafoRemunerado = db.IndisponibilidadRemuTrafo.Where(x => x.idTransformador == transformador.idTransformador && x.activo == true).FirstOrDefault();
                    decimal? cargoxHora = null;
                    decimal? Mesx = null;
                    if (transformador.remunerado == true)
                    {

                        if (trafoRemunerado != null)
                        {
                            trafoRemunerado.activo = false;

                            if (trafoRemunerado.remunerado != null)
                            {
                                var monto = db.IndisponibilidadMontoTrafo.Where(x => x.activo == true).FirstOrDefault();
                                cargoxHora = (trans.potenciaAparente * monto.monto * trafoRemunerado.remunerado) / 100;

                                var fechaHoy = new DateTime();
                                var añoHoy = fechaHoy.Year;
                                var mesHoy = fechaHoy.Month;

                                int cantidadDias = DateTime.DaysInMonth(fechaHoy.Year, fechaHoy.Month);
                                Mesx = cargoxHora * (cantidadDias * 24);
                            }


                            IndisponibilidadRemuTrafo TrafoRemu = new IndisponibilidadRemuTrafo
                            {
                                remunerado = trafoRemunerado.remunerado,
                                origenRegulatorio = trafoRemunerado.origenRegulatorio,
                                idTransformador = trans.idTransformador,
                                cargoRealxHoras = cargoxHora,
                                xMes = Mesx,
                                activo = true,
                            };
                            db.IndisponibilidadRemuTrafo.Add(TrafoRemu);
                            db.SaveChanges();

                            var equipoIndisponible = db.IndisponibilidadTrafos.Where(x => x.idTrafoRemunerado == trafoRemunerado.idIndisponibilidadesTrafo && x.idTrafo == consultaunTransformador.idTransformador).FirstOrDefault();
                            if (equipoIndisponible != null)
                            {
                                ServicioIndisponibilidadesRemu servIndis = new ServicioIndisponibilidadesRemu();
                                equipoIndisponible.idTrafoRemunerado = TrafoRemu.idIndisponibilidadesTrafo;
                                equipoIndisponible.idTrafo = trans.idTransformador;
                                db.SaveChanges();
                                //ver
                                servIndis.ModificoTrafoIndisponibles(equipoIndisponible, 0);
                            }


                        }
                        else //Si no existe el trafo en indisponibilidades , lo creo , PERO SIN LA REMUNERADO % 
                        {
                            IndisponibilidadRemuTrafo TrafoRemu = new IndisponibilidadRemuTrafo
                            {
                                idTransformador = trans.idTransformador,
                                activo = true,
                            };
                            db.IndisponibilidadRemuTrafo.Add(TrafoRemu);
                            db.SaveChanges();
                        }

                    }

                }
            }
            return 200;
        }

        public int EliminarTransformador(TransformadorVM transformador)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunTransformador = db.TranTransformador.Where(x => x.idTransformador == transformador.idTransformador).FirstOrDefault();
                if (consultaunTransformador != null)
                {
                    consultaunTransformador.activo = false;
                    db.SaveChanges();
                }
            }
            return 200;
        }

        public bool ValidarTransformador(TransformadorVM transformador)
        {
            if (transformador.estacion.idEstacion > 0 &&
                transformador.potenciaAparente > 0 /* &&
                transformador.tensionEntrada.idTension > 0 &&
                transformador.tensionSalida1.idTension > 0 &&
                (transformador.tensionSalida2.idTension > 0 || transformador.tensionSalida2.idTension == -2)*/)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public TransformadorVM ConsultarUnTransformador(int? idTransformador)
        {
            TransformadorVM transformador = new TransformadorVM();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                transformador = (from c in db.TranTransformador
                                 join x in db.EstEstacion on c.idEstacion equals x.idEstacion
                                 //join d in db.GralTension on c.tensionEntrada equals d.idTension
                                 // join e in db.GralTension on c.tensionSalida1 equals e.idTension
                                 where c.idTransformador == idTransformador
                                 select new TransformadorVM
                                 {
                                     idTransformador = c.idTransformador,
                                     nombreTransformador = c.nombreTransformador,
                                     nombreEquipo = "Transformador",
                                     idUsarioAlta = c.idUsuarioAlta,
                                     fechaUsarioAlta = c.fechaUsuarioAlta,
                                     activo = c.activo,
                                     codigoTransformador = c.codigoTransformador,
                                     idPagotran = c.idPagotran,
                                     idBde = c.idBde,
                                     potenciaAparente = c.potenciaAparente,
                                     tension1 = c.Tension1,
                                     tension2 = c.Tension2,
                                     tension3 = c.Tension3,
                                     potencia1 = c.Potencia1,
                                     potencia2 = c.Potencia2,
                                     potencia3 = c.Potencia2,
                                     remunerado = c.remunerado,
                                     /* tensionEntrada = new TensionVM
                                      {
                                          idTension = d.idTension,
                                          tension = d.tension,
                                          activo = d.activo
                                      },/*
                                      tensionSalida1 = new TensionVM
                                      {
                                          idTension = e.idTension,
                                          tension = e.tension,
                                          activo = e.activo
                                      },*/
                                     estacion = new EstacionVM
                                     {
                                         idEstacion = x.idEstacion,
                                         nombreEstacion = x.nombreEstacion,
                                         codigoEstacion = x.codigoEstacion,
                                         activo = x.activo,
                                     }


                                 }).FirstOrDefault();

                /*var tension2 = (from gt in db.GralTension
                               // join tt in db.TranTransformador on gt.idTension equals tt.tensionSalida2
                                where gt.activo == true // && tt.idTransformador == transformador.idTransformador
                                select new TensionVM
                                {
                                    idTension = gt.idTension,
                                    tension = gt.tension,
                                    activo = gt.activo
                                }).FirstOrDefault();*/
                /*
                if (tension2 != null)
                {
                    transformador.tensionSalida2 = new TensionVM();
                    transformador.tensionSalida2 = tension2;
                }
                else
                {
                    transformador.tensionSalida2 = new TensionVM();
                    transformador.tensionSalida2.idTension = -2;

                }
                */

            }
            return transformador;
        }

        public int RecuperarTransformador(TransformadorVM transformador)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunTransformador = db.TranTransformador.Where(x => x.idTransformador == transformador.idTransformador).FirstOrDefault();
                if (consultaunTransformador != null)
                {
                    consultaunTransformador.activo = true;
                    db.SaveChanges();
                }
            }
            return 200;
        }

        public List<IndisponibilidadVM> ConsultarIndisponibilidadTransformador(int idTransformador, int? idNovedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                List<IndisponibilidadVM> listaDisponibilidad = new List<IndisponibilidadVM>();
                var consultaunTransformador = db.TranTransformador.Where(x => x.idTransformador == idTransformador).FirstOrDefault();
                if (consultaunTransformador != null)
                {
                    var NomIndisponiblidad = db.TranTipoIndisponibilidad.Where(x => x.activo == true).ToList();
                    var cantTension = 0;
                    if (consultaunTransformador.Tension1 != null)
                    {

                        listaDisponibilidad.Add(new IndisponibilidadVM
                        {
                            idIndisponibilidad = NomIndisponiblidad[cantTension].idTipoIndisponibilidad,
                            nombreIndisponibilidad = NomIndisponiblidad[cantTension].nombreTipoIndisponibilidad
                        });
                        cantTension++;
                    }

                    if (consultaunTransformador.Tension2 != null)
                    {
                        listaDisponibilidad.Add(new IndisponibilidadVM
                        {
                            idIndisponibilidad = NomIndisponiblidad[cantTension].idTipoIndisponibilidad,
                            nombreIndisponibilidad = NomIndisponiblidad[cantTension].nombreTipoIndisponibilidad
                        });
                        cantTension++;
                    }
                    if (consultaunTransformador.Tension3 != null)
                    {
                        listaDisponibilidad.Add(new IndisponibilidadVM
                        {
                            idIndisponibilidad = NomIndisponiblidad[cantTension].idTipoIndisponibilidad,
                            nombreIndisponibilidad = NomIndisponiblidad[cantTension].nombreTipoIndisponibilidad
                        });
                        cantTension++;
                    }

                    if (idNovedad == -1)
                    {
                        var indisponible = db.NovNovedadIndisponibilidad.Where(x => x.activo == true && x.idEquipo == idTransformador).ToList();
                        //esto evitaba que aparecieran las indisponibilidades del transformador, pero ahora no hace revision para los que si tienen indisponibilidad
                        /*if (indisponible.Count() > 0)
                        {
                            listaDisponibilidad = new List<IndisponibilidadVM>();
                            foreach (var item in indisponible)
                            {
                                listaDisponibilidad.Add(new IndisponibilidadVM
                                {
                                    idIndisponibilidad = -1,
                                    nombreIndisponibilidad = "Transformador indisponible al " + Math.Round(Convert.ToDouble(item.PorcentajeIndisponible), 2) + "%",
                                });
                            }
                        }*/
                    }

                }
                return listaDisponibilidad;
            }
        }
    }
}
