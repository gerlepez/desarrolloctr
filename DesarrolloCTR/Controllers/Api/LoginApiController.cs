﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;

namespace DesarrolloCTR.Controllers.Api
{
    public class LoginApiController : ApiController
    {
        [System.Web.Http.Route("Api/Login/ObtenerUsuarioLogueado")]
        [System.Web.Http.ActionName("ObtenerUsuarioLogueado")]
        [System.Web.Http.HttpGet]
        public int ObtenerUsuarioLogueado(string nombreUsuario)
        {
            ServicioUsuario servUsuario = new ServicioUsuario();
            var resultVal = servUsuario.ObtenerUsuarioLogueado(nombreUsuario);

            return resultVal;
        }

        [System.Web.Http.Route("Api/Login/ValidarDatos")]
        [System.Web.Http.ActionName("ValidarDatos")]
        [System.Web.Http.HttpGet]
        public int ValidarDatos(string nombreUsuario, string password)
        {
            using (BuhoGestionEntities db = new BuhoGestionEntities())
            {
                var user = db.GralUsuario.Where(x => x.usuario == nombreUsuario).FirstOrDefault();
                if (user == null)
                {
                    return 0;
                }
                else
                {
                    if (user.password != password)
                    {
                        return 1;
                    }
                    else
                    {
                        return 2;
                    }
                }
            }
        }

        [System.Web.Http.Route("Api/Login/ObtenerLoginUsuario")]
        [System.Web.Http.ActionName("ObtenerLoginUsuario")]
        [System.Web.Http.HttpGet]
        public string ObtenerLoginUsuario(int nombreUsuario, string pass)
        {
            ServicioUsuario servUsuario = new ServicioUsuario();
            var resultVal = servUsuario.ObtenerLoginUsuario(nombreUsuario, pass);
            return resultVal;
        }
        [System.Web.Http.Route("Api/Login/ValidarIngreso")]
        [System.Web.Http.ActionName("ValidarIngreso")]
        [System.Web.Http.HttpGet]
        public Task<int> ValidarIngreso(string nombreUsuario, string pass)
        {
            ServicioUsuario servUsuario = new ServicioUsuario();
            var resultVal = servUsuario.ValidarIngreso(nombreUsuario, pass);
            return resultVal;
        }

        [System.Web.Http.Route("Api/Login/SetUserSessionOut")]
        [System.Web.Http.ActionName("SetUserSessionOut")]
        [System.Web.Http.HttpGet]
        public bool SetUserSessionOut()
        {
            ServicioUsuario servUsuario = new ServicioUsuario();
            var resultVal = servUsuario.SetUserSessionOut();

            return resultVal;
        }
    }
}
