﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioPropiedad
    {
        public object ObtenerPropiedad(bool? activa)
        {
            List<PropiedadVM> listaPropiedad;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                listaPropiedad = (from p in db.ProPropiedad
                                  where p.activa == activa
                                  select new PropiedadVM
                                  {
                                      idPropiedad = p.idPropiedad,
                                      nombrePropiedad = p.nombrePropiedad,
                                      activo = p.activa,

                                  }).ToList();

            }
            object json = new { data = listaPropiedad };
            return json;
        }

        public object TablaPropiedad(bool? activa, string draw, int recordsTotal, int skip, int pageSize)
        {
            try
            {

                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    IQueryable<PropiedadVM> query = (from p in db.ProPropiedad
                                                     where p.activa == activa
                                                     select new PropiedadVM
                                                     {
                                                         idPropiedad = p.idPropiedad,
                                                         nombrePropiedad = p.nombrePropiedad,
                                                         activo = p.activa,

                                                     }).OrderByDescending(x => x.idPropiedad).AsQueryable();

                    var queryFiltrada = query;
                    recordsTotal = queryFiltrada.Count();
                    var lst = queryFiltrada.Skip(skip).Take(pageSize).ToList();

                    object json = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = lst };
                    return json;
                }
            }
            catch (Exception e)
            {
                return 400;
            }
        }


        public int AltaPropiedad(PropiedadVM propiedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnaPropiedad = db.ProPropiedad.Where(x => x.idPropiedad == propiedad.idPropiedad).FirstOrDefault();
                var propiedadAnterior = db.ProPropiedad.Where(x => x.nombrePropiedad == propiedad.nombrePropiedad).FirstOrDefault();
                if (consultaUnaPropiedad == null && propiedadAnterior == null)
                {
                    try
                    {
                        ProPropiedad p = new ProPropiedad
                        {
                            nombrePropiedad = propiedad.nombrePropiedad,
                            activa = true,
                        };
                        db.ProPropiedad.Add(p);
                        db.SaveChanges();
                        return 200;
                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }

            }
        }

        public int ModificarPropiedad(PropiedadVM propiedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnaPropiedad = db.ProPropiedad.Where(x => x.idPropiedad == propiedad.idPropiedad).FirstOrDefault();
                if (consultaUnaPropiedad != null)
                {
                    consultaUnaPropiedad.nombrePropiedad = propiedad.nombrePropiedad;
                    consultaUnaPropiedad.activa = true;
                    db.SaveChanges();
                }
            }
            return 200;
        }


        public int EliminarPropiedad(PropiedadVM propiedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnaPropiedad = db.ProPropiedad.Where(x => x.idPropiedad == propiedad.idPropiedad).FirstOrDefault();
                if (consultaUnaPropiedad != null)
                {
                    consultaUnaPropiedad.activa = false;
                }
                db.SaveChanges();
            }
            return 200;
        }

        public PropiedadVM ConsultarUnaPropiedad(int? idPropiedad)
        {
            PropiedadVM propiedad = new PropiedadVM();

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                propiedad = (from p in db.ProPropiedad
                             where p.idPropiedad == idPropiedad
                             select new PropiedadVM()
                             {
                                 idPropiedad = p.idPropiedad,
                                 nombrePropiedad = p.nombrePropiedad,
                                 activo = p.activa
                             }).FirstOrDefault();
                return propiedad;
            }
        }


        public int RecuperarPropiedad(PropiedadVM propiedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())

            {
                var consultaUnaPropiedad = db.ProPropiedad.Where(x => x.idPropiedad == propiedad.idPropiedad).FirstOrDefault();
                var mismaPropiedad = db.ProPropiedad.Where(x => x.nombrePropiedad == propiedad.nombrePropiedad && x.activa == true).FirstOrDefault();
                if (consultaUnaPropiedad != null)
                {
                    if (mismaPropiedad == null)
                    {
                        consultaUnaPropiedad.activa = true;
                        db.SaveChanges();
                        return 200;
                    }
                    else
                    {
                        return 300;
                    }
                }
                else
                {
                    return 400;
                }

            }

        }

        public bool ValidarPropiedad(PropiedadVM propiedad)
        {
            if (!string.IsNullOrWhiteSpace(propiedad.nombrePropiedad.ToString()))
            {
                return true;
            }
            else
            {
                return false;
            }

        }


    }
}
