﻿using System;

namespace DesarrolloCTRServicios.ViewModels
{
    public class IndisponibilidadVM
    {
        public string descripcion { get; set; }
        public int? idIndisponibilidad { get; set; }
        public int? idTipoIndisponibilidad { get; set; }
        public int? idTipoEquipo { get; set; }
        public int codigoRetorno { get; set; }
        public int? idActuacion { get; set; }
        public int? acc { get; set; } //preguntar al PO
        public int? idNovedad { get; set; }
        public int? idEquipo { get; set; }
        public int? itn { get; set; }
        public int? idNovedadIndisponibilidad { get; set; }
        public string codEquipo { get; set; }
        public bool? fueraDeServicio { get; set; }
        public double? porcentajeIndisponibilidad { get; set; }

        public string nombreIndisponibilidad { get; set; }
        public string codigoNovedad { get; set; }
        public string fechaSalidaStr { get; set; }
        public string horaSalidaStr { get; set; }
        public string minutosSalidaStr { get; set; }
        public string fechaEntradaStr { get; set; }
        public string horaEntradaStr { get; set; }
        public string minutosEntradaStr { get; set; }
        public string detalleIndisponiblidad { get; set; }
        public string fechaInformeEntradaStr { get; set; }
        public string horaInformeEntradaStr { get; set; }
        public string minutosInformeEntradaStr { get; set; }
        public string nombretipoEquipo { get; set; }
        public string nombretipoIndisponibilidad { get; set; }

        public bool? activo { get; set; }
        public bool? estadoIndisp { get; set; }
        public bool? energiaNoSuministrada { get; set; }
        public bool? informoEnTermino { get; set; }

        public EquipoVM equipo { get; set; }
        public EquipoDetalleEstacionVM detalleEquipo { get; set; }
        public NovedadVM novedad { get; set; }
        public ActuacionVM actuacion { get; set; }

        public DateTime? fechaSalida { get; set; }
        public DateTime? horaSalida { get; set; }
        public DateTime? fechaEntrada { get; set; }
        public DateTime? horaEntrada { get; set; }
        public DateTime? fechaInformeEntrada { get; set; }
        public DateTime? horaInformeEntrada { get; set; }

    }
}
