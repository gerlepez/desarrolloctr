﻿using System.Collections.Generic;

namespace DesarrolloCTRServicios.ViewModels
{
    public class RolVM
    {
        public int? idRol { get; set; }
        public string nombreRol { get; set; }
        public int codigoRetorno { get; set; }
        public int? idMenu { get; set; }
        public int? idAccesoRol { get; set; }

        public bool? activo { get; set; }
        public bool? tienePermisoLectura { get; set; }
        public bool? tienePermisoEliminacion { get; set; }
        public bool? tienePermisoEdicion { get; set; }
        public bool? esJefe { get; set; }
        public bool? esOperador { get; set; }

        public List<MenuVM> menu { get; set; }
        public MenuVM menuu { get; set; }

    }
}
