﻿using DesarrolloCTR.Controllers.Extra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers
{
    [SessionUtility]
    [Permiso("PROP")]
    public class PropiedadController : Controller
    {
        // GET: Propiedad
        public ActionResult Propiedad()
        {
            return View();
        }
        public ActionResult AdministrarPropiedad()
        {
            return View();
        }
    }
}
