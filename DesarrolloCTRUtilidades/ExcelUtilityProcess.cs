﻿using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DesarrolloCTRUtilidades
{
    public class ExcelUtilityProcess
    {
        public DataTable Excel_To_DataTable(string pRutaArchivo, int pHojaIndex)
        {
            // --------------------------------- //
            /* REFERENCIAS:
                * NPOI.dll
                * NPOI.OOXML.dll
                * NPOI.OpenXml4Net.dll */
            // --------------------------------- //
            /* USING:
                * using NPOI.SS.UserModel;
                * using NPOI.HSSF.UserModel;
                * using NPOI.XSSF.UserModel; */
            // AUTOR: Ing. Jhollman Chacon R. 2015
            // --------------------------------- //
            DataTable Tabla = null;
            try
            {
                if (File.Exists(pRutaArchivo))
                {

                    IWorkbook workbook = null;  //IWorkbook determina si es xls o xlsx              
                    ISheet worksheet = null;
                    string first_sheet_name = "";

                    using (FileStream FS = new FileStream(pRutaArchivo, FileMode.Open, FileAccess.Read))
                    {
                        workbook = WorkbookFactory.Create(FS);          //Abre tanto XLS como XLSX
                        worksheet = workbook.GetSheetAt(pHojaIndex);    //Obtener Hoja por indice
                        first_sheet_name = worksheet.SheetName;         //Obtener el nombre de la Hoja

                        Tabla = new DataTable(first_sheet_name);
                        Tabla.Rows.Clear();
                        Tabla.Columns.Clear();

                        // Leer Fila por fila desde la primera
                        for (int rowIndex = 0; rowIndex <= worksheet.LastRowNum; rowIndex++)
                        {
                            DataRow NewReg = null;
                            IRow row = worksheet.GetRow(rowIndex);
                            IRow row2 = null;
                            IRow row3 = null;

                            if (rowIndex == 0)
                            {
                                row2 = worksheet.GetRow(rowIndex + 1); //Si es la Primera fila, obtengo tambien la segunda para saber el tipo de datos
                                row3 = worksheet.GetRow(rowIndex + 2); //Y la tercera tambien por las dudas
                            }

                            if (row != null) //null is when the row only contains empty cells 
                            {
                                if (rowIndex > 0) NewReg = Tabla.NewRow();

                                int colIndex = 0;
                                //Leer cada Columna de la fila
                                foreach (ICell cell in row.Cells)
                                {
                                    object valorCell = null;
                                    string cellType = "";
                                    string[] cellType2 = new string[2];

                                    if (rowIndex == 0) //Asumo que la primera fila contiene los titlos:
                                    {
                                        for (int i = 0; i < 2; i++)
                                        {
                                            ICell cell2 = null;
                                            if (i == 0) { cell2 = row2.GetCell(cell.ColumnIndex); }
                                            else { cell2 = row3.GetCell(cell.ColumnIndex); }

                                            if (cell2 != null)
                                            {
                                                switch (cell2.CellType)
                                                {
                                                    case CellType.Blank: break;
                                                    case CellType.Boolean: cellType2[i] = "System.Boolean"; break;
                                                    case CellType.String: cellType2[i] = "System.String"; break;
                                                    case CellType.Numeric:
                                                        if (HSSFDateUtil.IsCellDateFormatted(cell2)) { cellType2[i] = "System.DateTime"; }
                                                        else
                                                        {
                                                            cellType2[i] = "System.Double";  //valorCell = cell2.NumericCellValue;
                                                        }
                                                        break;

                                                    case CellType.Formula:
                                                        bool continuar = true;
                                                        switch (cell2.CachedFormulaResultType)
                                                        {
                                                            case CellType.Boolean: cellType2[i] = "System.Boolean"; break;
                                                            case CellType.String: cellType2[i] = "System.String"; break;
                                                            case CellType.Numeric:
                                                                if (HSSFDateUtil.IsCellDateFormatted(cell2)) { cellType2[i] = "System.DateTime"; }
                                                                else
                                                                {
                                                                    try
                                                                    {
                                                                        //DETERMINAR SI ES BOOLEANO
                                                                        if (cell2.CellFormula == "TRUE()") { cellType2[i] = "System.Boolean"; continuar = false; }
                                                                        if (continuar && cell2.CellFormula == "FALSE()") { cellType2[i] = "System.Boolean"; continuar = false; }
                                                                        if (continuar) { cellType2[i] = "System.Double"; continuar = false; }
                                                                    }
                                                                    catch { }
                                                                }
                                                                break;
                                                        }
                                                        break;
                                                    default:
                                                        cellType2[i] = "System.String"; break;
                                                }
                                            }
                                        }

                                        //Resolver las diferencias de Tipos
                                        if (cellType2[0] == cellType2[1]) { cellType = cellType2[0]; }
                                        else
                                        {
                                            if (cellType2[0] == null) cellType = cellType2[1];
                                            if (cellType2[1] == null) cellType = cellType2[0];
                                            if (cellType == "") cellType = "System.String";
                                        }

                                        //Obtener el nombre de la Columna
                                        string colName = "Column_{0}";
                                        try { colName = cell.StringCellValue; }
                                        catch { colName = string.Format(colName, colIndex); }

                                        //Verificar que NO se repita el Nombre de la Columna
                                        foreach (DataColumn col in Tabla.Columns)
                                        {
                                            if (col.ColumnName == colName) colName = string.Format("{0}_{1}", colName, colIndex);
                                        }

                                        //Agregar el campos de la tabla:
                                        DataColumn codigo = new DataColumn(colName, System.Type.GetType(cellType));
                                        Tabla.Columns.Add(codigo); colIndex++;
                                    }
                                    else
                                    {
                                        //Las demas filas son registros:
                                        switch (cell.CellType)
                                        {
                                            case CellType.Blank: valorCell = DBNull.Value; break;
                                            case CellType.Boolean: valorCell = cell.BooleanCellValue; break;
                                            case CellType.String: valorCell = cell.StringCellValue; break;
                                            case CellType.Numeric:
                                                if (HSSFDateUtil.IsCellDateFormatted(cell)) { valorCell = cell.DateCellValue; }
                                                else { valorCell = cell.NumericCellValue; }
                                                break;
                                            case CellType.Formula:
                                                switch (cell.CachedFormulaResultType)
                                                {
                                                    case CellType.Blank: valorCell = DBNull.Value; break;
                                                    case CellType.String: valorCell = cell.StringCellValue; break;
                                                    case CellType.Boolean: valorCell = cell.BooleanCellValue; break;
                                                    case CellType.Numeric:
                                                        if (HSSFDateUtil.IsCellDateFormatted(cell)) { valorCell = cell.DateCellValue; }
                                                        else { valorCell = cell.NumericCellValue; }
                                                        break;
                                                }
                                                break;
                                            default: valorCell = cell.StringCellValue; break;
                                        }
                                        //Agregar el nuevo Registro
                                        if (cell.ColumnIndex <= Tabla.Columns.Count - 1) NewReg[cell.ColumnIndex] = valorCell;
                                    }
                                }
                            }
                            if (rowIndex > 0) Tabla.Rows.Add(NewReg);
                        }
                        Tabla.AcceptChanges();
                    }
                }
                else
                {
                    throw new Exception("ERROR 404: El archivo especificado NO existe.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Tabla;
        }

        /// <summary>Convierte un DataTable en un archivo de Excel (xls o Xlsx) y lo guarda en disco.</summary>
        /// <param name="pDatos">Datos de la Tabla a guardar. Usa el nombre de la tabla como nombre de la Hoja</param>
        /// <param name="pFilePath">Ruta del archivo donde se guarda.</param>
        public MemoryStream DataTable_To_Excel(DataTable pDatos,
                                        //string pFilePath, 
                                        string rutaImagenHeader, string tituloHeader,
                                        int largoImagem, int anchoImagen,
                                        string rutaImagenBuho, int largoBoho)
        {

            MemoryStream output = new MemoryStream();
            try
            {
                if (pDatos != null && pDatos.Rows.Count > 0)
                {
                    IWorkbook workbook = null;
                    ISheet worksheet = null;

                    workbook = new XSSFWorkbook();
                    worksheet = workbook.CreateSheet(pDatos.TableName);

                    //CREAR EN LA PRIMERA FILA LOS TITULOS DE LAS COLUMNAS
                    worksheet.CreateFreezePane(0, anchoImagen + 1, 0, anchoImagen + 1);

                    //byte[] data = File.ReadAllBytes(HttpContext.Current.Server.MapPath(rutaImagenHeader));

                    //int pictureIndex = workbook.AddPicture(data, PictureType.PNG);
                    ICreationHelper helper = workbook.GetCreationHelper();
                    IDrawing drawing = worksheet.CreateDrawingPatriarch();
                    IClientAnchor anchor = helper.CreateClientAnchor();
                    anchor.Col1 = 0;//0 index based column
                    anchor.Row1 = 0;//0 index based row
                    //IPicture picture = drawing.CreatePicture(anchor, pictureIndex);
                    //picture.Resize(largoImagem, anchoImagen);

                    //Agrega el Buho al final del encabezado
                    //data = File.ReadAllBytes(HttpContext.Current.Server.MapPath(rutaImagenBuho));

                    //pictureIndex = workbook.AddPicture(data, PictureType.PNG);
                    helper = workbook.GetCreationHelper();
                    drawing = worksheet.CreateDrawingPatriarch();
                    anchor = helper.CreateClientAnchor();
                    anchor.Col1 = pDatos.Columns.Count - 2;//0 index based column
                    anchor.Row1 = 0;//0 index based row
                    //picture = drawing.CreatePicture(anchor, pictureIndex);
                    //picture.Resize(largoBoho, 2.5);



                    var headerRow = worksheet.CreateRow(0);

                    var RowStyle = workbook.CreateCellStyle();
                    RowStyle.Alignment = HorizontalAlignment.Left;
                    RowStyle.VerticalAlignment = VerticalAlignment.Center;
                    RowStyle.WrapText = true;
                    IFont fontR = workbook.CreateFont();
                    fontR.Boldweight = 3;
                    fontR.FontHeight = 20;
                    RowStyle.SetFont(fontR);

                    var Celda = headerRow.CreateCell(largoImagem);

                    Celda.SetCellValue(tituloHeader);
                    Celda.CellStyle = RowStyle;


                    //NPOI.SS.Util.CellRangeAddress merge1 = new NPOI.SS.Util.CellRangeAddress(0, 2, largoImagem, pDatos.Columns.Count - 1);
                    //worksheet.AddMergedRegion(merge1);

                    //NPOI.SS.Util.CellRangeAddress merge2 = new NPOI.SS.Util.CellRangeAddress(0, 2, 0, largoImagem - 1);
                    //worksheet.AddMergedRegion(merge2);


                    int iRow = anchoImagen;

                    if (pDatos.Columns.Count > 0)
                    {
                        int iCol = 0;

                        var styleHeader = workbook.CreateCellStyle();
                        styleHeader.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
                        styleHeader.FillPattern = FillPattern.SolidForeground;
                        styleHeader.BorderBottom = BorderStyle.Thin;
                        styleHeader.BorderLeft = BorderStyle.Thin;
                        styleHeader.BorderTop = BorderStyle.Thin;
                        styleHeader.BorderRight = BorderStyle.Thin;
                        styleHeader.WrapText = true;
                        styleHeader.VerticalAlignment = VerticalAlignment.Top;

                        IRow fila = worksheet.CreateRow(iRow);
                        foreach (DataColumn columna in pDatos.Columns)
                        {

                            int largoCol = columna.ColumnName.Length;
                            if (largoCol > 15)
                            { largoCol = (largoCol / 2); }
                            else { largoCol = 15; }

                            ICell cell = fila.CreateCell(iCol, CellType.String);
                            cell.SetCellValue(columna.ColumnName);

                            worksheet.SetColumnWidth(iCol, ((largoCol * 256) + 10));
                            cell.CellStyle = styleHeader;
                            iCol++;
                        }
                        iRow++;
                    }

                    //FORMATOS PARA CIERTOS TIPOS DE DATOS
                    #region estilos
                    ICellStyle _doubleCellStyle = workbook.CreateCellStyle();
                    _doubleCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("#,##0.00");
                    _doubleCellStyle.BorderBottom = BorderStyle.Thin;
                    _doubleCellStyle.BorderLeft = BorderStyle.Thin;
                    _doubleCellStyle.BorderTop = BorderStyle.Thin;
                    _doubleCellStyle.BorderRight = BorderStyle.Thin;


                    ICellStyle _intCellStyle = workbook.CreateCellStyle();
                    _intCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("#,##0");
                    _intCellStyle.BorderBottom = BorderStyle.Thin;
                    _intCellStyle.BorderLeft = BorderStyle.Thin;
                    _intCellStyle.BorderTop = BorderStyle.Thin;
                    _intCellStyle.BorderRight = BorderStyle.Thin;

                    //ICellStyle _boolCellStyle = workbook.CreateCellStyle();
                    //_boolCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("BOOLEAN");
                    //_boolCellStyle.BorderBottom = BorderStyle.Thin;
                    //_boolCellStyle.BorderLeft = BorderStyle.Thin;
                    //_boolCellStyle.BorderTop = BorderStyle.Thin;
                    //_boolCellStyle.BorderRight = BorderStyle.Thin;


                    ICellStyle _dateCellStyle = workbook.CreateCellStyle();
                    _dateCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("dd-MM-yyyy");
                    _dateCellStyle.BorderBottom = BorderStyle.Thin;
                    _dateCellStyle.BorderLeft = BorderStyle.Thin;
                    _dateCellStyle.BorderTop = BorderStyle.Thin;
                    _dateCellStyle.BorderRight = BorderStyle.Thin;


                    ICellStyle _dateTimeCellStyle = workbook.CreateCellStyle();
                    _dateTimeCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("dd-MM-yyyy HH:mm:ss");
                    _dateTimeCellStyle.BorderBottom = BorderStyle.Thin;
                    _dateTimeCellStyle.BorderLeft = BorderStyle.Thin;
                    _dateTimeCellStyle.BorderTop = BorderStyle.Thin;
                    _dateTimeCellStyle.BorderRight = BorderStyle.Thin;

                    ICellStyle _stringCellStyle = workbook.CreateCellStyle();
                    _stringCellStyle.BorderBottom = BorderStyle.Thin;
                    _stringCellStyle.BorderLeft = BorderStyle.Thin;
                    _stringCellStyle.BorderTop = BorderStyle.Thin;
                    _stringCellStyle.BorderRight = BorderStyle.Thin;

                    ICellStyle _stringCellStyleAmarillo = workbook.CreateCellStyle();
                    _stringCellStyleAmarillo.BorderBottom = BorderStyle.Thin;
                    _stringCellStyleAmarillo.BorderLeft = BorderStyle.Thin;
                    _stringCellStyleAmarillo.BorderTop = BorderStyle.Thin;
                    _stringCellStyleAmarillo.BorderRight = BorderStyle.Thin;
                    _stringCellStyleAmarillo.FillForegroundColor = HSSFColor.Yellow.Index;
                    _stringCellStyleAmarillo.FillPattern = FillPattern.SolidForeground;

                    ICellStyle _stringCellStyleRojo = workbook.CreateCellStyle();
                    _stringCellStyleRojo.BorderBottom = BorderStyle.Thin;
                    _stringCellStyleRojo.BorderLeft = BorderStyle.Thin;
                    _stringCellStyleRojo.BorderTop = BorderStyle.Thin;
                    _stringCellStyleRojo.BorderRight = BorderStyle.Thin;
                    _stringCellStyleRojo.FillForegroundColor = HSSFColor.Red.Index;
                    _stringCellStyleRojo.FillPattern = FillPattern.SolidForeground;

                    ICellStyle _stringCellStyleVerde = workbook.CreateCellStyle();
                    _stringCellStyleVerde.BorderBottom = BorderStyle.Thin;
                    _stringCellStyleVerde.BorderLeft = BorderStyle.Thin;
                    _stringCellStyleVerde.BorderTop = BorderStyle.Thin;
                    _stringCellStyleVerde.BorderRight = BorderStyle.Thin;
                    _stringCellStyleVerde.FillForegroundColor = HSSFColor.Green.Index;
                    _stringCellStyleVerde.FillPattern = FillPattern.SolidForeground;

                    #endregion


                    //AHORA CREAR UNA FILA POR CADA REGISTRO DE LA TABLA
                    foreach (DataRow row in pDatos.Rows)
                    {
                        IRow fila = worksheet.CreateRow(iRow);
                        int iCol = 0;
                        foreach (DataColumn column in pDatos.Columns)
                        {
                            ICell cell = null; //<-Representa la celda actual                               
                            object cellValue = row[iCol]; //<- El valor actual de la celda

                            switch (column.DataType.Name)
                            {
                                case "Boolean":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.Boolean);

                                        if (Convert.ToBoolean(cellValue)) { cell.SetCellFormula("TRUE()"); }
                                        else { cell.SetCellFormula("FALSE()"); }
                                        //cell.CellStyle = _boolCellStyle;
                                    }
                                    break;

                                case "String":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.String);
                                        cell.SetCellValue(Convert.ToString(cellValue));
                                        cell.CellStyle = _stringCellStyle;
                                    }
                                    break;

                                case "Int32":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.Numeric);
                                        cell.SetCellValue(Convert.ToInt32(cellValue));
                                        cell.CellStyle = _intCellStyle;
                                    }
                                    break;
                                case "Int64":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.Numeric);
                                        cell.SetCellValue(Convert.ToInt64(cellValue));
                                        cell.CellStyle = _intCellStyle;
                                    }
                                    break;
                                case "Decimal":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.Numeric);
                                        cell.SetCellValue(Convert.ToDouble(cellValue));
                                        cell.CellStyle = _doubleCellStyle;
                                    }
                                    break;
                                case "Double":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.Numeric);
                                        cell.SetCellValue(Convert.ToDouble(cellValue));
                                        cell.CellStyle = _doubleCellStyle;
                                    }
                                    break;
                                case "DateTime":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.Numeric);
                                        cell.SetCellValue(Convert.ToDateTime(cellValue));

                                        //Si No tiene valor de Hora, usar formato dd-MM-yyyy
                                        DateTime cDate = Convert.ToDateTime(cellValue);
                                        if (cDate.Hour > 0) { cell.CellStyle = _dateTimeCellStyle; }
                                        else { cell.CellStyle = _dateCellStyle; }
                                    }
                                    break;
                                case "CeldaColor":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.String);
                                        cell.SetCellValue("");

                                        var valor = cellValue as CeldaColor;
                                        switch (valor?.Color)
                                        {
                                            case "Rojo":
                                                cell.CellStyle = _stringCellStyleRojo;
                                                break;
                                            case "Amarillo":
                                                cell.CellStyle = _stringCellStyleAmarillo;
                                                break;
                                            case "Verde":
                                                cell.CellStyle = _stringCellStyleVerde;
                                                break;
                                            default:
                                                cell.CellStyle = _stringCellStyle;
                                                break;
                                        }
                                    }
                                    break;
                                default:
                                    break;
                            }
                            iCol++;
                        }
                        iRow++;
                    }

                    workbook.Write(output);
                    output.Close();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }

        /// <summary>Convierte un DataTable en un archivo de Excel (xls o Xlsx) y lo guarda en disco.</summary>
        /// <param name="pDatos">Datos de la Tabla a guardar. Usa el nombre de la tabla como nombre de la Hoja</param>
        /// <param name="pFilePath">Ruta del archivo donde se guarda.</param>
        public MemoryStream DataTable_To_Excel(DataTable pDatos, string tituloHeader)
        {

            MemoryStream output = new MemoryStream();
            try
            {
                if (pDatos != null && pDatos.Rows.Count > 0)
                {
                    IWorkbook workbook = null;
                    ISheet worksheet = null;

                    workbook = new XSSFWorkbook();
                    worksheet = workbook.CreateSheet(pDatos.TableName);

                    //CREAR EN LA PRIMERA FILA LOS TITULOS DE LAS COLUMNAS
                    ICreationHelper helper = workbook.GetCreationHelper();
                    IDrawing drawing = worksheet.CreateDrawingPatriarch();
                    IClientAnchor anchor = helper.CreateClientAnchor();
                    anchor.Col1 = 0;//0 index based column
                    anchor.Row1 = 0;//0 index based row

                    helper = workbook.GetCreationHelper();
                    drawing = worksheet.CreateDrawingPatriarch();
                    anchor = helper.CreateClientAnchor();
                    anchor.Col1 = pDatos.Columns.Count - 2;//0 index based column
                    anchor.Row1 = 0;//0 index based row                   

                    var headerRow = worksheet.CreateRow(0);

                    var RowStyle = workbook.CreateCellStyle();
                    //RowStyle.Alignment = HorizontalAlignment.Left;
                    //RowStyle.VerticalAlignment = VerticalAlignment.Center;
                    //RowStyle.WrapText = true;
                    IFont fontR = workbook.CreateFont();
                    fontR.Boldweight = 3;
                    fontR.FontHeight = 13;
                    RowStyle.SetFont(fontR);

                    var Celda = headerRow.CreateCell(4);
                    Celda.SetCellValue(tituloHeader);
                    Celda.CellStyle = RowStyle;

                    int iRow = 2;

                    if (pDatos.Columns.Count > 0)
                    {
                        int iCol = 0;

                        var styleHeader = workbook.CreateCellStyle();
                        styleHeader.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
                        styleHeader.FillPattern = FillPattern.SolidForeground;
                        styleHeader.BorderBottom = BorderStyle.Thin;
                        styleHeader.BorderLeft = BorderStyle.Thin;
                        styleHeader.BorderTop = BorderStyle.Thin;
                        styleHeader.BorderRight = BorderStyle.Thin;
                        styleHeader.WrapText = true;
                        styleHeader.VerticalAlignment = VerticalAlignment.Top;

                        IRow fila = worksheet.CreateRow(iRow);
                        foreach (DataColumn columna in pDatos.Columns)
                        {

                            int largoCol = columna.ColumnName.Length;
                            if (largoCol > 15)
                            { largoCol = (largoCol / 2); }
                            else { largoCol = 15; }

                            ICell cell = fila.CreateCell(iCol, CellType.String);
                            cell.SetCellValue(columna.ColumnName);

                            worksheet.SetColumnWidth(iCol, ((largoCol * 256) + 10));
                            cell.CellStyle = styleHeader;
                            iCol++;
                        }
                        iRow++;
                    }

                    //FORMATOS PARA CIERTOS TIPOS DE DATOS
                    #region estilos
                    ICellStyle _doubleCellStyle = workbook.CreateCellStyle();
                    _doubleCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("#,##0.00");
                    _doubleCellStyle.BorderBottom = BorderStyle.Thin;
                    _doubleCellStyle.BorderLeft = BorderStyle.Thin;
                    _doubleCellStyle.BorderTop = BorderStyle.Thin;
                    _doubleCellStyle.BorderRight = BorderStyle.Thin;


                    ICellStyle _intCellStyle = workbook.CreateCellStyle();
                    _intCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("#,##0");
                    _intCellStyle.BorderBottom = BorderStyle.Thin;
                    _intCellStyle.BorderLeft = BorderStyle.Thin;
                    _intCellStyle.BorderTop = BorderStyle.Thin;
                    _intCellStyle.BorderRight = BorderStyle.Thin;

                    //ICellStyle _boolCellStyle = workbook.CreateCellStyle();
                    //_boolCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("BOOLEAN");
                    //_boolCellStyle.BorderBottom = BorderStyle.Thin;
                    //_boolCellStyle.BorderLeft = BorderStyle.Thin;
                    //_boolCellStyle.BorderTop = BorderStyle.Thin;
                    //_boolCellStyle.BorderRight = BorderStyle.Thin;


                    ICellStyle _dateCellStyle = workbook.CreateCellStyle();
                    _dateCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("dd-MM-yyyy");
                    _dateCellStyle.BorderBottom = BorderStyle.Thin;
                    _dateCellStyle.BorderLeft = BorderStyle.Thin;
                    _dateCellStyle.BorderTop = BorderStyle.Thin;
                    _dateCellStyle.BorderRight = BorderStyle.Thin;


                    ICellStyle _dateTimeCellStyle = workbook.CreateCellStyle();
                    _dateTimeCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("dd-MM-yyyy HH:mm:ss");
                    _dateTimeCellStyle.BorderBottom = BorderStyle.Thin;
                    _dateTimeCellStyle.BorderLeft = BorderStyle.Thin;
                    _dateTimeCellStyle.BorderTop = BorderStyle.Thin;
                    _dateTimeCellStyle.BorderRight = BorderStyle.Thin;

                    ICellStyle _stringCellStyle = workbook.CreateCellStyle();
                    _stringCellStyle.BorderBottom = BorderStyle.Thin;
                    _stringCellStyle.BorderLeft = BorderStyle.Thin;
                    _stringCellStyle.BorderTop = BorderStyle.Thin;
                    _stringCellStyle.BorderRight = BorderStyle.Thin;

                    ICellStyle _stringCellStyleAmarillo = workbook.CreateCellStyle();
                    _stringCellStyleAmarillo.BorderBottom = BorderStyle.Thin;
                    _stringCellStyleAmarillo.BorderLeft = BorderStyle.Thin;
                    _stringCellStyleAmarillo.BorderTop = BorderStyle.Thin;
                    _stringCellStyleAmarillo.BorderRight = BorderStyle.Thin;
                    _stringCellStyleAmarillo.FillForegroundColor = HSSFColor.Yellow.Index;
                    _stringCellStyleAmarillo.FillPattern = FillPattern.SolidForeground;

                    ICellStyle _stringCellStyleRojo = workbook.CreateCellStyle();
                    _stringCellStyleRojo.BorderBottom = BorderStyle.Thin;
                    _stringCellStyleRojo.BorderLeft = BorderStyle.Thin;
                    _stringCellStyleRojo.BorderTop = BorderStyle.Thin;
                    _stringCellStyleRojo.BorderRight = BorderStyle.Thin;
                    _stringCellStyleRojo.FillForegroundColor = HSSFColor.Red.Index;
                    _stringCellStyleRojo.FillPattern = FillPattern.SolidForeground;

                    ICellStyle _stringCellStyleVerde = workbook.CreateCellStyle();
                    _stringCellStyleVerde.BorderBottom = BorderStyle.Thin;
                    _stringCellStyleVerde.BorderLeft = BorderStyle.Thin;
                    _stringCellStyleVerde.BorderTop = BorderStyle.Thin;
                    _stringCellStyleVerde.BorderRight = BorderStyle.Thin;
                    _stringCellStyleVerde.FillForegroundColor = HSSFColor.Green.Index;
                    _stringCellStyleVerde.FillPattern = FillPattern.SolidForeground;

                    #endregion


                    //AHORA CREAR UNA FILA POR CADA REGISTRO DE LA TABLA
                    foreach (DataRow row in pDatos.Rows)
                    {
                        IRow fila = worksheet.CreateRow(iRow);
                        int iCol = 0;
                        foreach (DataColumn column in pDatos.Columns)
                        {
                            ICell cell = null; //<-Representa la celda actual                               
                            object cellValue = row[iCol]; //<- El valor actual de la celda

                            switch (column.DataType.Name)
                            {
                                case "Boolean":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.Boolean);

                                        if (Convert.ToBoolean(cellValue)) { cell.SetCellFormula("TRUE()"); }
                                        else { cell.SetCellFormula("FALSE()"); }
                                        //cell.CellStyle = _boolCellStyle;
                                    }
                                    break;

                                case "String":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.String);
                                        cell.SetCellValue(Convert.ToString(cellValue));
                                        cell.CellStyle = _stringCellStyle;
                                    }
                                    break;

                                case "Int32":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.Numeric);
                                        cell.SetCellValue(Convert.ToInt32(cellValue));
                                        cell.CellStyle = _intCellStyle;
                                    }
                                    break;
                                case "Int64":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.Numeric);
                                        cell.SetCellValue(Convert.ToInt64(cellValue));
                                        cell.CellStyle = _intCellStyle;
                                    }
                                    break;
                                case "Decimal":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.Numeric);
                                        cell.SetCellValue(Convert.ToDouble(cellValue));
                                        cell.CellStyle = _doubleCellStyle;
                                    }
                                    break;
                                case "Double":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.Numeric);
                                        cell.SetCellValue(Convert.ToDouble(cellValue));
                                        cell.CellStyle = _doubleCellStyle;
                                    }
                                    break;
                                case "DateTime":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.Numeric);
                                        cell.SetCellValue(Convert.ToDateTime(cellValue));

                                        //Si No tiene valor de Hora, usar formato dd-MM-yyyy
                                        DateTime cDate = Convert.ToDateTime(cellValue);
                                        if (cDate.Hour > 0) { cell.CellStyle = _dateTimeCellStyle; }
                                        else { cell.CellStyle = _dateCellStyle; }
                                    }
                                    break;
                                case "CeldaColor":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.String);
                                        cell.SetCellValue("");

                                        var valor = cellValue as CeldaColor;
                                        switch (valor?.Color)
                                        {
                                            case "Rojo":
                                                cell.CellStyle = _stringCellStyleRojo;
                                                break;
                                            case "Amarillo":
                                                cell.CellStyle = _stringCellStyleAmarillo;
                                                break;
                                            case "Verde":
                                                cell.CellStyle = _stringCellStyleVerde;
                                                break;
                                            default:
                                                cell.CellStyle = _stringCellStyle;
                                                break;
                                        }
                                    }
                                    break;
                                default:
                                    break;
                            }
                            iCol++;
                        }
                        iRow++;
                    }

                    workbook.Write(output);
                    output.Close();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }

        public MemoryStream UpdateDataExcel(DataTable pDatos, string pFilePath)
        {

            MemoryStream output = new MemoryStream();
            IWorkbook workbook;
            using (FileStream fs = new FileStream(HttpContext.Current.Server.MapPath(pFilePath), FileMode.Open, FileAccess.Read))
            {
                workbook = new XSSFWorkbook(fs);
            }
            int index = workbook.GetSheetIndex(pDatos.TableName.ToString());
            workbook.RemoveSheetAt(index);

            ISheet sheet = workbook.CreateSheet(pDatos.TableName.ToString());
            int iRow = 0;

            if (pDatos.Columns.Count > 0)
            {
                int iCol = 0;

                var styleHeader = workbook.CreateCellStyle();
                styleHeader.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
                styleHeader.FillPattern = FillPattern.SolidForeground;
                styleHeader.BorderBottom = BorderStyle.Thin;
                styleHeader.BorderLeft = BorderStyle.Thin;
                styleHeader.BorderTop = BorderStyle.Thin;
                styleHeader.BorderRight = BorderStyle.Thin;
                styleHeader.WrapText = true;
                styleHeader.VerticalAlignment = VerticalAlignment.Top;

                IRow fila = sheet.CreateRow(iRow);
                foreach (DataColumn columna in pDatos.Columns)
                {

                    int largoCol = columna.ColumnName.Length;
                    if (largoCol > 15)
                    { largoCol = (largoCol / 2); }
                    else { largoCol = 15; }

                    ICell cell = fila.CreateCell(iCol, CellType.String);
                    cell.SetCellValue(columna.ColumnName);

                    sheet.SetColumnWidth(iCol, ((largoCol * 256) + 10));
                    cell.CellStyle = styleHeader;
                    iCol++;
                }
                iRow++;
            }

            //FORMATOS PARA CIERTOS TIPOS DE DATOS
            #region estilos
            ICellStyle _doubleCellStyle = workbook.CreateCellStyle();
            _doubleCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("#,##0.00");
            _doubleCellStyle.BorderBottom = BorderStyle.Thin;
            _doubleCellStyle.BorderLeft = BorderStyle.Thin;
            _doubleCellStyle.BorderTop = BorderStyle.Thin;
            _doubleCellStyle.BorderRight = BorderStyle.Thin;


            ICellStyle _intCellStyle = workbook.CreateCellStyle();
            _intCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("#,##0");
            _intCellStyle.BorderBottom = BorderStyle.Thin;
            _intCellStyle.BorderLeft = BorderStyle.Thin;
            _intCellStyle.BorderTop = BorderStyle.Thin;
            _intCellStyle.BorderRight = BorderStyle.Thin;

            //ICellStyle _boolCellStyle = workbook.CreateCellStyle();
            //_boolCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("BOOLEAN");
            //_boolCellStyle.BorderBottom = BorderStyle.Thin;
            //_boolCellStyle.BorderLeft = BorderStyle.Thin;
            //_boolCellStyle.BorderTop = BorderStyle.Thin;
            //_boolCellStyle.BorderRight = BorderStyle.Thin;


            ICellStyle _dateCellStyle = workbook.CreateCellStyle();
            _dateCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("dd-MM-yyyy");
            _dateCellStyle.BorderBottom = BorderStyle.Thin;
            _dateCellStyle.BorderLeft = BorderStyle.Thin;
            _dateCellStyle.BorderTop = BorderStyle.Thin;
            _dateCellStyle.BorderRight = BorderStyle.Thin;


            ICellStyle _dateTimeCellStyle = workbook.CreateCellStyle();
            _dateTimeCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("dd-MM-yyyy HH:mm:ss");
            _dateTimeCellStyle.BorderBottom = BorderStyle.Thin;
            _dateTimeCellStyle.BorderLeft = BorderStyle.Thin;
            _dateTimeCellStyle.BorderTop = BorderStyle.Thin;
            _dateTimeCellStyle.BorderRight = BorderStyle.Thin;

            ICellStyle _stringCellStyle = workbook.CreateCellStyle();
            _stringCellStyle.BorderBottom = BorderStyle.Thin;
            _stringCellStyle.BorderLeft = BorderStyle.Thin;
            _stringCellStyle.BorderTop = BorderStyle.Thin;
            _stringCellStyle.BorderRight = BorderStyle.Thin;

            ICellStyle _stringCellStyleAmarillo = workbook.CreateCellStyle();
            _stringCellStyleAmarillo.BorderBottom = BorderStyle.Thin;
            _stringCellStyleAmarillo.BorderLeft = BorderStyle.Thin;
            _stringCellStyleAmarillo.BorderTop = BorderStyle.Thin;
            _stringCellStyleAmarillo.BorderRight = BorderStyle.Thin;
            _stringCellStyleAmarillo.FillForegroundColor = HSSFColor.Yellow.Index;
            _stringCellStyleAmarillo.FillPattern = FillPattern.SolidForeground;

            ICellStyle _stringCellStyleRojo = workbook.CreateCellStyle();
            _stringCellStyleRojo.BorderBottom = BorderStyle.Thin;
            _stringCellStyleRojo.BorderLeft = BorderStyle.Thin;
            _stringCellStyleRojo.BorderTop = BorderStyle.Thin;
            _stringCellStyleRojo.BorderRight = BorderStyle.Thin;
            _stringCellStyleRojo.FillForegroundColor = HSSFColor.Red.Index;
            _stringCellStyleRojo.FillPattern = FillPattern.SolidForeground;

            ICellStyle _stringCellStyleVerde = workbook.CreateCellStyle();
            _stringCellStyleVerde.BorderBottom = BorderStyle.Thin;
            _stringCellStyleVerde.BorderLeft = BorderStyle.Thin;
            _stringCellStyleVerde.BorderTop = BorderStyle.Thin;
            _stringCellStyleVerde.BorderRight = BorderStyle.Thin;
            _stringCellStyleVerde.FillForegroundColor = HSSFColor.Green.Index;
            _stringCellStyleVerde.FillPattern = FillPattern.SolidForeground;

            #endregion


            //AHORA CREAR UNA FILA POR CADA REGISTRO DE LA TABLA
            foreach (DataRow row in pDatos.Rows)
            {
                IRow fila = sheet.CreateRow(iRow);
                int iCol = 0;
                foreach (DataColumn column in pDatos.Columns)
                {
                    ICell cell = null; //<-Representa la celda actual                               
                    object cellValue = row[iCol]; //<- El valor actual de la celda

                    switch (column.DataType.Name)
                    {
                        case "Boolean":
                            if (cellValue != DBNull.Value)
                            {
                                cell = fila.CreateCell(iCol, CellType.Boolean);

                                if (Convert.ToBoolean(cellValue)) { cell.SetCellFormula("TRUE()"); }
                                else { cell.SetCellFormula("FALSE()"); }
                                //cell.CellStyle = _boolCellStyle;
                            }
                            break;

                        case "String":
                            if (cellValue != DBNull.Value)
                            {
                                cell = fila.CreateCell(iCol, CellType.String);
                                cell.SetCellValue(Convert.ToString(cellValue));
                                cell.CellStyle = _stringCellStyle;
                            }
                            break;

                        case "Int32":
                            if (cellValue != DBNull.Value)
                            {
                                cell = fila.CreateCell(iCol, CellType.Numeric);
                                cell.SetCellValue(Convert.ToInt32(cellValue));
                                cell.CellStyle = _intCellStyle;
                            }
                            break;
                        case "Int64":
                            if (cellValue != DBNull.Value)
                            {
                                cell = fila.CreateCell(iCol, CellType.Numeric);
                                cell.SetCellValue(Convert.ToInt64(cellValue));
                                cell.CellStyle = _intCellStyle;
                            }
                            break;
                        case "Decimal":
                            if (cellValue != DBNull.Value)
                            {
                                cell = fila.CreateCell(iCol, CellType.Numeric);
                                cell.SetCellValue(Convert.ToDouble(cellValue));
                                cell.CellStyle = _doubleCellStyle;
                            }
                            break;
                        case "Double":
                            if (cellValue != DBNull.Value)
                            {
                                cell = fila.CreateCell(iCol, CellType.Numeric);
                                cell.SetCellValue(Convert.ToDouble(cellValue));
                                cell.CellStyle = _doubleCellStyle;
                            }
                            break;
                        case "DateTime":
                            if (cellValue != DBNull.Value)
                            {
                                cell = fila.CreateCell(iCol, CellType.Numeric);
                                cell.SetCellValue(Convert.ToDateTime(cellValue));

                                //Si No tiene valor de Hora, usar formato dd-MM-yyyy
                                DateTime cDate = Convert.ToDateTime(cellValue);
                                if (cDate.Hour > 0) { cell.CellStyle = _dateTimeCellStyle; }
                                else { cell.CellStyle = _dateCellStyle; }
                            }
                            break;
                        case "CeldaColor":
                            if (cellValue != DBNull.Value)
                            {
                                cell = fila.CreateCell(iCol, CellType.String);
                                cell.SetCellValue("");

                                var valor = cellValue as CeldaColor;
                                switch (valor?.Color)
                                {
                                    case "Rojo":
                                        cell.CellStyle = _stringCellStyleRojo;
                                        break;
                                    case "Amarillo":
                                        cell.CellStyle = _stringCellStyleAmarillo;
                                        break;
                                    case "Verde":
                                        cell.CellStyle = _stringCellStyleVerde;
                                        break;
                                    default:
                                        cell.CellStyle = _stringCellStyle;
                                        break;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                    iCol++;
                }
                iRow++;
            }



            /****************************************************/

            workbook.Write(output);
            return output;
        }

        public MemoryStream DataTable_To_ExcelCol(DataTable pDatos,
                                //string pFilePath, 
                                string rutaImagenHeader, string tituloHeader,
                                int largoImagem, int anchoImagen,
                                string rutaImagenBuho, int largoBoho)
        {

            MemoryStream output = new MemoryStream();
            try
            {
                if (pDatos != null && pDatos.Rows.Count > 0)
                {
                    IWorkbook workbook = null;
                    ISheet worksheet = null;

                    workbook = new XSSFWorkbook();
                    worksheet = workbook.CreateSheet(pDatos.TableName);

                    //CREAR EN LA PRIMERA FILA LOS TITULOS DE LAS COLUMNAS
                    worksheet.CreateFreezePane(0, anchoImagen + 1, 0, anchoImagen + 1);

                    //byte[] data = File.ReadAllBytes(HttpContext.Current.Server.MapPath(rutaImagenHeader));

                    //int pictureIndex = workbook.AddPicture(data, PictureType.PNG);
                    ICreationHelper helper = workbook.GetCreationHelper();
                    IDrawing drawing = worksheet.CreateDrawingPatriarch();
                    IClientAnchor anchor = helper.CreateClientAnchor();
                    anchor.Col1 = 0;//0 index based column
                    anchor.Row1 = 0;//0 index based row
                    //IPicture picture = drawing.CreatePicture(anchor, pictureIndex);
                    //picture.Resize(largoImagem, anchoImagen);

                    //Agrega el Buho al final del encabezado
                    //data = File.ReadAllBytes(HttpContext.Current.Server.MapPath(rutaImagenBuho));

                    //pictureIndex = workbook.AddPicture(data, PictureType.PNG);
                    helper = workbook.GetCreationHelper();
                    drawing = worksheet.CreateDrawingPatriarch();
                    anchor = helper.CreateClientAnchor();
                    anchor.Col1 = pDatos.Columns.Count - 2;//0 index based column
                    anchor.Row1 = 0;//0 index based row
                    //picture = drawing.CreatePicture(anchor, pictureIndex);
                    //picture.Resize(largoBoho, 2.5);



                    var headerRow = worksheet.CreateRow(0);

                    var RowStyle = workbook.CreateCellStyle();
                    RowStyle.Alignment = HorizontalAlignment.Left;
                    RowStyle.VerticalAlignment = VerticalAlignment.Center;
                    RowStyle.WrapText = true;
                    IFont fontR = workbook.CreateFont();
                    fontR.Boldweight = 3;
                    fontR.FontHeight = 20;
                    RowStyle.SetFont(fontR);

                    var Celda = headerRow.CreateCell(largoImagem);
                    Celda.SetCellValue(tituloHeader);
                    Celda.CellStyle = RowStyle;


                    //NPOI.SS.Util.CellRangeAddress merge1 = new NPOI.SS.Util.CellRangeAddress(0, 2, largoImagem, pDatos.Columns.Count - 1);
                    //worksheet.AddMergedRegion(merge1);

                    //NPOI.SS.Util.CellRangeAddress merge2 = new NPOI.SS.Util.CellRangeAddress(0, 2, 0, largoImagem - 1);
                    //worksheet.AddMergedRegion(merge2);


                    int iRow = anchoImagen;

                    if (pDatos.Columns.Count > 0)
                    {
                        int iCol = 0;

                        var styleHeader = workbook.CreateCellStyle();
                        styleHeader.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.Grey25Percent.Index;
                        styleHeader.FillPattern = FillPattern.SolidForeground;
                        styleHeader.BorderBottom = BorderStyle.Thin;
                        styleHeader.BorderLeft = BorderStyle.Thin;
                        styleHeader.BorderTop = BorderStyle.Thin;
                        styleHeader.BorderRight = BorderStyle.Thin;
                        styleHeader.WrapText = true;
                        styleHeader.VerticalAlignment = VerticalAlignment.Top;

                        IRow fila = worksheet.CreateRow(iRow);
                        foreach (DataColumn columna in pDatos.Columns)
                        {

                            int largoCol = columna.ColumnName.Length;
                            if (largoCol > 15)
                            { largoCol = (largoCol / 2); }
                            else { largoCol = 15; }

                            ICell cell = fila.CreateCell(iCol, CellType.String);
                            cell.SetCellValue(columna.ColumnName);

                            worksheet.SetColumnWidth(iCol, ((largoCol * 256) + 10));
                            cell.CellStyle = styleHeader;
                            iCol++;
                        }
                        iRow++;
                    }

                    //FORMATOS PARA CIERTOS TIPOS DE DATOS
                    #region estilos
                    ICellStyle _doubleCellStyle = workbook.CreateCellStyle();
                    _doubleCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("#,##0.00");
                    _doubleCellStyle.BorderBottom = BorderStyle.Thin;
                    _doubleCellStyle.BorderLeft = BorderStyle.Thin;
                    _doubleCellStyle.BorderTop = BorderStyle.Thin;
                    _doubleCellStyle.BorderRight = BorderStyle.Thin;


                    ICellStyle _intCellStyle = workbook.CreateCellStyle();
                    _intCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("#,##0");
                    _intCellStyle.BorderBottom = BorderStyle.Thin;
                    _intCellStyle.BorderLeft = BorderStyle.Thin;
                    _intCellStyle.BorderTop = BorderStyle.Thin;
                    _intCellStyle.BorderRight = BorderStyle.Thin;

                    //ICellStyle _boolCellStyle = workbook.CreateCellStyle();
                    //_boolCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("BOOLEAN");
                    //_boolCellStyle.BorderBottom = BorderStyle.Thin;
                    //_boolCellStyle.BorderLeft = BorderStyle.Thin;
                    //_boolCellStyle.BorderTop = BorderStyle.Thin;
                    //_boolCellStyle.BorderRight = BorderStyle.Thin;


                    ICellStyle _dateCellStyle = workbook.CreateCellStyle();
                    _dateCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("dd-MM-yyyy");
                    _dateCellStyle.BorderBottom = BorderStyle.Thin;
                    _dateCellStyle.BorderLeft = BorderStyle.Thin;
                    _dateCellStyle.BorderTop = BorderStyle.Thin;
                    _dateCellStyle.BorderRight = BorderStyle.Thin;


                    ICellStyle _dateTimeCellStyle = workbook.CreateCellStyle();
                    _dateTimeCellStyle.DataFormat = workbook.CreateDataFormat().GetFormat("dd-MM-yyyy HH:mm:ss");
                    _dateTimeCellStyle.BorderBottom = BorderStyle.Thin;
                    _dateTimeCellStyle.BorderLeft = BorderStyle.Thin;
                    _dateTimeCellStyle.BorderTop = BorderStyle.Thin;
                    _dateTimeCellStyle.BorderRight = BorderStyle.Thin;

                    ICellStyle _stringCellStyle = workbook.CreateCellStyle();
                    _stringCellStyle.BorderBottom = BorderStyle.Thin;
                    _stringCellStyle.BorderLeft = BorderStyle.Thin;
                    _stringCellStyle.BorderTop = BorderStyle.Thin;
                    _stringCellStyle.BorderRight = BorderStyle.Thin;

                    ICellStyle _stringCellStyleAmarillo = workbook.CreateCellStyle();
                    _stringCellStyleAmarillo.BorderBottom = BorderStyle.Thin;
                    _stringCellStyleAmarillo.BorderLeft = BorderStyle.Thin;
                    _stringCellStyleAmarillo.BorderTop = BorderStyle.Thin;
                    _stringCellStyleAmarillo.BorderRight = BorderStyle.Thin;
                    _stringCellStyleAmarillo.FillForegroundColor = HSSFColor.Yellow.Index;
                    _stringCellStyleAmarillo.FillPattern = FillPattern.SolidForeground;

                    ICellStyle _stringCellStyleRojo = workbook.CreateCellStyle();
                    _stringCellStyleRojo.BorderBottom = BorderStyle.Thin;
                    _stringCellStyleRojo.BorderLeft = BorderStyle.Thin;
                    _stringCellStyleRojo.BorderTop = BorderStyle.Thin;
                    _stringCellStyleRojo.BorderRight = BorderStyle.Thin;
                    _stringCellStyleRojo.FillForegroundColor = HSSFColor.Red.Index;
                    _stringCellStyleRojo.FillPattern = FillPattern.SolidForeground;

                    ICellStyle _stringCellStyleVerde = workbook.CreateCellStyle();
                    _stringCellStyleVerde.BorderBottom = BorderStyle.Thin;
                    _stringCellStyleVerde.BorderLeft = BorderStyle.Thin;
                    _stringCellStyleVerde.BorderTop = BorderStyle.Thin;
                    _stringCellStyleVerde.BorderRight = BorderStyle.Thin;
                    _stringCellStyleVerde.FillForegroundColor = HSSFColor.Green.Index;
                    _stringCellStyleVerde.FillPattern = FillPattern.SolidForeground;

                    #endregion


                    //AHORA CREAR UNA FILA POR CADA REGISTRO DE LA TABLA
                    foreach (DataRow row in pDatos.Rows)
                    {
                        IRow fila = worksheet.CreateRow(iRow);
                        int iCol = 0;
                        foreach (DataColumn column in pDatos.Columns)
                        {
                            ICell cell = null; //<-Representa la celda actual                               
                            object cellValue = row[iCol]; //<- El valor actual de la celda

                            switch (column.DataType.Name)
                            {
                                case "Boolean":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.Boolean);

                                        if (Convert.ToBoolean(cellValue)) { cell.SetCellFormula("TRUE()"); }
                                        else { cell.SetCellFormula("FALSE()"); }
                                        //cell.CellStyle = _boolCellStyle;
                                    }
                                    break;

                                case "String":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.String);
                                        cell.SetCellValue(Convert.ToString(cellValue));
                                        cell.CellStyle = _stringCellStyle;
                                    }
                                    break;

                                case "Int32":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.Numeric);
                                        cell.SetCellValue(Convert.ToInt32(cellValue));
                                        cell.CellStyle = _intCellStyle;
                                    }
                                    break;
                                case "Int64":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.Numeric);
                                        cell.SetCellValue(Convert.ToInt64(cellValue));
                                        cell.CellStyle = _intCellStyle;
                                    }
                                    break;
                                case "Decimal":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.Numeric);
                                        cell.SetCellValue(Convert.ToDouble(cellValue));
                                        cell.CellStyle = _doubleCellStyle;
                                    }
                                    break;
                                case "Double":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.Numeric);
                                        cell.SetCellValue(Convert.ToDouble(cellValue));
                                        cell.CellStyle = _doubleCellStyle;
                                    }
                                    break;
                                case "DateTime":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.Numeric);
                                        cell.SetCellValue(Convert.ToDateTime(cellValue));

                                        //Si No tiene valor de Hora, usar formato dd-MM-yyyy
                                        DateTime cDate = Convert.ToDateTime(cellValue);
                                        if (cDate.Hour > 0) { cell.CellStyle = _dateTimeCellStyle; }
                                        else { cell.CellStyle = _dateCellStyle; }
                                    }
                                    break;
                                case "CeldaColor":
                                    if (cellValue != DBNull.Value)
                                    {
                                        cell = fila.CreateCell(iCol, CellType.String);
                                        cell.SetCellValue("");

                                        var valor = cellValue as CeldaColor;
                                        switch (valor?.Color)
                                        {
                                            case "Rojo":
                                                cell.CellStyle = _stringCellStyleRojo;
                                                break;
                                            case "Amarillo":
                                                cell.CellStyle = _stringCellStyleAmarillo;
                                                break;
                                            case "Verde":
                                                cell.CellStyle = _stringCellStyleVerde;
                                                break;
                                            default:
                                                cell.CellStyle = _stringCellStyle;
                                                break;
                                        }
                                    }
                                    break;
                                default:
                                    break;
                            }
                            iCol++;
                        }
                        iRow++;
                    }
                    ////// Agregar Totales
                    ICell cell2 = null; //<-Representa la celda actual                               
                                        //<- El valor actual de la celda


                    workbook.Write(output);
                    output.Close();

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return output;
        }
       
       
        private void CreateCell(IRow CurrentRow, int CellIndex, string Value, XSSFCellStyle Style)
        {
            ICell Cell = CurrentRow.CreateCell(CellIndex);
            Cell.SetCellValue(Value);
            Cell.CellStyle = Style;
        }
    }
    public class CeldaColor
    {
        public string Color { get; set; }
    }
}
