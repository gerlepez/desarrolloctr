﻿using DesarrolloCTRServicios.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers.Extra
{
    public class SessionUtilityAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var ctx = HttpContext.Current;
            var user = HttpContext.Current.Session["usuarioLogeado"];
            if (user != null)
            {
                try
                {
                    var userName = user.ToString();
                    ServicioUsuario usuario = new ServicioUsuario();
                    var estadoLogin = usuario.ObtenerUsuarioLogueado(userName);

                    if (estadoLogin == 0 || estadoLogin == -1)
                    {
                        filterContext.Result = new RedirectResult("~/Login/SesionIncorrecta");
                    }
                }
                catch
                {

                    filterContext.Result = new RedirectResult("~/Login/UsuarioLogin");
                }
            }
            // check if session is supported
            //CurrentCustomer objCurrentCustomer = new CurrentCustomer();
            //objCurrentCustomer = ((CurrentCustomer)SessionStore.GetSessionValue(SessionStore.Customer));

            if (user == null)
            {
                // check if a new session id was generated
                filterContext.Result = new RedirectResult("~/Login/UsuarioLogin");

            }
            base.OnActionExecuting(filterContext);
        }
    }
}