﻿using System.Collections.Generic;

namespace DesarrolloCTRServicios.ViewModels
{
    public class AutomatismoVM
    {
        public int? idAutomatismo { get; set; }
        public int? idEquipo { get; set; }
        public int? idLugar { get; set; }
        public int? idAutAutomatismo { get; set; }
        public int? idTipoEquipo { get; set; }
        public int? idAutoAutomRelacion { get; set; }


        public string nombreAutomatismo { get; set; }
        public string nombreAutomatismoSiglas { get; set; }
        public string nombreDetalleAutomatismoPartido { get; set; }
        public int? idDetalleAutomatismo { get; set; }
        public bool? activo { get; set; }
        public bool? TieneEquipo { get; set; }
        public string tieneE { get; set; }
        public string nombreEquipo { get; set; }
        public string nombreTipoEquipo { get; set; }
        public string nombreLugar { get; set; }
        public string nombrePtoConexion { get; set; }
        public string codigoSap { get; set; }
        public EstacionVM estacion { get; set; }
        public EquipoVM equipo { get; set; }

        public List<string> listaEquipos { get; set; }


    }
}
