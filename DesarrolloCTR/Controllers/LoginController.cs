﻿using DesarrolloCTRServicios.Servicios;
using Microsoft.Ajax.Utilities;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OpenIdConnect;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Mvc.Routing;
using System.Web.Security;

namespace DesarrolloCTR.Controllers
{
    public class LoginController : Controller
    {
        [System.Web.Http.Authorize]
        public ActionResult UsuarioLogin()
        {

            ////verifico el tipo de configuracion desde webconfig, es por AD ?
            //var config = System.Configuration.ConfigurationManager.AppSettings["ActiveDirectoryAuth"];
            //if (config == "true")
            //{
            //    SetSessionAD();
            //    return View();
            //}            
            return View("Login");
        }
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            System.Web.HttpContext.Current.Session.Clear();
            if (Request.Cookies["jwtToken"] != null)
            {
                var cookie = new HttpCookie("jwtToken")
                {
                    Expires = DateTime.Now.AddDays(-1)
                };

                Response.Cookies.Add(cookie);
            }
            Session.Abandon();
            return RedirectToAction("LoginManual", "Login");
        }
        public ActionResult Validar(string nombre, int idUsuario, string password)
        {
            try
            {
                ServicioUsuario servicio = new ServicioUsuario();
                if (nombre == null)
                {
                    return RedirectToAction("LoginManual", "Login");
                }
                if (string.IsNullOrEmpty(password))
                {
                    return RedirectToAction("LoginManual", "Login");
                }
                var resp = servicio.ObtenerLoginUsuario(idUsuario, password);
                if (resp != "DatosIncorrectos#Login" && resp != "PermisoDenegado#Login")
                {
                    System.Web.HttpContext.Current.Session["usuarioLogeado"] = nombre;
                }
                return RedirectToAction(resp.Split('#')[0], resp.Split('#')[1]);

            }
            catch (UnauthorizedAccessException e)
            {
                ViewBag.User = e.Message;
            }
            return RedirectToAction("LoginManual", "Login");
        }
        public ActionResult LoginManual()
        {
            return View("Login");
        }
        public ActionResult SesionIncorrecta()
        {
            return View();

        }
        public ActionResult SesionIncorrecta2()
        {
            return View();

        }

        public ActionResult PermisoDenegado()
        {
            return View();

        }
        public ActionResult DatosIncorrectos()
        {
            return View();
        }
        public ActionResult ModuloDenegado()
        {
            return View();
        }
        public ActionResult DenegacionPorTurno()
        {
            return View();
        }

        public void SetSessionAD()
        {
            if (Request != null)
            {
                var userName = Request.LogonUserIdentity.Name;
                Session["usuarioLogeado"] = userName.Substring(userName.IndexOf('\\') + 1);
            }


        }
    }
}
