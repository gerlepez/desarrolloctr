﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;

namespace DesarrolloCTR.Controllers.Api
{
    public class AutomatismoApiController : ApiController
    {
        // GET: AutomatismoApi
        [System.Web.Http.Route("Api/Automatismo/ObtenerAutomatismo")]
        [System.Web.Http.ActionName("ObtenerAutomatismo")]
        [System.Web.Http.HttpGet]
        public object ObtenerAutomatismo(bool activo)
        {
            ServicioAutomatismo servicioAutomatismo = new ServicioAutomatismo();
            object lista = servicioAutomatismo.ObtenerAutomatismo(activo);
            return lista;
        }


        [System.Web.Http.Route("Api/Automatismo/ConsultarUnAutomatismo")]
        [System.Web.Http.ActionName("ConsultarUnAutomatismo")]
        [System.Web.Http.HttpGet]
        public AutomatismoVM ConsultarUnAutomatismo(int idAutomatismo)
        {
            ServicioAutomatismo servicioAutomatismo = new ServicioAutomatismo();
            AutomatismoVM automatismo = servicioAutomatismo.ConsultarUnAutomatismo(idAutomatismo);
            return automatismo;
        }

        [System.Web.Http.Route("Api/Automatismo/ObtenerAutomatismosDetalleLugar")]
        [System.Web.Http.ActionName("ObtenerAutomatismosDetalleLugar")]
        [System.Web.Http.HttpGet]
        public object ObtenerAutomatismosDetalleLugar(int idLugar, int automatismo , int idNovedad = 0)
        {
            ServicioAutomatismo servicio = new ServicioAutomatismo();

            var lista = servicio.ObtenerAutomatismosDetalleLugar(idLugar, automatismo , idNovedad);

            return lista;
        }
        [System.Web.Http.Route("Api/Automatismo/ObtenerEquiposPorTipoYLugar")]
        [System.Web.Http.ActionName("ObtenerEquiposPorTipoYLugar")]
        [System.Web.Http.HttpGet]
        public object ObtenerEquiposPorTipoYLugar(int idLugar, int automatismo)
        {
            ServicioAutomatismo servicio = new ServicioAutomatismo();

            var lista = servicio.ObtenerEquiposPorTipoYLugar(idLugar, automatismo);

            return lista;
        }
        

        [System.Web.Http.Route("Api/Automatismo/AltaAutomatismo")]
        [System.Web.Http.ActionName("AltaAutomatismo")]
        [System.Web.Http.HttpPost]
        public int AltaAutomatismo(AutomatismoVM automatismo)
        {   
            ServicioAutomatismo servicioAutomatismo = new ServicioAutomatismo();
            int codigo;
            if (servicioAutomatismo.ValidarAutomatismo(automatismo))
            {
                codigo = servicioAutomatismo.AltaAutomatismo(automatismo);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }


        [System.Web.Http.Route("Api/Automatismo/ModificarAutomatismo")]
        [System.Web.Http.ActionName("ModificarAutomatismo")]
        [System.Web.Http.HttpPost]
        public int ModificarAutomatismo(AutomatismoVM automatismo)
        {
            ServicioAutomatismo servicioAutomatismo = new ServicioAutomatismo();
            int codigo;
            if (servicioAutomatismo.ValidarAutomatismo(automatismo))
            {
                codigo = servicioAutomatismo.ModificaraAutomatismo(automatismo);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }


        [System.Web.Http.Route("Api/Automatismo/EliminarAutomatismo")]
        [System.Web.Http.ActionName("EliminarAutomatismo")]
        [System.Web.Http.HttpPost]
        public int EliminarAutomatismo(AutomatismoVM automatismo)
        {
            ServicioAutomatismo servicioAutomatismo = new ServicioAutomatismo();
            int codigo = servicioAutomatismo.EliminarAutomatismo(automatismo);
            return codigo;
        }

        [System.Web.Http.Route("Api/Automatismo/RecuperarAutomatismo")]
        [System.Web.Http.ActionName("RecuperarAutomatismo")]
        [System.Web.Http.HttpPost]
        public int RecuperarAutomatismo(AutomatismoVM automatismo)
        {
            ServicioAutomatismo servicioAutomatismo = new ServicioAutomatismo();
            int codigo = servicioAutomatismo.RecuperarAutomatismo(automatismo);
            return codigo;
        }


        //Relaciones

        [System.Web.Http.Route("Api/Automatismo/ObtenerAutomatismoRelacion")]
        [System.Web.Http.ActionName("ObtenerAutomatismoRelacion")]
        [System.Web.Http.HttpGet]
        public object ObtenerAutomatismoRelacion(bool activo)
        {
            ServicioAutomatismo servicioAutomatismo = new ServicioAutomatismo();
            object lista = servicioAutomatismo.ObtenerRelaciones(activo);
            return lista;
        }


        [System.Web.Http.Route("Api/Automatismo/ConsultarUnAutomatismoRelacion")]
        [System.Web.Http.ActionName("ConsultarUnAutomatismoRelacion")]
        [System.Web.Http.HttpGet]
        public AutomatismoVM ConsultarUnAutomatismoRelacion(int idAutAutoRelacion)
        {
            ServicioAutomatismo servicioAutomatismo = new ServicioAutomatismo();
            AutomatismoVM automatismo = servicioAutomatismo.ConsultarUnaRelacion(idAutAutoRelacion);
            return automatismo;
        }


        [System.Web.Http.Route("Api/Automatismo/AltaAutomatismoRelacion")]
        [System.Web.Http.ActionName("AltaAutomatismoRelacion")]
        [System.Web.Http.HttpPost]
        public int AltaAutomatismoRelacion(AutomatismoVM relacion)
        {
            ServicioAutomatismo servicioAutomatismo = new ServicioAutomatismo();
            int codigo;

            codigo = servicioAutomatismo.AltaAutomatismoRelacion(relacion);

            return codigo;
        }


        [System.Web.Http.Route("Api/Automatismo/ModificarAutomatismoRelacion")]
        [System.Web.Http.ActionName("ModificarAutomatismoRelacion")]
        [System.Web.Http.HttpPost]
        public int ModificarAutomatismoRelacion(AutomatismoVM relacion)
        {
            ServicioAutomatismo servicioAutomatismo = new ServicioAutomatismo();
            int codigo;
            codigo = servicioAutomatismo.ModificaraAutomatismoRelacion(relacion);

            return codigo;
        }


        [System.Web.Http.Route("Api/Automatismo/EliminarAutomatismoRelacion")]
        [System.Web.Http.ActionName("EliminarAutomatismoRelacion")]
        [System.Web.Http.HttpPost]
        public int EliminarAutomatismoRelacion(AutomatismoVM relacion)
        {
            ServicioAutomatismo servicioAutomatismo = new ServicioAutomatismo();
            int codigo = servicioAutomatismo.EliminarAutomatismoRelacion(relacion);
            return codigo;
        }

        [System.Web.Http.Route("Api/Automatismo/RecuperarAutomatismoRelacion")]
        [System.Web.Http.ActionName("RecuperarAutomatismoRelacion")]
        [System.Web.Http.HttpPost]
        public int RecuperarAutomatismoRelacion(AutomatismoVM relacion)
        {
            ServicioAutomatismo servicioAutomatismo = new ServicioAutomatismo();
            int codigo = servicioAutomatismo.RecuperarAutomatismoRelacion(relacion);
            return codigo;
        }


        [System.Web.Http.Route("Api/Automatismo/ObtenerListaTipoEquipo")]
        [System.Web.Http.ActionName("ObtenerListaTipoEquipo")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaEquipo(bool activo)
        {
            ServicioAutomatismo servicioAutomatismo = new ServicioAutomatismo();
            object listaEquipo = servicioAutomatismo.ObtenerListaTipoEquipo(activo);

            return listaEquipo;
        }

        [System.Web.Http.Route("Api/Automatismo/ObtenerListaDeLugar")]
        [System.Web.Http.ActionName("ObtenerListaDeLugar")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaDeLugar(int idTipoEquipo)
        {
            ServicioAutomatismo servicioAutomatismo = new ServicioAutomatismo();
            object listaEquipo = servicioAutomatismo.ObtenerListaDeLugar(idTipoEquipo);

            return listaEquipo;
        }
        

        [System.Web.Http.Route("Api/Automatismo/ObtenerListaDeEquipo")]
        [System.Web.Http.ActionName("ObtenerListaDeEquipo")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaDeEquipo(int idTipoEquipo , int idLugar)
        {
            ServicioAutomatismo servicioAutomatismo = new ServicioAutomatismo();
            object listaEquipo = servicioAutomatismo.ObtenerListaDeEquipo(idTipoEquipo, idLugar);

            return listaEquipo;
        }
    }
}