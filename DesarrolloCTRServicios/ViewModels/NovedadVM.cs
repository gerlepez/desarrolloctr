﻿using System;
using System.Collections.Generic;

namespace DesarrolloCTRServicios.ViewModels
{

    public class GrillaNovedadVM
    {
        public string numeroReclamo { get; set; }
        public string codigoTipoNovedad { get; set; }
        public int? idNovedad { get; set; }
        public string codigoNovedad { get; set; }
        public string codigoLicencia { get; set; }
        public int? idLicencia { get; set; }
        public int? idTipoNovedad { get; set; }
        public string nombreEstado { get; set; }
        public string nombreNovedad { get; set; }
        public string descripcion { get; set; }
        public bool? activo { get; set; }
        public int? idEstado { get; set; }
        public DateTime? fecha { get; set; }
        public DateTime? hora { get; set; }
        public int? idActuacion { get; set; }
        public string nombreActuacion { get; set; }
        public string decripcionFiltro { get; set; }
        public string nota { get; set; }
        public int? idturnoEjecucion { get; set; }
        public bool? tieneTurnoEjecucion { get; set; }
        public NovedadVM detalleNovedad { get; set; }
    }



    public class NovedadVM
    {
        public string nombreEquipo { get; set; }
        public string nombreActuacion { get; set; }
        public string nota { get; set; }
        public int idNovedad { get; set; }
        public int idNovedadOrigen { get; set; }
        public int? idNovedadRelacionada { get; set; }
        public string descripcionNovedad { get; set; }
        public string fechaHoraNovedad { get; set; }
        public int? idUsuarioAlta { get; set; }
        public int? idTipoNovedad { get; set; }
        public int? idEquipo { get; set; }
        public int? idTransformador { get; set; }
        public int? idLinea { get; set; }
        public int? idLicencia { get; set; }
        public int? idPuntoConexion { get; set; }
        public int? idCapacitores { get; set; }
        public int? idCentral { get; set; }
        public int? idGenerador { get; set; }
        public int? idActuacion { get; set; }
        public int? idTipoActuacion { get; set; }
        public int? idTipoLinea { get; set; }
        public int? idAlarma { get; set; }
        public int? posesionLinea { get; set; }
        public int? idTipoReclamo { get; set; }
        public int? idInfraestructura { get; set; }
        public int? idEquipoReclamo { get; set; }
        public int? idTipoEquipo { get; set; }
        public int? idEstacion { get; set; }
        public int? idCentroControl { get; set; }
        public int? idTipoinfra { get; set; }
        public int? idEquipoAdicional { get; set; }
        public String nombreEquipoAdicional { get; set; }
        public DateTime? fechaUsuarioModifica { get; set; }
        public bool? activo { get; set; }
        public string nombresuario { get; set; }
        public int? codigo { get; set; }
        public int? idEmpleado { get; set; }
        public string fechaParseada { get; set; } // no se si usarla
        public string fechaVisible { get; set; }
        public string horaParseada { get; set; }
        public int? idEstadoCierre { get; set; }
        public string alarma { get; set; }
        public bool estadoCierre { get; set; }
        public bool? energiaNoSuministrada { get; set; }
        public string CodigoNovedad { get; set; }
        public string CodigoEquipo { get; set; }
        public string nombreUsuarioAlta { get; set; }
        public DateTime? fechaNovedad { get; set; }
        public DateTime? horaNovedad { get; set; }
        public int? idServiciosAuxiliares { get; set; }
        public int? idIndisponibilidad { get; set; }
        public int? idDetalleAutomatismo { get; set; }
        public int? idAutomatismo { get; set; }
        public string numeroReclamo { get; set; }
        public string codigoTipoNovedad { get; set; }
        public bool entradaIndisponibilidad { get; set; }
        public string tipoCRUD { get; set; }
        public int NumNovedad { get; set; }
        public int anioNovedad { get; set; }
        public List<int> automatismos { get; set; }
        public IndisponibilidadVM Indisponibilidad { get; set; }
        public AutomatismoVM Automatismo { get; set; }
        public EstadoNovedadVM estadoNovedad { get; set; }
        public LicenciaVM licencia { get; set; }
        public TipoNovedadVM tipoNovedad { get; set; }
        public EquipoVM equipo { get; set; }
        public ActuacionVM actuacion { get; set; }
        public TipoActuacionVM tipo { get; set; }
        public TransformadorVM transformador { get; set; }
        public EstacionVM estacion { get; set; }
        public PropiedadEstacionVM propiedadEstacion { get; set; }
        public LineaVM linea { get; set; }
        public CapacitorVM capacitor { get; set; }
        public PuntoConexionVM puntoConexion { get; set; }
        public CentroControlVM centroControl { get; set; }
        public List<NovedadEquipoVM> novedadEquipo { get; set; }
        //public NovedadRelacionadaVM novedadRelacionada { get; set; }
        public CentralVM central { get; set; }
        public GeneradorVM generador { get; set; }
        public InfraestructuraVM infraestructura { get; set; }
        public ReclamoVM reclamo { get; set; }
        public TipoLineaVM tipoLinea { get; set; }
        public PosesionLineaVM lineaPosesion { get; set; }
        public PropiedadLineaVM propiedadLinea { get; set; }
        public EquipoReclamoVM equipoReclamo { get; set; }
        public List<ArchivoVM> listArchivos { get; set; }
        public EquipoDetalleEstacionVM equipoDetalleEstacion { get; set; }
        public List<NovedadVM> NovedadesNodos { get; set; }
        public NovedadVM novedadRelacionada { get; set; }
        public ServicioAuxiliarVM servicioAuxiliar { get; set; }

    }
}
