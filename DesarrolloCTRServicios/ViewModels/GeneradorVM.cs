﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class GeneradorVM
    {
        public int? idGenerador { get; set; }
        public int? idEquipo { get; set; }
        public int? idtipoEquipo { get; set; }
        public int? idGeneral { get; set; }
        public int? idCentroControl { get; set; }
        public int? idCentral { get; set; }
        public int idUsuarioAlta { get; set; }
        public int? idPropiedad { get; set; }

        public string descripcion { get; set; }
        public string codigoEquipo { get; set; }
        public string nombresuario { get; set; }
        public string nombreGenerador { get; set; }
        public string nombreEquipo { get; set; }
        public string nombreDetalleCentral { get; set; }

        public bool? activo { get; set; }
        public bool? esAutomatismo { get; set; }

        public CentralVM central { get; set; }
        public UsuarioVM usuario { get; set; }
        public CentroControlVM centrocontrol { get; set; }
        public PropiedadVM propiedad { get; set; }
    }
}
