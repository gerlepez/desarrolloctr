﻿using DesarrolloCTR.Controllers.Extra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers
{
    [SessionUtility]
    [Permiso("Recl")]
    public class ReclamoController : Controller
    {
        // GET: Reclamo
        public ActionResult Reclamo()
        {
            return View();
        }
        public ActionResult AdministrarReclamo()
        {
            return View();
        }
    }
}
