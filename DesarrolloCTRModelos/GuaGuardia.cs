//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DesarrolloCTRModelos
{
    using System;
    using System.Collections.Generic;
    
    public partial class GuaGuardia
    {
        public int idGuardia { get; set; }
        public Nullable<int> idPersona { get; set; }
        public Nullable<System.DateTime> FechaHoraDesde { get; set; }
        public Nullable<System.DateTime> FechaHoraHasta { get; set; }
        public Nullable<int> idMantenimiento { get; set; }
        public Nullable<int> idTipoGuardia { get; set; }
        public string Telefono { get; set; }
        public Nullable<bool> activo { get; set; }
        public Nullable<int> idPersona2 { get; set; }
        public string Telefono2 { get; set; }
    }
}
