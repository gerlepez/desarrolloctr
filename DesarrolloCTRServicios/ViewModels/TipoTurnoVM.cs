﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class TipoTurnoVM
    {
        public int? idTurnoCtr { get; set; }
        public string nombreTurno { get; set; }
    }
}
