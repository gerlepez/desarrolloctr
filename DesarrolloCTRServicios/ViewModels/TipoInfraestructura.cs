﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesarrolloCTRServicios.ViewModels
{
    public class TipoInfraestructura
    {
        public int? idTipoInfra { get; set; }
        public int? idUsuarioAlta { get; set; }

        public bool? activo { get; set; }

        public string codigoSap { get; set; }
        public string nombreInfra { get; set; }
    }
}
