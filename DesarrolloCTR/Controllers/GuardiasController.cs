﻿using DesarrolloCTR.Controllers.Extra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers
{
    [SessionUtilityAttribute]
    [Permiso("GUA")]
    public class GuardiasController : Controller
    {
        // GET: Guardias

        public ActionResult Guardias()
        {
            return View();
        }


        public ActionResult AdministrarPersonal(int? id)
        {
            if (Convert.ToBoolean(Session["tienePermisoEdicion"]))
            {
                return View();
            }
            else
            {
                return View("PersonalGuardias");
            }
        }

        public ActionResult PersonalGuardias()
        {
            return View();
        }
    }
}
