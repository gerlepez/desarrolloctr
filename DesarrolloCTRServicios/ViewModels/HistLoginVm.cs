﻿using System;

namespace DesarrolloCTRServicios.ViewModels
{
    public class HistLoginVM
    {
        public int idHistorialLogin { get; set; }
        public DateTime fechaHoraInicioSesion { get; set; }
        public DateTime fechaHoraFinSesion { get; set; }
        public int idUsuario { get; set; }
    }
}
