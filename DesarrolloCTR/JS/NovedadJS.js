﻿

// == trae datos del url ==
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
} //Trae valor del id del URL
//=========================

// === Botones Modal Para Licencia ===
    function ClickSi() {
    //let idNovedad=localStorage.getItem("idNovedadOrigen");
    window.location.href = '/Novedad/AdministrarLicencia?idNovedad=' + idNovedad;

}

    function ClickNo() {
    window.location.href = '/Novedad/Novedad';

}
// ===================================

// ======== Cargar Archivos===========

    function AgregarMultipleArchivos() {
    $('#InputArchivosNovedad').trigger('click');


}

    function mostrarNombreMultipleArchivos() {
    $("#nombreMultipleArchivos").empty();
    var filesCuerpo = $('#InputArchivosNovedad').get(0).files;
    for (f = 0; f < filesCuerpo.length; f++) {
        $("#nombreMultipleArchivos").append(filesCuerpo[f].name + "<br/>");
    }
}

    function GuardarArchivo(idNovedad) {

    var data = new FormData();

    data.append("Ruta", "Novedad");
    data.append("Id" + "," + idNovedad, idNovedad);

    var files = $('#InputArchivosNovedad').get(0).files;


    for (var i = 0; i < files.length; i++) {
        data.append("Archivos", files[i]);

    }
    llamadaGuardadoArchivo(data)

}

    function llamadaGuardadoArchivo(data) {

    $.ajax({
        type: "POST",
        url: "/Api/Novedad/GuardarArchivoNovedad",
        contentType: false,
        processData: false,
        data: data,
        success: function (response) {
            //procesoOk = 1;
        },
        error: function (jqxhr) {
            //Failed(JSON.parse(jqxhr.responseText));
            //procesoOk = 0;
            alert('Se ha producido un error al guardar el archivo')
        }
    });
}

    function verArchivo(url) {
    var IdArchivo = url;
    window.open("/Api/Novedad/GetFile?idArchivo=" + IdArchivo, '_blank')

}

    var archivoCant = 1;
    var IdArchivosEliminados = [];
    var alta = false;

    function AgregaArchivos() {

    var archivoNuevo = "";
    archivoNuevo = '<div class="col-md-4">' +
        '<input type=\"file\" id=\"adjuntoArchivo_' + archivoCant + '\">' +
        '</div>' +
        '<div class=\"col-md-4\">' +
        '</div>';
    $('#ArchivoNuevo').append(archivoNuevo);
    archivoCant++;

}

    function myFunction(item) {
    var archivos = "";
    archivos += '<a id =\"ArchivoGuardado_' + item.idArchivo
        + '\" class=\"color-link\"href=\"\" onclick=\"verArchivo(' +
        item.idArchivo +
        ')\"' +
        '>' +
        "<i class='material-icons'>insert_drive_file</i>" +
        item.nombreArchivo +
        ' </a>' +
        '<i id =\"IconoEliminado_' + item.idArchivo +
        '\" class=\"material-icons imagen\" onclick=\"EliminarArchivosParcial(' +
        item.idArchivo +
        ')\">clear</i></br>'
        ;


    $('#arhivosYaCargados').append(archivos);
}

    function EliminarArchivosParcial(idArchivo) {
    IdArchivosEliminados.push(idArchivo);
    document.getElementById('ArchivoGuardado_' + idArchivo).style.display = "none";
    document.getElementById('IconoEliminado_' + idArchivo).style.display = "none";

}

    function EliminarArchivos(listIdArchivos) {
    var stringIds = listIdArchivos.toString();
    $.ajax({
        type: "POST",
        url: "/Api/Novedad/EliminarArchivoNovedad?listArchivo=" + stringIds,

        dataType: "json",
        success: function (response) {
            //procesoOk = 1;
        },
        error: function (jqxhr) {
            //Failed(JSON.parse(jqxhr.responseText));
            //procesoOk = 0;
            alert('Se ha producido un error al eliminar')
        }
    });
}

//====================================

function OrdenarFecha(fechaGenerica) {
    let dia = fechaGenerica.substr(8, 2);
    let mes = fechaGenerica.substr(5, 2);
    let año = fechaGenerica.substr(0, 4);
    return dia + '/' + mes + '/' + año;
}


function CargarFiltros() {
    //Creo las fechas de hasta con dia de hoy y desde con 15 dias atras
    var Hasta = new Date();
    var Desde = new Date(Hasta);
    var dias = 7;  //Dias para atras del dia de hoy para que salgan en Novedades
    Desde.setDate(Desde.getDate() - dias);
    var fechaDesdeInput = FechaformatoInput(Desde);
    var fechaHastaInput = FechaformatoInput(Hasta);

    //las cargo en las variables globales
    fechaDesde = fechaHoyMostrar(Desde);
    fechaHasta = fechaHoyMostrar(Hasta);
    //las cargo en el input
    $('#fechaDesdeFiltro').val(fechaDesdeInput);
    $('#fechaHastaFiltro').val(fechaHastaInput);

    //cargo los selectores 
    tNovedad = -1;
    act = -1;
    CargarTipoNovedad(tNovedad);
    CargarActuacion(act);
    CargarPropiedad();
    ObtenerEstadoNovedad(0);
    //ObtenerNovedadesConRelacion(-1);
}
// ===Setear Valores de los Select2===
function setSelect(select, value) {
    var evt = document.createEvent("HTMLEvents"); // crea un objeto creando un evento
    evt.initEvent("change", false, true);  // 1er argumento type.
    // 2do argumento: bubbles:Un valor binario indicando si el evento debe ser recurrente en la cadena de eventos, o no.
    // 3er argumento: cancelable: Un valor binario indicando si el evento puede ser cancelado o no.

    select.value = value; // traigo el value del select de forma nativa
    select.dispatchEvent(evt); // disparo el onclick en el select enviado por parametro
} // setea select2 realizando evento onchange manual
// ===================================

function CargarTipoNovedad(tNovedad) {
    $.ajax({
        type: 'Get',
        url: '/Api/Novedad/ObtenerListaTipoNovedad',
        dataType: "json",
        data: {},
        success: function (respuesta) {
            if (tNovedad == -1) {
                $('#tipoNovedadFiltro').append($('<option selected>').val(-1).text("Seleccione una opción"))
            }
            else {
                $('#tipoNovedadFiltro').append($('<option>').val(-1).text("Seleccione una opción"))
            }
            //verificar id y nombre que consulto
            $.each(respuesta, function (index, result) {
                if (tNovedad == result.idTipoNovedad) {
                    $('#tipoNovedadFiltro').append($('<option selected>').val(result.idTipoNovedad).text(result.nombreTipoNovedad))
                }
                else {
                    $('#tipoNovedadFiltro').append($('<option>').val(result.idTipoNovedad).text(result.nombreTipoNovedad))
                }

            });
        }
    })
}

function CargarActuacion(act) {
    var activo = true;
    $.ajax({
        type: 'Get',
        url: '/Api/Actuacion/ObtenerActuacion?activo=' + activo,
        dataType: "json",
        data: {},
        success: function (respuesta) {
            if (act == -1) {
                $('#actuacionFiltro').append($('<option selected>').val(-1).text("Seleccione una opción"))
            }
            else {
                $('#actuacionFiltro').append($('<option>').val(-1).text("Seleccione una opción"))
            }
            //verificar id y nombre que consulto
            $.each(respuesta.data, function (index, result) {
                if (act == result.idActuacion) {
                    $('#actuacionFiltro').append($('<option selected>').val(result.idActuacion).text(result.nombreActuacion))
                }
                else {
                    $('#actuacionFiltro').append($('<option>').val(result.idActuacion).text(result.nombreActuacion))
                }

            });
        }
    })
}

function ObtenerEstadoNovedad(idEstado) {
    var activo = true;
    $.ajax({
        type: 'Get',
        url: '/Api/Novedad/ObtenerEstadoNovedad?activo=' + activo,
        dataType: "json",
        data: {},
        success: function (respuesta) {
            if (idEstado == 0) {
                $('#propiedadEstado').append($('<option selected>').val(0).text("Todas"))
            }
            else {
                $('#propiedadEstado').append($('<option>').val(0).text("Todas"))
            }
            $.each(respuesta.data, function (index, result) {
                if (idEstado == result.idEstadoNovedad) {
                    $('#propiedadEstado').append($('<option selected>').val(result.idEstadoNovedad).text(result.nombreEstadoNovedad))
                }
                else {
                    $('#propiedadEstado').append($('<option>').val(result.idEstadoNovedad).text(result.nombreEstadoNovedad))
                }

            });
        }
    })
}
/*
function ObtenerNovedadesConRelacion(idNovRelacion) {
    var activo = true;
    $.ajax({
        type: 'Get',
        url: '/Api/Novedad/ObtenerNovedadesConRelacion',
        dataType: "json",
        data: {},
        success: function (respuesta) {
             
            if (idNovRelacion == -1) {
                $('#novRelacionadas').append($('<option selected>').val(-1).text("Seleccione una opcion"))
            }
            else {
                $('#novRelacionadas').append($('<option>').val(-1).text("Seleccione una opcion"))
            }
            $.each(respuesta.data, function (index, result) {
                if (idNovRelacion == result.idEstadoNovedad) {
                    $('#novRelacionadas').append($('<option selected>').val(result.idNovedadRelacionada).text(result.NombreFiltroNovRelacionada))
                }
                else {
                    $('#novRelacionadas').append($('<option>').val(result.idNovedadRelacionada).text(result.NombreFiltroNovRelacionada))
                }

            });
        }
    })
}*/


function CargarPropiedad() {
    $('#propiedadFiltro').append($('<option selected>').val(-1).text("Seleccione una opción"));
    $('#propiedadFiltro').append($('<option>').val(1).text("Distrocuyo"));
    $('#propiedadFiltro').append($('<option>').val(0).text("Terceros"));
}

function ValidarFiltros() {
    if ($("#fechaDesdeFiltro").val() == "") {
        var alert = "";
        alert += "<div class='alert alert-danger alertDesde' id='alertDesde'>" +
            "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" +
            "<i class='material-icons'>close</i>" +
            "</button>" +
            "<span>La <b>Fecha Desde</b> no puede estar vacía.</span>" +
            "</div>";

        $('#alertNovedad').clearQueue();
        $('#alertNovedad').append(alert);
        $("#fechaDesdeFiltro").focus();
        return false;
    }
    if ($("#fechaHastaFiltro").val() == "") {
        var alert = "";
        alert += "<div class='alert alert-danger alertHasta' id='alertHasta'>" +
            "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" +
            "<i class='material-icons'>close</i>" +
            "</button>" +
            "<span>La <b>Fecha Hasta</b> no puede estar vacía.</span>" +
            "</div>";

        $('#alertNovedad').clearQueue();
        $('#alertNovedad').append(alert);
        $("#fechaHastaFiltro").focus();
        return false;
    }
    if ($("#fechaDesdeFiltro").val() > $("#fechaHastaFiltro").val()) {
        var alert = "";
        alert += "<div class='alert alert-danger alertControl' id='alertControl'>" +
            "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" +
            "<i class='material-icons'>close</i>" +
            "</button>" +
            "<span>La <b>Fecha Desde</b> no puede ser mayor que la fecha Hasta.</span>" +
            "</div>";

        $('#alertNovedad').clearQueue();
        $('#alertNovedad').append(alert);

        return false;
    }
    return true;
}

function ObtenerUrl() {
    var url;
    url = "/Api/Novedad/ObtenerListaNovedad";
    url += "?activo=" + activo;
    url += "&fechaDesde=" + $("#fechaDesdeFiltro").val();;
    url += "&fechaHasta=" + $("#fechaHastaFiltro").val();; 
    url += "&descripcion=" + descripcion;
    url += "&descCompleta=" + descCompleta;
    url += "&idEquipo=" + idEquipo;
    url += "&nombreEquipo=" + nombreEquipo;
    url += "&idTipoEquipo=" + idTipoEquipo;
    url += "&tipoNovedad=" + tipoNovedad;
    url += "&actuacion=" + actuacion;
    url += "&propiedad=" + propiedad;
    url += "&codigoLicencia=" + codigoLicencia;
    url += "&estadoNoveda=" + estadoNoveda; 
    url += "&novRelacionadas=" + novRelacionadas;
    return url;
}


function obtenerIndicadores() {
    var url;
    url = "/Api/Novedad/ObtenerIndicadoresNovedad";
    url += "?activo=" + activo;
    url += "&fechaDesde=" + fechaDesde;
    url += "&fechaHasta=" + fechaHasta;
    url += "&descripcion=" + descripcion;
    url += "&descCompleta=" + descCompleta;
    url += "&idEquipo=" + idEquipo;
    url += "&nombreEquipo=" + nombreEquipo;
    url += "&idTipoEquipo=" + idTipoEquipo;
    url += "&tipoNovedad=" + tipoNovedad;
    url += "&actuacion=" + actuacion;
    url += "&propiedad=" + propiedad;
    url += "&codigoLicencia=" + codigoLicencia;


    $.ajax({
        type: 'post',
        url: url,
        data: {},
        async: true,
        dataType: 'json',
        success: function (data) {
            $("#cantidadNovedad").html(data.CantidadNovedad);
            $("#cantidadNovedadesAbiertas").html(data.CantidadNovedadesAbiertas);
            $("#cantidadNovedadesCerradas").html(data.CantidadNovedadesCerradas);
            $("#cantidadRUTE").html(data.CantidadRUTE);
        }
    });

    

}

 