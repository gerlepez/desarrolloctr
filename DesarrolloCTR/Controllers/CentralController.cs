﻿using DesarrolloCTR.Controllers.Extra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers
{
    [SessionUtility]
    [Permiso("CEN")]
    public class CentralController : Controller
    {
        // GET: Central
        public ActionResult Central()
        {
            return View();
        }

        public ActionResult AdministrarCentral()
        {
            return View();
        }

    }
}