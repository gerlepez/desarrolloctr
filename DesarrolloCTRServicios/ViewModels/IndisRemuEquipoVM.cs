﻿using System;

namespace DesarrolloCTRServicios.ViewModels
{
    public class IndisRemuEquipoVM
    {
        public int? idLineaIndisRemu { get; set; }
        public int? idIndisponibilidadesTrafo { get; set; }
        public int? idResolucion { get; set; }
        public int? remunerado { get; set; }
        public int? idPagotran { get; set; }
        public int? idlinea { get; set; }
        public int? idTrafo { get; set; }
        public int? potenciaAparente { get; set; }
        public int factorPenalizacion { get; set; }
        public int? idNovedadIndisponibilidad { get; set; }
        public int? idMonto { get; set; }

        public string cammesa { get; set; }
        public string Ukv { get; set; }
        public string nombreTransformador { get; set; }
        public string potencias { get; set; }
        public string tensiones { get; set; }
        public string muestroRealxHora { get; set; }
        public string muestroxMes { get; set; }

        public string tipoSalida { get; set; }

        public decimal? longKm { get; set; }
        public decimal? cargoRealxHora { get; set; }
        public decimal? xMes { get; set; }
        public decimal? hsIndisponible { get; set; }
        public decimal? minutosIndisponible { get; set; }
        public decimal? hsProgramada { get; set; }
        public decimal? hsForzada { get; set; }
        public decimal? montoXhora { get; set; }
        public decimal? totalCargoReal { get; set; }


        public DateTime? origenRegulatorio { get; set; }
        public string fechaOrigen => origenRegulatorio.HasValue ? origenRegulatorio.Value.ToString() : "Sin fecha";

        public bool? activo { get; set; }
        public bool? indisponible { get; set; }
        public bool? noCargar { get; set; }

        public EstacionVM estacion { get; set; }

        public DateTime? fechaSalida { get; set; }
        public string fechaSalidaRemu => fechaSalida.HasValue ? fechaSalida.Value.ToString() : "Agregar fecha";
        public DateTime? fechaEntrada { get; set; }
        public string fechaEntradaRemu => fechaEntrada.HasValue ? fechaEntrada.Value.ToString() : "Agregar fecha";

        public DateTime fechaEntradaA { get; set; }
        public DateTime fechaSalidaA { get; set; }



        //Linea
        public int? idLineaRemunerada { get; set; }
        public string nombreLinea { get; set; }

        public string MuestroinformoEnTermino { get; set; }
        public string motivo { get; set; }

        public bool? informoEnTermino { get; set; }

        public decimal? penalizacionProgramada { get; set; }
        public decimal? totalPenalizacion { get; set; }
        public decimal? PenalizacionForzada1 { get; set; }
        public decimal? PenalizacionForzada2 { get; set; }
        public decimal? PenalizacionForzada3 { get; set; }
        public decimal? cargoReal { get; set; }
        public decimal? noPercibido { get; set; }
        public decimal? lucroCesante { get; set; }

        // Trafos
        public int? idTrafosRemu { get; set; }
        public int? idIndisponibilidadTrafo { get; set; }
        public int coeficientePenalizacionPorSalidaForzada { get; set; }
        public int? idTipoIndisponibilidad { get; set; }
        public decimal? CR { get; set; }

        public string muestroENS { get; set; }

        public bool? ENS { get; set; }

        //Pto Conexion
        public int? idPuntoConexion { get; set; }
        public int? idIndisponibilidadesPtosC { get; set; }
        public int? idPtoIndisRemu { get; set; }
        public int? idPtoRemunerado { get; set; }
        public string nombrePuntoConexion { get; set; }

        public TensionVM tension { get; set; }


        //Capacitores
        public int? idIndisponibilidadCap { get; set; }
        public int? idCapacitor { get; set; }
        public int? potenciaMvra { get; set; }
        public int? idCapacitorRemunerado { get; set; }
        public int? idIndisponibilidadCapacitor { get; set; }

        public decimal? TensionKv { get; set; }


        public string nombreCapacitor { get; set; }

        //Utilidades
        public int? idUtilidad { get; set; }
        public int? idTipoEquipo { get; set; }
        public int? valorUtilidad { get; set; }


        public string nombreUtilidad { get; set; }

        public MontoIndisponibilidadesVM montoIndis { get; set; }
    }
}
