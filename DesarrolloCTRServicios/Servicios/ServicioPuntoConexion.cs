﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioPuntoConexion
    {
        public object ObtenerListaPuntoConexion(bool activo)
        {
            List<PuntoConexionVM> listaPuntoConexion;

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                listaPuntoConexion = (from c in db.PCPuntoConexion
                                      join d in db.EstEstacion on c.idEstacion equals d.idEstacion
                                      join e in db.GralTension on c.idTension equals e.idTension
                                      where c.activo == activo && c.fechaEdicion == null
                                      select new PuntoConexionVM
                                      {
                                          idPuntoConexion = c.idPuntoConexion,
                                          nombrePuntoConexion = c.nombrePuntoConexion,
                                          descripcion = c.descripcion,
                                          activo = c.activo,
                                          idPagotran = c.idPagotran,
                                          codigoPuntoConexion = c.codigoPuntoConexion,
                                          esRemunerado = c.remunerado == true ? "Si" : "No",
                                          tension = new TensionVM
                                          {
                                              idTension = e.idTension,
                                              tension = e.tension,
                                              activo = e.activo
                                          },

                                          estacion = new EstacionVM
                                          {
                                              idEstacion = d.idEstacion,
                                              codigoEstacion = d.codigoEstacion,
                                              nombreEstacion = d.nombreEstacion,
                                              activo = d.activo,
                                          },
                                      }).ToList();
                var listaOrdenada = listaPuntoConexion.OrderByDescending(x => x.idPuntoConexion);
                object json = new { data = listaOrdenada };
                return json;
            }

        }

        public object ObtenerPuntosPorEstacion(int idEstacion, int idNovedad)
        {
            List<PuntoConexionVM> listaPuntoConexion;

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                if (idNovedad == 0)
                {
                    listaPuntoConexion = (from c in db.PCPuntoConexion
                                          join d in db.EstEstacion on c.idEstacion equals d.idEstacion
                                          join e in db.GralTension on c.idTension equals e.idTension
                                          where c.activo == true && d.idEstacion == idEstacion
                                          select new PuntoConexionVM
                                          {
                                              idPuntoConexion = c.idPuntoConexion,
                                              idEquipo = c.idPuntoConexion,
                                              nombreEquipo = c.nombrePuntoConexion,
                                              nombrePuntoConexion = c.nombrePuntoConexion,
                                              descripcion = c.descripcion,
                                              activo = c.activo,
                                              idPagotran = c.idPagotran,
                                              codigoPuntoConexion = c.codigoPuntoConexion,
                                              idGeneral = c.idPuntoConexion,
                                              codigoEquipo = "PCON",
                                              esAutomatismo = c.tieneAutomatismo == null ? false : c.tieneAutomatismo,
                                              tension = new TensionVM
                                              {
                                                  idTension = e.idTension,
                                                  tension = e.tension,
                                                  activo = e.activo
                                              },

                                              estacion = new EstacionVM
                                              {
                                                  idEstacion = d.idEstacion,
                                                  codigoEstacion = d.codigoEstacion,
                                                  nombreEstacion = d.nombreEstacion,
                                                  activo = d.activo,
                                              },
                                          }).ToList();
                }
                else
                {
                    listaPuntoConexion = (from c in db.PCPuntoConexion
                                          join d in db.EstEstacion on c.idEstacion equals d.idEstacion
                                          join e in db.GralTension on c.idTension equals e.idTension
                                          join n in db.NovNovedad on idNovedad equals n.idNovedad
                                          where d.idEstacion == idEstacion && c.fechaCreacion < n.horaNovedad && (c.fechaEdicion == null || c.fechaEdicion > n.horaNovedad)
                                          select new PuntoConexionVM
                                          {
                                              idPuntoConexion = c.idPuntoConexion,
                                              idEquipo = c.idPuntoConexion,
                                              nombreEquipo = c.nombrePuntoConexion,
                                              nombrePuntoConexion = c.nombrePuntoConexion,
                                              descripcion = c.descripcion,
                                              activo = c.activo,
                                              idPagotran = c.idPagotran,
                                              codigoPuntoConexion = c.codigoPuntoConexion,
                                              idGeneral = c.idPuntoConexion,
                                              codigoEquipo = "PCON",
                                              esAutomatismo = c.tieneAutomatismo == null ? false : c.tieneAutomatismo,
                                              tension = new TensionVM
                                              {
                                                  idTension = e.idTension,
                                                  tension = e.tension,
                                                  activo = e.activo
                                              },

                                              estacion = new EstacionVM
                                              {
                                                  idEstacion = d.idEstacion,
                                                  codigoEstacion = d.codigoEstacion,
                                                  nombreEstacion = d.nombreEstacion,
                                                  activo = d.activo,
                                              },
                                          }).ToList();
                }

            }
            object json = new { data = listaPuntoConexion };
            return json;
        }
        public int AltaPuntoConexion(PuntoConexionVM puntoConexion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var nombreptoConexionR = puntoConexion.estacion.nombreEstacion + " " + puntoConexion.tension.tension + "kV " + puntoConexion.descripcion;
                var ptoConeRepetido = db.PCPuntoConexion.Where(x => x.nombrePuntoConexion == nombreptoConexionR).FirstOrDefault();
                var consultaUnPuntoConexion = db.PCPuntoConexion.Where(x => x.idPuntoConexion == puntoConexion.idPuntoConexion).FirstOrDefault();
                if (consultaUnPuntoConexion == null && ptoConeRepetido == null)
                {
                    try
                    {
                        PCPuntoConexion punto = new PCPuntoConexion
                        {
                            nombrePuntoConexion = puntoConexion.estacion.nombreEstacion + " " + puntoConexion.tension.tension + "kV " + puntoConexion.descripcion,
                            descripcion = puntoConexion.descripcion,
                            idTension = puntoConexion.tension.idTension,
                            idEstacion = puntoConexion.estacion.idEstacion,
                            activo = true,
                            codigoPuntoConexion = puntoConexion.codigoPuntoConexion,
                            idPagotran = puntoConexion.idPagotran,
                            fechaCreacion = DateTime.Now,
                            remunerado = puntoConexion.remunerado,
                        };
                        db.PCPuntoConexion.Add(punto);
                        db.SaveChanges();

                        var kv = db.GralTension.Where(x => x.activo == true && x.idTension == punto.idTension).FirstOrDefault();
                        decimal? cargoRealxHora = 0; decimal? xMes = 0;
                        var UkvDecimal = decimal.Parse(kv.tension); // Convieto tension en DECIMAL
                        var monto = db.IndisponibilidadMontoPtosConexion.Where(x => x.activo == true && x.kv == UkvDecimal).FirstOrDefault(); // Busco el monto de la tension

                        if (puntoConexion.remunerado == true)
                        {
                            if (monto != null)
                            {
                                var fechaHoy = new DateTime(); var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;
                                int cantidadDias = DateTime.DaysInMonth(fechaHoy.Year, fechaHoy.Month);

                                if (UkvDecimal == 220)
                                {
                                    cargoRealxHora = monto.monto * Convert.ToDecimal("1.25");
                                    xMes = cargoRealxHora * (cantidadDias * 24);
                                }
                                else
                                {
                                    cargoRealxHora = monto.monto;
                                    xMes = cargoRealxHora * (cantidadDias * 24);
                                }
                                IndisponibilidadRemuPtosConexion PtoCRemu = new IndisponibilidadRemuPtosConexion
                                {
                                    idPuntoConexion = punto.idPuntoConexion,
                                    cargoRealxHora = cargoRealxHora,
                                    xMes = xMes,
                                    activo = true,
                                };
                                db.IndisponibilidadRemuPtosConexion.Add(PtoCRemu);
                                db.SaveChanges();
                            }
                            else
                            {
                                IndisponibilidadRemuPtosConexion PtoCRemu = new IndisponibilidadRemuPtosConexion
                                {
                                    idPuntoConexion = punto.idPuntoConexion,
                                    activo = true,
                                };
                                db.IndisponibilidadRemuPtosConexion.Add(PtoCRemu);
                                db.SaveChanges();
                            }

                        }
                        return 200;


                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //ya existe ese dato
                }
            }
        }

        public int ModificarPuntoConexion(PuntoConexionVM puntoConexion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnPuntoConexion = db.PCPuntoConexion.Where(x => x.idPuntoConexion == puntoConexion.idPuntoConexion).FirstOrDefault();
                if (consultaUnPuntoConexion != null)
                {
                    /* consultaUnPuntoConexion.nombrePuntoConexion = puntoConexion.estacion.nombreEstacion + " " + puntoConexion.tension.tension + "kV " + puntoConexion.descripcion + " "+puntoConexion.codigoPuntoConexion ;
                     consultaUnPuntoConexion.descripcion = puntoConexion.descripcion;
                     consultaUnPuntoConexion.idTension = puntoConexion.tension.idTension;
                     consultaUnPuntoConexion.idEstacion = puntoConexion.estacion.idEstacion;
                     consultaUnPuntoConexion.activo = true;
                     consultaUnPuntoConexion.idPagotran = puntoConexion.idPagotran;
                     consultaUnPuntoConexion.codigoPuntoConexion = puntoConexion.codigoPuntoConexion;*/

                    consultaUnPuntoConexion.activo = false;
                    consultaUnPuntoConexion.fechaEdicion = DateTime.Now;
                    consultaUnPuntoConexion.remunerado = false;

                    PCPuntoConexion punto = new PCPuntoConexion
                    {
                        nombrePuntoConexion = puntoConexion.estacion.nombreEstacion + " " + puntoConexion.tension.tension + "kV " + puntoConexion.descripcion,
                        descripcion = puntoConexion.descripcion,
                        idTension = puntoConexion.tension.idTension,
                        idEstacion = puntoConexion.estacion.idEstacion,
                        activo = true,
                        codigoPuntoConexion = puntoConexion.codigoPuntoConexion,
                        idPagotran = puntoConexion.idPagotran,
                        fechaCreacion = DateTime.Now,
                        remunerado = puntoConexion.remunerado,
                    };
                    db.PCPuntoConexion.Add(punto);
                    db.SaveChanges();

                    var ultimoPto = db.PCPuntoConexion.OrderByDescending(x => x.idPuntoConexion).FirstOrDefault();

                    var AutoRelacion = db.AutRelacionAutomatismo.Where(x => x.idEquipo == consultaUnPuntoConexion.idPuntoConexion && x.idTipoEquipo == 2 && x.idLugar == consultaUnPuntoConexion.idEstacion).ToList();
                    foreach (var PtoRel in AutoRelacion)
                    {
                        if (PtoRel.activo != false)
                        {
                            AutRelacionAutomatismo a = new AutRelacionAutomatismo
                            {
                                idAutomatismo = PtoRel.idAutomatismo,
                                idTipoEquipo = PtoRel.idTipoEquipo,
                                idLugar = PtoRel.idLugar,
                                idEquipo = ultimoPto.idPuntoConexion,
                                activo = true,
                            };
                            db.AutRelacionAutomatismo.Add(a);
                            PtoRel.activo = false;
                            db.SaveChanges();
                        }
                    }

                    if (punto.remunerado == true)
                    {
                        var consultaPtoRemunerado = db.IndisponibilidadRemuPtosConexion.Where(x => x.idPuntoConexion == consultaUnPuntoConexion.idPuntoConexion).FirstOrDefault();
                        var kv = db.GralTension.Where(x => x.activo == true && x.idTension == punto.idTension).FirstOrDefault(); // Busco la tension
                        var UkvInt = kv.idTension == 1 ? 0 : Convert.ToInt32(kv.tension); // Convierto tension en INT
                        var UkvDecimal = decimal.Parse(kv.tension); // Convieto tension en DECIMAL
                        var monto = db.IndisponibilidadMontoPtosConexion.Where(x => x.activo == true && x.kv == UkvDecimal).FirstOrDefault(); // Busco el monto de la tension

                        if (consultaPtoRemunerado == null)
                        {
                            if (monto != null)
                            {
                                decimal? cargoRealxHora = 0; decimal? xMes = 0;
                                var fechaHoy = new DateTime(); var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;
                                int cantidadDias = DateTime.DaysInMonth(fechaHoy.Year, fechaHoy.Month);

                                if (UkvInt == 220)
                                {
                                    cargoRealxHora = monto.monto * Convert.ToDecimal("1.25");
                                    xMes = cargoRealxHora * (cantidadDias * 24);
                                }
                                else
                                {
                                    cargoRealxHora = monto.monto;
                                    xMes = cargoRealxHora * (cantidadDias * 24);
                                }
                                IndisponibilidadRemuPtosConexion PtoCRemu = new IndisponibilidadRemuPtosConexion
                                {
                                    idPuntoConexion = punto.idPuntoConexion,
                                    cargoRealxHora = cargoRealxHora,
                                    xMes = xMes,
                                    activo = true,
                                };
                                db.IndisponibilidadRemuPtosConexion.Add(PtoCRemu);
                                db.SaveChanges();

                                var equipoIndisponible = db.IndisponibilidadPtosConexion.Where(x => x.idPtoIndisRemu == consultaPtoRemunerado.idIndisponibilidadesPtosC && x.idPtoConexion == consultaUnPuntoConexion.idPuntoConexion).FirstOrDefault();
                                if (equipoIndisponible != null)
                                {
                                    ServicioIndisponibilidadesRemu servIndis = new ServicioIndisponibilidadesRemu();
                                    equipoIndisponible.idPtoIndisRemu = PtoCRemu.idIndisponibilidadesPtosC;
                                    equipoIndisponible.idPtoConexion = punto.idPuntoConexion;
                                    db.SaveChanges();
                                    servIndis.ModificoPtoCIndisponibles(equipoIndisponible);
                                }



                            }
                            else
                            {
                                IndisponibilidadRemuPtosConexion PtoCRemu = new IndisponibilidadRemuPtosConexion
                                {
                                    idPuntoConexion = punto.idPuntoConexion,
                                    activo = true,
                                };
                                db.IndisponibilidadRemuPtosConexion.Add(PtoCRemu);
                                db.SaveChanges();
                            }

                        }
                        else
                        {
                            consultaPtoRemunerado.activo = false;
                            if (monto != null)
                            {
                                decimal? cargoRealxHora = 0; decimal? xMes = 0;
                                var fechaHoy = new DateTime(); var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;
                                int cantidadDias = DateTime.DaysInMonth(fechaHoy.Year, fechaHoy.Month);

                                if (UkvInt == 220)
                                {
                                    cargoRealxHora = monto.monto * Convert.ToDecimal("1.25");
                                    xMes = cargoRealxHora * (cantidadDias * 24);
                                }
                                else
                                {
                                    cargoRealxHora = monto.monto;
                                    xMes = cargoRealxHora * (cantidadDias * 24);
                                }
                                IndisponibilidadRemuPtosConexion PtoCRemu = new IndisponibilidadRemuPtosConexion
                                {
                                    idPuntoConexion = punto.idPuntoConexion,
                                    cargoRealxHora = cargoRealxHora,
                                    xMes = xMes,
                                    activo = true,
                                };
                                db.IndisponibilidadRemuPtosConexion.Add(PtoCRemu);
                                db.SaveChanges();
                            }
                            else
                            {
                                IndisponibilidadRemuPtosConexion PtoCRemu = new IndisponibilidadRemuPtosConexion
                                {
                                    idPuntoConexion = punto.idPuntoConexion,
                                    activo = true,
                                };
                                db.IndisponibilidadRemuPtosConexion.Add(PtoCRemu);
                                db.SaveChanges();
                            }

                        }
                    }
                    db.SaveChanges();
                }
            }
            return 200;
        }

        public int EliminarPuntoConexion(PuntoConexionVM puntoConexion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnPuntoConexion = db.PCPuntoConexion.Where(x => x.idPuntoConexion == puntoConexion.idPuntoConexion).FirstOrDefault();
                if (consultaUnPuntoConexion != null)
                {
                    consultaUnPuntoConexion.activo = false;
                }
                db.SaveChanges();
            }
            return 200;
        }

        public PuntoConexionVM ConsultarUnPuntoDeConexion(int? idPuntoConexion)
        {
            PuntoConexionVM puntoConexion = new PuntoConexionVM();

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                puntoConexion = (from c in db.PCPuntoConexion
                                 join d in db.EstEstacion on c.idEstacion equals d.idEstacion
                                 join e in db.GralTension on c.idTension equals e.idTension
                                 where c.idPuntoConexion == idPuntoConexion
                                 select new PuntoConexionVM
                                 {
                                     idPuntoConexion = c.idPuntoConexion,
                                     nombrePuntoConexion = c.nombrePuntoConexion,
                                     nombreEquipo = "Punto de Conexión",
                                     descripcion = c.descripcion,
                                     activo = c.activo,
                                     idPagotran = c.idPagotran,
                                     codigoPuntoConexion = c.codigoPuntoConexion,
                                     remunerado = c.remunerado,
                                     tension = new TensionVM
                                     {
                                         idTension = e.idTension,
                                         tension = e.tension,
                                         activo = e.activo
                                     },
                                     estacion = new EstacionVM
                                     {
                                         idEstacion = d.idEstacion,
                                         codigoEstacion = d.codigoEstacion,
                                         nombreEstacion = d.nombreEstacion,
                                         activo = d.activo
                                     },
                                 }).FirstOrDefault();
            }
            return puntoConexion;
        }

        public int RecuperarPuntoConexion(PuntoConexionVM puntoConexion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnPuntoConexion = db.PCPuntoConexion.Where(x => x.idPuntoConexion == puntoConexion.idPuntoConexion).FirstOrDefault();
                if (consultaUnPuntoConexion != null)
                {
                    consultaUnPuntoConexion.activo = true;
                }

                db.SaveChanges();
            }
            return 200;
        }

        public bool ValidarPuntoConexion(PuntoConexionVM puntoConexion)
        {
            if (!string.IsNullOrWhiteSpace(puntoConexion.descripcion) &&
                puntoConexion.tension.idTension > 0 &&
                puntoConexion.estacion.idEstacion > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public List<IndisponibilidadVM> ConsultarIndisponibilidadPuntoConexion(int idPuntoConexion, int idNovedad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                List<IndisponibilidadVM> listaDisponibilidad = new List<IndisponibilidadVM>();
                var consultaUnPunto = db.PCPuntoConexion.Where(x => x.idPuntoConexion == idPuntoConexion).FirstOrDefault();

                return listaDisponibilidad;
            }
        }



    }
}
