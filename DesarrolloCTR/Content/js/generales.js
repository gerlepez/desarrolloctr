﻿function ActivarItem(idItem) {
    $('.nav-item').removeClass('active');
    $(idItem).addClass('active');
}

function GetUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function FechaformatoInput(fecha) {
    var dd = fecha.getDate();
    var mm = fecha.getMonth() + 1;
    var yyyy = fecha.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }

    fechaString = yyyy + '-' + mm + '-' + dd;
    return fechaString;
}

function UnirFechaYHora(f, h) {
    var fechaHora = new Date(f);
    var hours = h.split(":")[0];
    var minutes = h.split(":")[1];
    fechaHora.setHours(hours);
    fechaHora.setMinutes(minutes);

    return fechaHora;
}

function fechaHoyInput() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }

    today = yyyy + '-' + mm + '-' + dd;
    return today;
}

function fechaHoyMostrar(date) {

    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var fechaString = dd + '/' + mm + '/' + yyyy;
    return fechaString;

}

function fechaHoyMostrar(date) {

    var dd = date.getDate();
    var mm = date.getMonth() + 1;
    var yyyy = date.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var fechaString = dd + '/' + mm + '/' + yyyy;
    return fechaString;

}

function ObtenerHora(d) {
    var h = d.getHours();
    var m = d.getMinutes();
    if (h < 10) {
        h = '0' + h;
    }
    if (m < 10) {
        m = '0' + m;
    }

    var hora = h + ":" + m;
    return hora;
}