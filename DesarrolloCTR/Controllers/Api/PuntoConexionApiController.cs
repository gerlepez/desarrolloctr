﻿using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DesarrolloCTR.Controllers.Api
{
    public class PuntoConexionApiController : ApiController
    {
        [System.Web.Http.Route("Api/PuntoConexion/ObtenerListaPuntoConexion")]
        [System.Web.Http.ActionName("ObtenerListaPuntoConexion")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaPuntoConexion(bool activo)
        {
            ServicioPuntoConexion servicioPuntoConexion = new ServicioPuntoConexion();
            object lista = servicioPuntoConexion.ObtenerListaPuntoConexion(activo);
            return lista;
        }


        [System.Web.Http.Route("Api/PuntoConexion/AltaPuntoConexion")]
        [System.Web.Http.ActionName("AltaPuntoConexion")]
        [System.Web.Http.HttpPost]
        public int AltaPuntoConexion(PuntoConexionVM puntoConexion)
        {
            ServicioPuntoConexion servicioPuntoConexion = new ServicioPuntoConexion();
            int codigo;
            if (servicioPuntoConexion.ValidarPuntoConexion(puntoConexion))
            {
                codigo = servicioPuntoConexion.AltaPuntoConexion(puntoConexion);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }

        [System.Web.Http.Route("Api/PuntoConexion/ModificarPuntoConexion")]
        [System.Web.Http.ActionName("ModificarPuntoConexion")]
        [System.Web.Http.HttpPost]
        public int ModificarPuntoConexion(PuntoConexionVM puntoConexion)
        {
            ServicioPuntoConexion servicioPuntoConexion = new ServicioPuntoConexion();
            int codigo;
            if (servicioPuntoConexion.ValidarPuntoConexion(puntoConexion))
            {
                codigo = servicioPuntoConexion.ModificarPuntoConexion(puntoConexion);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }

        [System.Web.Http.Route("Api/PuntoConexion/EliminarPuntoConexion")]
        [System.Web.Http.ActionName("EliminarPuntoConexion")]
        [System.Web.Http.HttpPost]
        public int EliminarPuntoConexion(PuntoConexionVM puntoConexion)
        {
            ServicioPuntoConexion servicioPuntoConexion = new ServicioPuntoConexion();
            int codigo = servicioPuntoConexion.EliminarPuntoConexion(puntoConexion);
            return codigo;
        }

        [System.Web.Http.Route("Api/PuntoConexion/ConsultarUnPuntoDeConexion")]
        [System.Web.Http.ActionName("ConsultarUnPuntoDeConexion")]
        [System.Web.Http.HttpGet]
        public PuntoConexionVM ConsultarUnPuntoDeConexion(int idPuntoConexion)
        {
            ServicioPuntoConexion servicioPuntoConexion = new ServicioPuntoConexion();
            PuntoConexionVM puntoConexion = servicioPuntoConexion.ConsultarUnPuntoDeConexion(idPuntoConexion);
            return puntoConexion;
        }

        [System.Web.Http.Route("Api/PuntoConexion/RecuperarPuntoConexion")]
        [System.Web.Http.ActionName("RecuperarPuntoConexion")]
        [System.Web.Http.HttpPost]
        public int RecuperarPuntoConexion(PuntoConexionVM puntoConexion)
        {
            ServicioPuntoConexion servicioPuntoConexion = new ServicioPuntoConexion();
            int codigo = servicioPuntoConexion.RecuperarPuntoConexion(puntoConexion);
            return codigo;
        }

        [System.Web.Http.Route("Api/PuntoConexion/ObtenerListaPuntoConexionPorEstacion")]
        [System.Web.Http.ActionName("ObtenerListaPuntoConexionPorEstacion")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaPuntoConexionPorEstacion(int idEstacion, int idNovedad = 0)
        {
            ServicioPuntoConexion servicioPuntoConexion = new ServicioPuntoConexion();
            object lista = servicioPuntoConexion.ObtenerPuntosPorEstacion(idEstacion, idNovedad);
            return lista;
        }
        [System.Web.Http.Route("Api/PuntoConexion/ConsultarIndisponibilidadPuntoConexion")]
        [System.Web.Http.ActionName("ConsultarIndisponibilidadPuntoConexion")]
        [System.Web.Http.HttpGet]
        public object ConsultarIndisponibilidadPuntoConexion(int idPuntoConexion, int idNovedad)
        {
            ServicioPuntoConexion servicioPuntoConexion = new ServicioPuntoConexion();
            var indisponibilidad = servicioPuntoConexion.ConsultarIndisponibilidadPuntoConexion(idPuntoConexion, idNovedad);

            //((IDisposable)servLinea).Dispose();

            return indisponibilidad;
        }
    }
}
