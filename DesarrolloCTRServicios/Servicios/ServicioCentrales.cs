﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{

    public class ServicioCentrales
    {
        IntranetCTREntities db = new IntranetCTREntities();

        public object ObtenerListaCentrales(bool? activo)
        {
            List<CentralVM> listaCentrales;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                ServicioUsuario usuarioservice = new ServicioUsuario();
                listaCentrales = (from cen in db.CConCentral
                                  join cc in db.CConCentroControl on cen.idCentroControl equals cc.idCentroControl
                                  join n in db.CConTipoCentral on cen.idTipoCentral equals n.idTipoCentral
                                  where cen.activo == activo
                                  select new CentralVM()
                                  {
                                      idCentral = cen.idCentral,
                                      nombreCentral = cen.nombreCentral,
                                      activo = cen.activo,

                                      idTipoCentral = cen.idTipoCentral,
                                      nombreTipoCentral = n.nombreTipoCentral,


                                      TieneGenerador = cen.TieneGenerador,
                                      propiedadGenerador = cen.TieneGenerador == true ? "Tiene" : "No Tiene",

                                      telefono = cen.telefono,
                                      centroControl = new CentroControlVM
                                      {
                                          idCentroControl = cc.idCentroControl,
                                          nombreCentroControl = cc.nombreCentroControl,
                                          activo = cc.activo
                                      }
                                  }).ToList();


                object json = new { data = listaCentrales };
                return json;
            }
        }


        public int ObtenerTipoCentroControl(int idCentroControl)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var centroControl = db.CConCentroControl.Where(x => x.idCentroControl == idCentroControl).FirstOrDefault();
                var tipo = Convert.ToInt32(centroControl.idTipo);
                return tipo;
            }

        }


        public object TablaCentrales(bool? activo, string draw, int recordsTotal, int skip, int pageSize)
        {

            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    ServicioUsuario usuarioservice = new ServicioUsuario();
                    IQueryable<CentralVM> query = (from cen in db.CConCentral
                                                   join cc in db.CConCentroControl on cen.idCentroControl equals cc.idCentroControl
                                                   join n in db.CConTipoCentral on cen.idTipoCentral equals n.idTipoCentral
                                                   where cen.activo == activo
                                                   select new CentralVM()
                                                   {
                                                       idCentral = cen.idCentral,
                                                       nombreCentral = cen.nombreCentral,
                                                       activo = cen.activo,

                                                       idTipoCentral = cen.idTipoCentral,
                                                       nombreTipoCentral = n.nombreTipoCentral,


                                                       TieneGenerador = cen.TieneGenerador,
                                                       propiedadGenerador = cen.TieneGenerador == true ? "Tiene" : "No Tiene",

                                                       centroControl = new CentroControlVM
                                                       {
                                                           idCentroControl = cc.idCentroControl,
                                                           nombreCentroControl = cc.nombreCentroControl,
                                                           activo = cc.activo
                                                       }
                                                   }).OrderByDescending(x => x.idCentral).AsQueryable();

                    var queryFiltrada = query;
                    recordsTotal = queryFiltrada.Count();
                    var lst = queryFiltrada.Skip(skip).Take(pageSize).ToList();

                    object json = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = lst };
                    return json;
                }
            }
            catch (Exception e)
            {
                return 400;
            }
        }

        public CentralVM ConsultarUnaCentral(int? idCentro)
        {
            CentralVM central = new CentralVM();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                central = (from cen in db.CConCentral
                           join cc in db.CConCentroControl on cen.idCentroControl equals cc.idCentroControl
                           where cen.idCentral == idCentro
                           select new CentralVM()
                           {
                               idCentral = cen.idTipoCentral,
                               nombreCentral = cen.nombreCentral,
                               idCentroControl = cc.idCentroControl,
                               activo = cen.activo,
                               idTipoCentral = cen.idTipoCentral,
                               TieneGenerador = cen.TieneGenerador,
                               telefono = cen.telefono
                           }).FirstOrDefault();
            }
            return central;
        }

        public List<CentralVM> ObtenerCentralPorCentro(int idCentro)
        {
            var central = (from cen in db.CConCentral
                           where cen.activo == true && cen.idCentroControl == idCentro //&& cen.fechaEdicion == null  // se quito por que evitaba traer centrales editadas
                           select new CentralVM()
                           {
                               activo = cen.activo,
                               nombreCentral = cen.nombreCentral,
                               idCentral = cen.idCentral,
                               TieneGenerador = cen.TieneGenerador,
                               telefono = cen.telefono
                           }).ToList();

            return central;
        }

        public int EliminarCentral(CentralVM central)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnaCentral = db.CConCentral.Where(x => x.idCentral == central.idCentral).FirstOrDefault();
                if (consultaUnaCentral != null)
                {
                    consultaUnaCentral.activo = false;
                }
                db.SaveChanges();
            }
            return 200;
        }

        public int RecuperarCentral(CentralVM central)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnCentral = db.CConCentral.Where(x => x.idCentral == central.idCentral).FirstOrDefault();
                if (consultaUnCentral != null)
                {
                    consultaUnCentral.activo = true;
                }
                db.SaveChanges();
            }
            return 200;
        }

        public int AltaCentral(CentralVM central, int? idUsuario)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var centralRepetido = db.CConCentral.Where(x => x.nombreCentral == central.nombreCentral && x.idCentroControl == central.centroControl.idCentroControl && x.idTipoCentral == central.idTipoCentral).FirstOrDefault();

                var consultaUnaCentral = db.CConCentral.Where(x => x.idCentral == central.idCentral).FirstOrDefault();
                if (consultaUnaCentral == null && centralRepetido == null)
                {
                    try
                    {
                        CConCentral c = new CConCentral
                        {
                            nombreCentral = central.nombreCentral,
                            idCentroControl = central.centroControl.idCentroControl,
                            idUsuarioAlta = idUsuario,
                            fechaCreacion = DateTime.Now,
                            activo = true,
                            idTipoCentral = central.idTipoCentral,
                            TieneGenerador = central.idTipoCentral >= 3 ? false : true,
                            telefono = central.telefono,
                        };
                        db.CConCentral.Add(c);
                        db.SaveChanges();
                        return 200;
                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300;
                }

            }
        }

        public bool ValidarCentral(CentralVM central)
        {
            if (central == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public int ModificarCentral(CentralVM central, int idUsuario)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnaCentral = db.CConCentral.Where(x => x.idCentral == central.idCentral).FirstOrDefault();
                if (consultaUnaCentral != null)
                {
                    consultaUnaCentral.nombreCentral = central.nombreCentral;
                    consultaUnaCentral.idCentroControl = central.centroControl.idCentroControl;
                    consultaUnaCentral.idUsuarioModifica = idUsuario;
                    consultaUnaCentral.fechaUsuarioModifica = DateTime.Now;
                    consultaUnaCentral.activo = true;
                    consultaUnaCentral.idTipoCentral = central.idTipoCentral;
                    consultaUnaCentral.TieneGenerador = true; //central.idTipoCentral >= 3 ? false : true; //antes se usaba así para diferenciar un parque solar
                    consultaUnaCentral.telefono = central.telefono;
                    consultaUnaCentral.fechaEdicion = DateTime.Now;
                    consultaUnaCentral.idUsuarioModifica = idUsuario;
                    db.SaveChanges();

                    /*
                                        consultaUnaCentral.fechaEdicion = DateTime.Now;
                                        consultaUnaCentral.activo = false;

                                        CConCentral c = new CConCentral
                                        {
                                            nombreCentral = central.nombreCentral,
                                            idCentroControl = central.centroControl.idCentroControl,
                                            idUsuarioAlta = idUsuario,
                                            fechaCreacion = DateTime.Now,
                                            activo = true,
                                            idTipoCentral = central.idTipoCentral,
                                            TieneGenerador = central.idTipoCentral >= 3 ? false : true,
                                            telefono = central.telefono,
                                        };
                                        db.CConCentral.Add(c);
                                        db.SaveChanges();

                                        var ultimaCentral = db.CConCentral.OrderByDescending(x => x.idCentral).FirstOrDefault(); //Me trae la ultima Central que acabo de crear

                                        var Generadores = db.GenGenerador.Where(x => x.idCentral == consultaUnaCentral.idCentral).ToList(); //Todos los generadores con el idCentral viejo
                                        foreach(var generador in Generadores)
                                        {
                                            generador.idCentral = ultimaCentral.idCentral;
                                            db.SaveChanges();
                                        }


                                       //RELACION CON AUTOMATISMOS CREO UNO NUEVA RELACION CON EL IDCENTRAL NUEVO y con el idViejo le pongo activo = false
                                    var AutoRelacion = db.AutRelacionAutomatismo.Where(x => x.idLugar == consultaUnaCentral.idCentral).ToList(); //Cambia el idCentral viejo por el nuevo idCentral en las relaciones de Automatismo
                                        foreach (var CentralRel in AutoRelacion)
                                        {
                                            if(CentralRel.activo != false)
                                            {
                                                AutRelacionAutomatismo a = new AutRelacionAutomatismo
                                                {
                                                    idAutomatismo = CentralRel.idAutomatismo,
                                                    idTipoEquipo = CentralRel.idTipoEquipo,
                                                    idLugar = ultimaCentral.idCentral,
                                                    idEquipo = CentralRel.idEquipo,
                                                    activo = true,
                                                };
                                                db.AutRelacionAutomatismo.Add(a);
                                            }

                                            CentralRel.activo = false;
                                            db.SaveChanges();
                                        }*/

                }
            }

            return 200;
        }
    }
}
