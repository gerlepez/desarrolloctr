﻿using DesarrolloCTR.Controllers.Extra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;


namespace DesarrolloCTR.Controllers
{
    [SessionUtilityAttribute]
    public class LibroNovedadController : Controller
    {
        public ActionResult LibroNovedad()
        {
            return View();
        }
    }
}
