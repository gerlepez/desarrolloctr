﻿using System;

namespace DesarrolloCTRServicios.ViewModels
{
    public class HistorialEstacionVM
    {
        public int idHistorialEstacion { get; set; }
        public int idEstacion { get; set; }
        public int idEstadoEstacion { get; set; }
        public int idUsuario { get; set; }
        public DateTime fechaCambioEstado { get; set; }
        public bool activo { get; set; }
    }
}
