﻿using System;

namespace DesarrolloCTRServicios.ViewModels
{
    public class MontoIndisponibilidadesVM
    {
        public int? idIndisponibilidadMontos { get; set; }
        public int? idIndisponibilidadMontosPtsC { get; set; }
        public int? idIndisponibilidadHistorialMontLinea { get; set; }
        public int? idIndisponibilidadHistorialMontTrafo { get; set; }
        public int? idIndisponibilidadHistorialMontPtosC { get; set; }
        public int? idIndisponibilidadHistorialMontCap { get; set; }
        public int? tipoEquipo { get; set; }
        public int? kv { get; set; }
        public int? idMontoPtosC { get; set; }
        public int? ku { get; set; }

        public int? idMontoCapacitor { get; set; }

        public string nombreMonto { get; set; }
        public string nombreMontoCap { get; set; }
        public string mostrarMonto { get; set; }
        public string resolucion { get; set; }

        public string montoCap { get; set; }
        public string fechaString { get; set; }


        public decimal? kvPtosC { get; set; }
        public decimal mont { get; set; }
        public decimal? monto { get; set; }

        public bool? activo { get; set; }


        public DateTime? fecha { get; set; }
        public string fechaCreacion => fecha.HasValue ? fecha.Value.ToString("dd-MM-yy") : "Sin fecha";

        public DateTime? fechaDesde { get; set; }
        public string fechaDesdeF => fechaDesde.HasValue ? fechaDesde.Value.ToString("dd-MM-yy") : "Sin fecha";
        public DateTime? fechaHasta { get; set; }
        public string fechaHastaF => fechaHasta.HasValue ? fechaHasta.Value.ToString("dd-MM-yy") : "Sin fecha";


        public DateTime? fechaCap { get; set; }
        public string fechaCapacitor => fechaCap.HasValue ? fechaCap.Value.ToString("dd-MM-yy") : " ";



        //Montos Meses
        public int idRemuneracionMes { get; set; }
        public int? idEquipoMes { get; set; }

        public string nombreRemuneracion { get; set; }
        public string mostrarcalculoParcial { get; set; }
        public string mostrarcalculoTotal { get; set; }
        public string mostrarcalculoCammesa { get; set; }
        public string mostrardiferencia { get; set; }

        public decimal? calculoParcial { get; set; }
        public decimal? calculoTotal { get; set; }
        public decimal? calculoCammesa { get; set; }
        public decimal? diferencia { get; set; }

        public DateTime? fechaMes { get; set; }
    }
}
