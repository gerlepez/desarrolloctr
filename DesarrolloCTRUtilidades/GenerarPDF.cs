﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;


namespace DesarrolloCTRUtilidades
{

    public class PDFFooter : PdfPageEventHelper {
        public override void OnEndPage(PdfWriter writer, Document document)
        {

            #region Font 
            // Creamos el tipo de Font que vamos utilizar
            iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

            iTextSharp.text.Font _standardFontChica = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            iTextSharp.text.Font _standardFontChica2 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 6, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            #endregion
            base.OnEndPage(writer, document);

            PdfPTable footer = new PdfPTable(1);
            footer.TotalWidth = 800f;

            PdfPCell blankCell = new PdfPCell(new Phrase(Chunk.NEWLINE));
            blankCell.BorderWidthBottom = 0;
            blankCell.BorderWidthLeft = 0;
            blankCell.BorderWidthTop = 0;
            blankCell.BorderWidthRight = 0;
            DateTime hoy = DateTime.Now;
            footer.AddCell(blankCell);

            PdfPCell pie = new PdfPCell(new Phrase("Impreso: "+hoy.ToString() +" libro de novedades",_standardFontChica));
            pie.BorderWidthBottom = 0;
            pie.BorderWidthLeft = 0;
            pie.BorderWidthTop = 0;
            pie.BorderWidthRight = 0;

            footer.AddCell(pie);
            footer.WriteSelectedRows(0, -1, 25, document.BottomMargin, writer.DirectContent);
        }
    }

    public class PDFHeader : PdfPageEventHelper
    {
        public override void OnStartPage(PdfWriter writer, Document document)
        {
            base.OnStartPage(writer, document);
            PdfPTable titulo = new PdfPTable(1);
            titulo.TotalWidth = 800f;
            titulo.WidthPercentage = 100;
            PdfPTable titulo2 = new PdfPTable(1);
            titulo2.TotalWidth = 800f;
            titulo2.WidthPercentage = 100;
            PdfPCell pdfcell = new PdfPCell(new Phrase("LIBRO DE NOVEDADES"));
            pdfcell.HorizontalAlignment = Element.ALIGN_LEFT;
            pdfcell.BorderWidthBottom = 0;
            pdfcell.BorderWidthTop = 0;
            pdfcell.BorderWidthLeft = 0;
            pdfcell.BorderWidthRight = 0;
            var rutaLogo = ConfigurationSettings.AppSettings["rutaLogo"].ToString();

            //Aca va la imagen
            iTextSharp.text.Image image1 = iTextSharp.text.Image.GetInstance(rutaLogo+"logodistro.jpg");
            //image1.ScalePercent(50f);            
            image1.ScaleAbsoluteWidth(300);
            image1.ScaleAbsoluteHeight(70);
            image1.SetAbsolutePosition(30, 700);
            document.Add(image1);


            PdfPCell pdfcell2 = new PdfPCell(new Phrase("del Centro de Telecontrol Regional Cuyo"));
            pdfcell2.HorizontalAlignment = Element.ALIGN_LEFT;
            pdfcell2.BorderWidthBottom = 0;
            pdfcell2.BorderWidthTop = 0;
            pdfcell2.BorderWidthLeft = 0;
            pdfcell2.BorderWidthRight = 0;


            titulo.AddCell(pdfcell);
            
            titulo2.AddCell(pdfcell2);

            
            titulo.WriteSelectedRows(0, -1, 228, 805, writer.DirectContent);
            titulo2.WriteSelectedRows(0, -1, 188, 685, writer.DirectContent);
        }
    }
    public class GenerarPDF
    {
        public string GenerarLibroNovedades(List<TurnoPdfVM> turnos,string fechaDesde,string fechaHasta)
        {
            if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/TempFolder/")))
                Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/TempFolder/"));
            var pathInic = "~/TempFolder/";

            var nombreArchivo = "Libro_Novedades_" + fechaDesde + "--" + fechaHasta+".pdf";

            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(pathInic), nombreArchivo);
            
            Document doc = new Document();
            doc.SetPageSize(iTextSharp.text.PageSize.A4);
            doc.SetMargins(30, 30, 200, 50);
            // Indicamos donde vamos a guardar el documento
            PdfWriter writer = PdfWriter.GetInstance(doc,
                                        new FileStream(path, FileMode.Create));
            // Abrimos el archivo
            writer.PageEvent = new PDFHeader();
            writer.PageEvent = new PDFFooter();
            // Abrimos el archivo
            
            doc.Open();

            #region Font 
            // Creamos el tipo de Font que vamos utilizar
            iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

            iTextSharp.text.Font _standardFontChica = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 9, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

            iTextSharp.text.Font _headerFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11, iTextSharp.text.Font.BOLD, BaseColor.BLACK);

            iTextSharp.text.Font _titleFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);

            iTextSharp.text.Font _emptyFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 2, iTextSharp.text.Font.BOLD, BaseColor.BLACK);

            iTextSharp.text.Font _title = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);

            iTextSharp.text.Font _headerItem = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
            #endregion

            
            foreach (var turno in turnos)
            {
                #region Encabezado Turno

                PdfPTable encabezadoTurno = new PdfPTable(4);
                encabezadoTurno.WidthPercentage = 100;
                encabezadoTurno.SetWidths(new float[] { 20f, 40f, 20f, 20f });

                PdfPCell celdaDia = new PdfPCell(new Phrase("Dia: ", _standardFontChica));
                celdaDia.HorizontalAlignment = Element.ALIGN_LEFT;
                celdaDia.BorderWidthBottom = 0;
                celdaDia.BorderWidthTop = 0.75f;
                celdaDia.BorderWidthLeft = 0.75f;
                celdaDia.BorderWidthRight = 0;

                PdfPCell celdaFecha = new PdfPCell(new Phrase(turno.fechaTurno.ToString().Split(' ')[0], _standardFontChica));
                celdaFecha.HorizontalAlignment = Element.ALIGN_LEFT;
                celdaFecha.BorderWidthBottom = 0;
                celdaFecha.BorderWidthTop = 0.75f;
                celdaFecha.BorderWidthLeft = 0.75f;
                celdaFecha.BorderWidthRight = 0;

                PdfPCell celdaJefeTurno = new PdfPCell(new Phrase("Jefe Turno", _standardFontChica));
                celdaJefeTurno.HorizontalAlignment = Element.ALIGN_LEFT;
                celdaJefeTurno.BorderWidthBottom = 0;
                celdaJefeTurno.BorderWidthTop = 0.75f;
                celdaJefeTurno.BorderWidthLeft = 0.75f;
                celdaJefeTurno.BorderWidthRight = 0;

                PdfPCell celdaJefe = new PdfPCell(new Phrase(turno.nombreJefeTurno, _standardFontChica));
                celdaJefe.HorizontalAlignment = Element.ALIGN_LEFT;
                celdaJefe.BorderWidthBottom = 0;
                celdaJefe.BorderWidthTop = 0.75f;
                celdaJefe.BorderWidthLeft = 0.75f;
                celdaJefe.BorderWidthRight = 0.75f;

                PdfPCell celdaTurno = new PdfPCell(new Phrase("Turno: ", _standardFontChica));
                celdaTurno.HorizontalAlignment = Element.ALIGN_LEFT;
                celdaTurno.BorderWidthBottom = 0;
                celdaTurno.BorderWidthTop = 0.75f;
                celdaTurno.BorderWidthLeft = 0.75f;
                celdaTurno.BorderWidthRight = 0;

                PdfPCell celdaTurno1 = new PdfPCell(new Phrase(turno.horaAperturaStr.ToString().Split('.')[0]+ " - "+turno.horaCierreStr.ToString().Split('.')[0] , _standardFontChica));
                celdaTurno1.HorizontalAlignment = Element.ALIGN_LEFT;
                celdaTurno1.BorderWidthBottom = 0;
                celdaTurno1.BorderWidthTop = 0.75f;
                celdaTurno1.BorderWidthLeft = 0.75f;
                celdaTurno1.BorderWidthRight = 0;

                PdfPCell celdaOperador = new PdfPCell(new Phrase("Operador", _standardFontChica));
                celdaOperador.HorizontalAlignment = Element.ALIGN_LEFT;
                celdaOperador.BorderWidthBottom = 0;
                celdaOperador.BorderWidthTop = 0.75f;
                celdaOperador.BorderWidthLeft = 0.75f;
                celdaOperador.BorderWidthRight = 0;

                var acompañantes = "";
                if (turno.nombreAcompañantes.Count > 0)
                {
                    foreach (var acompañante in acompañantes)
                    {
                        acompañantes = acompañantes + acompañante;
                    }
                }

                PdfPCell celdaOperadorNombre = new PdfPCell(new Phrase(acompañantes, _standardFontChica));
                celdaOperadorNombre.HorizontalAlignment = Element.ALIGN_LEFT;
                celdaOperadorNombre.BorderWidthBottom = 0;
                celdaOperadorNombre.BorderWidthTop = 0.75f;
                celdaOperadorNombre.BorderWidthLeft = 0.75f;
                celdaOperadorNombre.BorderWidthRight = 0.75f;

                PdfPCell celdaClima = new PdfPCell(new Phrase("Estado del Tiempo", _standardFontChica));
                celdaClima.HorizontalAlignment = Element.ALIGN_LEFT;
                celdaClima.BorderWidthBottom = 0.75f;
                celdaClima.BorderWidthTop = 0.75f;
                celdaClima.BorderWidthLeft = 0.75f;
                celdaClima.BorderWidthRight = 0;

                PdfPCell celdaClima1 = new PdfPCell(new Phrase(turno.estadoTiempo, _standardFontChica));
                celdaClima1.HorizontalAlignment = Element.ALIGN_LEFT;
                celdaClima1.BorderWidthBottom = 0.75f;
                celdaClima1.BorderWidthTop = 0.75f;
                celdaClima1.BorderWidthLeft = 0.75f;
                celdaClima1.BorderWidthRight = 0;

                PdfPCell celdaOperador2 = new PdfPCell(new Phrase(" ", _standardFontChica));
                celdaOperador2.HorizontalAlignment = Element.ALIGN_LEFT;
                celdaOperador2.BorderWidthBottom = 0.75f;
                celdaOperador2.BorderWidthTop = 0;
                celdaOperador2.BorderWidthLeft = 0.75f;
                celdaOperador2.BorderWidthRight = 0;

                PdfPCell celdaOperadorNombre1 = new PdfPCell(new Phrase(" ", _standardFontChica));
                celdaOperadorNombre1.HorizontalAlignment = Element.ALIGN_LEFT;
                celdaOperadorNombre1.BorderWidthBottom = 0.75f;
                celdaOperadorNombre1.BorderWidthTop = 0;
                celdaOperadorNombre1.BorderWidthLeft = 0.75f;
                celdaOperadorNombre1.BorderWidthRight = 0.75f;


                encabezadoTurno.AddCell(celdaDia);
                encabezadoTurno.AddCell(celdaFecha);
                encabezadoTurno.AddCell(celdaJefeTurno);
                encabezadoTurno.AddCell(celdaJefe);
                encabezadoTurno.AddCell(celdaTurno);
                encabezadoTurno.AddCell(celdaTurno1);
                encabezadoTurno.AddCell(celdaOperador);
                encabezadoTurno.AddCell(celdaOperadorNombre);
                encabezadoTurno.AddCell(celdaClima);
                encabezadoTurno.AddCell(celdaClima1);
                encabezadoTurno.AddCell(celdaOperador2);
                encabezadoTurno.AddCell(celdaOperadorNombre1);
                #endregion

                #region
                PdfPTable espacio = new PdfPTable(1);
                espacio.WidthPercentage = 100;
                PdfPCell linea = new PdfPCell(new Phrase(" "));
                linea.HorizontalAlignment = Element.ALIGN_CENTER;
                linea.BorderWidthBottom = 0;
                linea.BorderWidthTop = 0;
                linea.BorderWidthLeft = 0;
                linea.BorderWidthRight = 0;
                espacio.AddCell(linea);
                #endregion

                #region tituloTurno

                PdfPTable tituloTurno = new PdfPTable(3);

                tituloTurno.WidthPercentage = 100;
                tituloTurno.SetWidths(new float[] { 15f, 10f, 75f });

                PdfPCell fecha = new PdfPCell(new Phrase("Fecha", _standardFontChica));
                fecha.HorizontalAlignment = Element.ALIGN_LEFT;
                fecha.BorderWidthBottom = 0;
                fecha.BorderWidthTop = 0;
                fecha.BorderWidthLeft = 0;
                fecha.BorderWidthRight = 0;
                fecha.BackgroundColor = new iTextSharp.text.BaseColor(135, 154, 163);

                PdfPCell hora = new PdfPCell(new Phrase("Hora", _standardFontChica));
                hora.HorizontalAlignment = Element.ALIGN_LEFT;
                hora.BorderWidthBottom = 0;
                hora.BorderWidthTop = 0;
                hora.BorderWidthLeft = 0;
                hora.BorderWidthRight = 0;
                hora.BackgroundColor = new iTextSharp.text.BaseColor(135, 154, 163);

                PdfPCell novedad = new PdfPCell(new Phrase("Novedad", _standardFontChica));
                novedad.HorizontalAlignment = Element.ALIGN_LEFT;
                novedad.BorderWidthBottom = 0;
                novedad.BorderWidthTop = 0;
                novedad.BorderWidthLeft = 0;
                novedad.BorderWidthRight = 0;
                novedad.BackgroundColor = new iTextSharp.text.BaseColor(135, 154, 163);

                tituloTurno.AddCell(fecha);
                tituloTurno.AddCell(hora);
                tituloTurno.AddCell(novedad);
                #endregion

                doc.Add(encabezadoTurno);
                foreach (var nov in turno.novedades)
                {
                    #region Cuerpo
                    PdfPTable cuerpoNovedad = new PdfPTable(3);

                    cuerpoNovedad.WidthPercentage = 100;
                    cuerpoNovedad.SetWidths(new float[] { 15f, 10f, 75f });

                    PdfPCell fechaNov = new PdfPCell(new Phrase(" ", _standardFontChica));
                    fechaNov.HorizontalAlignment = Element.ALIGN_LEFT;
                    fechaNov.BorderWidthBottom = 0.75f;
                    fechaNov.BorderWidthTop = 0;
                    fechaNov.BorderWidthLeft = 0;
                    fechaNov.BorderWidthRight = 0;

                    PdfPCell horaNov = new PdfPCell(new Phrase(nov.horaParseada.Split(' ')[1], _standardFontChica));
                    horaNov.HorizontalAlignment = Element.ALIGN_LEFT;
                    horaNov.BorderWidthBottom = 0.75f;
                    horaNov.BorderWidthTop = 0;
                    horaNov.BorderWidthLeft = 0;
                    horaNov.BorderWidthRight = 0;

                    if (nov.nombreActuacion == "")
                    {
                        nov.nombreActuacion = " # ";
                    }
                    var textoNota = "";
                    if (nov.nota != null && nov.nota !="")
                    {
                        textoNota = "\t\r\nNOTA: " + nov.nota;
                    }
                    var textoAutomatismos = "";
                    if (nov.automatismos != null && nov.automatismos != "")
                    {
                        textoAutomatismos = "\t\r\nAutomatismos: " + nov.automatismos;
                    }
                    PdfPCell descripcion = new PdfPCell(new Phrase(nov.nombreEquipo + " " + nov.nombreActuacion.Split('#')[1] + " " + nov.nombreActuacion.Split('#')[0] +"\t\r\nOBS: " + nov.descripcionNovedad +textoNota + textoAutomatismos, _standardFontChica));
                    descripcion.HorizontalAlignment = Element.ALIGN_LEFT;
                    descripcion.BorderWidthBottom = 0.75f;
                    descripcion.BorderWidthTop = 0;
                    descripcion.BorderWidthLeft = 0;
                    descripcion.BorderWidthRight = 0;


                    cuerpoNovedad.AddCell(fechaNov);
                    cuerpoNovedad.AddCell(horaNov);
                    cuerpoNovedad.AddCell(descripcion);


                    #endregion

                    //doc.Add(encabezadoTurno);
                    doc.Add(espacio);
                    doc.Add(tituloTurno);
                    doc.Add(cuerpoNovedad);
                    doc.Add(espacio);

                   
                }

                
            }
            
            //Todo lo anterior en el primer foreach
            doc.Close();
            writer.Close();
            return path+"#"+nombreArchivo;
        }
    }
    class _events3 : PdfPageEventHelper
    {
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            PdfPTable tblPrueba = new PdfPTable(4);
            tblPrueba.WidthPercentage = 100;
        }
    }
    class _events2 : PdfPageEventHelper
    {
        public override void OnStartPage(PdfWriter writer, Document document)
        {
            iTextSharp.text.Font _headerItem = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 11, iTextSharp.text.Font.BOLD, BaseColor.BLACK);


            PdfPTable tblTitulo = new PdfPTable(1);
            //tblPrueba.SetWidths(new float[] { 12f, 8f });
            tblTitulo.WidthPercentage = 100;

            #region Titulo
            PdfPCell clTitulo1 = new PdfPCell(new Phrase("LIBRO DE NOVEDADES", _headerItem));
            clTitulo1.HorizontalAlignment = Element.ALIGN_CENTER;
            //clFechaEmision.Colspan = 2;
            //cltitulo.BorderWidthBottom = 0.75f;
            clTitulo1.BorderWidthBottom = 0.75f;
            clTitulo1.BorderWidthLeft = 0.75f;
            clTitulo1.BorderWidthTop = 0.75f;
            clTitulo1.BorderWidthRight = 0.75f;

            tblTitulo.AddCell(clTitulo1);
            #endregion
            var imagePath = System.Web.Hosting.HostingEnvironment.MapPath("~/Content/img/logo");

            var image = iTextSharp.text.Image.GetInstance(imagePath + "/distro.jpg");
            var imageCell = new PdfPCell(image);
            Font font8 = FontFactory.GetFont("ARIAL", 6);
            PdfPTable PdfTable = new PdfPTable(1);

            PdfTable.WidthPercentage = 100F;
            PdfPCell PdfPCell = null;
            //Add Header 
            imageCell.BorderWidthRight = 0;
            

            iTextSharp.text.Image txtImage = iTextSharp.text.Image.GetInstance(image);
            txtImage.Alignment = Element.ALIGN_CENTER;
            // set width and height
            txtImage.ScaleToFit(180f, 250f);

            // adding image to document
            document.Add(txtImage);

            Font font81 = FontFactory.GetFont("ARIAL", 10);
           

            Paragraph para5 = new Paragraph("\n", font81);
            document.Add(para5);

            
        }
    }
}


