﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System.Net.Http.Formatting;

namespace DesarrolloCTR.Controllers.Api
{
    public class EquiposAdicionalesApiController : ApiController
    {
        [System.Web.Http.Route("Api/EquiposAdicionales/ObtenerEquiposAdicionales")]
        [System.Web.Http.ActionName("ObtenerEquiposAdicionales")]
        [System.Web.Http.HttpGet]
        public object ObtenerEquiposAdicionales(FormDataCollection form, bool? activo)
        {

            ServicioEquipoAdicionales servicio = new ServicioEquipoAdicionales();
            object lista = servicio.obtenerEquipos(activo);
            return lista;
        }

        [System.Web.Http.Route("Api/EquiposAdicionales/AltaEquiposAdicionales")]
        [System.Web.Http.ActionName("AltaEquiposAdicionales")]
        [System.Web.Http.HttpPost]
        public int AltaEquiposAdicionales(EquipoVM equipo)
        {
            int codigo;
            ServicioEquipoAdicionales servicio = new ServicioEquipoAdicionales();
            codigo = servicio.AltaEquipoEspecial(equipo);

            return codigo;
        }

        [System.Web.Http.Route("Api/EquiposAdicionales/ModificarEquiposAdicionales")]
        [System.Web.Http.ActionName("ModificarEquiposAdicionales")]
        [System.Web.Http.HttpPost]
        public int ModificarEquiposAdicionales(EquipoVM equipo)
        {
            ServicioEquipoAdicionales servicio = new ServicioEquipoAdicionales();
            int codigo;
            codigo = servicio.ModificarEquipoAdicional(equipo);

            return codigo;
        }

        [System.Web.Http.Route("Api/EquiposAdicionales/EliminarEquiposAdicionales")]
        [System.Web.Http.ActionName("EliminarEquiposAdicionales")]
        [System.Web.Http.HttpPost]
        public int EliminarEquiposAdicionales(EquipoVM equipo)
        {
            ServicioEquipoAdicionales servicio = new ServicioEquipoAdicionales();
            int codigo = servicio.EliminarEquipoAdicional(equipo);
            return codigo;
        }

        [System.Web.Http.Route("Api/EquiposAdicionales/ConsultarUnEquiposAdicionales")]
        [System.Web.Http.ActionName("ConsultarUnEquiposAdicionales")]
        [System.Web.Http.HttpGet]
        public EquipoVM ConsultarUnEquiposAdicionales(int idEquipo)
        {
            ServicioEquipoAdicionales servicio = new ServicioEquipoAdicionales();
            EquipoVM equipo = servicio.ConsultarUnEquipo(idEquipo);
            return equipo;
        }

        [System.Web.Http.Route("Api/EquiposAdicionales/RecuperarEquiposAdicionales")]
        [System.Web.Http.ActionName("RecuperarEquiposAdicionales")]
        [System.Web.Http.HttpPost]
        public int RecuperarEquiposAdicionales(EquipoVM equipo)
        {
            ServicioEquipoAdicionales servicio = new ServicioEquipoAdicionales();
            int codigo = servicio.RecuperarEquipo(equipo);
            return codigo;
        }

    }
}
