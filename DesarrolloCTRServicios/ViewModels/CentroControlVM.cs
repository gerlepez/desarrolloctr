﻿using System.Collections.Generic;

namespace DesarrolloCTRServicios.ViewModels
{
    public class CentroControlVM
    {
        public int? idCentroControl { get; set; }
        public int? idTipoCControl { get; set; }
        public int? idPropiedad { get; set; }

        public string codigosap { get; set; }
        public string nombreCentroControl { get; set; }
        public string nombreTipoCControl { get; set; }
        public string nombrePropiedad { get; set; }
        public bool? activo { get; set; }

        public List<CentralVM> listaCentrales { get; set; }
    }
}
