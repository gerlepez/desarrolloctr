﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class EstadoEstacionVM
    {
        public int idEstadoEstacion { get; set; }
        public string nombreEstadoEstacion { get; set; }
        public bool activo { get; set; }
    }
}
