﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class AlarmasTipoEquipoVM
    {
        public int idEquipoAlarma { get; set; }
        public int? idEquipo { get; set; }
        public int? idAlarma { get; set; }
        public bool activo { get; set; }
        public AlarmasVM alarmas { get; set; }
    }
}
