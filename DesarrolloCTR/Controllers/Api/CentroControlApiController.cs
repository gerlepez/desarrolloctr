﻿using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Http.Formatting;


namespace DesarrolloCTR.Controllers.Api
{
    public class CentroControlApiController : ApiController
    {
        [System.Web.Http.Route("Api/CentroControl/ObtenerCentroControl")]
        [System.Web.Http.ActionName("ObtenerCentroControl")]
        [System.Web.Http.HttpGet]
        public object ObtenerCentroControl(bool? activo)
        {
            ServicioCentroControl servicioCentroControl = new ServicioCentroControl();
            object lista = servicioCentroControl.ObtenerCentroControl(activo);
            return lista;
        }


        [System.Web.Http.Route("Api/CentroControl/TablaCentroControl")]
        [System.Web.Http.ActionName("TablaCentroControl")]
        [System.Web.Http.HttpPost]
        public object TablaCentroControl(FormDataCollection form, bool? activo)
        {
            var draw = form.Get("draw");
            var start = form.Get("start");
            var length = form.Get("length");
            var sortColumn = (form.Get("columns[" + form.Get("order[0][column]").FirstOrDefault() + "][data]").ToString()).ToString();
            var sortColumnDir = form.Get("order[0][dir]").ToString();
            var searchValue = form.Get("search[value]").ToString();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            int cantidadRegistrosPagina = Int32.Parse(length);


            ServicioCentroControl servicioCentroControl = new ServicioCentroControl();
            object lista = servicioCentroControl.TablaCentroControl(activo, draw, recordsTotal, skip, pageSize);
            return lista;
        }

        /*
        [System.Web.Http.Route("Api/CentroControl/ListaCentroControles")]
        [System.Web.Http.ActionName("ListaCentroControles")]
        [System.Web.Http.HttpPost]
        public object ListaCentroControles(bool? activo)
        {

            ServicioCentroControl servicioCentroControl = new ServicioCentroControl();
            object lista = servicioCentroControl.ListaCentroControles(activo);
            return lista;
        }
        */

        [System.Web.Http.Route("Api/CentroControl/ConsultarUnCentroControl")]
        [System.Web.Http.ActionName("ConsultarUnCentroControl")]
        [System.Web.Http.HttpGet]
        public CentroControlVM ConsultarUnCentroControl(int idCentroControl)
        {
            ServicioCentroControl servicioCentroControl = new ServicioCentroControl();
            CentroControlVM centroControl = servicioCentroControl.ConsultarUnCentroControl(idCentroControl);
            return centroControl;
        }

        [System.Web.Http.Route("Api/CentroControl/ObtenerListaCentralesPorCentro")]
        [System.Web.Http.ActionName("ObtenerListaCentralesPorCentro")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaCentralesPorCentro(int idCentroControl)
        {
            ServicioCentroControl servicioCentroControl = new ServicioCentroControl();
            object centroControl = servicioCentroControl.ObtenerListaCentralesPorCentro(idCentroControl);
            return centroControl;
        }

        [System.Web.Http.Route("Api/CentroControl/EliminarCentroControl")]
        [System.Web.Http.ActionName("EliminarCentroControl")]
        [System.Web.Http.HttpPost]
        public int EliminarCentroControl(CentroControlVM centroControl)
        {
            ServicioCentroControl servicioCentroControl = new ServicioCentroControl();
            int codigo = servicioCentroControl.EliminarCentroControl(centroControl);
            return codigo;
        }

        [System.Web.Http.Route("Api/CentroControl/RecuperarCentroControl")]
        [System.Web.Http.ActionName("RecuperarCentroControl")]
        [System.Web.Http.HttpPost]
        public int RecuperarCentroControl(CentroControlVM centroControl)
        {
            ServicioCentroControl servicioCentroControl = new ServicioCentroControl();
            int codigo = servicioCentroControl.RecuperarCentroControl(centroControl);
            return codigo;

        }


        [System.Web.Http.Route("Api/CentroControl/AltaCentroControl")]
        [System.Web.Http.ActionName("AltaCentroControl")]
        [System.Web.Http.HttpPost]
        public int AltaCentroControl(CentroControlVM centroControl)
        {
            ServicioCentroControl servicioCentroControl = new ServicioCentroControl();
            int codigo;
            if (servicioCentroControl.ValidarCentroControl(centroControl))
            {
                codigo = servicioCentroControl.AltaCentroControl(centroControl);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }

        [System.Web.Http.Route("Api/CentroControl/ModificarCentroControl")]
        [System.Web.Http.ActionName("ModificarCentroControl")]
        [System.Web.Http.HttpPost]
        public int ModificarCentroControl(CentroControlVM centroControl)
        {

            ServicioCentroControl servicioCentroControl = new ServicioCentroControl();
            int codigo;
            if (servicioCentroControl.ValidarCentroControl(centroControl))
            {
                codigo = servicioCentroControl.ModificarCentroControl(centroControl);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }

        [System.Web.Http.Route("Api/CentroControl/ObtenerTipoCentroControl")]
        [System.Web.Http.ActionName("ObtenerTipoCentroControl")]
        [System.Web.Http.HttpGet]
        public object ObtenerTipoCentroControl()
        {
            ServicioCentroControl servicioCentroControl = new ServicioCentroControl();
            object listTipoCC = servicioCentroControl.ObtenerTipoCentroControl();
            return listTipoCC;
        }


    }
}
