﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class TipoActuacionVM
    {
        public int idTipoActuacion { get; set; }
        public string nombreTipoActuacion { get; set; }
        public bool? activo { get; set; }
        public string nombresuario { get; set; }
    }
}
