﻿using DesarrolloCTR.Controllers.Extra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers
{
    [SessionUtilityAttribute]
    [Permiso("PCON")]
    public class PuntoConexionController : Controller
    {

        public ActionResult PuntoConexion()
        {
            return View();
        }

        public ActionResult AdministrarPuntoConexion()
        {
            return View();
        }
    }
}