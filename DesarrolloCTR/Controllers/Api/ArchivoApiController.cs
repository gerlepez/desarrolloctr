﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers.Api
{
    public class ArchivoApiController : ApiController
    {
        private readonly Random _random = new Random();
        // GET: ArchivoApi
        [System.Web.Http.Route("Api/Archivo/Upload")]
        [System.Web.Http.ActionName("Upload")]
        [System.Web.Http.HttpPost]
        public List<ArchivoVM> Upload(string ruta)
        {
            List<ArchivoVM> listArchivo = new List<ArchivoVM>();
            try
            {
                var fileuploadPath = ConfigurationManager.AppSettings["RepoArchivos"];
                var local = ConfigurationManager.AppSettings["IsLocal"];
                HttpResponseMessage result = null;
                var httpRequest = System.Web.HttpContext.Current.Request;
                               
                var httpContext = HttpContext.Current;

                // Check for any uploaded file  
                if (httpContext.Request.Files.Count > 0)
                {
                    
                    var pathInic = "";
                    if (local == "false")
                    {
                        pathInic = fileuploadPath + ruta;
                    }
                    else
                    {
                        if (!Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~/TempFolder/")))
                            Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~/TempFolder/"));
                        pathInic = "~/TempFolder/";

                    }
                    //Loop through uploaded files  
                    for (int i = 0; i < httpContext.Request.Files.Count; i++)
                    {
                       
                        HttpPostedFile httpPostedFile = httpContext.Request.Files[i];
                        if (httpPostedFile != null)
                        {
                            var fileName = Path.GetFileName(httpPostedFile.FileName);
                           
                            var splitParam = httpRequest.Params.AllKeys[1].Split(',')[1];
                            var id = Int32.Parse(splitParam);
                            int numRandom = _random.Next();
                            var fileNameToSave = id + "_" + numRandom + "_" + fileName;
                            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath(pathInic), fileNameToSave);

                            ArchivoVM archivo = new ArchivoVM
                            {
                                idEntidad = id,
                                nombreRandom = fileNameToSave,
                                nombreArchivo = httpPostedFile.FileName,
                                urlArchivo = path
                            };

                            listArchivo.Add(archivo);
                            httpPostedFile.SaveAs(path);
                        }
                    }
                }
                               

                return listArchivo;
            }
            catch (Exception e)
            {
                listArchivo[0].error = 400;
                return listArchivo;
            }
        }

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Api/Archivo/GetFile")]
        [System.Web.Http.ActionName("GetFile")]
        public HttpResponseMessage GetFile(string filePath, string ruta, string fileName)
        {
            var local = ConfigurationManager.AppSettings["IsLocal"];
            var fileuploadPath = ConfigurationManager.AppSettings["RepoArchivos"];
            Request = new System.Net.Http.HttpRequestMessage();
            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.OK);
          
            //Check whether File exists.
            if (!File.Exists(filePath))
            {
                //Throw 404 (Not Found) exception if File not found.
                response.StatusCode = HttpStatusCode.NotFound;
                response.ReasonPhrase = string.Format("File not found: {0} .", filePath);
                throw new HttpResponseException(response);
            }

            //Read the File into a Byte Array.
            byte[] bytes = File.ReadAllBytes(filePath);

            //Set the Response Content.
            response.Content = new ByteArrayContent(bytes);

            //Set the Response Content Length.
            response.Content.Headers.ContentLength = bytes.LongLength;

            //Set the Content Disposition Header Value and FileName.
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            response.Content.Headers.ContentDisposition.FileName = fileName;


            //Set the File Content Type.
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(MimeMapping.GetMimeMapping(fileName));
            return response;
        }

    }
}