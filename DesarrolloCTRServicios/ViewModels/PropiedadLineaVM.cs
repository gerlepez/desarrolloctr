﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class PropiedadLineaVM
    {
        public int? idPropiedad { get; set; }
        public string nombrePropiedad { get; set; }
        public bool? activo { get; set; }
    }
}
