﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{

    public class ServicioGenerador
    {

        public object ObtenerGeneradores(bool? activo)
        {
            List<GeneradorVM> listaGeneradores;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                ServicioUsuario usuarioservice = new ServicioUsuario();
                listaGeneradores = (from g in db.GenGenerador // g = Base de datos de Generador
                                    join c in db.CConCentral on g.idCentral equals c.idCentral // la c hace refencia a central , en la base de datos CConCentral , que esta en g.idCentral (hace referencia a la id de central dentro de generadores) es igual al mismo id
                                    join cc in db.CConCentroControl on c.idCentroControl equals cc.idCentroControl
                                    join p in db.ProPropiedad on g.idPropiedad equals p.idPropiedad
                                    where g.activo == activo && g.fechaEdicion == null
                                    select new { g, c, cc, p }).AsEnumerable().Select(x => new GeneradorVM
                                    {
                                        idGenerador = x.g.idGenerador,
                                        nombreGenerador = x.g.nombreGenerador,
                                        esAutomatismo = x.g.tieneAutomatismo,
                                        activo = x.g.activo,

                                        central = new CentralVM
                                        {
                                            idCentral = x.c.idCentral,
                                            nombreCentral = x.c.nombreCentral,
                                            activo = x.c.activo
                                        },
                                        centrocontrol = new CentroControlVM
                                        {
                                            idCentroControl = x.cc.idCentroControl,
                                            nombreCentroControl = x.cc.nombreCentroControl,
                                            activo = x.cc.activo
                                        },
                                        propiedad = new PropiedadVM
                                        {
                                            idPropiedad = x.p.idPropiedad,
                                            nombrePropiedad = x.p.nombrePropiedad,
                                            activo = x.p.activa
                                        },
                                        usuario = usuarioservice.ObtenerDatosUsuarioPorId(x.g.idUsuarioAlta)
                                    }).ToList();

            }
            object json = new { data = listaGeneradores };
            return json;

        }

        public object TablaGeneradores(bool? activo, string draw, int recordsTotal, int skip, int pageSize)
        {
            try
            {

                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    ServicioUsuario usuarioservice = new ServicioUsuario();
                    IQueryable<GeneradorVM> query = (from g in db.GenGenerador // g = Base de datos de Generador
                                                     join c in db.CConCentral on g.idCentral equals c.idCentral // la c hace refencia a central , en la base de datos CConCentral , que esta en g.idCentral (hace referencia a la id de central dentro de generadores) es igual al mismo id
                                                     join cc in db.CConCentroControl on c.idCentroControl equals cc.idCentroControl
                                                     join p in db.ProPropiedad on g.idPropiedad equals p.idPropiedad
                                                     where g.activo == activo
                                                     select new { g, c, cc, p }).AsEnumerable().Select(x => new GeneradorVM
                                                     {
                                                         idGenerador = x.g.idGenerador,
                                                         nombreGenerador = x.g.nombreGenerador,
                                                         esAutomatismo = x.g.tieneAutomatismo,
                                                         activo = x.g.activo,

                                                         central = new CentralVM
                                                         {
                                                             idCentral = x.c.idCentral,
                                                             nombreCentral = x.c.nombreCentral,
                                                             activo = x.c.activo
                                                         },
                                                         centrocontrol = new CentroControlVM
                                                         {
                                                             idCentroControl = x.cc.idCentroControl,
                                                             nombreCentroControl = x.cc.nombreCentroControl,
                                                             activo = x.cc.activo
                                                         },
                                                         propiedad = new PropiedadVM
                                                         {
                                                             idPropiedad = x.p.idPropiedad,
                                                             nombrePropiedad = x.p.nombrePropiedad,
                                                             activo = x.p.activa
                                                         },
                                                         usuario = usuarioservice.ObtenerDatosUsuarioPorId(x.g.idUsuarioAlta)
                                                     }).OrderByDescending(x => x.idGenerador).AsQueryable();

                    var queryFiltrada = query;
                    recordsTotal = queryFiltrada.Count();
                    var lst = queryFiltrada.Skip(skip).Take(pageSize).ToList();

                    object json = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = lst };
                    return json;
                }
            }
            catch (Exception e)
            {
                return 400;
            }
        }


        public object ObtenerGeneradoresPorCentral(int idCentral, int idNovedad = 0)
        {
            List<GeneradorVM> listaGeneradores = new List<GeneradorVM>();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {

                if (idNovedad == 0)
                {
                    var central = db.CConCentral.Find(idCentral);
                    if (central.idTipoCentral != 3)
                    {
                        listaGeneradores = (from g in db.GenGenerador
                                            where g.activo == true && g.idCentral == idCentral && g.fechaEdicion == null
                                            select new GeneradorVM
                                            {
                                                activo = g.activo,
                                                idEquipo = g.idGenerador,
                                                nombreEquipo = g.nombreGenerador,
                                                nombreGenerador = g.nombreGenerador,
                                                idGenerador = g.idGenerador,
                                                idtipoEquipo = g.idTipoEquipo,
                                                idGeneral = g.idGenerador,
                                                codigoEquipo = "GEN",
                                                idPropiedad = g.idPropiedad,
                                            }).ToList();
                    }
                    else
                    {
                        //si es parque solar, no tiene generadores por el momento
                        GeneradorVM parqueSolar = new GeneradorVM();
                        parqueSolar.idGenerador = -1;
                        parqueSolar.codigoEquipo = "PSFV";
                        parqueSolar.idtipoEquipo = 5;
                        listaGeneradores.Add(parqueSolar);
                    }
                }
                else
                {
                    var central = db.CConCentral.Find(idCentral);
                    if (central.idTipoCentral != 3)
                    {
                        listaGeneradores = (from g in db.GenGenerador
                                            join n in db.NovNovedad on idNovedad equals n.idNovedad
                                            where g.idCentral == idCentral && g.fechaCreacion < n.horaNovedad && (g.fechaEdicion == null || g.fechaEdicion > n.horaNovedad)
                                            select new GeneradorVM
                                            {
                                                activo = g.activo,
                                                idEquipo = g.idGenerador,
                                                nombreEquipo = g.nombreGenerador,
                                                nombreGenerador = g.nombreGenerador,
                                                idGenerador = g.idGenerador,
                                                idtipoEquipo = g.idTipoEquipo,
                                                idGeneral = g.idGenerador,
                                                codigoEquipo = "GEN",
                                                idPropiedad = g.idPropiedad,
                                            }).ToList();
                    }
                    else
                    {
                        //si es parque solar, no tiene generadores por el momento
                        GeneradorVM parqueSolar = new GeneradorVM();
                        parqueSolar.idGenerador = -1;
                        parqueSolar.codigoEquipo = "PSFV";
                        parqueSolar.idtipoEquipo = 5;
                        listaGeneradores.Add(parqueSolar);
                    }
                }

            }
            object json = new { data = listaGeneradores };
            return json;

        }


        public object ObtenerGeneradoresPorCentralNovedad(int idCentral)
        {
            List<GeneradorVM> listaGeneradores = new List<GeneradorVM>();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {

                var central = db.CConCentral.Find(idCentral);
                if (central.idTipoCentral != 3)
                {
                    listaGeneradores = (from g in db.GenGenerador
                                        where g.activo == true && g.idCentral == idCentral // && g.fechaEdicion == null ()
                                        select new GeneradorVM
                                        {
                                            activo = g.activo,
                                            idEquipo = g.idGenerador,
                                            nombreEquipo = g.nombreGenerador,
                                            nombreGenerador = g.nombreGenerador,
                                            idGenerador = g.idGenerador,
                                            idtipoEquipo = g.idTipoEquipo,
                                            idGeneral = g.idGenerador,
                                            codigoEquipo = "GEN",
                                            idPropiedad = g.idPropiedad,
                                        }).ToList();
                }
                else
                {
                    //si es parque solar, no tiene generadores por el momento
                    GeneradorVM parqueSolar = new GeneradorVM();
                    parqueSolar.idGenerador = -1;
                    parqueSolar.codigoEquipo = "PSFV";
                    parqueSolar.idtipoEquipo = 5;
                    listaGeneradores.Add(parqueSolar);
                }
            }
            object json = new { data = listaGeneradores };
            return json;

        }





        public object ObtenerGeneradoresPorUsuario(int idUsuarioAlta)
        {
            List<UsuarioVM> listausuarios = new List<UsuarioVM>();

            using (BuhoGestionEntities db = new BuhoGestionEntities())
            {
                var usuario = db.GralUsuario.Find(idUsuarioAlta);
                if (usuario.idUsuario != 0)
                {
                    listausuarios = (from u in db.GralUsuario // g = Base de datos de Generador

                                     select new UsuarioVM
                                     {
                                         idUsuario = u.idUsuario,
                                         nombreUsuario = u.nombreUsuario

                                     }).ToList();

                }
                else
                {

                }
            }
            object json = new { data = listausuarios };
            return json;
        }

        public int AltaGenerador(GeneradorVM generador, int? idUsuario)
        {

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnGenerador = db.GenGenerador.Where(x => x.idGenerador == generador.idGenerador).FirstOrDefault();
                var generadorAnterior = db.GenGenerador.Where(x => x.nombreGenerador == generador.nombreGenerador && x.idCentral == generador.central.idCentral && x.activo == true).FirstOrDefault();
                if (consultaUnGenerador == null && generadorAnterior == null)
                {

                    try
                    {
                        GenGenerador g = new GenGenerador
                        {
                            // idGenerador = generador.idGenerador,
                            nombreGenerador = generador.nombreGenerador,
                            idCentral = generador.central.idCentral,
                            idUsuarioAlta = idUsuario,
                            idPropiedad = generador.propiedad.idPropiedad,
                            activo = true,
                            fechaCreacion = DateTime.Now,
                        };
                        db.GenGenerador.Add(g);
                        db.SaveChanges();
                        return 200;
                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }

            }
        }

        public int ModificarGenerador(GeneradorVM generador, int? idUsuario)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnGenerador = db.GenGenerador.Where(x => x.idGenerador == generador.idGenerador).FirstOrDefault();
                if (consultaUnGenerador != null)
                {
                    //consultaUnGenerador.nombreGenerador = generador.nombreGenerador;
                    //consultaUnGenerador.idCentral = generador.central.idCentral;
                    //consultaUnGenerador.idUsuarioAlta = idUsuario;
                    //consultaUnGenerador.idPropiedad = generador.propiedad.idPropiedad;
                    //consultaUnGenerador.activo = true;

                    consultaUnGenerador.activo = false;
                    consultaUnGenerador.fechaEdicion = DateTime.Now;

                    GenGenerador g = new GenGenerador
                    {
                        // idGenerador = generador.idGenerador,
                        nombreGenerador = generador.nombreGenerador,
                        idCentral = generador.central.idCentral,
                        idUsuarioAlta = idUsuario,
                        idPropiedad = generador.propiedad.idPropiedad,
                        activo = true,
                        fechaCreacion = DateTime.Now,
                    };
                    db.GenGenerador.Add(g);
                    db.SaveChanges();

                    //var ultimoGen = db.GenGenerador.OrderByDescending(x => x.idGenerador).FirstOrDefault();//Busco el generador que se acaba de crear
                    var UltimoGen = g.idGenerador;
                    var AutoRelacion = db.AutRelacionAutomatismo.Where(x => x.idEquipo == consultaUnGenerador.idGenerador && x.idTipoEquipo == 5 && x.idLugar == consultaUnGenerador.idCentral).ToList();//Busco los generadores que tenian el idGenerador viejo en la Relaciones con Automatismo
                    foreach (var GeneradorRel in AutoRelacion)
                    {
                        if (GeneradorRel.activo != false)
                        {
                            AutRelacionAutomatismo a = new AutRelacionAutomatismo
                            {
                                idAutomatismo = GeneradorRel.idAutomatismo,
                                idTipoEquipo = GeneradorRel.idTipoEquipo,
                                idLugar = GeneradorRel.idLugar,
                                idEquipo = UltimoGen,
                                activo = true,
                            };
                            db.AutRelacionAutomatismo.Add(a);
                            GeneradorRel.activo = false;
                            db.SaveChanges();

                        }
                    }
                }
            }
            return 200;
        }

        public int EliminarGenerador(GeneradorVM generador)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnGenerador = db.GenGenerador.Where(x => x.idGenerador == generador.idGenerador).FirstOrDefault();
                if (consultaUnGenerador != null)
                {
                    consultaUnGenerador.activo = false;
                }
                db.SaveChanges();
            }
            return 200;
        }


        public GeneradorVM ConsultarUnGenerador(int? idGenerador)
        {
            GeneradorVM generador = new GeneradorVM();

            using (IntranetCTREntities db = new IntranetCTREntities())
            {

                generador = (from g in db.GenGenerador
                             join c in db.CConCentral on g.idCentral equals c.idCentral
                             join cc in db.CConCentroControl on c.idCentroControl equals cc.idCentroControl
                             join p in db.ProPropiedad on g.idPropiedad equals p.idPropiedad
                             where g.idGenerador == idGenerador
                             select new GeneradorVM()
                             {
                                 idGenerador = g.idGenerador,
                                 nombreGenerador = g.nombreGenerador,
                                 nombreEquipo = "Generador",
                                 activo = g.activo,
                                 idCentral = c.idCentral,
                                 idCentroControl = cc.idCentroControl,
                                 central = new CentralVM
                                 {
                                     idCentral = c.idCentral,
                                     nombreCentral = c.nombreCentral,
                                     activo = c.activo
                                 },
                                 centrocontrol = new CentroControlVM
                                 {
                                     idCentroControl = cc.idCentroControl,
                                     nombreCentroControl = cc.nombreCentroControl,
                                     activo = cc.activo,
                                 },
                                 propiedad = new PropiedadVM
                                 {
                                     idPropiedad = p.idPropiedad,
                                     nombrePropiedad = p.nombrePropiedad,
                                     activo = p.activa
                                 },
                             }).FirstOrDefault();
            }
            return generador;
        }

        public object ObtenerObjetoGeneradorPorCentral(int idCentral)
        {
            List<GeneradorVM> listagenerador = new List<GeneradorVM>();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {

                listagenerador = (from g in db.GenGenerador
                                  where g.activo == true && g.idCentral == idCentral
                                  select new GeneradorVM
                                  {
                                      activo = g.activo,
                                      nombreGenerador = g.nombreGenerador,
                                      idGenerador = g.idGenerador,
                                      idtipoEquipo = g.idTipoEquipo,
                                      idGeneral = g.idGenerador,
                                      descripcion = g.nombreGenerador,
                                      esAutomatismo = g.tieneAutomatismo == null ? false : g.tieneAutomatismo,

                                  }).ToList();
            }
            object json = new { data = listagenerador };
            return json;
        }

        public int RecuperarGenerador(GeneradorVM generador)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())

            {
                var consultaUnGenerador = db.GenGenerador.Where(x => x.idGenerador == generador.idGenerador).FirstOrDefault();
                var mismoGen = db.GenGenerador.Where(x => x.nombreGenerador == generador.nombreGenerador && x.idCentral == generador.central.idCentral && x.activo == true).FirstOrDefault();
                if (consultaUnGenerador != null)
                {
                    if (mismoGen == null)
                    {
                        consultaUnGenerador.activo = true;
                        db.SaveChanges();
                        return 200;
                    }
                    else
                    {
                        return 300;
                    }
                }
                else
                {
                    return 400;
                }

            }

        }


        public bool ValidarGenerador(GeneradorVM generador)
        {
            if (!string.IsNullOrWhiteSpace(generador.nombreGenerador.ToString()))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

    }
}
