﻿using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using DesarrolloCTRUtilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers.Api
{
    public class IndisponibilidadRemuApiController : ApiController
    {
        public class RemuneracionIndisController : ApiController
        {
            [System.Web.Http.Route("Api/Remuneracion/ObtenerEquiposRemunerados")]
            [System.Web.Http.ActionName("ObtenerEquiposRemunerados")]
            [System.Web.Http.HttpGet]
            public object ObtenerEquiposRemunerados(FormDataCollection form, bool? activo)
            {
                ServicioIndisponibilidadesRemu servicioRemu = new ServicioIndisponibilidadesRemu();
                object lista = servicioRemu.ObtenerEquipos(activo);
                return lista;

            }

            #region Region Lineas

            [System.Web.Http.Route("Api/Remuneracion/obtenerLineas")]
            [System.Web.Http.ActionName("obtenerLineas")]
            [System.Web.Http.HttpGet]
            public object obtenerLineas(int idResolucion, string fecha)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                object listaLinea = servIndisponibilidadRemu.obtenerLineas(idResolucion, fecha);
                return listaLinea;
            }

            [System.Web.Http.Route("Api/Remuneracion/ConsultarUnaLineaRemunerada")]
            [System.Web.Http.ActionName("ConsultarUnaLineaRemunerada")]
            [System.Web.Http.HttpGet]
            public IndisRemuEquipoVM ConsultarUnaLinea(int idLineaIndisRemu)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                IndisRemuEquipoVM linea = servIndisponibilidadRemu.ConsultarLineaRemunerada(idLineaIndisRemu);

                return linea;
            }

            [System.Web.Http.Route("Api/Remuneracion/ModificarLinea")]
            [System.Web.Http.ActionName("ModificarLinea")]
            [System.Web.Http.HttpPost]
            public int ModificarLinea(IndisRemuEquipoVM linea)
            {
                var idUsuarioLogueado = 0;

                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;
                codigo = servIndisponibilidadRemu.ModificarLinea(linea);

                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/ObtenerMontosLineas")]
            [System.Web.Http.ActionName("ObtenerMontosLineas")]
            [System.Web.Http.HttpGet]
            public object ObtenerMontosLineas()
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                object listaLinea = servIndisponibilidadRemu.ObtenerMontosLineas();
                return listaLinea;
            }
            [System.Web.Http.Route("Api/Remuneracion/ObtenerMontosLineasTodos")]
            [System.Web.Http.ActionName("ObtenerMontosLineas")]
            [System.Web.Http.HttpGet]
            public object ObtenerMontosLineasTodos()
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                object listaLinea = servIndisponibilidadRemu.ObtenerMontosLineasTodos();
                return listaLinea;
            }
            [System.Web.Http.Route("Api/Remuneracion/ObtenerMontosTrafoTodos")]
            [System.Web.Http.ActionName("ObtenerMontosTrafoTodos")]
            [System.Web.Http.HttpGet]
            public object ObtenerMontosTrafoTodos()
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                object listaLinea = servIndisponibilidadRemu.ObtenerMontosTrafoTodos();
                return listaLinea;
            }
            [System.Web.Http.Route("Api/Remuneracion/obtenerMontosPtosConexionTodos")]
            [System.Web.Http.ActionName("obtenerMontosPtosConexionTodos")]
            [System.Web.Http.HttpGet]
            public object obtenerMontosPtosConexionTodos()
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                object listaLinea = servIndisponibilidadRemu.obtenerMontosPtosConexionTodos();
                return listaLinea;
            }
            [System.Web.Http.Route("Api/Remuneracion/obtenerMontosCapacitoresTodos")]
            [System.Web.Http.ActionName("obtenerMontosCapacitoresTodos")]
            [System.Web.Http.HttpGet]
            public object obtenerMontosCapacitoresTodos()
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                object listaLinea = servIndisponibilidadRemu.obtenerMontosCapacitoresTodos();
                return listaLinea;
            }
            [System.Web.Http.Route("Api/Remuneracion/ObtenerTensionLinea")]
            [System.Web.Http.ActionName("ObtenerTensionLinea")]
            [System.Web.Http.HttpGet]
            public object ObtenerTensionLinea()
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                var tensionServicio = servIndisponibilidadRemu.ObtenerTensionLinea();
                return tensionServicio;
            }

            [System.Web.Http.Route("Api/Remuneracion/AltaMontoLinea")]
            [System.Web.Http.ActionName("AltaMonto")]
            [System.Web.Http.HttpPost]
            public int AltaMontoLinea(MontoIndisponibilidadesVM monto)
            {

                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;

                codigo = servIndisponibilidadRemu.AltaMontoLinea(monto);

                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/ModificarMontoLinea")]
            [System.Web.Http.ActionName("ModificarMonto")]
            [System.Web.Http.HttpPost]
            public int ModificarMontoLinea(MontoIndisponibilidadesVM monto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;

                codigo = servIndisponibilidadRemu.ModificarMontoLinea(monto);

                //((IDisposable)servLinea).Dispose();

                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/EliminarMontoLinea")]
            [System.Web.Http.ActionName("EliminarMonto")]
            [System.Web.Http.HttpPut]
            public int EliminarMontoLinea(MontoIndisponibilidadesVM monto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo = servIndisponibilidadRemu.EliminarMontoLinea(monto);
                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/ObtenerHistorialMontoLineas")]
            [System.Web.Http.ActionName("ObtenerHistorialMontoLineas")]
            [System.Web.Http.HttpGet]
            public object ObtenerHistorialMontoLineas()
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                object historialMontoLinea = servIndisponibilidadRemu.ObtenerHistorialLinea();
                return historialMontoLinea;
            }

            [System.Web.Http.Route("Api/Remuneracion/EliminarHistorialLinea")]
            [System.Web.Http.ActionName("EliminarHistorialLinea")]
            [System.Web.Http.HttpPut]
            public int EliminarHistorialLinea(MontoIndisponibilidadesVM monto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo = servIndisponibilidadRemu.EliminarHistorialLinea(monto);
                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/ModificarHistorialMontoLinea")]
            [System.Web.Http.ActionName("ModificarHistorialMontoLinea")]
            [System.Web.Http.HttpPost]
            public int ModificarHistorialMontoLinea(MontoIndisponibilidadesVM monto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;

                codigo = servIndisponibilidadRemu.ModificarMontoLinea(monto);

                //((IDisposable)servLinea).Dispose();

                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/obtenerLineasIndisponibles")]
            [System.Web.Http.ActionName("obtenerLineasIndisponibles")]
            [System.Web.Http.HttpGet]
            public object obtenerLineasIndisponibles(DateTime fechaDesde, DateTime fechaHasta)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                object listaLinea = servIndisponibilidadRemu.obtenerLineasIndisponibles(fechaDesde, fechaHasta);
                return listaLinea;
            }

            [System.Web.Http.Route("Api/Remuneracion/AltaLineaIndisponibles")]
            [System.Web.Http.ActionName("AltaLineaIndisponibles")]
            [System.Web.Http.HttpPost]
            public int AltaLineaIndisponibles(IndisRemuEquipoVM linea, DateTime fechaDesdeMonto, DateTime? fechaHastaMonto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                var codigo = servIndisponibilidadRemu.AltaLineaIndisponibles(linea, fechaDesdeMonto, fechaHastaMonto);
                return codigo;
            }


            [System.Web.Http.Route("Api/Remuneracion/ModificarLineaIndisponibles")]
            [System.Web.Http.ActionName("ModificarLineaIndisponibles")]
            [System.Web.Http.HttpPost]
            public int ModificarLineaIndisponibles(IndisRemuEquipoVM linea, DateTime fechaDesdeMonto, DateTime? fechaHastaMonto)
            {
                var idUsuarioLogueado = 0;

                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;
                codigo = servIndisponibilidadRemu.ModificarLineaIndisponibles(linea, fechaDesdeMonto, fechaHastaMonto);

                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/EliminarLineaIndisponible")]
            [System.Web.Http.ActionName("EliminarLineaIndisponible")]
            [System.Web.Http.HttpPut]
            public int EliminarLineaIndisponible(IndisRemuEquipoVM linea)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo = servIndisponibilidadRemu.EliminarLineaIndisponible(linea);
                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/ObtenerFactorPenalizacion")]
            [System.Web.Http.ActionName("ObtenerFactorPenalizacion")]
            [System.Web.Http.HttpGet]
            public IndisRemuEquipoVM ObtenerFactorPenalizacion()
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                IndisRemuEquipoVM factor = servIndisponibilidadRemu.obtenerFactorPenalizacion();

                return factor;
            }


            [System.Web.Http.Route("Api/Remuneracion/CambiarFactorK")]
            [System.Web.Http.ActionName("ModificarFactorPenalizacion")]
            [System.Web.Http.HttpPost]
            public int ModificarFactorPenalizacion(int FactorK)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;

                codigo = servIndisponibilidadRemu.modificarFactorPenalizacion(FactorK);

                //((IDisposable)servLinea).Dispose();

                return codigo;
            }


            [System.Web.Http.Route("Api/Remuneracion/ConsultarMonto")]
            [System.Web.Http.ActionName("ConsultarMonto")]
            [System.Web.Http.HttpGet]
            public MontoIndisponibilidadesVM ConsultarMonto(int idlinea, DateTime fechaDesdeMonto, DateTime? fechaHastaMonto, int? idMonto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                MontoIndisponibilidadesVM monto = servIndisponibilidadRemu.ConsultarMonto(idlinea, fechaDesdeMonto, fechaHastaMonto, idMonto);

                return monto;
            }


            //Generar reporte excel
            //[System.Web.Http.Route("Api/Remuneracion/GenerarReporteExcelLinea")]
            //[System.Web.Http.ActionName("GenerarReporteExcelLinea")]
            //[System.Web.Http.HttpGet]
            //public HojaExcelVM GenerarReporteExcelLinea(string fechaDesde, string fechaHasta)
            //{

            //    ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
            //    var result = servIndisponibilidadRemu.GenerarReporteExcelLinea(fechaDesde, fechaHasta);
            //    return result;

            //}



            #endregion

            #region Transformadores

            [System.Web.Http.Route("Api/Remuneracion/obtenerTransformadores")]
            [System.Web.Http.ActionName("obtenerTransformadores")]
            [System.Web.Http.HttpGet]
            public object obtenerTransformadores(int idResolucion, string fecha)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                object listaTrafo = servIndisponibilidadRemu.obtenerTransformadores(idResolucion, fecha);
                return listaTrafo;
            }

            [System.Web.Http.Route("Api/Remuneracion/ConsultarUnTrafo")]
            [System.Web.Http.ActionName("ConsultarUnTrafo")]
            [System.Web.Http.HttpGet]
            public IndisRemuEquipoVM ConsultarUnTrafo(int idIndisponibilidadesTrafo)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidad = new ServicioIndisponibilidadesRemu();
                IndisRemuEquipoVM trafo = servIndisponibilidad.ConsultarTrafoRemunerada(idIndisponibilidadesTrafo);

                return trafo;
            }

            // Remuneracion Trafos 
            [System.Web.Http.Route("Api/Remuneracion/ModificarTrafo")]
            [System.Web.Http.ActionName("ModificarTrafo")]
            [System.Web.Http.HttpPost]
            public int ModificarTrafo(IndisRemuEquipoVM trafo)
            {
                var idUsuarioLogueado = 0;

                ServicioIndisponibilidadesRemu servIndisponibilidad = new ServicioIndisponibilidadesRemu();
                int codigo;
                codigo = servIndisponibilidad.ModificarTrafo(trafo);

                return codigo;
            }

            // Montos Trafos //
            [System.Web.Http.Route("Api/Remuneracion/ObtenerMontosTrafo")]
            [System.Web.Http.ActionName("ObtenerMontosTrafo")]
            [System.Web.Http.HttpGet]
            public object ObtenerMontosTrafo()
            {
                ServicioIndisponibilidadesRemu servIndisponibilidad = new ServicioIndisponibilidadesRemu();
                object listatrafo = servIndisponibilidad.ObtenerMontosTrafo();
                return listatrafo;
            }

            [System.Web.Http.Route("Api/Remuneracion/AltaMontoTrafo")]
            [System.Web.Http.ActionName("AltaMontoTrafo")]
            [System.Web.Http.HttpPost]
            public int AltaMontoTrafo(MontoIndisponibilidadesVM monto)
            {

                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;

                codigo = servIndisponibilidadRemu.AltaMontoTrafo(monto);

                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/ModificarMontoTrafo")]
            [System.Web.Http.ActionName("ModificarMontoTrafo")]
            [System.Web.Http.HttpPost]
            public int ModificarMontoTrafo(MontoIndisponibilidadesVM monto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;

                codigo = servIndisponibilidadRemu.ModificarMontoTrafo(monto);

                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/EliminarMontoTrafo")]
            [System.Web.Http.ActionName("EliminarMontoTrafo")]
            [System.Web.Http.HttpPut]
            public int EliminarMontoTrafo(MontoIndisponibilidadesVM monto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo = servIndisponibilidadRemu.EliminarMontoTrafo(monto);
                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/ConsultarMontoTrafo")]
            [System.Web.Http.ActionName("ConsultarMontoTrafo")]
            [System.Web.Http.HttpGet]
            public MontoIndisponibilidadesVM ConsultarMontoTrafo(DateTime fechaDesdeMonto, DateTime? fechaHastaMonto, int? idMonto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                MontoIndisponibilidadesVM monto = servIndisponibilidadRemu.ConsultarMontoTrafo(fechaDesdeMonto, fechaHastaMonto, idMonto);

                return monto;
            }

            //Historial Monto Trafos //

            [System.Web.Http.Route("Api/Remuneracion/ObtenerHistorialMontoTrafo")]
            [System.Web.Http.ActionName("ObtenerHistorialMontoTrafo")]
            [System.Web.Http.HttpGet]
            public object ObtenerHistorialMontoTrafo()
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                object historialMontoTrafo = servIndisponibilidadRemu.ObtenerHistorialTrafo();
                return historialMontoTrafo;
            }

            [System.Web.Http.Route("Api/Remuneracion/EliminarHistorialTrafo")]
            [System.Web.Http.ActionName("EliminarHistorialTrafo")]
            [System.Web.Http.HttpPut]
            public int EliminarHistorialTrafo(MontoIndisponibilidadesVM monto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo = servIndisponibilidadRemu.EliminarHistorialTrafo(monto);
                return codigo;
            }
            [System.Web.Http.Route("Api/Remuneracion/EliminarHistorialPto")]
            [System.Web.Http.ActionName("EliminarHistorialPto")]
            [System.Web.Http.HttpPut]
            public int EliminarHistorialPto(MontoIndisponibilidadesVM monto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo = servIndisponibilidadRemu.EliminarHistorialPto(monto);
                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/EliminarHistorialCapacitor")]
            [System.Web.Http.ActionName("EliminarHistorialCapacitor")]
            [System.Web.Http.HttpPut]
            public int EliminarHistorialCapacitor(MontoIndisponibilidadesVM monto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo = servIndisponibilidadRemu.EliminarHistorialCapacitor(monto);
                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/ModificarHistorialMontoTrafo")]
            [System.Web.Http.ActionName("ModificarHistorialMontoTrafo")]
            [System.Web.Http.HttpPost]
            public int ModificarHistorialMontoTrafo(MontoIndisponibilidadesVM monto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;

                codigo = servIndisponibilidadRemu.ModificarMontoTrafo(monto);

                return codigo;
            }

            // Trafos Indisponibles //

            [System.Web.Http.Route("Api/Remuneracion/obtenerTrafosIndisponibles")]
            [System.Web.Http.ActionName("obtenerTrafosIndisponibles")]
            [System.Web.Http.HttpGet]
            public object obtenerTrafosIndisponibles(DateTime fechaDesde, DateTime fechaHasta)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                object listaTrafo = servIndisponibilidadRemu.obtenerTrafosIndisponibles(fechaDesde, fechaHasta);
                return listaTrafo;
            }

            [System.Web.Http.Route("Api/Remuneracion/AltaTrafosIndisponibles")]
            [System.Web.Http.ActionName("AltaTrafosIndisponibles")]
            [System.Web.Http.HttpPost]
            public int AltaTrafosIndisponibles(IndisRemuEquipoVM trafo, DateTime fechaDesdeMonto, DateTime? fechaHastaMonto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                var codigo = servIndisponibilidadRemu.AltaTrafosIndisponibles(trafo, fechaDesdeMonto, fechaHastaMonto);
                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/ModificarTrafoIndisponibles")]
            [System.Web.Http.ActionName("ModificarTrafoIndisponibles")]
            [System.Web.Http.HttpPost]
            public int ModificarTrafoIndisponibles(IndisRemuEquipoVM trafo, DateTime fechaDesdeMonto, DateTime? fechaHastaMonto)
            {
                var idUsuarioLogueado = 0;

                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;
                codigo = servIndisponibilidadRemu.ModificarTrafoIndisponibles(trafo, fechaDesdeMonto, fechaHastaMonto);

                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/EliminarTrafoIndisponible")]
            [System.Web.Http.ActionName("EliminarTrafoIndisponible")]
            [System.Web.Http.HttpPut]
            public int EliminarTrafoIndisponible(IndisRemuEquipoVM trafo)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo = servIndisponibilidadRemu.EliminarTrafoIndisponible(trafo);
                return codigo;
            }


            [System.Web.Http.Route("Api/Remuneracion/ObtenerUtilidadTrafo")]
            [System.Web.Http.ActionName("ObtenerUtilidadTrafo")]
            [System.Web.Http.HttpGet]
            public IndisRemuEquipoVM ObtenerUtilidadTrafo()
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                IndisRemuEquipoVM factor = servIndisponibilidadRemu.obtenerUtilidadTrafo();

                return factor;
            }


            [System.Web.Http.Route("Api/Remuneracion/ModificarUtilidadTrafo")]
            [System.Web.Http.ActionName("ModificarUtilidadTrafo")]
            [System.Web.Http.HttpPost]
            public int ModificarUtilidadTrafo(int utilidad)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;

                codigo = servIndisponibilidadRemu.modificarUtilidadTrafo(utilidad);

                //((IDisposable)servLinea).Dispose();

                return codigo;
            }

            #endregion

            #region PtoConexion

            [System.Web.Http.Route("Api/Remuneracion/obtenerPTOConexion")]
            [System.Web.Http.ActionName("obtenerPTOConexion")]
            [System.Web.Http.HttpGet]
            public object obtenerPTOConexion(int idResolucion, string fecha)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                object listaPtos = servIndisponibilidadRemu.obtenerPTOConexion(idResolucion, fecha);
                return listaPtos;
            }

            [System.Web.Http.Route("Api/Remuneracion/ObtenerMontosPtosC")]
            [System.Web.Http.ActionName("ObtenerMontosPtosC")]
            [System.Web.Http.HttpGet]
            public object ObtenerMontosPtosC()
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                object listaLinea = servIndisponibilidadRemu.obtenerMontosPtosConexion();
                return listaLinea;
            }

            [System.Web.Http.Route("Api/Remuneracion/AltaMontoPtosC")]
            [System.Web.Http.ActionName("AltaMontoPtosC")]
            [System.Web.Http.HttpPost]
            public int AltaMontoPtosC(MontoIndisponibilidadesVM monto)
            {

                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;

                codigo = servIndisponibilidadRemu.AltaMontoPtosC(monto);

                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/ModificarMontoPtosC")]
            [System.Web.Http.ActionName("ModificarMontoPtosC")]
            [System.Web.Http.HttpPost]
            public int ModificarMontoPtosC(MontoIndisponibilidadesVM monto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;

                codigo = servIndisponibilidadRemu.ModificarMontoPtosC(monto);

                //((IDisposable)servLinea).Dispose();

                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/EliminarMontoPtosC")]
            [System.Web.Http.ActionName("EliminarMontoPtosC")]
            [System.Web.Http.HttpPut]
            public int EliminarMontoPtosC(MontoIndisponibilidadesVM monto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo = servIndisponibilidadRemu.EliminarMontoPtosC(monto);
                return codigo;
            }


            [System.Web.Http.Route("Api/Remuneracion/ObtenerTensionPtosC")]
            [System.Web.Http.ActionName("ObtenerTensionPtosC")]
            [System.Web.Http.HttpGet]
            public object ObtenerTensionPtosC()
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                var tensionServicio = servIndisponibilidadRemu.ObtenerTensionPtosC();
                return tensionServicio;
            }

            [System.Web.Http.Route("Api/Remuneracion/ObtenerHistorialMontoPtosC")]
            [System.Web.Http.ActionName("ObtenerHistorialMontoPtosC")]
            [System.Web.Http.HttpGet]
            public object ObtenerHistorialMontoPtosC()
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                object historialMontoPtosC = servIndisponibilidadRemu.ObtenerHistorialPtosC();
                return historialMontoPtosC;
            }

            [System.Web.Http.Route("Api/Remuneracion/ModificarHistorialMontoPtosC")]
            [System.Web.Http.ActionName("ModificarHistorialMontoPtosC")]
            [System.Web.Http.HttpPost]
            public int ModificarHistorialMontoPtosC(MontoIndisponibilidadesVM monto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;

                codigo = servIndisponibilidadRemu.ModificarHistorialMontoPtosC(monto);

                //((IDisposable)servLinea).Dispose();

                return codigo;
            }


            [System.Web.Http.Route("Api/Remuneracion/ConsultarMontoPtosC")]
            [System.Web.Http.ActionName("ConsultarMontoPtosC")]
            [System.Web.Http.HttpGet]
            public MontoIndisponibilidadesVM ConsultarMontoPtosC(int idPtoC, DateTime fechaDesdeMonto, DateTime? fechaHastaMonto, int idMonto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                MontoIndisponibilidadesVM monto = servIndisponibilidadRemu.ConsultarMontoPtosC(idPtoC, fechaDesdeMonto, fechaHastaMonto, idMonto);

                return monto;
            }

            //Indisponibles

            [System.Web.Http.Route("Api/Remuneracion/obtenerPtosCIndisponibles")]
            [System.Web.Http.ActionName("obtenerPtosCIndisponibles")]
            [System.Web.Http.HttpGet]
            public object obtenerPtosCIndisponibles(DateTime fechaDesde, DateTime fechaHasta)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                object listaPtosCIndisponibles = servIndisponibilidadRemu.obtenerPtosCIndisponibles(fechaDesde, fechaHasta);
                return listaPtosCIndisponibles;
            }

            [System.Web.Http.Route("Api/Remuneracion/AltaPtoCIndisponibles")]
            [System.Web.Http.ActionName("AltaPtoCIndisponibles")]
            [System.Web.Http.HttpPost]
            public int AltaPtoCIndisponibles(IndisRemuEquipoVM ptoC, DateTime fechaDesdeMonto, DateTime? fechaHastaMonto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                var codigo = servIndisponibilidadRemu.AltaPtoCIndisponibles(ptoC, fechaDesdeMonto, fechaHastaMonto);
                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/ModificarPtoCIndisponibles")]
            [System.Web.Http.ActionName("ModificarLineaIndisponibles")]
            [System.Web.Http.HttpPost]
            public int ModificarPtoCIndisponibles(IndisRemuEquipoVM ptoC, DateTime fechaDesdeMonto, DateTime? fechaHastaMonto)
            {

                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;
                codigo = servIndisponibilidadRemu.ModificarPtoCIndisponibles(ptoC, fechaDesdeMonto, fechaHastaMonto);

                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/EliminarPtoCIndisponible")]
            [System.Web.Http.ActionName("EliminarLineaIndisponible")]
            [System.Web.Http.HttpPut]
            public int EliminarPtoCIndisponible(IndisRemuEquipoVM ptoC)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo = servIndisponibilidadRemu.EliminarPtoCIndisponible(ptoC);
                return codigo;
            }


            #endregion

            #region Capacitores
            [System.Web.Http.Route("Api/Remuneracion/obtenerCapacitores")]
            [System.Web.Http.ActionName("obtenerCapacitores")]
            [System.Web.Http.HttpGet]
            public object obtenerCapacitores(int idResolucion, string fecha)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                object listaCap = servIndisponibilidadRemu.obtenerCapacitores(idResolucion, fecha);
                return listaCap;
            }

            [System.Web.Http.Route("Api/Remuneracion/ObtenerMontosCapacitores")]
            [System.Web.Http.ActionName("ObtenerMontosCapacitores")]
            [System.Web.Http.HttpGet]
            public object ObtenerMontosCapacitores()
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                object listaMontoC = servIndisponibilidadRemu.obtenerMontosCapacitores();
                return listaMontoC;
            }

            [System.Web.Http.Route("Api/Remuneracion/AltaMontoCapacitor")]
            [System.Web.Http.ActionName("AltaMontoCapacitor")]
            [System.Web.Http.HttpPost]
            public int AltaMontoCapacitor(MontoIndisponibilidadesVM monto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;
                codigo = servIndisponibilidadRemu.AltaMontoCapacitor(monto);
                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/ModificarMontoCapacitor")]
            [System.Web.Http.ActionName("ModificarMontoCapacitor")]
            [System.Web.Http.HttpPost]
            public int ModificarMontoCapacitor(MontoIndisponibilidadesVM monto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;
                codigo = servIndisponibilidadRemu.ModificarMontoCapacitor(monto);
                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/EliminarMontoCapacitor")]
            [System.Web.Http.ActionName("EliminarMontoCapacitor")]
            [System.Web.Http.HttpPut]
            public int EliminarMontoCapacitor(MontoIndisponibilidadesVM monto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo = servIndisponibilidadRemu.EliminarMontoCapacitor(monto);
                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/ObtenerHistorialMontosCapacitores")]
            [System.Web.Http.ActionName("ObtenerHistorialMontosCapacitores")]
            [System.Web.Http.HttpGet]
            public object ObtenerHistorialMontosCapacitores()
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                object listaMontoC = servIndisponibilidadRemu.ObtenerHistorialMontosCapacitores();
                return listaMontoC;
            }

            [System.Web.Http.Route("Api/Remuneracion/ModificarHistorialMontoCapacitor")]
            [System.Web.Http.ActionName("ModificarHistorialMontoCapacitor")]
            [System.Web.Http.HttpPost]
            public int ModificarHistorialMontoCapacitor(MontoIndisponibilidadesVM monto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;
                codigo = servIndisponibilidadRemu.ModificarHistorialMontoCapacitor(monto);
                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/ConsultarMontoCapacitor")]
            [System.Web.Http.ActionName("ConsultarMontoCapacitor")]
            [System.Web.Http.HttpGet]
            public MontoIndisponibilidadesVM ConsultarMontoCapacitor(DateTime fechaDesdeMonto, DateTime? fechaHastaMonto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                MontoIndisponibilidadesVM monto = servIndisponibilidadRemu.ConsultarMontoCapacitor(fechaDesdeMonto, fechaHastaMonto);

                return monto;
            }


            //Indisponibles

            [System.Web.Http.Route("Api/Remuneracion/obtenerCapacitoresIndisponibles")]
            [System.Web.Http.ActionName("obtenerCapacitoresIndisponibles")]
            [System.Web.Http.HttpGet]
            public object obtenerCapacitoresIndisponibles(DateTime fechaDesde, DateTime fechaHasta)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                object listaCap = servIndisponibilidadRemu.obtenerCapacitoresIndisponibles(fechaDesde, fechaHasta);
                return listaCap;
            }

            [System.Web.Http.Route("Api/Remuneracion/AltaCapacitoresIndisponibles")]
            [System.Web.Http.ActionName("AltaCapacitoresIndisponibles")]
            [System.Web.Http.HttpPost]
            public int AltaCapacitoresIndisponibles(IndisRemuEquipoVM capacitor, DateTime fechaDesdeMonto, DateTime? fechaHastaMonto)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                var codigo = servIndisponibilidadRemu.AltaCapacitoresIndisponibles(capacitor, fechaDesdeMonto, fechaHastaMonto);
                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/ModificarCapacitoresIndisponibles")]
            [System.Web.Http.ActionName("ModificarCapacitoresIndisponibles")]
            [System.Web.Http.HttpPost]
            public int ModificarCapacitoresIndisponibles(IndisRemuEquipoVM capacitor, DateTime fechaDesdeMonto, DateTime? fechaHastaMonto)
            {

                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;
                codigo = servIndisponibilidadRemu.ModificarCapacitoresIndisponibles(capacitor, fechaDesdeMonto, fechaHastaMonto);

                return codigo;
            }

            [System.Web.Http.Route("Api/Remuneracion/EliminarCapacitorIndisponible")]
            [System.Web.Http.ActionName("EliminarCapacitorIndisponible")]
            [System.Web.Http.HttpPut]
            public int EliminarCapacitorIndisponible(IndisRemuEquipoVM capacitor)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo = servIndisponibilidadRemu.EliminarCapacitorIndisponible(capacitor);
                return codigo;
            }

            //Factor valor adicional


            [System.Web.Http.Route("Api/Remuneracion/obtenerCoeficienteDePenalizacion")]
            [System.Web.Http.ActionName("obtenerCoeficienteDePenalizacion")]
            [System.Web.Http.HttpGet]
            public IndisRemuEquipoVM obtenerCoeficienteDePenalizacion()
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                IndisRemuEquipoVM factor = servIndisponibilidadRemu.obtenerCoeficienteDePenalizacion();

                return factor;
            }


            [System.Web.Http.Route("Api/Remuneracion/CambiarCoeficienteDePenalizacion")]
            [System.Web.Http.ActionName("CambiarCoeficienteDePenalizacion")]
            [System.Web.Http.HttpPost]
            public int CambiarCoeficienteDePenalizacion(int FactorK)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;

                codigo = servIndisponibilidadRemu.CambiarCoeficienteDePenalizacion(FactorK);

                //((IDisposable)servLinea).Dispose();

                return codigo;
            }

            #endregion

            //Generar Excel 
            [System.Web.Http.Route("Api/Remuneracion/GenerarExcelGenerico")]
            [System.Web.Http.ActionName("GenerarExcelGenerico")]
            [System.Web.Http.HttpGet]
            public HojaExcelVM GenerarExcelGenerico(string fechaDesde, string fechaHasta, string equipo)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                var result = servIndisponibilidadRemu.GenerarExcelGenerico(fechaDesde, fechaHasta, equipo);
                return result;
            }

            //MESES REMUNERADOS
            [System.Web.Http.Route("Api/Remuneracion/ObtenerMesRemunerado")]
            [System.Web.Http.ActionName("ObtenerMesRemunerado")]
            [System.Web.Http.HttpGet]
            public object ObtenerMesRemunerado(DateTime fecha)
            {
                ServicioIndisponibilidadesRemu servicioRemu = new ServicioIndisponibilidadesRemu();
                object lista = servicioRemu.ObtenerMontosMes(fecha);
                return lista;

            }

            //Generar Excel Meses
            [System.Web.Http.Route("Api/Remuneracion/GenerarReporteExcelMes")]
            [System.Web.Http.ActionName("GenerarReporteExcelMes")]
            [System.Web.Http.HttpGet]
            public HojaExcelVM GenerarReporteExcelMes(string fecha)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                var result = servIndisponibilidadRemu.GenerarReporteExcelMeses(fecha);
                return result;
            }

            [System.Web.Http.Route("Api/Remuneracion/ModificarMontoCammesa")]
            [System.Web.Http.ActionName("ModificarMontoCammesa")]
            [System.Web.Http.HttpPost]
            public int ModificarMontoCammesa(MontoIndisponibilidadesVM mesCammesa)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                int codigo;
                codigo = servIndisponibilidadRemu.ModificarMontoCammesa(mesCammesa);

                return codigo;
            }


            //Generar Excel 
            [System.Web.Http.Route("Api/Remuneracion/GenerarExcelCompleto")]
            [System.Web.Http.ActionName("GenerarExcelCompleto")]
            [System.Web.Http.HttpGet]
            public HojaExcelVM GenerarExcelCompleto(string fechaDesde, string fechaHasta)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                var result = servIndisponibilidadRemu.GenerarExcelCompleto(fechaDesde, fechaHasta);
                return result;
            }

            //Generar Excel Equipos 
            [System.Web.Http.Route("Api/Remuneracion/GenerarExcelEquiposRemu")]
            [System.Web.Http.ActionName("GenerarExcelEquiposRemu")]
            [System.Web.Http.HttpGet]
            public HojaExcelVM GenerarExcelEquiposRemu(int idResolucion, int idEquipo)
            {
                ServicioIndisponibilidadesRemu servIndisponibilidadRemu = new ServicioIndisponibilidadesRemu();
                var result = servIndisponibilidadRemu.GenerarExcelEquiposRemu(idResolucion, idEquipo);
                return result;
            }

            

        }
    }
}
