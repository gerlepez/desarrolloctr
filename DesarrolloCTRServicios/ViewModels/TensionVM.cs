﻿using System;

namespace DesarrolloCTRServicios.ViewModels
{
    public class TensionVM
    {
        public int idTension { get; set; }
        public string tension { get; set; }
        public Boolean? activo { get; set; }
    }
}
