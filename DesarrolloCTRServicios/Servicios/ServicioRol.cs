﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioRol
    {
        public object ObtenerRoles(bool activo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                List<RolVM> listaRoles;
                listaRoles = (from r in db.GralRol
                              where r.activo == activo
                              select new RolVM
                              {
                                  activo = r.activo,
                                  idRol = r.idRol,
                                  nombreRol = r.nombreRol
                              }).ToList();
                object json = new { data = listaRoles };
                return json;
            }
        }

        public object ObtenerRolesSelect(bool activo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                List<RolVM> listaRoles;
                listaRoles = (from r in db.GralRol
                              where r.activo == activo && r.idRol != 1
                              select new RolVM
                              {
                                  activo = r.activo,
                                  idRol = r.idRol,
                                  nombreRol = r.nombreRol
                              }).ToList();
                object json = new { data = listaRoles };
                return json;
            }
        }

        public RolVM ConsultarRol(int idRol)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                RolVM rol;
                List<MenuVM> listAcceso = new List<MenuVM>();
                rol = (from r in db.GralRol
                       where r.activo == true && r.idRol == idRol
                       select new RolVM
                       {
                           activo = r.activo,
                           idRol = r.idRol,
                           nombreRol = r.nombreRol,
                           esJefe = r.esJefe,
                           esOperador = r.esOperador,
                       }).FirstOrDefault();


                rol.menu = new List<MenuVM>();
                var list = db.GralAccesoRol.Where(x => x.idRol == rol.idRol).ToList();
                foreach (var i in list)
                {
                    MenuVM menu = new MenuVM
                    {
                        idAccesoRol = i.idAccesoRol,
                        idMenu = i.idMenu,
                        tienePermisoLectura = i.TienePermisoLectura,
                        tienePermisoEdicion = i.TienePermisoEdicion,
                        tienePermisoEliminacion = i.TienePermisoEliminacion,
                    };
                    rol.menu.Add(menu);
                }

                return rol;
            }
        }

        public int ModificarRol(RolVM rol)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunRol = db.GralRol.Where(x => x.idRol == rol.idRol).FirstOrDefault();
                var list = db.GralAccesoRol.Where(x => x.idRol == rol.idRol).ToList();

                if (consultaunRol != null)
                {
                    //Rol
                    consultaunRol.nombreRol = rol.nombreRol;
                    consultaunRol.esJefe = rol.esJefe;
                    consultaunRol.esOperador = rol.esOperador;

                    foreach (var consultaAccesoRol in list)
                    {
                        //Acceso Rol
                        db.GralAccesoRol.Remove(consultaAccesoRol);
                    }
                    foreach (var lisMenu in rol.menu)
                    {
                        GralAccesoRol gar = new GralAccesoRol
                        {
                            idRol = rol.idRol,
                            TienePermisoLectura = lisMenu.tienePermisoLectura,
                            TienePermisoEdicion = lisMenu.tienePermisoEdicion,
                            TienePermisoEliminacion = lisMenu.tienePermisoEliminacion,
                            idMenu = lisMenu.idMenu,
                            activo = true,
                        };
                        db.GralAccesoRol.Add(gar);
                    }

                }

                db.SaveChanges();
            }
            return 200;

        }

        public int AltaRol(RolVM rol)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunRol = db.GralRol.Where(x => x.idRol == rol.idRol).FirstOrDefault();
                if (consultaunRol == null)
                {
                    try
                    {
                        GralRol r = new GralRol
                        {
                            nombreRol = rol.nombreRol,
                            esJefe = rol.esJefe,
                            esOperador = rol.esOperador,
                            activo = true,
                        };
                        db.GralRol.Add(r);
                        db.SaveChanges();


                        if (rol.menu != null)
                        {
                            var idRol = db.GralRol.OrderByDescending(x => x.idRol).FirstOrDefault().idRol;
                            foreach (var lisMenu in rol.menu)
                            {
                                GralAccesoRol gar = new GralAccesoRol
                                {
                                    idRol = idRol,
                                    TienePermisoLectura = lisMenu.tienePermisoLectura,
                                    TienePermisoEdicion = lisMenu.tienePermisoEdicion,
                                    TienePermisoEliminacion = lisMenu.tienePermisoEliminacion,
                                    idMenu = lisMenu.idMenu,
                                    activo = true,
                                };
                                db.GralAccesoRol.Add(gar);
                            }
                            db.SaveChanges();
                        }
                        return 200;

                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }

            }
        }


        public int EliminarRol(RolVM rol)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarUnRol = db.GralRol.Where(x => x.idRol == rol.idRol).FirstOrDefault();
                //var consultarUnAccesoRol = db.GralAccesoRol.Where(x => x.idRol == rol.idRol).FirstOrDefault();
                var consultaRol = db.GralRol.Where(x => x.idRol == rol.idRol).ToList();
                var list = db.GralAccesoRol.Where(x => x.idRol == rol.idRol).ToList();

                if (consultarUnRol != null)
                {
                    // consultarUnAccesoRol.activo = false;

                    foreach (var EsteRol in consultaRol)
                    {
                        EsteRol.activo = false;
                        //db.GralRol.Remove(EsteRol);
                    }

                    foreach (var consultaAccesoRol in list)
                    {
                        //Acceso Rol
                        //db.GralAccesoRol.Remove(consultaAccesoRol);
                        consultaAccesoRol.activo = false;
                    }

                }
                db.SaveChanges();
            }
            return 200;
        }


        public int RecuperarRol(RolVM rol)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarUnRol = db.GralRol.Where(x => x.idRol == rol.idRol).FirstOrDefault();
                var consultarUnAccesoRol = db.GralAccesoRol.Where(x => x.idRol == rol.idRol).FirstOrDefault();
                if (consultarUnRol != null && consultarUnAccesoRol != null)
                {
                    consultarUnRol.activo = true;
                    consultarUnAccesoRol.activo = true;
                }
                db.SaveChanges();
            }
            return 200;
        }


        /*
        public int CrudRol(RolVM rol, int idUsuario, int tipo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                try
                {
                    //alta
                    if (tipo == 1)
                    {
                        //validar antes
                        GralRol rolaguardar = new GralRol
                        {
                            activo = true,
                            fechaUsuarioAlta = DateTime.Now,
                            idUsuarioAlta = idUsuario,
                            nombreRol = rol.nombreRol
                        };
                        db.GralRol.Add(rolaguardar);
                        db.SaveChanges();
                        return 200;
                    }
                    //modificacion
                    else if (tipo == 2)
                    {
                        var consultarRol = db.GralRol.Find(rol.idRol);

                        if (consultarRol != null)
                        {
                            consultarRol.nombreRol = rol.nombreRol;
                            consultarRol.activo = rol.activo;
                            db.SaveChanges();
                            return 200;
                        }
                        return 400;
                    }
                    //eliminacion
                    else if (tipo == 3)
                    {
                        var consultarRol = db.GralRol.Find(rol.idRol);

                        if (consultarRol != null)
                        {

                            consultarRol.activo = false;
                            db.SaveChanges();
                            return 200;
                        }
                        return 400;
                    }
                    //recuperacion
                    else if (tipo == 4)
                    {
                        var consultarRol = db.GralRol.Find(rol.idRol);

                        if (consultarRol != null)
                        {

                            consultarRol.activo = true;
                            db.SaveChanges();
                            return 200;
                        }
                        return 400;
                    }
                    return 400;
                }
                catch
                {
                    return 400;
                }
            }
        }*/
        public List<MenuVM> ObtenerMenu(string nombreUsuario)
        {
            ServicioUsuario serUsuario = new ServicioUsuario();
            var usuario = serUsuario.ObtenerDatosUsuarioLogueado(nombreUsuario);
            List<MenuVM> lstMenu = new List<MenuVM>();
            if (usuario != null)
            {
                var idUsuario = usuario.idUsuario;
                var rolesUsuario = RolesPorUsuario(idUsuario);


                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    foreach (var item in rolesUsuario)
                    {
                        var menuRol = (from m in db.GralMenu
                                       join ga in db.GralAccesoRol on m.idMenu equals ga.idMenu
                                       where ga.idRol == item.idRol && m.activo == true && m.seMuestra == true
                                       select new MenuVM
                                       {
                                           idMenu = m.idMenu,
                                           codigoMenu = m.codigoMenu,
                                           activo = m.activo,
                                           codigoRetorno = 200,
                                           descripcionMenu = m.descripcionMenu,
                                           icono = m.icono,
                                           idHtml = m.idHtml,
                                           nombreMenu = m.nombreMenu,
                                           urlMenu = m.url,
                                           posicion = m.posicion
                                       }).Where((x) => x.idMenu > 0).Distinct().ToList();

                        foreach (var item2 in menuRol)
                        {
                            lstMenu.Add(item2);
                        }
                    }
                    if (lstMenu.Count > 0)
                    {
                        lstMenu = lstMenu.Where((x) => x.idMenu > 0).Distinct().OrderBy(y => y.posicion).ToList();
                    }
                    else
                    {
                        MenuVM sinMenu = new MenuVM
                        {
                            idMenu = -1,
                            codigoRetorno = 400
                        };
                        lstMenu.Add(sinMenu);
                    }

                }

            }
            return lstMenu;
        }
        public List<RolVM> RolesPorUsuario(int idUsuario)
        {
            List<RolVM> listaRoles = new List<RolVM>();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                using (BuhoGestionEntities buhodb = new BuhoGestionEntities())
                {
                    var consultarUsuario = buhodb.GralUsuario.Find(idUsuario);
                    if (consultarUsuario != null)
                    {
                        var rol = (from r in db.GralRol
                                   join nr in db.GralUsuarioRol on r.idRol equals nr.idRol
                                   where nr.idUsuario == idUsuario && r.activo == true && nr.activo == true
                                   select new RolVM
                                   {
                                       activo = r.activo,
                                       codigoRetorno = 200,
                                       idRol = r.idRol,
                                       nombreRol = r.nombreRol,

                                   }).ToList();
                        foreach (var item in rol)
                        {
                            listaRoles.Add(item);
                        }
                    }
                }

            }
            return listaRoles;
            //var user = nombreUsuario.Remove(0, 10);

        }


        public List<MenuVM> ObtenerListaMenu()
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                List<MenuVM> listaMenu = new List<MenuVM>();
                listaMenu = (from m in db.GralMenu
                             where m.activo == true
                             select new MenuVM
                             {
                                 idMenu = m.idMenu,
                                 nombreMenu = m.nombreMenu,
                             }).ToList();
                return listaMenu;
            }
        }


    }
}
