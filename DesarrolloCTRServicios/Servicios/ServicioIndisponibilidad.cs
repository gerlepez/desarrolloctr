﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using DesarrolloCTRUtilidades;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioIndisponibilidad
    {
        public void EliminarIndisponibilidad(int idNovedadIndisponibilidad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var indis = db.NovNovedadIndisponibilidad.Where(x => x.idNovedadIndisponibilidad == idNovedadIndisponibilidad).FirstOrDefault();
                if (indis != null)
                {
                    indis.activo = false;
                    db.SaveChanges();


                    if (indis.idTipoEquipo == 4)
                    {
                        var lineaIndisRemu = db.IndisponibilidadLineas.Where(x => x.idIndisponibilidad == idNovedadIndisponibilidad && x.activo == true).FirstOrDefault();
                        if (lineaIndisRemu != null)
                        {
                            lineaIndisRemu.activo = false;
                            db.SaveChanges();
                        }
                    }
                    else if (indis.idTipoEquipo == 1)
                    {
                        var trafoRemu = db.IndisponibilidadTrafos.Where(x => x.idIndisponibilidad == idNovedadIndisponibilidad && x.activo == true).FirstOrDefault();
                        if (trafoRemu != null)
                        {
                            trafoRemu.activo = false;
                            db.SaveChanges();
                        }
                    }
                    else if (indis.idTipoEquipo == 2)
                    {
                        var ptoIndisponible = db.IndisponibilidadPtosConexion.Where(x => x.idIndisponibilidad == idNovedadIndisponibilidad && x.activo == true).FirstOrDefault();
                        if (ptoIndisponible != null)
                        {
                            ptoIndisponible.activo = false;
                            db.SaveChanges();
                        }
                    }
                    else if (indis.idTipoEquipo == 7)
                    {
                        var ptoIndisponible = db.IndisponibilidadCapacitor.Where(x => x.idIndisponibilidad == idNovedadIndisponibilidad && x.activo == true).FirstOrDefault();
                        if (ptoIndisponible != null)
                        {
                            ptoIndisponible.activo = false;
                            db.SaveChanges();
                        }
                    }
                }
            }
        }
        public void GenerarIndisponibilidad(IndisponibilidadVM indisponibilidad)
        {
            //ver
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                NovNovedadIndisponibilidad indis = new NovNovedadIndisponibilidad { };

                if (indisponibilidad.codEquipo == "TRA")
                {
                    var getTransf = db.TranTransformador.Find(indisponibilidad.idEquipo);
                    decimal? tension1 = getTransf.Tension1;
                    decimal? tension2 = 0;
                    decimal? tension3 = 0;
                    if (getTransf.Tension2 != null)
                    {
                        tension2 = getTransf.Tension2;
                    }
                    if (getTransf.Tension3 != null)
                    {
                        tension3 = getTransf.Tension3;
                    }
                    decimal? porcentajeIndisp = 0;
                    if (indisponibilidad.idTipoIndisponibilidad == 1)
                    {
                        porcentajeIndisp = 100;
                    }
                    else if (indisponibilidad.idTipoIndisponibilidad == 2)
                    {
                        porcentajeIndisp = ((tension2 + tension3) / tension1) * 100;
                    }
                    else if (indisponibilidad.idTipoIndisponibilidad == 3)
                    {
                        porcentajeIndisp = ((tension1 - tension2 - tension3) / tension1) * 100;
                    }
                    var horaEnString = indisponibilidad.horaEntrada.ToString().Split(' ')[1];
                    var fechaEn = indisponibilidad.fechaEntrada.ToString().Split(' ')[0] + " " + horaEnString;
                    var fechaEntrada = Convert.ToDateTime(fechaEn);
                    var horaSaString = indisponibilidad.horaSalida.ToString().Split(' ')[1];
                    var fechaSa = indisponibilidad.fechaSalida.ToString().Split(' ')[0] + " " + horaSaString;
                    var fechaSalida = Convert.ToDateTime(fechaSa);
                    if (indisponibilidad.idNovedadIndisponibilidad != null) //SI ES UNA EDICION
                    {
                        var indisExistente = db.NovNovedadIndisponibilidad.Where(x => x.idNovedadIndisponibilidad == indisponibilidad.idNovedadIndisponibilidad).FirstOrDefault();
                        if (indisExistente != null)
                        {

                            indisExistente.activo = true;
                            indisExistente.FechaEntrada = fechaEntrada;
                            indisExistente.FechaSalida = fechaSalida;
                            indisExistente.PorcentajeIndisponible = Convert.ToDouble(porcentajeIndisp);
                            indisExistente.idEquipo = indisponibilidad.idEquipo;
                            indisExistente.idTipoEquipo = indisponibilidad.idTipoEquipo;
                            indisExistente.EnergiaNoSuministrada = indisponibilidad.energiaNoSuministrada;
                            indisExistente.idTipoIndisponibilidad = indisponibilidad.idTipoIndisponibilidad;
                            indisExistente.descripcion = indisponibilidad.descripcion;
                            indisExistente.itn = indisponibilidad.itn;
                            indisExistente.fueraDeServicio = indisponibilidad.fueraDeServicio;
                            db.SaveChanges();
                        }
                    }
                    else
                    { //SI ES UNA NOVEDAD NUEVA
                        indis.activo = true;
                        indis.FechaEntrada = fechaEntrada;
                        indis.FechaSalida = fechaSalida;
                        indis.PorcentajeIndisponible = Convert.ToDouble(porcentajeIndisp);
                        indis.idEquipo = indisponibilidad.idEquipo;
                        indis.idTipoEquipo = indisponibilidad.idTipoEquipo;
                        indis.EnergiaNoSuministrada = indisponibilidad.energiaNoSuministrada;
                        indis.idTipoIndisponibilidad = indisponibilidad.idTipoIndisponibilidad;
                        indis.descripcion = indisponibilidad.descripcion;
                        indis.itn = indisponibilidad.itn;
                        indis.fueraDeServicio = indisponibilidad.fueraDeServicio;
                        db.NovNovedadIndisponibilidad.Add(indis);
                        db.SaveChanges();
                    }
                }
                else if (indisponibilidad.codEquipo == "PCON" || indisponibilidad.codEquipo == "LIN" || indisponibilidad.codEquipo == "CAP")
                {
                    var horaEnString = indisponibilidad.horaEntrada.ToString().Split(' ')[1];
                    var fechaEn = indisponibilidad.fechaEntrada.ToString().Split(' ')[0] + " " + horaEnString;
                    var fechaEntrada = Convert.ToDateTime(fechaEn);
                    var horaSaString = indisponibilidad.horaSalida.ToString().Split(' ')[1];
                    var fechaSa = indisponibilidad.fechaSalida.ToString().Split(' ')[0] + " " + horaSaString;
                    var fechaSalida = Convert.ToDateTime(fechaSa);
                    if (indisponibilidad.idNovedadIndisponibilidad != null) //SI ES UNA EDICION
                    {
                        var indisExistente = db.NovNovedadIndisponibilidad.Where(x => x.idNovedadIndisponibilidad == indisponibilidad.idNovedadIndisponibilidad).FirstOrDefault();
                        if (indisExistente != null)
                        {

                            indisExistente.activo = true;
                            indisExistente.FechaEntrada = fechaEntrada;
                            indisExistente.FechaSalida = fechaSalida;
                            indisExistente.PorcentajeIndisponible = 100;
                            indisExistente.idEquipo = indisponibilidad.idEquipo;
                            indisExistente.idTipoEquipo = indisponibilidad.idTipoEquipo;
                            indisExistente.EnergiaNoSuministrada = indisponibilidad.energiaNoSuministrada;
                            indisExistente.idTipoIndisponibilidad = indisponibilidad.idTipoIndisponibilidad;
                            indisExistente.descripcion = indisponibilidad.descripcion;
                            indisExistente.itn = indisponibilidad.itn;
                            indisExistente.fueraDeServicio = indisponibilidad.fueraDeServicio;
                            db.SaveChanges();
                        }




                    }
                    else
                    {
                        indis.activo = true;
                        indis.FechaEntrada = fechaEntrada;
                        indis.FechaSalida = fechaSalida;
                        indis.PorcentajeIndisponible = 100; //??
                        indis.idEquipo = indisponibilidad.idEquipo;
                        indis.idTipoEquipo = indisponibilidad.idTipoEquipo;
                        indis.idTipoIndisponibilidad = 1;
                        indis.descripcion = indisponibilidad.descripcion;
                        indis.itn = indisponibilidad.itn;
                        indis.fueraDeServicio = indisponibilidad.fueraDeServicio;
                        db.NovNovedadIndisponibilidad.Add(indis);
                        db.SaveChanges();
                    }
                }


                //REMUNERACIONES INDISPONIBLES // 

                ServicioIndisponibilidadesRemu serviIndisRemu = new ServicioIndisponibilidadesRemu();

                if (indisponibilidad.codEquipo == "LIN")
                {
                    var lineasRemu = db.IndisponibilidadRemuLineas.Where(x => x.idLinea == indisponibilidad.idEquipo && x.activo == true).FirstOrDefault();
                    var lineaIndisponibles = db.IndisponibilidadLineas.Where(x => x.idIndisponibilidad == indisponibilidad.idNovedadIndisponibilidad && x.activo == true).FirstOrDefault();

                    var tipoSalida = "";
                    if (indisponibilidad.itn == 1)
                    {
                        tipoSalida = "F";
                    }
                    else if (indisponibilidad.itn == 2)
                    {
                        tipoSalida = "FA";
                    }
                    else if (indisponibilidad.itn == 3)
                    {
                        tipoSalida = "P";
                    }
                    else if (indisponibilidad.itn == 4)
                    {
                        tipoSalida = "ST";
                    }


                    var horaEnStringIndis = indisponibilidad.horaEntrada.ToString().Split(' ')[1];
                    var fechaEnIndis = indisponibilidad.fechaEntrada.ToString().Split(' ')[0] + " " + horaEnStringIndis;
                    var fechaEntradaIndis = Convert.ToDateTime(fechaEnIndis);
                    var horaSaStringIndis = indisponibilidad.horaSalida.ToString().Split(' ')[1];
                    var fechaSaIndis = indisponibilidad.fechaSalida.ToString().Split(' ')[0] + " " + horaSaStringIndis;
                    var fechaSalidaIndis = Convert.ToDateTime(fechaSaIndis);
                    var FactorP = db.IndisponibilidadesUtilidades.Where(x => x.idTipoEquipo == 4 && x.activo == true).FirstOrDefault();

                    if (lineaIndisponibles == null) //Si no existe esa linea indisponible , la guarda en remuneraciones/indisponibles
                    {
                        IndisRemuEquipoVM lineaIndis = new IndisRemuEquipoVM();
                        lineaIndis.idlinea = indisponibilidad.idEquipo;
                        lineaIndis.fechaSalida = fechaSalidaIndis;
                        lineaIndis.fechaEntrada = fechaEntradaIndis;
                        lineaIndis.factorPenalizacion = (int)FactorP.valor;
                        lineaIndis.tipoSalida = tipoSalida;
                        lineaIndis.motivo = indisponibilidad.descripcion;
                        lineaIndis.informoEnTermino = indisponibilidad.informoEnTermino;
                        lineaIndis.idLineaIndisRemu = lineasRemu.idLineaIndisRemu;
                        lineaIndis.noCargar = true;
                        lineaIndis.idNovedadIndisponibilidad = indis.idNovedadIndisponibilidad;
                        serviIndisRemu.AltaLineaIndisponibles(lineaIndis, fechaSalidaIndis, fechaEntradaIndis);

                        var ultimaLineaIndis = db.IndisponibilidadLineas.Where(x => x.idIndisponibilidad == indis.idNovedadIndisponibilidad && x.activo == true).FirstOrDefault();
                        if(ultimaLineaIndis != null)
                        {
                            indis.idIndisponibilidadRemu = ultimaLineaIndis.IdLineaRemunerada;
                        }
                        
                        db.SaveChanges();
                    }
                    else
                    {
                        lineaIndisponibles.idLinea = indisponibilidad.idEquipo;
                        lineaIndisponibles.fechaSalida = fechaSalidaIndis;
                        lineaIndisponibles.fechaEntrada = fechaEntradaIndis;
                        lineaIndisponibles.tipoSalida = tipoSalida;
                        lineaIndisponibles.motivo = indisponibilidad.descripcion;
                        lineaIndisponibles.informoEnTermino = indisponibilidad.informoEnTermino;
                        lineaIndisponibles.IdLineaIndisRemu = lineasRemu.idLineaIndisRemu;
                        lineaIndisponibles.idIndisponibilidad = indisponibilidad.idNovedadIndisponibilidad;

                        db.SaveChanges();

                        IndisRemuEquipoVM lineaIndis = new IndisRemuEquipoVM();
                        lineaIndis.idlinea = indisponibilidad.idEquipo;
                        lineaIndis.fechaSalida = fechaSalidaIndis;
                        lineaIndis.fechaEntrada = fechaEntradaIndis;
                        lineaIndis.factorPenalizacion = (int)FactorP.valor;
                        lineaIndis.tipoSalida = tipoSalida;
                        lineaIndis.motivo = indisponibilidad.descripcion;
                        lineaIndis.informoEnTermino = indisponibilidad.informoEnTermino;
                        lineaIndis.idLineaIndisRemu = lineasRemu.idLineaIndisRemu;
                        lineaIndis.noCargar = true;
                        lineaIndis.idNovedadIndisponibilidad = indis.idNovedadIndisponibilidad;
                        serviIndisRemu.ModificarLineaIndisponibles(lineaIndis, fechaSalidaIndis, fechaEntradaIndis);


                    }
                }
                else if (indisponibilidad.codEquipo == "TRA")
                {
                    var trafoRemu = db.IndisponibilidadRemuTrafo.Where(x => x.idTransformador == indisponibilidad.idEquipo && x.activo == true).FirstOrDefault();
                    var trafoIndis = db.IndisponibilidadTrafos.Where(x => x.idIndisponibilidad == indisponibilidad.idNovedadIndisponibilidad && x.activo == true).FirstOrDefault();

                    var tipoSalida = "";
                    if (indisponibilidad.itn == 1)
                    {
                        tipoSalida = "F";
                    }
                    else if (indisponibilidad.itn == 2)
                    {
                        tipoSalida = "FA";
                    }
                    else if (indisponibilidad.itn == 3)
                    {
                        tipoSalida = "P";
                    }
                    else if (indisponibilidad.itn == 99)
                    {
                        tipoSalida = "R";
                    }
                    else if (indisponibilidad.itn == 4)
                    {
                        tipoSalida = "ST";
                    }

                    var horaEnStringIndis = indisponibilidad.horaEntrada.ToString().Split(' ')[1];
                    var fechaEnIndis = indisponibilidad.fechaEntrada.ToString().Split(' ')[0] + " " + horaEnStringIndis;
                    var fechaEntradaIndis = Convert.ToDateTime(fechaEnIndis);
                    var horaSaStringIndis = indisponibilidad.horaSalida.ToString().Split(' ')[1];
                    var fechaSaIndis = indisponibilidad.fechaSalida.ToString().Split(' ')[0] + " " + horaSaStringIndis;
                    var fechaSalidaIndis = Convert.ToDateTime(fechaSaIndis);
                    var utilidad = db.IndisponibilidadesUtilidades.Where(x => x.idTipoEquipo == 1 && x.activo == true).FirstOrDefault();

                    if (trafoIndis == null) //Si no existe ese trafo indisponible , la guarda en remuneraciones/indisponibles
                    {
                        IndisRemuEquipoVM trafoIndisponible = new IndisRemuEquipoVM();
                        trafoIndisponible.idTrafo = indisponibilidad.idEquipo;
                        trafoIndisponible.fechaSalida = fechaSalidaIndis;
                        trafoIndisponible.fechaEntrada = fechaEntradaIndis;
                        trafoIndisponible.factorPenalizacion = (int)utilidad.valor;
                        trafoIndisponible.tipoSalida = tipoSalida;
                        trafoIndisponible.motivo = indisponibilidad.descripcion;
                        trafoIndisponible.informoEnTermino = indisponibilidad.informoEnTermino;
                        trafoIndisponible.idIndisponibilidadesTrafo = trafoRemu.idIndisponibilidadesTrafo;
                        trafoIndisponible.noCargar = true;
                        trafoIndisponible.idNovedadIndisponibilidad = indis.idNovedadIndisponibilidad;
                        trafoIndisponible.ENS = indisponibilidad.energiaNoSuministrada;
                        trafoIndisponible.idTipoIndisponibilidad = indisponibilidad.idTipoIndisponibilidad;
                        serviIndisRemu.AltaTrafosIndisponibles(trafoIndisponible, fechaSalidaIndis, fechaEntradaIndis);

                        var ultimoTrafo = db.IndisponibilidadTrafos.Where(x => x.idIndisponibilidad == indis.idNovedadIndisponibilidad && x.activo == true).FirstOrDefault();
                        if(ultimoTrafo != null)
                        {
                            indis.idIndisponibilidadRemu = ultimoTrafo.idIndisponibilidadTrafo;
                        }
                        
                        db.SaveChanges();
                    }
                    else
                    {
                        trafoIndis.idTrafo = indisponibilidad.idEquipo;
                        trafoIndis.fechaSalida = fechaSalidaIndis;
                        trafoIndis.fechaEntrada = fechaEntradaIndis;
                        trafoIndis.tipoSalida = tipoSalida;
                        trafoIndis.motivo = indisponibilidad.descripcion;
                        trafoIndis.informoEnTermino = indisponibilidad.informoEnTermino;
                        trafoIndis.idIndisponibilidadTrafo = trafoIndis.idIndisponibilidadTrafo;
                        trafoIndis.idIndisponibilidad = indisponibilidad.idNovedadIndisponibilidad;
                        trafoIndis.ENS = indisponibilidad.energiaNoSuministrada;

                        IndisRemuEquipoVM trafoIndisponible = new IndisRemuEquipoVM();
                        trafoIndisponible.idTrafo = indisponibilidad.idEquipo;
                        trafoIndisponible.fechaSalida = fechaSalidaIndis;
                        trafoIndisponible.fechaEntrada = fechaEntradaIndis;
                        trafoIndisponible.factorPenalizacion = (int)utilidad.valor;
                        trafoIndisponible.tipoSalida = tipoSalida;
                        trafoIndisponible.motivo = indisponibilidad.descripcion;
                        trafoIndisponible.informoEnTermino = indisponibilidad.informoEnTermino;
                        trafoIndisponible.idIndisponibilidadesTrafo = trafoRemu.idIndisponibilidadesTrafo;
                        trafoIndisponible.noCargar = true;
                        trafoIndisponible.idNovedadIndisponibilidad = indis.idNovedadIndisponibilidad;
                        trafoIndisponible.ENS = indisponibilidad.energiaNoSuministrada;
                        trafoIndisponible.idTipoIndisponibilidad = indisponibilidad.idTipoIndisponibilidad;

                        db.SaveChanges();
                        //trafoIndis.idTipoIndisponibilidad = indisponibilidad.idTipoIndisponibilidad;
                        serviIndisRemu.ModificarTrafoIndisponibles(trafoIndisponible, fechaSalidaIndis, fechaEntradaIndis);
                    }
                }
                else if (indisponibilidad.codEquipo == "PCON")
                {
                    var ptoRemu = db.IndisponibilidadRemuPtosConexion.Where(x => x.idPuntoConexion == indisponibilidad.idEquipo && x.activo == true).FirstOrDefault();
                    var PtoCindisponibles = db.IndisponibilidadPtosConexion.Where(x => x.idIndisponibilidad == indisponibilidad.idNovedadIndisponibilidad && x.activo == true).FirstOrDefault();

                    var tipoSalida = "";
                    if (indisponibilidad.itn == 1)
                    {
                        tipoSalida = "F";
                    }
                    else if (indisponibilidad.itn == 2)
                    {
                        tipoSalida = "FA";
                    }
                    else if (indisponibilidad.itn == 3)
                    {
                        tipoSalida = "P";
                    }
                    else if (indisponibilidad.itn == 4)
                    {
                        tipoSalida = "ST";
                    }


                    var horaEnStringIndis = indisponibilidad.horaEntrada.ToString().Split(' ')[1];
                    var fechaEnIndis = indisponibilidad.fechaEntrada.ToString().Split(' ')[0] + " " + horaEnStringIndis;
                    var fechaEntradaIndis = Convert.ToDateTime(fechaEnIndis);
                    var horaSaStringIndis = indisponibilidad.horaSalida.ToString().Split(' ')[1];
                    var fechaSaIndis = indisponibilidad.fechaSalida.ToString().Split(' ')[0] + " " + horaSaStringIndis;
                    var fechaSalidaIndis = Convert.ToDateTime(fechaSaIndis);
                    var FactorP = db.IndisponibilidadesUtilidades.Where(x => x.idTipoEquipo == 4 && x.activo == true).FirstOrDefault();

                    if (PtoCindisponibles == null) //Si no existe ese pto indisponible , la guarda en remuneraciones/indisponibles
                    {
                        IndisRemuEquipoVM PtoCIndis = new IndisRemuEquipoVM();
                        PtoCIndis.idPuntoConexion = indisponibilidad.idEquipo;
                        PtoCIndis.fechaSalida = fechaSalidaIndis;
                        PtoCIndis.fechaEntrada = fechaEntradaIndis;
                        PtoCIndis.tipoSalida = tipoSalida;
                        PtoCIndis.motivo = indisponibilidad.descripcion;
                        PtoCIndis.informoEnTermino = indisponibilidad.informoEnTermino;
                        PtoCIndis.idPtoIndisRemu = ptoRemu.idIndisponibilidadesPtosC;
                        PtoCIndis.noCargar = true;
                        PtoCIndis.idNovedadIndisponibilidad = indis.idNovedadIndisponibilidad;
                        serviIndisRemu.AltaPtoCIndisponibles(PtoCIndis, fechaSalidaIndis, fechaEntradaIndis);

                        var ultimoPtoIndis = db.IndisponibilidadPtosConexion.Where(x => x.idIndisponibilidad == indis.idNovedadIndisponibilidad && x.activo == true).FirstOrDefault();
                        if(ultimoPtoIndis != null)
                        {
                            indis.idIndisponibilidadRemu = ultimoPtoIndis.IdPtoRemunerado;

                        }
                        db.SaveChanges();
                    }
                    else
                    {
                        PtoCindisponibles.idPtoConexion = indisponibilidad.idEquipo;
                        PtoCindisponibles.fechaSalida = fechaSalidaIndis;
                        PtoCindisponibles.fechaEntrada = fechaEntradaIndis;
                        PtoCindisponibles.tipoSalida = tipoSalida;
                        PtoCindisponibles.motivo = indisponibilidad.descripcion;
                        PtoCindisponibles.informoEnTermino = indisponibilidad.informoEnTermino;
                        PtoCindisponibles.idPtoIndisRemu = ptoRemu.idIndisponibilidadesPtosC;
                        PtoCindisponibles.idIndisponibilidad = indisponibilidad.idNovedadIndisponibilidad;

                        IndisRemuEquipoVM PtoCIndis = new IndisRemuEquipoVM();
                        PtoCIndis.idPuntoConexion = indisponibilidad.idEquipo;
                        PtoCIndis.fechaSalida = fechaSalidaIndis;
                        PtoCIndis.fechaEntrada = fechaEntradaIndis;
                        PtoCIndis.tipoSalida = tipoSalida;
                        PtoCIndis.motivo = indisponibilidad.descripcion;
                        PtoCIndis.informoEnTermino = indisponibilidad.informoEnTermino;
                        PtoCIndis.idPtoIndisRemu = ptoRemu.idIndisponibilidadesPtosC;
                        PtoCIndis.noCargar = true;
                        PtoCIndis.idNovedadIndisponibilidad = indis.idNovedadIndisponibilidad;

                        serviIndisRemu.ModificarPtoCIndisponibles(PtoCIndis, fechaSalidaIndis, fechaEntradaIndis);
                    }
                }
                else if (indisponibilidad.codEquipo == "CAP")
                {
                    var capRemu = db.IndisponibilidadRemuCapacitor.Where(x => x.idCapacitor == indisponibilidad.idEquipo && x.activo == true).FirstOrDefault();
                    var Capindisponibles = db.IndisponibilidadCapacitor.Where(x => x.idIndisponibilidad == indisponibilidad.idNovedadIndisponibilidad && x.activo == true).FirstOrDefault();

                    var tipoSalida = "";
                    if (indisponibilidad.itn == 1)
                    {
                        tipoSalida = "F";
                    }
                    else if (indisponibilidad.itn == 3)
                    {
                        tipoSalida = "P";
                    }



                    var horaEnStringIndis = indisponibilidad.horaEntrada.ToString().Split(' ')[1];
                    var fechaEnIndis = indisponibilidad.fechaEntrada.ToString().Split(' ')[0] + " " + horaEnStringIndis;
                    var fechaEntradaIndis = Convert.ToDateTime(fechaEnIndis);
                    var horaSaStringIndis = indisponibilidad.horaSalida.ToString().Split(' ')[1];
                    var fechaSaIndis = indisponibilidad.fechaSalida.ToString().Split(' ')[0] + " " + horaSaStringIndis;
                    var fechaSalidaIndis = Convert.ToDateTime(fechaSaIndis);
                    var FactorP = db.IndisponibilidadesUtilidades.Where(x => x.idTipoEquipo == 4 && x.activo == true).FirstOrDefault();

                    if (Capindisponibles == null) //Si no existe ese pto indisponible , la guarda en remuneraciones/indisponibles
                    {
                        IndisRemuEquipoVM CapIndis = new IndisRemuEquipoVM();
                        CapIndis.idCapacitor = indisponibilidad.idEquipo;
                        CapIndis.fechaSalida = fechaSalidaIndis;
                        CapIndis.fechaEntrada = fechaEntradaIndis;
                        CapIndis.tipoSalida = tipoSalida;
                        CapIndis.motivo = indisponibilidad.descripcion;
                        CapIndis.informoEnTermino = indisponibilidad.informoEnTermino;
                        CapIndis.idCapacitorRemunerado = capRemu.idIndisponibilidadesCapacitor;
                        CapIndis.noCargar = true;
                        CapIndis.idNovedadIndisponibilidad = indis.idNovedadIndisponibilidad;
                        CapIndis.idTipoIndisponibilidad = indis.idTipoIndisponibilidad;
                        serviIndisRemu.AltaCapacitoresIndisponibles(CapIndis, fechaSalidaIndis, fechaEntradaIndis);

                        var ultimoPtoIndis = db.IndisponibilidadCapacitor.Where(x => x.idIndisponibilidad == indis.idNovedadIndisponibilidad && x.activo == true).FirstOrDefault();
                        if(ultimoPtoIndis != null)
                        {
                            indis.idIndisponibilidadRemu = ultimoPtoIndis.idIndisponibilidadCapacitor;

                        }
                        db.SaveChanges();
                    }
                    else
                    {
                        Capindisponibles.idCapacitor = indisponibilidad.idEquipo;
                        Capindisponibles.fechaSalida = fechaSalidaIndis;
                        Capindisponibles.fechaEntrada = fechaEntradaIndis;
                        Capindisponibles.tipoSalida = tipoSalida;
                        Capindisponibles.motivo = indisponibilidad.descripcion;
                        Capindisponibles.informoEnTermino = indisponibilidad.informoEnTermino;
                        //     Capindisponibles.idIndisponibilidadCapacitor = capRemu.idIndisponibilidadesCapacitor;
                        Capindisponibles.idIndisponibilidad = indisponibilidad.idNovedadIndisponibilidad;

                        IndisRemuEquipoVM CapIndis = new IndisRemuEquipoVM();
                        CapIndis.idCapacitor = indisponibilidad.idEquipo;
                        CapIndis.fechaSalida = fechaSalidaIndis;
                        CapIndis.fechaEntrada = fechaEntradaIndis;
                        CapIndis.tipoSalida = tipoSalida;
                        CapIndis.motivo = indisponibilidad.descripcion;
                        CapIndis.informoEnTermino = indisponibilidad.informoEnTermino;
                        CapIndis.idCapacitorRemunerado = capRemu.idIndisponibilidadesCapacitor;
                        CapIndis.noCargar = true;
                        CapIndis.idNovedadIndisponibilidad = indis.idNovedadIndisponibilidad;
                        CapIndis.idTipoIndisponibilidad = indis.idTipoIndisponibilidad;
                        serviIndisRemu.ModificarCapacitoresIndisponibles(CapIndis, fechaSalidaIndis, fechaEntradaIndis);
                    }
                }


            }
        }

        public object ObtenerPlanillaIndisponibilidad(string fechaDesdeSalida, string fechaHastaSalida, string draw, int recordsTotal, int skip, int pageSize)
        {
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    ServicioEquipo servEquipo = new ServicioEquipo();
                    IQueryable<IndisponibilidadVM> query = (from i in db.NovNovedadIndisponibilidad
                                                            join e in db.EquiEquipo on i.idTipoEquipo equals e.idEquipo into ee
                                                            from e in ee.DefaultIfEmpty()
                                                            where i.activo == true && i.activo == true
                                                            select new { i, e }).AsEnumerable().Select(x => new IndisponibilidadVM
                                                            {
                                                                idNovedadIndisponibilidad = x.i.idNovedadIndisponibilidad,
                                                                idNovedad = x.i.idNovedadIndisponibilidad,
                                                                descripcion = x.i.descripcion,
                                                                detalleEquipo = servEquipo.ConsultarEquipoDetalle(x.i.idEquipo, x.e.codigoEquipo, 0),
                                                                porcentajeIndisponibilidad = x.i.PorcentajeIndisponible,
                                                                horaEntradaStr = String.Format("{0:HH:mm}", x.i.FechaEntrada),
                                                                horaSalidaStr = String.Format("{0:HH:mm}", x.i.FechaSalida),
                                                                fechaEntradaStr = String.Format("{0:dd/MM/yyyy}", x.i.FechaEntrada),
                                                                fechaSalidaStr = String.Format("{0:dd/MM/yyyy}", x.i.FechaSalida),
                                                                fechaSalida = x.i.FechaSalida,
                                                                fechaEntrada = x.i.FechaEntrada,
                                                                fechaInformeEntradaStr = String.Format("{0:dd/MM/yyyy}", x.i.FechaSalida.Value.AddMinutes(10)),
                                                                horaInformeEntradaStr = String.Format("{0:HH:mm}", x.i.FechaSalida.Value.AddMinutes(10)),
                                                                fueraDeServicio = x.i.fueraDeServicio,
                                                                nombretipoEquipo = x.e.nombreEquipo,
                                                                itn = x.i.itn,
                                                            }).OrderByDescending(x => x.idIndisponibilidad).AsQueryable();

                    var fechaHasta = Convert.ToDateTime(fechaHastaSalida).AddDays(1);
                    var queryFiltrada = FiltrarQuery(query, fechaDesdeSalida, fechaHasta.ToString());
                    recordsTotal = queryFiltrada.Count();
                    var lst = queryFiltrada.Skip(skip).Take(pageSize).ToList();

                    foreach (var item in lst)
                    {
                        switch (item.itn)
                        {
                            case 1:
                                item.nombretipoIndisponibilidad = "Forzado";
                                break;
                            case 2:
                                item.nombretipoIndisponibilidad = "Forzado Autorizado";
                                break;
                            case 3:
                                item.nombretipoIndisponibilidad = "Programado";
                                break;
                            case 4:
                                item.nombretipoIndisponibilidad = "Vinculado sin tensión";
                                break;
                            case 99:
                                item.nombretipoIndisponibilidad = "Reducido";
                                break;
                            default:
                                item.nombretipoIndisponibilidad = "Sin tipo Salida";
                                break;
                        }
                    }



                    object json = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = lst };
                    return json;
                }
            }
            catch (Exception e)
            {
                return 400;
            }
        }


        public object ConsultarIndisponibilidad(int? idIndisponibilidad)
        {
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    var listaIndisp = (from i in db.NovNovedadIndisponibilidad
                                       where i.idNovedadIndisponibilidad == idIndisponibilidad
                                       select new IndisponibilidadVM
                                       {
                                           idIndisponibilidad = i.idNovedadIndisponibilidad,
                                           estadoIndisp = i.activo
                                       }).FirstOrDefault();
                    object json = new { data = listaIndisp };
                    return json;
                }
            }
            catch
            {
                return 400;
            }
        }

        public List<IndisponibilidadVM> ConsultarIndPorEquipo(int? idEquipo, int? idTipoEquipo)
        {
            List<IndisponibilidadVM> listaIndisp = new List<IndisponibilidadVM>();
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    listaIndisp = (from i in db.NovNovedadIndisponibilidad
                                   where i.idEquipo == idEquipo && i.activo == true && i.idTipoEquipo == idTipoEquipo
                                   select new IndisponibilidadVM
                                   {
                                       idIndisponibilidad = i.idTipoIndisponibilidad,
                                       idTipoIndisponibilidad = i.idTipoIndisponibilidad,
                                       estadoIndisp = i.activo,

                                   }).ToList();
                    object json = new { data = listaIndisp };
                    return listaIndisp;
                }
            }
            catch
            {
                return listaIndisp;
            }
        }
        public int GenerarIndisponibilidad(string codigoEquipo, int? idIndisponibilidad, int? idEquipo, int? idTipoEquipo, bool? energiaNoSuministrada)
        {
            try
            {
                var idIndisp = 0;
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    NovNovedadIndisponibilidad indisp = new NovNovedadIndisponibilidad();
                    if (codigoEquipo == "TRA")
                    {
                        var getTransf = db.TranTransformador.Find(idEquipo);
                        decimal? tension1 = getTransf.Tension1;
                        decimal? tension2 = 0;
                        decimal? tension3 = 0;
                        if (getTransf.Tension2 != null)
                        {
                            tension2 = getTransf.Tension2;
                        }
                        if (getTransf.Tension3 != null)
                        {
                            tension3 = getTransf.Tension3;
                        }
                        decimal? porcentajeIndisp = 0;
                        if (idIndisponibilidad == 1)
                        {
                            porcentajeIndisp = 100;
                        }
                        else if (idIndisponibilidad == 2)
                        {
                            porcentajeIndisp = ((tension2 + tension3) / tension1) * 100;
                        }
                        else if (idIndisponibilidad == 3)
                        {
                            porcentajeIndisp = ((tension1 - tension2 - tension3) / tension1) * 100;
                        }

                        indisp.idEquipo = idEquipo;
                        indisp.idTipoEquipo = idTipoEquipo;
                        indisp.idTipoIndisponibilidad = idIndisponibilidad;
                        indisp.PorcentajeIndisponible = Convert.ToDouble(porcentajeIndisp);
                        indisp.FechaSalida = DateTime.Now;
                        indisp.activo = true;
                        indisp.EnergiaNoSuministrada = energiaNoSuministrada;


                        db.NovNovedadIndisponibilidad.Add(indisp);

                        db.SaveChanges();
                        idIndisp = indisp.idNovedadIndisponibilidad;
                    }
                    else if (codigoEquipo == "PCON" || codigoEquipo == "LIN")
                    {

                        indisp.idEquipo = idEquipo;
                        indisp.idTipoEquipo = idTipoEquipo;
                        //indisp.idTipoIndisponibilidad = novedad.Indisponibilidad.idIndisponibilidad; //se setea siempre en 100
                        indisp.PorcentajeIndisponible = 100;
                        indisp.FechaSalida = DateTime.Now;
                        indisp.activo = true;
                        indisp.EnergiaNoSuministrada = energiaNoSuministrada;

                        db.NovNovedadIndisponibilidad.Add(indisp);
                        db.SaveChanges();
                        idIndisp = indisp.idNovedadIndisponibilidad;
                    }

                }
                return idIndisp;//todo ok
            }
            catch (Exception e)
            {
                return -1;
            }
        }


        public IndisponibilidadVM ObtenerIndisponibilidad(int idIndisponibilidad)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                IndisponibilidadVM resp = new IndisponibilidadVM();
                var indis = db.NovNovedadIndisponibilidad.Where(x => x.idNovedadIndisponibilidad == idIndisponibilidad).FirstOrDefault();
                if (indis != null)
                {
                    resp.idIndisponibilidad = indis.idNovedadIndisponibilidad;
                    resp.idEquipo = indis.idEquipo;
                    resp.idTipoEquipo = indis.idTipoEquipo;
                    resp.energiaNoSuministrada = indis.EnergiaNoSuministrada;
                    resp.fechaEntrada = indis.FechaEntrada;
                    resp.fechaSalida = indis.FechaSalida;
                    resp.idTipoIndisponibilidad = indis.idTipoIndisponibilidad;
                    resp.idNovedad = indis.idNovedadIndisponibilidad;
                    resp.descripcion = indis.descripcion;
                    resp.itn = indis.itn;
                    resp.fueraDeServicio = indis.fueraDeServicio;


                    if (indis.idTipoEquipo == 4) //Linea
                    {
                        var lineaIndis = db.IndisponibilidadLineas.Where(x => x.idIndisponibilidad == indis.idNovedadIndisponibilidad).FirstOrDefault();
                        if (lineaIndis != null)
                        {
                            resp.informoEnTermino = lineaIndis.informoEnTermino;
                        }
                    }
                    else if (indis.idTipoEquipo == 1) // Trafo
                    {
                        var trafoIndis = db.IndisponibilidadTrafos.Where(x => x.idIndisponibilidad == indis.idNovedadIndisponibilidad).FirstOrDefault();
                        if (trafoIndis != null)
                        {
                            resp.informoEnTermino = trafoIndis.informoEnTermino;
                        }
                    }
                    else if (indis.idTipoEquipo == 2) // ptoC
                    {
                        var trafoIndis = db.IndisponibilidadPtosConexion.Where(x => x.idIndisponibilidad == indis.idNovedadIndisponibilidad).FirstOrDefault();
                        if (trafoIndis != null)
                        {
                            resp.informoEnTermino = trafoIndis.informoEnTermino;
                        }
                    }
                    else if (indis.idTipoEquipo == 7) // Capacitor
                    {
                        var trafoIndis = db.IndisponibilidadCapacitor.Where(x => x.idIndisponibilidad == indis.idNovedadIndisponibilidad).FirstOrDefault();
                        if (trafoIndis != null)
                        {
                            resp.informoEnTermino = trafoIndis.informoEnTermino;
                        }
                    }

                }
                return resp;
            }
        }

        public HojaExcelVM GenerarReporteExcel(string fechaDesdeSalida, string fechaHastaSalida)
        {
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    ServicioEquipo servEquipo = new ServicioEquipo();
                    IQueryable<IndisponibilidadVM> query = (from i in db.NovNovedadIndisponibilidad
                                                            join e in db.EquiEquipo on i.idTipoEquipo equals e.idEquipo into ee
                                                            from e in ee.DefaultIfEmpty()
                                                            where i.activo == true
                                                            select new { i, e }).AsEnumerable().Select(x => new IndisponibilidadVM
                                                            {

                                                                descripcion = x.i.descripcion,
                                                                idIndisponibilidad = x.i.idNovedadIndisponibilidad,
                                                                idNovedad = x.i.idNovedadIndisponibilidad,
                                                                idTipoEquipo = x.i.idTipoEquipo,
                                                                idEquipo = x.i.idEquipo,
                                                                detalleEquipo = servEquipo.ConsultarEquipoDetalle(x.i.idEquipo, x.e.codigoEquipo, 0),
                                                                equipo = servEquipo.ConsultarUnEquipo(x.e.codigoEquipo),
                                                                acc = 1,
                                                                itn = x.i.itn == 4 ? 1 : x.i.itn,
                                                                detalleIndisponiblidad = x.i.descripcion,
                                                                codigoRetorno = 200,
                                                                idTipoIndisponibilidad = x.i.idTipoIndisponibilidad,
                                                                energiaNoSuministrada = x.i.EnergiaNoSuministrada,
                                                                fechaEntrada = x.i.FechaEntrada,
                                                                fechaSalida = x.i.FechaSalida,
                                                                porcentajeIndisponibilidad = x.i.PorcentajeIndisponible,
                                                                horaEntradaStr = String.Format("{0:HH:mm}", x.i.FechaEntrada),
                                                                horaSalidaStr = String.Format("{0:HH:mm}", x.i.FechaSalida),
                                                                fechaEntradaStr = String.Format("{0:dd/MM/yyyy}", x.i.FechaEntrada),
                                                                fechaSalidaStr = String.Format("{0:dd/MM/yyyy}", x.i.FechaSalida),
                                                                fechaInformeEntrada = x.i.FechaSalida.Value.AddMinutes(10),
                                                                fechaInformeEntradaStr = String.Format("{0:dd/MM/yyyy}", x.i.FechaSalida.Value.AddMinutes(10)),
                                                                horaInformeEntrada = x.i.FechaSalida.Value.AddMinutes(10),
                                                                horaInformeEntradaStr = String.Format("{0:HH:mm}", x.i.FechaSalida.Value.AddMinutes(10))

                                                            }).OrderByDescending(x => x.idIndisponibilidad).AsQueryable();
                    var fechaHasta = Convert.ToDateTime(fechaHastaSalida).AddDays(1);
                    var queryFiltrada = FiltrarQuery(query, fechaDesdeSalida, fechaHasta.ToString());

                    var lst = queryFiltrada.ToList();
                    //if (lst.Count == 0)
                    //{
                    //    HojaExcelVM Excel = new HojaExcelVM { 
                    //        fileName="Error"
                    //    };
                    //    return Excel;
                    //}
                    GenerarExcel excel = new GenerarExcel();
                    HojaExcelVM hojaDetalle = new HojaExcelVM();

                    var col1 = "Equipo";
                    var col2 = "ninf";
                    var col3 = "idnov";
                    var col4 = "ideq";
                    var col5 = "nomb";
                    var col6 = "itn";
                    var col7 = "acc";
                    var col8 = "fsal";
                    var col9 = "hsal";
                    var col10 = "msal";
                    var col11 = "fent";
                    var col12 = "hent";
                    var col13 = "ment";
                    var col14 = "ens";
                    var col15 = "predu";
                    var col16 = "observacion";
                    var col17 = "finf";
                    var col18 = "hinf";
                    var col19 = "minf";

                    hojaDetalle.encabezado = new List<string>();
                    hojaDetalle.encabezado.Add(col1);
                    hojaDetalle.encabezado.Add(col2);
                    hojaDetalle.encabezado.Add(col3);
                    hojaDetalle.encabezado.Add(col4);
                    hojaDetalle.encabezado.Add(col5);
                    hojaDetalle.encabezado.Add(col6);
                    hojaDetalle.encabezado.Add(col7);
                    hojaDetalle.encabezado.Add(col8);
                    hojaDetalle.encabezado.Add(col9);
                    hojaDetalle.encabezado.Add(col10);
                    hojaDetalle.encabezado.Add(col11);
                    hojaDetalle.encabezado.Add(col12);
                    hojaDetalle.encabezado.Add(col13);
                    hojaDetalle.encabezado.Add(col14);
                    hojaDetalle.encabezado.Add(col15);
                    hojaDetalle.encabezado.Add(col16);
                    hojaDetalle.encabezado.Add(col17);
                    hojaDetalle.encabezado.Add(col18);
                    hojaDetalle.encabezado.Add(col19);
                    //if (lst.Count > 0)
                    //{
                    hojaDetalle.listaFilas = new List<DetalleExcelVM>();
                    foreach (var item in lst)
                    {

                        DateTimeFormatInfo dfi = DateTimeFormatInfo.CurrentInfo;
                        Calendar cal = dfi.Calendar;
                        var semanaEntrada = cal.GetWeekOfYear(Convert.ToDateTime(item.fechaEntrada), dfi.CalendarWeekRule, DayOfWeek.Monday);
                        var semanaSalida = cal.GetWeekOfYear(Convert.ToDateTime(item.fechaSalida), dfi.CalendarWeekRule, DayOfWeek.Monday);
                        if (semanaEntrada == semanaSalida)
                        {
                            item.acc = 1;
                        }
                        else
                        {
                            item.acc = 2;
                        }
                        hojaDetalle.unaFila = new DetalleExcelVM();
                        hojaDetalle.unaFila.datoPorFila = new List<string>();
                        hojaDetalle.unaFila.datoPorFila.Add(item.detalleEquipo.nombreTipoEquipo);
                        hojaDetalle.unaFila.datoPorFila.Add(4.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.idNovedad.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.detalleEquipo.idPagoTran.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.detalleEquipo.nombreEquipo);
                        hojaDetalle.unaFila.datoPorFila.Add(item.itn.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.acc.ToString());
                        hojaDetalle.unaFila.datoPorFila.Add(item.fechaSalidaStr);
                        hojaDetalle.unaFila.datoPorFila.Add(item.horaSalidaStr != "" ? item.horaSalidaStr.Split(':')[0] : " ");
                        hojaDetalle.unaFila.datoPorFila.Add(item.horaSalidaStr != "" ? item.horaSalidaStr.Split(':')[1] : " ");
                        hojaDetalle.unaFila.datoPorFila.Add(item.fechaEntradaStr);
                        hojaDetalle.unaFila.datoPorFila.Add(item.horaEntradaStr != "" ? item.horaEntradaStr.Split(':')[0] : " ");
                        hojaDetalle.unaFila.datoPorFila.Add(item.horaEntradaStr != "" ? item.horaEntradaStr.Split(':')[1] : " ");
                        if (item.energiaNoSuministrada == true)
                        {
                            hojaDetalle.unaFila.datoPorFila.Add("S");
                        }
                        else
                        {
                            if (item.energiaNoSuministrada == false)
                            {
                                hojaDetalle.unaFila.datoPorFila.Add("N");
                            }
                            else
                            {
                                hojaDetalle.unaFila.datoPorFila.Add(" ");
                            }
                        }
                        hojaDetalle.unaFila.datoPorFila.Add(item.porcentajeIndisponibilidad.ToString() + " %");
                        hojaDetalle.unaFila.datoPorFila.Add(item.detalleIndisponiblidad);
                        hojaDetalle.unaFila.datoPorFila.Add(item.fechaInformeEntradaStr);
                        hojaDetalle.unaFila.datoPorFila.Add(item.horaInformeEntradaStr != "" ? item.horaInformeEntradaStr.Split(':')[0] : " ");
                        hojaDetalle.unaFila.datoPorFila.Add(item.horaInformeEntradaStr != "" ? item.horaInformeEntradaStr.Split(':')[1] : " ");

                        hojaDetalle.listaFilas.Add(hojaDetalle.unaFila);

                    }
                    //}

                    var datoExcel = excel.GenerarExcelGeneral(hojaDetalle);

                    return datoExcel;

                }
            }
            catch (Exception e)
            {
                var datoExcel = new HojaExcelVM();
                return datoExcel;
            }

        }

        public IQueryable<IndisponibilidadVM> FiltrarQuery(IQueryable<IndisponibilidadVM> query, string fechaDesdeSalida, string fechaHastaSalida)
        {
            if (fechaDesdeSalida != null && fechaHastaSalida != null)
            {
                var fechaDateDesde = DateTime.Parse(fechaDesdeSalida);
                var fechaDateHasta = DateTime.Parse(fechaHastaSalida);
                var lst = query.ToList();
                query = query.Where(x => x.fechaSalida <= fechaDateHasta && x.fechaSalida >= fechaDateDesde);
                lst = query.ToList();
            }
            return query;
        }
    }
}
