﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class EstadoTiempoVM
    {
        public int? idEstadoTiempo { get; set; }
        public string nombreEstadoTiempo { get; set; }
    }
}
