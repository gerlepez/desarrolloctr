﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class EquipoReclamoVM
    {
        public int? idEquipoReclamo { get; set; }
        public string nombreEquipoReclamo { get; set; }
        public int idEquipo { get; set; }
        public string nombreEquipo { get; set; }
    }
}
