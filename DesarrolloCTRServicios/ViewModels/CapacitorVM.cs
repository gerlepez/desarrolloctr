﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class CapacitorVM
    {
        public int idCapacitor { get; set; }
        public int idEquipo { get; set; }
        public int? idGeneral { get; set; }
        public int? potenciaMVAR { get; set; }
        public int? idPagotran { get; set; }
        public int? idBde { get; set; }
        public decimal? TensionKV { get; set; }

        public bool? activo { get; set; }
        public bool? esAutomatismo { get; set; }
        public bool? remunerado { get; set; }

        public string esRemunerado { get; set; }
        public string codigoCapacitor { get; set; }
        public string descripcion { get; set; }
        public string codigoEquipo { get; set; }
        public string codigoSap { get; set; }
        public string nombreCapacitor { get; set; }
        public string nombreEquipo { get; set; }
        public string TieneAutomatismo { get; set; }

        public EstacionVM estacion { get; set; }
        public EquipoVM equipo { get; set; }
    }
}
