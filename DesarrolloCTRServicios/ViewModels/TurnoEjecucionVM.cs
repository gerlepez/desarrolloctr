﻿using System;
using System.Collections.Generic;

namespace DesarrolloCTRServicios.ViewModels
{
    public class TurnoEjecucionVM
    {
        public DateTime? fechaTurno { get; set; }
        public string nombreJefeTurno { get; set; }
        public List<string> nombreAcompañantes { get; set; }
        public string horaAperturaStr { get; set; }
        public string horaCierreStr { get; set; }
        public string estadoTiempo { get; set; }
        public int idTurnoEjecucion { get; set; }
        public DateTime? fechaHoraApertura { get; set; }
        public DateTime? fechaHoraCierre { get; set; }
        public bool? activo { get; set; }
        public int? idJefeTurno { get; set; }
        public int? idTipoTurno { get; set; }
        public TimeSpan? horaDesdeTipoTurno { get; set; }
        public TimeSpan? horaHastaTipoTurno { get; set; }
        public List<NovedadVM> novedades { get; set; }
        public string nombreTipoTurno { get; set; }
        public string nombreAcompañante { get; set; }
        public UsuarioVM nombreJefe { get; set; }
        public NovedadVM novededad { get; set; }
        public TurnoAcompañanteVM turnoacompañante { get; set; }

    }
}
