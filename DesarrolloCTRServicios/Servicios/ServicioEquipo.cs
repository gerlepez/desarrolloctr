﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioEquipo
    {

        public object ObtenerListaEquipo(bool activo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                List<EquipoVM> listaEquipo;

                listaEquipo = (from c in db.EquiEquipo

                               join d in db.EquiEstadoEquipo on c.idEstadoActual equals d.idEstadoEquipo
                               where c.activo == activo && c.TieneEstacion == true
                               select new EquipoVM
                               {
                                   idEquipo = c.idEquipo,
                                   nombreEquipo = c.nombreEquipo,
                                   idUsarioAlta = c.idUsuarioAlta,
                                   fechaUsarioAlta = c.fechaUsuarioAlta,
                                   codigoEquipo = c.codigoEquipo,
                                   activo = c.activo,

                                   estadoEquipo = new EstadoEquipoVM
                                   {
                                       idEstadoEquipo = d.idEstadoEquipo,
                                       nombreEstadoEquipo = d.nombreEstadoEquipo
                                   }
                               }).ToList();

                object json = new { data = listaEquipo };
                return json;
            }

        }
        public object ObtenerEquipoDetallePorEstacion(int idEstacion, string codigoEquipo, int idNovedad = 0)
        {

            if (codigoEquipo != null)
            {
                if (codigoEquipo == "TRA")
                {
                    //llama al servicio transformadores
                    ServicioTransformador servicio = new ServicioTransformador();
                    return servicio.ObtenerTransformadoresPorEstacion(idEstacion, idNovedad);

                }
                else if (codigoEquipo == "PCON")
                {
                    //llama al servicio puntos de conexion
                    ServicioPuntoConexion servicio = new ServicioPuntoConexion();
                    return servicio.ObtenerPuntosPorEstacion(idEstacion, idNovedad);
                }

                else if (codigoEquipo == "LIN")
                {
                    //llama al servicio de lineas
                    ServicioLinea servicio = new ServicioLinea();
                    return servicio.ObtenerLineasPorEstacion(idEstacion, idNovedad);
                }
                if (codigoEquipo == "GEN")
                {
                    ServicioGenerador servicio = new ServicioGenerador();
                    return servicio.ObtenerGeneradoresPorCentral(idEstacion, idNovedad);
                }
                if (codigoEquipo == "CAP")
                {
                    //llama al servicio de capacitores
                    ServicioCapacitores servicio = new ServicioCapacitores();
                    return servicio.ObtenerCapacitoresPorEstacion(idEstacion, idNovedad);
                }
                if (codigoEquipo == "SAUX")
                {
                    //llama al servicio de servicios auxiliares
                    ServicioServiciosAuxiliares servicio = new ServicioServiciosAuxiliares();
                    return servicio.ObtenerServiciosAuxiliaresPorLugar(idEstacion);
                }
            }


            return null;

        }
        public EquipoReclamoVM ObtenerUnEquipoPorInfra(string codigoInfra, int? idEquipo, bool activo)
        {
            try
            {

                EquipoReclamoVM equipoReclamo = new EquipoReclamoVM();
                if (codigoInfra == "ET")
                {
                    ServicioEstacion ET = new ServicioEstacion();
                    var equipo = ET.ConsultarUnEstacion(idEquipo);
                    equipoReclamo.idEquipoReclamo = Convert.ToInt16(equipo.idEquipo);
                    equipoReclamo.nombreEquipoReclamo = equipo.nombreEquipo;
                    return equipoReclamo;
                }
                else if (codigoInfra == "LIN")
                {
                    ServicioLinea LAT = new ServicioLinea();
                    var linea = LAT.ConsultarUnaLinea(idEquipo);
                    equipoReclamo.idEquipoReclamo = linea.idLinea;
                    equipoReclamo.nombreEquipoReclamo = linea.nombreEquipo;
                    return equipoReclamo;
                }
                else if (codigoInfra == "EDI")
                {
                    ServicioInfraestructura infr = new ServicioInfraestructura();
                    var edificio = infr.ConsultarUnaInfraestructura(idEquipo);
                    equipoReclamo.idEquipoReclamo = edificio.idInfra;
                    equipoReclamo.nombreEquipoReclamo = edificio.nombreInfra;
                    return equipoReclamo;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public object ObtenerEquiposPorInfraestructura(string codigoInfra, bool activo)
        {
            if (codigoInfra == "ET")
            {
                // llama al servicio de Estacion Transformadora
                ServicioEstacion estacion = new ServicioEstacion();
                return estacion.ObtenerListaEstacionDistroCuyo(activo);
            }
            else if (codigoInfra == "LIN")
            {

                //llama al servicio de Linea
                ServicioLinea lineas = new ServicioLinea();
                return lineas.ObtenerListaLineaDistroCuyo(activo);
            }
            else if (codigoInfra == "EDI")
            {
                ServicioInfraestructura infraestructura = new ServicioInfraestructura();
                return infraestructura.ObtenerInfraestructuraReclamo(activo);
            }
            else
            {
                return 400;
            }


        }

        public object ObtenerEquiposPorIndisponibilidad(bool activo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                List<EquipoVM> listaEquipo;

                listaEquipo = (from c in db.EquiEquipo

                               join d in db.EquiEstadoEquipo on c.idEstadoActual equals d.idEstadoEquipo
                               where c.activo == activo && (c.codigoEquipo == "TRA" || c.codigoEquipo == "PCON" || c.codigoEquipo == "LIN")
                               select new EquipoVM
                               {
                                   idEquipo = c.idEquipo,
                                   nombreEquipo = c.nombreEquipo,
                                   idUsarioAlta = c.idUsuarioAlta,
                                   fechaUsarioAlta = c.fechaUsuarioAlta,
                                   codigoEquipo = c.codigoEquipo,
                                   activo = c.activo,

                                   estadoEquipo = new EstadoEquipoVM
                                   {
                                       idEstadoEquipo = d.idEstadoEquipo,
                                       nombreEstadoEquipo = d.nombreEstadoEquipo
                                   }
                               }).ToList();

                object json = new { data = listaEquipo };
                return json;
            }

        }
        public List<EquipoVM> ObtenerEquipoPorTipo(string codTipo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                List<EquipoVM> lista = new List<EquipoVM>();
                switch (codTipo)
                {
                    case "TRA":
                        var tran = db.TranTransformador.Where(x => x.activo == true).ToList();
                        foreach (var t in tran)
                        {
                            EquipoVM equipo = new EquipoVM
                            {
                                nombreEquipo = t.nombreTransformador,
                                idEquipo = t.idTransformador,
                            };
                            lista.Add(equipo);
                        }
                        return lista;
                    case "LIN":
                        var lstLin = db.LinLinea.Where(x => x.activo == true).ToList();
                        foreach (var lin in lstLin)
                        {
                            EquipoVM equipo = new EquipoVM
                            {
                                nombreEquipo = lin.nombreLinea,
                                idEquipo = lin.idLinea,
                            };
                            lista.Add(equipo);
                        }
                        return lista;

                    case "PCON":
                        var lstPcon = db.PCPuntoConexion.Where(x => x.activo == true).ToList();
                        foreach (var pc in lstPcon)
                        {
                            EquipoVM equipo = new EquipoVM
                            {
                                nombreEquipo = pc.nombrePuntoConexion,
                                idEquipo = pc.idPuntoConexion,
                            };
                            lista.Add(equipo);
                        }
                        return lista;

                    case "CAP":
                        var listCap = db.CapCapacitor.Where(x => x.activo == true).ToList();
                        foreach (var ca in listCap)
                        {
                            EquipoVM equipo = new EquipoVM
                            {
                                nombreEquipo = ca.nombreCapacitor,
                                idEquipo = ca.idCapacitor,
                            };
                            lista.Add(equipo);
                        }
                        return lista;

                    default: return lista;

                }
            }
        }

        public List<EquipoVM> ObtenerEquiposIndisponibles(string codTipo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                List<EquipoVM> lista = new List<EquipoVM>();
                switch (codTipo)
                {
                    case "TRA":
                        //var tran = db.IndisponibilidadRemuTrafo.Where(x => x.activo == true).ToList();
                        var trans = db.TranTransformador.Where(x => x.activo == true && x.remunerado == true).ToList();

                        foreach (var t in trans)
                        {
                            //var trans = db.TranTransformador.Where(x => x.idTransformador == t.idTransformador && x.activo == true).FirstOrDefault();
                            if (trans != null)
                            {
                                EquipoVM equipo = new EquipoVM
                                {
                                    nombreEquipo = t.nombreTransformador,
                                    idEquipo = t.idTransformador,
                                };
                                lista.Add(equipo);
                            }

                        }
                        return lista;
                    case "LIN":
                        //var lineaIndis = db.IndisponibilidadRemuLineas.Where(x => x.activo == true).ToList();
                        var lstLin = db.LinLinea.Where(x => x.activo == true && x.remunerado == true).ToList();

                        foreach (var lin in lstLin)
                        {
                            //var lstLin = db.LinLinea.Where(x => x.activo == true && x.idLinea == lin.idLinea).FirstOrDefault();
                            if (lstLin != null)
                            {
                                EquipoVM equipo = new EquipoVM
                                {
                                    nombreEquipo = lin.nombreLinea,
                                    idEquipo = lin.idLinea,
                                };
                                lista.Add(equipo);
                            }

                        }
                        return lista;

                    case "PCON":
                        //var ptcoIndisponibles = db.IndisponibilidadRemuPtosConexion.Where(x => x.activo == true).ToList();
                        var lstPcon = db.PCPuntoConexion.Where(x => x.activo == true && x.remunerado == true).ToList();

                        foreach (var pc in lstPcon)
                        {
                            if (lstPcon != null)
                            {
                                EquipoVM equipo = new EquipoVM
                                {
                                    nombreEquipo = pc.nombrePuntoConexion,
                                    idEquipo = pc.idPuntoConexion,
                                };
                                lista.Add(equipo);
                            }

                        }
                        return lista;

                    case "CAP":
                        //var capIndisponible = db.IndisponibilidadRemuCapacitor.Where(x => x.activo == true).ToList();
                        var listCap = db.CapCapacitor.Where(x => x.activo == true && x.remunerado == true).ToList();

                        foreach (var ca in listCap)
                        {
                            if (listCap != null)
                            {
                                EquipoVM equipo = new EquipoVM
                                {
                                    nombreEquipo = ca.nombreCapacitor,
                                    idEquipo = ca.idCapacitor,
                                };
                                lista.Add(equipo);
                            }

                        }
                        return lista;

                    default: return lista;

                }
            }
        }

        public EquipoDetalleEstacionVM ConsultarEquipoDetalle(int? idEquipo, string codigoEquipo, int? idCentral)
        {

            if (codigoEquipo != null)
            {
                if (codigoEquipo == "TRA")
                {
                    //llama al servicio transformadores
                    ServicioTransformador servicio = new ServicioTransformador();
                    var ConsultaTransformador = servicio.ConsultarUnTransformador(idEquipo);
                    EquipoDetalleEstacionVM equiDetalleEstacion = new EquipoDetalleEstacionVM();
                    equiDetalleEstacion.idEquipoDetalleEstacion = ConsultaTransformador.estacion.idEstacion;
                    equiDetalleEstacion.idEquipo = ConsultaTransformador.idTransformador;
                    equiDetalleEstacion.nombreEquipoDetalleEstacion = ConsultaTransformador.estacion.nombreEstacion;
                    equiDetalleEstacion.nombreEquipo = ConsultaTransformador.nombreTransformador;
                    equiDetalleEstacion.CodigoEquipo = codigoEquipo;
                    equiDetalleEstacion.idPagoTran = ConsultaTransformador.idPagotran;
                    equiDetalleEstacion.nombreTipoEquipo = "TRANSFORMADOR";
                    return equiDetalleEstacion;

                }
                else if (codigoEquipo == "PCON")
                {
                    //llama al servicio puntos de conexion
                    ServicioPuntoConexion servicio = new ServicioPuntoConexion();
                    var ConsultaPuntodeConexion = servicio.ConsultarUnPuntoDeConexion(idEquipo);
                    EquipoDetalleEstacionVM equiDetalleEstacion = new EquipoDetalleEstacionVM();
                    equiDetalleEstacion.idEquipoDetalleEstacion = ConsultaPuntodeConexion.estacion.idEstacion;
                    equiDetalleEstacion.idEquipo = ConsultaPuntodeConexion.idPuntoConexion;
                    equiDetalleEstacion.nombreEquipoDetalleEstacion = ConsultaPuntodeConexion.estacion.nombreEstacion;
                    equiDetalleEstacion.nombreEquipo = ConsultaPuntodeConexion.nombrePuntoConexion;
                    equiDetalleEstacion.CodigoEquipo = codigoEquipo;
                    equiDetalleEstacion.idPagoTran = ConsultaPuntodeConexion.idPagotran;
                    equiDetalleEstacion.nombreTipoEquipo = "PUNTO DE CONEXION";
                    return equiDetalleEstacion;
                }
                else if (codigoEquipo == "EDI")
                {
                    //llama al servicio de infraestructura
                    ServicioInfraestructura servicio = new ServicioInfraestructura();
                    var ConsultaEdificio = servicio.ConsultarUnaInfraestructura(idEquipo);
                    EquipoDetalleEstacionVM equiDetalleEstacion = new EquipoDetalleEstacionVM();
                    equiDetalleEstacion.idEquipoDetalleEstacion = ConsultaEdificio.idInfra;
                    equiDetalleEstacion.idEquipo = ConsultaEdificio.idInfra;
                    equiDetalleEstacion.nombreEquipoDetalleEstacion = ConsultaEdificio.nombreInfra;
                    equiDetalleEstacion.nombreEquipo = ConsultaEdificio.nombreInfra;
                    equiDetalleEstacion.CodigoEquipo = codigoEquipo;
                    equiDetalleEstacion.nombreTipoEquipo = "EDIFICIO";
                    return equiDetalleEstacion;

                }
                else if (codigoEquipo == "LIN")
                {
                    //llama al servicio de lineas
                    ServicioLinea servicio = new ServicioLinea();
                    var ConsultaLinea = servicio.ConsultarUnaLinea(idEquipo);
                    EquipoDetalleEstacionVM equiDetalleEstacion = new EquipoDetalleEstacionVM();

                    if (ConsultaLinea == null)
                    {

                    }
                    else
                    {
                        equiDetalleEstacion.idEquipoDetalleEstacion = ConsultaLinea.idLinea;
                        equiDetalleEstacion.idEquipo = ConsultaLinea.idLinea;
                        equiDetalleEstacion.nombreEquipoDetalleEstacion = ConsultaLinea.nombreLinea;
                        equiDetalleEstacion.nombreEquipo = ConsultaLinea.nombreLinea;
                        equiDetalleEstacion.CodigoEquipo = codigoEquipo;
                        equiDetalleEstacion.idEstacion = ConsultaLinea.estacionDestino.idEstacion;
                        equiDetalleEstacion.propiedadLinea = ConsultaLinea.esPropia;
                        equiDetalleEstacion.idTipoLinea = ConsultaLinea.idTipoLinea;
                        equiDetalleEstacion.idPagoTran = ConsultaLinea.idPagotran;
                        equiDetalleEstacion.nombreTipoEquipo = "LINEA";
                        equiDetalleEstacion.idPropiedad = ConsultaLinea.idPropiedad;
                    }
                    return equiDetalleEstacion;


                }
                else if (codigoEquipo == "GEN")
                {
                    EquipoDetalleEstacionVM equiDetalleEstacion = new EquipoDetalleEstacionVM();

                    ServicioGenerador servicio = new ServicioGenerador();
                    var ConsultaGenerador = servicio.ConsultarUnGenerador(Convert.ToInt16(idEquipo)); //cambiar cuando se finalice el abm generador
                    if (ConsultaGenerador != null)
                    {
                        equiDetalleEstacion.idEquipoDetalleEstacion = Convert.ToInt16(ConsultaGenerador.idGenerador);
                        equiDetalleEstacion.idEquipo = Convert.ToInt16(ConsultaGenerador.idGenerador);
                        //equiDetalleEstacion.nombreEquipoDetalleEstacion = ConsultaGenerador.nombreGenerador;
                        equiDetalleEstacion.nombreEquipo = ConsultaGenerador.nombreGenerador;
                        equiDetalleEstacion.idCentral = ConsultaGenerador.idCentral;
                        equiDetalleEstacion.idCentroControl = ConsultaGenerador.idCentroControl;
                        equiDetalleEstacion.nombreTipoEquipo = "GENERADOR";
                    }
                    equiDetalleEstacion.CodigoEquipo = codigoEquipo;
                    return equiDetalleEstacion;

                }
                else if (codigoEquipo == "CAP")
                {
                    //llama al servicio de capacitores
                    ServicioCapacitores servicio = new ServicioCapacitores();
                    var ConsultaCapacitor = servicio.ConsultarUnCapacitor(idEquipo);
                    EquipoDetalleEstacionVM equiDetalleEstacion = new EquipoDetalleEstacionVM();
                    equiDetalleEstacion.idEquipoDetalleEstacion = ConsultaCapacitor.estacion.idEstacion;
                    equiDetalleEstacion.idEquipo = Convert.ToInt16(ConsultaCapacitor.idCapacitor);
                    equiDetalleEstacion.nombreEquipoDetalleEstacion = ConsultaCapacitor.estacion.nombreEstacion;
                    equiDetalleEstacion.nombreEquipo = ConsultaCapacitor.nombreCapacitor;
                    equiDetalleEstacion.CodigoEquipo = codigoEquipo;
                    equiDetalleEstacion.nombreTipoEquipo = "CAPACITOR";
                    return equiDetalleEstacion;

                }
                else if (codigoEquipo == "SAUX")
                {
                    //llama al servicio de servicios auxiliares
                    ServicioServiciosAuxiliares servicio = new ServicioServiciosAuxiliares();
                    var ConsultaServicioAuxiliar = servicio.ConsultarUnServicioAuxiliar(idEquipo);
                    EquipoDetalleEstacionVM equiDetalleEstacion = new EquipoDetalleEstacionVM();
                    equiDetalleEstacion.idEquipoDetalleEstacion = Convert.ToInt16(ConsultaServicioAuxiliar.idServAuxiliar);
                    equiDetalleEstacion.idEquipo = Convert.ToInt16(ConsultaServicioAuxiliar.idServAuxiliar);
                    equiDetalleEstacion.nombreEquipoDetalleEstacion = ConsultaServicioAuxiliar.nombreServicioAuxiliar;
                    equiDetalleEstacion.nombreEquipo = ConsultaServicioAuxiliar.nombreServicioAuxiliar;
                    equiDetalleEstacion.nombreTipoEquipo = "SERVICIO AUXILIAR";
                    equiDetalleEstacion.CodigoEquipo = codigoEquipo;
                    return equiDetalleEstacion;

                }
                else if (codigoEquipo == "PSFV")
                {
                    EquipoDetalleEstacionVM equiDetalleEstacion = new EquipoDetalleEstacionVM();
                    ServicioCentrales central = new ServicioCentrales();
                    var centralConCentro = central.ConsultarUnaCentral(idCentral);
                    if (centralConCentro != null)
                    {
                        equiDetalleEstacion.idCentral = idCentral;
                        equiDetalleEstacion.idCentroControl = centralConCentro.idCentroControl;
                        equiDetalleEstacion.nombreEquipoDetalleEstacion = centralConCentro.nombreCentral;
                        equiDetalleEstacion.nombreEquipo = centralConCentro.nombreCentral;
                    }
                    equiDetalleEstacion.CodigoEquipo = codigoEquipo;
                    return equiDetalleEstacion;
                }

            }


            return null;

        }

        public int AltaEquipo(EquipoVM equipo, int idUsuario)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunEquipo = db.EquiEquipo.Where(x => x.idEquipo == equipo.idEquipo).FirstOrDefault();
                if (consultaunEquipo == null)
                {
                    try
                    {
                        EquiEquipo equi = new EquiEquipo
                        {
                            nombreEquipo = equipo.nombreEquipo,
                            activo = true,
                            idUsuarioAlta = idUsuario, //
                            fechaUsuarioAlta = DateTime.Now,
                            codigoEquipo = equipo.codigoEquipo,
                            idEstadoActual = 1
                        };
                        db.EquiEquipo.Add(equi);
                        db.SaveChanges();
                        EquiHistoricoEquipo historial = new EquiHistoricoEquipo
                        {
                            idEquipo = equi.idEquipo,
                            activo = true,
                            fechaCambioEstado = equi.fechaUsuarioAlta,
                            idEstadoEquipo = 1,
                            idUsuario = equi.idUsuarioAlta
                        };

                        db.EquiHistoricoEquipo.Add(historial);
                        db.SaveChanges();
                        return 200;
                    }
                    catch (Exception e)
                    {
                        return 404;
                    }

                }
                else
                {
                    return 300; //ya existe ese dato
                }
            }


        }

        public int ModificarEquipo(EquipoVM equipo, int idUsuario)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunEquipo = db.EquiEquipo.Where(x => x.idEquipo == equipo.idEquipo).FirstOrDefault();
                if (consultaunEquipo != null)
                {
                    consultaunEquipo.nombreEquipo = equipo.nombreEquipo;
                    consultaunEquipo.activo = true;
                    consultaunEquipo.idUsuarioModifica = idUsuario;//
                    consultaunEquipo.codigoEquipo = equipo.codigoEquipo;
                    consultaunEquipo.fechaUsuarioModifica = DateTime.Now;

                    db.SaveChanges();
                }

                return 200;
            }

        }

        public int EliminarEquipo(EquipoVM equipo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunEquipo = db.EquiEquipo.Where(x => x.idEquipo == equipo.idEquipo).FirstOrDefault();
                if (consultaunEquipo != null)
                {
                    consultaunEquipo.activo = false;
                }

                db.SaveChanges();

                return 200;
            }
        }

        public bool ValidarEquipo(EquipoVM equipo)
        {
            if (/*!string.IsNullOrWhiteSpace(equipo.codigoEquipo) &&*/ equipo.nombreEquipo.Count() <= 30)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public EquipoVM ConsultarUnEquipo(string codigoEquipo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                EquipoVM estacion = new EquipoVM();

                estacion = (from c in db.EquiEquipo
                            join d in db.EquiEstadoEquipo on c.idEstadoActual equals d.idEstadoEquipo
                            where c.activo == true && c.codigoEquipo == codigoEquipo
                            select new EquipoVM
                            {
                                idEquipo = c.idEquipo,
                                nombreEquipo = c.nombreEquipo,
                                idUsarioAlta = c.idUsuarioAlta,
                                fechaUsarioAlta = c.fechaUsuarioAlta,
                                activo = c.activo,
                                codigoEquipo = c.codigoEquipo,
                                estadoEquipo = new EstadoEquipoVM
                                {
                                    idEstadoEquipo = d.idEstadoEquipo,
                                    nombreEstadoEquipo = d.nombreEstadoEquipo
                                }
                            }).FirstOrDefault();

                return estacion;
            }
        }

        public EquipoVM ConsultarUnEquipoPorId(int? idTipoEquipo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                EquipoVM estacion = new EquipoVM();

                estacion = (from c in db.EquiEquipo
                            join d in db.EquiEstadoEquipo on c.idEstadoActual equals d.idEstadoEquipo
                            where c.activo == true && c.idEquipo == idTipoEquipo
                            select new EquipoVM
                            {
                                idEquipo = c.idEquipo,
                                nombreEquipo = c.nombreEquipo,
                                idUsarioAlta = c.idUsuarioAlta,
                                fechaUsarioAlta = c.fechaUsuarioAlta,
                                activo = c.activo,
                                codigoEquipo = c.codigoEquipo,
                                estadoEquipo = new EstadoEquipoVM
                                {
                                    idEstadoEquipo = d.idEstadoEquipo,
                                    nombreEstadoEquipo = d.nombreEstadoEquipo
                                }
                            }).FirstOrDefault();

                return estacion;
            }
        }

        public int RecuperarEquipo(EquipoVM equipo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunEquipo = db.EquiEquipo.Where(x => x.idEquipo == equipo.idEquipo).FirstOrDefault();
                if (consultaunEquipo != null)
                {
                    consultaunEquipo.activo = true;
                }

                db.SaveChanges();

                return 200;
            }
        }

        public int ObtenerTipoIndisponibilidad(int idEquipo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {

                var equi = db.TranTransformador.Where(x => x.idTransformador == idEquipo).FirstOrDefault();
                var c = 1;
                if (equi.Tension2 != null)
                {
                    c = c + 1;
                }
                if (equi.Tension3 != null)
                {
                    c = c + 1;
                }
                return c;
            }
        }
    }
}
