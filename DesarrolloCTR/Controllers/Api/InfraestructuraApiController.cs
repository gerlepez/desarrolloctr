﻿using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers.Api
{
    public class InfraestructuraApiController : ApiController
    {
        [System.Web.Http.Route("Api/Infraestructura/ObtenerInfraestructura")]
        [System.Web.Http.ActionName("ObtenerInfraestructura")]
        [System.Web.Http.HttpGet]
        public object ObtenerInfraestructura(bool activo)
        {
            ServicioInfraestructura servInfra = new ServicioInfraestructura();
            var infraestructura = servInfra.ObtenerInfraestructura(activo);
            return infraestructura;
        }

        [System.Web.Http.Route("Api/Infraestructura/ObtenerInfraestructuraReclamo")]
        [System.Web.Http.ActionName("ObtenerInfraestructuraReclamo")]
        [System.Web.Http.HttpGet]
        public object ObtenerInfraestructuraReclamo(bool activo)
        {
            ServicioInfraestructura servInfra = new ServicioInfraestructura();
            var infraestructuraReclamo = servInfra.ObtenerInfraestructuraReclamo(activo);
            return infraestructuraReclamo;
        }


        [System.Web.Http.Route("Api/Infraestructura/ConsultarUnaInfraestructura")]
        [System.Web.Http.ActionName("ConsultarUnaInfraestructura")]
        [System.Web.Http.HttpGet]
        public InfraestructuraVM ConsultarUnaInfraestructura(int idInfraestructura)
        {
            ServicioInfraestructura servInfra = new ServicioInfraestructura();
            InfraestructuraVM infraestructura = servInfra.ConsultarUnaInfraestructura(idInfraestructura);
            return infraestructura;
        }


        [System.Web.Http.Route("Api/Infraestructura/AltaInfraestructura")]
        [System.Web.Http.ActionName("AltaGenerador")]
        [System.Web.Http.HttpPost]
        public int AltaInfraestructura(InfraestructuraVM infra)
        {
            ServicioInfraestructura servicioinfra = new ServicioInfraestructura();
            int codigo;
            if (servicioinfra.ValidarInfraestructura(infra))
            {
                codigo = servicioinfra.AltaInfraestructura(infra);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }

        [System.Web.Http.Route("Api/Infraestructura/ModificarInfraestructura")]
        [System.Web.Http.ActionName("ModificarInfraestructura")]
        [System.Web.Http.HttpPost]
        public int ModificarInfraestructura(InfraestructuraVM infra)
        {
            ServicioInfraestructura servicioinfra = new ServicioInfraestructura();
            int codigo;
            if (servicioinfra.ValidarInfraestructura(infra))
            {
                codigo = servicioinfra.ModificarInfraestructura(infra);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }

        [System.Web.Http.Route("Api/Infraestructura/EliminarInfraestructura")]
        [System.Web.Http.ActionName("EliminarInfraestructura")]
        [System.Web.Http.HttpPost]
        public int EliminarInfraestructura(InfraestructuraVM infra)
        {
            ServicioInfraestructura servicioinfra = new ServicioInfraestructura();
            int codigo = servicioinfra.EliminarInfraestructura(infra);
            return codigo;
        }

        [System.Web.Http.Route("Api/Infraestructura/RecuperarInfraestructura")]
        [System.Web.Http.ActionName("RecuperarInfraestructura")]
        [System.Web.Http.HttpPost]
        public int RecuperarInfraestructura(InfraestructuraVM infra)
        {
            ServicioInfraestructura servicioinfra = new ServicioInfraestructura();
            int codigo = servicioinfra.RecuperarInfraestructura(infra);
            return codigo;

        }


        [System.Web.Http.Route("Api/Infraestructura/ObtenerTipoInfraestructura")]
        [System.Web.Http.ActionName("ObtenerTipoInfraestructura")]
        [System.Web.Http.HttpGet]
        public object ObtenerTipoInfraestructura(bool activo)
        {
            ServicioInfraestructura servInfra = new ServicioInfraestructura();
            var TipoInfraestructura = servInfra.ObtenerTipoInfraestructura(activo);
            return TipoInfraestructura;
        }


    }

}