﻿using System;
using System.Collections.Generic;

namespace DesarrolloCTRServicios.ViewModels
{
    public class LicenciaVM
    {
        public int idLicencia { get; set; }
        public DateTime? fechaHoraInicioLicencia { get; set; }
        public string fechaHoraInicioLic => fechaHoraInicioLicencia.HasValue ? fechaHoraInicioLicencia.Value.ToString() : " ";
        public DateTime? fechaHoraFinLicencia { get; set; }

        public DateTime? fechaInicio { get; set; }
        public DateTime? horaInicio { get; set; }
        public string codigoLicencia { get; set; }
        public string codigoAutorizacion { get; set; }
        public int codigoEstadoAlta { get; set; }
        public int codigoEstadoModif { get; set; }
        public string descripcionLicencia { get; set; }
        public int? idUsarioAlta { get; set; }
        public DateTime? fechaUsarioAlta { get; set; }
        public int? idUsuarioModifica { get; set; }
        public DateTime? fechaUsuarioModifica { get; set; }
        public bool? activo { get; set; }
        public string nombresuario { get; set; }
        public int? idNovedad { get; set; }
        public int? idTipoLicencia { get; set; }
        public int? idPersonal { get; set; }
        public int? idPersonalAutoriza { get; set; }
        public int? idResponsableConsigna { get; set; }
        public int? idPersonalFinaliza { get; set; }
        public int? idPersonalCancela { get; set; }
        public string observacionesFinaliza { get; set; }
        public List<AutorizacionTrabajoVM> autorizacionTrabajo { get; set; }
        public EstadoLicenciaVM estadoLicencia { get; set; }
        public NovedadVM novedad { get; set; }
        public string fechaLicenciaParseada { get; set; }
        public string horaLicenciaParseada { get; set; }
        public string fechaFinLicenciaParseada { get; set; }
        public string horaFinLicenciaParseada { get; set; }
        public string fechaParseada { get; set; }
        public DateTime? fechaFin { get; set; }
        public DateTime horaFin { get; set; }
        public TipoLicenciaVM tipoLicencia { get; set; }
        public UsuarioVM usuario { get; set; }
        public int? ultimoNumeroAutorizacion { get; set; }
        public string nombreEntrega { get; set; }
    }
}
