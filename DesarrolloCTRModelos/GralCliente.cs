//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DesarrolloCTRModelos
{
    using System;
    using System.Collections.Generic;
    
    public partial class GralCliente
    {
        public int idCliente { get; set; }
        public string nombreCliente { get; set; }
        public Nullable<int> idSector { get; set; }
        public string codigoTransmittal { get; set; }
        public Nullable<int> nroTransmittal { get; set; }
        public Nullable<int> diaGracia { get; set; }
        public string codigoTrCliente { get; set; }
        public Nullable<int> nroTransmittalCliente { get; set; }
        public string notificarCalificacion { get; set; }
    }
}
