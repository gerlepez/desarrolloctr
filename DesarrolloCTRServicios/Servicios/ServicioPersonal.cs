﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioPersonal
    {
        public UsuarioVM ObtenerDatosUsuarioPorId(int idUsuario)
        {

            //var user = nombreUsuario.Remove(0, 10);
            using (BuhoGestionEntities buhodb = new BuhoGestionEntities())
            {
                UsuarioVM usuario = new UsuarioVM();
                if (idUsuario != null)
                {
                    usuario = (from c in buhodb.GralUsuario
                               where c.activo == 1 && c.idUsuario == idUsuario
                               select new UsuarioVM
                               {
                                   idUsuario = c.idUsuario,
                                   nombreUsuario = c.Nombre,

                               }).FirstOrDefault();
                    //aca debo controlar lo que se hace si no encuentro usuario en la base
                    if (usuario == null)
                    {
                        usuario = new UsuarioVM();
                        usuario.nombreUsuario = "Usuario no existente";
                        usuario.idUsuario = idUsuario;

                    }

                }
                return usuario;
            }

        }
    }
}
