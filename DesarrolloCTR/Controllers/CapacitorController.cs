﻿using DesarrolloCTR.Controllers.Extra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers
{
    [SessionUtilityAttribute]
    [Permiso("CAP")]
    public class CapacitorController : Controller
    {

        public ActionResult Capacitor()
        {
            return View();
        }

        public ActionResult AdministrarCapacitor()
        {
            return View();
        }
    }
}