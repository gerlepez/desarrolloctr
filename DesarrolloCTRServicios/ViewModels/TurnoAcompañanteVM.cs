﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class TurnoAcompañanteVM
    {
        public int idTurnoAcompañante { get; set; }
        public int idTurnoEjecucion { get; set; }
        public int idAcompañante { get; set; }
        public bool activo { get; set; }
    }
}
