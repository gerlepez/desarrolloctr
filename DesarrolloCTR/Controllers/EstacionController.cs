﻿using DesarrolloCTR.Controllers.Extra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers
{
    [SessionUtilityAttribute]
    [Permiso("EST")]
    public class EstacionController : Controller
    {
        public ActionResult Estacion()
        {
            return View();
        }

        public ActionResult AdministrarEstacion()
        {
            return View();
        }
    }
}
