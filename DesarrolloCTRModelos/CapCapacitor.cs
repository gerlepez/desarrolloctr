//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DesarrolloCTRModelos
{
    using System;
    using System.Collections.Generic;
    
    public partial class CapCapacitor
    {
        public int idCapacitor { get; set; }
        public string nombreCapacitor { get; set; }
        public Nullable<int> idEstacion { get; set; }
        public Nullable<int> potenciaMVAR { get; set; }
        public Nullable<bool> activo { get; set; }
        public Nullable<int> idPagotran { get; set; }
        public string codigoCapacitor { get; set; }
        public Nullable<int> idBde { get; set; }
        public string codigoSap { get; set; }
        public Nullable<int> IdTipoEquipo { get; set; }
        public Nullable<bool> tieneAutomatismo { get; set; }
        public Nullable<decimal> TensionKV { get; set; }
        public Nullable<System.DateTime> fechaCreacion { get; set; }
        public Nullable<System.DateTime> fechaEdicion { get; set; }
        public Nullable<bool> remunerado { get; set; }
    }
}
