﻿using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers.Api
{
    public class RolApiController : ApiController
    {
        [System.Web.Http.Route("Api/Rol/ObtenerRoles")]
        [System.Web.Http.ActionName("ObtenerRoles")]
        [System.Web.Http.HttpGet]
        public object ObtenerRoles(bool activo)
        {
            ServicioRol servicio = new ServicioRol();

            object lista = servicio.ObtenerRoles(activo);

            // ((IDisposable)servEstacion).Dispose();

            return lista;
        }

        [System.Web.Http.Route("Api/Rol/ObtenerRolesSelect")]
        [System.Web.Http.ActionName("ObtenerRolesSelect")]
        [System.Web.Http.HttpGet]
        public object ObtenerRolesSelect(bool activo)
        {
            ServicioRol servicio = new ServicioRol();

            object lista = servicio.ObtenerRolesSelect(activo);

            // ((IDisposable)servEstacion).Dispose();

            return lista;
        }


        [System.Web.Http.Route("Api/Rol/ConsultarRol")]
        [System.Web.Http.ActionName("ConsultarRol")]
        [System.Web.Http.HttpGet]
        public RolVM ConsultarRol(int idRol)
        {
            ServicioRol servicio = new ServicioRol();

            RolVM rol = servicio.ConsultarRol(idRol);

            return rol;
        }

        [System.Web.Http.Route("Api/Rol/ModificarRol")]
        [System.Web.Http.ActionName("ModificarRol")]
        [System.Web.Http.HttpPost]
        public int ModificarRol(RolVM rol)
        {
            ServicioRol servicio = new ServicioRol();
            int codigo;
            codigo = servicio.ModificarRol(rol);
            return codigo;
        }

        [System.Web.Http.Route("Api/Rol/AltaRol")]
        [System.Web.Http.ActionName("AltaRol")]
        [System.Web.Http.HttpPost]
        public int AltaRol(RolVM rol)
        {
            ServicioRol servicio = new ServicioRol();
            int codigo;
            codigo = servicio.AltaRol(rol);

            return codigo;
        }


        [System.Web.Http.Route("Api/Rol/EliminarAccesoRol")]
        [System.Web.Http.ActionName("EliminarAccesoRol")]
        [System.Web.Http.HttpPost]
        public int EliminarAccesoRol(RolVM rol)
        {
            ServicioRol servicio = new ServicioRol();
            int codigo = servicio.EliminarRol(rol);
            return codigo;
        }

        [System.Web.Http.Route("Api/Rol/RecuperarAccesoRol")]
        [System.Web.Http.ActionName("RecuperarAccesoRol")]
        [System.Web.Http.HttpPost]
        public int RecuperarAccesoRol(RolVM rol)
        {
            ServicioRol servicio = new ServicioRol();
            int codigo = servicio.RecuperarRol(rol);
            return codigo;
        }

        /*
        [System.Web.Http.Route("Api/Rol/CrudRol")]
        [System.Web.Http.ActionName("CrudRol")]
        [System.Web.Http.HttpPost]
        public int CrudRol(RolVM rol, string nomUsuario, int tipo)
        {
            
            var idUsuarioLogueado = 0;
            int codigo = 400;
            if (rol != null)
            {
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(nomUsuario);
                ServicioRol servicio = new ServicioRol();
                codigo = servicio.CrudRol(rol, idUsuarioLogueado, tipo);

            }


            return codigo;
        }*/

        [System.Web.Http.Route("Api/Rol/ObtenerMenu")]
        [System.Web.Http.ActionName("ObtenerMenu")]
        [System.Web.Http.HttpGet]
        public List<MenuVM> ObtenerMenu(string nombreUsuario)
        {

            var idUsuarioLogueado = 0;
            List<MenuVM> listMenu = new List<MenuVM>();

            if (nombreUsuario != null)
            {
                ServicioRol servRol = new ServicioRol();
                listMenu = servRol.ObtenerMenu(nombreUsuario);

            }


            return listMenu;
        }


        /*
        [System.Web.Http.Route("Api/Rol/ObtenerAccesosRol")]
        [System.Web.Http.ActionName("ObtenerAccesosRol")]
        [System.Web.Http.HttpGet]
        public List<RolVM> ObtenerAccesosRol(int idRol)
        {
            ServicioRol servicio = new ServicioRol();
            List<RolVM> lista = servicio.ObtenerAccesosRol(idRol);
            return lista;
            
        }*/



        [System.Web.Http.Route("Api/Rol/ObtenerListaMenu")]
        [System.Web.Http.ActionName("ObtenerListaMenu")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaMenu()
        {
            ServicioRol servicio = new ServicioRol();

            object lista = servicio.ObtenerListaMenu();
            return lista;
        }

        //  ROLES USUARIOS  


        [System.Web.Http.Route("Api/Rol/ObtenerPersonasRoles")]
        [System.Web.Http.ActionName("ObtenerPersonasRoles")]
        [System.Web.Http.HttpGet]
        public object ObtenerPersonasRoles(bool activo)
        {
            ServicioRoles servicioRoles = new ServicioRoles();
            object lista = servicioRoles.ObtenerPersonasRoles(activo);
            return lista;
        }

        [System.Web.Http.Route("Api/Rol/ObtenerRoles")]
        [System.Web.Http.ActionName("ObtenerRoles")]
        [System.Web.Http.HttpGet]
        public List<RolVM> ObtenerRoles(int idRol)
        {
            ServicioRoles servicioRoles = new ServicioRoles();
            List<RolVM> lista = servicioRoles.ObtenerRoles(idRol);
            return lista;
        }


        [System.Web.Http.Route("Api/Rol/AltaUsuarioRoles")]
        [System.Web.Http.ActionName("AltaUsuarioRoles")]
        [System.Web.Http.HttpPost]
        public int AltaRoles(UsuarioRolVM rol)
        {
            ServicioRoles servicioRoles = new ServicioRoles();
            int codigo;
            codigo = servicioRoles.AltaRoles(rol);

            return codigo;
        }


        [System.Web.Http.Route("Api/Rol/ConsultarUnRol")]
        [System.Web.Http.ActionName("ConsultarUnRol")]
        [System.Web.Http.HttpGet]
        public UsuarioRolVM ConsultarUnRol(int idUsuarioRol)
        {
            ServicioRoles servicioRoles = new ServicioRoles();
            UsuarioRolVM UsuarioRol = servicioRoles.ConsultarUnRol(idUsuarioRol);
            return UsuarioRol;
        }

        [System.Web.Http.Route("Api/Rol/ModificarUsuarioRol")]
        [System.Web.Http.ActionName("ModificarUsuarioRol")]
        [System.Web.Http.HttpPost]
        public int ModificarRol(UsuarioRolVM rol)
        {
            ServicioRoles servicioRoles = new ServicioRoles();
            int codigo;
            codigo = servicioRoles.ModificarRol(rol);
            return codigo;
        }

        [System.Web.Http.Route("Api/Rol/EliminarUsuarioRol")]
        [System.Web.Http.ActionName("EliminarUsuarioRol")]
        [System.Web.Http.HttpPost]
        public int EliminarRol(UsuarioRolVM rol)
        {
            ServicioRoles servicioRoles = new ServicioRoles();
            int codigo = servicioRoles.EliminarRol(rol);
            return codigo;
        }

        [System.Web.Http.Route("Api/Rol/RecuperarUsuarioRol")]
        [System.Web.Http.ActionName("RecuperarUsuarioRol")]
        [System.Web.Http.HttpPost]
        public int RecuperarRol(UsuarioRolVM rol)
        {
            ServicioRoles servicioRoles = new ServicioRoles();
            int codigo = servicioRoles.RecuperarRol(rol);
            return codigo;

        }


        [System.Web.Http.Route("Api/Rol/ObtenerListaUsuarioCTR")]
        [System.Web.Http.ActionName("ObtenerListaUsuarioCTR")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaUsuarioCTR()
        {
            ServicioRoles servicioRoles = new ServicioRoles();
            object lista = servicioRoles.ObtenerListaUsuarioCTR();
            return lista;
        }

        [System.Web.Http.Route("Api/Rol/ObtenerListaUsuarioCTREditar")]
        [System.Web.Http.ActionName("ObtenerListaUsuarioCTREditar")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaUsuarioCTREditar()
        {
            ServicioRoles servicioRoles = new ServicioRoles();
            object lista = servicioRoles.ObtenerListaUsuarioCTREditar();
            return lista;
        }

    }

}