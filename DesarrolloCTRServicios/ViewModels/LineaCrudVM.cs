﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesarrolloCTRServicios.ViewModels
{
    public class LineaCrudVM
    {
        public int? idLinea { get; set; }
        public int? idTipoLinea { get; set; }
        public int? idPagotran { get; set; }
        public int? idTension { get; set; }
        public int? idEstacionOrigen { get; set; }
        public int? idEstacionDestino { get; set; }
        public int? nroTerna { get; set; }
        public decimal? kilometraje { get; set; }
        public string codigoLinea { get; set; }
        public string nombreLinea { get; set; }
        public string nombreTipoLinea { get; set; }
        public string TipoLinea { get; set; }
        public string nombresuario { get; set; }
        public string PropiedadLinea { get; set; }
        public string tension { get; set; }
        public string nombreEstacionOrigen { get; set; }
        public string nombreEstacionDestino { get; set; }
        public int? idPropiedad { get; set; }
        public bool? esPropia { get; set; }
        public bool? remunerada { get; set; }

        
    }
}
