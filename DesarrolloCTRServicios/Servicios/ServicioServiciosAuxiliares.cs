﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioServiciosAuxiliares
    {
        private IntranetCTREntities db = new IntranetCTREntities();

        public object ObtenerServiciosAuxiliares(bool activo)
        {
            List<ServicioAuxiliarVM> listaServicioAuxiliar;
            listaServicioAuxiliar = (from sa in db.ServAuxServicioAuxiliar
                                     where sa.activo == activo
                                     select new ServicioAuxiliarVM
                                     {
                                         idServAuxiliar = sa.idServAuxiliar,
                                         nombreServicioAuxiliar = sa.nombreServicioAuxiliar,
                                         idUsarioAlta = sa.idUsuarioAlta,
                                         fechaUsarioAlta = sa.fechaUsuarioAlta,
                                         activo = sa.activo

                                     }).ToList();

            object json = new { data = listaServicioAuxiliar };
            return json;
        }

        public ServicioAuxiliarVM ConsultarUnServicioAuxiliar(int? idServicioAuxiliar)
        {
            ServicioAuxiliarVM servicioAuxiliar = new ServicioAuxiliarVM();
            servicioAuxiliar = (from sa in db.ServAuxServicioAuxiliar
                                where sa.activo == true && sa.idServAuxiliar == idServicioAuxiliar
                                select new ServicioAuxiliarVM
                                {
                                    activo = sa.activo,
                                    fechaUsarioAlta = sa.fechaUsuarioAlta,
                                    nombreEquipo = "Servicio Auxiliar",
                                    fechaUsuarioModifica = sa.fechaUsuarioModifica,
                                    idServAuxiliar = sa.idServAuxiliar,
                                    idUsarioAlta = sa.idUsuarioAlta,
                                    idUsuarioModifica = sa.idUsuarioModifica,
                                    nombreServicioAuxiliar = sa.nombreServicioAuxiliar
                                }).FirstOrDefault();

            return servicioAuxiliar;
        }
        public object ObtenerServiciosAuxiliaresPorLugar(int idLugar)
        {
            List<ServicioAuxiliarVM> servicioAuxiliar;
            servicioAuxiliar = (from sa in db.ServAuxServicioAuxiliar
                                join sae in db.ServAuxServEstacionAutomat on sa.idServAuxiliar equals sae.idServicio
                                join e in db.EstEstacion on sae.idEstacion equals e.idEstacion
                                where sa.activo == true && e.idEstacion == idLugar
                                select new ServicioAuxiliarVM
                                {
                                    activo = sa.activo,
                                    idEquipo = sa.idServAuxiliar,
                                    nombreEquipo = sa.nombreServicioAuxiliar,
                                    fechaUsarioAlta = sa.fechaUsuarioAlta,
                                    fechaUsuarioModifica = sa.fechaUsuarioModifica,
                                    idServAuxiliar = sa.idServAuxiliar,
                                    idUsarioAlta = sa.idUsuarioAlta,
                                    idUsuarioModifica = sa.idUsuarioModifica,
                                    nombreServicioAuxiliar = sa.nombreServicioAuxiliar,
                                    codigoEquipo = "SAUX"
                                }).ToList();
            object json = new { data = servicioAuxiliar };
            return json;
        }

        public int AltaServicioAuxiliar(ServicioAuxiliarVM servicioAuxiliar)
        {

            var consultaServicioAuxiliar = db.ServAuxServicioAuxiliar.Where(x => x.idServAuxiliar == servicioAuxiliar.idServAuxiliar).FirstOrDefault();
            if (consultaServicioAuxiliar == null)
            {
                try
                {
                    ServAuxServicioAuxiliar servAuxiliarNue = new ServAuxServicioAuxiliar
                    {
                        fechaUsuarioAlta = DateTime.Now,
                        idUsuarioAlta = servicioAuxiliar.idUsarioAlta,
                        nombreServicioAuxiliar = servicioAuxiliar.nombreServicioAuxiliar,
                        activo = true,
                    };
                    db.ServAuxServicioAuxiliar.Add(servAuxiliarNue);
                    db.SaveChanges();
                    return 200;
                }
                catch (Exception e)
                {
                    return 404;
                }
            }
            else
            {
                return 300; //ya existe ese dato
            }

        }
        public int ModificarServicioAuxiliar(ServicioAuxiliarVM servicioAuxiliar)
        {

            var consultaUnServicioAuxiliar = db.ServAuxServicioAuxiliar.Where(x => x.idServAuxiliar == servicioAuxiliar.idServAuxiliar).FirstOrDefault();
            if (consultaUnServicioAuxiliar != null)
            {
                consultaUnServicioAuxiliar.nombreServicioAuxiliar = servicioAuxiliar.nombreServicioAuxiliar;
                consultaUnServicioAuxiliar.activo = true;
                consultaUnServicioAuxiliar.fechaUsuarioModifica = DateTime.Now;
                consultaUnServicioAuxiliar.idUsuarioModifica = servicioAuxiliar.idUsuarioModifica;

                db.SaveChanges();
            }

            return 200;
        }

        public int EliminarServicioAuxiliar(ServicioAuxiliarVM servicioAuxiliar)
        {

            var consultaUnServicioAuxiliar = db.ServAuxServicioAuxiliar.Where(x => x.idServAuxiliar == servicioAuxiliar.idServAuxiliar).FirstOrDefault();
            if (consultaUnServicioAuxiliar != null)
            {
                consultaUnServicioAuxiliar.activo = false;
            }
            db.SaveChanges();

            return 200;
        }

        public int RecuperarServicioAuxiliar(ServicioAuxiliarVM servicioAuxiliar)
        {

            var consultaUnServicioAuxiliar = db.ServAuxServicioAuxiliar.Where(x => x.idServAuxiliar == servicioAuxiliar.idServAuxiliar).FirstOrDefault();
            if (consultaUnServicioAuxiliar != null)
            {
                consultaUnServicioAuxiliar.activo = true;
            }
            db.SaveChanges();

            return 200;
        }


    }
}
