﻿using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using DesarrolloCTRUtilidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers.Api
{
    public class IndisponibilidadApiController : ApiController
    {
        // GET: IndisponibilidadApi
        [System.Web.Http.Route("Api/Indisponibilidad/ObtenerPlanillaIndisponibilidad")]
        [System.Web.Http.ActionName("ObtenerPlanillaIndisponibilidad")]
        [System.Web.Http.HttpPost]
        public object ObtenerPlanillaIndisponibilidad(FormDataCollection form, string fechaDesdeSalida, string fechaHastaSalida)
        {
            var draw = form.Get("draw");
            var start = form.Get("start");
            var length = form.Get("length");
            var sortColumn = (form.Get("columns[" + form.Get("order[0][column]").FirstOrDefault() + "][data]").ToString()).ToString();
            var sortColumnDir = form.Get("order[0][dir]").ToString();
            var searchValue = form.Get("search[value]").ToString();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            int cantidadRegistrosPagina = Int32.Parse(length);

            ServicioIndisponibilidad servIndisp = new ServicioIndisponibilidad();

            object listaIndisp = servIndisp.ObtenerPlanillaIndisponibilidad(fechaDesdeSalida, fechaHastaSalida, draw, recordsTotal, skip, pageSize);
            return listaIndisp;

        }


        [System.Web.Http.Route("Api/Indisponibilidad/ObtenerIndisponibilidad")]
        [System.Web.Http.ActionName("ObtenerIndisponibilidad")]
        [System.Web.Http.HttpGet]
        public IndisponibilidadVM ObtenerIndisponibilidad(int idIndis)
        {
            ServicioIndisponibilidad servicio = new ServicioIndisponibilidad();
            IndisponibilidadVM res = servicio.ObtenerIndisponibilidad(idIndis);
            return res;
        }

        [System.Web.Http.Route("Api/Indisponibilidad/GenerarIndisponibilidad")]
        [System.Web.Http.ActionName("GenerarIndisponibilidad")]
        [System.Web.Http.HttpPost]
        public void GenerarIndisponibilidad(IndisponibilidadVM indisponibilidad)
        {


            ServicioIndisponibilidad servIndisp = new ServicioIndisponibilidad();

            servIndisp.GenerarIndisponibilidad(indisponibilidad);

        }

        [System.Web.Http.Route("Api/Indisponibilidad/EliminarIndisponibilidad")]
        [System.Web.Http.ActionName("EliminarIndisponibilidad")]
        [System.Web.Http.HttpPost]
        public void EliminarIndisponibilidad(int idNovedadIndisponibilidad)
        {
            ServicioIndisponibilidad servIndisp = new ServicioIndisponibilidad();
            servIndisp.EliminarIndisponibilidad(idNovedadIndisponibilidad);
        }


        [System.Web.Http.Route("Api/Indisponibilidad/GenerarReporteExcel")]
        [System.Web.Http.ActionName("GenerarReporteExcel")]
        [System.Web.Http.HttpGet]
        public HojaExcelVM GenerarReporteExcel(string fechaDesdeSalida, string fechaHastaSalida)
        {

            ServicioIndisponibilidad servIndisp = new ServicioIndisponibilidad();
            var result = servIndisp.GenerarReporteExcel(fechaDesdeSalida, fechaHastaSalida);
            return result;

        }
        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("Api/Indisponibilidad/GetFile")]
        [System.Web.Http.ActionName("GetFile")]
        public HttpResponseMessage GetFile(string fileUrl, string fileName)
        {

            ArchivoApiController apiArchivo = new ArchivoApiController();
            return apiArchivo.GetFile(fileUrl, "Novedad", fileName);

        }
    }
}