﻿using DesarrolloCTR.Controllers.Extra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers
{
    public class TurnoController : Controller
    {
        // GET: Turno
        [SessionUtilityAttribute]
        [Permiso("TURN")]
        public ActionResult Turno()
        {
            return View();
        }

        [Permiso("ADMTURN")]
        public ActionResult AdministrarTurno()
        {
            return View();
        }


        public ActionResult CerrarTurno()
        {
            return View();
        }
        public ActionResult CargaMasiva()
        {
            return View();
        }
    }
}