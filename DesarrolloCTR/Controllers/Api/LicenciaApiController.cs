﻿using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DesarrolloCTR.Controllers.Api
{
    public class LicenciaApiController : ApiController
    {
        [System.Web.Http.Route("Api/Licencia/ObtenerListaLicencia")]
        [System.Web.Http.ActionName("ObtenerListaLicencia")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaLicencia(bool activo)
        {
            ServicioLicencia servLicencia = new ServicioLicencia();

            object listaLicencia = servLicencia.ObtenerListaLicencia(activo);

            return listaLicencia;
        }

        [System.Web.Http.Route("Api/Licencia/CompletarLicencia")]
        [System.Web.Http.ActionName("CompletarLicencia")]
        [System.Web.Http.HttpPost]

        public LicenciaVM CompletarLicencia(LicenciaVM licencia)
        {
            var idUsuarioLogueado = 0;
            if (licencia != null)
            {
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(licencia.nombresuario);
            }

            ServicioLicencia servicioLicencia = new ServicioLicencia();

            LicenciaVM response = new LicenciaVM();

            response = servicioLicencia.CompletarLicencia(licencia, idUsuarioLogueado);

            return response;
        }

        [System.Web.Http.Route("Api/Licencia/ConsultarRutesAbiertos")]
        [System.Web.Http.ActionName("ConsultarRutesAbiertos")]
        [System.Web.Http.HttpGet]
        public object ConsultarRutesAbiertos(string nombreUsuario)
        {
            ServicioUsuario servicioUsuario = new ServicioUsuario();
            var idUsuarioLogueado = servicioUsuario.ObtenerUsuarioLogueado(nombreUsuario);
            ServicioLicencia servicioLicencia = new ServicioLicencia();
            object response = servicioLicencia.ConsultarRutesAbiertos(idUsuarioLogueado);

            return response;

        }


        [System.Web.Http.Route("Api/Licencia/MostrarRutesAbiertos")]
        [System.Web.Http.ActionName("MostrarRutesAbiertos")]
        [System.Web.Http.HttpGet]
        public object MostrarRutesAbiertos()
        {
            ServicioLicencia servicioLicencia = new ServicioLicencia();
            object response = servicioLicencia.MostrarRutesAbiertos();

            return response;

        }

        [System.Web.Http.Route("Api/Licencia/AltaLicencia")]
        [System.Web.Http.ActionName("AltaLicencia")]
        [System.Web.Http.HttpPost]
        public LicenciaVM AltaLicencia(LicenciaVM licencia)
        {
            var idUsuarioLogueado = 0;
            if (licencia != null)
            {
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(licencia.nombresuario);
            }

            ServicioLicencia servLicencia = new ServicioLicencia();
            LicenciaVM codigo = new LicenciaVM();
            if (servLicencia.ValidarLicencia(licencia))
            {
                codigo = servLicencia.AltaLicencia(licencia, idUsuarioLogueado);
            }
            else
            {
                codigo.codigoEstadoAlta = 400;
            }

            return codigo;
        }

        [System.Web.Http.Route("Api/Licencia/ModificarLicencia")]
        [System.Web.Http.ActionName("ModificarLicencia")]
        [System.Web.Http.HttpPost]
        public LicenciaVM ModificarLicencia(LicenciaVM licencia)
        {
            var idUsuarioLogueado = 0;
            //LicenciaVM licencia = new LicenciaVM();
            if (licencia != null)
            {
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(licencia.nombresuario);
            }
            ServicioLicencia servLicencia = new ServicioLicencia();
            int codigo;
            if (servLicencia.ValidarLicencia(licencia))
            {
                var lic = servLicencia.ModificarLicencia(licencia, idUsuarioLogueado);
                licencia.codigoEstadoModif = lic.codigoEstadoModif;
                licencia.codigoLicencia = lic.codigoLicencia;
            }
            else
            {
                licencia.codigoEstadoModif = 400;
            }
            return licencia;
        }

        [System.Web.Http.Route("Api/Licencia/EliminarLicencia")]
        [System.Web.Http.ActionName("EliminarLicencia")]
        [System.Web.Http.HttpPost]
        public int EliminarLicencia(LicenciaVM licencia)
        {
            ServicioLicencia servLicencia = new ServicioLicencia();
            int codigo = servLicencia.EliminarLicencia(licencia);

            return codigo;
        }

        [System.Web.Http.Route("Api/Licencia/CerrarLicencia")]
        [System.Web.Http.ActionName("CerrarLicencia")]
        [System.Web.Http.HttpPost]
        public object CerrarLicencia(LicenciaVM licencia)
        {
            ServicioLicencia servLicencia = new ServicioLicencia();
            var codigo = servLicencia.CerrarLicencia(licencia);

            return codigo;
        }

        [System.Web.Http.Route("Api/Licencia/ObtenerListaTipoLicencia")]
        [System.Web.Http.ActionName("ObtenerListaTipoLicencia")]
        [System.Web.Http.HttpGet]
        public List<TipoLicenciaVM> ObtenerListaTipoLicencia()
        {
            ServicioLicencia servLicencia = new ServicioLicencia();
            var TipoLicencia = servLicencia.ObtenerListaTipoLicencia();

            return TipoLicencia;
        }

        [System.Web.Http.Route("Api/Licencia/ConsultarUnTipoLicencia")]
        [System.Web.Http.ActionName("ConsultarUnTipoLicencia")]
        [System.Web.Http.HttpGet]
        public TipoLicenciaVM ConsultarUnTipoLicencia(int idTipoLicencia)
        {
            ServicioLicencia servLicencia = new ServicioLicencia();
            var TipoLicencia = servLicencia.ConsultarUnTipoLicencia(idTipoLicencia);

            return TipoLicencia;
        }

        [System.Web.Http.Route("Api/Licencia/ConsultarUnaLicencia")]
        [System.Web.Http.ActionName("ConsultarUnaLicencia")]
        [System.Web.Http.HttpGet]
        public LicenciaVM ConsultarUnaLicencia(int idLicencia)
        {
            ServicioLicencia servLicencia = new ServicioLicencia();
            LicenciaVM licencia = servLicencia.ConsultarUnaLicencia(idLicencia);

            return licencia;
        }


        [System.Web.Http.Route("Api/Licencia/ConsultLicencia")]
        [System.Web.Http.ActionName("ConsultLicencia")]
        [System.Web.Http.HttpGet]
        public LicenciaVM ConsultLicencia(int idLicencia)
        {
            ServicioLicencia servLicencia = new ServicioLicencia();
            LicenciaVM licencia = servLicencia.ConsultLicencia(idLicencia);

            return licencia;
        }

        [System.Web.Http.Route("Api/Licencia/ConsultaLicenciaParaAutTrabajo")]
        [System.Web.Http.ActionName("ConsultaLicenciaParaAutTrabajo")]
        [System.Web.Http.HttpGet]
        public NovedadVM ConsultaLicenciaParaAutTrabajo(int idLicencia)
        {
            ServicioLicencia servLicencia = new ServicioLicencia();
            NovedadVM novedad = servLicencia.ConsultaLicenciaParaAutTrabajo(idLicencia);

            return novedad;
        }



        [System.Web.Http.Route("Api/Licencia/RecuperarLicencia")]
        [System.Web.Http.ActionName("RecuperarLicencia")]
        [System.Web.Http.HttpPost]
        public int RecuperarLicencia(LicenciaVM licencia)
        {
            ServicioLicencia servLicencia = new ServicioLicencia();
            int codigo = servLicencia.RecuperarLicencia(licencia);

            return codigo;
        }

        [System.Web.Http.Route("Api/Licencia/ObtenerListaAutorizacionTrabajo")]
        [System.Web.Http.ActionName("ObtenerListaAutorizacionTrabajo")]
        [System.Web.Http.HttpGet]
        public List<AutorizacionTrabajoVM> ObtenerListaAutorizacionTrabajo(int? idLicencia)
        {
            ServicioAutorizacionTrabajo servAutTrabajo = new ServicioAutorizacionTrabajo();
            List<AutorizacionTrabajoVM> listaAutorizacionTrabajo = servAutTrabajo.ObtenerListaAutorizacionTrabajo(idLicencia);
            return listaAutorizacionTrabajo;
        }

        [System.Web.Http.Route("Api/Licencia/ConsultarUnaAutorizacionTrabajo")]
        [System.Web.Http.ActionName("ConsultarUnaAutorizacionTrabajo")]
        [System.Web.Http.HttpGet]
        public AutorizacionTrabajoVM ConsultarUnaAutorizacionTrabajo(int idAutorizacionTrabajo)
        {
            ServicioAutorizacionTrabajo servAutTrabajo = new ServicioAutorizacionTrabajo();
            AutorizacionTrabajoVM autorizacionTrabajo = servAutTrabajo.ConsultarUnaAutorizacionTrabajo(idAutorizacionTrabajo);
            return autorizacionTrabajo;
        }

        [System.Web.Http.Route("Api/Licencia/ConsultarUltimoCodigoLicencia")]
        [System.Web.Http.ActionName("ConsultarUltimoCodigoLicencia")]
        [System.Web.Http.HttpGet]
        public LicenciaVM ConsultarUltimoCodigoLicencia()
        {
            ServicioLicencia servicio = new ServicioLicencia();
            LicenciaVM licencia = servicio.ConsultarUltimoCodigoLicencia();
            return licencia;
        }

        [System.Web.Http.Route("Api/Licencia/GenerarLicencia")]
        [System.Web.Http.ActionName("GenerarLicencia")]
        [System.Web.Http.HttpGet]
        public LicenciaVM GenerarLicencia(string nombreUsuario)
        {
            ServicioUsuario serUsuario = new ServicioUsuario();
            int idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(nombreUsuario);
            ServicioLicencia servicio = new ServicioLicencia();
            LicenciaVM licencia = servicio.GenerarLicencia(idUsuarioLogueado);
            return licencia;
        }

        [System.Web.Http.Route("Api/Licencia/GuardarDescripcion")]
        [System.Web.Http.ActionName("GuardarDescripcion")]
        [System.Web.Http.HttpPost]

        public int? GuardarDescripcion(string codigoLicencia, string descripcion = " ")
        {
            var response = 0;
            ServicioLicencia servicioLicencia = new ServicioLicencia();

            response = servicioLicencia.GuardarDescripcion(codigoLicencia, descripcion);
            return response;
        }


        [System.Web.Http.Route("Api/Licencia/AltaAutorizacionTrabajo")]
        [System.Web.Http.ActionName("AltaAutorizacionTrabajo")]
        [System.Web.Http.HttpPost]
        public int? AltaAutorizacionTrabajo(LicenciaVM licencia)
        {

            var idUsuarioLogueado = 0;
            int? codigo = 400;
            AutorizacionTrabajoVM autorizacionAlta = new AutorizacionTrabajoVM();

            ServicioAutorizacionTrabajo servAutTrabajo = new ServicioAutorizacionTrabajo();
            if (licencia.autorizacionTrabajo.Count > 0)
            {
                foreach (var autorizacion in licencia.autorizacionTrabajo)
                {
                    if (autorizacion != null)
                    {
                        ServicioUsuario serUsuario = new ServicioUsuario();
                        idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(autorizacion.nombresuario);
                    }

                    if (servAutTrabajo.ValidarAutorizacionTrabajo(autorizacion))
                    {
                        try
                        {
                            autorizacion.idLicencia = autorizacion.idLicencia;
                            autorizacionAlta = servAutTrabajo.AltaAutorizacionTrabajo(autorizacion, idUsuarioLogueado);
                            codigo = autorizacionAlta.codigoResultadoAlta;
                        }
                        catch (Exception e)
                        {

                            return 400;
                        }

                    }
                    else
                    {
                        return 400;
                    }

                }
            }


            return codigo;
        }

        [System.Web.Http.Route("Api/Licencia/ModificarAutorizacionTrabajo")]
        [System.Web.Http.ActionName("ModificarAutorizacionTrabajo")]
        [System.Web.Http.HttpPost]
        public int? ModificarAutorizacionTrabajo(LicenciaVM licencia)
        {
            var idUsuarioLogueado = 0;
            int? codigo = 400;
            AutorizacionTrabajoVM autorizacionAlta = new AutorizacionTrabajoVM();
            ServicioAutorizacionTrabajo servAutTrabajo = new ServicioAutorizacionTrabajo();
            if (licencia.autorizacionTrabajo != null)
            {
                foreach (var item in ObtenerListaAutorizacionTrabajo(licencia.autorizacionTrabajo[0].idLicencia))
                {
                    if (item.estadoAutorizacion != null)
                    {
                        if (item.estadoAutorizacion.idEstadoAutorizacion == 1)
                        {
                            servAutTrabajo.EliminarAutorizacionTrabajo(item);
                        }
                    }
                }
                ServicioNovedad servNovedad = new ServicioNovedad();
                var novedad = servNovedad.ConsultarUnaNovedad(licencia.autorizacionTrabajo[0].idNovedad, true);

                foreach (var autorizacion in licencia.autorizacionTrabajo)
                {
                    if (autorizacion != null)
                    {
                        ServicioUsuario serUsuario = new ServicioUsuario();
                        idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(autorizacion.nombresuario);
                    }

                    if (autorizacion.estadoAutorizacion != null)
                    {
                        if (autorizacion.estadoAutorizacion.idEstadoAutorizacion != 2)
                        {
                            if (servAutTrabajo.ValidarAutorizacionTrabajo(autorizacion))
                            {
                                try
                                {

                                    autorizacion.idLicencia = autorizacion.idLicencia;
                                    autorizacionAlta = servAutTrabajo.ModificarAutorizacionTrabajo(autorizacion, idUsuarioLogueado);
                                    codigo = autorizacionAlta.codigoResultadoAlta;

                                }
                                catch (Exception e)
                                {
                                    var a = e;
                                    return 400;
                                }
                            }
                            else
                            {
                                return 400;
                            }
                        }

                    }
                    else
                    {
                        if (servAutTrabajo.ValidarAutorizacionTrabajo(autorizacion))
                        {
                            try
                            {

                                //autorizacion.idLicencia = novedad.licencia.idLicencia;
                                autorizacionAlta = servAutTrabajo.AltaAutorizacionTrabajo(autorizacion, idUsuarioLogueado);
                                codigo = autorizacionAlta.codigoResultadoAlta;

                            }
                            catch (Exception e)
                            {
                                var a = e;
                                return 400;
                            }
                        }
                        else
                        {
                            return 400;
                        }
                    }

                }
            }


            return codigo;
        }


        [System.Web.Http.Route("Api/Licencia/EliminarAutorizacionTrabajo")]
        [System.Web.Http.ActionName("EliminarAutorizacionTrabajo")]
        [System.Web.Http.HttpPost]
        public int EliminarAutorizacionTrabajo(AutorizacionTrabajoVM autorizacionTrabajo)
        {
            ServicioAutorizacionTrabajo servAutTrabajo = new ServicioAutorizacionTrabajo();
            int codigo = servAutTrabajo.RecuperarAutorizacionTrabajo(autorizacionTrabajo);
            return codigo;
        }

        [System.Web.Http.Route("Api/Licencia/RecuperarAutorizacionTrabajo")]
        [System.Web.Http.ActionName("RecuperarAutorizacionTrabajo")]
        [System.Web.Http.HttpPost]
        public int RecuperarAutorizacionTrabajo(AutorizacionTrabajoVM autorizacionTrabajo)
        {
            ServicioAutorizacionTrabajo servAutTrabajo = new ServicioAutorizacionTrabajo();
            int codigo = servAutTrabajo.RecuperarAutorizacionTrabajo(autorizacionTrabajo);
            return codigo;
        }

        [System.Web.Http.Route("Api/Licencia/CerrarAutorizacion")]
        [System.Web.Http.ActionName("CerrarAutorizacion")]
        [System.Web.Http.HttpPost]
        public int? CerrarAutorizacion(AutorizacionTrabajoVM aut)
        {
            ServicioAutorizacionTrabajo servLicencia = new ServicioAutorizacionTrabajo();
            var cierreAutorizacion = servLicencia.CerrarAutorizacion(aut);
            int? codigo = cierreAutorizacion.codResultadoCierre;

            return codigo;
        }

    }
}
