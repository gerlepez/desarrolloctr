﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class PuntoConexionVM
    {
        public int idPuntoConexion { get; set; }
        public int idEquipo { get; set; }
        public int? idGeneral { get; set; }
        public int? idPagotran { get; set; }

        public string nombrePuntoConexion { get; set; }
        public string nombreEquipo { get; set; }
        public string descripcion { get; set; }
        public string codigoPuntoConexion { get; set; }
        public string codigoEquipo { get; set; }
        public string esRemunerado { get; set; }

        public bool? activo { get; set; }
        public bool? esAutomatismo { get; set; }
        public bool? remunerado { get; set; }

        public TensionVM tension { get; set; }
        public EstacionVM estacion { get; set; }



    }
}
