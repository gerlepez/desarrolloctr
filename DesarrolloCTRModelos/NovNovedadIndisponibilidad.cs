//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DesarrolloCTRModelos
{
    using System;
    using System.Collections.Generic;
    
    public partial class NovNovedadIndisponibilidad
    {
        public int idNovedadIndisponibilidad { get; set; }
        public Nullable<int> numNovedad { get; set; }
        public Nullable<int> idEquipo { get; set; }
        public Nullable<int> idTipoEquipo { get; set; }
        public Nullable<int> idTipoIndisponibilidad { get; set; }
        public Nullable<bool> activo { get; set; }
        public Nullable<double> PorcentajeIndisponible { get; set; }
        public Nullable<System.DateTime> FechaEntrada { get; set; }
        public Nullable<System.DateTime> FechaSalida { get; set; }
        public Nullable<bool> EnergiaNoSuministrada { get; set; }
        public string descripcion { get; set; }
        public Nullable<int> itn { get; set; }
        public Nullable<bool> fueraDeServicio { get; set; }
        public Nullable<int> idIndisponibilidadRemu { get; set; }
    }
}
