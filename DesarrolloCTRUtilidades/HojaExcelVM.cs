﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesarrolloCTRUtilidades
{
    public class HojaExcelVM
    {
        //Generico
        public List<string> encabezado { get; set; }
        public string encabezadoPrimero { get; set; }
        public string encabezadoMonto { get; set; }
        public DetalleExcelVM unaFila { get; set; }
        public DetalleExcelVM unaFilaMonto { get; set; }
        public List<DetalleExcelVM> listaFilas { get; set; }
        public string filePath { get; set; }
        public string fileName { get; set; }

        public DetalleExcelVM totales { get; set; }

        //Lineas
        public List<string> encabezadoLinea { get; set; }
        public DetalleExcelVM unaFilaLinea { get; set; }
        public DetalleExcelVM lineas { get; set; }
        public DetalleExcelVM montoLineas { get; set; }
        public DetalleExcelVM totalesLineas { get; set; }
        public List<DetalleExcelVM> listaFilasLinea { get; set; }
        public DetalleExcelVM unaFilaMontoLinea { get; set; }

        //Trafos
        public List<string> encabezadoTrafo { get; set; }
        public DetalleExcelVM unaFilaTrafo { get; set; }
        public DetalleExcelVM trafos { get; set; }
        public DetalleExcelVM montoTrafo { get; set; }
        public DetalleExcelVM totalesTrafo { get; set; }
        public List<DetalleExcelVM> listaFilasTrafo { get; set; }
        public DetalleExcelVM unaFilaMontoTrafo { get; set; }

        //PtosC
        public List<string> encabezadoPtoC { get; set; }
        public DetalleExcelVM unaFilaPtoC { get; set; }
        public DetalleExcelVM PtoC { get; set; }
        public DetalleExcelVM montoPtoC { get; set; }
        public DetalleExcelVM totalesPtoC { get; set; }
        public List<DetalleExcelVM> listaFilasPtoC { get; set; }
        public DetalleExcelVM unaFilaMontoPtoC { get; set; }

        //Capacitores
        public List<string> encabezadoCapacitor { get; set; }
        public DetalleExcelVM unaFilaCapacitor { get; set; }
        public DetalleExcelVM Capacitor { get; set; }
        public DetalleExcelVM montoCapacitor { get; set; }
        public DetalleExcelVM totalesCapacitores { get; set; }
        public List<DetalleExcelVM> listaFilasCapacitor { get; set; }
        public DetalleExcelVM unaFilaMontoCapacitor { get; set; }

        //MES
        public List<string> encabezadoMes { get; set; }
        public DetalleExcelVM unaFilaMes { get; set; }
        public DetalleExcelVM meses { get; set; }
        public List<DetalleExcelVM> listaFilasMes { get; set; }


        //EQUIPOS REMUNERADOS //
        //LINEAS//
        public List<string> encabezadoLineaRemu { get; set; }
        public List<DetalleExcelVM> listaFilasLineaRemu { get; set; }
        public DetalleExcelVM unaFilaLineaRemu { get; set; }
        public DetalleExcelVM totalesLineaRemu { get; set; }
        public DetalleExcelVM unaFilaMontoLineaRemu { get; set; }

        //TRAFOS
        public List<string> encabezadoTrafosRemu { get; set; }
        public List<DetalleExcelVM> listaFilasTrafosRemu { get; set; }
        public DetalleExcelVM unaFilaTrafosRemu { get; set; }
        public DetalleExcelVM totalesTrafosRemu { get; set; }
        public DetalleExcelVM unaFilaMontoTrafosRemu { get; set; }

        //PTOS C//
        public List<string> encabezadoPtosCRemu { get; set; }
        public List<DetalleExcelVM> listaFilasPtosCRemu { get; set; }
        public DetalleExcelVM unaFilaPtosCRemu { get; set; }
        public DetalleExcelVM totalesPtosCRemu { get; set; }
        public DetalleExcelVM unaFilaMontoPtosCRemu { get; set; }

        //Capacitor//
        public List<string> encabezadoCapRemu { get; set; }
        public List<DetalleExcelVM> listaFilasCapCRemu { get; set; }
        public DetalleExcelVM unaFilaCapRemu { get; set; }
        public DetalleExcelVM totalesCapRemu { get; set; }
        public DetalleExcelVM unaFilaMontoCapRemu { get; set; }

    }
    public class TurnoPdfVM
    {
        public int? idTurno { get; set; }
        public DateTime? fechaTurno { get; set; }
        public string nombreJefeTurno { get; set; }
        public List<string> nombreAcompañantes { get; set; }
        public string horaAperturaStr { get; set; }
        public string horaCierreStr { get; set; }
        public string estadoTiempo { get; set; }
        public int idTurnoEjecucion { get; set; }
        public DateTime? fechaHoraApertura { get; set; }
        public DateTime? fechaHoraCierre { get; set; }

        public int? idJefeTurno { get; set; }
        public int? idTipoTurno { get; set; }
        public TimeSpan? horaDesdeTipoTurno { get; set; }
        public TimeSpan? horaHastaTipoTurno { get; set; }
        public List<NovedadPdfVM> novedades { get; set; }
        public string nombreTipoTurno { get; set; }

    }

    public class NovedadPdfVM
    {
        public string nombreEquipo { get; set; }
        public string nombreActuacion { get; set; }
        public string nota { get; set; }
        public string descripcionNovedad { get; set; }
        public int? idTransformador { get; set; }
        public string horaParseada { get; set; }
        public string codNovedad { get; set; }
        public string automatismos { get; set; }
    }
    public class DetalleExcelVM
    {
        public List<string> datoPorFila { get; set; }
        public List<string> datoPorFilaMonto { get; set; }
        public List<string> datoPorFilaTotales { get; set; }

        //Lineas
        public List<string> datoPorFilaTotalesLineas { get; set; }
        public List<string> datoPorFilaLinea { get; set; }
        public List<string> datoPorFilaMontoLinea { get; set; }
       
        //Trafos
        public List<string> datoPorFilaTotalesTrafo { get; set; }
        public List<string> datoPorFilaTrafo { get; set; }
        public List<string> datoPorFilaMontoTrafo { get; set; }

        //PtosC
        public List<string> datoPorFilaTotalesPtosC { get; set; }
        public List<string> datoPorFilaPtosC { get; set; }
        public List<string> datoPorFilaMontoPtosC { get; set; }

        //Capacitor
        public List<string> datoPorFilaTotalesCapacitor { get; set; }
        public List<string> datoPorFilaCapacitor { get; set; }
        public List<string> datoPorFilaMontoCapacitor { get; set; }

        //Mes
        public List<string> datoPorFilaMes { get; set; }

        // EQUIPOS REMUNERADOS //
        //LINEA//
        public List<string> datoPorFilaLineaRemu { get; set; }
        public List<string> datoPorFilaTotalesLineaRemu { get; set; }
        public List<string> datoPorFilaMontoLineaRemu { get; set; }

        //TRAFO//
        public List<string> datoPorFilaTrafoRemu { get; set; }
        public List<string> datoPorFilaTotalesTrafoRemu { get; set; }
        public List<string> datoPorFilaMontoTrafoRemu { get; set; }

        //PTOC//
        public List<string> datoPorFilaPtoCRemu { get; set; }
        public List<string> datoPorFilaTotalesPtoCRemu { get; set; }
        public List<string> datoPorFilaMontoPtoCRemu { get; set; }

        //CAP//
        public List<string> datoPorFilaCapRemu { get; set; }
        public List<string> datoPorFilaTotalesCapRemu { get; set; }
        public List<string> datoPorFilaMontoCapRemu { get; set; }
    }
}
