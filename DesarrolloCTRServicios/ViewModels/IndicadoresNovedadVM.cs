﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class IndicadoresNovedadVM
    {
        public int CantidadNovedad { get; set; }

        public int CantidadNovedadesAbiertas { get; set; }

        public int CantidadNovedadesCerradas { get; set; }

        public int CantidadRUTE { get; set; }


    }
}
