﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioCapacitores
    {

        public object ObtenerCapacitores(bool activo)
        {
            List<CapacitorVM> listabancoCapacitores;

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                listabancoCapacitores = (from c in db.CapCapacitor
                                         join d in db.EstEstacion on c.idEstacion equals d.idEstacion
                                         join e in db.EquiEquipo on c.IdTipoEquipo equals e.idEquipo
                                         where c.activo == activo && c.fechaEdicion == null
                                         select new CapacitorVM
                                         {
                                             idCapacitor = c.idCapacitor,
                                             nombreCapacitor = c.nombreCapacitor,
                                             idPagotran = c.idPagotran,
                                             idBde = c.idBde,
                                             activo = c.activo,
                                             potenciaMVAR = c.potenciaMVAR,
                                             codigoCapacitor = c.codigoCapacitor,
                                             codigoSap = c.codigoSap,
                                             TensionKV = c.TensionKV,
                                             esRemunerado = c.remunerado == true ? "Si" : "No",
                                             estacion = new EstacionVM
                                             {
                                                 idEstacion = d.idEstacion,
                                                 nombreEstacion = d.nombreEstacion,
                                                 activo = d.activo,

                                             },

                                             equipo = new EquipoVM
                                             {
                                                 idEquipo = e.idEquipo,
                                                 nombreEquipo = e.nombreEquipo,
                                                 activo = e.activo
                                             }


                                         }).ToList();
            }
            object json = new { data = listabancoCapacitores };
            return json;
        }

        public object ObtenerCapacitoresPorEstacion(int idEstacion, int idNovedad)
        {
            List<CapacitorVM> listabancoCapacitores;

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                if (idNovedad == 0)
                {
                    listabancoCapacitores = (from c in db.CapCapacitor
                                             join d in db.EstEstacion on c.idEstacion equals d.idEstacion
                                             where c.activo == true && d.idEstacion == idEstacion
                                             select new CapacitorVM
                                             {
                                                 idCapacitor = c.idCapacitor,
                                                 idEquipo = c.idCapacitor,
                                                 nombreEquipo = c.nombreCapacitor,
                                                 nombreCapacitor = c.nombreCapacitor,
                                                 idPagotran = c.idPagotran,
                                                 idBde = c.idBde,
                                                 activo = c.activo,
                                                 potenciaMVAR = c.potenciaMVAR,
                                                 codigoCapacitor = c.codigoCapacitor,
                                                 idGeneral = c.idCapacitor,
                                                 esAutomatismo = c.tieneAutomatismo == null ? false : c.tieneAutomatismo,
                                                 descripcion = c.nombreCapacitor,
                                                 codigoEquipo = "CAP",
                                                 estacion = new EstacionVM
                                                 {
                                                     idEstacion = d.idEstacion,
                                                     nombreEstacion = d.nombreEstacion,
                                                     activo = d.activo,

                                                 },
                                             }).ToList();
                }
                else
                {
                    listabancoCapacitores = (from c in db.CapCapacitor
                                             join d in db.EstEstacion on c.idEstacion equals d.idEstacion
                                             join n in db.NovNovedad on idNovedad equals n.idNovedad
                                             where d.idEstacion == idEstacion && c.fechaCreacion < n.horaNovedad && (c.fechaEdicion != null || c.fechaEdicion > n.horaNovedad)
                                             select new CapacitorVM
                                             {
                                                 idCapacitor = c.idCapacitor,
                                                 idEquipo = c.idCapacitor,
                                                 nombreEquipo = c.nombreCapacitor,
                                                 nombreCapacitor = c.nombreCapacitor,
                                                 idPagotran = c.idPagotran,
                                                 idBde = c.idBde,
                                                 activo = c.activo,
                                                 potenciaMVAR = c.potenciaMVAR,
                                                 codigoCapacitor = c.codigoCapacitor,
                                                 idGeneral = c.idCapacitor,
                                                 esAutomatismo = c.tieneAutomatismo == null ? false : c.tieneAutomatismo,
                                                 descripcion = c.nombreCapacitor,
                                                 codigoEquipo = "CAP",
                                                 estacion = new EstacionVM
                                                 {
                                                     idEstacion = d.idEstacion,
                                                     nombreEstacion = d.nombreEstacion,
                                                     activo = d.activo,

                                                 },
                                             }).ToList();
                }

            }
            object json = new { data = listabancoCapacitores };
            return json;
        }
        public int AltaCapacitor(CapacitorVM capacitor)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var nombreCapacitorRepetido = capacitor.estacion.nombreEstacion + " " + capacitor.TensionKV + " " + capacitor.codigoCapacitor;
                var capacitorRepetido = db.CapCapacitor.Where(x => x.nombreCapacitor == nombreCapacitorRepetido).FirstOrDefault();
                var consultaUnCapacitor = db.CapCapacitor.Where(x => x.idCapacitor == capacitor.idCapacitor).FirstOrDefault();
                if (consultaUnCapacitor == null && capacitorRepetido == null)
                {
                    try
                    {
                        CapCapacitor c = new CapCapacitor
                        {
                            potenciaMVAR = capacitor.potenciaMVAR,
                            idEstacion = capacitor.estacion.idEstacion,
                            codigoCapacitor = capacitor.codigoCapacitor,
                            idPagotran = capacitor.idPagotran,
                            idBde = capacitor.idBde,
                            nombreCapacitor = capacitor.estacion.nombreEstacion + " " + capacitor.TensionKV + " " + capacitor.codigoCapacitor,
                            IdTipoEquipo = 7,
                            TensionKV = capacitor.TensionKV,
                            fechaCreacion = DateTime.Now,
                            activo = true,
                            remunerado = capacitor.remunerado,
                        };
                        db.CapCapacitor.Add(c);
                        db.SaveChanges();
                        var monto = db.IndisponibilidadMontoCapacitor.Where(x => x.activo == true && x.monto != null).FirstOrDefault(); // Busco el monto de la tension

                        if (capacitor.remunerado == true)
                        {
                            if (monto != null)
                            {
                                decimal? cargoRealxHora = 0; decimal? xMes = 0;

                                var fechaHoy = new DateTime(); var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;
                                int cantidadDias = DateTime.DaysInMonth(fechaHoy.Year, fechaHoy.Month);

                                cargoRealxHora = capacitor.potenciaMVAR * monto.monto;
                                xMes = cargoRealxHora * (cantidadDias * 24);

                                IndisponibilidadRemuCapacitor CapRemu = new IndisponibilidadRemuCapacitor
                                {
                                    idCapacitor = c.idCapacitor,
                                    cargoRealxHoras = cargoRealxHora,
                                    xMes = xMes,
                                    activo = true
                                };
                                db.IndisponibilidadRemuCapacitor.Add(CapRemu);
                                db.SaveChanges();
                            }
                            else
                            {
                                IndisponibilidadRemuCapacitor CapRemu = new IndisponibilidadRemuCapacitor
                                {
                                    idCapacitor = c.idCapacitor,
                                    activo = true
                                };
                                db.IndisponibilidadRemuCapacitor.Add(CapRemu);
                                db.SaveChanges();
                            }

                        }


                        return 200;
                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //ya existe ese dato
                }
            }
        }

        public int ModificarCapacitor(CapacitorVM capacitor)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnCapacitor = db.CapCapacitor.Where(x => x.idCapacitor == capacitor.idCapacitor).FirstOrDefault();
                if (consultaUnCapacitor != null)
                {
                    /*consultaUnCapacitor.nombreCapacitor = capacitor.estacion.nombreEstacion + " " + capacitor.TensionKV  + " " + capacitor.codigoCapacitor;
                    consultaUnCapacitor.potenciaMVAR = capacitor.potenciaMVAR;
                    consultaUnCapacitor.idEstacion = capacitor.estacion.idEstacion;
                    consultaUnCapacitor.idPagotran = capacitor.idPagotran;
                    consultaUnCapacitor.idBde = capacitor.idBde;
                    consultaUnCapacitor.codigoCapacitor = capacitor.codigoCapacitor;
                    consultaUnCapacitor.TensionKV = capacitor.TensionKV;
                    consultaUnCapacitor.tieneAutomatismo = capacitor.esAutomatismo;
                    consultaUnCapacitor.activo = true;*/

                    consultaUnCapacitor.fechaEdicion = DateTime.Now;
                    consultaUnCapacitor.activo = false;
                    consultaUnCapacitor.remunerado = false;

                    CapCapacitor c = new CapCapacitor
                    {
                        potenciaMVAR = capacitor.potenciaMVAR,
                        idEstacion = capacitor.estacion.idEstacion,
                        codigoCapacitor = capacitor.codigoCapacitor,
                        idPagotran = capacitor.idPagotran,
                        idBde = capacitor.idBde,
                        nombreCapacitor = capacitor.estacion.nombreEstacion + " " + capacitor.TensionKV + " " + capacitor.codigoCapacitor,
                        IdTipoEquipo = 7,
                        TensionKV = capacitor.TensionKV,
                        fechaCreacion = DateTime.Now,
                        activo = true,
                        remunerado = capacitor.remunerado,
                    };
                    db.CapCapacitor.Add(c);
                    db.SaveChanges();

                    var ultimoCap = db.CapCapacitor.OrderByDescending(x => x.idCapacitor).FirstOrDefault();

                    var AutoRelacion = db.AutRelacionAutomatismo.Where(x => x.idEquipo == consultaUnCapacitor.idCapacitor && x.idTipoEquipo == 7 && x.idLugar == consultaUnCapacitor.idEstacion).ToList();//Busco los generadores que tenian el idGenerador viejo en la Relaciones con Automatismo
                    foreach (var CapaRel in AutoRelacion)
                    {
                        if (CapaRel.activo != false)
                        {
                            AutRelacionAutomatismo a = new AutRelacionAutomatismo
                            {
                                idAutomatismo = CapaRel.idAutomatismo,
                                idTipoEquipo = CapaRel.idTipoEquipo,
                                idLugar = CapaRel.idLugar,
                                idEquipo = ultimoCap.idCapacitor,
                                activo = true,
                            };
                            db.AutRelacionAutomatismo.Add(a);
                            CapaRel.activo = false;
                            db.SaveChanges();

                        }
                    }

                    var consultaCapRemunerado = db.IndisponibilidadRemuCapacitor.Where(x => x.idCapacitor == consultaUnCapacitor.idCapacitor).FirstOrDefault();
                    if (capacitor.remunerado == true)
                    {
                        var monto = db.IndisponibilidadMontoCapacitor.Where(x => x.activo == true && x.monto != null).FirstOrDefault(); // Busco el monto de la tension

                        if (consultaCapRemunerado == null)
                        {
                            if (monto != null)
                            {
                                decimal? cargoRealxHora = 0; decimal? xMes = 0;

                                var fechaHoy = new DateTime(); var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;
                                int cantidadDias = DateTime.DaysInMonth(fechaHoy.Year, fechaHoy.Month);

                                cargoRealxHora = capacitor.potenciaMVAR * monto.monto;
                                xMes = cargoRealxHora * (cantidadDias * 24);

                                IndisponibilidadRemuCapacitor CapRemu = new IndisponibilidadRemuCapacitor
                                {
                                    idCapacitor = c.idCapacitor,
                                    cargoRealxHoras = cargoRealxHora,
                                    xMes = xMes,
                                    activo = true
                                };
                                db.IndisponibilidadRemuCapacitor.Add(CapRemu);
                                db.SaveChanges();

                                var CapacitorIndisponible = db.IndisponibilidadCapacitor.Where(x => x.idCapacitorRemunerado == consultaCapRemunerado.idIndisponibilidadesCapacitor && x.idCapacitor == consultaUnCapacitor.idCapacitor).FirstOrDefault();
                                if (CapacitorIndisponible != null)
                                {
                                    ServicioIndisponibilidadesRemu servIndis = new ServicioIndisponibilidadesRemu();
                                    CapacitorIndisponible.idCapacitorRemunerado = CapRemu.idIndisponibilidadesCapacitor;
                                    CapacitorIndisponible.idCapacitor = c.idCapacitor;
                                    db.SaveChanges();
                                    servIndis.ModificoCapacitorIndisponibles(CapacitorIndisponible, monto.idIndisponibilidadMontosCapacitor);
                                }



                            }
                            else
                            {
                                IndisponibilidadRemuCapacitor CapRemu = new IndisponibilidadRemuCapacitor
                                {
                                    idCapacitor = c.idCapacitor,
                                    activo = true
                                };
                                db.IndisponibilidadRemuCapacitor.Add(CapRemu);
                                db.SaveChanges();
                            }

                        }
                        else // Si existe ese Cap remunerado , le pongo activo en falso y creo uno nuevo para tener un registro
                        {
                            if (monto != null)
                            {
                                consultaCapRemunerado.activo = false;

                                decimal? cargoRealxHora = 0; decimal? xMes = 0;

                                var fechaHoy = new DateTime(); var añoHoy = fechaHoy.Year; var mesHoy = fechaHoy.Month;
                                int cantidadDias = DateTime.DaysInMonth(fechaHoy.Year, fechaHoy.Month);

                                cargoRealxHora = capacitor.potenciaMVAR * monto.monto;
                                xMes = cargoRealxHora * (cantidadDias * 24);

                                IndisponibilidadRemuCapacitor CapRemu = new IndisponibilidadRemuCapacitor
                                {
                                    idCapacitor = c.idCapacitor,
                                    cargoRealxHoras = cargoRealxHora,
                                    xMes = xMes,
                                    activo = true
                                };
                                db.IndisponibilidadRemuCapacitor.Add(CapRemu);
                                db.SaveChanges();
                            }
                            else
                            {
                                IndisponibilidadRemuCapacitor CapRemu = new IndisponibilidadRemuCapacitor
                                {
                                    idCapacitor = c.idCapacitor,
                                    activo = true
                                };
                                db.IndisponibilidadRemuCapacitor.Add(CapRemu);
                                db.SaveChanges();
                            }

                        }

                    }
                    else
                    {
                        consultaCapRemunerado.activo = false;
                        db.SaveChanges();
                    }

                }
            }
            return 200;
        }

        public int EliminarCapacitor(CapacitorVM capacitor)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnCapacitor = db.CapCapacitor.Where(x => x.idCapacitor == capacitor.idCapacitor).FirstOrDefault();
                if (consultaUnCapacitor != null)
                {
                    consultaUnCapacitor.activo = false;
                }
                db.SaveChanges();
            }
            return 200;
        }

        public CapacitorVM ConsultarUnCapacitor(int? idCapacitor)
        {
            CapacitorVM capacitor = new CapacitorVM();

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                capacitor = (from c in db.CapCapacitor
                             join d in db.EstEstacion on c.idEstacion equals d.idEstacion
                             where c.idCapacitor == idCapacitor
                             select new CapacitorVM
                             {
                                 idCapacitor = c.idCapacitor,
                                 idEquipo = c.idCapacitor,
                                 nombreCapacitor = c.nombreCapacitor,
                                 nombreEquipo = "Capacitor",
                                 activo = c.activo,
                                 potenciaMVAR = c.potenciaMVAR,
                                 idPagotran = c.idPagotran,
                                 idBde = c.idBde,
                                 codigoCapacitor = c.codigoCapacitor,
                                 esAutomatismo = c.tieneAutomatismo,
                                 TensionKV = c.TensionKV,
                                 remunerado = c.remunerado,
                                 estacion = new EstacionVM
                                 {
                                     idEstacion = d.idEstacion,
                                     nombreEstacion = d.nombreEstacion,
                                     activo = d.activo,

                                 },
                             }).FirstOrDefault();
            }
            return capacitor;
        }

        public int RecuperarCapacitor(CapacitorVM capacitor)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnCapacitor = db.CapCapacitor.Where(x => x.idCapacitor == capacitor.idCapacitor).FirstOrDefault();
                if (consultaUnCapacitor != null)
                {
                    consultaUnCapacitor.activo = true;
                }

                db.SaveChanges();
            }
            return 200;
        }

        public bool ValidarCapacitor(CapacitorVM capacitor)
        {
            if (!string.IsNullOrWhiteSpace(capacitor.potenciaMVAR.ToString()) &&
                capacitor.estacion.idEstacion > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



    }
}
