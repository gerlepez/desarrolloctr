﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class NovCentroControlVM
    {
        public int idCentroControl { get; set; }
        public int? idCentral { get; set; }
        public int? idActuacionEstado { get; set; }
        public int? idTipoActuacion { get; set; }
        public int idNovedad { get; set; }
        public int idEquipo { get; set; }
    }
}
