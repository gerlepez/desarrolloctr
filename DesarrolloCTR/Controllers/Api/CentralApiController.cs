﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System.Net.Http.Formatting;

namespace DesarrolloCTR.Controllers.Api
{
    public class CentralApiController : ApiController
    {
        [System.Web.Http.Route("Api/Central/ObtenerListaCentrales")]
        [System.Web.Http.ActionName("ObtenerListaCentrales")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaCentrales(bool? activo)
        {

            ServicioCentrales servicioCentral = new ServicioCentrales();
            object lista = servicioCentral.ObtenerListaCentrales(activo);
            return lista;
        }
        

        [System.Web.Http.Route("Api/Central/ObtenerTipoCentroControl")]
        [System.Web.Http.ActionName("ObtenerTipoCentroControl")]
        [System.Web.Http.HttpGet]
        public int ObtenerTipoCentroControl(int idCentroControl)
        {

            ServicioCentrales servicioCentral = new ServicioCentrales();
            int tipo = servicioCentral.ObtenerTipoCentroControl(idCentroControl);
            return tipo;
        }

        [System.Web.Http.Route("Api/Central/TablaCentrales")]
        [System.Web.Http.ActionName("TablaCentrales")]
        [System.Web.Http.HttpPost]
        public object TablaCentrales(FormDataCollection form, bool? activo)
        {
            var draw = form.Get("draw");
            var start = form.Get("start");
            var length = form.Get("length");
            var sortColumn = (form.Get("columns[" + form.Get("order[0][column]").FirstOrDefault() + "][data]").ToString()).ToString();
            var sortColumnDir = form.Get("order[0][dir]").ToString();
            var searchValue = form.Get("search[value]").ToString();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            int cantidadRegistrosPagina = Int32.Parse(length);

            ServicioCentrales servicioCentral = new ServicioCentrales();
            object lista = servicioCentral.TablaCentrales(activo, draw, recordsTotal, skip, pageSize);
            return lista;
        }

        [System.Web.Http.Route("Api/Central/ConsultarUnaCentral")]
        [System.Web.Http.ActionName("ConsultarUnaCentral")]
        [System.Web.Http.HttpGet]
        public CentralVM ConsultarUnaCentral(int idCentro)
        {
            ServicioCentrales servicioCentral = new ServicioCentrales();
            CentralVM central = servicioCentral.ConsultarUnaCentral(idCentro);
            return central;
        }

        [System.Web.Http.Route("Api/Central/ObtenerCentralPorCentro")]
        [System.Web.Http.ActionName("ObtenerCentralPorCentro")]
        [System.Web.Http.HttpGet]
        public object ObtenerCentralPorCentro(int idCentro)
        {
            ServicioCentrales servicioCentral = new ServicioCentrales();
            object central = servicioCentral.ObtenerCentralPorCentro(idCentro);
            return central;
        }

        [System.Web.Http.Route("Api/Central/EliminarCentral")]
        [System.Web.Http.ActionName("EliminarCentral")]
        [System.Web.Http.HttpPost]
        public int EliminarCentral(CentralVM central)
        {
            ServicioCentrales servicioCentral = new ServicioCentrales();
            int codigo = servicioCentral.EliminarCentral(central);
            return codigo;
        }

        [System.Web.Http.Route("Api/Central/RecuperarCentral")]
        [System.Web.Http.ActionName("RecuperarCentral")]
        [System.Web.Http.HttpPost]
        public int RecuperarCentral(CentralVM central)
        {
            ServicioCentrales servicioCentral = new ServicioCentrales();
            int codigo = servicioCentral.RecuperarCentral(central);
            return codigo;

        }


        [System.Web.Http.Route("Api/Central/AltaCentral")]
        [System.Web.Http.ActionName("AltaCentral")]
        [System.Web.Http.HttpPost]
        public int AltaCentral(CentralVM central)
        {
            var idUsuarioLogeado = 0;
            if (central != null)
            {
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogeado = serUsuario.ObtenerUsuarioLogueado(central.nombresuario);
            }
            ServicioCentrales servicioCentral = new ServicioCentrales();
            int codigo;
            if (servicioCentral.ValidarCentral(central))
            {
                codigo = servicioCentral.AltaCentral(central, idUsuarioLogeado);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }


        [System.Web.Http.Route("Api/Central/ModificarCentral")]
        [System.Web.Http.ActionName("ModificarCentral")]
        [System.Web.Http.HttpPost]
        public int ModificaCentral(CentralVM central)
        {
            var idUsuarioLogeado = 0;
            if (central != null)
            {
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogeado = serUsuario.ObtenerUsuarioLogueado(central.nombresuario);
            }

            ServicioCentrales servicioCentral = new ServicioCentrales();
            int codigo;
            if (servicioCentral.ValidarCentral(central))
            {
                codigo = servicioCentral.ModificarCentral(central, idUsuarioLogeado);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }
    }
}