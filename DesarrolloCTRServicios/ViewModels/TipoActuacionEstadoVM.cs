﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class TipoActuacionEstadoVM
    {
        public int? idTipoActuacionEstado { get; set; }
        public string nombreTipoActuacionEstado { get; set; }
        public bool? activo { get; set; }
    }
}
