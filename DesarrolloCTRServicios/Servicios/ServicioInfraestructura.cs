﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioInfraestructura
    {

        public object ObtenerInfraestructura(bool activo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var listaEquipodeInfraestructura = (from i in db.RecInfraestructura
                                                    join t in db.RecReclamoTipoInfra on i.idTipoInfra equals t.idTipoInfra into tin
                                                    from ti in tin.DefaultIfEmpty()
                                                    where i.activo == activo
                                                    select new InfraestructuraVM
                                                    {
                                                        idInfra = i.idInfraestructura,
                                                        nombreInfra = i.nombreInfraestructura,
                                                        codigoInfraestructura = i.codigoInfraestructura,
                                                        activo = i.activo,
                                                        idTipoIfra = i.idTipoInfra,
                                                        tipoInfra = new TipoInfraestructuraVM
                                                        {
                                                            idTipoInfra = ti.idTipoInfra,
                                                            nombreInfra = ti.nombreInfra,
                                                            activo = ti.activo,
                                                        }
                                                    }).ToList();
                object json = new { data = listaEquipodeInfraestructura };
                return json;
            }
        }



        public object ObtenerInfraestructuraReclamo(bool activo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var listaEquipodeInfraestructura = (from t in db.RecReclamoTipoInfra
                                                    where activo == true
                                                    select new EquipoReclamoVM
                                                    {
                                                        idEquipoReclamo = t.idTipoInfra,
                                                        nombreEquipoReclamo = t.nombreInfra,
                                                        idEquipo = t.idTipoInfra,
                                                        nombreEquipo = t.nombreInfra

                                                    }).ToList();
                object json = new { data = listaEquipodeInfraestructura };
                return json;
            }
        }

        public InfraestructuraVM ConsultarUnaInfraestructura(int? idInfraestructura)
        {
            InfraestructuraVM infraestructura = new InfraestructuraVM();

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                infraestructura = (from i in db.RecInfraestructura
                                   join tin in db.RecReclamoTipoInfra on i.idTipoInfra equals tin.idTipoInfra
                                   where i.idInfraestructura == idInfraestructura
                                   select new InfraestructuraVM
                                   {
                                       activo = i.activo,
                                       idInfra = i.idInfraestructura,
                                       nombreInfra = i.nombreInfraestructura,
                                       codigoInfraestructura = i.codigoInfraestructura,

                                       tipoInfra = new TipoInfraestructuraVM
                                       {
                                           idTipoInfra = tin.idTipoInfra,
                                           nombreInfra = tin.nombreInfra,
                                           activo = tin.activo,
                                       },
                                   }).FirstOrDefault();
                return infraestructura;
            }
        }

        public int AltaInfraestructura(InfraestructuraVM infra)
        {

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarUnaInfraestructura = db.RecInfraestructura.Where(x => x.idInfraestructura == infra.idInfra).FirstOrDefault();
                var InfraestructuraAnterior = db.RecInfraestructura.Where(x => x.nombreInfraestructura == infra.nombreInfra && x.activo == true).FirstOrDefault();
                if (consultarUnaInfraestructura == null && InfraestructuraAnterior == null)
                {

                    try
                    {
                        RecInfraestructura i = new RecInfraestructura
                        {
                            // idGenerador = generador.idGenerador,
                            nombreInfraestructura = infra.nombreInfra,
                            codigoInfraestructura = infra.codigoInfraestructura,
                            idTipoInfra = infra.tipoInfra.idTipoInfra,
                            activo = true,
                        };
                        db.RecInfraestructura.Add(i);
                        db.SaveChanges();
                        return 200;
                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }

            }
        }

        public int ModificarInfraestructura(InfraestructuraVM infra)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarUnaInfraestructura = db.RecInfraestructura.Where(x => x.idInfraestructura == infra.idInfra).FirstOrDefault();
                if (consultarUnaInfraestructura != null)
                {
                    consultarUnaInfraestructura.nombreInfraestructura = infra.nombreInfra;
                    consultarUnaInfraestructura.codigoInfraestructura = infra.codigoInfraestructura;
                    consultarUnaInfraestructura.idTipoInfra = infra.tipoInfra.idTipoInfra;
                    consultarUnaInfraestructura.activo = true;

                    db.SaveChanges();
                }
            }
            return 200;

        }

        public int EliminarInfraestructura(InfraestructuraVM infra)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarUnaInfraestructura = db.RecInfraestructura.Where(x => x.idInfraestructura == infra.idInfra).FirstOrDefault();
                if (consultarUnaInfraestructura != null)
                {
                    consultarUnaInfraestructura.activo = false;
                }
                db.SaveChanges();
            }
            return 200;
        }

        public int RecuperarInfraestructura(InfraestructuraVM infra)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())

            {
                var consultarUnaInfraestructura = db.RecInfraestructura.Where(x => x.idInfraestructura == infra.idInfra).FirstOrDefault();
                var InfraestructuraAnterior = db.RecInfraestructura.Where(x => x.nombreInfraestructura == infra.nombreInfra && x.activo == true).FirstOrDefault();
                if (consultarUnaInfraestructura != null)
                {
                    if (InfraestructuraAnterior == null)
                    {
                        consultarUnaInfraestructura.activo = true;
                        db.SaveChanges();
                        return 200;
                    }
                    else
                    {
                        return 300;
                    }
                }
                else
                {
                    return 400;
                }

            }

        }

        public bool ValidarInfraestructura(InfraestructuraVM infra)
        {
            if (!string.IsNullOrWhiteSpace(infra.nombreInfra.ToString()))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public object ObtenerTipoInfraestructura(bool activo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var listaTipoInfraestructura = (from t in db.RecReclamoTipoInfra
                                                where activo == true
                                                select new TipoInfraestructuraVM
                                                {
                                                    idTipoInfra = t.idTipoInfra,
                                                    nombreInfra = t.nombreInfra,

                                                }).ToList();
                object json = new { data = listaTipoInfraestructura };
                return json;
            }
        }
    }
}
