﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class MantenimientoVM
    {
        public int? idMantenimiento { get; set; }
        public string nombreMantenimiento { get; set; }
        public bool? activo { get; set; }
    }
}
