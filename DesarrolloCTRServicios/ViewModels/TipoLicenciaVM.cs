﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class TipoLicenciaVM
    {
        public int idTipoLicencia { get; set; }
        public string nombreTipoLicencia { get; set; }
    }
}
