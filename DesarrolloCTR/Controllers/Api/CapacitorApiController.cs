﻿using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DesarrolloCTR.Controllers.Api
{
    public class CapacitorApiController : ApiController
    {
        [System.Web.Http.Route("Api/Capacitor/ObtenerBancoCapacitor")]
        [System.Web.Http.ActionName("ObtenerBancoCapacitor")]
        [System.Web.Http.HttpGet]
        public object ObtenerBancoCapacitor(bool activo)
        {
            ServicioCapacitores servicioBancoCapacitores = new ServicioCapacitores();
            object lista = servicioBancoCapacitores.ObtenerCapacitores(activo);
            return lista;
        }


        [System.Web.Http.Route("Api/Capacitor/AltaCapacitor")]
        [System.Web.Http.ActionName("AltaCapacitor")]
        [System.Web.Http.HttpPost]
        public int AltaCapacitor(CapacitorVM capacitor)
        {
            ServicioCapacitores servicioBancoCapacitores = new ServicioCapacitores();
            int codigo;
            if (servicioBancoCapacitores.ValidarCapacitor(capacitor))
            {
                codigo = servicioBancoCapacitores.AltaCapacitor(capacitor);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }

        [System.Web.Http.Route("Api/Capacitor/ModificarCapacitor")]
        [System.Web.Http.ActionName("ModificarCapacitor")]
        [System.Web.Http.HttpPost]
        public int ModificarCapacitor(CapacitorVM capacitor)
        {
            ServicioCapacitores servicioBancoCapacitores = new ServicioCapacitores();
            int codigo;
            if (servicioBancoCapacitores.ValidarCapacitor(capacitor))
            {
                codigo = servicioBancoCapacitores.ModificarCapacitor(capacitor);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }

        [System.Web.Http.Route("Api/Capacitor/EliminarCapacitor")]
        [System.Web.Http.ActionName("EliminarCapacitor")]
        [System.Web.Http.HttpPost]
        public int EliminarCapacitor(CapacitorVM capacitor)
        {
            ServicioCapacitores servicioBancoCapacitores = new ServicioCapacitores();
            int codigo = servicioBancoCapacitores.EliminarCapacitor(capacitor);
            return codigo;
        }

        [System.Web.Http.Route("Api/Capacitor/ConsultarUnCapacitor")]
        [System.Web.Http.ActionName("ConsultarUnCapacitor")]
        [System.Web.Http.HttpGet]
        public CapacitorVM ConsultarUnCapacitor(int idCapacitor)
        {
            ServicioCapacitores servicioBancoCapacitores = new ServicioCapacitores();
            CapacitorVM capacitor = servicioBancoCapacitores.ConsultarUnCapacitor(idCapacitor);
            return capacitor;
        }

        [System.Web.Http.Route("Api/Capacitor/RecuperarCapacitor")]
        [System.Web.Http.ActionName("RecuperarCapacitor")]
        [System.Web.Http.HttpPost]
        public int RecuperarCapacitor(CapacitorVM bancoCapacitores)
        {
            ServicioCapacitores servicioBancoCapacitores = new ServicioCapacitores();
            int codigo = servicioBancoCapacitores.RecuperarCapacitor(bancoCapacitores);
            return codigo;
        }
    }
}
