﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;


namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioEquipoAdicionales
    {
        public object obtenerEquipos(bool? activo)
        {
            List<EquipoVM> listaEquipos;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                listaEquipos = (from e in db.EquiEquipoAdicionales
                                where e.activo == activo
                                select new EquipoVM
                                {
                                    idEquipo = e.idEquipoAdicional,
                                    nombreEquipo = e.equipoAdicional,
                                    activo = e.activo
                                }).ToList();
                object json = new { data = listaEquipos };
                return json;
            }
        }


        public int AltaEquipoEspecial(EquipoVM equipo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarEquipo = db.EquiEquipoAdicionales.Where(x => x.idEquipoAdicional == equipo.idEquipo).FirstOrDefault();
                if (consultarEquipo == null)
                {
                    try
                    {
                        EquiEquipoAdicionales e = new EquiEquipoAdicionales
                        {
                            equipoAdicional = equipo.nombreEquipo,
                            activo = true
                        };
                        db.EquiEquipoAdicionales.Add(e);
                        db.SaveChanges();
                        return 200;
                    }
                    catch (Exception e)
                    {
                        return 400;
                    }
                }
                else
                {
                    return 300; //Existe
                }
            }
        }

        public EquipoVM ConsultarUnEquipo(int? idEquipo)
        {
            EquipoVM equipo = new EquipoVM();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                equipo = (from e in db.EquiEquipoAdicionales
                          where e.idEquipoAdicional == idEquipo
                          select new EquipoVM()
                          {
                              idEquipo = e.idEquipoAdicional,
                              nombreEquipo = e.equipoAdicional,
                              activo = e.activo
                          }).FirstOrDefault();
            }
            return equipo;
        }

        public int ModificarEquipoAdicional(EquipoVM equipo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarEquipo = db.EquiEquipoAdicionales.Where(x => x.idEquipoAdicional == equipo.idEquipo).FirstOrDefault();
                if (consultarEquipo != null)
                {
                    consultarEquipo.equipoAdicional = equipo.nombreEquipo;
                    db.SaveChanges();
                }
            }
            return 200;
        }

        public int EliminarEquipoAdicional(EquipoVM equipo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarEquipo = db.EquiEquipoAdicionales.Where(x => x.idEquipoAdicional == equipo.idEquipo).FirstOrDefault();
                if (consultarEquipo != null)
                {
                    consultarEquipo.activo = false;
                }
                db.SaveChanges();
            }
            return 200;
        }


        public int RecuperarEquipo(EquipoVM equipo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())

            {
                var consultarEquipo = db.EquiEquipoAdicionales.Where(x => x.idEquipoAdicional == equipo.idEquipo).FirstOrDefault();
                if (consultarEquipo != null)
                {
                    consultarEquipo.activo = true;
                    db.SaveChanges();
                    return 200;
                }
                else
                {
                    return 400;
                }

            }
        }

    }
}
