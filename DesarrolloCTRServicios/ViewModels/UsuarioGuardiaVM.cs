﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class UsuarioGuardiaVM
    {
        public int idUsuarioGuardia { get; set; }
        public int idUsuario { get; set; }
        public int idGuardia { get; set; }
        public int idMantenimiento { get; set; }
        public bool activo { get; set; }
        public int codigo { get; set; }
    }
}
