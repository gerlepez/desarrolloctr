﻿using System;

namespace DesarrolloCTRServicios.ViewModels
{
    public class EstacionVM
    {
        public int? idEstacion { get; set; }
        public string nombreEstacion { get; set; }
        public int? idEquipo { get; set; }
        public string nombreEquipo { get; set; }
        public PropiedadEstacionVM propiedad { get; set; }
        public EstadoEstacionVM estadoEstacion { get; set; }
        public int? idUsarioAlta { get; set; }
        public DateTime? fechaUsarioAlta { get; set; }
        public int? idUsuarioModifica { get; set; }
        public DateTime? fechaUsuarioModifica { get; set; }
        public bool? activo { get; set; }
        public string codigoEstacion { get; set; }
        public string nombresuario { get; set; }
        public TipoEstacionVM tipoEstacion { get; set; }
        public int? idEquipoAdicional { get; set; }
        public string nombreEquipoAdicional { get; set; }
    }
}
