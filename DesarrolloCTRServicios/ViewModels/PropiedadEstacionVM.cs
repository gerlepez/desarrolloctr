﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class PropiedadEstacionVM
    {
        public int idPropiedad { get; set; }
        public string nombrePropiedad { get; set; }
        public bool? activo { get; set; }
    }
}
