﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioCentroControl
    {

        public object ObtenerCentroControl(bool? activo)
        {
            List<CentroControlVM> listaCentroControl;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                listaCentroControl = (from c in db.CConCentroControl
                                      join t in db.CConTipo on c.idTipo equals t.idCConTipo
                                      join p in db.ProPropiedad on c.idPropiedad equals p.idPropiedad into pro
                                      from p in pro.DefaultIfEmpty()
                                      where c.activo == activo
                                      select new CentroControlVM
                                      {
                                          idCentroControl = c.idCentroControl,
                                          nombreCentroControl = t.nombre + " " + c.nombreCentroControl,
                                          codigosap = c.codigoSap,
                                          idTipoCControl = t.idCConTipo,
                                          nombreTipoCControl = t.nombre,
                                          activo = c.activo,
                                          nombrePropiedad = p.nombrePropiedad,
                                      }).ToList();

                var listaOrdenada = listaCentroControl.OrderByDescending(x => x.idCentroControl);
                object json = new { data = listaOrdenada };
                return json;
            }
        }


        public object TablaCentroControl(bool? activo, string draw, int recordsTotal, int skip, int pageSize)
        {
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    ServicioUsuario usuarioservice = new ServicioUsuario();
                    IQueryable<CentroControlVM> query = (from c in db.CConCentroControl
                                                         where c.activo == activo
                                                         select new CentroControlVM
                                                         {
                                                             idCentroControl = c.idCentroControl,
                                                             nombreCentroControl = c.nombreCentroControl,
                                                             codigosap = c.codigoSap,
                                                             activo = c.activo,
                                                         }).OrderByDescending(x => x.idCentroControl).AsQueryable();
                    var queryFiltrada = query;
                    recordsTotal = queryFiltrada.Count();
                    var lst = queryFiltrada.Skip(skip).Take(pageSize).ToList();

                    object json = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = lst };
                    return json;
                }
            }
            catch (Exception e)
            {
                return 400;
            }
        }

        public CentroControlVM ConsultarUnCentroControl(int idCentroControl)
        {
            CentroControlVM centroControl = new CentroControlVM();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                centroControl = (from c in db.CConCentroControl
                                 where c.idCentroControl == idCentroControl
                                 select new CentroControlVM
                                 {
                                     idCentroControl = c.idCentroControl,
                                     nombreCentroControl = c.nombreCentroControl,
                                     codigosap = c.codigoSap,
                                     idTipoCControl = c.idTipo,
                                     activo = c.activo,
                                     idPropiedad = c.idPropiedad,
                                 }).FirstOrDefault();
            }
            return centroControl;
        }

        public List<CentralVM> ObtenerListaCentralesPorCentro(int idCentroControl)
        {
            List<CentralVM> listaCentrales = new List<CentralVM>();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                listaCentrales = (from c in db.CConCentral
                                  where c.activo == true && c.idCentroControl == idCentroControl
                                  select new CentralVM
                                  {
                                      activo = c.activo,
                                      idCentral = c.idCentral,
                                      nombreCentral = c.nombreCentral
                                  }).ToList();
            }
            return listaCentrales;
        }

        public int EliminarCentroControl(CentroControlVM centrocontrol)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnCentro = db.CConCentroControl.Where(x => x.idCentroControl == centrocontrol.idCentroControl).FirstOrDefault();
                if (consultaUnCentro != null)
                {
                    consultaUnCentro.activo = false;
                }
                db.SaveChanges();
            }
            return 200;
        }

        public int RecuperarCentroControl(CentroControlVM centroControl)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunCentro = db.CConCentroControl.Where(x => x.idCentroControl == centroControl.idCentroControl).FirstOrDefault();
                if (consultaunCentro != null)
                {
                    consultaunCentro.activo = true;
                    db.SaveChanges();
                }
            }
            return 200;
        }

        public int AltaCentroControl(CentroControlVM centroControl)
        {

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var centroControlRepetido = db.CConCentroControl.Where(x => x.nombreCentroControl == centroControl.nombreCentroControl && x.idTipo == centroControl.idTipoCControl).FirstOrDefault();
                var consultaUnCentro = db.CConCentroControl.Where(x => x.idCentroControl == centroControl.idCentroControl).FirstOrDefault();
                if (consultaUnCentro == null && centroControlRepetido == null)
                {
                    try
                    {
                        CConCentroControl cc = new CConCentroControl
                        {
                            nombreCentroControl = centroControl.nombreCentroControl,
                            codigoSap = centroControl.codigosap,
                            idTipo = centroControl.idTipoCControl,
                            fechaCreacion = DateTime.Now,
                            activo = true,
                            idPropiedad = centroControl.idPropiedad,
                        };
                        db.CConCentroControl.Add(cc);
                        db.SaveChanges();
                        return 200;
                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }

            }
        }

        public bool ValidarCentroControl(CentroControlVM centroControl)
        {
            if (!string.IsNullOrWhiteSpace(centroControl.nombreCentroControl.ToString()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int ModificarCentroControl(CentroControlVM centroControl)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarUnCentroControl = db.CConCentroControl.Where(x => x.idCentroControl == centroControl.idCentroControl).FirstOrDefault();
                if (consultarUnCentroControl != null)
                {
                    consultarUnCentroControl.nombreCentroControl = centroControl.nombreCentroControl;
                    consultarUnCentroControl.codigoSap = centroControl.codigosap;
                    consultarUnCentroControl.idTipo = centroControl.idTipoCControl;
                    consultarUnCentroControl.activo = true;
                    consultarUnCentroControl.fechaEdicion = DateTime.Now;
                    consultarUnCentroControl.idPropiedad = centroControl.idPropiedad;
                    db.SaveChanges();

                    /*
                                        consultarUnCentroControl.activo = false;
                                        consultarUnCentroControl.fechaEdicion = DateTime.Now;

                                        CConCentroControl cc = new CConCentroControl
                                        {
                                            nombreCentroControl = centroControl.nombreCentroControl,
                                            codigoSap = centroControl.codigosap,
                                            idTipo = centroControl.idTipoCControl,
                                            fechaCreacion = DateTime.Now,
                                            activo = true,
                                        };
                                        db.CConCentroControl.Add(cc);
                                        db.SaveChanges();

                                        var ultimoCentro = db.CConCentroControl.OrderByDescending(x => x.idCentroControl).FirstOrDefault();

                                        var Centrales = db.CConCentral.Where(x => x.idCentroControl == consultarUnCentroControl.idCentroControl).ToList();
                                        foreach (var central in Centrales)
                                        {                       
                                                central.idCentroControl = ultimoCentro.idCentroControl;
                                                db.SaveChanges();                      
                                        }
                    */

                }

            }
            return 200;

        }

        public object ObtenerTipoCentroControl()
        {
            List<CentroControlVM> centroControl = new List<CentroControlVM>();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {

                centroControl = (from t in db.CConTipo
                                 select new CentroControlVM
                                 {
                                     idTipoCControl = t.idCConTipo,
                                     nombreTipoCControl = t.nombre
                                 }).ToList();

            }
            object json = new { data = centroControl };
            return json;

        }


    }
}
