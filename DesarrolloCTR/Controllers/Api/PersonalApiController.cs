﻿using DesarrolloCTRServicios.Servicios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers.Api
{
    public class PersonalApiController : ApiController
    {
        [System.Web.Http.Route("Api/Personal/ObtenerPersonalPorId")]
        [System.Web.Http.ActionName("ObtenerPersonalPorId")]
        [System.Web.Http.HttpGet]
        public object ObtenerPersonalPorId(int idPersonal)
        {
            ServicioPersonal servicio = new ServicioPersonal();

            var Usuario = servicio.ObtenerDatosUsuarioPorId(idPersonal);

            return Usuario;
        }
    }
}