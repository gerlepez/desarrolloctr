﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioTension
    {
        public object TraeListaTension()
        {
            List<TensionVM> traerListaTension;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                traerListaTension = (from c in db.GralTension
                                     where c.activo == true
                                     select new TensionVM
                                     {
                                         idTension = c.idTension,
                                         tension = c.tension,
                                         activo = c.activo
                                     }).ToList();
            }
            object json = new { data = traerListaTension };

            return json;
        }

        public TensionVM TraerUnaTension(int idTension)
        {
            TensionVM unaTension;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                unaTension = (from c in db.GralTension
                              where c.activo == true && c.idTension == idTension
                              select new TensionVM
                              {
                                  idTension = c.idTension,
                                  tension = c.tension,
                                  activo = c.activo
                              }).FirstOrDefault();
            }
            return unaTension;
        }

    }
}
