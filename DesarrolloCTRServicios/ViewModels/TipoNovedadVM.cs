﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class TipoNovedadVM
    {
        public int? idTipoNovedad { get; set; }
        public string nombreTipoNovedad { get; set; }
        public bool? tipofalla { get; set; }
        public bool? personal { get; set; }
        public bool? transformador { get; set; }
        public bool? equipo { get; set; }
        public bool? estacion { get; set; }
        public bool? linea { get; set; }
        public bool? activo { get; set; }
        public bool? capacitor { get; set; }
        public bool? puntoConexion { get; set; }
        public string nombreusuario { get; set; }
        public bool? centroControl { get; set; }
        public bool? reclamo { get; set; }
        public bool? automatismo { get; set; }
        public string codigoTipoNovedad { get; set; }
    }
}
