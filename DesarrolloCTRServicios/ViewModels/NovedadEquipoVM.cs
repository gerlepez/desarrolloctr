﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class NovedadEquipoVM
    {
        public int idNovNovedadEquipo { get; set; }
        public int? idNovedad { get; set; }
        public int? idEquipo { get; set; }
    }
}
