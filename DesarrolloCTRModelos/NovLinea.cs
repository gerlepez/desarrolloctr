//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DesarrolloCTRModelos
{
    using System;
    using System.Collections.Generic;
    
    public partial class NovLinea
    {
        public int idNovLinea { get; set; }
        public Nullable<int> idTipoLinea { get; set; }
        public Nullable<int> idPropiedad { get; set; }
        public Nullable<int> idLinea { get; set; }
        public Nullable<int> idNovedad { get; set; }
        public string alarma { get; set; }
    }
}
