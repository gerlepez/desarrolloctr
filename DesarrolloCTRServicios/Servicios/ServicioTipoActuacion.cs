﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioTipoActuacion
    {
        public object ObtenerListaTipoActuacion(bool activo)
        {
            List<TipoActuacionVM> listaTipoActuacion;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                listaTipoActuacion = (from c in db.NovTipoActuacion
                                      where c.activo == activo
                                      select new TipoActuacionVM
                                      {
                                          idTipoActuacion = c.idTipoActuacion,
                                          nombreTipoActuacion = c.nombreTipoActuacion,
                                          activo = c.activo,

                                      }).ToList();
            }
            object json = new { data = listaTipoActuacion };
            return json;
        }

        public object ObtenerTipoActuacionPorActEqui(int idActuacion, string codigoEquipo)
        {

            List<TipoActuacionVM> listaTipoActuacion;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var idEquipo = (from e in db.EquiEquipo where e.codigoEquipo == codigoEquipo select e).FirstOrDefault().idEquipo;
                listaTipoActuacion = (from c in db.NovActuacTipoEquipo
                                      join tae in db.NovActuacionEstado on c.idActuacion equals tae.idActuacionEstado
                                      join ta in db.NovTipoActuacion on c.idTipoActuacion equals ta.idTipoActuacion
                                      where c.activo == true && c.idEquipo == idEquipo && c.idActuacion == idActuacion
                                      select new TipoActuacionVM
                                      {
                                          nombreTipoActuacion = ta.nombreTipoActuacion,
                                          idTipoActuacion = ta.idTipoActuacion,
                                          activo = ta.activo

                                      }).ToList();
            }
            object json = new { data = listaTipoActuacion };
            return json;
        }

        public int AltaTipoActuacion(TipoActuacionVM TipoActuacion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunActuacion = db.NovTipoActuacion.Where(x => x.idTipoActuacion == TipoActuacion.idTipoActuacion).FirstOrDefault();
                if (consultaunActuacion == null)
                {
                    try
                    {
                        NovTipoActuacion tipoF = new NovTipoActuacion
                        {
                            nombreTipoActuacion = TipoActuacion.nombreTipoActuacion,
                            activo = true,
                        };
                        db.NovTipoActuacion.Add(tipoF);
                        db.SaveChanges();
                        return 200;
                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //ya existe ese dato
                }
            }

        }

        public int ModificarTipoActuacion(TipoActuacionVM TipoActuacion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunActuacion = db.NovTipoActuacion.Where(x => x.idTipoActuacion == TipoActuacion.idTipoActuacion).FirstOrDefault();
                if (consultaunActuacion != null)
                {
                    consultaunActuacion.nombreTipoActuacion = TipoActuacion.nombreTipoActuacion;
                    consultaunActuacion.activo = true;
                    db.SaveChanges();
                }
            }
            return 200;
        }

        public int EliminarTipoActuacion(TipoActuacionVM TipoActuacion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunActuacion = db.NovTipoActuacion.Where(x => x.idTipoActuacion == TipoActuacion.idTipoActuacion).FirstOrDefault();
                if (consultaunActuacion != null)
                {
                    consultaunActuacion.activo = false;
                    db.SaveChanges();
                }
            }
            return 200;
        }

        public bool ValidarActuacion(TipoActuacionVM TipoActuacion)
        {
            if (!string.IsNullOrWhiteSpace(TipoActuacion.nombreTipoActuacion) && TipoActuacion.nombreTipoActuacion.Count() <= 30)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public TipoActuacionVM ConsultarUnTipoActuacion(int idActuacion)
        {
            TipoActuacionVM Actuacion = new TipoActuacionVM();
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                Actuacion = (from c in db.NovTipoActuacion
                             where c.activo == true && c.idTipoActuacion == idActuacion
                             select new TipoActuacionVM
                             {
                                 idTipoActuacion = c.idTipoActuacion,
                                 nombreTipoActuacion = c.nombreTipoActuacion,
                                 activo = c.activo,
                             }).FirstOrDefault();
            }
            return Actuacion;
        }

        public int RecuperarTipoActuacion(TipoActuacionVM TipoActuacion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunActuacion = db.NovTipoActuacion.Where(x => x.idTipoActuacion == TipoActuacion.idTipoActuacion).FirstOrDefault();
                if (consultaunActuacion != null)
                {
                    consultaunActuacion.activo = true;
                    db.SaveChanges();
                }
            }
            return 200;
        }
    }
}
