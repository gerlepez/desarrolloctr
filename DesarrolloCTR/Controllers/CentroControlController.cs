﻿using DesarrolloCTR.Controllers.Extra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers
{
    [SessionUtilityAttribute]
    [Permiso("CCON")]
    public class CentroControlController : Controller
    {
        // GET: CentroControl
        public ActionResult CentroControl()
        {
            return View();
        }

        public ActionResult AdministrarCentroControl()
        {
            return View();
        }
    }
}