﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;


namespace DesarrolloCTRServicios.Servicios
{


    public class ServicioUsuario
    {
        //IntranetCTREntities intranetdb = new IntranetCTREntities();
        //BuhoGestionEntities buhodb = new BuhoGestionEntities();
        public UsuarioVM ObtenerUsuarioLogin(string nombreUsuario, string contraseniaUsuario)
        {
            UsuarioVM loginUsuario;
            UsuarioVM Usuario;
            using (BuhoGestionEntities buhodb = new BuhoGestionEntities())
            {
                using (IntranetCTREntities intranetdb = new IntranetCTREntities())
                {
                    loginUsuario = (from c in buhodb.GralUsuario
                                    where c.usuario == nombreUsuario
                                    select new UsuarioVM
                                    {
                                        idUsuario = c.idUsuario,

                                    }).FirstOrDefault();
                    if (loginUsuario != null)
                    {


                        Usuario = (from d in intranetdb.GralLogin
                                   where d.idUsuario == loginUsuario.idUsuario && d.activo == true
                                          && d.contraseniaUsuario == contraseniaUsuario
                                   select new UsuarioVM
                                   {
                                       idLogin = d.idLogin,
                                       idUsuario = d.idUsuario,
                                       contraseniaUsuario = d.contraseniaUsuario,
                                       activo = d.activo,
                                       fechaCambioContrasenia = d.fechaCambioContrasenia,
                                   }).FirstOrDefault();
                    }
                    else
                    {
                        Usuario = null;
                    }

                    return Usuario;
                }
            }
        }
        public int ObtenerUsuarioLogueado(string nombreUsuario)
        {
            using (BuhoGestionEntities buhodb = new BuhoGestionEntities())
            {
                var usuario = (from c in buhodb.GralUsuario

                               where c.usuario == nombreUsuario
                               select new UsuarioVM
                               {
                                   idUsuario = c.idUsuario,
                                   activo = c.activo != 0,
                               }).FirstOrDefault() ?? new UsuarioVM()
                               {
                                   idUsuario = 0,
                                   activo = true,
                               };
                if (!usuario.activo)
                {
                    usuario.idUsuario = -1;
                }
                //devuelvo 0 si el usuario no existe en la bd ó -1 si existe pero no está activo

                return usuario.idUsuario;
            }
        }

        public string ObtenerLoginUsuario(int idUsuario, string pass)
        {
            using (BuhoGestionEntities buhodb = new BuhoGestionEntities())
            using (IntranetCTREntities bc = new IntranetCTREntities())
            {
                {
                  
                    //var usuario = (from c in buhodb.GralUsuario

                    //               where c.usuario == nombreUsuario && c.password == pass
                    //               select new UsuarioVM
                    //               {
                    //                   idUsuario = c.idUsuario,
                    //                   activo = c.activo != 0,
                    //                   //esGuardia = c.esJefeGuardia
                    //               }).FirstOrDefault() ?? new UsuarioVM()
                    //               {
                    //                   idUsuario = 0,
                    //                   activo = true,
                    //               };
                    if (idUsuario == 0)
                    {
                        return "DatosIncorrectos#Login";
                    }

                    var turnoJefeTurno = bc.TurnTurnoEjecucion.Where(x => x.idJefeTurno == idUsuario && x.fechaHoraApertura != null && x.fechaHoraCierre == null).FirstOrDefault();


                    var rolUsuario = bc.GralUsuarioRol.Where(x => x.idUsuario == idUsuario && x.activo == true).FirstOrDefault();
                    if (rolUsuario == null)
                    {
                        return "PermisoDenegado#Login";
                    }
                    var turnosAbiertos = bc.TurnTurnoEjecucion.Where(x => x.fechaHoraApertura != null && x.fechaHoraCierre == null).ToList();
                    var rol = bc.GralRol.Where(x => x.idRol == rolUsuario.idRol).FirstOrDefault();

                    if (rolUsuario != null)
                    {
                        if (rol != null)
                        {
                            if (rol.esJefe == true)
                            {
                                return "Novedad#Novedad";
                            }
                            if (rol.esOperador == true)
                            {
                                return "Novedad#Novedad";
                            }
                            var acceso = bc.GralAccesoRol.Where(x => x.idRol == rol.idRol).FirstOrDefault();
                            var menuInicio = bc.GralMenu.Where(x => x.idMenu == acceso.idMenu).FirstOrDefault().url;
                            var response = menuInicio.Split('/')[1] + "#" + menuInicio.Split('/')[2];
                            return response;
                        }
                        else
                        {
                            return "PermisoDenegado#Login";
                        }

                    }
                    else
                    {
                        return "PermisoDenegado#Login";
                    }
                }
            }
        }
        public async Task<int> ValidarIngreso(string usuario, string password)
        {
            //Volver a aplicar la anterior url antes de colocar en prod "https://buhogestion.distrocuyo.com/"
            //reemplazar url a una local si produccion no tiene el jwt activo
            string API_BASE_URL = "https://buhogestion.distrocuyo.com/";
            string endpoint = $"Ingenieria/api/Login/GetUsuario?usuario={usuario}&password={password}";
            var cookieContainer = new CookieContainer();
            using (var handler = new HttpClientHandler { CookieContainer = cookieContainer})

            using (HttpClient client = new HttpClient())
            {
                try
                {
                    HttpResponseMessage response = await client.GetAsync(API_BASE_URL + endpoint);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();
                    //Sector que toma la cookie de buho y la añade al navegador
                    var jwt = response.Headers.GetValues("Set-Cookie").FirstOrDefault();
                    var jwtCookie = jwt.Split(';');
                    var jwtValue = jwtCookie[0].Replace("jwtToken=", "");
                    var cookieExp = jwtCookie[1].Replace("expires=","");

                    var expirationDate = DateTime.Parse(cookieExp);
                    
                    HttpCookie cookie = new HttpCookie("jwtToken")
                    {
                        Value = jwtValue,
                        Expires = expirationDate,
                        Secure = true,
                        HttpOnly = true,
                        Shareable = false,
                    };
                    HttpContext.Current.Response.Cookies.Add(cookie);


                    if (int.TryParse(responseBody, out int result)) //verifica que sea un numero
                    {

                        return result;
                    }
                    else
                    {
                        return 0;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error al leer la respuesta JSON:");
                    Console.WriteLine(ex.Message);
                    throw;
                }

            }
        }

        public UsuarioVM ObtenerDatosUsuarioLogueado(string nombreUsuario)
        {
            using (BuhoGestionEntities buhodb = new BuhoGestionEntities())
            {

                //var user = nombreUsuario.Remove(0, 10);
                UsuarioVM usuario = new UsuarioVM();
                if (nombreUsuario != null)
                {
                    usuario = (from c in buhodb.GralUsuario
                               where c.activo == 1 && c.usuario == nombreUsuario
                               select new UsuarioVM
                               {
                                   idUsuario = c.idUsuario,
                                   nombreUsuario = c.Nombre,
                                   foto = c.foto,
                               }).FirstOrDefault();
                    //aca debo controlar lo que se hace si no encuentro usuario en la base
                    if (usuario == null)
                    {
                        usuario = new UsuarioVM();
                        usuario.nombreUsuario = "Sin nombre";
                        usuario.idUsuario = 0;

                    }

                }
                return usuario;
            }
        }

        public UsuarioVM ObtenerDatosUsuarioPorId(int? idUsuario)
        {
            using (BuhoGestionEntities buhodb = new BuhoGestionEntities())
            {

                //var user = nombreUsuario.Remove(0, 10);
                UsuarioVM usuario = new UsuarioVM();
                if (idUsuario != null)
                {
                    usuario = (from c in buhodb.GralUsuario
                               where c.activo == 1 && c.idUsuario == idUsuario
                               select new UsuarioVM
                               {
                                   idUsuario = c.idUsuario,
                                   nombreUsuario = c.Nombre,
                                   foto = c.foto,
                                   activo = true,

                               }).FirstOrDefault();
                    //aca debo controlar lo que se hace si no encuentro usuario en la base
                    if (usuario == null)
                    {
                        usuario = new UsuarioVM();
                        usuario.nombreUsuario = "Sin nombre";
                        usuario.idUsuario = 0;
                    }

                }
                return usuario;
            }
        }

        public List<UsuarioVM> ObtenerListaUsuarioCTR()
        {
            using (BuhoGestionEntities buhodb = new BuhoGestionEntities())
            {

                List<UsuarioVM> listaUsuario = new List<UsuarioVM>();
                listaUsuario = (from c in buhodb.GralUsuario
                                    //where c.activo == 1 && c.esCtr == true
                                where c.activo == 1 && c.esCtr == true
                                select new UsuarioVM
                                {
                                    idUsuario = c.idUsuario,
                                    nombreUsuario = c.Nombre,
                                }).ToList();

                return listaUsuario;
            }
        }

        public List<UsuarioVM> ObtenerListaOperadorCTR(int idUsuarioLogueado = 0)
        {
            using (BuhoGestionEntities buhodb = new BuhoGestionEntities())
            {

                List<UsuarioVM> listaUsuario = new List<UsuarioVM>();
                listaUsuario = (from c in buhodb.GralUsuario
                                join p in buhodb.GralUsuarioPuesto on c.idUsuario equals p.idUsuario
                                where c.activo == 1 && c.esCtr == true && (p.idPuesto == 92 || p.idPuesto == 84) && c.idUsuario != idUsuarioLogueado
                                select new UsuarioVM
                                {
                                    idUsuario = c.idUsuario,
                                    nombreUsuario = c.Nombre,
                                }).ToList();

                return listaUsuario;
            }
        }


        public List<UsuarioVM> ObtenerListaEmpleadosGuardia(int idGuardia)
        {
            using (BuhoGestionEntities buhodb = new BuhoGestionEntities())
            {
                using (IntranetCTREntities intranetdb = new IntranetCTREntities())
                {

                    List<UsuarioVM> listaUsuario = new List<UsuarioVM>();

                    List<UsuarioGuardiaVM> listaUsuariosGuardia = new List<UsuarioGuardiaVM>();
                    listaUsuariosGuardia = (from c in intranetdb.TurnUsuarioGuardia
                                            where c.activo == true && c.idGuardia == idGuardia
                                            select new UsuarioGuardiaVM
                                            {
                                                idUsuario = c.idUsuario,
                                                idGuardia = c.idGuardia,
                                                idUsuarioGuardia = c.idUsuarioGuardia,
                                                activo = c.activo
                                            }).ToList();

                    foreach (var item in listaUsuariosGuardia)
                    {
                        var usuario = (from c in buhodb.GralUsuario
                                       where c.activo == 1 && c.idUsuario == item.idUsuario
                                       select new UsuarioVM
                                       {
                                           idUsuario = c.idUsuario,
                                           nombreUsuario = c.Nombre,
                                           foto = c.foto,
                                       }).FirstOrDefault();
                        listaUsuario.Add(usuario);

                    }

                    return listaUsuario;

                }
            }
        }

        public List<UsuarioVM> ObtenerListaResponsableConsignacion()
        {
            using (BuhoGestionEntities buhodb = new BuhoGestionEntities())
            {

                List<UsuarioVM> listaUsuarios = new List<UsuarioVM>();
                listaUsuarios = (from c in buhodb.GralUsuario
                                 where c.activo == 1 && c.esGuardia == true
                                 select new UsuarioVM
                                 {
                                     idUsuario = c.idUsuario,
                                     nombreUsuario = c.Nombre,
                                 }).ToList();

                var listaOrdenada = listaUsuarios.OrderBy(x => x.nombreUsuario).ToList();
                return listaOrdenada;
            }
        }

        public bool TienePermisoPorRol(int idUsuario, int idRol)
        {
            using (BuhoGestionEntities buhodb = new BuhoGestionEntities())
            {
                using (IntranetCTREntities intranetdb = new IntranetCTREntities())
                {
                    //var user = nombreUsuario.Remove(0, 10);
                    UsuarioVM usuario = new UsuarioVM();
                    RolVM rol = new RolVM();
                    if (idUsuario != null && idRol != null)
                    {
                        usuario = (from c in buhodb.GralUsuario
                                   where c.activo == 1 && c.idUsuario == idUsuario
                                   select new UsuarioVM
                                   {
                                       idUsuario = c.idUsuario,
                                       nombreUsuario = c.Nombre,
                                       foto = c.foto,
                                   }).FirstOrDefault();
                        rol = (from r in intranetdb.GralRol
                               where r.activo == true && r.idRol == idRol
                               select new RolVM
                               {
                                   idRol = r.idRol,
                                   activo = r.activo,
                                   nombreRol = r.nombreRol
                               }).FirstOrDefault();
                        if (usuario != null && rol != null)
                        {
                            var userRol = (from ur in intranetdb.GralUsuarioRol
                                           where ur.activo == true && ur.idUsuario == usuario.idUsuario && ur.idRol == rol.idRol
                                           select ur).ToList();
                            if (userRol.Count > 0)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        //aca debo controlar lo que se hace si no encuentro usuario en la base
                        //if (usuario == null)
                        //{



                        //}

                    }
                    return false;
                }
            }
        }


        public bool SetUserSessionOut()
        {

            return true;
        }

        public static bool TienePermisoPorLogin(int? access, string nombreUsuario)
        {

            var usuario = Get(nombreUsuario);

            if (usuario > 0)
            {
                return HasAccess(usuario, access);
            }

            return false;
        }

        public static bool tienePermisoPorTurno(string nombreUsuario)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var usuario = Get(nombreUsuario);
                var turnosAbiertos = db.TurnTurnoEjecucion.Where(x => x.fechaHoraApertura != null && x.fechaHoraCierre == null).ToList();
                var rolUsuario = db.GralUsuarioRol.Where(x => x.idUsuario == usuario).FirstOrDefault();
                var rol = db.GralRol.Where(x => x.idRol == rolUsuario.idRol).FirstOrDefault();
                if (rol.idRol == 1)
                {
                    return true;
                }
                foreach (var turno in turnosAbiertos)
                {
                    if (turno.idJefeTurno == usuario && turno.activo == true)
                    {
                        return true;
                    }

                    var turnoAcompañante = db.TurnTurnoAcompañante.Where(x => x.idTurnoEjecucion == turno.idTurnoEjecucion).ToList(); //Lista de acompañantes
                    for (int j = 0; j < turnoAcompañante.Count; j++)
                    {
                        if (turnoAcompañante[j].idAcompañante == usuario && turnoAcompañante[j].activo == true)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }

        }

        public static int Get(string nombreUsuario)
        {
            using (BuhoGestionEntities buhodb = new BuhoGestionEntities())
            {
                var usuario = (from c in buhodb.GralUsuario

                               where c.usuario == nombreUsuario
                               select new UsuarioVM
                               {
                                   idUsuario = c.idUsuario,
                                   activo = c.activo != 0,
                               }).FirstOrDefault() ?? new UsuarioVM()
                               {
                                   idUsuario = 0,
                                   activo = true,
                               };
                if (!usuario.activo)
                {
                    usuario.idUsuario = -1;
                }
                //devuelvo 0 si el usuario no existe en la bd ó -1 si existe pero no está activo

                return usuario.idUsuario;
            }

        }

        private static bool HasAccess(int idUser, int? acceso)
        {
            using (IntranetCTREntities intranetdb = new IntranetCTREntities())
            {
                var list = (from t1 in intranetdb.GralUsuarioRol
                            join t2 in intranetdb.GralAccesoRol
                                on t1.idRol equals t2.idRol
                            join t3 in intranetdb.GralMenu
                                on t2.idMenu equals t3.idMenu
                            where t3.activo == true
                            select new { t1, t3 }).Where(x => x.t1.idUsuario == idUser).ToList();


                if (list.Any(item => item.t3.idMenu == acceso))
                {
                    return true;
                }
                return false;
            }
        }

        public static RolVM ObtenerTiposPermisos(int? access, string nombreUsuario)
        {
            using (IntranetCTREntities intranetdb = new IntranetCTREntities())
            {
                var idUsuario = Get(nombreUsuario);
                var rolPerm = (from t1 in intranetdb.GralUsuarioRol
                               join t2 in intranetdb.GralAccesoRol
                                   on t1.idRol equals t2.idRol
                               join t3 in intranetdb.GralMenu
                                   on t2.idMenu equals t3.idMenu
                               where t3.activo == true && t1.idUsuario == idUsuario && t2.idMenu == access
                               select new RolVM
                               {
                                   tienePermisoEdicion = t2.TienePermisoEdicion,
                                   tienePermisoEliminacion = t2.TienePermisoEliminacion,
                                   tienePermisoLectura = t2.TienePermisoLectura,
                                   idRol = t2.idRol,
                               }).FirstOrDefault();


                return rolPerm;

            }
        }



    }
}
