﻿using DesarrolloCTR.Controllers.Extra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers
{
    [SessionUtility]
    [Permiso("REMU")]
    public class IndisponibilidadRemuController : Controller
    {

        // GET: IndisponibilidadRemu
        public ActionResult IndisponibilidadRemu() //Vistad de todos los equipos
        {
            return View();
        }

        //Administrar trafo y lineas

        public ActionResult AdministrarIndisponibilidadRemuLinea() //Administrar Linea Remunerada (remuneracion , fecha y resolucion)
        {
            return View();
        }
        public ActionResult AdministrarIndisponibilidadRemuTrafo() //Administrar Transformador Remunerada (remuneracion , fecha)
        {
            return View();
        }

        //Historiales

        public ActionResult HistorialMontosLinea() // Historial Lineas
        {
            return View();
        }
        public ActionResult HistorialMontosTrafos() // Historial Lineas
        {
            return View();
        }
        public ActionResult HistorialMontosPtosC() // Historial PtosC
        {
            return View();
        }
        public ActionResult HistorialMontosCapacitor() // Historial Capacitor
        {
            return View();
        }


        public ActionResult IndisponibilidadLinea()  //Lineas Indisponibles
        {
            return View();
        }

        public ActionResult IndisponibilidadTrafo()  //Trafo Indisponibles
        {
            return View();
        }

        public ActionResult IndisponibilidadPtosConexion()  //PtosC Indisponibles
        {
            return View();
        }
        public ActionResult IndisponibilidadCapacitor()  //Capacitor Indisponibles
        {
            return View();
        }


    }
}
