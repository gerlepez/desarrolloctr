﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class TipoLineaVM
    {
        public int? idTipoLinea { get; set; }
        public string nombreTipoLinea { get; set; }
        public bool? activo { get; set; }
    }
}
