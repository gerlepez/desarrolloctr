﻿using System;

namespace DesarrolloCTRServicios.ViewModels
{
    public class HorarioCtrVM
    {
        public int? idTurnoCtr { get; set; }
        public TimeSpan? horaEntrada { get; set; }
        public TimeSpan? horaSalida { get; set; }
        public string nombreTurno { get; set; }
    }
}
