//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DesarrolloCTRModelos
{
    using System;
    using System.Collections.Generic;
    
    public partial class AutAutomatismo
    {
        public int idAutomatismo { get; set; }
        public string nombreAutomatismo { get; set; }
        public Nullable<bool> activo { get; set; }
        public Nullable<bool> TieneEquipo { get; set; }
        public Nullable<System.DateTime> fechaCreacion { get; set; }
        public Nullable<System.DateTime> fechaEdicion { get; set; }
    }
}
