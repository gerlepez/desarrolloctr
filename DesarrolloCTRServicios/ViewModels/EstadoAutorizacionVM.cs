﻿namespace DesarrolloCTRServicios.ViewModels
{

    public class EstadoAutorizacionVM
    {
        public int? idEstadoAutorizacion { get; set; }
        public string nombreEstadoAutorizacion { get; set; }
        public bool? activo { get; set; }
    }
}

