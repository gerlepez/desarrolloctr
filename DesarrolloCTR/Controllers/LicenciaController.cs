﻿using DesarrolloCTR.Controllers.Extra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers
{
    public class LicenciaController : Controller
    {
        [SessionUtility]
        [Permiso("LIC")]
        // GET: Licencia
        public ActionResult Licencia()
        {
            return View();
        }

        public ActionResult AdministrarLicencia()
        {
            return View();
        }
    }
}