﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class InfraestructuraVM
    {
        public int? idInfra { get; set; }
        public string nombreInfra { get; set; }
        public string codigoInfraestructura { get; set; }
        public bool? activo { get; set; }
        public int? idTipoIfra { get; set; }

        public TipoInfraestructuraVM tipoInfra { get; set; }

    }
}
