﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class MenuVM
    {
        public int? idMenu { get; set; }
        public string nombreMenu { get; set; }
        public string descripcionMenu { get; set; }
        public string codigoMenu { get; set; }
        public string urlMenu { get; set; }
        public string icono { get; set; }
        public int? posicion { get; set; }
        public string idHtml { get; set; }
        public bool? activo { get; set; }
        public int codigoRetorno { get; set; }

        public bool? tienePermisoLectura { get; set; }
        public bool? tienePermisoEliminacion { get; set; }
        public bool? tienePermisoEdicion { get; set; }
        public int idRol { get; set; }
        public int? idAccesoRol { get; set; }

        public RolVM rol { get; set; }

    }
}
