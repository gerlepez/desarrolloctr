﻿using DesarrolloCTR.Controllers.Extra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Threading;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers
{

    [SessionUtilityAttribute]
    [Permiso("NOV")]
    public class NovedadController : Controller
    {

        public ActionResult Novedad()
        {
            //string nombreUsuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            //var user = nombreUsuario.Remove(0, 11);
            //Session["usuarioLogeado"] = user;

            //IPrincipal p = Thread.CurrentPrincipal;
            //IIdentity i = Thread.CurrentPrincipal.Identity;
            //if (i.Name != "")
            //{
            //    var userSplit = i.Name.Split('\\');
            //    Session["usuarioLogeado"] = userSplit;
            //}
            //else
            //{
            //    var usuario = WebConfigurationManager.AppSettings["usuario"].ToString();
            //    Session["usuarioLogeado"] = usuario;
            //}


            return View();
        }
        [Permiso("ADMNOV")]
        public ActionResult AdministrarNovedad()
        {

            //string nombreUsuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            //var user = nombreUsuario.Remove(0, 11);
            //Session["usuarioLogeado"] = user;

            //IPrincipal p = Thread.CurrentPrincipal;
            //IIdentity i = Thread.CurrentPrincipal.Identity;
            //if (i.Name != "")
            //{
            //    var userSplit = i.Name.Split('\\');
            //    Session["usuarioLogeado"] = userSplit;
            //}
            //else
            //{
            //    var usuario = WebConfigurationManager.AppSettings["usuario"].ToString();
            //    Session["usuarioLogeado"] = usuario;
            //}


            return View();
        }

        public ActionResult AdministrarLicencia()
        {

            //string nombreUsuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            //var user = nombreUsuario.Remove(0, 11);
            //Session["usuarioLogeado"] = user;

            //IPrincipal p = Thread.CurrentPrincipal;
            //IIdentity i = Thread.CurrentPrincipal.Identity;

            //if (i.Name != "")
            //{
            //    var userSplit = i.Name.Split('\\');
            //    Session["usuarioLogeado"] = userSplit;
            //}
            //else
            //{
            //    var usuario = WebConfigurationManager.AppSettings["usuario"].ToString();
            //    Session["usuarioLogeado"] = usuario;
            //}


            return View();
        }

        public ActionResult AdministrarTipoNovedad()
        {
            //string nombreUsuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            //var user = nombreUsuario.Remove(0, 11);
            //Session["usuarioLogeado"] = user;
            return View();
        }

        public ActionResult TipoNovedad()
        {
            //string nombreUsuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            //var user = nombreUsuario.Remove(0, 11);
            //Session["usuarioLogeado"] = user;
            return View();
        }

        public ActionResult GetView(string viewName)
        {

            if (viewName == "trazabilidad")
            {
                viewName = "TrazabilidadNovedad";
            }
            if (viewName == "trazabilidad")
            {
                viewName = "TrazabilidadNovedad";
            }

            return PartialView(viewName);
        }

        public ActionResult AdministrarAutorizacionTrabajo()
        {
            //string nombreUsuario = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            //var user = nombreUsuario.Remove(0, 11);
            //Session["usuarioLogeado"] = user;
            return View();
        }

        public ActionResult VisualizarNovedad()
        {
            return View();
        }

    }
}
