﻿using DesarrolloCTRModelos;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioEstadoTiempo
    {
        public object ObtenerListaEstadoTiempo()
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                object listaEstados = db.EstadoTiempo.ToList();
                return listaEstados;
            }
        }
    }
}
