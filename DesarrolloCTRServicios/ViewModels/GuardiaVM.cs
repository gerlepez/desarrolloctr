﻿using System;

namespace DesarrolloCTRServicios.ViewModels
{
    public class GuardiaVM
    {
        public int idGuardia { get; set; }
        public int? idMantenimiento { get; set; }
        public int? idPersona { get; set; }
        public int? idTipoGuardia { get; set; }
        public int? codigoRetorno { get; set; }
        public int? idTipoTurno { get; set; }
        public int? idJefe { get; set; }
        public int? idPersona2 { get; set; }

        public int validacionCantPersonas { get; set; }
        public int validacionGuardiaPorPersona { get; set; }
        public int validacionCantDias { get; set; }

        public string telefono { get; set; }
        public string telefono2 { get; set; }
        public string nombreGuardia { get; set; }
        public string color { get; set; }
        public string valCantPersonasString { get; set; }
        public string valTurnoPorPersona { get; set; }
        public string valCantDias { get; set; }
        public string usuario { get; set; }
        public string tipoCRUD { get; set; }
        public string nombreMantenimiento { get; set; }
        public string nombrePersona { get; set; }
        public string nombreJefe { get; set; }
        public string usuario2 { get; set; }

        public bool? activo { get; set; }

        public DateTime? fechaDesde { get; set; }
        public DateTime? fechaHasta { get; set; }
        public TimeSpan? horaDesde { get; set; }
        public TimeSpan? horaHasta { get; set; }
        public DateTime? fechaHoraApertura { get; set; }
        public DateTime? fechaHoraCierre { get; set; }

        public UsuarioVM personal { get; set; }
        public UsuarioVM personal2 { get; set; }
        public TipoTurnoVM tipoTurno { get; set; }
        public MantenimientoVM mantenimiento { get; set; }


    }

}
