﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioAutorizacionTrabajo
    {
        public List<AutorizacionTrabajoVM> ObtenerListaAutorizacionTrabajo(int? idLicencia)
        {
            List<AutorizacionTrabajoVM> listaAutorizacionTrabajo;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                using (BuhoGestionEntities db2 = new BuhoGestionEntities())
                {
                    listaAutorizacionTrabajo = (from a in db.LicAutorizacionTrabajo
                                                join l in db.LicLicencia on a.idLicencia equals l.idLicencia
                                                join nov in db.NovNovedad on idLicencia equals nov.idLicencia
                                                join e in db.LicEstadoAutorizacion on a.idEstadoActual equals e.idEstadoAutorizacion
                                                //join a in db.LicAutorizacionTrabajo on l.idLicencia equals a.idLicencia
                                                where l.activo == true && a.idLicencia == idLicencia && a.activo == true
                                                select new AutorizacionTrabajoVM
                                                {
                                                    idAutorizacionTrabajo = a.idAutorizacionTrabajo,
                                                    descripcionAutorizacionTrabajo = a.descripcion,
                                                    idLicencia = a.idLicencia,
                                                    fechaHoraInicioAutorizacionTrabajo = a.fechaHoraInicio,
                                                    fechaHoraFinAutorizacionTrabajo = a.fechaHoraFin,
                                                    //fechaInicio = a.fechaHoraInicio,
                                                    idEmpleado = a.idUsuario,
                                                    codigoAutorizacion = a.codigoAutorizacion,

                                                    estadoAutorizacion = new EstadoAutorizacionVM
                                                    {
                                                        activo = e.activo,
                                                        idEstadoAutorizacion = e.idEstadoAutorizacion,
                                                        nombreEstadoAutorizacion = e.nombreEstadoAutorizacion
                                                    }
                                                }).Where(x => x.idAutorizacionTrabajo > 0).Distinct().ToList();
                    ServicioUsuario servUsuario = new ServicioUsuario();
                    foreach (var item in listaAutorizacionTrabajo)
                    {

                        var buscarUnUsuario = servUsuario.ObtenerDatosUsuarioPorId(item.idEmpleado);
                        if (buscarUnUsuario != null)
                        {
                            item.nombreAutorizado = buscarUnUsuario.nombreUsuario;
                        }
                    }
                }
            }

            return listaAutorizacionTrabajo;
        }

        public AutorizacionTrabajoVM ConsultarUnaAutorizacionTrabajo(int idAutorizacionTrabajo)
        {
            AutorizacionTrabajoVM autorizacionTrabajo;

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                using (BuhoGestionEntities db2 = new BuhoGestionEntities())
                {
                    autorizacionTrabajo = (from c in db.LicAutorizacionTrabajo
                                           join d in db.LicLicencia on c.idLicencia equals d.idLicencia
                                           where c.activo == true & c.idAutorizacionTrabajo == idAutorizacionTrabajo
                                           select new AutorizacionTrabajoVM
                                           {
                                               idAutorizacionTrabajo = c.idAutorizacionTrabajo,
                                               descripcionAutorizacionTrabajo = c.descripcion,
                                               fechaHoraInicioAutorizacionTrabajo = c.fechaHoraInicio,
                                               fechaHoraFinAutorizacionTrabajo = c.fechaHoraFin,
                                               activo = c.activo,
                                               idLicencia = c.idLicencia,
                                               idUsuario = c.idUsuario,
                                           }).FirstOrDefault();

                    if (autorizacionTrabajo != null)
                    {
                        var buscarUnUsuario = (from c in db2.GralUsuario
                                               where c.idUsuario == autorizacionTrabajo.idUsuario
                                               select new AutorizacionTrabajoVM
                                               {
                                                   idUsuario = c.idUsuario,
                                                   nombresuario = c.Nombre
                                               }).FirstOrDefault();

                        if (buscarUnUsuario != null)
                        {
                            autorizacionTrabajo.nombresuario = buscarUnUsuario.nombresuario;
                        }
                    }

                }
            }

            return autorizacionTrabajo;
        }

        public AutorizacionTrabajoVM AltaAutorizacionTrabajo(AutorizacionTrabajoVM autorizacionTrabajo, int idUsuario)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {

                DateTime fecha1 = (DateTime)autorizacionTrabajo.fechaInicio;
                DateTime date = fecha1.AddHours(autorizacionTrabajo.horaInicio.Hour).AddMinutes(autorizacionTrabajo.horaInicio.Minute);
                DateTime date1 = fecha1.AddHours(autorizacionTrabajo.horaFin.Hour).AddMinutes(autorizacionTrabajo.horaFin.Minute);
                var licencia = db.LicLicencia.Where(x => x.idLicencia == autorizacionTrabajo.idLicencia).FirstOrDefault();
                var codigoCorto = licencia.codigoLicencia.Split('/')[0];

                var ultimaAut = db.LicAutorizacionTrabajo
                    .Where(x => x.idLicencia == autorizacionTrabajo.idLicencia)
                    .OrderByDescending(x => x.idAutorizacionTrabajo).FirstOrDefault();
                var numeroAutorizacion = "01";
                if (ultimaAut != null)
                {
                    numeroAutorizacion = (ultimaAut.numAutorizacion + 1).ToString();

                }
                int count = numeroAutorizacion.Length;
                switch (count)
                {
                    case 1:
                        numeroAutorizacion = codigoCorto + "/" + "0" + numeroAutorizacion;
                        break;
                    case 2:
                        numeroAutorizacion = codigoCorto + "/" + numeroAutorizacion;
                        break;
                }
                try
                {
                    LicAutorizacionTrabajo aut = new LicAutorizacionTrabajo
                    {
                        descripcion = autorizacionTrabajo.descripcionAutorizacionTrabajo,
                        fechaHoraInicio = date,
                        fechaHoraFin = date1,
                        idLicencia = autorizacionTrabajo.idLicencia,
                        activo = true,
                        idEstadoActual = 1,
                        idUsuario = autorizacionTrabajo.idEmpleado,
                        codigoAutorizacion = numeroAutorizacion,
                        numAutorizacion = ultimaAut == null ? 1 : ultimaAut.numAutorizacion + 1

                    };
                    db.LicAutorizacionTrabajo.Add(aut);
                    db.SaveChanges();

                    var autGuardada = db.LicAutorizacionTrabajo.OrderByDescending(x => x.idAutorizacionTrabajo)
                        .FirstOrDefault();

                    //trackeo historial autorizaciones
                    LicHistorialEstadoAut histAut = new LicHistorialEstadoAut
                    {
                        idUsuario = idUsuario,
                        idAutorizacion = autGuardada.idAutorizacionTrabajo,
                        idEstadoAut = 1,
                        fechaHistEstAut = DateTime.Now
                    };
                    db.LicHistorialEstadoAut.Add(histAut);

                    db.SaveChanges();
                    AutorizacionTrabajoVM autVM = new AutorizacionTrabajoVM();
                    autVM.codigoAutorizacion = aut.codigoAutorizacion;
                    autVM.codigoResultadoAlta = 200;
                    return autVM;
                }
                catch (Exception e)
                {
                    AutorizacionTrabajoVM autVM = new AutorizacionTrabajoVM();
                    autVM.codigoResultadoAlta = 404;
                    return autVM;
                }



            }
        }

        public AutorizacionTrabajoVM ModificarAutorizacionTrabajo(AutorizacionTrabajoVM autorizacionTrabajo, int idUsuario)
        {
            AutorizacionTrabajoVM autVM = new AutorizacionTrabajoVM();
            //todo verificar si realmente es una modificacion
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                DateTime fecha1 = (DateTime)autorizacionTrabajo.fechaInicio;
                DateTime date = fecha1.AddHours(autorizacionTrabajo.horaInicio.Hour).AddMinutes(autorizacionTrabajo.horaInicio.Minute);

                DateTime date2 = fecha1.AddHours(autorizacionTrabajo.horaFin.Hour).AddMinutes(autorizacionTrabajo.horaFin.Minute);

                var consultaunAutorizacionTrabajo = db.LicAutorizacionTrabajo.Where(x => x.idAutorizacionTrabajo == autorizacionTrabajo.idAutorizacionTrabajo).FirstOrDefault();
                if (consultaunAutorizacionTrabajo != null)
                {
                    consultaunAutorizacionTrabajo.descripcion = autorizacionTrabajo.descripcionAutorizacionTrabajo;
                    consultaunAutorizacionTrabajo.activo = true;
                    consultaunAutorizacionTrabajo.fechaHoraInicio = date;
                    consultaunAutorizacionTrabajo.fechaHoraFin = date2;
                    consultaunAutorizacionTrabajo.idLicencia = autorizacionTrabajo.idLicencia;
                    consultaunAutorizacionTrabajo.activo = true;
                    consultaunAutorizacionTrabajo.idUsuario = autorizacionTrabajo.idEmpleado;
                    //consultaunAutorizacionTrabajo.codigoAutorizacion = autorizacionTrabajo;

                    //       autVM.codigoAutorizacion = consultaunAutorizacionTrabajo.codigoAutorizacion;
                    autVM.codigoResultadoAlta = 200;
                    db.SaveChanges();
                }
                else
                {
                    AltaAutorizacionTrabajo(autorizacionTrabajo, idUsuario);
                }
            }
            return autVM;
        }

        public int EliminarAutorizacionTrabajo(AutorizacionTrabajoVM autorizacionTrabajo)
        {
            //todo verificar si se elimina realmente o si es modificacion
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunAutorizacionTrabajo = db.LicAutorizacionTrabajo.Where(x => x.idAutorizacionTrabajo == autorizacionTrabajo.idAutorizacionTrabajo).FirstOrDefault();
                if (consultaunAutorizacionTrabajo != null)
                {
                    consultaunAutorizacionTrabajo.activo = false;
                    db.SaveChanges();
                }
                else
                {
                    return 300;
                }
            }
            return 200;
        }

        public AutorizacionTrabajoVM CerrarAutorizacion(AutorizacionTrabajoVM aut)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                AutorizacionTrabajoVM autVM = new AutorizacionTrabajoVM();
                var consultaunaAutorizacion = db.LicAutorizacionTrabajo.Find(aut.idAutorizacionTrabajo);
                var consultarNovedad = (from n in db.NovNovedad
                                        where n.idLicencia == aut.idLicencia
                                        select n).FirstOrDefault();
                if (consultaunaAutorizacion != null)
                {
                    if (consultaunaAutorizacion.activo == true &&
                        (consultaunaAutorizacion.idEstadoActual == 1))
                    {
                        consultaunaAutorizacion.idEstadoActual = 2;
                        DateTime fecha = (DateTime)aut.fechaFin;
                        DateTime hora = (DateTime)aut.horaFin;
                        DateTime date = fecha.AddHours(hora.Hour).AddMinutes(hora.Minute);
                        consultaunaAutorizacion.fechaHoraFin = date;
                        //trackeo historial autorizaciones
                        LicHistorialEstadoAut histAut = new LicHistorialEstadoAut
                        {
                            idUsuario = aut.idUsuario,
                            idAutorizacion = consultaunaAutorizacion.idAutorizacionTrabajo,
                            idEstadoAut = 2,
                            fechaHistEstAut = DateTime.Now
                        };
                        db.LicHistorialEstadoAut.Add(histAut);
                        db.SaveChanges();
                        autVM.codResultadoCierre = 200;
                        autVM.idNovedad = consultarNovedad.idNovedad;
                        autVM.codigoAutorizacion = consultaunaAutorizacion.codigoAutorizacion;
                        return autVM;
                    }
                    else
                    {
                        autVM.codResultadoCierre = 600;
                        return autVM; //en este caso dicha autorizacion ya esta cerrada o eliminada
                    }
                }
                else
                {
                    autVM.codResultadoCierre = 400;
                    return autVM; //en este caso la autorizacion no existe
                }

            }
        }

        public int RecuperarAutorizacionTrabajo(AutorizacionTrabajoVM autorizacionTrabajo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaunAutorizacionTrabajo = db.LicAutorizacionTrabajo.Where(x => x.idAutorizacionTrabajo == autorizacionTrabajo.idAutorizacionTrabajo).FirstOrDefault();
                if (consultaunAutorizacionTrabajo != null)
                {
                    consultaunAutorizacionTrabajo.activo = true;
                    db.SaveChanges();
                }
                else
                {
                    return 300;
                }
            }
            return 200;
        }

        public bool ValidarAutorizacionTrabajo(AutorizacionTrabajoVM autorizacionTrabajo)
        {
            if ((autorizacionTrabajo.idEmpleado != 0 || autorizacionTrabajo.idEmpleado != null) &&
                autorizacionTrabajo.fechaInicio != null && autorizacionTrabajo.horaInicio != null
                )
            {
                return true;
            }
            else
            {
                return false;
            }
        }



    }
}
