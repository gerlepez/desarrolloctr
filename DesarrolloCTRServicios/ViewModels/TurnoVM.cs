﻿using System;

namespace DesarrolloCTRServicios.ViewModels
{
    public class TurnoVM
    {
        public int idTurno { get; set; }
        public int? idPersona { get; set; }
        public int? idTipoTurno { get; set; }
        public int? codigoRetorno { get; set; }
        public int validacionCantPersonas { get; set; }
        public string valCantPersonasString { get; set; }
        public int validacionTurnoPorPersona { get; set; }
        public string valTurnoPorPersona { get; set; }
        public int validacionCantDias { get; set; }
        public string valCantDias { get; set; }
        public string usuario { get; set; }
        public string color { get; set; }
        public DateTime? fechaDesde { get; set; }
        public DateTime? fechaHasta { get; set; }
        public DateTime? horaDesde { get; set; }
        public DateTime? horaHasta { get; set; }
        public DateTime? fechaHoraApertura { get; set; }
        public DateTime? fechaHoraCierre { get; set; }
        public string tipoCRUD { get; set; }
        public bool? activo { get; set; }
        public TipoTurnoVM tipoTurno { get; set; }
        public MantenimientoVM mantenimiento { get; set; }
        public UsuarioVM personal { get; set; }

        public string nombreJefeT { get; set; }
        public string nombreOperadorT { get; set; }

        public int mes { get; set; }
        public int ano { get; set; }
        public int idAcompañantee { get; set; }

        public int idTurnoCTR { get; set; }
        public int? idEmpleadoCTR { get; set; }
        public string nombreCTR { get; set; }
        public DateTime? fecha { get; set; }

        public TimeSpan? horaEntrada { get; set; }
        public TimeSpan? horaSalida { get; set; }
    }
}
