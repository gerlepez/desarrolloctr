﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioTurno
    {
        public object ObtenerListaTipoTurno(bool activo)
        {
            using (BuhoGestionEntities db = new BuhoGestionEntities())
            {
                List<TipoTurnoVM> tipoTurno;
                tipoTurno = (from c in db.RecTurnoCtr
                             select new TipoTurnoVM
                             {
                                 idTurnoCtr = c.idTurnoCtr,
                                 nombreTurno = c.nombreTurno

                             }).ToList();


                object json = new { data = tipoTurno };
                return json;
            }

        }
        public object ObtenerListaMantenimiento(bool activo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                List<MantenimientoVM> mantenimiento;
                mantenimiento = (from c in db.TurnMantenimiento
                                 where c.activo == activo
                                 select new MantenimientoVM
                                 {
                                     activo = c.activo,
                                     idMantenimiento = c.idMantenimiento,
                                     nombreMantenimiento = c.nombreMantenimiento

                                 }).ToList();

                object json = new { data = mantenimiento };
                return json;
            }

        }
        public object ObtenerListaGuardia(int idMantenimiento)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                List<GuardiaVM> guardia;
                guardia = (from c in db.TurnGuardia
                           join d in db.TurnMantenimiento on c.idMantenimiento equals d.idMantenimiento
                           where c.activo == true && c.idMantenimiento == idMantenimiento
                           select new GuardiaVM
                           {
                               activo = c.activo,
                               idGuardia = c.idGuardia,
                               nombreGuardia = c.nombreGuardia,
                               idMantenimiento = c.idMantenimiento

                           }).ToList();

                object json = new { data = guardia };
                return json;
            }
        }
        public object ObtenerTurnosAbiertos(int idUsuarioLogueado)
        {
            using (BuhoGestionEntities buhoGestion = new BuhoGestionEntities())
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    ServicioUsuario servUsuario = new ServicioUsuario();
                    List<TurnoEjecucionVM> listaTurnosEjecucion = new List<TurnoEjecucionVM>();
                    var rolUsuario = db.GralUsuarioRol.Where(x => x.idUsuario == idUsuarioLogueado).FirstOrDefault();
                    //      var rol = db.GralRol.Where(x => x.idRol == rolUsuario.idRol).FirstOrDefault();
                    //if (rol.esOperador == true)
                    //{
                    //    var TurnoCompa = db.TurnTurnoAcompañante.Where(x => x.idAcompañante == idUsuarioLogueado && x.activo == true).FirstOrDefault();                    
                    //    if (TurnoCompa != null)
                    //    {
                    //        listaTurnosEjecucion = (from t in db.TurnTurnoEjecucion
                    //                                where /*t.idJefeTurno == idUsuarioLogueado && */t.fechaHoraApertura != null && t.fechaHoraCierre == null && t.activo == true
                    //                                select new TurnoEjecucionVM
                    //                                {
                    //                                    idTurnoEjecucion = t.idTurnoEjecucion,
                    //                                    fechaHoraApertura = t.fechaHoraApertura,
                    //                                    idTipoTurno = t.idTipoTurno,
                    //                                    idJefeTurno = t.idJefeTurno,
                    //                                }).ToList();
                    //        var listaTurnosCtr = buhoGestion.RecTurnoCtr.ToList();
                    //        foreach (var turno in listaTurnosEjecucion)
                    //        {
                    //            turno.nombreJefeTurno = servUsuario.ObtenerDatosUsuarioPorId(turno.idJefeTurno).nombreUsuario;
                    //            foreach (var turnoCtr in listaTurnosCtr)
                    //            {
                    //                if (turno.idTipoTurno == turnoCtr.idTurnoCtr)
                    //                {
                    //                    turno.nombreTipoTurno = turnoCtr.nombreTurno;
                    //                    turno.horaDesdeTipoTurno = turnoCtr.horaEntrada;
                    //                    turno.horaHastaTipoTurno = turnoCtr.horaSalida;

                    //                }
                    //            }
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    listaTurnosEjecucion = (from t in db.TurnTurnoEjecucion
                                            where /*t.idJefeTurno == idUsuarioLogueado && */t.fechaHoraApertura != null && t.fechaHoraCierre == null && t.activo == true
                                            select new TurnoEjecucionVM
                                            {
                                                idTurnoEjecucion = t.idTurnoEjecucion,
                                                fechaHoraApertura = t.fechaHoraApertura,
                                                idTipoTurno = t.idTipoTurno,
                                                idJefeTurno = t.idJefeTurno,

                                            }).ToList();

                    var listaTurnosCtr = buhoGestion.RecTurnoCtr.ToList();
                    foreach (var turno in listaTurnosEjecucion)
                    {
                        var turnoAcompañate = db.TurnTurnoAcompañante.Where(x => x.idTurnoEjecucion == turno.idTurnoEjecucion && x.activo == true).FirstOrDefault();
                        turno.nombreJefeTurno = servUsuario.ObtenerDatosUsuarioPorId(turno.idJefeTurno).nombreUsuario;
                        turno.nombreAcompañante = servUsuario.ObtenerDatosUsuarioPorId(turnoAcompañate.idAcompañante).nombreUsuario;
                        foreach (var turnoCtr in listaTurnosCtr)
                        {
                            if (turno.idTipoTurno == turnoCtr.idTurnoCtr)
                            {
                                turno.nombreTipoTurno = turnoCtr.nombreTurno;
                                turno.horaDesdeTipoTurno = turnoCtr.horaEntrada;
                                turno.horaHastaTipoTurno = turnoCtr.horaSalida;
                            }
                        }
                    }
                    //}

                    object json = new { data = listaTurnosEjecucion };
                    return json;
                }
            }
        }
        public object ObtenerTurnos(string fechaHoraDesde, string fechaHoraHasta, int idUsuarioLogueado)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                using (BuhoGestionEntities db2 = new BuhoGestionEntities())
                {
                    var fechaDateDesde = DateTime.ParseExact(fechaHoraDesde, "dd/MM/yyyy", null);
                    var fechaDateHasta = DateTime.ParseExact(fechaHoraHasta, "dd/MM/yyyy", null);


                    List<TurnoVM> turnoFiltrada = new List<TurnoVM>();
                    IQueryable<TurnoVM> turno = (//from t in db.TurnTurno
                                                 from t in db2.RecHorarioCtr
                                                 join g in db2.GralUsuario on t.idEmpleado equals g.idUsuario
                                                 join tt in db2.RecTurnoCtr on t.idTurno equals tt.idTurnoCtr
                                                 where t.activo == true && fechaDateHasta >= t.fecha && fechaDateDesde <= t.fecha
                                                 select new TurnoVM
                                                 {
                                                     activo = t.activo,
                                                     codigoRetorno = 200,
                                                     fechaDesde = t.fecha,
                                                     fechaHasta = t.fecha,
                                                     idPersona = t.idEmpleado,
                                                     nombreCTR = g.Nombre,
                                                     idTipoTurno = t.idTurno,
                                                     idTurno = t.idHorarioCtr,


                                                     horaEntrada = tt.horaEntrada,
                                                     horaSalida = tt.horaSalida,


                                                 }).OrderByDescending(x => x.idTurno).AsQueryable();
                    ServicioUsuario servUsuario = new ServicioUsuario();
                    var turnos = turno.ToList();
                    foreach (var item in turnos)
                    {

                        if (item.idTipoTurno == 1 || item.idTipoTurno == 4 || item.idTipoTurno == 5 || item.idTipoTurno == 6 || item.idTipoTurno == 7)
                        {
                            var fechaHastaStringNoche = Convert.ToDateTime(item.fechaHasta).AddDays(1).ToString().Split(' ')[0];
                            var horaEntradaString = item.horaEntrada.ToString();
                            var horaSalidaString = item.horaSalida.ToString();
                            var fechaEntradaString = item.fechaDesde.ToString().Split(' ')[0];

                            DateTime fechaDesdeFinal = Convert.ToDateTime(fechaEntradaString + ' ' + horaEntradaString);
                            item.fechaDesde = fechaDesdeFinal;

                            DateTime fechaHastaFinal = Convert.ToDateTime(fechaHastaStringNoche + ' ' + horaSalidaString);
                            item.fechaHasta = fechaHastaFinal;
                        }
                        else
                        {
                            var fechaHastaString = item.fechaHasta.ToString().Split(' ')[0];
                            var horaEntradaString = item.horaEntrada.ToString();
                            var horaSalidaString = item.horaSalida.ToString();
                            var fechaEntradaString = item.fechaDesde.ToString().Split(' ')[0];

                            DateTime fechaDesdeFinal = Convert.ToDateTime(fechaEntradaString + ' ' + horaEntradaString);
                            item.fechaDesde = fechaDesdeFinal;

                            DateTime fechaHastaFinal = Convert.ToDateTime(fechaHastaString + ' ' + horaSalidaString);
                            item.fechaHasta = fechaHastaFinal;
                        }


                        var buscarUnUsuario = servUsuario.ObtenerDatosUsuarioPorId(item.idPersona);
                        if (buscarUnUsuario != null)
                        {
                            item.personal = buscarUnUsuario;
                        }
                        var consultarUnTipoTurno = db2.RecTurnoCtr.Find(item.idTipoTurno);
                        if (item.idPersona == idUsuarioLogueado)
                        {
                            item.color = consultarUnTipoTurno.colorMain;

                        }
                        else
                        {
                            item.color = consultarUnTipoTurno.colorSecundary;

                        }
                    }
                    return turnos;
                }
            }

        }
        public IEnumerable<DateTime> EachCalendarDay(DateTime startDate, DateTime endDate)
        {
            for (var date = startDate.Date; date.Date <= endDate.Date; date = date.AddDays(1)) yield
            return date;
        }
        public TurnoVM CRUD(TurnoVM turno, int? idUsuario)
        {
            TurnoVM unTurno = new TurnoVM()
            {
                idTurno = 0,
                codigoRetorno = 400
            };
            if (turno.tipoCRUD != "D")
            {
                unTurno.validacionCantPersonas = ValidarCantidadPersonasPorGuardia(turno);
                unTurno.validacionTurnoPorPersona = ValidarTurnoPersona(turno);
                unTurno.validacionCantDias = ValidarCantidadDias(turno);
            }
            try
            {
                using (BuhoGestionEntities buhodb = new BuhoGestionEntities())
                {
                    using (IntranetCTREntities db = new IntranetCTREntities())
                    {
                        if (turno.tipoCRUD == "D")
                        {
                            unTurno.codigoRetorno = EliminarTurno(turno);
                            if (unTurno.codigoRetorno == 200)
                            {
                                unTurno.codigoRetorno = GenerarHistorialTurno(turno, "ELI", idUsuario);
                            }
                            unTurno.idTurno = turno.idTurno;
                            return unTurno;

                        }

                        if (turno.tipoCRUD == "C")
                        {
                            List<DateTime> listaDias = new List<DateTime>();
                            foreach (DateTime day in EachCalendarDay(Convert.ToDateTime(turno.fechaDesde), Convert.ToDateTime(turno.fechaHasta)))
                            {
                                listaDias.Add(day);
                            }
                            var horaDesdee = Convert.ToDateTime(turno.fechaDesde).ToString("HH:mm");
                            var horaHastaa = Convert.ToDateTime(turno.fechaHasta).ToString("HH:mm");
                            foreach (DateTime dia in listaDias)
                            {
                                if (turno.idTipoTurno == 1 || turno.idTipoTurno == 4 || turno.idTipoTurno == 5 || turno.idTipoTurno == 6 || turno.idTipoTurno == 7)
                                {
                                    var esteDia = dia.ToString("dd/MM/yyyy");
                                    var proximoDia = dia.AddDays(1).ToString("dd/MM/yyyy");
                                    var fechaDesde = esteDia + " " + horaDesdee;
                                    var fechaHasta = proximoDia + " " + horaHastaa;
                                    var fechaConvertidaDesde = DateTime.Parse(fechaDesde);
                                    var fechaConvertidaHasta = DateTime.Parse(fechaHasta);
                                    TurnTurno turnoo = new TurnTurno
                                    {
                                        activo = true,
                                        fechaHoraDesde = fechaConvertidaDesde,
                                        fechaHoraHasta = fechaConvertidaHasta,
                                        idPersona = turno.idPersona,
                                        idTipoTurno = turno.idTipoTurno,
                                        fechaCreacion = DateTime.Today
                                    };
                                    db.TurnTurno.Add(turnoo);
                                    //Aca nico xd

                                    //var fecha = dia.ToString("yyyy/MM/dd");               
                                    //    var fechaConvertida = Da
                                    var mes = dia.ToString("MM");
                                    var mesConvertido = Int16.Parse(DateTime.Now.ToString(mes));
                                    var ano = dia.ToString("yyyy");
                                    var anoConvertido = Int16.Parse(DateTime.Now.ToString(ano));
                                    RecHorarioCtr horarioCTR = new RecHorarioCtr
                                    {
                                        activo = true,
                                        fecha = dia,
                                        mes = mesConvertido,
                                        ano = anoConvertido,
                                        idEmpleado = turno.idPersona,
                                        idTurno = turno.idTipoTurno
                                    };
                                    buhodb.RecHorarioCtr.Add(horarioCTR);
                                }
                                else
                                {
                                    var esteDia = dia.ToString("dd/MM/yyyy");
                                    var fechaDesde = esteDia + " " + horaDesdee;
                                    var fechaHasta = esteDia + " " + horaHastaa;
                                    var fechaConvertidaDesde = DateTime.Parse(fechaDesde);
                                    var fechaConvertidaHasta = DateTime.Parse(fechaHasta);
                                    TurnTurno turnoo = new TurnTurno
                                    {
                                        activo = true,
                                        fechaHoraDesde = fechaConvertidaDesde,
                                        fechaHoraHasta = fechaConvertidaHasta,
                                        idPersona = turno.idPersona,
                                        idTipoTurno = turno.idTipoTurno,
                                        fechaCreacion = DateTime.Today
                                    };
                                    db.TurnTurno.Add(turnoo);

                                    //Y aca tambien xD
                                    var fechaConvertida = DateTime.Parse(esteDia);
                                    // fechaConvertida =  DateTime.Now.Date;
                                    var mes = dia.ToString("MM");
                                    var mesConvertido = Int16.Parse(DateTime.Now.ToString(mes));
                                    var ano = dia.ToString("yyyy");
                                    var anoConvertido = Int16.Parse(DateTime.Now.ToString(ano));
                                    RecHorarioCtr horarioCTR = new RecHorarioCtr
                                    {
                                        activo = true,
                                        fecha = fechaConvertida,
                                        mes = mesConvertido,
                                        ano = anoConvertido,
                                        idEmpleado = turno.idPersona,
                                        idTurno = turno.idTipoTurno
                                    };
                                    buhodb.RecHorarioCtr.Add(horarioCTR);

                                }

                            }
                            db.SaveChanges();
                            buhodb.SaveChanges();
                            //TurnTurno tur = new TurnTurno
                            //{
                            //    activo = true,
                            //    fechaHoraDesde = turno.fechaDesde,
                            //    fechaHoraHasta = turno.fechaHasta,
                            //    idPersona = turno.idPersona,
                            //    idTipoTurno = turno.idTipoTurno,
                            //    fechaCreacion = DateTime.Today
                            //};
                            //db.TurnTurno.Add(tur);
                            //db.SaveChanges();

                            //DateTime fechaHoraInicio = (DateTime)tur.fechaHoraDesde;
                            //DateTime fechaHoraFin = (DateTime)tur.fechaHoraHasta;

                            //while (fechaHoraInicio <= fechaHoraFin)
                            //{
                            //    var fechaDesde = DateTime.Parse(String.Format("{0:dd/MM/yyyy}", fechaHoraInicio));
                            //    var fechaHasta = DateTime.Parse(String.Format("{0:dd/MM/yyyy}", fechaHoraFin));
                            //    var horaDesde = String.Format("{0:HH:mm}", fechaHoraInicio);
                            //    var horaHasta = String.Format("{0:HH:mm}", fechaHoraFin);
                            //    var diffHor = TimeSpan.Parse(horaHasta) - TimeSpan.Parse(horaDesde);

                            //    var dHoras = diffHor.Hours;
                            //    if (dHoras < 0)
                            //    {
                            //        dHoras = dHoras + 24;
                            //    }

                            //    var dMins = diffHor.Minutes;
                            //    TurnTurnoDetalle detalleTurno = new TurnTurnoDetalle
                            //    {
                            //        idTurno = tur.idTurno,
                            //        fechaDesde = fechaDesde,
                            //        fechaHasta = fechaDesde.AddMinutes(dHoras * 60 + dMins),
                            //        horaDesde = TimeSpan.Parse(horaDesde),
                            //        horaHasta = TimeSpan.Parse(horaHasta),

                            //    };
                            //    db.TurnTurnoDetalle.Add(detalleTurno);
                            //    db.SaveChanges();
                            //    fechaHoraInicio = fechaHoraInicio.AddDays(1);
                            //}


                            //unTurno.codigoRetorno = GenerarHistorialTurno(turno, "ALT", idUsuario);
                            //unTurno.idTurno = tur.idTurno;


                            return unTurno;
                        }
                        else if (turno.tipoCRUD == "U")
                        {

                            var consultarUnTurno = buhodb.RecHorarioCtr.Find(turno.idTurnoCTR);

                            if (consultarUnTurno != null)
                            {
                                consultarUnTurno.fecha = consultarUnTurno.fecha;
                                consultarUnTurno.idEmpleado = turno.idPersona;
                                consultarUnTurno.idTurno = turno.idTipoTurno;
                                consultarUnTurno.mes = consultarUnTurno.mes;
                                consultarUnTurno.ano = consultarUnTurno.ano;

                                buhodb.SaveChanges();
                            }
                            return unTurno;
                        }
                    }
                }
                return unTurno;
            }
            catch (Exception e)
            {
                return unTurno;
            }
        }
        public int ValidarCantidadPersonasPorGuardia(TurnoVM turnoAlta)
        {
            //valida que no existan mas de 3 personas por turno
            List<TurnoVM> turno;
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var cuenta = 0;
                turno = (from t in db.TurnTurno
                         join dt in db.TurnTurnoDetalle on t.idTurno equals dt.idTurno
                         join g in db.TurnUsuarioGuardia on t.idPersona equals g.idUsuario
                         where t.activo == true && dt.fechaDesde >= turnoAlta.fechaDesde && dt.fechaHasta <= turnoAlta.fechaHasta
                         && turnoAlta.idTipoTurno == 1 && turnoAlta.idTipoTurno == t.idTipoTurno
                         select new TurnoVM
                         {
                             activo = t.activo,
                             codigoRetorno = 200,
                             fechaDesde = t.fechaHoraDesde,
                             fechaHasta = t.fechaHoraHasta,
                             idPersona = t.idPersona,
                             idTipoTurno = t.idTipoTurno,
                             idTurno = t.idTurno,

                         }).Where(x => x.idTurno > 0).Distinct().ToList();
                if (turno.Count >= 3)
                {
                    cuenta = 50;
                }

                return cuenta;
            }
        }
        public int ValidarTurnoPersona(TurnoVM turnoAlta)
        {
            //valida que la persona no repita el turno para esa fecha
            List<TurnoVM> turno;
            var soloFechaDesde = DateTime.Parse(String.Format("{0:dd/MM/yyyy}", turnoAlta.fechaDesde));
            var soloFechaHasta = DateTime.Parse(String.Format("{0:dd/MM/yyyy}", turnoAlta.fechaHasta));
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var cuenta = 0;
                turno = (from t in db.TurnTurno
                         join dt in db.TurnTurnoDetalle on t.idTurno equals dt.idTurno
                         where t.activo == true && (dt.fechaDesde >= soloFechaDesde && dt.fechaHasta <= soloFechaHasta) && turnoAlta.idPersona == t.idPersona
                         && turnoAlta.idTipoTurno == t.idTipoTurno
                         select new TurnoVM
                         {
                             activo = t.activo,
                             codigoRetorno = 200,
                             fechaDesde = t.fechaHoraDesde,
                             fechaHasta = t.fechaHoraHasta,
                             idPersona = t.idPersona,
                             idTipoTurno = t.idTipoTurno,
                             idTurno = t.idTurno,

                         }).Where(x => x.idTurno > 0).Distinct().ToList();
                if (turno.Count > 0)
                {
                    cuenta = 100;
                }

                return cuenta;
            }
        }
        public int ValidarCantidadDias(TurnoVM turnoAlta)
        {
            //valida que no se ha tomado mas de 4 francos en un determinado mes
            List<TurnoVM> turno;
            DateTime fechaDesde = (DateTime)turnoAlta.fechaDesde;
            var startDate = new DateTime(fechaDesde.Year, fechaDesde.Month, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var cuenta = 0;
                turno = (from t in db.TurnTurno
                         join dt in db.TurnTurnoDetalle on t.idTurno equals dt.idTurno
                         where t.activo == true && dt.fechaDesde >= fechaDesde && dt.fechaHasta <= fechaDesde && turnoAlta.idPersona == t.idPersona
                         && turnoAlta.idTipoTurno == 1 && turnoAlta.idTipoTurno == t.idTipoTurno
                         select new TurnoVM
                         {
                             activo = t.activo,
                             codigoRetorno = 200,
                             fechaDesde = t.fechaHoraDesde,
                             fechaHasta = t.fechaHoraHasta,
                             idPersona = t.idPersona,
                             idTipoTurno = t.idTipoTurno,
                             idTurno = t.idTurno,

                         }).ToList();
                if (turno.Count > 26)
                {
                    cuenta = 150;
                }

                return cuenta;
            }
        }
        public bool ObtenerTurnoEjecucion(int idUsuarioLogueado)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                using (BuhoGestionEntities buhodb = new BuhoGestionEntities())
                {

                    var abrioTurno = false;

                    var rolUsuario = db.GralUsuarioRol.Where(x => x.idUsuario == idUsuarioLogueado).FirstOrDefault();
                    var turnosAbiertos = db.TurnTurnoEjecucion.Where(x => x.fechaHoraApertura != null && x.fechaHoraCierre == null && x.activo == true).ToList();
                    if (rolUsuario != null)
                    {
                        var rol = db.GralRol.Where(x => x.idRol == rolUsuario.idRol).FirstOrDefault();
                        if (rol.esJefe == true) //Si es jefe de turno chequeo si tiene turno abierto en TurnTurnoEjecucion y tambien como acompañante
                        {
                            var turno1 = db.TurnTurnoEjecucion.Where(x => x.idJefeTurno == idUsuarioLogueado && x.fechaHoraApertura != null && x.fechaHoraCierre == null && x.activo == true).FirstOrDefault();
                            if (turno1 != null) //Reviso si el abrio turno
                            {
                                abrioTurno = true;
                            }
                            for (int i = 0; i < turnosAbiertos.Count; i++) //Reviso si esta como acompañante en algun turno abierto
                            {
                                var idTurnoEjecucion = turnosAbiertos[i].idTurnoEjecucion;
                                var turnoAcompañante = db.TurnTurnoAcompañante.Where(x => x.idTurnoEjecucion == idTurnoEjecucion && x.activo == true).ToList();
                                for (int j = 0; j < turnoAcompañante.Count; j++)
                                {
                                    if (turnoAcompañante[j].idAcompañante == idUsuarioLogueado)
                                    {
                                        abrioTurno = true;
                                    }
                                }
                            }
                        }
                        else //Si no es jefe solo tengo que buscar en las tablas de acompañantes
                        {
                            if (rol.esOperador == true)
                            {
                                for (int i = 0; i < turnosAbiertos.Count; i++) //Reviso si esta como acompañante en algun turno abierto
                                {
                                    var idEsteTurno = turnosAbiertos[i].idTurnoEjecucion;
                                    var turnoAcompañante = db.TurnTurnoAcompañante.Where(x => x.idTurnoEjecucion == idEsteTurno && x.activo == true).ToList(); //Lista de acompañantes
                                    for (int j = 0; j < turnoAcompañante.Count; j++)
                                    {
                                        if (turnoAcompañante[j].idAcompañante == idUsuarioLogueado)
                                        {
                                            abrioTurno = true;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                abrioTurno = false;
                            }

                        }

                        if (rol.idRol == 1 || rol.idRol == 2)
                        {
                            abrioTurno = true;
                        }
                    }
                    return abrioTurno;
                }
            }
        }
        public object AbrirTurno(int idUsuarioLogueado, int idTipoTurno, string idAcompañante, string estadoTiempo)
        {
            object response = new
            {
                codigoRespuesta = 400,
            };
            var listaIdAcompañantes = idAcompañante.Split(',');
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {

                    var fechaHoraActual = DateTime.Now;
                    List<TurnoEjecucionVM> listaTurnosEjecucion;

                    listaTurnosEjecucion = (from t in db.TurnTurnoEjecucion
                                            join tipo in db.TurnTipoTurno on t.idTipoTurno equals tipo.idTipoTurno
                                            join a in db.TurnTurnoAcompañante on t.idTurnoEjecucion equals a.idTurnoEjecucion
                                            where t.idJefeTurno == idUsuarioLogueado && t.fechaHoraApertura != null && t.fechaHoraCierre == null
                                            select new TurnoEjecucionVM
                                            {
                                                idTurnoEjecucion = t.idTurnoEjecucion,
                                                fechaHoraApertura = t.fechaHoraApertura,
                                                horaDesdeTipoTurno = tipo.horaDesde,
                                                horaHastaTipoTurno = tipo.horaHasta,
                                                idTipoTurno = t.idTipoTurno,
                                                nombreTipoTurno = tipo.nombreTipoTurno,
                                                activo = true,
                                            }).ToList();

                    if (listaTurnosEjecucion.Count > 0)
                    {
                        //for (int i = 0; i < listaTurnosEjecucion.Count; i++)
                        //{
                        //var fechaTurno = listaTurnosEjecucion[i].fechaHoraApertura.ToString().Split(' ')[0];
                        //var fechaTurno2 = fechaHoraActual.ToString().Split(' ')[0];
                        //if (fechaTurno == fechaTurno2 && listaTurnosEjecucion[i].idTipoTurno == idTipoTurno)
                        //{
                        response = new
                        {
                            codigoRespuesta = 300
                        };
                        return response;
                        //}

                    }

                    var JefeTurnoAbierto = db.TurnTurnoAcompañante.Where(x => x.idAcompañante == idUsuarioLogueado && x.activo == true).FirstOrDefault();
                    if (JefeTurnoAbierto != null)//Si el que esta logeado tiene un turno abierto como acompañante , no le deja abrir otro
                    {
                        if (JefeTurnoAbierto.activo == true)
                        {
                            response = new
                            {
                                codigoRespuesta = 300
                            };
                            return response;
                        }
                    }

                    foreach (var acompañante in listaIdAcompañantes)//Comprueba si los acompañantes tiene un turno abierto
                    {
                        if (acompañante != "")
                        {
                            var idCompa = Convert.ToInt32(acompañante);
                            var TurnoCompaAbierto = db.TurnTurnoAcompañante.Where(x => x.idAcompañante == idCompa && x.activo == true).FirstOrDefault();
                            if (TurnoCompaAbierto != null)
                            {
                                if (TurnoCompaAbierto.activo == true)
                                {
                                    response = new
                                    {
                                        codigoRespuesta = 303
                                    };
                                    return response;
                                }
                            }


                        }
                    }






                    TurnTurnoEjecucion turnoEjecucion = new TurnTurnoEjecucion
                    {
                        fechaHoraApertura = fechaHoraActual,
                        fechaHoraCierre = null,
                        idTipoTurno = idTipoTurno,
                        idJefeTurno = idUsuarioLogueado,
                        estadoTiempo = estadoTiempo,
                        activo = true,
                    };
                    db.TurnTurnoEjecucion.Add(turnoEjecucion);
                    db.SaveChanges();
                    var esteTurnoEjecucion = db.TurnTurnoEjecucion.OrderByDescending(x => x.idTurnoEjecucion).Where(z => z.idTipoTurno == idTipoTurno && z.estadoTiempo == estadoTiempo && z.idJefeTurno == idUsuarioLogueado).FirstOrDefault();
                    foreach (var acompañante in listaIdAcompañantes)
                    {
                        if (acompañante != "")
                        {
                            TurnTurnoAcompañante turnoAcompañante = new TurnTurnoAcompañante
                            {
                                idTurnoEjecucion = esteTurnoEjecucion.idTurnoEjecucion,
                                idAcompañante = Convert.ToInt32(acompañante),
                                activo = true,
                            };
                            db.TurnTurnoAcompañante.Add(turnoAcompañante);
                        }
                    }

                    db.SaveChanges();
                    response = new
                    {
                        codigoRespuesta = 200,
                    };

                    return response;


                }
            }
            catch (Exception e)
            {
                return response;
            }
        }
        public int CerrarTurno(int idUsuarioLogueado, int idTurnoEjecucion)
        {
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    var turnoEjecucion = db.TurnTurnoEjecucion.Where(x => x.idTurnoEjecucion == idTurnoEjecucion).FirstOrDefault();
                    var rolUsuario = db.GralUsuarioRol.Where(x => x.idUsuario == idUsuarioLogueado).FirstOrDefault();
                    var rol = db.GralRol.Where(x => x.idRol == rolUsuario.idRol).FirstOrDefault();
                    //if (rol.esOperador == true)
                    //{
                    //    var TurnoCompa = db.TurnTurnoAcompañante.Where(x => x.idTurnoEjecucion == idTurnoEjecucion && x.idAcompañante == idUsuarioLogueado).FirstOrDefault();
                    //    if (turnoEjecucion != null && TurnoCompa.activo == true)
                    //    {
                    //        TurnoCompa.activo = false;
                    //        db.SaveChanges();
                    //    }
                    //}
                    //else
                    //{
                    var fechaHoraActual = DateTime.Now;
                    if (turnoEjecucion != null && turnoEjecucion.activo == true) //Aca pongo en false el Turno de TurnTurno
                    {
                        turnoEjecucion.fechaHoraCierre = fechaHoraActual;
                        turnoEjecucion.activo = false;
                        db.SaveChanges();
                    }

                    var Acomp = db.TurnTurnoAcompañante.Where(x => idTurnoEjecucion == turnoEjecucion.idTurnoEjecucion).ToList();
                    foreach (var idC in Acomp) // Y aca pongo false los id de los acompañantes en TurnTurnoAcompañante
                    {
                        if (turnoEjecucion.idTurnoEjecucion == idC.idTurnoEjecucion)
                        {
                            idC.activo = false;
                            db.SaveChanges();
                        }

                    }

                    //}


                    return 200;
                }
            }
            catch (Exception e)
            {
                return 400;
            }

        }
        public TurnoVM VerificarTurno(int idUsuarioLogueado)
        {

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                TurnoVM turnovm = new TurnoVM();
                var fechaHoraActual = DateTime.Now;
                var turno = db.TurnTurno.Where(x => x.idPersona == idUsuarioLogueado && x.activo == true && x.fechaHoraDesde <= fechaHoraActual && x.fechaHoraHasta > fechaHoraActual && x.fechaHoraApertura == null && x.fechaHoraCierre == null).FirstOrDefault();
                if (turno != null)
                {
                    turno.fechaHoraApertura = fechaHoraActual;
                    db.SaveChanges();
                    turnovm = new TurnoVM
                    {
                        activo = turno.activo,
                        fechaHoraApertura = fechaHoraActual,
                        fechaHoraCierre = null,
                        fechaHasta = turno.fechaHoraHasta
                    };
                }
                return turnovm;
            }
        }
        public int EliminarTurno(TurnoVM turno)
        {
            using (BuhoGestionEntities db2 = new BuhoGestionEntities())
            {
                var turnoEliminar = db2.RecHorarioCtr.Find(turno.idTurno);
                if (turnoEliminar != null)
                {
                    turnoEliminar.activo = false;
                    db2.SaveChanges();
                    return 200;
                }
                return 400;
            }
        }
        public int GenerarHistorialTurno(TurnoVM turno, string codigoTipo, int? idUsuario)
        {
            try
            {
                using (IntranetCTREntities db = new IntranetCTREntities())
                {
                    var tipoHistorial = (from th in db.TurnTipoHistorialTurno where th.codigoHistorialTurno == codigoTipo select th).FirstOrDefault();
                    var ultHist = (from h in db.TurnHistorialTurno where h.idTurno == turno.idTurno && h.activo == true select h).FirstOrDefault();
                    TurnHistorialTurno historial = new TurnHistorialTurno
                    {
                        activo = true,
                        fechaHoraHistorial = DateTime.Now,
                        idTurno = turno.idTurno,
                        idUsuario = idUsuario,
                        idTipoHistorialTurno = tipoHistorial.idTipoHistorialTurno
                    };
                    db.TurnHistorialTurno.Add(historial);
                    db.SaveChanges();
                    ultHist.activo = false;
                    db.SaveChanges();

                    return 200;
                }
            }
            catch (Exception e)
            {
                return 400;

            }
        }

        public UsuarioGuardiaVM VerificarUsuarioGuardia(int idUsuarioLogueado)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                UsuarioGuardiaVM guardia = new UsuarioGuardiaVM();
                var g = (from c in db.TurnUsuarioGuardia
                         join d in db.TurnGuardia on c.idGuardia equals d.idGuardia
                         where c.idUsuario == idUsuarioLogueado && c.activo == true
                         select new UsuarioGuardiaVM
                         {
                             activo = c.activo,
                             idUsuario = c.idUsuario,
                             idGuardia = c.idGuardia,
                             idMantenimiento = d.idMantenimiento,
                             idUsuarioGuardia = c.idUsuarioGuardia

                         }).FirstOrDefault();

                if (g != null)
                {
                    g.codigo = 200;
                    guardia = g;
                }
                else
                {
                    guardia.codigo = 400;
                }

                return guardia;
            }
        }


        public object ObtenerListaTurno(bool activo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                List<TurnoVM> Turno;
                Turno = (from t in db.TurnTurno
                         where t.activo == activo
                         select new TurnoVM
                         {
                             activo = t.activo,
                             idTipoTurno = t.idTipoTurno,
                             idPersona = t.idPersona,
                             fechaDesde = t.fechaHoraDesde,
                             fechaHasta = t.fechaHoraHasta,

                         }).ToList();


                object json = new { data = Turno };
                return json;
            }

        }

        public object ObtenerTipoTurnoCTR(bool activo)
        {

            using (BuhoGestionEntities db2 = new BuhoGestionEntities())
            {
                List<HorarioCtrVM> horarioCtrVMs = new List<HorarioCtrVM>();
                horarioCtrVMs = (from h in db2.RecTurnoCtr
                                 select new HorarioCtrVM
                                 {
                                     idTurnoCtr = h.idTurnoCtr,
                                     horaEntrada = h.horaEntrada,
                                     horaSalida = h.horaSalida,
                                     nombreTurno = h.nombreTurno,


                                 }).ToList();
                //object json = new { data = horarioCtrVMs };
                return horarioCtrVMs;
            }
        }

        public TurnoVM ObtenerTurnoAbiertoParaGrilla()
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                TurnoVM Turno = new TurnoVM();
                Turno = (from t in db.TurnTurnoEjecucion
                         join a in db.TurnTurnoAcompañante on t.idTurnoEjecucion equals a.idTurnoEjecucion
                         where t.activo == true
                         select new TurnoVM
                         {
                             activo = t.activo,
                             idTipoTurno = t.idTipoTurno,
                             idPersona = t.idJefeTurno,
                             idAcompañantee = a.idAcompañante,

                         }).FirstOrDefault();
                ServicioUsuario servUsuario = new ServicioUsuario();
                if (Turno != null)
                {
                    var buscarJefe = servUsuario.ObtenerDatosUsuarioPorId(Turno.idPersona);
                    if (buscarJefe != null)
                    {
                        Turno.nombreJefeT = buscarJefe.nombreUsuario;
                    }
                    var buscarAcompañante = servUsuario.ObtenerDatosUsuarioPorId(Turno.idAcompañantee);
                    if (buscarAcompañante != null)
                    {
                        Turno.nombreOperadorT = buscarAcompañante.nombreUsuario;
                    }
                }

                return Turno;
            }
        }
        public void procesarTurnos(string ruta, string nombre, string fechaDesde, string fechaHasta)
        {
            using (BuhoGestionEntities buhodb = new BuhoGestionEntities())
            {
                DesarrolloCTRUtilidades.ExcelUtilityProcess excelToDb = new DesarrolloCTRUtilidades.ExcelUtilityProcess();
                var excelPlanilla = excelToDb.Excel_To_DataTable(ruta + nombre, 0);
                
                if (excelPlanilla.Rows.Count > 0)
                {
                    DataRow[] rows = excelPlanilla.Rows.Cast<DataRow>().ToArray();
                    for (int i = 0; i < rows.Count(); i++)
                    {
                        var fd = Convert.ToDateTime(fechaDesde);
                        var fh = Convert.ToDateTime(fechaHasta);
                        var cadaFila = rows[i];
                        var apellido = cadaFila.ItemArray[0].ToString();
                        var idUsuario = buhodb.GralUsuario.Where(x => x.esCtr == true && x.Nombre.Contains(apellido)).FirstOrDefault().idUsuario;
                        for (int j = 1; j< cadaFila.ItemArray.Count(); j++)
                        {
                            TurnoVM nuevo = new TurnoVM
                            {
                                tipoCRUD = "C",
                                fechaDesde = fd,
                                fechaHasta = fd,
                                idPersona = idUsuario,
                            };
                            var tipoTurno = cadaFila[j].ToString();
                            if(tipoTurno == "D" || tipoTurno == "X")
                            {
                                nuevo.idTipoTurno = 5;
                            }else if(tipoTurno == "F")
                            {
                                nuevo.idTipoTurno = 7;
                            }
                            else
                            {
                                nuevo.idTipoTurno = Convert.ToInt32(tipoTurno);
                            }
                            CRUD(nuevo, 0);
                            fd = fd.AddDays(1);
                            fh = fh.AddDays(1);
                        }
                        
                        
                    }

                }
            }
           
        }
    }
}
