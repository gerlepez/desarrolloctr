﻿using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace DesarrolloCTR.Controllers.Api
{
    public class ReclamoApiController : ApiController
    {
        [System.Web.Http.Route("Api/Reclamo/ObtenerTipoReclamos")]
        [System.Web.Http.ActionName("ObtenerTipoReclamos")]
        [System.Web.Http.HttpGet]
        public object ObtenerTipoReclamos(bool activo)
        {
            ServicioReclamo servicioReclamo = new ServicioReclamo();
            object lista = servicioReclamo.ObtenerTipoReclamos(activo);
            return lista;
        }

        [System.Web.Http.Route("Api/Reclamo/TablaReclamo")]
        [System.Web.Http.ActionName("TablaReclamo")]
        [System.Web.Http.HttpPost]
        public object TablaReclamo(FormDataCollection form, bool? activo)
        {
            var draw = form.Get("draw");
            var start = form.Get("start");
            var length = form.Get("length");
            var sortColumn = (form.Get("columns[" + form.Get("order[0][column]").FirstOrDefault() + "][data]").ToString()).ToString();
            var sortColumnDir = form.Get("order[0][dir]").ToString();
            var searchValue = form.Get("search[value]").ToString();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            int cantidadRegistrosPagina = Int32.Parse(length);

            ServicioReclamo servicioReclamo = new ServicioReclamo();
            object lista = servicioReclamo.TablaReclamo(activo, draw, recordsTotal, skip, pageSize);
            return lista;
        }

        [System.Web.Http.Route("Api/Reclamo/ConsultarUnReclamo")]
        [System.Web.Http.ActionName("ConsultarUnReclamo")]
        [System.Web.Http.HttpGet]
        public ReclamoVM ConsultarUnReclamo(int idReclamo)
        {
            ServicioReclamo servicioReclamo = new ServicioReclamo();
            ReclamoVM reclamo = servicioReclamo.ConsultarUnReclamo(idReclamo);
            return reclamo;
        }

        [System.Web.Http.Route("Api/Reclamo/EliminarReclamo")]
        [System.Web.Http.ActionName("EliminarReclamo")]
        [System.Web.Http.HttpPost]
        public int EliminarReclamo(ReclamoVM reclamo)
        {
            ServicioReclamo servicioReclamo = new ServicioReclamo();
            int codigo = servicioReclamo.EliminarReclamo(reclamo);
            return codigo;
        }

        [System.Web.Http.Route("Api/Reclamo/RecuperarReclamo")]
        [System.Web.Http.ActionName("RecuperarReclamo")]
        [System.Web.Http.HttpPost]
        public int RecuperarReclamo(ReclamoVM reclamo)
        {
            ServicioReclamo servicioReclamo = new ServicioReclamo();
            int codigo = servicioReclamo.RecuperarReclamo(reclamo);
            return codigo;

        }

        [System.Web.Http.Route("Api/Reclamo/AltaReclamo")]
        [System.Web.Http.ActionName("AltaReclamo")]
        [System.Web.Http.HttpPost]
        public int AltaReclamo(ReclamoVM reclamo)
        {

            ServicioReclamo servicioReclamo = new ServicioReclamo();
            int codigo;
            if (servicioReclamo.ValidarReclamo(reclamo))
            {
                codigo = servicioReclamo.AltaReclamo(reclamo);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }


        [System.Web.Http.Route("Api/Reclamo/ModificarReclamo")]
        [System.Web.Http.ActionName("ModificarReclamo")]
        [System.Web.Http.HttpPost]
        public int ModificarReclamo(ReclamoVM reclamo)
        {

            ServicioReclamo servicioReclamo = new ServicioReclamo();
            int codigo;
            if (servicioReclamo.ValidarReclamo(reclamo))
            {
                codigo = servicioReclamo.ModificarReclamo(reclamo);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }
    }


}