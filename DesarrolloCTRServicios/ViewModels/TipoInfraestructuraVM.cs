﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class TipoInfraestructuraVM
    {
        public int? idTipoInfra { get; set; }
        public int? idUsuarioAlta { get; set; }

        public bool? activo { get; set; }

        public string codigoSap { get; set; }
        public string nombreInfra { get; set; }
    }
}
