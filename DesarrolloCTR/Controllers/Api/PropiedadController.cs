﻿using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers.Api
{
    public class PropiedadController : ApiController
    {
        [System.Web.Http.Route("Api/Propiedad/ObtenerPropiedad")]
        [System.Web.Http.ActionName("ObtenerPropiedad")]
        [System.Web.Http.HttpGet]
        public object ObtenerPropiedad(bool? activo)
        {
            ServicioPropiedad servicioPropiedad = new ServicioPropiedad();
            object lista = servicioPropiedad.ObtenerPropiedad(activo);
            return lista;
        }

        [System.Web.Http.Route("Api/Propiedad/AltaPropiedad")]
        [System.Web.Http.ActionName("AltaPropiedad")]
        [System.Web.Http.HttpPost]
        public int AltaPropiedad(PropiedadVM propiedad)
        {
            ServicioPropiedad servicioPropiedad = new ServicioPropiedad();
            int codigo;
            if (servicioPropiedad.ValidarPropiedad(propiedad))
            {
                codigo = servicioPropiedad.AltaPropiedad(propiedad);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }

        [System.Web.Http.Route("Api/Propiedad/ConsultarUnaPropiedad")]
        [System.Web.Http.ActionName("ConsultarUnaPropiedad")]
        [System.Web.Http.HttpGet]
        public PropiedadVM ConsultarUnaPropiedad(int idPropiedad)
        {
            ServicioPropiedad servicioPropiedad = new ServicioPropiedad();
            PropiedadVM propiedad = servicioPropiedad.ConsultarUnaPropiedad(idPropiedad);
            return propiedad;
        }

        [System.Web.Http.Route("Api/Propiedad/EliminarPropiedad")]
        [System.Web.Http.ActionName("EliminarPropiedad")]
        [System.Web.Http.HttpPost]
        public int EliminarPropiedad(PropiedadVM propiedad)
        {
            ServicioPropiedad servicioPropiedad = new ServicioPropiedad();
            int codigo = servicioPropiedad.EliminarPropiedad(propiedad);
            return codigo;
        }

        [System.Web.Http.Route("Api/Propiedad/RecuperarPropiedad")]
        [System.Web.Http.ActionName("RecuperarPropiedad")]
        [System.Web.Http.HttpPost]
        public int RecuperarPropiedad(PropiedadVM propiedad)
        {
            ServicioPropiedad servicioPropiedad = new ServicioPropiedad();
            int codigo = servicioPropiedad.RecuperarPropiedad(propiedad);
            return codigo;
        }

        [System.Web.Http.Route("Api/Propiedad/ModificarPropiedad")]
        [System.Web.Http.ActionName("ModificarPropiedad")]
        [System.Web.Http.HttpPost]
        public int ModificarPropiedad(PropiedadVM propiedad)
        {

            ServicioPropiedad servicioPropiedad = new ServicioPropiedad();
            int codigo;
            if (servicioPropiedad.ValidarPropiedad(propiedad))
            {
                codigo = servicioPropiedad.ModificarPropiedad(propiedad);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }
    }
}