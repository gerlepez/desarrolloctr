﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;

namespace DesarrolloCTR.Controllers.Api
{
    public class EstadoTiempoApiController : ApiController
    {
        
        [System.Web.Http.Route("Api/Turno/ObtenerListaEstadoTiempo")]
        [System.Web.Http.ActionName("ObtenerListaEstadoTiempo")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaEstadoTiempo()
        {
            ServicioEstadoTiempo estado = new ServicioEstadoTiempo();
            object lista;
            estado.ObtenerListaEstadoTiempo();
            lista = estado.ObtenerListaEstadoTiempo();
            return lista;
        }
        
    }
}
