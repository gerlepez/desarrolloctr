﻿using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DesarrolloCTR.Controllers.Api
{
    public class TransformadorApiController : ApiController
    {
        [System.Web.Http.Route("Api/Transformador/ObtenerListaTransformador")]
        [System.Web.Http.ActionName("ObtenerListaTransformador")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaTransformador(bool activo)
        {
            ServicioTransformador servTransformador = new ServicioTransformador();

            object listaTransformador = servTransformador.ObtenerListaTransformador(activo);

            //((IDisposable)servTransformador).Dispose();

            return listaTransformador;
        }


        [System.Web.Http.Route("Api/Transformador/AltaTransformador")]
        [System.Web.Http.ActionName("AltaTransformador")]
        [System.Web.Http.HttpPost]
        public int AltaTransformador(TransformadorVM transformador)
        {
            var idUsuarioLogueado = 0;
            if (transformador != null)
            {
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(transformador.nombresuario);
            }
            ServicioTransformador servTransformador = new ServicioTransformador();

            int codigo;
            if (servTransformador.ValidarTransformador(transformador))
            {
                codigo = servTransformador.AltaTransformador(transformador, idUsuarioLogueado);
            }
            else
            {
                codigo = 400;
            }

            //((IDisposable)servTransformador).Dispose();

            return codigo;
        }

        [System.Web.Http.Route("Api/Transformador/ModificarTransformador")]
        [System.Web.Http.ActionName("ModificarTransformador")]
        [System.Web.Http.HttpPost]
        public int ModificarTransformador(TransformadorVM transformador)
        {
            var idUsuarioLogueado = 0;
            if (transformador != null)
            {
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(transformador.nombresuario);
            }

            ServicioTransformador servTransformador = new ServicioTransformador();

            int codigo;
            if (servTransformador.ValidarTransformador(transformador))
            {
                codigo = servTransformador.ModificarTransformador(transformador, idUsuarioLogueado);
            }
            else
            {
                codigo = 400;
            }

            //((IDisposable)servTransformador).Dispose();

            return codigo;
        }

        [System.Web.Http.Route("Api/Transformador/EliminarTransformador")]
        [System.Web.Http.ActionName("EliminarTransformador")]
        [System.Web.Http.HttpPost]
        public int EliminarTransformador(TransformadorVM transformador)
        {
            ServicioTransformador servTransformador = new ServicioTransformador();
            int codigo = servTransformador.EliminarTransformador(transformador);

            //((IDisposable)servTransformador).Dispose();

            return codigo;
        }

        [System.Web.Http.Route("Api/Transformador/ConsultarUnTransformador")]
        [System.Web.Http.ActionName("ConsultarUnTransformador")]
        [System.Web.Http.HttpGet]
        public TransformadorVM ConsultarUnTransformador(int idTransformador)
        {
            ServicioTransformador servTransformador = new ServicioTransformador();
            TransformadorVM transformador = servTransformador.ConsultarUnTransformador(idTransformador);

            //((IDisposable)servTransformador).Dispose();

            return transformador;
        }

        [System.Web.Http.Route("Api/Transformador/RecuperarTransformador")]
        [System.Web.Http.ActionName("RecuperarTransformador")]
        [System.Web.Http.HttpPost]
        public int RecuperarTransformador(TransformadorVM transformador)
        {
            ServicioTransformador servTransformador = new ServicioTransformador();
            int codigo = servTransformador.RecuperarTransformador(transformador);

            //((IDisposable)servTransformador).Dispose();

            return codigo;
        }
        [System.Web.Http.Route("Api/Transformador/ConsultarIndisponibilidadTransformador")]
        [System.Web.Http.ActionName("ConsultarIndisponibilidadTransformador")]
        [System.Web.Http.HttpGet]
        public object ConsultarIndisponibilidadTransformador(int idTransformador, int idNovedad)
        {
            ServicioTransformador servTransformador = new ServicioTransformador();
            var indisponibilidad = servTransformador.ConsultarIndisponibilidadTransformador(idTransformador, idNovedad);

            return indisponibilidad;
        }
    }
}
