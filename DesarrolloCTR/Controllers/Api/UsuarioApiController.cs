﻿using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Configuration;
using System.Web.Http;

namespace DesarrolloCTR.Controllers.Api
{
    public class UsuarioApiController : ApiController
    {
        [System.Web.Http.Route("Api/Usuario/ObtenerUsuarioLogueado")]
        [System.Web.Http.ActionName("ObtenerUsuarioLogueado")]
        [System.Web.Http.HttpGet]
        public int ObtenerUsuarioLogueado(String nombreUsuario)
        {
            ServicioUsuario servUsuario = new ServicioUsuario();

            int idUsuario = servUsuario.ObtenerUsuarioLogueado(nombreUsuario);

            return idUsuario;
        }

        [System.Web.Http.Route("Api/Usuario/ObtenerListaUsuarioCTR")]
        [System.Web.Http.ActionName("ObtenerListaUsuarioCTR")]
        [System.Web.Http.HttpGet]
        public List<UsuarioVM> ObtenerListaUsuarioCTR()
        {
            ServicioUsuario servUsuario = new ServicioUsuario();

            List<UsuarioVM> listaUsuario = servUsuario.ObtenerListaUsuarioCTR();

            return listaUsuario;
        }

        [System.Web.Http.Route("Api/Usuario/ObtenerListaOperadorCTR")]
        [System.Web.Http.ActionName("ObtenerListaOperadorCTR")]
        [System.Web.Http.HttpGet]
        public List<UsuarioVM> ObtenerListaOperadorCTR(string nombreJefe = "")
        {
            ServicioUsuario servUsuario = new ServicioUsuario();
            var idUsuarioLogueado = 0;
            idUsuarioLogueado = servUsuario.ObtenerUsuarioLogueado(nombreJefe);
            List<UsuarioVM> listaUsuario = servUsuario.ObtenerListaOperadorCTR(idUsuarioLogueado);

            return listaUsuario;
        }


        [System.Web.Http.Route("Api/Usuario/ObtenerListaEmpleadosGuardia")]
        [System.Web.Http.ActionName("ObtenerListaEmpleadosGuardia")]
        [System.Web.Http.HttpGet]
        public List<UsuarioVM> ObtenerListaEmpleadosGuardia(int idGuardia)
        {
            ServicioUsuario servUsuario = new ServicioUsuario();

            List<UsuarioVM> listaUsuario = servUsuario.ObtenerListaEmpleadosGuardia(idGuardia);

            return listaUsuario;
        }

        [System.Web.Http.Route("Api/Usuario/ObtenerListaResponsableConsignacion")]
        [System.Web.Http.ActionName("ObtenerListaResponsableConsignacion")]
        [System.Web.Http.HttpGet]
        public List<UsuarioVM> ObtenerListaResponsableConsignacion()
        {
            ServicioUsuario servUsuario = new ServicioUsuario();

            List<UsuarioVM> listaUsuario = servUsuario.ObtenerListaResponsableConsignacion();

            return listaUsuario;
        }

        [System.Web.Http.Route("Api/Usuario/ObtenerDatosUsuarioLogueado")]
        [System.Web.Http.ActionName("ObtenerDatosUsuarioLogueado")]
        [System.Web.Http.HttpGet]
        public UsuarioVM ObtenerDatosUsuarioLogueado(string nombreUsuario)
        {
            ServicioUsuario servUsuario = new ServicioUsuario();
            //TODO encontrar la manera saber si inicio sesion por AD o pag login
            if (nombreUsuario == null)
            {
                LoginController instanciaControl = new LoginController();
                instanciaControl.SetSessionAD();
            }
            UsuarioVM usuario = servUsuario.ObtenerDatosUsuarioLogueado(nombreUsuario);
            if (usuario != null)
            {
                usuario.filePath = WebConfigurationManager.AppSettings["rutaFotos"].ToString() + usuario.foto;
            }
            return usuario;
        }


    }
}
