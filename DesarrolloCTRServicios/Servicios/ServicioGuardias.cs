﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioGuardias
    {
        public object ObtenerListaGuardia(int idMantenimiento)
        {
            using (BuhoGestionEntities db = new BuhoGestionEntities())
            {

                List<GuardiaVM> guardia;
                guardia = (from c in db.GuaTurnGuardia
                           join m in db.GuaMantenimiento on c.idMantenimiento equals m.idMantenimiento
                           where c.activo == true && c.idMantenimiento == idMantenimiento
                           select new GuardiaVM
                           {
                               activo = c.activo,
                               idGuardia = c.idGuardia,
                               nombreGuardia = c.nombreGuardia,
                               nombreMantenimiento = m.nombreMantenimiento,
                               idMantenimiento = c.idMantenimiento,
                           }).ToList();

                object json = new { data = guardia };
                return json;
            }
        }


        public object ObtenerGuardias(string fechaHoraDesde, string fechaHoraHasta, int idUsuarioLogueado, int idMantenimiento, int idGuardia)
        {
            using (BuhoGestionEntities db = new BuhoGestionEntities())
            {
                var fechaDateDesde = DateTime.ParseExact(fechaHoraDesde, "dd/MM/yyyy", null);
                var fechaDateHasta = DateTime.ParseExact(fechaHoraHasta, "dd/MM/yyyy", null);

                IQueryable<GuardiaVM> guardia = (from g in db.GuaGuardia
                                                 join u in db.GralUsuario on g.idPersona equals u.idUsuario
                                                 //join u2 in db.GralUsuario on g.idPersona2 equals u2.idUsuario
                                                 join tg in db.GuaTurnGuardia on g.idTipoGuardia equals tg.idGuardia
                                                 join m in db.GuaMantenimiento on g.idMantenimiento equals m.idMantenimiento
                                                 join t in db.GuaTelefonos on u.idUsuario equals t.idUsuario into te
                                                 from tel in te.DefaultIfEmpty()
                                                 where g.activo == true && fechaDateDesde <= g.FechaHoraDesde && fechaDateHasta >= g.FechaHoraHasta
                                                && g.idMantenimiento == idMantenimiento
                                                 select new GuardiaVM
                                                 {
                                                     idGuardia = g.idGuardia,
                                                     idPersona = g.idPersona,
                                                     usuario = u.Nombre,
                                                     telefono = tel.telefono,

                                                     //idPersona2 = g.idPersona2,
                                                     //usuario2 = u2.Nombre,
                                                     //telefono2 = tel.telefono,

                                                     fechaDesde = g.FechaHoraDesde,
                                                     fechaHasta = g.FechaHoraHasta,
                                                     idMantenimiento = g.idMantenimiento,
                                                     nombreMantenimiento = m.nombreMantenimiento,
                                                     idTipoGuardia = g.idTipoGuardia,
                                                     nombreGuardia = tg.nombreGuardia,
                                                     activo = g.activo,
                                                     codigoRetorno = 200,

                                                     idTipoTurno = 4,

                                                 }).OrderByDescending(x => x.idGuardia).AsQueryable();

                //Filtrado
                if (idGuardia != -1)
                {
                    guardia = guardia.Where(x => x.idTipoGuardia == idGuardia);
                }

                var guardias = guardia.ToList();
                foreach (var item in guardias)
                {
                    var persona2 = db.GuaGuardia.Where(x => x.idGuardia == item.idGuardia).FirstOrDefault();
                    ServicioUsuario servicioUsuario = new ServicioUsuario();
                    if (persona2.idPersona2 != -1)
                    {
                        var usuario = servicioUsuario.ObtenerDatosUsuarioPorId(persona2.idPersona2);
                        var telefono2 = db.GuaTelefonos.Where(x => x.idUsuario == persona2.idPersona2).FirstOrDefault();
                        item.idPersona2 = persona2.idPersona2;
                        item.usuario2 = usuario.nombreUsuario;
                        if(item.telefono2 != null)
                        {
                            item.telefono2 = telefono2.telefono;
                        }
                        
                    }
                }

                return guardias;
            }

        }




        public UsuarioGuardiaVM VerificarUsuarioGuardia(int idUsuarioLogueado)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                UsuarioGuardiaVM guardia = new UsuarioGuardiaVM();
                var g = (from c in db.TurnUsuarioGuardia
                         join d in db.TurnGuardia on c.idGuardia equals d.idGuardia
                         where c.idUsuario == idUsuarioLogueado && c.activo == true
                         select new UsuarioGuardiaVM
                         {
                             activo = c.activo,
                             idUsuario = c.idUsuario,
                             idGuardia = c.idGuardia,
                             idMantenimiento = d.idMantenimiento,
                             idUsuarioGuardia = c.idUsuarioGuardia

                         }).FirstOrDefault();

                if (g != null)
                {
                    g.codigo = 200;
                    guardia = g;
                }
                else
                {
                    guardia.codigo = 400;
                }

                return guardia;
            }
        }

        public object ObtenerListaMantenimiento(bool activo)
        {
            using (BuhoGestionEntities db = new BuhoGestionEntities())
            {
                List<MantenimientoVM> mantenimiento;
                mantenimiento = (from c in db.GuaMantenimiento
                                 where c.activo == activo
                                 select new MantenimientoVM
                                 {
                                     activo = c.activo,
                                     idMantenimiento = c.idMantenimiento,
                                     nombreMantenimiento = c.nombreMantenimiento

                                 }).ToList();

                object json = new { data = mantenimiento };
                return json;
            }

        }

        public List<GuardiaVM> ObtenerListaUsuariosGuardias()
        {
            using (BuhoGestionEntities db = new BuhoGestionEntities())
            {
                List<GuardiaVM> listaUsuarios = new List<GuardiaVM>();
                listaUsuarios = (from u in db.GralUsuario
                                 join t in db.GuaTelefonos on u.idUsuario equals t.idUsuario into te
                                 from tel in te.DefaultIfEmpty()
                                 where u.esGuardia == true
                                 select new GuardiaVM
                                 {
                                     idPersona = u.idUsuario,
                                     nombrePersona = u.Nombre,
                                     telefono = tel.telefono,

                                 }).ToList();

                var listaOrdenada = listaUsuarios.OrderBy(x => x.nombrePersona).ToList();
                return listaOrdenada;
            }
        }


        public GuardiaVM CRUD(GuardiaVM guardia, int? idUsuario)
        {
            GuardiaVM unaGuardia = new GuardiaVM()
            {
                idGuardia = 0,
                codigoRetorno = 400
            };
            if (guardia.tipoCRUD != "D")
            {
                unaGuardia.validacionCantPersonas = ValidarCantidadPersonasPorGuardia(guardia);
                unaGuardia.validacionGuardiaPorPersona = ValidarGuardiaPersona(guardia);
                unaGuardia.validacionCantDias = ValidarCantidadDias(guardia);
            }
            try
            {
                using (BuhoGestionEntities buhodb = new BuhoGestionEntities())
                {
                    if (guardia.tipoCRUD == "D")
                    {
                        unaGuardia.codigoRetorno = EliminarGuardia(guardia);

                        unaGuardia.idGuardia = guardia.idGuardia;
                        return unaGuardia;
                    }

                    if (guardia.tipoCRUD == "C")
                    {
                        List<DateTime> listaDias = new List<DateTime>();
                        foreach (DateTime day in EachCalendarDay(Convert.ToDateTime(guardia.fechaDesde), Convert.ToDateTime(guardia.fechaHasta)))
                        {
                            listaDias.Add(day);
                        }
                        var horaDesdee = Convert.ToDateTime(guardia.fechaDesde).ToString("HH:mm");
                        var horaHastaa = Convert.ToDateTime(guardia.fechaHasta).ToString("HH:mm");
                        if (guardia.fechaDesde == guardia.fechaHasta)
                        {
                            foreach (DateTime dia in listaDias)
                            {
                                {
                                    var esteDia = dia.ToString("dd/MM/yyyy");
                                    //var proximoDia = dia.AddDays(1).ToString("dd/MM/yyyy");
                                    var fechaDesde = esteDia + " " + horaDesdee;
                                    var fechaHasta = esteDia + " " + horaHastaa;
                                    var fechaConvertidaDesde = DateTime.Parse(fechaDesde);
                                    var fechaConvertidaHasta = DateTime.Parse(fechaHasta);
                                    GuaGuardia guardiaa = new GuaGuardia
                                    {
                                        idPersona = guardia.idPersona,
                                        FechaHoraDesde = fechaConvertidaDesde,
                                        FechaHoraHasta = fechaConvertidaHasta,
                                        idMantenimiento = guardia.idMantenimiento,
                                        idTipoGuardia = guardia.idTipoGuardia,
                                        Telefono = guardia.telefono,
                                        idPersona2 = guardia.idPersona2,
                                        Telefono2 = guardia.telefono2,
                                        activo = true,

                                    };
                                    buhodb.GuaGuardia.Add(guardiaa);
                                }
                            }
                        }
                        else
                        {
                            foreach (DateTime dia in listaDias)
                            {
                                {
                                    var esteDia = dia.ToString("dd/MM/yyyy");
                                    var proximoDia = dia.AddDays(1).ToString("dd/MM/yyyy");
                                    var fechaDesde = esteDia + " " + horaDesdee;
                                    var fechaHasta = proximoDia + " " + horaHastaa;
                                    var fechaConvertidaDesde = DateTime.Parse(fechaDesde);
                                    var fechaConvertidaHasta = DateTime.Parse(fechaHasta);
                                    GuaGuardia guardiaa = new GuaGuardia
                                    {
                                        idPersona = guardia.idPersona,
                                        FechaHoraDesde = fechaConvertidaDesde,
                                        FechaHoraHasta = fechaConvertidaHasta,
                                        idMantenimiento = guardia.idMantenimiento,
                                        idTipoGuardia = guardia.idTipoGuardia,
                                        Telefono = guardia.telefono,
                                        idPersona2 = guardia.idPersona2,
                                        Telefono2 = guardia.telefono2,
                                        activo = true,

                                    };
                                    buhodb.GuaGuardia.Add(guardiaa);
                                }
                            }
                        }

                        buhodb.SaveChanges();
                        return unaGuardia;
                    }
                    else if (guardia.tipoCRUD == "U")
                    {
                        var consultarUnaGuardia = buhodb.GuaGuardia.Find(guardia.idGuardia);
                        if (consultarUnaGuardia != null)
                        {
                            //consultarUnaGuardia.idTipoGuardia = guardia.idTipoGuardia;
                            //consultarUnaGuardia.idMantenimiento = guardia.idMantenimiento;
                            consultarUnaGuardia.Telefono = guardia.telefono;
                            consultarUnaGuardia.FechaHoraDesde = guardia.fechaDesde;
                            consultarUnaGuardia.FechaHoraHasta = guardia.fechaHasta;
                            consultarUnaGuardia.idPersona = guardia.idPersona;
                            consultarUnaGuardia.idPersona2 = guardia.idPersona2;
                            consultarUnaGuardia.Telefono2 = guardia.telefono2;
                            buhodb.SaveChanges();



                            DateTime fechaHoraInicio = (DateTime)consultarUnaGuardia.FechaHoraDesde;
                            DateTime fechaHoraFin = (DateTime)consultarUnaGuardia.FechaHoraHasta;


                            unaGuardia.idGuardia = consultarUnaGuardia.idGuardia;
                        }
                        return unaGuardia;
                    }

                }
                return unaGuardia;
            }
            catch (Exception e)
            {
                return unaGuardia;
            }
        }

        private IEnumerable<DateTime> EachCalendarDay(DateTime startDate, DateTime endDate)
        {
            for (var date = startDate.Date; date.Date <= endDate.Date; date = date.AddDays(1)) yield
            return date;
        }

        public int ValidarCantidadPersonasPorGuardia(GuardiaVM guardiaAlta)
        {
            //valida que no existan mas de 3 personas por turno
            List<GuardiaVM> guardia;
            using (BuhoGestionEntities db = new BuhoGestionEntities())
            {

                var idTipoGuardiaAlta = guardiaAlta.idTipoGuardia;
                var fechaDesdeGuardiaAlta = guardiaAlta.fechaDesde;
                var fechaHastaGuardiaAlta = guardiaAlta.fechaHasta;
                var cuenta = 0;
                guardia = (from g in db.GuaGuardia
                           where g.activo == true && g.FechaHoraDesde >= fechaDesdeGuardiaAlta && g.FechaHoraHasta <= fechaHastaGuardiaAlta
                           && idTipoGuardiaAlta == g.idTipoGuardia
                           select new GuardiaVM
                           {
                               idGuardia = g.idGuardia,
                               idPersona = g.idPersona,
                               fechaDesde = g.FechaHoraDesde,
                               fechaHasta = g.FechaHoraHasta,
                               idMantenimiento = g.idMantenimiento,
                               idTipoGuardia = g.idTipoGuardia,
                               telefono = g.Telefono,
                               activo = g.activo,
                               codigoRetorno = 200,

                           }).Where(x => x.idGuardia > 0).Distinct().ToList();
                if (guardia.Count >= 3)
                {
                    cuenta = 50;
                }

                return cuenta;
            }
        }

        public int ValidarGuardiaPersona(GuardiaVM guardiaAlta)
        {
            //valida que la persona no repita el turno para esa fecha
            List<GuardiaVM> guardia;
            var soloFechaDesde = DateTime.Parse(String.Format("{0:dd/MM/yyyy}", guardiaAlta.fechaDesde));
            var soloFechaHasta = DateTime.Parse(String.Format("{0:dd/MM/yyyy}", guardiaAlta.fechaHasta));
            using (BuhoGestionEntities db = new BuhoGestionEntities())
            {
                var cuenta = 0;
                guardia = (from g in db.GuaGuardia
                           where g.activo == true && (g.FechaHoraDesde == soloFechaDesde && g.FechaHoraHasta <= soloFechaHasta) && guardiaAlta.idPersona == g.idPersona
                           && guardiaAlta.idTipoGuardia == g.idTipoGuardia
                           select new GuardiaVM
                           {
                               idGuardia = g.idGuardia,
                               idPersona = g.idPersona,
                               fechaDesde = g.FechaHoraDesde,
                               fechaHasta = g.FechaHoraHasta,
                               idMantenimiento = g.idMantenimiento,
                               idTipoGuardia = g.idTipoGuardia,
                               telefono = g.Telefono,
                               activo = g.activo,
                               codigoRetorno = 200,
                               idPersona2 = g.idPersona2,
                               telefono2 = g.Telefono2,
                           }).Where(x => x.idGuardia > 0).Distinct().ToList();
                if (guardia.Count > 0)
                {
                    cuenta = 100;
                }

                return cuenta;
            }
        }
        public int ValidarCantidadDias(GuardiaVM guardiaAlta)
        {
            //valida que no se ha tomado mas de 4 francos en un determinado mes
            List<GuardiaVM> guardia;
            DateTime fechaDesde = (DateTime)guardiaAlta.fechaDesde;
            var startDate = new DateTime(fechaDesde.Year, fechaDesde.Month, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);
            using (BuhoGestionEntities db = new BuhoGestionEntities())
            {
                var cuenta = 0;
                guardia = (from g in db.GuaGuardia
                           where g.activo == true && g.FechaHoraDesde >= fechaDesde && g.FechaHoraHasta <= fechaDesde && guardiaAlta.idPersona == g.idPersona
                           && guardiaAlta.idTipoGuardia == g.idTipoGuardia
                           select new GuardiaVM
                           {
                               idGuardia = g.idGuardia,
                               idPersona = g.idPersona,
                               fechaDesde = g.FechaHoraDesde,
                               fechaHasta = g.FechaHoraHasta,
                               idMantenimiento = g.idMantenimiento,
                               idTipoGuardia = g.idTipoGuardia,
                               telefono = g.Telefono,
                               activo = g.activo,
                               idPersona2 = g.idPersona2,
                               telefono2 = g.Telefono2,
                               codigoRetorno = 200

                           }).ToList();
                if (guardia.Count > 26)
                {
                    cuenta = 150;
                }

                return cuenta;
            }
        }

        public int EliminarGuardia(GuardiaVM guardia)
        {
            using (BuhoGestionEntities db = new BuhoGestionEntities())
            {
                var guardiaEliminar = db.GuaGuardia.Find(guardia.idGuardia);
                if (guardiaEliminar != null)
                {
                    guardiaEliminar.activo = false;
                    db.SaveChanges();
                    return 200;
                }
                return 400;
            }
        }



        // ↓↓↓ Personal de Guardias ↓↓↓  

        public object TablaPersonal(bool? activo)
        {
            List<GuardiaVM> listaPersonal;
            // List<JefeGuardiaVM> listaJefe;
            using (BuhoGestionEntities db = new BuhoGestionEntities())
            {
                ServicioUsuario usuarioservice = new ServicioUsuario();

                listaPersonal = (from p in db.GralUsuario
                                 join t in db.GuaTelefonos on p.idUsuario equals t.idUsuario into te
                                 from tel in te.DefaultIfEmpty()
                                     //join j in db.GuaTelefonos on p.idUsuario equals j.idJefe
                                 where p.activo == 1 && p.esGuardia == true //&& p.esJefeGuardia!=true
                                 select new GuardiaVM()
                                 {
                                     idPersona = p.idUsuario,
                                     nombrePersona = p.Nombre,
                                     idJefe = tel.idJefe,
                                     // nombreJefe = tel.idJefe == -1 ? "Es Jefe" : p.Nombre,
                                     telefono = tel.telefono,
                                     activo = true,
                                 }).ToList();

                foreach (var persona in listaPersonal)
                {
                    var jefe = db.GralUsuario.Where(x => x.idUsuario == persona.idJefe).FirstOrDefault();
                    if (jefe != null)
                    {
                        persona.nombreJefe = jefe.Nombre;

                    }
                    else
                    {
                        persona.nombreJefe = "Es Jefe";
                    }
                }


                object json = new { data = listaPersonal };
                return json;
            }
        }
        /*
        public int AltaPersonal(GuardiaVM personal)
        {
            using (BuhoGestionEntities db = new BuhoGestionEntities())
            {
                var consultaUnPersonal = db.GuaPersonalGuardias.Where(x => x.idPersona == personal.idPersona).FirstOrDefault();
                var personalAnterior = db.GuaPersonalGuardias.Where(x => x.Nombre == personal.nombrePersona && x.Telefono == personal.telefono && x.Activo == true).FirstOrDefault();
                if (consultaUnPersonal == null && personalAnterior == null)
                {

                    try
                    {
                        if(personal.idJefe != -1)
                        {
                            GuaPersonalGuardias g = new GuaPersonalGuardias
                            {
                                Nombre = personal.nombrePersona,
                                idJefe = personal.idJefe,
                                Telefono = personal.telefono,
                                Activo = true,
                            };
                            db.GuaPersonalGuardias.Add(g);
                            db.SaveChanges();
                            return 200;
                        }
                        else
                        {
                            GuaPersonalGuardias g = new GuaPersonalGuardias
                            {
                                Nombre = personal.nombrePersona,
                                idJefe = null,
                                Telefono = personal.telefono,
                                Activo = true,
                            };
                            db.GuaPersonalGuardias.Add(g);
                            db.SaveChanges();
                            return 200;
                        }
                        
                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }

            }
        }*/

        public GuardiaVM ConsultarUnaPersona(int? idPersona)
        {
            GuardiaVM persona = new GuardiaVM();

            using (BuhoGestionEntities db = new BuhoGestionEntities())
            {

                persona = (from p in db.GralUsuario
                           join t in db.GuaTelefonos on p.idUsuario equals t.idUsuario into te
                           from tel in te.DefaultIfEmpty()
                           where p.idUsuario == idPersona
                           select new GuardiaVM()
                           {
                               idPersona = p.idUsuario,
                               nombrePersona = p.Nombre,
                               idJefe = tel.idJefe,
                               telefono = tel.telefono,
                               //activo = p.activo,
                           }).FirstOrDefault();
            }
            return persona;
        }

        public int ModificarPersonal(GuardiaVM persona)
        {
            using (BuhoGestionEntities db = new BuhoGestionEntities())
            {
                // var consultaUnPersonal = db.GralUsuario.Where(x => x.idUsuario == persona.idPersona).FirstOrDefault();
                var consultaTelefono = db.GuaTelefonos.Where(x => x.idUsuario == persona.idPersona).FirstOrDefault();
                if (consultaTelefono != null)
                {
                    //consultaUnPersonal.Nombre = persona.nombrePersona;
                    consultaTelefono.idJefe = persona.idJefe;
                    consultaTelefono.telefono = persona.telefono;
                    //consultaUnPersonal.activo = 1;
                    db.SaveChanges();
                }
                else
                {
                    GuaTelefonos nuevoTel = new GuaTelefonos
                    {
                        activo = true,
                        idJefe = persona.idJefe,
                        idUsuario = persona.idPersona,
                        telefono = persona.telefono
                    };
                    db.GuaTelefonos.Add(nuevoTel);
                    db.SaveChanges();
                }
            }
            return 200;
        }
        /*
        public int EliminarPersonal(GuardiaVM persona)
        {
            using (BuhoGestionEntities db = new BuhoGestionEntities())
            {
                var consultaUnPersonal = db.GralUsuario.Where(x => x.idUsuario == persona.idPersona).FirstOrDefault();
                if (consultaUnPersonal != null)
                {
                    consultaUnPersonal.activo = 0;
                }
                db.SaveChanges();
            }
            return 200;
        }

        public int RecuperarPersonal(GuardiaVM persona)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())

            {
                var consultaUnPersonal = db.GuaPersonalGuardias.Where(x => x.idPersona == persona.idPersona).FirstOrDefault();
                var mismaPersona = db.GuaPersonalGuardias.Where(x => x.Nombre == persona.nombrePersona && x.Activo == true).FirstOrDefault();
                if (consultaUnPersonal != null)
                {
                    if (mismaPersona == null)
                    {
                        consultaUnPersonal.Activo = true;
                        db.SaveChanges();
                        return 200;
                    }
                    else
                    {
                        return 300;
                    }
                }
                else
                {
                    return 400;
                }

            }

        }
        */

        public bool ValidarPersona(GuardiaVM persona)
        {
            if (!string.IsNullOrWhiteSpace(persona.nombrePersona.ToString()))
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        public object ObtenerJefes(bool activo)
        {
            List<GuardiaVM> listaJefes = new List<GuardiaVM>();
            using (BuhoGestionEntities db = new BuhoGestionEntities())
            {
                listaJefes = (from p in db.GralUsuario
                              join t in db.GuaTelefonos on p.idUsuario equals t.idUsuario into te
                              from tel in te.DefaultIfEmpty()
                              where p.activo == 1 && tel.idJefe == -1
                              select new GuardiaVM
                              {
                                  idPersona = p.idUsuario,
                                  nombrePersona = p.Nombre,
                                  telefono = tel.telefono,
                                  idJefe = tel.idJefe,
                              }).ToList();
            }
            object json = new { data = listaJefes };
            return json;

        }


        public object ObtenerListadoGuardias(bool? activo)
        {
            List<UsuarioVM> listadoGuardia = new List<UsuarioVM>();
            using (BuhoGestionEntities db = new BuhoGestionEntities())
            {
                listadoGuardia = (from u in db.GralUsuario
                                  where u.activo == 1 && u.esGuardia == true
                                  select new UsuarioVM
                                  {
                                      idUsuario = u.idUsuario,
                                      nombreUsuario = u.Nombre,
                                  }).ToList();
            }
            object json = new { data = listadoGuardia };
            return json;
        }


    }
}
