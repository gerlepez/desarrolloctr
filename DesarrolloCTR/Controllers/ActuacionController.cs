﻿using DesarrolloCTR.Controllers.Extra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers
{
    [SessionUtility]
    [Permiso("Actu")]
    public class ActuacionController : Controller
    {

        public ActionResult Actuacion()
        {
            return View();
        }
        public ActionResult AdministraActuacionRelaciones()
        {
            return View();
        }
        public ActionResult AdministraActuacionTipoYEquipo()
        {
            return View();
        }
        public ActionResult ActuacionTipo()
        {
            return View();
        }
        public ActionResult AdministraTipoActuacion()
        {
            return View();
        }
        public ActionResult Actuaciones()
        {
            return View();
        }
        public ActionResult AdministraActuaciones()
        {
            return View();
        }
    }
}
