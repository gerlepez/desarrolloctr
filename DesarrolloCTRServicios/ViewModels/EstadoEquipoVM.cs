﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class EstadoEquipoVM
    {
        public int idEstadoEquipo { get; set; }
        public string nombreEstadoEquipo { get; set; }
        public bool activo { get; set; }
    }
}
