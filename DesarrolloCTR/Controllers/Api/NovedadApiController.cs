﻿using DesarrolloCTR.Controllers;
using DesarrolloCTR.Controllers.Extra;
using DesarrolloCTRServicios.Servicios;
using DesarrolloCTRServicios.ViewModels;
using DesarrolloCTRUtilidades;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;


namespace DesarrolloCTR.Controllers.Api
{
    public class NovedadApiController : ApiController
    {
        //[System.Web.Http.Route("Api/Novedad/ObtenerListaNovedad2")]
        //[System.Web.Http.ActionName("ObtenerListaNovedad2")]
        //[System.Web.Http.HttpPost]
        //[System.Web.Http.Authorize]
        //public object ObtenerListaNovedad2(FormDataCollection form, bool activo, string fechaDesde, string fechaHasta,string descripcion, bool descCompleta, int idEquipo, string nombreEquipo,int idTipoEquipo, int tipoNovedad, int actuacion, int propiedad)
        //{
        //    var draw = form.Get("draw");
        //    var start = form.Get("start");
        //    var length = form.Get("length");
        //    var sortColumn = (form.Get("columns[" + form.Get("order[0][column]").FirstOrDefault() + "][data]").ToString()).ToString();
        //    var sortColumnDir = form.Get("order[0][dir]").ToString();
        //    var searchValue = form.Get("search[value]").ToString();
        //    int pageSize = length != null ? Convert.ToInt32(length) : 0;
        //    int skip = start != null ? Convert.ToInt32(start) : 0;
        //    int recordsTotal = 0;

        //    int cantidadRegistrosPagina = Int32.Parse(length);

        //    //Servicio.ServicioTicket _servicio = new Servicio.ServicioTicket();
        //    //var lst = _servicio.listarTickets(fechaDesde, fechaHasta, ref recordsTotal, sortColumn,
        //    //    sortColumnDir, searchValue, idSistema, idPerfil, idUsuario, idModulo, pageSize, skip);
        //    //((IDisposable)_servicio).Dispose();

        //    //var json = Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = lst });

        //    //return Request.CreateResponse(HttpStatusCode.OK, new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = lst }, Configuration.Formatters.JsonFormatter);

        //    ServicioNovedad servNovedad = new ServicioNovedad();

        //    object listaNovedad = servNovedad.ObtenerListaNovedad(activo, fechaDesde, fechaHasta, descripcion, descCompleta, idEquipo, nombreEquipo, idTipoEquipo, tipoNovedad, actuacion, propiedad, draw, recordsTotal, skip, pageSize);

        //    //((IDisposable)servNovedad).Dispose();

        //    return listaNovedad;
        //}


        [System.Web.Http.Route("Api/Novedad/ValidarNovedad")]
        [System.Web.Http.ActionName("ValidarNovedad")]
        [System.Web.Http.HttpGet]
        public bool ValidarNovedad(int idNovedad)
        {
            ServicioNovedad servicio = new ServicioNovedad();
            return servicio.ValidarCierreNovedad(idNovedad);

        }

        [System.Web.Http.Route("Api/Novedad/ImprimirPdf")]
        [System.Web.Http.ActionName("ImprimirPdf")]
        [System.Web.Http.HttpGet]
        public string ImprimirPdf(string fechaDesde, string fechaHasta)
        {

            GenerarPDF pdf = new GenerarPDF();
            ServicioNovedad servicioNovedad = new ServicioNovedad();
            var pdfvm = servicioNovedad.generarPdf(fechaDesde, fechaHasta);
            var pathYnombre = pdf.GenerarLibroNovedades(pdfvm, fechaDesde, fechaHasta);


            return pathYnombre;
        }

        [System.Web.Http.Route("Api/Novedad/ObtenerLibro")]
        [System.Web.Http.ActionName("ObtenerLibro")]
        [System.Web.Http.HttpGet]
        public List<TurnoPdfVM> ObtenerLibro(string fechaDesde, string fechaHasta, string texto = "")
        {

            GenerarPDF pdf = new GenerarPDF();
            ServicioNovedad servicioNovedad = new ServicioNovedad();
            var pdfvm = servicioNovedad.generarPdf(fechaDesde, fechaHasta);
            if (texto != "")
            {
                var pdfFiltrado = servicioNovedad.filtrarPdf(pdfvm, texto);
                return pdfFiltrado;
            }
            else
            {
                return pdfvm;
            }

        }


        [System.Web.Http.Route("Api/Novedad/DescargarPdf")]
        [System.Web.Http.ActionName("DescargarPdf")]
        [System.Web.Http.HttpGet]

        public IHttpActionResult DescargarPdf(string path, string nombreArchivo)
        {
            IHttpActionResult response;
            HttpResponseMessage responseMsg = new HttpResponseMessage(HttpStatusCode.OK);
            var fileStream = new FileStream(path, FileMode.Open);
            responseMsg.Content = new StreamContent(fileStream);
            responseMsg.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            responseMsg.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            responseMsg.Content.Headers.ContentDisposition.FileName = nombreArchivo;
            response = ResponseMessage(responseMsg);
            return response;
        }

        [System.Web.Http.Route("Api/Novedad/ObtenerNovedadPorFecha")]
        [System.Web.Http.ActionName("ObtenerNovedadPorFecha")]
        [System.Web.Http.HttpGet]
        public List<NovedadVM> ObtenerNovedadPorFecha(string fecha)
        {
            ServicioNovedad servicio = new ServicioNovedad();
            return servicio.ObtenerNovedadPorFecha(fecha);

        }

        [System.Web.Http.Route("Api/Novedad/ObtenerListaNovedad")]
        [System.Web.Http.ActionName("ObtenerListaNovedad")]
        [System.Web.Http.HttpPost]
        //[System.Web.Http.Authorize]
        public object ObtenerListaNovedad(FormDataCollection form, bool activo, DateTime fechaDesde, DateTime fechaHasta, string descripcion, bool descCompleta, int idEquipo,
            string nombreEquipo, int idTipoEquipo, int tipoNovedad, int actuacion, int propiedad, string codigoLicencia, int estadoNoveda, string novRelacionadas)
        {
            var draw = form.Get("draw");
            var start = form.Get("start");
            var length = form.Get("length");
            var sortColumn = "detalleNovedad.fechaHoraNovedad";//(form.Get("columns[" + form.Get("order[0][column]").FirstOrDefault() + "][data]").ToString()).ToString();
            var sortColumnDir = "desc";//form.Get("order[0][dir]").ToString();
            var searchValue = form.Get("search[value]").ToString();
            int tamano = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int cantidadDeRegistros = 0;

            int cantidadRegistrosPagina = Int32.Parse(length);
            ServicioNovedad servNovedad = new ServicioNovedad();

            var iquery = servNovedad.ObtenerGrillaNovedad(fechaDesde, fechaHasta);

            var listaNovedadFiltrada = servNovedad.ObtenerGrillaNovedadFiltrada(iquery, activo, fechaDesde, fechaHasta, ref cantidadDeRegistros, tipoNovedad, actuacion, skip, tamano, descripcion, codigoLicencia, estadoNoveda, novRelacionadas, sortColumn, sortColumnDir, searchValue);

            var json = Json(new
            {
                draw = draw,
                recordsFiltered = cantidadDeRegistros,
                recordsTotal = cantidadDeRegistros,
                data = listaNovedadFiltrada
            });
            return Request.CreateResponse(HttpStatusCode.OK, new
            {
                draw = draw,
                recordsFiltered = cantidadDeRegistros,
                recordsTotal = cantidadDeRegistros,
                data = listaNovedadFiltrada
            }, Configuration.Formatters.JsonFormatter);


        }



        [System.Web.Http.Route("Api/Novedad/ObtenerIndicadoresNovedad")]
        [System.Web.Http.ActionName("ObtenerIndicadoresNovedad")]
        [System.Web.Http.HttpPost]
        public IndicadoresNovedadVM ObtenerIndicadoresNovedad(bool activo, string fechaDesde, string fechaHasta, string descripcion, bool descCompleta, int idEquipo, string nombreEquipo, int idTipoEquipo, int tipoNovedad, int actuacion, int propiedad, string codigoLicencia)
        {
            ServicioNovedad servNovedad = new ServicioNovedad();
            var fechaDesdeDateTime = DateTime.ParseExact(fechaDesde, "dd/MM/yyyy", null);
            var fechaHastaDateTime = DateTime.ParseExact(fechaHasta, "dd/MM/yyyy", null);
            var iquery = servNovedad.ObtenerGrillaNovedad(fechaDesdeDateTime, fechaHastaDateTime);
            IndicadoresNovedadVM listaIndicadoresNovedad = servNovedad.ObtenerIndicadoresGrillaNovedad(iquery, activo, fechaDesdeDateTime, fechaHastaDateTime, tipoNovedad, actuacion, descripcion);

            return listaIndicadoresNovedad;
        }

        [System.Web.Http.Route("Api/Novedad/ObtenerLibroNovedad")]
        [System.Web.Http.ActionName("ObtenerLibroNovedad")]
        [System.Web.Http.HttpPost]
        //[System.Web.Http.Authorize]
        public object ObtenerLibroNovedad(FormDataCollection form, bool activo, string fechaDesde, string fechaHasta, string descripcion, bool descCompleta, int idEquipo, string nombreEquipo, int idTipoEquipo, int tipoNovedad, int actuacion, int propiedad)
        {
            var draw = form.Get("draw");
            var start = form.Get("start");
            var length = form.Get("length");
            var sortColumn = (form.Get("columns[" + form.Get("order[0][column]").FirstOrDefault() + "][data]").ToString()).ToString();
            var sortColumnDir = form.Get("order[0][dir]").ToString();
            var searchValue = form.Get("search[value]").ToString();
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;

            int cantidadRegistrosPagina = Int32.Parse(length);

            ServicioNovedad servNovedad = new ServicioNovedad();

            object listaNovedad = servNovedad.ObtenerLibroNovedad(activo, fechaDesde, fechaHasta, descripcion, descCompleta, idEquipo, nombreEquipo, idTipoEquipo, tipoNovedad, actuacion, propiedad, draw, recordsTotal, skip, pageSize);

            /*
            object listaNovedad = servNovedad.generarPdf(fechaDesde, fechaHasta);
            object json = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = listaNovedad };
            return json;*/

            return listaNovedad;
        }


        [System.Web.Http.Route("Api/Novedad/ObtenerListaEstadoNovedad")]
        [System.Web.Http.ActionName("ObtenerListaEstadoNovedad")]
        [System.Web.Http.HttpGet]
        //[System.Web.Http.Authorize]
        public List<EstadoNovedadVM> ObtenerListaEstadoNovedad()
        {
            ServicioEstadoNovedad servNovedadEstado = new ServicioEstadoNovedad();

            List<EstadoNovedadVM> listaEstadoNovedad = servNovedadEstado.ObtenerListaEstadoNovedad();

            //((IDisposable)servNovedad).Dispose();

            return listaEstadoNovedad;
        }
        [SessionUtilityAttribute]
        [System.Web.Http.Route("Api/Novedad/CRUD")]
        [System.Web.Http.ActionName("CRUD")]
        [System.Web.Http.HttpPost]
        public NovedadVM CRUD(NovedadVM novedad)
        {

            NovedadVM nov = new NovedadVM();
            var idUsuarioLogueado = 0;
            if (novedad != null)
            {
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioLogueado = serUsuario.ObtenerUsuarioLogueado(novedad.nombresuario);

                ServicioTurno servTurno = new ServicioTurno();
                if (!servTurno.ObtenerTurnoEjecucion(idUsuarioLogueado))
                {
                    nov.codigo = 500;
                    return nov;
                }
            }
            ServicioNovedad servNovedad = new ServicioNovedad();
            var tipoCRUD = novedad.tipoCRUD;
            if (servNovedad.ValidarNovedad(novedad))
            {
                nov = servNovedad.CRUD(novedad, idUsuarioLogueado, tipoCRUD);
            }
            else
            {
                nov.codigo = 400;

            }
            //((IDisposable)servNovedad).Dispose();
            return nov;
        }




        [System.Web.Http.Route("Api/Novedad/RelacionarRuteNovedad")]
        [System.Web.Http.ActionName("RelacionarRuteNovedad")]
        [System.Web.Http.HttpPost]
        public int RelacionarRuteNovedad(int idLicLicenciaUsuario, int idNovedad)
        {
            ServicioNovedad servNovedad = new ServicioNovedad();
            int codigo = servNovedad.RelacionarRuteNovedad(idLicLicenciaUsuario, idNovedad);


            return codigo;
        }

        [System.Web.Http.Route("Api/Novedad/CerrarNovedad")]
        [System.Web.Http.ActionName("CerrarNovedad")]
        [System.Web.Http.HttpPost]
        public int CerrarNovedad(int idNovedad)
        {
            ServicioNovedad servNovedad = new ServicioNovedad();
            int codigo = servNovedad.CerrarNovedad(idNovedad);
            //((IDisposable)servNovedad).Dispose();

            return codigo;
        }

        [System.Web.Http.Route("Api/Novedad/CerrarNoveadesConLicencia")]
        [System.Web.Http.ActionName("CerrarNoveadesConLicencia")]
        [System.Web.Http.HttpPost]
        public int CerrarNoveadesConLicencia(int? idNovedad)
        {
            ServicioNovedad servNovedad = new ServicioNovedad();
            int codigo = servNovedad.CerrarNoveadesConLicencia(idNovedad);
            //((IDisposable)servNovedad).Dispose();

            return codigo;
        }

        [System.Web.Http.Route("Api/Novedad/consultarRamaNovedades")]
        [System.Web.Http.ActionName("consultarRamaNovedades")]
        [System.Web.Http.HttpPost]
        public bool consultarRamaNovedades(int idNovedad)
        {
            ServicioNovedad servNovedad = new ServicioNovedad();
            bool codigo = servNovedad.consultarRamaNovedades(idNovedad);
            //((IDisposable)servNovedad).Dispose();

            return codigo;
        }


        [System.Web.Http.Route("Api/Novedad/ObtenerRamaCompletaDeNovedad")]
        [System.Web.Http.ActionName("ObtenerRamaCompletaDeNovedad")]
        [System.Web.Http.HttpGet]
        public List<NovedadVM> ObtenerRamaCompletaDeNovedad(int idNovedad)
        {
            ServicioNovedad servNovedad = new ServicioNovedad();
            var listado = servNovedad.ObtenerRamaCompletaDeNovedad(idNovedad);

            return listado;
        }

        [System.Web.Http.Route("Api/Novedad/ObtenerLaUltimaNovedadRelacionada")]
        [System.Web.Http.ActionName("ObtenerLaUltimaNovedadRelacionada")]
        [System.Web.Http.HttpGet]
        public object ObtenerLaUltimaNovedadRelacionada(int idNovedad)
        {
            ServicioNovedad servNovedad = new ServicioNovedad();
            var listado = servNovedad.ObtenerLaUltimaNovedadRelacionada(idNovedad);

            return listado;
        }

        [System.Web.Http.Route("Api/Novedad/ValidarNovedadRelacionada")]
        [System.Web.Http.ActionName("ValidarNovedadRelacionada")]
        [System.Web.Http.HttpGet]
        public bool ValidarNovedadRelacionada(int idNovedad)
        {
            //revisar esta validacion
            ServicioNovedad servNovedad = new ServicioNovedad();
            var codigo = servNovedad.ValidarNovedadRelacionada(idNovedad);

            return codigo;
        }

        [System.Web.Http.Route("Api/Novedad/RecuperarNovedad")]
        [System.Web.Http.ActionName("RecuperarNovedad")]
        [System.Web.Http.HttpPost]
        public int RecuperarNovedad(NovedadVM novedad)
        {
            ServicioNovedad servNovedad = new ServicioNovedad();
            int codigo = servNovedad.RecuperarNovedad(novedad);
            return codigo;
        }

        //consultar una novedad y su estado
        [System.Web.Http.Route("Api/Novedad/ConsultarUnaNovedad")]
        [System.Web.Http.ActionName("ConsultarUnaNovedad")]
        [System.Web.Http.HttpGet]
        public NovedadVM ConsultarUnaNovedad(int idNovedad)
        {
            ServicioNovedad servNovedad = new ServicioNovedad();
            NovedadVM novedad = servNovedad.ConsultarUnaNovedad(idNovedad, true);

            return novedad;
        }


        //TIPO NOVEDAD

        [System.Web.Http.Route("Api/Novedad/ObtenerListaTipoNovedad")]
        [System.Web.Http.ActionName("ObtenerListaTipoNovedad")]
        [System.Web.Http.HttpGet]
        public object ObtenerListaTipoNovedad(bool activo)
        {
            ServicioTipoNovedad servTipoNovedad = new ServicioTipoNovedad();
            object listaTiponovedad = servTipoNovedad.ObtenerListaTipoNovedad(activo);
            return listaTiponovedad;
        }

        [System.Web.Http.Route("Api/Novedad/ObtenerListaTipoNovedad")]
        [System.Web.Http.ActionName("ObtenerListaTipoNovedad")]
        [System.Web.Http.HttpGet]
        public List<TipoNovedadVM> ObtenerListaTipoNovedad()
        {
            ServicioTipoNovedad servTipoNovedad = new ServicioTipoNovedad();
            List<TipoNovedadVM> listaTiponovedad = servTipoNovedad.ObtenerListaTipoNovedad();
            return listaTiponovedad;
        }

        //devuelve los una novedad con solo los true
        [System.Web.Http.Route("Api/Novedad/ObtenerUnTipoNovedad")]
        [System.Web.Http.ActionName("ObtenerUnTipoNovedad")]
        [System.Web.Http.HttpGet]
        public TipoNovedadVM ObtenerUnTipoNovedad(int idTipoNovedad)
        {
            ServicioTipoNovedad servTipoNovedad = new ServicioTipoNovedad();
            TipoNovedadVM untipoNovedad = servTipoNovedad.ObtenerUnTipoNovedad(idTipoNovedad);
            return untipoNovedad;
        }

        [System.Web.Http.Route("Api/Novedad/AltaTipoNovedad")]
        [System.Web.Http.ActionName("AltaTipoNovedad")]
        [System.Web.Http.HttpPost]
        public int AltaTipoNovedad(TipoNovedadVM tiponovedad)
        {
            ServicioTipoNovedad servTipoNovedad = new ServicioTipoNovedad();
            int codigo;
            if (servTipoNovedad.ValidarTipoNovedad(tiponovedad))
            {
                codigo = servTipoNovedad.AltaTipoNovedad(tiponovedad);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }

        [System.Web.Http.Route("Api/Novedad/ModificarTipoNovedad")]
        [System.Web.Http.ActionName("ModificarTipoNovedad")]
        [System.Web.Http.HttpPost]
        public object ModificarTipoNovedad(TipoNovedadVM tiponovedad)
        {
            ServicioTipoNovedad servTipoNovedad = new ServicioTipoNovedad();
            int codigo;
            if (servTipoNovedad.ValidarTipoNovedad(tiponovedad))
            {
                codigo = servTipoNovedad.ModificarTipoNovedad(tiponovedad);
            }
            else
            {
                codigo = 400;
            }
            return codigo;
        }


        [System.Web.Http.Route("Api/Novedad/EliminarTipoNovedad")]
        [System.Web.Http.ActionName("EliminarTipoNovedad")]
        [System.Web.Http.HttpPost]
        public int EliminarTipoNovedad(TipoNovedadVM tiponovedad)
        {
            ServicioTipoNovedad servTipoNovedad = new ServicioTipoNovedad();
            int codigo = servTipoNovedad.EliminarTipoNovedad(tiponovedad);
            return codigo;
        }


        [System.Web.Http.Route("Api/Novedad/RecuperarTipoNovedad")]
        [System.Web.Http.ActionName("RecuperarTipoNovedad")]
        [System.Web.Http.HttpPost]
        public int RecuperarTipoNovedad(TipoNovedadVM tiponovedad)
        {
            ServicioTipoNovedad servTipoNovedad = new ServicioTipoNovedad();
            int codigo = servTipoNovedad.RecuperarTipoNovedad(tiponovedad);
            return codigo;
        }

        //[system.web.http.route("api/novedad/obtenerlistanovedadrelacionada")]
        //[system.web.http.actionname("obtenerlistanovedadrelacionada")]
        //[system.web.http.httpget]
        //public list<novedadrelacionadavm> obtenerlistanovedadrelacionada(int idnovedad, bool activo)
        //{
        //    servicionovedad servnovedad = new servicionovedad();
        //    list<novedadrelacionadavm> novedadrelacionada = servnovedad.obtenerlistanovedadrelacionada(idnovedad, activo);
        //    return novedadrelacionada;
        //}

        [System.Web.Http.Route("Api/Novedad/GenerarNovedadRelacionada")]
        [System.Web.Http.ActionName("GenerarNovedadRelacionada")]
        [System.Web.Http.HttpPost]
        public NovedadVM GenerarNovedadRelacionada(NovedadVM novedad)
        {
            ServicioNovedad servNovedad = new ServicioNovedad();
            var idUsuarioAlta = 0;
            if (novedad != null)
            {
                ServicioUsuario serUsuario = new ServicioUsuario();
                idUsuarioAlta = serUsuario.ObtenerUsuarioLogueado(novedad.nombresuario);

            }

            NovedadVM novedadRelacionada = servNovedad.GenerarNovedadRelacionada(novedad, idUsuarioAlta, false);
            return novedadRelacionada;
        }

        [HttpGet]
        [Route("Api/Novedad/GetFile")]
        [System.Web.Http.ActionName("GetFile")]
        public HttpResponseMessage GetFile(int idArchivo)
        {
            //Create HTTP Response.
            ServicioNovedad servicio = new ServicioNovedad();
            var archivo = servicio.GetArchivo(idArchivo);
            var fileUrl = archivo.urlArchivo;
            var fileName = archivo.nombreArchivo;
            ArchivoApiController apiArchivo = new ArchivoApiController();
            return apiArchivo.GetFile(fileUrl, "Novedad", fileName);

        }

        [System.Web.Http.Route("Api/Novedad/GuardarArchivoNovedad")]
        [System.Web.Http.ActionName("GuardarArchivoNovedad")]
        [System.Web.Http.HttpPost]
        public int GuardarArchivoNovedad()
        {
            ArchivoApiController apiArchivos = new ArchivoApiController();
            var listArchivos = apiArchivos.Upload("Novedad");
            ServicioNovedad servicio = new ServicioNovedad();
            if (listArchivos.Count > 0)
            {
                if (listArchivos[0].error != 400)
                {
                    return servicio.GuardarArchivoNovedad(listArchivos);
                }
                else
                {
                    return 500;
                }
            }
            else
            {
                return 500;
            }



        }

        [System.Web.Http.Route("Api/Novedad/EliminarArchivoNovedad")]
        [System.Web.Http.ActionName("EliminarArchivoNovedad")]
        [System.Web.Http.HttpPost]
        public int EliminarArchivoNovedad(string listArchivo)
        {

            ServicioNovedad servicio = new ServicioNovedad();
            return servicio.EliminarArchivoNovedad(listArchivo);

        }


        [System.Web.Http.Route("Api/Novedad/ObtenerEstadoNovedad")]
        [System.Web.Http.ActionName("ObtenerEstadoNovedad")]
        [System.Web.Http.HttpGet]
        public object ObtenerEstadoNovedad(bool activo)
        {
            ServicioNovedad servicio = new ServicioNovedad();
            object listEstado = servicio.ObtenerTipoEstadoNovedad(activo);
            return listEstado;
        }


        [System.Web.Http.Route("Api/Novedad/consultaTurnoEjecucion")]
        [System.Web.Http.ActionName("consultaTurnoEjecucion")]
        [System.Web.Http.HttpPost]
        public int consultaTurnoEjecucion(int? idNovedad)
        {

            ServicioNovedad servicio = new ServicioNovedad();
            return servicio.consultaTurnoEjecucion(idNovedad);

        }
    }



}
