﻿using DesarrolloCTR.Controllers.Extra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesarrolloCTR.Controllers
{
    [SessionUtility]
    [Permiso("Gen")]
    public class GeneradorController : Controller
    {
        // GET: Generadores
        public ActionResult Generador()
        {
            return View();
        }
        public ActionResult AdministrarGenerador()
        {
            return View();
        }
    }
}