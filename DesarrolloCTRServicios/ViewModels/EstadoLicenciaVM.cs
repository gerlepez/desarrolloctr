﻿namespace DesarrolloCTRServicios.ViewModels
{
    public class EstadoLicenciaVM
    {
        public int idEstadoLicencia { get; set; }
        public string nombreEstadoLicencia { get; set; }
        public bool? activo { get; set; }
    }
}
