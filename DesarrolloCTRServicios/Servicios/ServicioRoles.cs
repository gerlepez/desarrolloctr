﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{
    public class ServicioRoles
    {
        private IntranetCTREntities db = new IntranetCTREntities();

        public object ObtenerPersonasRoles(bool? activo)
        {
            List<UsuarioRolVM> listaUsuarioRol;
            ServicioUsuario usuarioservice = new ServicioUsuario();
            listaUsuarioRol = (from u in db.GralUsuarioRol
                               join r in db.GralRol on u.idRol equals r.idRol
                               where u.activo == activo && r.idRol != 1
                               select new { u, r }).AsEnumerable().Select(x => new UsuarioRolVM
                               {
                                   idUsuarioRol = x.u.idUsuarioRol,
                                   idRol = x.u.idRol,
                                   nombreRol = x.r.nombreRol,
                                   usuario = usuarioservice.ObtenerDatosUsuarioPorId(x.u.idUsuario),
                                   activo = x.u.activo,
                               }).ToList();


            object json = new { data = listaUsuarioRol };
            return json;
        }

        public List<RolVM> ObtenerRoles(int idRol)
        {
            List<RolVM> listaRoles = new List<RolVM>();
            listaRoles = (from r in db.GralRol
                          where r.activo == true
                          select new RolVM
                          {
                              idRol = r.idRol,
                              nombreRol = r.nombreRol,
                          }).ToList();

            return listaRoles;
        }

        public int AltaRoles(UsuarioRolVM rol)
        {
            var consultaUnRol = db.GralUsuarioRol.Where(x => x.idUsuarioRol == rol.idUsuarioRol).FirstOrDefault();
            if (consultaUnRol == null)
            {
                GralUsuarioRol u = new GralUsuarioRol
                {
                    idRol = rol.idRol,
                    idUsuario = rol.idUsuario,
                    activo = true,
                };
                db.GralUsuarioRol.Add(u);
                db.SaveChanges();
                return 200;
            }
            else
            {
                return 400; //Ya existe
            }
        }

        public UsuarioRolVM ConsultarUnRol(int idRol)
        {
            var rol = (from u in db.GralUsuarioRol
                       join r in db.GralRol on u.idRol equals r.idRol
                       where u.idUsuarioRol == idRol
                       select new UsuarioRolVM()
                       {
                           idUsuarioRol = u.idUsuarioRol,
                           idRol = u.idRol,
                           idUsuario = u.idUsuario,
                           activo = u.activo,
                       }).FirstOrDefault();

            return rol;
        }

        public int ModificarRol(UsuarioRolVM rol)
        {
            var consultaUnRol = db.GralUsuarioRol.Where(x => x.idUsuarioRol == rol.idUsuarioRol).FirstOrDefault();
            if (consultaUnRol != null)
            {
                consultaUnRol.idRol = rol.idRol;
                consultaUnRol.idUsuario = rol.idUsuario;
                consultaUnRol.activo = true;
                db.SaveChanges();
            }
            return 200;
        }

        public int EliminarRol(UsuarioRolVM rol)
        {
            var consultaUnRol = db.GralUsuarioRol.Where(x => x.idUsuarioRol == rol.idUsuarioRol).FirstOrDefault();
            if (consultaUnRol != null)
            {
                consultaUnRol.activo = false;
            }
            db.SaveChanges();

            return 200;
        }

        public int RecuperarRol(UsuarioRolVM rol)
        {
            var consultaUnRol = db.GralUsuarioRol.Where(x => x.idUsuarioRol == rol.idUsuarioRol).FirstOrDefault();
            if (consultaUnRol != null)
            {
                consultaUnRol.activo = true;
                db.SaveChanges();
                return 200;
            }
            else
            {
                return 400;
            }

        }

        public bool ValidarRol(UsuarioRolVM rol)
        {
            if (rol.idUsuarioRol > 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        public object ObtenerListaUsuarioCTR()
        {
            using (BuhoGestionEntities buhodb = new BuhoGestionEntities())
            {

                List<UsuarioVM> listaUsuario = new List<UsuarioVM>();
                listaUsuario = (from c in buhodb.GralUsuario

                                where c.activo == 1
                                select new UsuarioVM
                                {
                                    idUsuario = c.idUsuario,
                                    nombreUsuario = c.Nombre,
                                }).ToList();

                /*object json = new { data = listaUsuario };
                return json;*/
                List<UsuarioVM> lista = new List<UsuarioVM>();
                foreach (var usuario in listaUsuario)
                {
                    var a = 0;
                    var usuarioRol = db.GralUsuarioRol.Where(x => x.activo == true).ToList();
                    foreach (var usuRol in usuarioRol)
                    {
                        if (usuRol.idUsuario == usuario.idUsuario)
                        {
                            a = 1;
                        }
                    }
                    if (a == 0)
                    {
                        lista.Add(usuario);
                    }

                }
                return lista;

            }

        }

        public object ObtenerListaUsuarioCTREditar()
        {
            using (BuhoGestionEntities buhodb = new BuhoGestionEntities())
            {

                List<UsuarioVM> listaUsuario = new List<UsuarioVM>();
                listaUsuario = (from c in buhodb.GralUsuario

                                where c.activo == 1
                                select new UsuarioVM
                                {
                                    idUsuario = c.idUsuario,
                                    nombreUsuario = c.Nombre,
                                }).ToList();

                object json = new { data = listaUsuario };
                return json;

            }

        }
    }
}
