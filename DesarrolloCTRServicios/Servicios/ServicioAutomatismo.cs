﻿using DesarrolloCTRModelos;
using DesarrolloCTRServicios.ViewModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DesarrolloCTRServicios.Servicios
{

    public class ServicioAutomatismo
    {
        IntranetCTREntities db = new IntranetCTREntities();

        public object ObtenerAutomatismo(bool activo)
        {
            var listaAutom = (from aut in db.AutAutomatismo
                              where aut.activo == activo && aut.fechaEdicion == null
                              select new AutomatismoVM
                              {
                                  activo = aut.activo,
                                  idAutomatismo = aut.idAutomatismo,
                                  nombreAutomatismo = aut.nombreAutomatismo,
                                  TieneEquipo = aut.TieneEquipo,
                                  tieneE = aut.TieneEquipo == true ? "Tiene" : "No tiene",

                              }).ToList();
            object json = new { data = listaAutom };
            return json;
        }

        public AutomatismoVM ConsultarUnAutomatismo(int idAutomatismo)
        {
            var autom = (from aut in db.AutAutomatismo
                             //Equipo
                             /* join ae in db.AutAutomTipoEquipo on aut.idAutomatismo equals ae.idAutomatismo
                              join eq in db.EquiEquipo on ae.idTipoEquipo equals eq.idEquipo
                              //Estacion
                              join aes in db.AutAutomatismoEstacion on aut.idAutomatismo equals aes.idAutomatismo
                              join es in db.EstEstacion on aes.idEstacion equals es.idEstacion*/
                         where aut.idAutomatismo == idAutomatismo
                         select new AutomatismoVM
                         {
                             activo = aut.activo,
                             idAutomatismo = aut.idAutomatismo,
                             nombreAutomatismo = aut.nombreAutomatismo,
                             TieneEquipo = aut.TieneEquipo,
                             /*equipo = new EquipoVM
                             {
                                 idEquipo = eq.idEquipo,
                                 nombreEquipo = eq.nombreEquipo

                             },
                             estacion = new EstacionVM
                             {
                                 idEstacion = es.idEstacion,
                                 nombreEstacion = es.nombreEstacion

                             }*/
                         }).FirstOrDefault();
            return autom;
        }
        public object ObtenerEquiposPorTipoYLugar(int idLugar, int automatismo)
        {
            List<AutomatismoVM> listRelaciones = (from r in db.AutRelacionAutomatismo
                                                  where r.idLugar == idLugar
                                                  join a in db.AutAutomatismo on r.idAutomatismo equals a.idAutomatismo
                                                  join e in db.EquiEquipo on r.idTipoEquipo equals e.idEquipo

                                                  where a.idAutomatismo == automatismo
                                                  select new { r, a, e }).Select(x => new AutomatismoVM
                                                  {
                                                      idAutoAutomRelacion = x.r.idAutoAutomRelacion,
                                                      idAutomatismo = x.a.idAutomatismo,
                                                      nombreAutomatismo = x.a.nombreAutomatismo,

                                                      idTipoEquipo = x.r.idTipoEquipo,
                                                      nombreTipoEquipo = x.e.nombreEquipo,

                                                      idEquipo = x.r.idEquipo,
                                                      idLugar = x.r.idLugar,
                                                  }).ToList();

            foreach (var relacion in listRelaciones)
            {
                switch (relacion.idTipoEquipo)
                {
                    case 1: //Transformador
                        var Transformador = db.TranTransformador.Where(x => x.idTransformador == relacion.idEquipo).FirstOrDefault();
                        var Estacion = db.EstEstacion.Where(x => x.idEstacion == relacion.idLugar).FirstOrDefault();
                        if (Transformador != null)
                        {
                            relacion.nombreEquipo = Transformador.nombreTransformador;
                        }
                        if (Estacion != null)
                        {
                            relacion.nombreLugar = Estacion.nombreEstacion;
                        }
                        break;
                    case 2: //Pto Conexion
                        var PtoConexion = db.PCPuntoConexion.Where(x => x.idPuntoConexion == relacion.idEquipo).FirstOrDefault();
                        var EstacionPto = db.EstEstacion.Where(x => x.idEstacion == relacion.idLugar).FirstOrDefault();
                        if (PtoConexion != null)
                        {
                            relacion.nombreEquipo = PtoConexion.nombrePuntoConexion;
                        }
                        if (EstacionPto != null)
                        {
                            relacion.nombreLugar = EstacionPto.nombreEstacion;
                        }
                        break;
                    case 5: //Generador
                        var Generador = db.GenGenerador.Where(x => x.idGenerador == relacion.idEquipo).FirstOrDefault();
                        var Central = db.CConCentral.Where(x => x.idCentral == relacion.idLugar).FirstOrDefault();
                        if (Generador != null)
                        {
                            relacion.nombreEquipo = Generador.nombreGenerador;
                        }
                        if (Central != null)
                        {
                            relacion.nombreLugar = Central.nombreCentral;
                        }
                        break;

                    case 7: //Capacitor
                        var Capacitor = db.CapCapacitor.Where(x => x.idCapacitor == relacion.idEquipo).FirstOrDefault();
                        var EstacionCapacitor = db.EstEstacion.Where(x => x.idEstacion == relacion.idLugar).FirstOrDefault();
                        if (Capacitor != null)
                        {
                            relacion.nombreEquipo = Capacitor.nombreCapacitor;
                        }
                        if (EstacionCapacitor != null)
                        {
                            relacion.nombreLugar = EstacionCapacitor.nombreEstacion;
                        }
                        break;
                }
            }

            object json = new { data = listRelaciones };
            return json;
        }

        public object ObtenerAutomatismosDetalleLugar(int idLugar, int automatismo, int idNovedad)
        {

            var equiposId = (from ae in db.AutRelacionAutomatismo
                             where ae.idAutomatismo == automatismo
                             join eq in db.EquiEquipo on ae.idTipoEquipo equals eq.idEquipo
                             select (eq)).ToList();
            var listaDetalle = new List<object>();
            List<AutomatismoVM> listaAutomatismos = new List<AutomatismoVM>();
            foreach (var item in equiposId)
            {
                if (item.codigoEquipo == "PCON")
                {
                    ServicioPuntoConexion servicio = new ServicioPuntoConexion();
                    listaDetalle.Add(servicio.ObtenerPuntosPorEstacion(idLugar, idNovedad));

                }
                else if (item.codigoEquipo == "TRA")
                {
                    ServicioTransformador servicio = new ServicioTransformador();
                    listaDetalle.Add(servicio.ObtenerTransformadoresPorEstacion(idLugar, idNovedad));

                }
                else if (item.codigoEquipo == "CAP")
                {
                    ServicioCapacitores servicio = new ServicioCapacitores();
                    listaDetalle.Add(servicio.ObtenerCapacitoresPorEstacion(idLugar, idNovedad));
                }
                else if (item.codigoEquipo == "GEN")
                {
                    ServicioGenerador servicio = new ServicioGenerador();
                    listaDetalle.Add(servicio.ObtenerGeneradoresPorCentral(idLugar, idNovedad));
                }
            }

            foreach (var item in listaDetalle)
            {
                var ListaDetalleEquipo = (IEnumerable)item.GetType().GetProperty("data").GetValue(item);
                foreach (var detalle in ListaDetalleEquipo)
                {

                    var det = detalle.GetType().GetProperty("descripcion").GetValue(detalle);
                    var idGral = detalle.GetType().GetProperty("idGeneral").GetValue(detalle);
                    var esAutomatismo = (bool)detalle.GetType().GetProperty("esAutomatismo").GetValue(detalle);
                    if (esAutomatismo)
                    {
                        AutomatismoVM aut = new AutomatismoVM();
                        aut.idDetalleAutomatismo = (int?)idGral;
                        aut.nombreAutomatismo = det.ToString();
                        listaAutomatismos.Add(aut);
                    }
                }
            }
            object json = new { data = listaAutomatismos };
            return json;

        }


        public int AltaAutomatismo(AutomatismoVM automatismo)
        {

            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnAutomatismo = db.AutAutomatismo.Where(x => x.idAutomatismo == automatismo.idAutomatismo).FirstOrDefault();
                var automatismoAnterior = db.AutAutomatismo.Where(x => x.nombreAutomatismo == automatismo.nombreAutomatismo).FirstOrDefault();
                // var consultaUnEquipo = db.EquiEquipo.Where(x => x.idEquipo == automatismo.equipo.idEquipo).FirstOrDefault();
                //   var consultaUnaEstacion = db.EstEstacion.Where(x => x.idEstacion == automatismo.estacion.idEstacion).FirstOrDefault();

                if (consultaUnAutomatismo == null && automatismoAnterior == null)
                {
                    try
                    {
                        AutAutomatismo a = new AutAutomatismo
                        {
                            nombreAutomatismo = automatismo.nombreAutomatismo,
                            TieneEquipo = true,
                            fechaCreacion = DateTime.Now,
                            activo = true,
                        };
                        db.AutAutomatismo.Add(a);
                        db.SaveChanges();
                        /*
                        AutAutomTipoEquipo e = new AutAutomTipoEquipo
                        {
                            idAutomatismo = a.idAutomatismo,
                            idTipoEquipo = automatismo.equipo.idEquipo,
                            activo = true
                        };
                        db.AutAutomTipoEquipo.Add(e);
                        db.SaveChanges();

                        AutAutomatismoEstacion es = new AutAutomatismoEstacion
                        {
                            idAutomatismo = a.idAutomatismo,
                            idEstacion = automatismo.estacion.idEstacion,
                            activo = true
                        };
                        db.AutAutomatismoEstacion.Add(es);
                        db.SaveChanges();*/
                        return 200;

                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }

            }

        }

        public int ModificaraAutomatismo(AutomatismoVM automatismo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnAutomatismo = db.AutAutomatismo.Where(x => x.idAutomatismo == automatismo.idAutomatismo).FirstOrDefault();
                if (consultaUnAutomatismo != null)
                {
                    consultaUnAutomatismo.fechaEdicion = DateTime.Now;
                    consultaUnAutomatismo.activo = false;
                    AutAutomatismo a = new AutAutomatismo
                    {
                        nombreAutomatismo = automatismo.nombreAutomatismo,
                        TieneEquipo = true,
                        fechaCreacion = DateTime.Now,
                        activo = true,
                    };
                    db.AutAutomatismo.Add(a);
                    db.SaveChanges();

                }

            }
            return 200;
        }


        public int EliminarAutomatismo(AutomatismoVM automatismo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnAutomatismo = db.AutAutomatismo.Where(x => x.idAutomatismo == automatismo.idAutomatismo).FirstOrDefault();
                // var consultaUnEquipo = db.AutAutomTipoEquipo.Where(x => x.idAutomatismo == automatismo.idAutomatismo).FirstOrDefault();
                //   var consultaUnaEstacion = db.AutAutomatismoEstacion.Where(x => x.idAutomatismo == automatismo.idAutomatismo).FirstOrDefault();
                if (consultaUnAutomatismo != null /*&& consultaUnEquipo != null && consultaUnaEstacion != null*/)
                {
                    consultaUnAutomatismo.activo = false;
                    //      consultaUnEquipo.activo = false;
                    //      consultaUnaEstacion.activo = false;
                }
                db.SaveChanges();
            }
            return 200;
        }


        public int RecuperarAutomatismo(AutomatismoVM automatismo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())

            {
                var consultaUnAutomatismo = db.AutAutomatismo.Where(x => x.idAutomatismo == automatismo.idAutomatismo).FirstOrDefault();
                //var consultaUnEquipo = db.AutAutomTipoEquipo.Where(x => x.idAutomatismo == automatismo.idAutomatismo).FirstOrDefault();
                //var consultaUnaEstacion = db.AutAutomatismoEstacion.Where(x => x.idAutomatismo == automatismo.idAutomatismo).FirstOrDefault();
                if (consultaUnAutomatismo != null/* && consultaUnEquipo != null && consultaUnaEstacion != null*/)
                {
                    consultaUnAutomatismo.activo = true;
                    // consultaUnEquipo.activo = true;
                    // consultaUnaEstacion.activo = true;
                    db.SaveChanges();
                    return 200;
                }
                else
                {
                    return 400;
                }

            }

        }


        public bool ValidarAutomatismo(AutomatismoVM automatismo)
        {
            if (!string.IsNullOrWhiteSpace(automatismo.nombreAutomatismo.ToString()))
            {
                return true;
            }
            else
            {
                return false;
            }

        }



        // Relaciones

        public object ObtenerRelaciones(bool? activo)
        {

            List<AutomatismoVM> listRelaciones = (from r in db.AutRelacionAutomatismo
                                                  join a in db.AutAutomatismo on r.idAutomatismo equals a.idAutomatismo
                                                  join e in db.EquiEquipo on r.idTipoEquipo equals e.idEquipo

                                                  where r.activo == activo
                                                  select new { r, a, e }).Select(x => new AutomatismoVM
                                                  {
                                                      idAutoAutomRelacion = x.r.idAutoAutomRelacion,
                                                      idAutomatismo = x.a.idAutomatismo,
                                                      nombreAutomatismo = x.a.nombreAutomatismo,

                                                      idTipoEquipo = x.r.idTipoEquipo,
                                                      nombreTipoEquipo = x.e.nombreEquipo,

                                                      idEquipo = x.r.idEquipo,
                                                      idLugar = x.r.idLugar,

                                                      activo = x.r.activo,
                                                  }).ToList();

            foreach (var relacion in listRelaciones)
            {
                switch (relacion.idTipoEquipo)
                {
                    case 1: //Transformador
                        var Transformador = db.TranTransformador.Where(x => x.idTransformador == relacion.idEquipo).FirstOrDefault();
                        var Estacion = db.EstEstacion.Where(x => x.idEstacion == relacion.idLugar).FirstOrDefault();
                        if (Transformador != null)
                        {
                            relacion.nombreEquipo = Transformador.nombreTransformador;
                        }
                        if (Estacion != null)
                        {
                            relacion.nombreLugar = Estacion.nombreEstacion;
                        }
                        break;
                    case 2: //Pto Conexion
                        var PtoConexion = db.PCPuntoConexion.Where(x => x.idPuntoConexion == relacion.idEquipo).FirstOrDefault();
                        var EstacionPto = db.EstEstacion.Where(x => x.idEstacion == relacion.idLugar).FirstOrDefault();
                        if (PtoConexion != null)
                        {
                            relacion.nombreEquipo = PtoConexion.nombrePuntoConexion;
                        }
                        if (EstacionPto != null)
                        {
                            relacion.nombreLugar = EstacionPto.nombreEstacion;
                        }
                        break;
                    case 5: //Generador
                        var Generador = db.GenGenerador.Where(x => x.idGenerador == relacion.idEquipo).FirstOrDefault();
                        var Central = db.CConCentral.Where(x => x.idCentral == relacion.idLugar).FirstOrDefault();
                        if (Generador != null)
                        {
                            relacion.nombreEquipo = Generador.nombreGenerador;
                        }
                        if (Central != null)
                        {
                            relacion.nombreLugar = Central.nombreCentral;
                        }
                        break;

                    case 7: //Capacitor
                        var Capacitor = db.CapCapacitor.Where(x => x.idCapacitor == relacion.idEquipo).FirstOrDefault();
                        var EstacionCapacitor = db.EstEstacion.Where(x => x.idEstacion == relacion.idLugar).FirstOrDefault();
                        if (Capacitor != null)
                        {
                            relacion.nombreEquipo = Capacitor.nombreCapacitor;
                        }
                        if (EstacionCapacitor != null)
                        {
                            relacion.nombreLugar = EstacionCapacitor.nombreEstacion;
                        }
                        break;
                }
            }

            object json = new { data = listRelaciones };
            return json;
        }

        public AutomatismoVM ConsultarUnaRelacion(int idAutAutoRelacion)
        {
            var relacion = (from r in db.AutRelacionAutomatismo
                            join a in db.AutAutomatismo on r.idAutomatismo equals a.idAutomatismo
                            join e in db.EquiEquipo on r.idTipoEquipo equals e.idEquipo

                            where r.activo == true && r.idAutoAutomRelacion == idAutAutoRelacion
                            select new { r, a, e }).Select(x => new AutomatismoVM
                            {
                                idAutoAutomRelacion = x.r.idAutoAutomRelacion,
                                idAutomatismo = x.a.idAutomatismo,
                                nombreAutomatismo = x.a.nombreAutomatismo,

                                idTipoEquipo = x.r.idTipoEquipo,
                                nombreTipoEquipo = x.e.nombreEquipo,

                                idEquipo = x.r.idEquipo,
                                idLugar = x.r.idLugar,
                            }).FirstOrDefault();


            switch (relacion.idTipoEquipo)
            {
                case 1: //Transformador
                    var Transformador = db.TranTransformador.Where(x => x.idTransformador == relacion.idEquipo).FirstOrDefault();
                    var Estacion = db.EstEstacion.Where(x => x.idEstacion == relacion.idLugar).FirstOrDefault();
                    if (Transformador != null)
                    {
                        relacion.nombreEquipo = Transformador.nombreTransformador;
                    }
                    if (Estacion != null)
                    {
                        relacion.nombreLugar = Estacion.nombreEstacion;
                    }
                    break;
                case 2: //Pto Conexion
                    var PtoConexion = db.PCPuntoConexion.Where(x => x.idPuntoConexion == relacion.idEquipo).FirstOrDefault();
                    var EstacionPto = db.EstEstacion.Where(x => x.idEstacion == relacion.idLugar).FirstOrDefault();
                    if (PtoConexion != null)
                    {
                        relacion.nombreEquipo = PtoConexion.nombrePuntoConexion;
                    }
                    if (EstacionPto != null)
                    {
                        relacion.nombreLugar = EstacionPto.nombreEstacion;
                    }
                    break;
                case 5: //Generador
                    var Generador = db.GenGenerador.Where(x => x.idGenerador == relacion.idEquipo).FirstOrDefault();
                    var Central = db.CConCentral.Where(x => x.idCentral == relacion.idLugar).FirstOrDefault();
                    if (Generador != null)
                    {
                        relacion.nombreEquipo = Generador.nombreGenerador;
                    }
                    if (Central != null)
                    {
                        relacion.nombreLugar = Central.nombreCentral;
                    }
                    break;

                case 7: //Capacitor
                    var Capacitor = db.CapCapacitor.Where(x => x.idCapacitor == relacion.idEquipo).FirstOrDefault();
                    var EstacionCapacitor = db.EstEstacion.Where(x => x.idEstacion == relacion.idLugar).FirstOrDefault();
                    if (Capacitor != null)
                    {
                        relacion.nombreEquipo = Capacitor.nombreCapacitor;
                    }
                    if (EstacionCapacitor != null)
                    {
                        relacion.nombreLugar = EstacionCapacitor.nombreEstacion;
                    }
                    break;
            }


            return relacion;
        }


        public int AltaAutomatismoRelacion(AutomatismoVM relacion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultaUnaRelacion = db.AutRelacionAutomatismo.Where(x => x.idAutoAutomRelacion == relacion.idAutoAutomRelacion).FirstOrDefault();
                if (consultaUnaRelacion == null)
                {
                    try
                    {
                        foreach (var equipo in relacion.listaEquipos)
                        {
                            var idEquipo = Convert.ToInt32(equipo);
                            AutRelacionAutomatismo a = new AutRelacionAutomatismo
                            {
                                idAutomatismo = relacion.idAutomatismo,
                                idTipoEquipo = relacion.idTipoEquipo,
                                idLugar = relacion.idLugar,
                                idEquipo = idEquipo,
                                activo = true,
                            };
                            db.AutRelacionAutomatismo.Add(a);
                        }
                        db.SaveChanges();
                        return 200;

                    }
                    catch (Exception e)
                    {
                        return 404;
                    }
                }
                else
                {
                    return 300; //Ya existe
                }

            }

        }

        public int ModificaraAutomatismoRelacion(AutomatismoVM relacion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarUnaRelacion = db.AutRelacionAutomatismo.Where(x => x.idAutoAutomRelacion == relacion.idAutoAutomRelacion).FirstOrDefault();
                if (consultarUnaRelacion != null && relacion.listaEquipos.Count == 1)
                {
                    consultarUnaRelacion.idAutomatismo = relacion.idAutomatismo;
                    consultarUnaRelacion.idTipoEquipo = relacion.idTipoEquipo;
                    consultarUnaRelacion.idLugar = relacion.idLugar;
                    consultarUnaRelacion.idEquipo = relacion.idEquipo;
                    db.SaveChanges();
                }
                else
                {
                    return 300;
                }
            }
            return 200;
        }


        public int EliminarAutomatismoRelacion(AutomatismoVM relacion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                var consultarUnaRelacion = db.AutRelacionAutomatismo.Where(x => x.idAutoAutomRelacion == relacion.idAutoAutomRelacion).FirstOrDefault();

                if (consultarUnaRelacion != null)
                {
                    consultarUnaRelacion.activo = false;

                }
                db.SaveChanges();
            }
            return 200;
        }


        public int RecuperarAutomatismoRelacion(AutomatismoVM relacion)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())

            {
                var consultarUnaRelacion = db.AutRelacionAutomatismo.Where(x => x.idAutoAutomRelacion == relacion.idAutoAutomRelacion).FirstOrDefault();
                if (consultarUnaRelacion != null)
                {
                    consultarUnaRelacion.activo = true;

                    db.SaveChanges();
                    return 200;
                }
                else
                {
                    return 400;
                }

            }

        }
        public object ObtenerListaTipoEquipo(bool activo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                List<EquipoVM> listaEquipo;

                listaEquipo = (from c in db.EquiEquipo
                               where c.activo == activo && c.TieneAutomatismo == true
                               select new EquipoVM
                               {
                                   idEquipo = c.idEquipo,
                                   nombreEquipo = c.nombreEquipo,
                                   idUsarioAlta = c.idUsuarioAlta,
                                   fechaUsarioAlta = c.fechaUsuarioAlta,
                                   codigoEquipo = c.codigoEquipo,
                                   activo = c.activo,
                               }).ToList();

                object json = new { data = listaEquipo };
                return json;
            }

        }

        public object ObtenerListaDeLugar(int idTipoEquipo)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                List<AutomatismoVM> lista = new List<AutomatismoVM>();

                if (idTipoEquipo == 1 || idTipoEquipo == 2 || idTipoEquipo == 7)
                {
                    lista = (from e in db.EstEstacion
                             where e.activo == true
                             select new AutomatismoVM
                             {
                                 idLugar = e.idEstacion,
                                 nombreLugar = e.nombreEstacion,
                             }).ToList();

                }
                if (idTipoEquipo == 5)
                {
                    lista = (from c in db.CConCentral
                             where c.activo == true
                             select new AutomatismoVM
                             {
                                 idLugar = c.idCentral,
                                 nombreLugar = c.nombreCentral,
                             }).ToList();
                }
                return lista;
            }
        }

        public object ObtenerListaDeEquipo(int idTipoEquipo, int idLugar)
        {
            using (IntranetCTREntities db = new IntranetCTREntities())
            {
                List<EquipoVM> lista = new List<EquipoVM>();
                switch (idTipoEquipo)
                {
                    case 1: //Transformador
                        lista = (from t in db.TranTransformador
                                 where t.activo == true && t.idEstacion == idLugar
                                 select new EquipoVM
                                 {
                                     idEquipo = t.idTransformador,
                                     nombreEquipo = t.nombreTransformador,
                                 }).ToList();
                        break;
                    case 2://PtoConexion
                        lista = (from pt in db.PCPuntoConexion
                                 where pt.activo == true && pt.idEstacion == idLugar
                                 select new EquipoVM
                                 {
                                     idEquipo = pt.idPuntoConexion,
                                     nombreEquipo = pt.nombrePuntoConexion,
                                 }).ToList();
                        break;
                    case 7: //Capacitor
                        lista = (from c in db.CapCapacitor
                                 where c.activo == true && c.idEstacion == idLugar
                                 select new EquipoVM
                                 {
                                     idEquipo = c.idCapacitor,
                                     nombreEquipo = c.nombreCapacitor,
                                 }).ToList();
                        break;

                    case 5: //Generador
                        lista = (from g in db.GenGenerador
                                 where g.activo == true && g.idCentral == idLugar
                                 select new EquipoVM
                                 {
                                     idEquipo = g.idGenerador,
                                     nombreEquipo = g.nombreGenerador,
                                 }).ToList();
                        break;
                }
                return lista;
            }
        }


    }

}
